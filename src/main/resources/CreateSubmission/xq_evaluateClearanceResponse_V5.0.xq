xquery version "1.0" encoding "Cp1252";

declare namespace xf = "http://tempuri.org/Utilities_V1/Transformation/CreateSubmissionDecision/";
declare namespace dv = "http://xmlns.oracle.com/dvm";

(: 0 = Stop :)
(: 1 = Continue & ignore this Product :)
(: 2.1 = Continue & Record this Product as Declined with Reason code (set by user) )
(: 2.2 = Continue & Record this Product as Declined with Multiple Submissions as Reason code (set by service)  )
(: 3 = Continue & Record this Product as Clear :)




declare function xf:CreateSubmissionDecision($performAutomatedClearanceStatus as xs:string,
    $submissionProductStatus as xs:string)
    as xs:string {
               
        let $dvmdata := getDVM()        
        let $returnVal := $dvmdata/dv:rows/dv:row[dv:cell[1]=$performAutomatedClearanceStatus and dv:cell[2]=$submissionProductStatus]/dv:cell[3]
        return if (fn:string-length($returnVal) = 0) then
         'unknown'
         else ($returnVal)        
};

declare function getDVM() as element(*){
<dvm name="CreateSubmission_PerformAutomationClearanceResult_SubmissionProductStatus" 
xmlns="http://xmlns.oracle.com/dvm">
  <description>(: 0 = Stop :)
(: 1 = Continue &amp; ignore this Product :)
(: 2.1 = Continue &amp; Record this Product as Declined with Reason code (set by
               user) )
(: 2.2 = Continue &amp; Record this Product as Declined with Multiple
               Submissions as Reason code (set by service)  )
(: 3 = Continue &amp; Record this Product as Clear :)</description>
  <columns>
    <column name="Perform Automation Clearance Result"/>
    <column name="Submission Product Status"/>
    <column name="ExecutionPath"/>
  </columns>
  <rows>
    <row>
      <cell>Duplicate</cell>
      <cell></cell>
      <cell>0</cell>
    </row>
     <row>
      <cell>Duplicate</cell>
      <cell>Default</cell>
      <cell>1</cell>
    </row>
    <row>
      <cell>Duplicate</cell>
      <cell>Accept</cell>
      <cell>1</cell>
    </row>
     <row>
      <cell>Duplicate</cell>
      <cell>Decline</cell>
      <cell>2.1</cell>
    </row>
    <row>
      <cell>Duplicate</cell>
      <cell>Override</cell>
      <cell>3</cell>
    </row>
    
      <row>
      <cell>Conflict</cell>
      <cell></cell>
      <cell>0</cell>
    </row>
     <row>
      <cell>Conflict</cell>
      <cell>Default</cell>
      <cell>2.2</cell>
    </row>
    <row>
      <cell>Conflict</cell>
      <cell>Accept</cell>
      <cell>2.2</cell>
    </row>
     <row>
      <cell>Conflict</cell>
      <cell>Decline</cell>
      <cell>2.1</cell>
    </row>
    <row>
      <cell>Conflict</cell>
      <cell>Override</cell>
      <cell>3</cell>
    </row>
    
      <row>
      <cell>Potential Duplicate</cell>
      <cell></cell>
      <cell>0</cell>
    </row>
     <row>
      <cell>Potential Duplicate</cell>
      <cell>Default</cell>
      <cell>0</cell>
    </row>
    <row>
      <cell>Potential Duplicate</cell>
      <cell>Accept</cell>
      <cell>1</cell>
    </row>
     <row>
      <cell>Potential Duplicate</cell>
      <cell>Decline</cell>
      <cell>2.1</cell>
    </row>
    <row>
      <cell>Potential Duplicate</cell>
      <cell>Override</cell>
      <cell>3</cell>
    </row>
    
      <row>
      <cell>Potential Conflict</cell>
      <cell></cell>
      <cell>0</cell>
    </row>
     <row>
      <cell>Potential Conflict</cell>
      <cell>Default</cell>
      <cell>0</cell>
    </row>
    <row>
      <cell>Potential Conflict</cell>
      <cell>Accept</cell>
      <cell>2.2</cell>
    </row>
     <row>
      <cell>Potential Conflict</cell>
      <cell>Decline</cell>
      <cell>2.1</cell>
    </row>
    <row>
      <cell>Potential Conflict</cell>
      <cell>Override</cell>
      <cell>3</cell>
    </row>
    
      <row>
      <cell>Clear</cell>
      <cell></cell>
      <cell>3or2.2</cell>
    </row>
     <row>
      <cell>Clear</cell>
      <cell>Default</cell>
      <cell>3or2.2</cell>
    </row>
    <row>
      <cell>Clear</cell>
      <cell>Accept</cell>
      <cell>3or2.2</cell>
    </row>
     <row>
      <cell>Clear</cell>
      <cell>Decline</cell>
      <cell>2.1</cell>
    </row>
    <row>
      <cell>Clear</cell>
      <cell>Override</cell>
      <cell>3or2.2</cell>
    </row>
  </rows>
</dvm>

}    ;
   

declare variable $performAutomatedClearanceStatus as xs:string external;
declare variable $submissionProductStatus as xs:string external;

xf:CreateSubmissionDecision($performAutomatedClearanceStatus,
    $submissionProductStatus)