<?xml version="1.0"?>
<xsl:stylesheet
    version = "1.0"
            xmlns:ns32 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/"
            xmlns:ns13 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/"
            xmlns:ns12 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/"
            xmlns:xsd_1 = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/"
            xmlns:ns21 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/"
            xmlns:ns7 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/"
            xmlns:ns5 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/"
            xmlns:ns4 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/"
            xmlns:ns1 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/"
            xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
            xmlns:ns0 = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/"
            xmlns:tns = "http://workstation.znawebservices.zurichna.com"
            xmlns:ns9 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/"
            xmlns:ns2 = "http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/">
            
       <xsl:template match = "/">
        <ns0:createSubmissionResponse>
       
            <xsl:for-each select = "/tns:RecordSubmissionResponse/tns:RecordSubmissionResult/tns:submissionV2/tns:SubmissionV2">
                <xsd_1:submissions>
                  <!-- <xsl:if test = "tns:NEW_RENL_CD != &quot;&quot;">
                        <ns1:objectCategories>
                            <ns1:name>
                                <xsl:text disable-output-escaping = "no">New Renewal Classification</xsl:text>
                            </ns1:name>
                            <ns1:externalReference>
                                <xsl:value-of select = "tns:NEW_RENL_CD"/>
                            </ns1:externalReference>
                        </ns1:objectCategories>
                    </xsl:if> -->
                     
                     
                     <!-- Date Updates -->
                      <xsl:if test = 'tns:ENT_TISTMP != ""'>
                        <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:ENT_TISTMP"/>
                        </ns1:systemCreationDateTime>
                    </xsl:if>
                    
                    <!-- 
                    <xsl:if test = "tns:RECD_DT != &quot;&quot;">
                        <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:RECD_DT"/>
                        </ns1:systemCreationDateTime>
                    </xsl:if>
                     
                    <xsl:if test = "tns:RECD_BY_NM != &quot;&quot;">
                        <ns1:systemCreationById>
                            <xsl:value-of select = "tns:RECD_BY_NM"/>
                        </ns1:systemCreationById>
                    </xsl:if>-->
                    
                    <xsl:if test = "tns:SMSN_ID != &quot;&quot;">
                        <ns5:externalReference>
                            <xsl:value-of select = "tns:SMSN_ID"/>
                        </ns5:externalReference>
                    </xsl:if>
                   <!-- <xsl:if test = "tns:EFF_DT != &quot;&quot;">
                        <ns5:status>
                            <ns1:startDate>
                                <xsl:value-of select = "substring(tns:EFF_DT,1,10)"/>
                            </ns1:startDate>
                            <ns5:state>
                                <xsl:text disable-output-escaping = "no">Initial</xsl:text>
                            </ns5:state>
                        </ns5:status>
                    </xsl:if>
                    <xsl:if test = "tns:ORG_PTY_ID != &quot;&quot;">
                        <ns5:rolesInContractRequest>
                            <xsl:attribute name = "xsi:type">
                                <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                            </xsl:attribute>
                            <ns5:rolePlayer>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns32:Organisation_TO</xsl:text>
                                </xsl:attribute>
                                <ns32:alternateReference>
                                    <ns1:type>
                                        <xsl:text disable-output-escaping = "no">Party ID</xsl:text>
                                    </ns1:type>
                                    <ns1:identifier>
                                        <xsl:value-of select = "tns:ORG_PTY_ID"/>
                                    </ns1:identifier>
                                </ns32:alternateReference>
                            </ns5:rolePlayer>
                            <ns5:partyInvolvedInContractRequestRootType>
                                <ns5:name>
                                    <xsl:text disable-output-escaping = "no">Underwriting Unit</xsl:text>
                                </ns5:name>
                            </ns5:partyInvolvedInContractRequestRootType>
                        </ns5:rolesInContractRequest>
                    </xsl:if>-->
                    
                    <ns5:rolesInContractRequest>
                        <xsl:attribute name = "xsi:type">
                            <xsl:text disable-output-escaping = "no">ns4:Applicant_TO</xsl:text>
                        </xsl:attribute>
                        
                        
                          <xsl:if test = 'tns:CUST_PTY_ROL_SMSN_ENT_TISTMP != ""'>
               
                               <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:CUST_PTY_ROL_SMSN_ENT_TISTMP"/>
                        </ns1:systemCreationDateTime>
                    
                    </xsl:if>
                        
                        <ns5:rolePlayer>
                            <xsl:attribute name = "xsi:type">
                                <xsl:text disable-output-escaping = "no">ns32:Customer_TO</xsl:text>
                            </xsl:attribute>
                            <ns32:rolePlayer>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns32:Company_TO</xsl:text>
                                </xsl:attribute>
                                
                                <!-- 
                                <xsl:if test = "tns:NM != &quot;&quot;">
                                    <ns32:defaultName>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns32:fullName>
                                            <xsl:value-of select = "tns:NM"/>
                                        </ns32:fullName>
                                    </ns32:defaultName>
                                </xsl:if>
                                -->
                                <ns32:rolesInRolePlayer>
                                    <ns32:roleInRolePlayerRootType>
                                        <ns32:name>
                                            <xsl:text disable-output-escaping = "no">Activity Occurrence Performance</xsl:text>
                                        </ns32:name>
                                    </ns32:roleInRolePlayerRootType>
                                  
                                    <xsl:for-each select = "tns:projectName/tns:ProjectName">
                                        <ns32:activityOccurrences>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns21:ObjectTreatment_TO</xsl:text>
                                            </xsl:attribute>
                                            
                                                  <xsl:if test = 'tns:PROJ_NM_ENT_TISTMP != ""'>
               
                               <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:PROJ_NM_ENT_TISTMP"/>
                        </ns1:systemCreationDateTime>
                    
                    </xsl:if>
                        
                        
                                            <xsl:if test = "tns:PROJ_NM != &quot;&quot;">
                                                <ns21:name>
                                                    <xsl:value-of select = "tns:PROJ_NM"/>
                                                </ns21:name>
                                            </xsl:if>
                                            <!-- 
                                            <xsl:if test = "tns:PROJ_NM_ID != &quot;&quot;">
                                                <ns21:externalReference>
                                                    <xsl:value-of select = "tns:PROJ_NM_ID"/>
                                                </ns21:externalReference>
                                            </xsl:if>
                                             -->
                                            <ns21:activityOccurrenceRootType>
                                                <ns21:name>
                                                    <xsl:text disable-output-escaping = "no">Construction Activity</xsl:text>
                                                </ns21:name>
                                            </ns21:activityOccurrenceRootType>
                                        </ns32:activityOccurrences>
                                    </xsl:for-each>
                                </ns32:rolesInRolePlayer>
                                <!-- 
                                <xsl:if test = "tns:CUST_PTY_ID != &quot;&quot;">
                                    <ns32:alternateReference>
                                        <ns1:type>
                                            <xsl:text disable-output-escaping = "no">Party ID</xsl:text>
                                        </ns1:type>
                                        <ns1:identifier>
                                            <xsl:value-of select = "tns:CUST_PTY_ID"/>
                                        </ns1:identifier>
                                    </ns32:alternateReference>
                                </xsl:if>
                                 -->
                            </ns32:rolePlayer>
                        </ns5:rolePlayer>
                    </ns5:rolesInContractRequest>
                    
                    <!-- Date Updates -->
                  
                    
                    
                     <xsl:if test = 'tns:ORG_PTY_ROL_SMSN_ENT_TISTMP != ""'>
                        <ns5:rolesInContractRequest>
                            <xsl:attribute name = "xsi:type">
                                <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                            </xsl:attribute>
                              <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:ORG_PTY_ROL_SMSN_ENT_TISTMP"/>
                        </ns1:systemCreationDateTime>
                            <ns5:partyInvolvedInContractRequestRootType>
                                <ns5:name>
                                    <xsl:text disable-output-escaping = "no">Underwriting Unit</xsl:text>
                                </ns5:name>
                            </ns5:partyInvolvedInContractRequestRootType>
                        </ns5:rolesInContractRequest>
                        </xsl:if>
                    
                    <xsl:for-each select = "tns:submissionProductV2/tns:SubmissionProductV2">
                        <ns5:includedContractRequests>
                            <xsl:attribute name = "xsi:type">
                                <xsl:text disable-output-escaping = "no">ns4:NewBusinessRequest_TO</xsl:text>
                            </xsl:attribute>
                            
                            <xsl:if test = "tns:SMSN_UNDR_ENT_TISTMP != ''">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Direct Assumed</xsl:text>
                                    </ns1:name>
                                    <ns1:startDateTime>
                                        <xsl:value-of select = "tns:SMSN_UNDR_ENT_TISTMP"/>
                                    </ns1:startDateTime>
                                </ns1:objectCategories>
                            </xsl:if>
                             <xsl:if test = "tns:ENT_TISTMP != ''">
                              <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:ENT_TISTMP"/>
                        </ns1:systemCreationDateTime>
                            </xsl:if>
                            <!--<xsl:if test = "tns:INCL_EXCL_CD != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Inclusion Exclusion Code</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:INCL_EXCL_CD"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <ns1:objectCategories>
                                <ns1:name>
                                    <xsl:text disable-output-escaping = "no">Underwriting Program Code</xsl:text>
                                </ns1:name>
                                <xsl:if test = "tns:UNDG_PGM_NM != &quot;&quot;">
                                    <ns1:description>
                                        <xsl:value-of select = "tns:UNDG_PGM_NM"/>
                                    </ns1:description>
                                </xsl:if>
                                <xsl:if test = "tns:UNDG_PGM_CD != &quot;&quot;">
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:UNDG_PGM_CD"/>
                                    </ns1:externalReference>
                                </xsl:if>
                                <xsl:if test = "tns:UNDG_PGM_ID != &quot;&quot;">
                                    <ns1:alternateReference>
                                        <ns1:type>
                                            <xsl:text disable-output-escaping = "no">Underwriting Program ID</xsl:text>
                                        </ns1:type>
                                        <ns1:identifier>
                                            <xsl:value-of select = "tns:UNDG_PGM_ID"/>
                                        </ns1:identifier>
                                    </ns1:alternateReference>
                                </xsl:if>
                            </ns1:objectCategories>
                            <xsl:if test = "tns:NEW_RENL_CD != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">New Renewal Classification</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:NEW_RENL_CD"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:OPTN_ID != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">OPTN ID</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:OPTN_ID"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:SALES_PROF_IND != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Sales Prof Indicator</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:SALES_PROF_IND"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:CNDA_IBC_ID != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Canada IBC ID</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:CNDA_IBC_ID"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:CNDA_BUSN_AREA_ID != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Canada Business Area ID</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:CNDA_BUSN_AREA_ID"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <ns1:objectCategories>
                                <ns1:name>
                                    <xsl:text disable-output-escaping = "no">ASSM FM</xsl:text>
                                </ns1:name>
                                <xsl:if test = "tns:ASSM_FM_NM != &quot;&quot;">
                                    <ns1:description>
                                        <xsl:value-of select = "tns:ASSM_FM_NM"/>
                                    </ns1:description>
                                </xsl:if>
                                <xsl:if test = "tns:ASSM_FM_CD != &quot;&quot;">
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:ASSM_FM_CD"/>
                                    </ns1:externalReference>
                                </xsl:if>
                            </ns1:objectCategories>
                            <xsl:if test = "tns:IBC_CD != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">IBC Code</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:IBC_CD"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <ns1:objectCategories>
                                <ns1:name>
                                    <xsl:text disable-output-escaping = "no">CLS</xsl:text>
                                </ns1:name>
                                <xsl:if test = "tns:CLS_DESC != &quot;&quot;">
                                    <ns1:description>
                                        <xsl:value-of select = "tns:CLS_DESC"/>
                                    </ns1:description>
                                </xsl:if>
                                <ns1:parentCategoryScheme>
                                    <ns1:subCategories>
                                        <ns1:name>
                                            <xsl:text disable-output-escaping = "no">Sub CLS</xsl:text>
                                        </ns1:name>
                                        <xsl:if test = "tns:SUB_CLS_DESC != &quot;&quot;">
                                            <ns1:description>
                                                <xsl:value-of select = "tns:SUB_CLS_DESC"/>
                                            </ns1:description>
                                        </xsl:if>
                                    </ns1:subCategories>
                                </ns1:parentCategoryScheme>
                            </ns1:objectCategories>
                            <xsl:if test = "tns:BUSN_CD != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Business Segment Code</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:BUSN_CD"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:BUSN_NM != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Business Name</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:BUSN_NM"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:DESC0 != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">DESC0</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:DESC0"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:PGM_TYP_ID != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Program Type</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:PGM_TYP_ID"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:CNDA_BRK_TYP_TXT != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Canada Broker Type Text</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:CNDA_BRK_TYP_TXT"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            <xsl:if test = "tns:PACE_ELGB_IND != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">PACE Eligibility Indicator</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:PACE_ELGB_IND"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                             <xsl:if test = "tns:LANG_PREF != &quot;&quot;">
                                <ns1:objectCategories>
                                    <ns1:name>
                                        <xsl:text disable-output-escaping = "no">Language Preference Code</xsl:text>
                                    </ns1:name>
                                    <ns1:externalReference>
                                        <xsl:value-of select = "tns:LANG_PREF"/>
                                    </ns1:externalReference>
                                </ns1:objectCategories>
                            </xsl:if>
                            
                            <xsl:if test = "tns:QLTY_SMSN != &quot;&quot;">
                                <ns1:rolesInContext>
                                    <ns1:rolePlayerReference>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns21:Score_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns21:description>
                                            <xsl:text disable-output-escaping = "no">Submission Quality</xsl:text>
                                        </ns21:description>
                                        <ns21:freeTextScore>
                                            <xsl:value-of select = "tns:QLTY_SMSN"/>
                                        </ns21:freeTextScore>
                                    </ns1:rolePlayerReference>
                                </ns1:rolesInContext>
                            </xsl:if>
                            <xsl:if test = "tns:Z_SHR_PCT != &quot;&quot;">
                                <ns1:rolesInContext>
                                    <ns1:rolePlayerReference>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns21:Score_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns21:description>
                                            <xsl:text disable-output-escaping = "no">Zurich Share Percentage</xsl:text>
                                        </ns21:description>
                                        <ns21:freeTextScore>
                                            <xsl:value-of select = "tns:Z_SHR_PCT"/>
                                        </ns21:freeTextScore>
                                    </ns1:rolePlayerReference>
                                </ns1:rolesInContext>
                            </xsl:if>
                            <xsl:if test = "tns:SMSN_ID != &quot;&quot;">
                                <ns1:dynamicProperties>
                                    <ns1:kind>
                                        <xsl:text disable-output-escaping = "no">Submission ID</xsl:text>
                                    </ns1:kind>
                                    <ns1:theValue>
                                        <xsl:value-of select = "tns:SMSN_ID"/>
                                    </ns1:theValue>
                                </ns1:dynamicProperties>
                            </xsl:if>
                            <xsl:if test = "tns:CMNT_TXT != &quot;&quot;">
                                <ns1:notes>
                                    <ns1:text>
                                        <xsl:value-of select = "tns:CMNT_TXT"/>
                                    </ns1:text>
                                </ns1:notes>
                            </xsl:if>-->
                            <xsl:if test = "tns:RQST_COVG_EFF_DT != &quot;&quot;">
                                <ns5:requestedDate>
                                    <xsl:value-of select = "substring(tns:RQST_COVG_EFF_DT,1,10)"/>
                                </ns5:requestedDate>
                            </xsl:if>
                            <xsl:if test = "tns:CUSTM_INS_SPEC_ID != &quot;&quot;">
                                <ns5:externalReference>
                                    <xsl:value-of select = "tns:CUSTM_INS_SPEC_ID"/>
                                </ns5:externalReference>
                            </xsl:if>
                           <!-- <xsl:if test = "tns:EFF_DT != &quot;&quot;">
                                <ns5:status>
                                    <ns1:startDate>
                                        <xsl:value-of select = "substring(tns:EFF_DT,1,10)"/>
                                    </ns1:startDate>
                                    <ns5:state>
                                        <xsl:text disable-output-escaping = "no">Initial</xsl:text>
                                    </ns5:state>
                                </ns5:status>
                            </xsl:if>
                            <xsl:if test = "tns:IN_OFC_DT != &quot;&quot;"> 
                                <ns5:status>
                                    <ns1:startDate>
                                        <xsl:value-of select = "substring(tns:IN_OFC_DT,1,10)"/>
                                    </ns1:startDate>
                                    <ns5:state>
                                        <xsl:text disable-output-escaping = "no">Received</xsl:text>
                                    </ns5:state>
                                </ns5:status>
                            </xsl:if>-->
                            <xsl:for-each select = "tns:submissionProductStatusV2/tns:SubmissionProductStatusV2">
                                <ns5:status>
                                    <xsl:if test = "tns:STS_TISTMP != ''">
                                        <ns1:startDateTime>
                                            <xsl:value-of select = "tns:STS_TISTMP"/>
                                        </ns1:startDateTime>
                                    </xsl:if>
                                    <!--
                                    <xsl:if test = "tns:RSN_NM != &quot;&quot;">
                                    
                                        <ns1:reasonDescription>
                                            <xsl:value-of select = "tns:RSN_NM"/>
                                        </ns1:reasonDescription>
                                    </xsl:if>
                                    <xsl:if test = "tns:STS_NM != &quot;&quot;">
                                        <ns1:dynamicProperties>
                                            <ns1:kind>
                                                <xsl:text disable-output-escaping = "no">Status Name</xsl:text>
                                            </ns1:kind>
                                            <ns1:theValue>
                                                <xsl:value-of select = "tns:STS_NM"/>
                                            </ns1:theValue>
                                        </ns1:dynamicProperties>
                                    </xsl:if>
                                    <xsl:if test = "tns:SCISPEC_STS_ID != &quot;&quot;">
                                        <ns1:dynamicProperties>
                                            <ns1:kind>
                                                <xsl:text disable-output-escaping = "no">Status ID</xsl:text>
                                            </ns1:kind>
                                            <ns1:theValue>
                                                <xsl:value-of select = "tns:SCISPEC_STS_ID"/>
                                            </ns1:theValue>
                                        </ns1:dynamicProperties>
                                    </xsl:if>
                                    <xsl:if test = "tns:CUSTM_INS_SPEC_ID != &quot;&quot;">
                                        <ns1:dynamicProperties>
                                            <ns1:kind>
                                                <xsl:text disable-output-escaping = "no">Status Product ID</xsl:text>
                                            </ns1:kind>
                                            <ns1:theValue>
                                                <xsl:value-of select = "tns:CUSTM_INS_SPEC_ID"/>
                                            </ns1:theValue>
                                        </ns1:dynamicProperties>
                                    </xsl:if>-->
                                    <xsl:if test = "tns:RSN_CD != &quot;&quot;">
                                        <ns1:reason>
                                            <xsl:value-of select = "tns:RSN_CD"/>
                                        </ns1:reason>
                                    </xsl:if>
                                    <xsl:if test = "tns:STS_CD != &quot;&quot;">
                                        <ns5:state>
                                            <xsl:value-of select = "tns:STS_CD"/>
                                        </ns5:state>
                                    </xsl:if>
                                </ns5:status>
                            </xsl:for-each>
                            <!-- <xsl:if test = "tns:ND_PREM_AMT != &quot;&quot;">
                                <ns5:rolesInContractRequest>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns5:MoneyProvisionInvolvedInContractRequest_TO</xsl:text>
                                    </xsl:attribute>
                                    <ns5:requestMoney>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns12:ParticularMoneyProvision_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns12:moneyProvisionElements>
                                            <ns12:baseAmount>
                                                <ns1:baseAmount>
                                                    <ns2:theAmount>
                                                        <xsl:value-of select = "tns:ND_PREM_AMT"/>
                                                    </ns2:theAmount>
                                               </ns1:baseAmount>
                                            </ns12:baseAmount>
                                            <ns12:moneyProvisionElementRootType>
                                                <ns12:name>
                                                    <xsl:text disable-output-escaping = "no">Premium</xsl:text>
                                                </ns12:name>
                                           </ns12:moneyProvisionElementRootType>
                                            <ns12:kind>
                                                <xsl:text disable-output-escaping = "no">Needed Premium</xsl:text>
                                            </ns12:kind>
                                        </ns12:moneyProvisionElements>
                                    </ns5:requestMoney>
                                </ns5:rolesInContractRequest>
                            </xsl:if>
                            <xsl:if test = "tns:EST_PREM_AMT != &quot;&quot;">
                                <ns5:rolesInContractRequest>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns5:MoneyProvisionInvolvedInContractRequest_TO</xsl:text>
                                    </xsl:attribute>
                                    <ns5:requestMoney>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns12:ParticularMoneyProvision_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns12:moneyProvisionElements>
                                            <ns12:baseAmount>
                                                <ns1:baseAmount>
                                                    <ns2:theAmount>
                                                        <xsl:value-of select = "tns:EST_PREM_AMT"/>
                                                 </ns2:theAmount>
                                                  <ns2:theCurrencyCode>
                                                   <xsl:value-of select = "tns:EST_PREM_CRCY_CD"/>
                                                  </ns2:theCurrencyCode>
                                                </ns1:baseAmount>
                                            </ns12:baseAmount>
                                            <ns12:moneyProvisionElementRootType>
                                                <ns12:name>
                                                    <xsl:text disable-output-escaping = "no">Premium</xsl:text>
                                                </ns12:name>
                                            </ns12:moneyProvisionElementRootType>
                                            <ns12:isEstimate>
                                                <xsl:text disable-output-escaping = "no">true</xsl:text>
                                            </ns12:isEstimate>
                                        </ns12:moneyProvisionElements>
                                    </ns5:requestMoney>
                                </ns5:rolesInContractRequest>
                            </xsl:if>-->
                            
                            <!-- Date Updated -->
                            <xsl:if test = "tns:ORG_PTY_ROL_CISPEC_ENT_TISTMP != ''">
                             <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                                </xsl:attribute>
                                  <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:ORG_PTY_ROL_CISPEC_ENT_TISTMP"/>
                        </ns1:systemCreationDateTime>
                              <ns5:partyInvolvedInContractRequestRootType>
                                    <ns5:name>
                                        <xsl:text disable-output-escaping = "no">Underwriting Unit</xsl:text>
                                    </ns5:name>
                                </ns5:partyInvolvedInContractRequestRootType>
                            </ns5:rolesInContractRequest>
                            </xsl:if>
                            <!-- 
                            <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                                </xsl:attribute>
                                <ns5:rolePlayer>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:Organisation_TO</xsl:text>
                                    </xsl:attribute>
                                    <xsl:if test = "tns:ORG_NM != &quot;&quot;">
                                        <ns32:allNames>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns1:description>
                                                <xsl:text disable-output-escaping = "no">Organization Name</xsl:text>
                                            </ns1:description>
                                            <ns32:fullName>
                                                <xsl:value-of select = "tns:ORG_NM"/>
                                            </ns32:fullName>
                                        </ns32:allNames>
                                    </xsl:if>
                                    <xsl:if test = "tns:ORG_NM_COMB != &quot;&quot;">
                                        <ns32:allNames>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns1:description>
                                                <xsl:text disable-output-escaping = "no">Organization Name COMB</xsl:text>
                                            </ns1:description>
                                            <ns32:fullName>
                                                <xsl:value-of select = "tns:ORG_NM_COMB"/>
                                            </ns32:fullName>
                                       </ns32:allNames>
                                    </xsl:if>
                                    <xsl:if test = "tns:ORG_ABBR_COMB != &quot;&quot;">
                                        <ns32:allNames>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns1:description>
                                                <xsl:text disable-output-escaping = "no">Organization Abbreviation COMB</xsl:text>
                                            </ns1:description>
                                            <ns32:fullName>
                                                <xsl:value-of select = "tns:ORG_ABBR_COMB"/>
                                            </ns32:fullName>
                                       </ns32:allNames>
                                    </xsl:if>
                                    <xsl:if test = "tns:ORG_BU_NM != &quot;&quot;">
                                        <ns32:allNames>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns1:description>
                                                <xsl:text disable-output-escaping = "no">Organization Business Unit Name</xsl:text>
                                            </ns1:description>
                                            <ns32:fullName>
                                                <xsl:value-of select = "tns:ORG_BU_NM"/>
                                            </ns32:fullName>
                                       </ns32:allNames>
                                    </xsl:if>
                                    <xsl:if test = "tns:ORG_BU_ABBR != &quot;&quot;">
                                        <ns32:allNames>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns1:description>
                                                <xsl:text disable-output-escaping = "no">Organization Business Unit Abbreviation</xsl:text>
                                            </ns1:description>
                                            <ns32:fullName>
                                                <xsl:value-of select = "tns:ORG_BU_ABBR"/>
                                            </ns32:fullName>
                                       </ns32:allNames>
                                    </xsl:if>
                                    <xsl:if test = "tns:ORG_ID != &quot;&quot;">
                                        <ns32:externalReference>
                                            <xsl:value-of select = "tns:ORG_ID"/>
                                      </ns32:externalReference>
                                    </xsl:if>
                                    <xsl:if test = "tns:ORG_PTY_ID != &quot;&quot;">
                                        <ns32:alternateReference>
                                            <ns1:type>
                                                <xsl:text disable-output-escaping = "no">Party ID</xsl:text>
                                           </ns1:type>
                                            <ns1:identifier>
                                                <xsl:value-of select = "tns:ORG_PTY_ID"/>
                                            </ns1:identifier>
                                        </ns32:alternateReference>
                                    </xsl:if>
                               </ns5:rolePlayer>
                                <ns5:partyInvolvedInContractRequestRootType>
                                    <ns5:name>
                                        <xsl:text disable-output-escaping = "no">Underwriting Unit</xsl:text>
                                    </ns5:name>
                                </ns5:partyInvolvedInContractRequestRootType>
                            </ns5:rolesInContractRequest> -->
                            
                            <!-- Date Updated -->
                               <xsl:if test = "tns:UNDR_PTY_ROL_CISPEC_ENT_TISTMP != ''">
                                      <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                                </xsl:attribute>
                                <ns5:rolePlayer>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:Person_TO</xsl:text>
                                    </xsl:attribute>
                                      <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:UNDR_PTY_ROL_CISPEC_ENT_TISTMP"/>
                        </ns1:systemCreationDateTime>
                               </ns5:rolePlayer>
                                <ns5:partyInvolvedInContractRequestRootType>
                                    <ns5:name>
                                        <xsl:text disable-output-escaping = "no">Underwriter</xsl:text>
                                    </ns5:name>
                                </ns5:partyInvolvedInContractRequestRootType>
                            </ns5:rolesInContractRequest>
                            </xsl:if>
                            <!-- 
                            <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                                </xsl:attribute>
                                <ns5:rolePlayer>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:Person_TO</xsl:text>
                                    </xsl:attribute>
                                    <xsl:if test = "tns:UNDR_EMAIL_ADDR != &quot;&quot;">
                                        <ns32:contactPreferences>
                                            <ns32:contactPoints>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns32:ElectronicAddress_TO</xsl:text>
                                                </xsl:attribute>
                                                <ns32:contactPointAsString>
                                                    <xsl:value-of select = "tns:UNDR_EMAIL_ADDR"/>
                                                </ns32:contactPointAsString>
                                                <ns32:electronicType>
                                                    <xsl:text disable-output-escaping = "no">Internet Email</xsl:text>
                                                </ns32:electronicType>
                                           </ns32:contactPoints>
                                        </ns32:contactPreferences>
                                    </xsl:if>
                                    <ns32:defaultName>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns32:PersonName_TO</xsl:text>
                                        </xsl:attribute>
                                        <xsl:if test = "tns:UNDR_FST_NM != &quot;&quot;">
                                            <ns32:firstName>
                                                <xsl:value-of select = "tns:UNDR_FST_NM"/>
                                            </ns32:firstName>
                                        </xsl:if>
                                        <xsl:if test = "tns:UNDR_LST_NM != &quot;&quot;">
                                            <ns32:lastName>
                                                <xsl:value-of select = "tns:UNDR_LST_NM"/>
                                            </ns32:lastName>
                                        </xsl:if>
                                    </ns32:defaultName>
                                    <xsl:if test = "tns:UNDR_NOTES_ID != &quot;&quot;">
                                        <ns32:securityRegistrations>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns13:AccessRegistration_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns1:typeName>
                                                <xsl:text disable-output-escaping = "no">LOTUS_NOTES</xsl:text>
                                            </ns1:typeName>
                                            <ns13:userId>
                                                <xsl:value-of select = "tns:UNDR_NOTES_ID"/>
                                            </ns13:userId>
                                        </ns32:securityRegistrations>
                                    </xsl:if>
                                    <xsl:if test = "tns:UNDR_PTY_ID != &quot;&quot;">
                                        <ns32:alternateReference>
                                            <ns1:type>
                                                <xsl:text disable-output-escaping = "no">Party ID</xsl:text>
                                            </ns1:type>
                                            <ns1:identifier>
                                                <xsl:value-of select = "tns:UNDR_PTY_ID"/>
                                            </ns1:identifier>
                                        </ns32:alternateReference>
                                    </xsl:if>
                               </ns5:rolePlayer>
                                <ns5:partyInvolvedInContractRequestRootType>
                                    <ns5:name>
                                        <xsl:text disable-output-escaping = "no">Underwriter</xsl:text>
                                    </ns5:name>
                                </ns5:partyInvolvedInContractRequestRootType>
                            </ns5:rolesInContractRequest>
                            <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                                </xsl:attribute>
                                <ns5:rolePlayer>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:Person_TO</xsl:text>
                                    </xsl:attribute>
                                    <xsl:if test = "tns:ASST_UNDR_EMAIL_ADDR != &quot;&quot;">
                                        <ns32:contactPreferences>
                                            <ns32:contactPoints>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns32:ElectronicAddress_TO</xsl:text>
                                                </xsl:attribute>
                                                <ns32:contactPointAsString>
                                                    <xsl:value-of select = "tns:ASST_UNDR_EMAIL_ADDR"/>
                                                </ns32:contactPointAsString>
                                                <ns32:electronicType>
                                                    <xsl:text disable-output-escaping = "no">Internet Email</xsl:text>
                                                </ns32:electronicType>
                                            </ns32:contactPoints>
                                        </ns32:contactPreferences>
                                    </xsl:if>
                                    <ns32:defaultName>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns32:PersonName_TO</xsl:text>
                                        </xsl:attribute>
                                        <xsl:if test = "tns:ASST_UNDR_FST_NM != &quot;&quot;">
                                            <ns32:firstName>
                                                <xsl:value-of select = "tns:ASST_UNDR_FST_NM"/>
                                            </ns32:firstName>
                                        </xsl:if>
                                        <xsl:if test = "tns:ASST_UNDR_LST_NM != &quot;&quot;">
                                            <ns32:lastName>
                                                <xsl:value-of select = "tns:ASST_UNDR_LST_NM"/>
                                            </ns32:lastName>
                                        </xsl:if>
                                    </ns32:defaultName>
                                    <xsl:if test = "tns:ASST_UNDR_NOTES_ID != &quot;&quot;">
                                        <ns32:securityRegistrations>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns13:AccessRegistration_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns1:typeName>
                                                <xsl:text disable-output-escaping = "no">LOTUS_NOTES</xsl:text>
                                            </ns1:typeName>
                                            <ns13:userId>
                                                <xsl:value-of select = "tns:ASST_UNDR_NOTES_ID"/>
                                            </ns13:userId>
                                        </ns32:securityRegistrations>
                                    </xsl:if>
                                    <xsl:if test = "tns:ASST_UNDR_PTY_ID != &quot;&quot;">
                                        <ns32:alternateReference>
                                            <ns1:type>
                                                <xsl:text disable-output-escaping = "no">Party ID</xsl:text>
                                           </ns1:type>
                                            <ns1:identifier>
                                                <xsl:value-of select = "tns:ASST_UNDR_PTY_ID"/>
                                            </ns1:identifier>
                                        </ns32:alternateReference>
                                    </xsl:if>
                                </ns5:rolePlayer>
                                <ns5:partyInvolvedInContractRequestRootType>
                                    <ns5:name>
                                        <xsl:text disable-output-escaping = "no">Underwriting Assistant</xsl:text>
                                   </ns5:name>
                                </ns5:partyInvolvedInContractRequestRootType>
                            </ns5:rolesInContractRequest>
                            <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                                </xsl:attribute>
                                <ns5:rolePlayer>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:Person_TO</xsl:text>
                                    </xsl:attribute>
                                    <ns32:defaultName>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns32:PersonName_TO</xsl:text>
                                        </xsl:attribute>
                                        <xsl:if test = "tns:BUSN_DVL_LDR_FST_NM != &quot;&quot;">
                                            <ns32:firstName>
                                                <xsl:value-of select = "tns:BUSN_DVL_LDR_FST_NM"/>
                                            </ns32:firstName>
                                        </xsl:if>
                                        <xsl:if test = "tns:BUSN_DVL_LDR_LST_NM != &quot;&quot;">
                                            <ns32:lastName>
                                                <xsl:value-of select = "tns:BUSN_DVL_LDR_LST_NM"/>
                                            </ns32:lastName>
                                        </xsl:if>
                                    </ns32:defaultName>
                                    <xsl:if test = "tns:BUSN_DVL_LDR_NOTES_ID != &quot;&quot;">
                                        <ns32:securityRegistrations>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns13:AccessRegistration_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns1:typeName>
                                                <xsl:text disable-output-escaping = "no">LOTUS_NOTES</xsl:text>
                                            </ns1:typeName>
                                            <ns13:userId>
                                                <xsl:value-of select = "tns:BUSN_DVL_LDR_NOTES_ID"/>
                                            </ns13:userId>
                                        </ns32:securityRegistrations>
                                    </xsl:if>
                                    <xsl:if test = "tns:BUSN_DVL_LDR_PTY_ID != &quot;&quot;">
                                        <ns32:alternateReference>
                                            <ns1:type>
                                                <xsl:text disable-output-escaping = "no">Party ID</xsl:text>
                                            </ns1:type>
                                            <ns1:identifier>
                                                <xsl:value-of select = "tns:BUSN_DVL_LDR_PTY_ID"/>
                                            </ns1:identifier>
                                        </ns32:alternateReference>
                                    </xsl:if>
                               </ns5:rolePlayer>
                                <ns5:partyInvolvedInContractRequestRootType>
                                    <ns5:name>
                                        <xsl:text disable-output-escaping = "no">Business Development Leader</xsl:text>
                                    </ns5:name>
                                </ns5:partyInvolvedInContractRequestRootType>
                            </ns5:rolesInContractRequest>
                            <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns5:PartyInvolvedInContractRequest_TO</xsl:text>
                                </xsl:attribute>
                                <ns5:rolePlayer>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:Organisation_TO</xsl:text>
                                    </xsl:attribute>
                                    <xsl:if test = "tns:CARR_CD != &quot;&quot;">
                                        <ns32:externalReference>
                                            <xsl:value-of select = "tns:CARR_CD"/>
                                        </ns32:externalReference>
                                    </xsl:if>
                               </ns5:rolePlayer>
                                <ns5:partyInvolvedInContractRequestRootType>
                                    <ns5:name>
                                        <xsl:text disable-output-escaping = "no">Prior Insurer</xsl:text>
                                    </ns5:name>
                                </ns5:partyInvolvedInContractRequestRootType>
                            </ns5:rolesInContractRequest>
                            <ns5:rolesInContractRequest> 
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns4:Applicant_TO</xsl:text>
                                </xsl:attribute>
                                <ns5:rolePlayer>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:Customer_TO</xsl:text>
                                    </xsl:attribute>
                                    <ns32:rolePlayer>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns32:Company_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns32:contactPreferences>
                                            <ns32:preferedContactPersonReference>
                                                <xsl:choose>
                                                    <xsl:when test = "tns:INSD_CNTA_IND = &quot;N&quot;">
                                                        <ns1:basicDataIncomplete>
                                                            <xsl:text disable-output-escaping = "no">true</xsl:text>
                                                        </ns1:basicDataIncomplete>
                                                    </xsl:when>
                                                    <xsl:when test = "tns:INSD_CNTA_IND = &quot;D&quot;">
                                                        <ns1:basicDataIncomplete>
                                                            <xsl:text disable-output-escaping = "no">false</xsl:text>
                                                        </ns1:basicDataIncomplete>
                                                    </xsl:when>
                                                </xsl:choose>
                                                <xsl:if test = "tns:INSD_EMAIL != &quot;&quot;">
                                                    <ns32:contactPreferences>
                                                        <ns32:contactPoints>
                                                            <xsl:attribute name = "xsi:type">
                                                                <xsl:text disable-output-escaping = "no">ns32:ElectronicAddress_TO</xsl:text>
                                                            </xsl:attribute>
                                                            <ns32:contactPointAsString>
                                                                <xsl:value-of select = "tns:INSD_EMAIL"/>
                                                            </ns32:contactPointAsString>
                                                            <ns32:electronicType>
                                                                <xsl:text disable-output-escaping = "no">Internet Email</xsl:text>
                                                            </ns32:electronicType>
                                                        </ns32:contactPoints>
                                                    </ns32:contactPreferences>
                                                </xsl:if>
                                                <ns32:defaultName>
                                                    <xsl:attribute name = "xsi:type">
                                                        <xsl:text disable-output-escaping = "no">ns32:PersonName_TO</xsl:text>
                                                    </xsl:attribute>
                                                    <xsl:if test = "tns:INSD_FST_NM != &quot;&quot;">
                                                        <ns32:firstName>
                                                            <xsl:value-of select = "tns:INSD_FST_NM"/>
                                                        </ns32:firstName>
                                                    </xsl:if>
                                                    <xsl:if test = "tns:INSD_LST_NM != &quot;&quot;">
                                                        <ns32:lastName>
                                                            <xsl:value-of select = "tns:INSD_LST_NM"/>
                                                        </ns32:lastName>
                                                    </xsl:if>
                                                </ns32:defaultName>
                                            </ns32:preferedContactPersonReference>
                                        </ns32:contactPreferences>
                                        <ns32:rolesInRolePlayer>
                                            <ns32:roleInRolePlayerRootType>
                                                <ns32:name>
                                                    <xsl:text disable-output-escaping = "no">Activity Occurrence Performance</xsl:text>
                                               </ns32:name>
                                            </ns32:roleInRolePlayerRootType>
                                            <xsl:if test = "tns:PROJ_NM != &quot;&quot;">
                                                <ns32:activityOccurrences>
                                                    <ns21:name>
                                                        <xsl:value-of select = "tns:PROJ_NM"/>
                                                    </ns21:name>
                                                    <ns21:activityOccurrenceRootType>
                                                        <ns21:name>
                                                            <xsl:text disable-output-escaping = "no">Construction Activity</xsl:text>
                                                        </ns21:name>
                                                    </ns21:activityOccurrenceRootType>
                                                </ns32:activityOccurrences>
                                            </xsl:if>
                                        </ns32:rolesInRolePlayer>
                                        <xsl:if test = "tns:ICC_NBR != &quot;&quot;">
                                            <ns32:partyRegistrations>
                                                <ns21:externalReference>
                                                    <xsl:value-of select = "tns:ICC_NBR"/>
                                                </ns21:externalReference>
                                                <ns32:partyRegistrationRootType>
                                                    <ns32:name>
                                                        <xsl:text disable-output-escaping = "no">Interstate Commerce Commission</xsl:text>
                                                    </ns32:name>
                                                </ns32:partyRegistrationRootType>
                                            </ns32:partyRegistrations>
                                        </xsl:if>
                                        <xsl:if test = "tns:US_DOT_NBR != &quot;&quot;">
                                            <ns32:partyRegistrations>
                                                <ns21:externalReference>
                                                    <xsl:value-of select = "tns:US_DOT_NBR"/>
                                                </ns21:externalReference>
                                                <ns32:partyRegistrationRootType>
                                                    <ns32:name>
                                                        <xsl:text disable-output-escaping = "no">United States Department of Transporation</xsl:text>
                                                   </ns32:name>
                                                </ns32:partyRegistrationRootType>
                                            </ns32:partyRegistrations>
                                        </xsl:if>
                                   </ns32:rolePlayer>
                               </ns5:rolePlayer>
                            </ns5:rolesInContractRequest>-->
                            <!-- Date updated -->
                              <xsl:if test = "tns:PRDR_PTY_ROL_CISPEC_ENT_TISTMP != ''">
                              <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns4:RoleInFinancialServicesRequest_TO</xsl:text>
                                </xsl:attribute>
                                <ns4:producerAgreement>
                                <ns9:rolesInIntermediaryAgreement>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns9:PartyInvolvedInIntermediaryAgreement_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns9:rolePlayer>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns32:Organisation_TO</xsl:text>
                                                </xsl:attribute>
                                          
                                            <ns1:systemCreationDateTime>
                            <xsl:value-of select = "tns:PRDR_PTY_ROL_CISPEC_ENT_TISTMP"/>
                        </ns1:systemCreationDateTime>
                                            </ns9:rolePlayer>
                                        </ns9:rolesInIntermediaryAgreement>
                                    
                                </ns4:producerAgreement>
                                <ns4:roleInFinancialServicesRequestRootType>
                                    <ns4:name>
                                        <xsl:text disable-output-escaping = "no">Producer Intermediary Agreement</xsl:text>
                                    </ns4:name>
                                </ns4:roleInFinancialServicesRequestRootType>
                            </ns5:rolesInContractRequest>
                            </xsl:if>
                            
                            <!-- PACE INDICATOR CHANGE OCT-25-2016 -->
                            
                            
                            
                            <xsl:if test = "tns:PACE_ELGB_IND != &quot;&quot;">
                                <ns5:rolesInContractRequest>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns5:AssessmentResultInvolvedInContractRequest_TO</xsl:text>
                                    </xsl:attribute>
                                 <ns5:assessmentResultInvolvedInContractRequestRootType> 
                             
                  	<ns5:name><xsl:text disable-output-escaping = "no">PACE Eligibility Assessment</xsl:text></ns5:name>
                  </ns5:assessmentResultInvolvedInContractRequestRootType>
                  <ns5:assessmentResult>
                       <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns21:Score_TO</xsl:text>
                                    </xsl:attribute>
                     <ns21:freeTextScore> <xsl:value-of select = "tns:PACE_ELGB_IND"/></ns21:freeTextScore>
                  </ns5:assessmentResult>   
                                </ns5:rolesInContractRequest>
                            </xsl:if>
                            
                            
                            <!-- 
                            <ns5:rolesInContractRequest>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns4:RoleInFinancialServicesRequest_TO</xsl:text>
                                </xsl:attribute>
                                <ns4:producerAgreement>
                                    <xsl:if test = "tns:PRDR_NBR != &quot;&quot;">
                                        <ns5:externalReference>
                                            <xsl:value-of select = "tns:PRDR_NBR"/>
                                       </ns5:externalReference>
                                    </xsl:if>
                                    <xsl:if test = "tns:PRDR_PTY_ID != &quot;&quot;">
                                        <ns5:alternateReference>
                                            <ns1:type>
                                                <xsl:text disable-output-escaping = "no">Party ID</xsl:text>
                                            </ns1:type>
                                            <ns1:identifier>
                                                <xsl:value-of select = "tns:PRDR_PTY_ID"/>
                                            </ns1:identifier>
                                        </ns5:alternateReference>
                                    </xsl:if>
                                    <ns9:rolesInIntermediaryAgreement>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns9:PartyInvolvedInIntermediaryAgreement_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns9:rolePlayer>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:Organisation_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns32:contactPreferences>
                                                <ns32:preferedContactPersonReference>
                                                    <xsl:choose>
                                                        <xsl:when test = "tns:PRDR_CNTA_IND = &quot;N&quot;">
                                                            <ns1:basicDataIncomplete>
                                                                <xsl:text disable-output-escaping = "no">true</xsl:text>
                                                            </ns1:basicDataIncomplete>
                                                        </xsl:when>
                                                        <xsl:when test = "tns:PRDR_CNTA_IND = &quot;D&quot;">
                                                            <ns1:basicDataIncomplete>
                                                                <xsl:text disable-output-escaping = "no">false</xsl:text>
                                                            </ns1:basicDataIncomplete>
                                                        </xsl:when>
                                                    </xsl:choose>
                                                    <xsl:if test = "tns:PRDR_EMAIL != &quot;&quot;">
                                                        <ns32:contactPreferences>
                                                            <ns32:contactPoints>
                                                                <xsl:attribute name = "xsi:type">
                                                                    <xsl:text disable-output-escaping = "no">ns32:ElectronicAddress_TO</xsl:text>
                                                                </xsl:attribute>
                                                                <ns32:contactPointAsString>
                                                                    <xsl:value-of select = "tns:PRDR_EMAIL"/>
                                                                </ns32:contactPointAsString>
                                                                <ns32:electronicType>
                                                                    <xsl:text disable-output-escaping = "no">Internet Email</xsl:text>
                                                                </ns32:electronicType>
                                                            </ns32:contactPoints>
                                                        </ns32:contactPreferences>
                                                    </xsl:if>
                                                    <ns32:defaultName>
                                                        <xsl:attribute name = "xsi:type">
                                                            <xsl:text disable-output-escaping = "no">ns32:PersonName_TO</xsl:text>
                                                        </xsl:attribute>
                                                        <xsl:if test = "tns:PRDR_FST_NM != &quot;&quot;">
                                                            <ns32:firstName>
                                                                <xsl:value-of select = "tns:PRDR_FST_NM"/>
                                                            </ns32:firstName>
                                                        </xsl:if>
                                                        <xsl:if test = "tns:PRDR_LST_NM != &quot;&quot;">
                                                            <ns32:lastName>
                                                                <xsl:value-of select = "tns:PRDR_LST_NM"/>
                                                            </ns32:lastName>
                                                        </xsl:if>
                                                    </ns32:defaultName>
                                                </ns32:preferedContactPersonReference>
                                            </ns32:contactPreferences>
                                       </ns9:rolePlayer>
                                    </ns9:rolesInIntermediaryAgreement>
                                    <xsl:if test = "tns:ORGL_PRDR_NBR != &quot;&quot;">
                                        <ns32:alternateReference>
                                            <ns1:type>
                                                <xsl:text disable-output-escaping = "no">Original Number</xsl:text>
                                            </ns1:type>
                                            <ns1:identifier>
                                                <xsl:value-of select = "ORGL_PRDR_NBR"/>
                                            </ns1:identifier>
                                        </ns32:alternateReference>
                                    </xsl:if>
                                    <xsl:if test = "tns:PRDR_STS != &quot;&quot;">
                                        <ns5:status>
                                            <xsl:value-of select = "tns:PRDR_STS"/>
                                        </ns5:status>
                                    </xsl:if>
                                    <xsl:if test = "tns:PRDR_NM != &quot;&quot;">
                                        <ns9:rolesInIntermediaryAgreement>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns9:PartyInvolvedInIntermediaryAgreement_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns9:rolePlayer>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns32:Organisation_TO</xsl:text>
                                                </xsl:attribute>
                                                <ns32:defaultName>
                                                    <xsl:attribute name = "xsi:type">
                                                        <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                                    </xsl:attribute>
                                                    <ns32:fullName>
                                                        <xsl:value-of select = "tns:PRDR_NM"/>
                                                    </ns32:fullName>
                                                </ns32:defaultName>
                                            </ns9:rolePlayer>
                                        </ns9:rolesInIntermediaryAgreement>
                                    </xsl:if>
                                </ns4:producerAgreement>
                                <ns4:roleInFinancialServicesRequestRootType>
                                    <ns4:name>
                                        <xsl:text disable-output-escaping = "no">Producer Intermediary Agreement</xsl:text>
                                    </ns4:name>
                                </ns4:roleInFinancialServicesRequestRootType>
                            </ns5:rolesInContractRequest>
                            <xsl:if test = "tns:INS_SPEC_TYP_ID != &quot;&quot;">
                                <ns5:alternateReference>
                                    <ns1:type>
                                        <xsl:text disable-output-escaping = "no">Insurance Specification Type ID</xsl:text>
                                    </ns1:type>
                                    <ns1:identifier>
                                        <xsl:value-of select = "tns:INS_SPEC_TYP_ID"/>
                                    </ns1:identifier>
                                </ns5:alternateReference>
                            </xsl:if>
                            

								<ns4:createdFinancialServicesAgreement
									xsi:type="ns4:CommercialAgreement_TO">
									<ns5:externalReference><xsl:value-of select = "tns:RENL_OF_AGMT_ID"/></ns5:externalReference>
									<ns5:description>Renewal Agreement</ns5:description>
									<ns5:contractSpecification xsi:type="ns5:MarketableProduct_TO">
										<ns5:externalReference><xsl:value-of select = "tns:RENL_OF_POL_SYM"/></ns5:externalReference>
									</ns5:contractSpecification>
								</ns4:createdFinancialServicesAgreement>                                 
                            
                            -->
                            
                              <xsl:if test = "tns:RQST_COVG_EXPI_DT != &quot;&quot;">
                                <ns5:requestedExpirationDate>
                                    <xsl:value-of select = "substring(tns:RQST_COVG_EXPI_DT,1,10)"/>
                                </ns5:requestedExpirationDate>
                            </xsl:if>

                            
                            
                            <ns4:targetProduct>
                                <!--<xsl:if test = "tns:EXPO_LOC_CLS_CD != &quot;&quot;">
                                    <ns1:objectCategories>
                                        <ns1:name>
                                            <xsl:text disable-output-escaping = "no">Exposure</xsl:text>
                                       </ns1:name>
                                        <ns1:externalReference>
                                            <xsl:value-of select = "tns:EXPO_LOC_CLS_CD"/>
                                        </ns1:externalReference>
                                    </ns1:objectCategories>
                                </xsl:if> -->
                                <xsl:if test = "tns:TMPL_INS_SPEC_ID != &quot;&quot;">
                                    <ns5:externalReference>
                                        <xsl:value-of select = "tns:TMPL_INS_SPEC_ID"/>
                                    </ns5:externalReference>
                                </xsl:if>
                               <!-- <xsl:if test = "tns:PRDT_ABBR != &quot;&quot;">
                                    
                                    <ns5:shortName>
                                        <xsl:value-of select = "tns:PRDT_ABBR"/>
                                    </ns5:shortName>
                                </xsl:if>
                                <ns5:targetMarketSegments>
                                    <xsl:if test = "tns:SIC_NM != &quot;&quot;">
                                        <ns1:name>
                                            <xsl:value-of select = "tns:SIC_NM"/>
                                       </ns1:name>
                                    </xsl:if>
                                    <xsl:if test = "tns:SIC_CD != &quot;&quot;">
                                        <ns1:externalReference>
                                            <xsl:value-of select = "tns:SIC_CD"/>
                                        </ns1:externalReference>
                                    </xsl:if>
                                    <ns1:parentCategoryScheme>
                                        <ns1:name>
                                            <xsl:text disable-output-escaping = "no">Standard Industrial Classification</xsl:text>
                                       </ns1:name>
                                    </ns1:parentCategoryScheme>
                               </ns5:targetMarketSegments>
                                <xsl:if test = "tns:CNDA_SIC_CD != &quot;&quot;">
                                    <ns5:targetMarketSegments>
                                        <ns1:externalReference>
                                            <xsl:value-of select = "tns:CNDA_SIC_CD"/>
                                        </ns1:externalReference>
                                        <ns1:parentCategoryScheme>
                                            <ns1:name>
                                                <xsl:text disable-output-escaping = "no">Canada Standard Industrial Classification</xsl:text>
                                           </ns1:name>
                                        </ns1:parentCategoryScheme>
                                    </ns5:targetMarketSegments>
                                </xsl:if> -->
                            </ns4:targetProduct>
                          <!-- Moved request Expiration Date -->
                        </ns5:includedContractRequests>
                    </xsl:for-each>
                    <!-- <xsl:if test = "tns:SYS_SRC_CD != &quot;&quot;">
                        <ns5:alternateReference>
                            <ns1:type>
                                <xsl:text disable-output-escaping = "no">Source System Abbreviation</xsl:text>
                            </ns1:type>
                            <ns1:identifier>
                                <xsl:value-of select = "tns:SYS_SRC_CD"/>
                            </ns1:identifier>
                        </ns5:alternateReference>
                    </xsl:if>
                    <xsl:if test = "tns:SYS_SRC_TXT != &quot;&quot;">
                        <ns32:alternateReference>
                            <ns1:type>
                                <xsl:text disable-output-escaping = "no">Source System Text</xsl:text>
                            </ns1:type>
                            <ns1:identifier>
                                <xsl:value-of select = "tns:SYS_SRC_TXT"/>
                            </ns1:identifier>
                        </ns32:alternateReference>
                    </xsl:if>-->
                </xsd_1:submissions>
            </xsl:for-each>
            <xsl:if test = "/tns:RecordSubmissionResponse/tns:RecordSubmissionResult/tns:submissionName!=&quot;&quot;">
                <xsd_1:customer>
                    <xsl:for-each select = "/tns:RecordSubmissionResponse/tns:RecordSubmissionResult/tns:submissionName">
                    
                    <ns1:systemCreationDateTime><xsl:value-of select = "tns:ENT_TISTMP"/></ns1:systemCreationDateTime>
                        <ns32:rolePlayer>
                            <xsl:attribute name = "xsi:type">
                                <xsl:text disable-output-escaping = "no">ns32:Company_TO</xsl:text>
                            </xsl:attribute>
                            <!--<xsl:if test = "tns:CUST_REVNW != &quot;&quot;">
                                <ns1:rolesInContext>
                                    <ns1:rolePlayerReference>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns7:SpecifiedContent_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns7:includedContents>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns7:AskedQuestion_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns7:isBasedOn>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns7:QuestionTemplate_TO</xsl:text>
                                                </xsl:attribute>
                                                <ns7:externalReference>
                                                    <xsl:text disable-output-escaping = "no">Account Revenue</xsl:text>
                                                </ns7:externalReference>
                                            </ns7:isBasedOn>
                                            <ns7:selectedAnswers>
                                                <ns7:selectedAnswer>
                                                    <ns7:externalReference>
                                                        <xsl:value-of select = "tns:CUST_REVNW"/>
                                                    </ns7:externalReference>
                                                </ns7:selectedAnswer>
                                            </ns7:selectedAnswers>
                                        </ns7:includedContents>
                                    </ns1:rolePlayerReference>
                                </ns1:rolesInContext>
                            </xsl:if>
                            <xsl:if test = "tns:LOC_WITH_TIV != &quot;&quot;">
                                <ns1:rolesInContext>
                                    <ns1:rolePlayerReference>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns7:SpecifiedContent_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns7:includedContents>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns7:AskedQuestion_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns7:isBasedOn>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns7:QuestionTemplate_TO</xsl:text>
                                                </xsl:attribute>
                                                <ns7:externalReference>
                                                    <xsl:text disable-output-escaping = "no">Location TIV</xsl:text>
                                                </ns7:externalReference>
                                            </ns7:isBasedOn>
                                            <ns7:selectedAnswers>
                                                <ns7:selectedAnswer>
                                                    <ns7:externalReference>
                                                        <xsl:value-of select = "tns:LOC_WITH_TIV"/>
                                                    </ns7:externalReference>
                                                </ns7:selectedAnswer>
                                            </ns7:selectedAnswers>
                                        </ns7:includedContents>
                                    </ns1:rolePlayerReference>
                                </ns1:rolesInContext>
                            </xsl:if>
                            <xsl:if test = "tns:UMB_OCCUR_LMT != &quot;&quot;">
                                <ns1:rolesInContext>
                                    <ns1:rolePlayerReference>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns7:SpecifiedContent_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns7:includedContents>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns7:AskedQuestion_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns7:isBasedOn>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns7:QuestionTemplate_TO</xsl:text>
                                                </xsl:attribute>
                                                <ns7:externalReference>
                                                    <xsl:text disable-output-escaping = "no">Umbrella GLX Limit</xsl:text>
                                                </ns7:externalReference>
                                            </ns7:isBasedOn>
                                            <ns7:selectedAnswers>
                                                <ns7:selectedAnswer>
                                                    <ns7:externalReference>
                                                        <xsl:value-of select = "tns:UMB_OCCUR_LMT"/>
                                                    </ns7:externalReference>
                                                </ns7:selectedAnswer>
                                            </ns7:selectedAnswers>
                                        </ns7:includedContents>
                                    </ns1:rolePlayerReference>
                                </ns1:rolesInContext>
                            </xsl:if>
                            <xsl:choose>
                                <xsl:when test = "tns:PACE_INELGB_IND = &quot;false&quot;">
                                    <ns1:rolesInContext>
                                        <ns1:rolePlayerReference>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns7:SpecifiedContent_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns7:includedContents>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns7:AskedQuestion_TO</xsl:text>
                                                </xsl:attribute>
                                                <ns7:isBasedOn>
                                                    <xsl:attribute name = "xsi:type">
                                                        <xsl:text disable-output-escaping = "no">ns7:QuestionTemplate_TO</xsl:text>
                                                    </xsl:attribute>
                                                    <ns7:externalReference>
                                                        <xsl:text disable-output-escaping = "no">Permanently Ineligible</xsl:text>
                                                    </ns7:externalReference>
                                                </ns7:isBasedOn>
                                                <ns7:selectedAnswers>
                                                    <xsl:if test = "tns:PACE_INELGB_RSN != &quot;&quot;">
                                                        <reason>
                                                            <xsl:value-of select = "tns:PACE_INELGB_RSN"/>
                                                        </reason>
                                                    </xsl:if>
                                                    <ns7:selectedAnswer>
                                                        <ns7:externalReference>
                                                            <xsl:text disable-output-escaping = "no">N</xsl:text>
                                                        </ns7:externalReference>
                                                    </ns7:selectedAnswer>
                                                </ns7:selectedAnswers>
                                            </ns7:includedContents>
                                        </ns1:rolePlayerReference>
                                    </ns1:rolesInContext>
                                </xsl:when>
                                <xsl:when test = "tns:PACE_INELGB_IND = &quot;true&quot;">
                                    <ns1:rolesInContext>
                                        <ns1:rolePlayerReference>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns7:SpecifiedContent_TO</xsl:text>
                                            </xsl:attribute>
                                            <ns7:includedContents>
                                                <xsl:attribute name = "xsi:type">
                                                    <xsl:text disable-output-escaping = "no">ns7:AskedQuestion_TO</xsl:text>
                                                </xsl:attribute>
                                                <ns7:isBasedOn>
                                                    <xsl:attribute name = "xsi:type">
                                                        <xsl:text disable-output-escaping = "no">ns7:QuestionTemplate_TO</xsl:text>
                                                    </xsl:attribute>
                                                    <ns7:externalReference>
                                                        <xsl:text disable-output-escaping = "no">Permanently Ineligible</xsl:text>
                                                    </ns7:externalReference>
                                                </ns7:isBasedOn>
                                                <ns7:selectedAnswers>
                                                    <xsl:if test = "tns:PACE_INELGB_RSN != &quot;&quot;">
                                                        <reason>
                                                            <xsl:value-of select = "tns:PACE_INELGB_RSN"/>
                                                        </reason>
                                                    </xsl:if>
                                                    <ns7:selectedAnswer>
                                                        <ns7:externalReference>
                                                            <xsl:text disable-output-escaping = "no">Y</xsl:text>
                                                        </ns7:externalReference>
                                                    </ns7:selectedAnswer>
                                                </ns7:selectedAnswers>
                                            </ns7:includedContents>
                                        </ns1:rolePlayerReference>
                                    </ns1:rolesInContext>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:if test = "tns:TRD_NM != &quot;&quot;">
                                <ns32:allNames>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                    </xsl:attribute>
                                    <ns32:partyNameRootType>
                                        <ns32:name>
                                            <xsl:text disable-output-escaping = "no">Trading Name</xsl:text>
                                        </ns32:name>
                                    </ns32:partyNameRootType>
                                    <ns32:fullName>
                                        <xsl:value-of select = "tns:TRD_NM"/>
                                    </ns32:fullName>
                               </ns32:allNames>
                            </xsl:if>-->
                            
                            <!-- Date updates -->
                            <ns32:contactPreferences>
                        
                                <xsl:for-each select = "tns:address/tns:Address">
                                    <xsl:if test ='tns:PTY_ROL_GLOC_ENT_TISTMP !=""'>
                                        <ns32:contactPoints>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:PostalAddress_TO</xsl:text>
                                            </xsl:attribute>
                                            
                 <ns1:systemCreationDateTime><xsl:value-of select = "tns:PTY_ROL_GLOC_ENT_TISTMP"/></ns1:systemCreationDateTime>
                                            
                                 </ns32:contactPoints>
                                 </xsl:if>
                                 </xsl:for-each>           
                             </ns32:contactPreferences>
                            <!-- 
                            <ns32:contactPreferences>
                                <xsl:if test = "tns:INTERNET_ADDR != &quot;&quot;">
                                    <ns32:contactPoints>
                                        <xsl:attribute name = "xsi:type">
                                            <xsl:text disable-output-escaping = "no">ns32:ElectronicAddress_TO</xsl:text>
                                        </xsl:attribute>
                                        <ns32:contactPointAsString>
                                            <xsl:value-of select = "tns:INTERNET_ADDR"/>
                                        </ns32:contactPointAsString>
                                    </ns32:contactPoints>
                                </xsl:if>
                                <xsl:for-each select = "tns:address/tns:Address">
                                    <xsl:if test = "tns:GEOG_LOC_TYP_ID = &quot;1&quot;">
                                        <ns32:contactPoints>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:PostalAddress_TO</xsl:text>
                                            </xsl:attribute>
                                            <xsl:if test = "tns:EFF_DT != &quot;&quot;">
                                                <ns32:startDate>
                                                    <xsl:value-of select = "substring(tns:EFF_DT,1,10)"/>
                                                </ns32:startDate>
                                            </xsl:if>
                                            <xsl:if test = "(tns:CTRY_NM_ABBR != &quot;&quot;) or (tns:CTRY_NM != &quot;&quot;)">
                                                <ns32:referredPlaces>
                                                    <xsl:if test = "tns:CTRY_NM_ABBR != &quot;&quot;">
                                                        <ns21:abbreviation>
                                                            <xsl:value-of select = "tns:CTRY_NM_ABBR"/>
                                                       </ns21:abbreviation>
                                                    </xsl:if>
                                                    <xsl:if test = "tns:CTRY_NM != &quot;&quot;">
                                                        <ns21:name>
                                                            <xsl:value-of select = "tns:CTRY_NM"/>
                                                        </ns21:name>
                                                    </xsl:if>
                                                    <ns21:placeRootType>
                                                        <ns21:name>
                                                            <xsl:text disable-output-escaping = "no">Country</xsl:text>
                                                        </ns21:name>
                                                   </ns21:placeRootType>
                                               </ns32:referredPlaces>
                                            </xsl:if>
                                            <xsl:if test = "tns:CTY_NM != &quot;&quot;">
                                                <ns32:city>
                                                    <xsl:value-of select = "tns:CTY_NM"/>
                                               </ns32:city>
                                            </xsl:if>
                                            <xsl:if test = "tns:CTRY_CD != &quot;&quot;">
                                                <ns32:country>
                                                    <xsl:value-of select = "tns:CTRY_CD"/>
                                                </ns32:country>
                                            </xsl:if>
                                            <xsl:if test = "tns:ST_ABBR != &quot;&quot;">
                                                <ns32:region>
                                                    <xsl:value-of select = "tns:ST_ABBR"/>
                                               </ns32:region>
                                            </xsl:if>
                                            <xsl:if test = "tns:ZIP_CD != &quot;&quot;">
                                                <ns32:postalCode>
                                                    <xsl:value-of select = "tns:ZIP_CD"/>
                                               </ns32:postalCode>
                                            </xsl:if>
                                            <xsl:if test = "tns:ADDR != &quot;&quot;">
                                                <ns32:addressLines>
                                                    <xsl:value-of select = "tns:ADDR"/>
                                                </ns32:addressLines>
                                            </xsl:if>
                                            <xsl:if test = "tns:DEP_ADDR != &quot;&quot;">
                                                <ns32:addressLines>
                                                    <xsl:value-of select = "tns:DEP_ADDR"/>
                                               </ns32:addressLines>
                                            </xsl:if>
                                        </ns32:contactPoints>
                                    </xsl:if>
                                    <xsl:if test = "tns:GEOG_LOC_TYP_ID = &quot;5&quot;">
                                        <ns32:contactPoints>
                                            <xsl:attribute name = "xsi:type">
                                                <xsl:text disable-output-escaping = "no">ns32:PostalAddress_TO</xsl:text>
                                            </xsl:attribute>
                                            <xsl:if test = "tns:EFF_DT != &quot;&quot;">
                                                <ns1:startDate>
                                                    <xsl:value-of select = "substring(tns:EFF_DT,1,10)"/>
                                                </ns1:startDate>
                                            </xsl:if>
                                            <xsl:if test = "(tns:CTRY_NM_ABBR != &quot;&quot;) or (tns:CTRY_NM != &quot;&quot;)">
                                                <ns32:referredPlaces>
                                                    <xsl:if test = "tns:CTRY_NM_ABBR != &quot;&quot;">
                                                        <ns21:abbreviation>
                                                            <xsl:value-of select = "tns:CTRY_NM_ABBR"/>
                                                        </ns21:abbreviation>
                                                    </xsl:if>
                                                    <xsl:if test = "tns:CTRY_NM != &quot;&quot;">
                                                        <ns21:name>
                                                            <xsl:value-of select = "tns:CTRY_NM"/>
                                                       </ns21:name>
                                                    </xsl:if>
                                                    <ns21:placeRootType>
                                                        <ns21:name>
                                                            <xsl:text disable-output-escaping = "no">Country</xsl:text>
                                                      </ns21:name>
                                                    </ns21:placeRootType>
                                                </ns32:referredPlaces>
                                               
                                                <ns32:isForeign>
                                                    <xsl:text disable-output-escaping = "no">true</xsl:text>
                                                </ns32:isForeign>
                                            </xsl:if>
                                            <xsl:if test = "tns:CTY_NM != &quot;&quot;">
                                                <ns32:city>
                                                    <xsl:value-of select = "tns:CTY_NM"/>
                                                </ns32:city>
                                            </xsl:if>
                                            <xsl:if test = "tns:CTRY_CD != &quot;&quot;">
                                                <ns32:country>
                                                    <xsl:value-of select = "tns:CTRY_CD"/>
                                                </ns32:country>
                                            </xsl:if>
                                            <xsl:if test = "tns:FRGN_PRVN_NM != &quot;&quot;">
                                                <ns32:region>
                                                    <xsl:value-of select = "tns:FRGN_PRVN_NM"/>
                                                </ns32:region>
                                            </xsl:if>
                                            <xsl:if test = "tns:FRGN_POSTAL_CD != &quot;&quot;">
                                                <ns32:postalCode>
                                                    <xsl:value-of select = "tns:FRGN_POSTAL_CD"/>
                                                </ns32:postalCode>
                                            </xsl:if>
                                            <xsl:if test = "tns:ADDR != &quot;&quot;">
                                                <ns32:addressLines>
                                                    <xsl:value-of select = "tns:ADDR"/>
                                                </ns32:addressLines>
                                            </xsl:if>
                                            <xsl:if test = "tns:DEP_ADDR != &quot;&quot;">
                                                <ns32:addressLines>
                                                    <xsl:value-of select = "tns:DEP_ADDR"/>
                                                </ns32:addressLines>
                                            </xsl:if>
                                        </ns32:contactPoints>
                                    </xsl:if>
                                </xsl:for-each>
                            </ns32:contactPreferences>
                            <xsl:if test = "tns:SMSN_NM != &quot;&quot;">
                                <ns32:defaultName>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:UnstructuredName_TO</xsl:text>
                                    </xsl:attribute>
                                    <ns32:fullName>
                                        <xsl:value-of select = "tns:SMSN_NM"/>
                                    </ns32:fullName>
                                </ns32:defaultName>
                            </xsl:if>
                            <xsl:if test = "tns:ENPRS_ID != &quot;&quot;">
                                <ns32:externalReference>
                                    <xsl:value-of select = "tns:ENPRS_ID"/>
                                </ns32:externalReference>
                            </xsl:if>
                            <ns32:partyRegistrations>
                                <xsl:attribute name = "xsi:type">
                                    <xsl:text disable-output-escaping = "no">ns32:CompanyRegistration_TO</xsl:text>
                                </xsl:attribute>
                                <xsl:if test = "tns:SITE_DUNS_NBR != &quot;&quot;">
                                    <ns21:externalReference>
                                        <xsl:value-of select = "tns:SITE_DUNS_NBR"/>
                                    </ns21:externalReference>
                                </xsl:if>
                                <ns21:alternateReference>
                                    <ns1:type>
                                        <xsl:text disable-output-escaping = "no">D&amp;B Number</xsl:text>
                                    </ns1:type>
                                    <xsl:if test = "&quot;&quot; != tns:DUNS_NBR">
                                        <ns1:identifier>
                                            <xsl:value-of select = "tns:DUNS_NBR"/>
                                        </ns1:identifier>
                                    </xsl:if>
                                </ns21:alternateReference>
                                <ns32:partyRegistrationRootType>
                                    <ns32:name>
                                        <xsl:text disable-output-escaping = "no">Dun And Bradstreet</xsl:text>
                                    </ns32:name>
                                </ns32:partyRegistrationRootType>
                            </ns32:partyRegistrations>
                            <xsl:if test = "tns:FED_TAX_ID != &quot;&quot;">
                                <ns32:partyRegistrations>
                                    <xsl:attribute name = "xsi:type">
                                        <xsl:text disable-output-escaping = "no">ns32:CompanyRegistration_TO</xsl:text>
                                    </xsl:attribute>
                                    <ns21:externalReference>
                                        <xsl:value-of select = "tns:FED_TAX_ID"/>
                                    </ns21:externalReference>
                                    <ns32:partyRegistrationRootType>
                                        <ns32:name>
                                            <xsl:text disable-output-escaping = "no">Tax Registration</xsl:text>
                                        </ns32:name>
                                    </ns32:partyRegistrationRootType>
                                </ns32:partyRegistrations>
                            </xsl:if>-->
                            <xsl:if test = "tns:PTY_ID != &quot;&quot;">
                                <ns32:alternateReference>
                                    <ns1:type>
                                        <xsl:text disable-output-escaping = "no">Party ID</xsl:text>
                                    </ns1:type>
                                    <ns1:identifier>
                                        <xsl:value-of select = "tns:PTY_ID"/>
                                    </ns1:identifier>
                                </ns32:alternateReference>
                            </xsl:if>
                            <!--<xsl:if test = "tns:PTY_TYP_ID != &quot;&quot;">
                                <ns32:alternateReference>
                                    <ns1:type>
                                        <xsl:text disable-output-escaping = "no">Party Type ID</xsl:text>
                                    </ns1:type>
                                    <ns1:identifier>
                                        <xsl:value-of select = "tns:PTY_TYP_ID"/>
                                    </ns1:identifier>
                                </ns32:alternateReference>
                            </xsl:if>-->
                            
                            
                            
                            
                            <!-- Date updates -->
                            <xsl:if test = 'tns:PTY_ROL_SIC_ENT_TISTMP != ""'>
                                <ns32:industries>
                                     <ns1:systemCreationDateTime>
                                            <xsl:value-of select = "tns:PTY_ROL_SIC_ENT_TISTMP"/>
                                        </ns1:systemCreationDateTime>
                                 
                                    <ns32:codeIssuer>
                                        <xsl:text disable-output-escaping = "no">Sic</xsl:text>
                                    </ns32:codeIssuer>
                                </ns32:industries>
                            </xsl:if>
                            
                            <!-- 
                                <xsl:if test = "tns:SIC_NM != &quot;&quot; or tns:SIC_CD != &quot;&quot;">
                                <ns32:industries>
                                    <xsl:if test = "tns:SIC_NM != &quot;&quot;">
                                        <ns21:name>
                                            <xsl:value-of select = "tns:SIC_NM"/>
                                        </ns21:name>
                                    </xsl:if>
                                    <ns21:priorityLevel>
                                        <xsl:text disable-output-escaping = "no">Primary</xsl:text>
                                    </ns21:priorityLevel>
                                    <xsl:if test = "tns:SIC_CD != &quot;&quot;">
                                        <ns32:industryCode>
                                            <xsl:value-of select = "tns:SIC_CD"/>
                                        </ns32:industryCode>
                                    </xsl:if>
                                    <ns32:codeIssuer>
                                        <xsl:text disable-output-escaping = "no">Sic</xsl:text>
                                    </ns32:codeIssuer>
                                </ns32:industries>
                            </xsl:if>
                            <xsl:if test = "tns:SEC_SIC_NM != &quot;&quot; or tns:SEC_SIC_CD != &quot;&quot;">
                                <ns32:industries>
                                    <xsl:if test = "tns:SEC_SIC_NM != &quot;&quot;">
                                        <ns21:name>
                                            <xsl:value-of select = "tns:SEC_SIC_NM"/>
                                        </ns21:name>
                                    </xsl:if>
                                    <ns21:priorityLevel>
                                        <xsl:text disable-output-escaping = "no">Secondary</xsl:text>
                                    </ns21:priorityLevel>
                                    <xsl:if test = "tns:SEC_SIC_CD != &quot;&quot;">
                                        <ns32:industryCode>
                                            <xsl:value-of select = "tns:SEC_SIC_CD"/>
                                        </ns32:industryCode>
                                    </xsl:if>
                                    <ns32:codeIssuer>
                                        <xsl:text disable-output-escaping = "no">Sic</xsl:text>
                                    </ns32:codeIssuer>
                                </ns32:industries>
                            </xsl:if>-->
                        </ns32:rolePlayer>
                    </xsl:for-each>
                   <!-- <xsl:if test = "/tns:RecordSubmissionResponse/tns:RecordSubmissionResult/tns:submissionName/tns:EFF_DT != &quot;&quot;">
                        <ns32:status>
                            <ns1:startDate>
                                <xsl:value-of select = "substring(/tns:RecordSubmissionResponse/tns:RecordSubmissionResult/tns:submissionName/tns:EFF_DT,1,10)"/>
                            </ns1:startDate>
                            <ns32:state>
                                <xsl:text disable-output-escaping = "no">Initial</xsl:text>
                            </ns32:state>
                        </ns32:status>
                    </xsl:if>-->
                </xsd_1:customer>
            </xsl:if>
        </ns0:createSubmissionResponse>
    </xsl:template>
</xsl:stylesheet>