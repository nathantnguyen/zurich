<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<!-- implementing array :: -->
<xsl:template match="createSubmissionResponse|submissions">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|submissions/includedContractRequests">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|submissions/rolesInContractRequest/rolePlayer/rolePlayer/rolesInRolePlayer/activityOccurrences">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|submissions/includedContractRequests/status">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|questionnaire/standardTextSpecifications">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|questionnaire/standardTextSpecifications/standardTextSpecification/possibleAnswers">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|submissions/includedContractRequests/includedContractRequests">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|submissions/includedContractRequests/includedContractRequests/includedContractRequests">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|submissions/includedContractRequests/includedContractRequests/includedContractRequests/rolesInContractRequest">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|submissions/includedContractRequests/includedContractRequests/includedContractRequests/targetProduct/objectCategories">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="createSubmissionResponse|responseHeader/transactionNotification/notification">
<xsl:choose>
<xsl:when test="name(preceding-sibling::*[1]) != name(current()) and name(following-sibling::*[1]) != name(current())">
  <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
 <xsl:element name="{name(current())}"/>
</xsl:when>
<xsl:otherwise>
 <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>


 <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>



</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="1_Adapter_Response.xml" htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml=""
		          commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="schemaCache" value="||"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="true"/>
			<advancedProp name="bGenerateByteCode" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="xsltVersion" value="2.0"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="fatal"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->