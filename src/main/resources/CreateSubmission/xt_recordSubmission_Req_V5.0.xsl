<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:tns="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/"
	xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ora="http://schemas.oracle.com/xpath/extension"
	xmlns:ns20="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/"
	xmlns:ns19="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/"
	xmlns:ns18="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/"
	xmlns:ns17="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/DisputeResolution/"
	xmlns:ns16="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/"
	xmlns:ns15="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/"
	xmlns:ns14="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/"
	xmlns:ns13="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/"
	xmlns:ns12="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/"
	xmlns:ns11="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/"
	xmlns:ns10="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/"
	xmlns:ns9="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/"
	xmlns:ns8="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/"
	xmlns:ns7="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/"
	xmlns:ns6="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/"
	xmlns:ns5="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/"
	xmlns:ns4="http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/"
	xmlns:ns3="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/"
	xmlns:ns2="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/"
	xmlns:def="http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns35="http://workstation.znawebservices.zurichna.com"
	xmlns:xn="http://xml.apache.org/xalan" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<xsl:key name="Products-by-GroupName"
		match="tns:createSubmission/ns3:submission/ns9:includedContractRequests"
		use="productGroupName" />
	<xsl:param name="paceElegibilityAndDecisionStatus" />
	<xsl:template match="/">
		<xsl:variable name="stringList"
			select="xn:tokenize($paceElegibilityAndDecisionStatus, ',')" />
		<xsl:variable name="cs">
			<ClearanceStatus>
				<xsl:for-each select="$stringList">
					<xsl:variable name="prod_values" select="substring-after(.,'=')" />
					<xsl:variable name="temp" select="substring-after($prod_values,'+')" />
					<status>
						<xsl:value-of select="substring-after($temp,'+')" />
					</status>
				</xsl:for-each>
			</ClearanceStatus>
		</xsl:variable>
		<xsl:variable name="productsData">
			<ProductDetails>
				<xsl:for-each select="$stringList">
					<xsl:variable name="prod_id" select="substring-before(.,'=')" />
					<xsl:variable name="prod_values" select="substring-after(.,'=')" />
					<Product>
						<target_ref>
							<xsl:value-of select="substring-before($prod_id,'+')" />
						</target_ref>
						<ciid>
							<xsl:value-of select="substring-after($prod_id,'+')" />
						</ciid>
						<pace_ind>
							<xsl:value-of select="substring-before($prod_values,'+')" />
						</pace_ind>
						<xsl:variable name="temp"
							select="substring-after($prod_values,'+')" />
						<xsl:variable name="supplied_status" select="substring-before($temp,'+')" />
						<decision_res>
							<xsl:choose>
								<xsl:when test="$supplied_status = '3or2.2'">
									<xsl:choose>
										<xsl:when test="xn:nodeset($cs)//status/text() = 'Conflict'">
											<xsl:text>2.2</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>3</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$supplied_status" />
								</xsl:otherwise>
							</xsl:choose>
						</decision_res>
					</Product>
				</xsl:for-each>
			</ProductDetails>
		</xsl:variable>
		<ns35:RecordSubmission>
			<ns35:request>
				<ns35:requester>
					<xsl:if
						test="/tns:createSubmission/ns3:requestHeader/def:systemName !=&quot;&quot;">
						<ns35:system>
							<xsl:value-of
								select="/tns:createSubmission/ns3:requestHeader/def:systemName" />
						</ns35:system>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:requestHeader/def:userId !=&quot;&quot;">
						<ns35:userId>
							<xsl:value-of
								select="/tns:createSubmission/ns3:requestHeader/def:userId" />
						</ns35:userId>
					</xsl:if>
				</ns35:requester>
				<ns35:submissionName>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference/ns2:type = &quot;Party ID&quot;">
						<ns35:PTY_ID>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference/ns2:identifier" />
						</ns35:PTY_ID>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:status/ns6:state = &quot;Initial&quot; and string-length(/tns:createSubmission/ns3:customer/ns6:status[ns6:state='Initial']/ns2:startDate) !=0">
						<ns35:EFF_DT>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:status[ns6:state = &quot;Initial&quot;]/ns2:startDate" />
						</ns35:EFF_DT>
					</xsl:if>
					<ns35:PTY_TYP_ID>
						<xsl:text disable-output-escaping="no">3</xsl:text>
					</ns35:PTY_TYP_ID>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:defaultName/ns6:fullName != &quot;&quot;">
						<ns35:SMSN_NM>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:defaultName/ns6:fullName" />
						</ns35:SMSN_NM>
					</xsl:if>
					<xsl:if
						test="(/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:partyRegistrations/ns7:alternateReference/ns2:type = &quot;D&amp;B Number&quot;) and (/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:partyRegistrations/ns6:partyRegistrationRootType/ns6:name = &quot;Dun And Bradstreet&quot;)">
						<ns35:DUNS_NBR>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:partyRegistrations/ns7:alternateReference[ns2:type = 'D&amp;B Number']/ns2:identifier" />
						</ns35:DUNS_NBR>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:partyRegistrations/ns6:partyRegistrationRootType/ns6:name = &quot;Dun And Bradstreet&quot;">
						<ns35:SITE_DUNS_NBR>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:partyRegistrations[ns6:partyRegistrationRootType/ns6:name = 'Dun And Bradstreet']/ns7:externalReference" />
						</ns35:SITE_DUNS_NBR>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:externalReference != &quot;&quot;">
						<ns35:ENPRS_ID>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:externalReference" />
						</ns35:ENPRS_ID>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:allNames/ns6:partyNameRootType/ns6:name = &quot;Trading Name&quot;">
						<ns35:TRD_NM>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:allNames/ns6:fullName" />
						</ns35:TRD_NM>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:partyRegistrations/ns6:partyRegistrationRootType/ns6:name = &quot;Tax Registration&quot;">
						<ns35:FED_TAX_ID>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:partyRegistrations[ns6:partyRegistrationRootType/ns6:name = 'Tax Registration']/ns7:externalReference" />
						</ns35:FED_TAX_ID>
					</xsl:if>
					<xsl:if
						test="(/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:industries/ns6:codeIssuer = &quot;Sic&quot;) and (/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:industries/ns7:priorityLevel = &quot;Primary&quot;)">
						<ns35:SIC_CD>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:industries[ns7:priorityLevel = 'Primary']/ns6:industryCode" />
						</ns35:SIC_CD>
					</xsl:if>
					<xsl:if
						test="(/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:industries/ns6:codeIssuer = &quot;Sic&quot;) and (/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:industries/ns7:priorityLevel = &quot;Secondary&quot;)">
						<ns35:SEC_SIC_CD>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:industries[ns7:priorityLevel = 'Secondary']/ns6:industryCode" />
						</ns35:SEC_SIC_CD>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:contactPreferences/ns6:contactPoints[contains(@xsi:type,'ElectronicAddress_TO')]/ns6:contactPointAsString !=&quot;&quot;">
						<ns35:INTERNET_ADDR>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:contactPreferences/ns6:contactPoints[contains(@xsi:type,'ElectronicAddress_TO')]/ns6:contactPointAsString" />
						</ns35:INTERNET_ADDR>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Revenue Category&quot;">
						<ns35:CUST_REVNW>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Revenue Category']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference" />
						</ns35:CUST_REVNW>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Location TIV&quot;">
						<ns35:LOC_WITH_TIV>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Location TIV']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference" />
						</ns35:LOC_WITH_TIV>
					</xsl:if>
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Umbrella GLX Limit&quot;">
						<ns35:UMB_OCCUR_LMT>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Umbrella GLX Limit']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference" />
						</ns35:UMB_OCCUR_LMT>
					</xsl:if>
					
					<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Revenue Category2&quot;">
						<ns35:CUST_REVNW_75MILL>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Revenue Category2']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference" />
						</ns35:CUST_REVNW_75MILL>
					</xsl:if>
					
						<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Policy TIV&quot;">
						<ns35:CUST_POL_TIV>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Policy TIV']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference" />
						</ns35:CUST_POL_TIV>
					</xsl:if>
						<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Edge&quot;">
						<ns35:EDGE_FRM>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Edge']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference" />
						</ns35:EDGE_FRM>
					</xsl:if>
						<xsl:if
						test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;ZLAPS&quot;">
						<ns35:PRC_IN_ZLAPS>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'ZLAPS']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference" />
						</ns35:PRC_IN_ZLAPS>
					</xsl:if>
					<!-- removed for pace changes 10/24/2016 -->
					<!--  
					<xsl:choose>
						<xsl:when
							test="(/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Permanently Ineligible&quot;) and (/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference =&quot;N&quot;)">
							<ns35:PACE_INELGB_IND>
								<xsl:text disable-output-escaping="no">false</xsl:text>
							</ns35:PACE_INELGB_IND>
						</xsl:when>
						<xsl:when
							test="(/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Permanently Ineligible&quot;) and (/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference =&quot;Y&quot;)">
							<ns35:PACE_INELGB_IND>
								<xsl:text disable-output-escaping="no">true</xsl:text>
							</ns35:PACE_INELGB_IND>
						</xsl:when>
					</xsl:choose>
					<xsl:if
						test="(/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:isBasedOn/ns5:externalReference = &quot;Permanently Ineligible&quot;) and (/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents/ns5:selectedAnswers/ns5:reason !=&quot;&quot;)">
						<ns35:PACE_INELGB_RSN>
							<xsl:value-of
								select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Permanently Ineligible']/ns5:selectedAnswers/ns5:reason" />
						</ns35:PACE_INELGB_RSN>
					</xsl:if>
					-->
					
					
					<ns35:address>
						<xsl:for-each
							select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:contactPreferences/ns6:contactPoints[contains(@xsi:type,'PostalAddress_TO')]">
							<xsl:choose>
								<xsl:when test="(ns6:isForeign != '') and (ns6:isForeign = 'true')">
									<ns35:Address>
										<ns35:GEOG_LOC_ID>
											<xsl:text disable-output-escaping="no">0</xsl:text>
										</ns35:GEOG_LOC_ID>
										<xsl:if
											test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference/ns2:identifier !=&quot;&quot;">
											<ns35:PTY_ID>
												<xsl:value-of
													select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference/ns2:identifier" />
											</ns35:PTY_ID>
										</xsl:if>
										<xsl:if test="ns6:startDate != &quot;&quot;">
											<ns35:EFF_DT>
												<xsl:value-of select="ns6:startDate" />
											</ns35:EFF_DT>
										</xsl:if>
										<ns35:GEOG_LOC_TYP_ID>
											<xsl:text disable-output-escaping="no">5</xsl:text>
										</ns35:GEOG_LOC_TYP_ID>
										<xsl:if test="ns6:addressLines[1] != &quot;&quot;">
											<ns35:ADDR>
												<xsl:value-of select="ns6:addressLines[1]" />
											</ns35:ADDR>
										</xsl:if>
										<xsl:if test="ns6:addressLines[2] != &quot;&quot;">
											<ns35:DEP_ADDR>
												<xsl:value-of select="ns6:addressLines[2]" />
											</ns35:DEP_ADDR>
										</xsl:if>
										<xsl:if test="ns6:city != &quot;&quot;">
											<ns35:CTY_NM>
												<xsl:value-of select="ns6:city" />
											</ns35:CTY_NM>
										</xsl:if>
										<ns35:FRGN_PRVN_NM>
											<xsl:value-of select="ns6:region" />
										</ns35:FRGN_PRVN_NM>
										<ns35:FRGN_POSTAL_CD>
											<xsl:value-of select="ns6:postalCode" />
										</ns35:FRGN_POSTAL_CD>
										<xsl:if test="ns6:country != &quot;&quot;">
											<ns35:CTRY_CD>
												<xsl:value-of select="ns6:country" />
											</ns35:CTRY_CD>
										</xsl:if>
										<xsl:if
											test="ns6:referredPlaces/ns7:placeRootType/ns7:name = &quot;Country&quot;">
											<ns35:CTRY_NM>
												<xsl:value-of select="ns6:referredPlaces/ns7:name" />
											</ns35:CTRY_NM>
										</xsl:if>
										<ns35:changeState>
											<xsl:text disable-output-escaping="no">Added</xsl:text>
										</ns35:changeState>
									</ns35:Address>
								</xsl:when>
								<xsl:otherwise>
									<ns35:Address>
										<ns35:GEOG_LOC_ID>
											<xsl:text disable-output-escaping="no">0</xsl:text>
										</ns35:GEOG_LOC_ID>
										<xsl:if
											test="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference/ns2:identifier !=&quot;&quot;">
											<ns35:PTY_ID>
												<xsl:value-of
													select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference/ns2:identifier" />
											</ns35:PTY_ID>
										</xsl:if>
										<xsl:if test="ns6:startDate != &quot;&quot;">
											<ns35:EFF_DT>
												<xsl:value-of select="ns6:startDate" />
											</ns35:EFF_DT>
										</xsl:if>
										<ns35:GEOG_LOC_TYP_ID>
											<xsl:text disable-output-escaping="no">1</xsl:text>
										</ns35:GEOG_LOC_TYP_ID>
										<xsl:if test="ns6:addressLines[1] != &quot;&quot;">
											<ns35:ADDR>
												<xsl:value-of select="ns6:addressLines[1]" />
											</ns35:ADDR>
										</xsl:if>
										<xsl:if test="ns6:addressLines[2] != &quot;&quot;">
											<ns35:DEP_ADDR>
												<xsl:value-of select="ns6:addressLines[2]" />
											</ns35:DEP_ADDR>
										</xsl:if>
										<xsl:if test="ns6:city != &quot;&quot;">
											<ns35:CTY_NM>
												<xsl:value-of select="ns6:city" />
											</ns35:CTY_NM>
										</xsl:if>
										<ns35:ST_ABBR>
											<xsl:value-of select="ns6:region" />
										</ns35:ST_ABBR>
										<ns35:ZIP_CD>
											<xsl:value-of select="ns6:postalCode" />
										</ns35:ZIP_CD>
										<xsl:if test="ns6:country != &quot;&quot;">
											<ns35:CTRY_CD>
												<xsl:value-of select="ns6:country" />
											</ns35:CTRY_CD>
										</xsl:if>
										<xsl:if
											test="ns6:referredPlaces/ns7:placeRootType/ns7:name = &quot;Country&quot;">
											<ns35:CTRY_NM>
												<xsl:value-of select="ns6:referredPlaces/ns7:name" />
											</ns35:CTRY_NM>
										</xsl:if>
										<ns35:changeState>
											<xsl:text disable-output-escaping="no">Added</xsl:text>
										</ns35:changeState>
									</ns35:Address>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</ns35:address>
					<ns35:changeState>
						<xsl:text disable-output-escaping="no">Added</xsl:text>
					</ns35:changeState>
				</ns35:submissionName>
				<ns35:submission>
					<xsl:for-each
						select="tns:createSubmission/ns3:submission/ns9:includedContractRequests[count(. | key('Products-by-GroupName', productGroupName)[1]) = 1]">
						<xsl:if test="productGroupName!=&quot;&quot;">
							<ns35:SubmissionV2>
								<xsl:if
									test="/tns:createSubmission/ns3:submission/ns9:externalReference != &quot;&quot;">
									<ns35:SMSN_ID>
										<xsl:value-of
											select="/tns:createSubmission/ns3:submission/ns9:externalReference" />
									</ns35:SMSN_ID>
								</xsl:if>
								<xsl:if
									test="/tns:createSubmission/ns3:submission/ns2:systemCreationDate != &quot;&quot;">
									<ns35:RECD_DT>
										<xsl:value-of
											select="/tns:createSubmission/ns3:submission/ns2:systemCreationDate" />
									</ns35:RECD_DT>
								</xsl:if>
								<xsl:if
									test="/tns:createSubmission/ns3:submission/ns2:systemCreationById != &quot;&quot;">
									<ns35:RECD_BY_NM>
										<xsl:value-of
											select="/tns:createSubmission/ns3:submission/ns2:systemCreationById" />
									</ns35:RECD_BY_NM>
								</xsl:if>
								<xsl:if
									test="(/tns:createSubmission/ns3:submission/ns9:status/ns9:state = &quot;Initial&quot;) and (/tns:createSubmission/ns3:submission/ns9:status/ns2:startDate !=&quot;&quot;)">
									<ns35:EFF_DT>
										<xsl:value-of
											select="/tns:createSubmission/ns3:submission/ns9:status[ns9:state = &quot;Initial&quot;]/ns2:startDate" />
									</ns35:EFF_DT>
								</xsl:if>
								<xsl:if
									test="/tns:createSubmission/ns3:submission/ns2:objectCategories/ns2:name = &quot;New Renewal Classification&quot;">
									<ns35:NEW_RENL_CD>
										<xsl:value-of
											select="/tns:createSubmission/ns3:submission/ns2:objectCategories[ns2:name = &quot;New Renewal Classification&quot;]/ns2:externalReference" />
									</ns35:NEW_RENL_CD>
								</xsl:if>
								<xsl:if
									test="/tns:createSubmission/ns3:submission/ns9:alternateReference/ns2:type = &quot;Source System Abbreviation&quot;">
									<ns35:SYS_SRC_CD>
										<xsl:value-of
											select="/tns:createSubmission/ns3:submission/ns9:alternateReference[ns2:type = &quot;Source System Abbreviation&quot;]/ns2:identifier" />
									</ns35:SYS_SRC_CD>
								</xsl:if>
								<ns35:projectName>
									<xsl:if
										test="/tns:createSubmission/ns3:submission/ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:rolesInRolePlayer/ns6:roleInRolePlayerRootType/ns6:name = &quot;Activity Occurrence Performance&quot;">
										<xsl:for-each
											select="/tns:createSubmission/ns3:submission/ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:rolesInRolePlayer/ns6:activityOccurrences">
											<ns35:ProjectName>
												<xsl:if
													test="ns7:activityOccurrenceRootType/ns7:name = &quot;Construction Activity&quot;">
													<ns35:PROJ_NM>
														<xsl:value-of select="ns7:name" />
													</ns35:PROJ_NM>
												</xsl:if>
												<ns35:changeState>
													<xsl:text disable-output-escaping="no">Added</xsl:text>
												</ns35:changeState>
											</ns35:ProjectName>
										</xsl:for-each>
									</xsl:if>
								</ns35:projectName>
								<ns35:CUST_PTY_ID>
									<xsl:value-of
										select="/tns:createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference/ns2:identifier" />
								</ns35:CUST_PTY_ID>
								<ns35:changeState>
									<xsl:text disable-output-escaping="no">Added</xsl:text>
								</ns35:changeState>
								<ns35:submissionProductV2>
									<xsl:for-each
										select="key('Products-by-GroupName',current()/productGroupName)">
										<xsl:variable name="prod_id"
											select="ns11:targetProduct/ns9:externalReference" />
										<xsl:variable name="ciid_ind"
											select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriting Unit']/ns9:rolePlayer/ns6:externalReference" />
										<xsl:variable name="pace_ind"
											select="xn:nodeset($productsData)/ProductDetails/Product[target_ref=$prod_id and ciid=$ciid_ind]/pace_ind/text()" />
										<xsl:variable name="decision"
											select="xn:nodeset($productsData)/ProductDetails/Product[target_ref=$prod_id and ciid=$ciid_ind]/decision_res/text()" />
										<ns35:SubmissionProductV2>
											<xsl:if
												test="(ns9:status/ns9:state = &quot;Initial&quot;) and (ns9:status[ns9:state = &quot;Initial&quot;]/ns2:startDate !=&quot;&quot;)">
												<ns35:EFF_DT>
													<xsl:value-of
														select="ns9:status[ns9:state = 'Initial']/ns2:startDate" />
												</ns35:EFF_DT>
											</xsl:if>



											<!-- <xsl:choose> <xsl:when test="string-length(ns11:createdFinancialServicesAgreement[ns9:description= 
												'Renewal Agreement']/ns9:externalReference) != 0"> <ns35:RENL_OF_AGMT_ID> 
												<xsl:value-of select="ns11:createdFinancialServicesAgreement[ns9:description= 
												'Renewal Agreement']/ns9:externalReference" /> </ns35:RENL_OF_AGMT_ID> </xsl:when> 
												<xsl:otherwise> <ns35:RENL_OF_AGMT_ID> <xsl:value-of select="'0'" /> </ns35:RENL_OF_AGMT_ID> 
												</xsl:otherwise> </xsl:choose> -->


											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;Inclusion Exclusion Code&quot;">
												<ns35:INCL_EXCL_CD>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Inclusion Exclusion Code&quot;]/ns2:externalReference" />
												</ns35:INCL_EXCL_CD>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;OPTN ID&quot; and string-length(ns2:objectCategories[ns2:name = &quot;OPTN ID&quot;]/ns2:externalReference) != 0">
												<ns35:OPTN_ID>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;OPTN ID&quot;]/ns2:externalReference" />
												</ns35:OPTN_ID>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;Underwriting Program Code&quot;">
												<ns35:UNDG_PGM_CD>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Underwriting Program Code&quot;]/ns2:externalReference" />
												</ns35:UNDG_PGM_CD>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;Sales Prof Indicator&quot;">
												<ns35:SALES_PROF_IND>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Sales Prof Indicator&quot;]/ns2:externalReference" />
												</ns35:SALES_PROF_IND>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;Canada IBC ID&quot; and string-length(ns2:objectCategories[ns2:name = &quot;Canada IBC ID&quot;]/ns2:externalReference)!=0">
												<ns35:CNDA_IBC_ID>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Canada IBC ID&quot;]/ns2:externalReference" />
												</ns35:CNDA_IBC_ID>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;Canada Business Area ID&quot; and string-length(ns2:objectCategories[ns2:name = &quot;Canada Business Area ID&quot;]/ns2:externalReference)!=0">
												<ns35:CNDA_BUSN_AREA_ID>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Canada Business Area ID&quot;]/ns2:externalReference" />
												</ns35:CNDA_BUSN_AREA_ID>
											</xsl:if>
											<xsl:if test="ns2:objectCategories/ns2:name = &quot;ASSM FM&quot;">
												<ns35:ASSM_FM_CD>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;ASSM FM&quot;]/ns2:externalReference" />
												</ns35:ASSM_FM_CD>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;IBC Code&quot;">
												<ns35:IBC_CD>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;IBC Code&quot;]/ns2:externalReference" />
												</ns35:IBC_CD>
											</xsl:if>
											<xsl:if test="ns2:objectCategories/ns2:name = &quot;CLS&quot;">
												<ns35:CLS_DESC>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;CLS&quot;]/ns2:description" />
												</ns35:CLS_DESC>
											</xsl:if>
											<xsl:if test="ns2:objectCategories/ns2:name = &quot;ASSM FM&quot;">
												<ns35:ASSM_FM_NM>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;ASSM FM&quot;]/ns2:description" />
												</ns35:ASSM_FM_NM>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;Business Segment Code&quot;">
												<ns35:BUSN_CD>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Business Segment Code&quot;]/ns2:externalReference" />
												</ns35:BUSN_CD>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;Business Name&quot;">
												<ns35:BUSN_NM>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Business Name&quot;]/ns2:externalReference" />
												</ns35:BUSN_NM>
											</xsl:if>
											<xsl:if test="ns2:objectCategories/ns2:name = &quot;DESC0&quot;">
												<ns35:DESC0>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;DESC0&quot;]/ns2:externalReference" />
												</ns35:DESC0>
											</xsl:if>
											<xsl:if
												test="string-length(ns2:objectCategories[ns2:name = &quot;Program Type&quot;]/ns2:externalReference) != 0">
												<ns35:PGM_TYP_ID>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Program Type&quot;]/ns2:externalReference" />
												</ns35:PGM_TYP_ID>
											</xsl:if>
											<xsl:if
												test="ns2:objectCategories/ns2:name = &quot;Canada Broker Type Text&quot;">
												<ns35:CNDA_BRK_TYP_TXT>
													<xsl:value-of
														select="ns2:objectCategories[ns2:name = &quot;Canada Broker Type Text&quot;]/ns2:externalReference" />
												</ns35:CNDA_BRK_TYP_TXT>
											</xsl:if>
											<xsl:if
												test="(ns2:objectCategories/ns2:name = &quot;CLS&quot;) and (ns2:objectCategories/ns2:parentCategoryScheme/ns2:subCategories/ns2:name = &quot;Sub CLS&quot;)">
												<ns35:SUB_CLS_DESC>
													<xsl:value-of
														select="ns2:objectCategories/ns2:parentCategoryScheme/ns2:subCategories[ns2:name = &quot;Sub CLS&quot;]/ns2:description" />
												</ns35:SUB_CLS_DESC>
											</xsl:if>
											<xsl:if
												test="ns11:targetProduct/ns9:targetMarketSegments/ns2:parentCategoryScheme/ns2:name = &quot;Standard Industrial Classification&quot;">
												<ns35:SIC_CD>
													<xsl:value-of
														select="ns11:targetProduct/ns9:targetMarketSegments[ns2:parentCategoryScheme/ns2:name = 'Standard Industrial Classification']/ns2:externalReference" />
												</ns35:SIC_CD>
											</xsl:if>
											<xsl:if
												test="ns11:targetProduct/ns9:externalReference != &quot;&quot;">
												<ns35:TMPL_INS_SPEC_ID>
													<xsl:value-of select="ns11:targetProduct/ns9:externalReference" />
												</ns35:TMPL_INS_SPEC_ID>
											</xsl:if>
											<xsl:if test="ns9:requestedDate != &quot;&quot;">
												<ns35:RQST_COVG_EFF_DT>
													<xsl:value-of select="ns9:requestedDate" />
												</ns35:RQST_COVG_EFF_DT>
											</xsl:if>
											<xsl:if test="ns9:requestedExpirationDate != &quot;&quot;">
												<ns35:RQST_COVG_EXPI_DT>
													<xsl:value-of select="ns9:requestedExpirationDate" />
												</ns35:RQST_COVG_EXPI_DT>
											</xsl:if>
											<!--Change for 3-->
											 <xsl:if
												test="ns11:targetProduct/ns2:objectCategories/ns2:name = &quot;Exposure&quot; 
												and 
												ns11:targetProduct/ns2:objectCategories[ns2:name = &quot;Exposure&quot;]/ns2:externalReference != &quot;&quot;">
												<ns35:EXPO_LOC_CLS_CD>
													<xsl:value-of
														select="ns11:targetProduct/ns2:objectCategories[ns2:name = 'Exposure']/ns2:externalReference" />
												</ns35:EXPO_LOC_CLS_CD>
											</xsl:if>
											<xsl:if
												test="(ns9:rolesInContractRequest/ns9:requestMoney/ns13:moneyProvisionElements/ns13:moneyProvisionElementRootType/ns13:name = &quot;Premium&quot;) and (ns9:rolesInContractRequest/ns9:requestMoney/ns13:moneyProvisionElements/ns13:kind = &quot;Needed Premium&quot;)">
												<ns35:ND_PREM_AMT>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns9:requestMoney/ns13:moneyProvisionElements[ns13:kind = 'Needed Premium']/ns13:baseAmount/ns2:baseAmount/ns4:theAmount" />
												</ns35:ND_PREM_AMT>
											</xsl:if>
											<xsl:if
												test="(ns9:rolesInContractRequest/ns9:requestMoney/ns13:moneyProvisionElements/ns13:moneyProvisionElementRootType/ns13:name = &quot;Premium&quot;) and (ns9:rolesInContractRequest/ns9:requestMoney/ns13:moneyProvisionElements/ns13:isEstimate = &quot;true&quot;)">
												<ns35:EST_PREM_AMT>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns9:requestMoney/ns13:moneyProvisionElements[ns13:isEstimate = 'true']/ns13:baseAmount/ns2:baseAmount/ns4:theAmount" />
												</ns35:EST_PREM_AMT>
											</xsl:if>
											<ns35:EST_PREM_CRCY_CD>
											      <xsl:value-of
														select="ns9:rolesInContractRequest/ns9:requestMoney/ns13:moneyProvisionElements[ns13:isEstimate = 'true'][ns13:moneyProvisionElementRootType/ns13:name = &quot;Premium&quot;]/ns13:baseAmount/ns2:baseAmount/ns4:theCurrencyCode" />
											</ns35:EST_PREM_CRCY_CD>
											<xsl:if
												test="ns9:rolesInContractRequest/ns11:roleInFinancialServicesRequestRootType/ns11:name = &quot;Producer Intermediary Agreement&quot;">
												<ns35:PRDR_NBR>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns11:roleInFinancialServicesRequestRootType/ns11:name = &quot;Producer Intermediary Agreement&quot;]/ns11:producerAgreement/ns9:externalReference" />
												</ns35:PRDR_NBR>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriter']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier and ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriter']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier!=''">
												<ns35:UNDR_PTY_ID>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriter']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier" />
												</ns35:UNDR_PTY_ID>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriter']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId and ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriter']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId!=''">
												<ns35:UNDR_NOTES_ID>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriter']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId" />
												</ns35:UNDR_NOTES_ID>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriting Assistant']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier and ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriting Assistant']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier!=''">
												<ns35:ASST_UNDR_PTY_ID>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriting Assistant']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier" />
												</ns35:ASST_UNDR_PTY_ID>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriting Assistant']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId and ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriting Assistant']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId!=''">
												<ns35:ASST_UNDR_NOTES_ID>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriting Assistant']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId" />
												</ns35:ASST_UNDR_NOTES_ID>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Business Development Leader']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier and ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Business Development Leader']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier!=''">
												<ns35:BUSN_DVL_LDR_PTY_ID>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Business Development Leader']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier" />
												</ns35:BUSN_DVL_LDR_PTY_ID>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Business Development Leader']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId and ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Business Development Leader']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId!=''">
												<ns35:BUSN_DVL_LDR_NOTES_ID>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Business Development Leader']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId" />
												</ns35:BUSN_DVL_LDR_NOTES_ID>
											</xsl:if>
											<xsl:if
												test="(ns9:rolesInContractRequest/ns9:rolePlayer/ns6:alternateReference/ns2:type = &quot;Party ID&quot;) and (ns9:rolesInContractRequest/ns9:partyInvolvedInContractRequestRootType/ns9:name = &quot;Underwriting Unit&quot;)">
												<ns35:ORG_PTY_ID>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = &quot;Underwriting Unit&quot;]/ns9:rolePlayer/ns6:alternateReference/ns2:identifier" />
												</ns35:ORG_PTY_ID>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns9:partyInvolvedInContractRequestRootType/ns9:name = &quot;Underwriting Unit&quot;">
												<ns35:ORG_ID>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = &quot;Underwriting Unit&quot;]/ns9:rolePlayer/ns6:externalReference" />
												</ns35:ORG_ID>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:defaultName/ns6:firstName != &quot;&quot;">
												<ns35:PRDR_FST_NM>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:defaultName/ns6:firstName" />
												</ns35:PRDR_FST_NM>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:defaultName/ns6:lastName != &quot;&quot;">
												<ns35:PRDR_LST_NM>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:defaultName/ns6:lastName" />
												</ns35:PRDR_LST_NM>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:contactPreferences/ns6:contactPoints/ns6:electronicType = &quot;Internet Email&quot;">
												<ns35:PRDR_EMAIL>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:contactPreferences/ns6:contactPoints[ns6:electronicType = 'Internet Email']/ns6:contactPointAsString" />
												</ns35:PRDR_EMAIL>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:defaultName/ns6:firstName != &quot;&quot;">
												<ns35:INSD_FST_NM>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:defaultName/ns6:firstName" />
												</ns35:INSD_FST_NM>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:defaultName/ns6:lastName != &quot;&quot;">
												<ns35:INSD_LST_NM>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:defaultName/ns6:lastName" />
												</ns35:INSD_LST_NM>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:contactPreferences/ns6:contactPoints/ns6:electronicType = &quot;Internet Email&quot;">
												<ns35:INSD_EMAIL>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns6:contactPreferences/ns6:contactPoints/ns6:contactPointAsString" />
												</ns35:INSD_EMAIL>
											</xsl:if>
											<xsl:choose>
												<xsl:when
													test="ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns2:basicDataIncomplete =&quot;&quot; or ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns2:basicDataIncomplete = &quot;true&quot;">
													<ns35:PRDR_CNTA_IND>
														<xsl:text disable-output-escaping="no">N</xsl:text>
													</ns35:PRDR_CNTA_IND>
												</xsl:when>
												<xsl:when
													test="ns9:rolesInContractRequest/ns11:producerAgreement/ns12:rolesInIntermediaryAgreement/ns12:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns2:basicDataIncomplete = &quot;false&quot;">
													<ns35:PRDR_CNTA_IND>
														<xsl:text disable-output-escaping="no">D</xsl:text>
													</ns35:PRDR_CNTA_IND>
												</xsl:when>
											</xsl:choose>
											<xsl:choose>
												<xsl:when
													test="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns2:basicDataIncomplete = &quot;&quot; or ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns2:basicDataIncomplete = 'true'">
													<ns35:INSD_CNTA_IND>
														<xsl:text disable-output-escaping="no">N</xsl:text>
													</ns35:INSD_CNTA_IND>
												</xsl:when>
												<xsl:when
													test="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:contactPreferences/ns6:preferedContactPersonReference/ns2:basicDataIncomplete = &quot;false&quot;">
													<ns35:INSD_CNTA_IND>
														<xsl:text disable-output-escaping="no">D</xsl:text>
													</ns35:INSD_CNTA_IND>
												</xsl:when>
											</xsl:choose>
											<xsl:if
												test="(ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:rolesInRolePlayer/ns6:roleInRolePlayerRootType/ns6:name = &quot;Activity Occurrence Performance&quot;) and (ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:rolesInRolePlayer/ns6:activityOccurrences/ns7:activityOccurrenceRootType/ns7:name = &quot;Construction Activity&quot;)">
												<ns35:PROJ_NM>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:rolesInRolePlayer[ns6:roleInRolePlayerRootType/ns6:name = &quot;Activity Occurrence Performance&quot;]/ns6:activityOccurrences/ns7:name" />
												</ns35:PROJ_NM>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns9:partyInvolvedInContractRequestRootType/ns9:name = &quot;Prior Insurer&quot;">
												<ns35:CARR_CD>
													<xsl:value-of
														select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Prior Insurer']/ns9:rolePlayer/ns6:externalReference" />
												</ns35:CARR_CD>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:partyRegistrations/ns6:partyRegistrationRootType/ns6:name = &quot;Interstate Commerce Commission&quot;">
												<ns35:ICC_NBR>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:partyRegistrations[ns6:partyRegistrationRootType/ns6:name = 'Interstate Commerce Commission']/ns7:externalReference" />
												</ns35:ICC_NBR>
											</xsl:if>
											<xsl:if
												test="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:partyRegistrations/ns6:partyRegistrationRootType/ns6:name = &quot;United States Department of Transporation&quot;">
												<ns35:US_DOT_NBR>
													<xsl:value-of
														select="ns9:rolesInContractRequest/ns9:rolePlayer/ns6:rolePlayer/ns6:partyRegistrations[ns6:partyRegistrationRootType/ns6:name = 'United States Department of Transporation']/ns7:externalReference" />
												</ns35:US_DOT_NBR>
											</xsl:if>
											<xsl:if
												test="ns11:targetProduct/ns9:targetMarketSegments/ns2:parentCategoryScheme/ns2:name = &quot;Canada Standard Industrial Classification&quot;">
												<ns35:CNDA_SIC_CD>
													<xsl:value-of
														select="ns11:targetProduct/ns9:targetMarketSegments[ns2:parentCategoryScheme/ns2:name = 'Canada Standard Industrial Classification']/ns2:externalReference" />
												</ns35:CNDA_SIC_CD>
											</xsl:if>
											<xsl:if test="ns9:status/ns9:state = &quot;Received&quot;">
												<ns35:IN_OFC_DT>
													<xsl:value-of
														select="ns9:status[ns9:state = 'Received']/ns2:startDate" />
												</ns35:IN_OFC_DT>
											</xsl:if>
											<xsl:if test="ns2:notes/ns2:text != &quot;&quot;">
												<ns35:CMNT_TXT>
													<xsl:value-of select="ns2:notes/ns2:text" />
												</ns35:CMNT_TXT>
											</xsl:if>
											<xsl:if
												test="ns2:rolesInContext/ns2:rolePlayerReference/ns7:description = &quot;Submission Quality&quot;">
												<ns35:QLTY_SMSN>
													<xsl:value-of
														select="ns2:rolesInContext/ns2:rolePlayerReference[ns7:description = 'Submission Quality']/ns7:freeTextScore" />
												</ns35:QLTY_SMSN>
											</xsl:if>
											<xsl:if
												test="ns2:rolesInContext/ns2:rolePlayerReference/ns7:description = &quot;Zurich Share Percentage&quot;">
												<ns35:Z_SHR_PCT>
													<xsl:value-of
														select="ns2:rolesInContext/ns2:rolePlayerReference[ns7:description = 'Zurich Share Percentage']/ns7:freeTextScore" />
												</ns35:Z_SHR_PCT>
											</xsl:if>
											<ns35:CUSTM_INS_SPEC_ID>
												<xsl:text disable-output-escaping="no">0</xsl:text>
											</ns35:CUSTM_INS_SPEC_ID>
											<ns35:SMSN_ID>
												<xsl:value-of
													select="/tns:createSubmission/ns3:submission/ns9:externalReference" />
											</ns35:SMSN_ID>
											<ns35:INS_SPEC_TYP_ID>
												<xsl:text disable-output-escaping="no">2</xsl:text>
											</ns35:INS_SPEC_TYP_ID>

											<ns35:NEW_RENL_CD>
												<xsl:choose>
													<xsl:when
														test="ns2:objectCategories/ns2:name = &quot;New Renewal Classification&quot;">
														<xsl:value-of
															select="ns2:objectCategories[ns2:name = &quot;New Renewal Classification&quot;]/ns2:externalReference" />
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of
															select="/tns:createSubmission/ns3:submission/ns2:objectCategories/ns2:externalReference" />
													</xsl:otherwise>
												</xsl:choose>
											</ns35:NEW_RENL_CD>









											<xsl:choose>
												<xsl:when
													test="string-length(ns11:createdFinancialServicesAgreement[ns9:description= 'Renewal Agreement']/ns9:externalReference) != 0">
													<ns35:RENL_OF_AGMT_ID>
														<xsl:value-of
															select="ns11:createdFinancialServicesAgreement[ns9:description= 'Renewal Agreement']/ns9:externalReference" />
													</ns35:RENL_OF_AGMT_ID>
												</xsl:when>
												<xsl:otherwise>
													<ns35:RENL_OF_AGMT_ID>
														<xsl:value-of select="'0'" />
													</ns35:RENL_OF_AGMT_ID>
												</xsl:otherwise>
											</xsl:choose>

											<ns35:RENL_OF_POL_SYM>
												<xsl:value-of
													select="ns11:createdFinancialServicesAgreement[ns9:description= 'Renewal Agreement']/ns9:contractSpecification/ns9:externalReference" />
											</ns35:RENL_OF_POL_SYM>


											<ns35:changeState>
												<xsl:text disable-output-escaping="no">Added</xsl:text>
											</ns35:changeState>
											<ns35:PACE_ELGB_IND>
												<xsl:value-of select="$pace_ind" />
											</ns35:PACE_ELGB_IND>
											
												<xsl:if
												test="ns9:rolesInContractRequest[ns9:assessmentResultInvolvedInContractRequestRootType/ns9:name='PACE Eligibility Assessment']/ns9:assessmentResult/ns7:status/ns7:state='Overridden'">
												
											
												
											
											<ns35:PACE_ELGB_OVRID_RSN>
												<xsl:value-of select="ns9:rolesInContractRequest[ns9:assessmentResultInvolvedInContractRequestRootType/ns9:name='PACE Eligibility Assessment']/ns9:assessmentResult/ns7:status[ns7:state='Overridden']/ns2:reason/text()" />
											</ns35:PACE_ELGB_OVRID_RSN>
											
											</xsl:if>
											<ns35:LANG_PREF>
											   <xsl:value-of
														select="ns2:objectCategories[ns2:name= 'Language Preference Code']/ns2:externalReference" />
											</ns35:LANG_PREF>
											<ns35:submissionProductStatusV2>
											<xsl:variable name = "newRenewalClassification">
                                                    <xsl:choose>
                                                        <xsl:when test = "string-length(ns2:objectCategories[ns2:name='New Renewal Classification']/ns2:externalReference/text()) != 0">
                                                            <xsl:value-of select = "ns2:objectCategories[ns2:name='New Renewal Classification']/ns2:externalReference/text()"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of select = "/tns:createSubmission/ns3:submission/ns2:objectCategories[ns2:name='New Renewal Classification']/ns2:externalReference/text()"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:variable>
												<xsl:choose>
													<xsl:when test="$decision = '3'">
														<ns35:SubmissionProductStatusV2>
															<ns35:SCISPEC_STS_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:SCISPEC_STS_ID>
															<ns35:CUSTM_INS_SPEC_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:CUSTM_INS_SPEC_ID>
															<ns35:changeState>
																<xsl:text disable-output-escaping="no">Added</xsl:text>
															</ns35:changeState>
															<xsl:if
																test="ns9:status[not(ns9:state =&quot;Initial&quot; or ns9:state =&quot;Received&quot; or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate!=&quot;&quot;">
																<ns35:EFF_DT>
																	<xsl:value-of
																		select="ns9:status[not(ns9:state ='Initial' or ns9:state ='Received' or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate" />
																</ns35:EFF_DT>
															</xsl:if>
															<!-- <xsl:if test = "string-length(ns9:status[ns2:description 
																= 'User Action']/ns2:startDate) != 0"> <ns35:EFF_DT> <xsl:value-of select 
																= "ns9:status[ns2:description = 'User Action']/ns2:startDate"/> </ns35:EFF_DT> 
																</xsl:if> -->
															<xsl:if test = "$newRenewalClassification = 'N'">
                                                                <ns35:STS_CD>
                                                                    <xsl:text disable-output-escaping = "no">RECV</xsl:text>
                                                                </ns35:STS_CD>
                                                            </xsl:if>
                                                            <xsl:if test = "$newRenewalClassification = 'R'">
                                                                <ns35:STS_CD>
                                                                    <xsl:text disable-output-escaping = "no">SEL</xsl:text>
                                                                </ns35:STS_CD>
                                                            </xsl:if>
														</ns35:SubmissionProductStatusV2>
													</xsl:when>
													<xsl:when test="$decision = '2.1'">
														<ns35:SubmissionProductStatusV2>
															<ns35:SCISPEC_STS_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:SCISPEC_STS_ID>
															<ns35:CUSTM_INS_SPEC_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:CUSTM_INS_SPEC_ID>
															<ns35:changeState>
																<xsl:text disable-output-escaping="no">Added</xsl:text>
															</ns35:changeState>
															<xsl:if
																test="ns9:status[not(ns9:state =&quot;Initial&quot; or ns9:state =&quot;Received&quot; or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate!=&quot;&quot;">
																<ns35:EFF_DT>
																	<xsl:value-of
																		select="ns9:status[not(ns9:state ='Initial' or ns9:state ='Received' or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate" />
																</ns35:EFF_DT>
															</xsl:if>
															<!-- <xsl:if test = "string-length(ns9:status[ns2:description 
																= 'User Action']/ns2:startDate) != 0"> <ns35:EFF_DT> <xsl:value-of select 
																= "ns9:status[ns2:description = 'User Action']/ns2:startDate"/> </ns35:EFF_DT> 
																</xsl:if> -->

															<xsl:if test = "$newRenewalClassification = 'N'">
                                                                <ns35:STS_CD>
                                                                    <xsl:text disable-output-escaping = "no">RECV</xsl:text>
                                                                </ns35:STS_CD>
                                                            </xsl:if>
                                                            <xsl:if test = "$newRenewalClassification = 'R'">
                                                                <ns35:STS_CD>
                                                                    <xsl:text disable-output-escaping = "no">SEL</xsl:text>
                                                                </ns35:STS_CD>
                                                            </xsl:if>

														</ns35:SubmissionProductStatusV2>
														<ns35:SubmissionProductStatusV2>
															<ns35:SCISPEC_STS_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:SCISPEC_STS_ID>
															<ns35:CUSTM_INS_SPEC_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:CUSTM_INS_SPEC_ID>
															<ns35:changeState>
																<xsl:text disable-output-escaping="no">Added</xsl:text>
															</ns35:changeState>
															<xsl:if
																test="ns9:status[not(ns9:state =&quot;Initial&quot; or ns9:state =&quot;Received&quot; or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate!=&quot;&quot;">
																<ns35:EFF_DT>
																	<xsl:value-of
																		select="ns9:status[not(ns9:state ='Initial' or ns9:state ='Received' or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate" />
																</ns35:EFF_DT>
															</xsl:if>
															<!-- <xsl:if test = "string-length(ns9:status[ns2:description 
																= 'User Action']/ns2:startDate) != 0"> <ns35:EFF_DT> <xsl:value-of select 
																= "ns9:status[ns2:description = 'User Action']/ns2:startDate"/> </ns35:EFF_DT> 
																</xsl:if> -->

															<ns35:STS_CD>
																<xsl:text disable-output-escaping="no">DECL</xsl:text>
															</ns35:STS_CD>

															<xsl:if
																test="ns9:status[not(ns9:state =&quot;Initial&quot; or ns9:state =&quot;Received&quot; or ns2:description=&quot;Additional Status&quot;)]/ns2:reason!=&quot;&quot;">
																<ns35:RSN_CD>
																	<xsl:value-of
																		select="ns9:status[not(ns9:state =&quot;Initial&quot; or ns9:state =&quot;Received&quot; or ns2:description=&quot;Additional Status&quot;)]/ns2:reason" />
																</ns35:RSN_CD>
															</xsl:if>
														</ns35:SubmissionProductStatusV2>
													</xsl:when>
													<xsl:when test="$decision = '2.2'">
														<ns35:SubmissionProductStatusV2>
															<ns35:SCISPEC_STS_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:SCISPEC_STS_ID>
															<ns35:CUSTM_INS_SPEC_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:CUSTM_INS_SPEC_ID>
															<ns35:changeState>
																<xsl:text disable-output-escaping="no">Added</xsl:text>
															</ns35:changeState>
															<xsl:if
																test="ns9:status[not(ns9:state =&quot;Initial&quot; or ns9:state =&quot;Received&quot; or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate!=&quot;&quot;">
																<ns35:EFF_DT>
																	<xsl:value-of
																		select="ns9:status[not(ns9:state ='Initial' or ns9:state ='Received' or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate" />
																</ns35:EFF_DT>
															</xsl:if>
															<xsl:if test = "$newRenewalClassification = 'N'">
                                                                <ns35:STS_CD>
                                                                    <xsl:text disable-output-escaping = "no">RECV</xsl:text>
                                                                </ns35:STS_CD>
                                                            </xsl:if>
                                                            <xsl:if test = "$newRenewalClassification = 'R'">
                                                                <ns35:STS_CD>
                                                                    <xsl:text disable-output-escaping = "no">SEL</xsl:text>
                                                                </ns35:STS_CD>
                                                            </xsl:if>
															
														</ns35:SubmissionProductStatusV2>
														<ns35:SubmissionProductStatusV2>
															<ns35:SCISPEC_STS_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:SCISPEC_STS_ID>
															<ns35:CUSTM_INS_SPEC_ID>
																<xsl:text disable-output-escaping="no">0</xsl:text>
															</ns35:CUSTM_INS_SPEC_ID>
															<ns35:changeState>
																<xsl:text disable-output-escaping="no">Added</xsl:text>
															</ns35:changeState>
															<xsl:if
																test="ns9:status[not(ns9:state =&quot;Initial&quot; or ns9:state =&quot;Received&quot; or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate!=&quot;&quot;">
																<ns35:EFF_DT>
																	<xsl:value-of
																		select="ns9:status[not(ns9:state ='Initial' or ns9:state ='Received' or ns2:description=&quot;Additional Status&quot;)]/ns2:startDate" />
																</ns35:EFF_DT>
															</xsl:if>



															<ns35:STS_CD>
																<xsl:text disable-output-escaping="no">DECL</xsl:text>
															</ns35:STS_CD>

															<ns35:RSN_CD>
																<xsl:text disable-output-escaping="no">MULT</xsl:text>
															</ns35:RSN_CD>
														</ns35:SubmissionProductStatusV2>
													</xsl:when>
												</xsl:choose>
												<xsl:for-each
													select="ns9:status[ns2:description = 'Additional Status']">
													<ns35:SubmissionProductStatusV2>
														<ns35:SCISPEC_STS_ID>
															<xsl:text disable-output-escaping="no">0</xsl:text>
														</ns35:SCISPEC_STS_ID>
														<ns35:CUSTM_INS_SPEC_ID>
															<xsl:text disable-output-escaping="no">0</xsl:text>
														</ns35:CUSTM_INS_SPEC_ID>
														<ns35:changeState>
															<xsl:text disable-output-escaping="no">Added</xsl:text>
														</ns35:changeState>
														<xsl:if test="string-length(ns2:startDate) != 0">
															<ns35:EFF_DT>
																<xsl:value-of select="ns2:startDate" />
															</ns35:EFF_DT>
														</xsl:if>
														<xsl:if test="string-length(ns9:state) !=0">
															<ns35:STS_CD>
																<xsl:value-of select="ns9:state" />
															</ns35:STS_CD>
														</xsl:if>
														<xsl:if test="string-length(ns2:reason) !=0">
															<ns35:RSN_CD>
																<xsl:value-of select="ns2:reason" />
															</ns35:RSN_CD>
														</xsl:if>
													</ns35:SubmissionProductStatusV2>
												</xsl:for-each>
											</ns35:submissionProductStatusV2>
										</ns35:SubmissionProductV2>
									</xsl:for-each>
								</ns35:submissionProductV2>
							</ns35:SubmissionV2>
						</xsl:if>
					</xsl:for-each>
				</ns35:submission>
			</ns35:request>
		</ns35:RecordSubmission>
	</xsl:template>
</xsl:stylesheet>