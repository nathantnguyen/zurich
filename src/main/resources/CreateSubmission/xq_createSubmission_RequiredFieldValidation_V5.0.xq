xquery version "1.0" encoding "Cp1252";


declare namespace ns20 = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/";
declare namespace ns19 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/";
declare namespace ns18 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/";
declare namespace ns17 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/DisputeResolution/";
declare namespace ns16 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/";
declare namespace ns15 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/";
declare namespace ns14 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/";
declare namespace ns13 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/";
declare namespace ns12 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/";
declare namespace ns11 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/";
declare namespace ns10 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/";
declare namespace ns9 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/";
declare namespace ns8 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/";
declare namespace ns7 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/";
declare namespace  ns6 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/";
declare namespace  ns5 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/";
declare namespace  ns4 = "http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/";
declare namespace  ns3 = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/";
declare namespace		  ns2 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/";
declare namespace		  def = "http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/";
declare namespace xf = "http://tempuri.org/PolicyDataRecording_V2/Transformation/xq_createSubmission_RequiredFieldValidation/";


declare function xf:xq_createSubmission_RequiredFieldValidation($createSubmission as element(ns20:createSubmission))
    as element(*) {
        <zSOAFault>
        {
        
     
        if (fn:empty($createSubmission/ns3:customer/ns6:rolePlayer/ns6:externalReference) or fn:normalize-space($createSubmission/ns3:customer/ns6:rolePlayer/ns6:externalReference/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field EID is empty ')}</message>
               <fieldName>EID</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-216</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
        {
        if (fn:empty($createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier) or fn:normalize-space($createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field EDB Party ID is empty')}</message>
               <fieldName>EDBPartyID</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-257</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
     
        {
           if (fn:normalize-space($createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier/text()) = '0'  and
           (fn:empty($createSubmission/ns3:customer/ns6:status[ns6:state = 'Initial']/ns2:startDate) or
            fn:normalize-space($createSubmission/ns3:customer/ns6:status[ns6:state = 'Initial']/ns2:startDate/text())='')) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Account-Effective Date is empty')}</message>
               <fieldName>createSubmission_customer_status_state_Initial_startDate</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-258</errorCode>
            </invalidFieldValueException>)	
            else ('') 
         }   		   
        {
         if (fn:normalize-space($createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier/text()) = '0'  and 
         (fn:empty($createSubmission/ns3:customer/ns6:rolePlayer/ns6:contactPreferences/ns6:contactPoints[contains(@xsi:type,'PostalAddress_TO') and 
         not(fn:exists(ns6:isForeign))]/ns6:startDate) or 
         fn:normalize-space($createSubmission/ns3:customer/ns6:rolePlayer/ns6:contactPreferences/ns6:contactPoints[contains(@xsi:type,'PostalAddress_TO') and 
         not(fn:exists(ns6:isForeign))]/ns6:startDate/text())='')) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Address-Effective Date is empty')}</message>
               <fieldName>Address_Effective_Date</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-258</errorCode>
            </invalidFieldValueException>)	
             else ('') 	
          }
          {  	   
          if (fn:normalize-space($createSubmission/ns3:customer/ns6:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier/text()) = '0'  and
          (fn:empty($createSubmission/ns3:customer/ns6:rolePlayer/ns6:contactPreferences/ns6:contactPoints[contains(@xsi:type,'PostalAddress_TO') and
           not(fn:exists(ns6:isForeign))]/ns6:country) or
            fn:normalize-space($createSubmission/ns3:customer/ns6:rolePlayer/ns6:contactPreferences/ns6:contactPoints[contains(@xsi:type,'PostalAddress_TO') and
            not(fn:exists(ns6:isForeign))]/ns6:country/text())='')) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Country Code is empty')}</message>
               <fieldName>CountryCode</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-257</errorCode>
            </invalidFieldValueException>)			   
        else ('') 
          }
                
	(:	{
        if (fn:empty($createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Permanently Ineligible']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference) or fn:normalize-space($createSubmission/ns3:customer/ns6:rolePlayer/ns2:rolesInContext/ns2:rolePlayerReference/ns5:includedContents[ns5:isBasedOn/ns5:externalReference = 'Permanently Ineligible']/ns5:selectedAnswers/ns5:selectedAnswer/ns5:externalReference/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Account PACE Permanently Ineligible Indicator is empty')}</message>
               <fieldName>AccountPACEPermanentlyIneligibleIndicator</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-209</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        } :)
		{
        if (fn:empty($createSubmission/ns3:submission/ns9:externalReference) or fn:normalize-space($createSubmission/ns3:submission/ns9:externalReference/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Submission ID is empty')}</message>
               <fieldName>SubmissionID</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-225</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
		{
        if (fn:empty($createSubmission/ns3:submission/ns2:systemCreationDate) or fn:normalize-space($createSubmission/ns3:submission/ns2:systemCreationDate/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Submission-ReceivedDate is empty')}</message>
               <fieldName>createSubmission_submission_systemCreationDate</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-260</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
		{
        if (fn:empty($createSubmission/ns3:submission/ns9:status[ns9:state = 'Initial']/ns2:startDate) or fn:normalize-space($createSubmission/ns3:submission/ns9:status[ns9:state = 'Initial']/ns2:startDate/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Submission-EffectiveDate is empty')}</message>
               <fieldName>createSubmission_submission_status_state_Initial_startDate</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-258</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
        {
        
        for $submissionProduct in $createSubmission/ns3:submission/ns9:includedContractRequests
        return
        checkSubmissionProductValidation($submissionProduct)/*
        
        }
        
        
        </zSOAFault>
};

declare function checkSubmissionProductValidation($submissionProduct as element(*)) as element(*)
{ 
	<zSOAFault>
	  {
        if (fn:string-length($submissionProduct/ns2:objectCategories[ns2:name='Underwriting Program Code']/ns2:externalReference/text())=0) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Underwriting Program Code is empty ')}</message>
               <fieldName>UnderwritingProgramCode</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-318</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
        {
        if (fn:empty($submissionProduct/ns11:targetProduct/ns9:externalReference) or fn:normalize-space($submissionProduct/ns11:targetProduct/ns9:externalReference/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Product ID is empty')}</message>
               <fieldName>ProductID</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-202</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }    
        
        {
		if (fn:empty($submissionProduct/ns11:targetProduct/ns9:targetMarketSegments[ns2:parentCategoryScheme/ns2:name='Standard Industrial Classification']/ns2:externalReference) or fn:normalize-space($submissionProduct/ns11:targetProduct/ns9:targetMarketSegments[ns2:parentCategoryScheme/ns2:name='Standard Industrial Classification']/ns2:externalReference/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Product SIC Code is empty')}</message>
               <fieldName>ProductSICCode</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-203</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        
		
		
		}
        
        {
        if (fn:empty($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriting Unit']/ns9:rolePlayer/ns6:externalReference) or fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriting Unit']/ns9:rolePlayer/ns6:externalReference/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Product CIID Organization ID(Lowest) is empty ')}</message>
               <fieldName>ProductCIIDOrganizationID_Lowest_</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-204</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }  
        
        
        {
        if (fn:empty($submissionProduct/ns9:rolesInContractRequest[ns11:roleInFinancialServicesRequestRootType/ns11:name='Producer Intermediary Agreement']/ns11:producerAgreement/ns9:externalReference) or fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns11:roleInFinancialServicesRequestRootType/ns11:name='Producer Intermediary Agreement']/ns11:producerAgreement/ns9:externalReference/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Product Producer Code is empty ')}</message>
               <fieldName>ProductProducerCode</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-205</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }  
        
        
    (:    {
        if (fn:empty($submissionProduct/ns11:targetProduct/ns2:objectCategories[ns2:name='Exposure']/ns2:externalReference) or fn:normalize-space($submissionProduct/ns11:targetProduct/ns2:objectCategories[ns2:name='Exposure']/ns2:externalReference/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Product Domestic/Foreign Exposure Flag is empty or Invalid')}</message>
               <fieldName>ProductDomestic_ForeignExposureFlag</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-222</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }  :)
        
        {
        if ((fn:empty($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriter']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier) and fn:empty($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriter']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId)) or  (fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriter']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier/text())=''  and fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriter']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId/text())='')) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Underwriter Party ID and Underwriter Lotus Notes ID is empty or Invalid')}</message>
               <fieldName>UnderwriterPartyID_UnderwriterLotusNotesID</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-244</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
        
  (:      {
        if ((fn:empty($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriting Assistant']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier) and fn:empty($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriting Assistant']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId)) or  (fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriting Assistant']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier/text())=''  and fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriting Assistant']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId/text())='')) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Underwriter Assistant Party ID and Underwriter Assistant Lotus Notes ID is empty or Invalid')}</message>
               <fieldName>UnderwriterAssistantPartyID_UnderwriterAssistantLotusNotesID</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-245</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
        
       {
        if ((fn:empty($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Business Development Leader']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier) and fn:empty($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Business Development Leader']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId)) or  (fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Business Development Leader']/ns9:rolePlayer/ns6:alternateReference[ns2:type = 'Party ID']/ns2:identifier/text())=''  and fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Business Development Leader']/ns9:rolePlayer/ns6:securityRegistrations[ns2:typeName = 'LOTUS_NOTES']/ns8:userId/text())='')) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Business Development Leader Party ID and Business Development Leader Lotus Notes ID is empty or Invalid')}</message>
               <fieldName>BusinessDevelopmentLeaderPartyID_BusinessDevelopmentLeaderLotusNotesID</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-246</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }    :)
        
        
        (: PACE ELIGIBILITY REQUIRED FIELD VALIDATION OCT-26-2016 :)
        
        
        {
        if (fn:empty($submissionProduct/ns9:rolesInContractRequest[ns9:assessmentResultInvolvedInContractRequestRootType/ns9:name='PACE Eligibility Assessment']/ns9:assessmentResult/ns7:freeTextScore)  or  
        fn:normalize-space($submissionProduct/ns9:rolesInContractRequest[ns9:assessmentResultInvolvedInContractRequestRootType/ns9:name='PACE Eligibility Assessment']/ns9:assessmentResult/ns7:freeTextScore/text())='') then 
        (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field PACE Eligibility Indicator is empty')}</message>
               <fieldName>PACEEligibilityIndicator</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-411</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        } 
        
            
        {
        
        let $freeTextScore := $submissionProduct/ns9:rolesInContractRequest[ns9:assessmentResultInvolvedInContractRequestRootType/ns9:name='PACE Eligibility Assessment']/ns9:assessmentResult/ns7:freeTextScore/text()
        return
        (
        if ( $freeTextScore = '0' or  $freeTextScore = '1' or $freeTextScore = '2' or $freeTextScore = '3' or $freeTextScore = '4' ) then 
        
       ('')
        else 
 (
			<invalidFieldValueException>
               <message>{fn:normalize-space('PACE Eligibility Indicator has an invalid value')}</message>
               <fieldName>PACEEligibilityIndicator</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-410</errorCode>
            </invalidFieldValueException>)		
        )
        
        }
        
        {
        if (fn:empty($submissionProduct/ns9:status[ns9:state = 'Initial']/ns2:startDate) or fn:normalize-space($submissionProduct/ns9:status[ns9:state = 'Initial']/ns2:startDate/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field SubmissionProduct-EffectiveDate is empty ')}</message>
               <fieldName>submissionProduct_status_state_Initial_startDate</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-258</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
		{
        if (fn:empty($submissionProduct/ns9:requestedDate) or fn:normalize-space($submissionProduct/ns9:requestedDate/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Request Coverage Effective Date is empty ')}</message>
               <fieldName>RequestCoverageEffectiveDate</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-261</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
		{
        if (fn:empty($submissionProduct/ns9:requestedExpirationDate) or fn:normalize-space($submissionProduct/ns9:requestedExpirationDate/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Request Coverage Expiry Date is empty ')}</message>
               <fieldName>RequestCoverageExpiryDate</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-262</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
 	    {
        if (fn:empty($submissionProduct/ns9:status[ns9:state = 'Received']/ns2:startDate) or fn:normalize-space($submissionProduct/ns9:status[ns9:state = 'Received']/ns2:startDate/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field In Office Date is empty ')}</message>
               <fieldName>submissionProduct_status_state_Received_startDate</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-265</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
		{
        if (fn:empty($submissionProduct/ns2:rolesInContext/ns2:rolePlayerReference[ns7:description = 'Zurich Share Percentage']/ns7:freeTextScore) or fn:normalize-space($submissionProduct/ns2:rolesInContext/ns2:rolePlayerReference[ns7:description = 'Zurich Share Percentage']/ns7:freeTextScore/text())='') then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field Zurich Share Percentage is empty ')}</message>
               <fieldName>ZurichSharePercentage</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-267</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
        {
        for $status in $submissionProduct/ns9:status[ns9:state !='Initial' and ns9:state !='Received']
        return 
        if (fn:string-length($status/ns2:startDate) = 0) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Required field SubmissionProductStatus-EffectiveDate is empty ')}</message>
               <fieldName>submissionProduct_status_state_Initial_and_state_Received_startDate</fieldName>
               <fieldValue/>
               <errorCode>ZSOAFLT-258</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
        
        (: User Action Allowed value check 06/28/2016 :)
        
        {
        for $status in $submissionProduct/ns9:status[ns9:state !='Initial' and ns9:state !='Received' and not(fn:exists(ns2:description))]
        return 
        if ((fn:normalize-space($status/ns9:state) != '') and (fn:normalize-space($status/ns9:state) != 'Default') and (fn:normalize-space($status/ns9:state) != 'Accept') and (fn:normalize-space($status/ns9:state) != 'Decline') and (fn:normalize-space($status/ns9:state) != 'Override')) then (
			<invalidFieldValueException>
               <message>{fn:normalize-space('Provide a valid value for User Action')}</message>
               <fieldName>submissionProduct_status_state_Invalid_UserAction</fieldName>
               <fieldValue>{data($status/ns9:state)}</fieldValue>
               <errorCode>ZSOAFLT-340</errorCode>
            </invalidFieldValueException>)			   
        else ('')
        }
               
        </zSOAFault>
};



declare variable $createSubmission as element(ns20:createSubmission) external;

xf:xq_createSubmission_RequiredFieldValidation($createSubmission)