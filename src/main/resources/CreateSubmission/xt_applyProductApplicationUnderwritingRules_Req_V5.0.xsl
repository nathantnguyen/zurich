<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ex3="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/"
	xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:ex2="http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/"
	xmlns:b="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/"
	xmlns:ns11="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/"
	xmlns:ns9="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/"
	xmlns:ns2="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/"
	xmlns:ns4="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/"
	xmlns:ns6="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/"
	xmlns:ns0="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/UnderwritingManagement/ServiceParameters/ICaseProcessing/"
	xmlns:ns3="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/">
	<xsl:template match="/">
		<ex3:applyProductApplicationUnderwritingRules
			xmlns:ex3="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/UnderwritingManagement/"
			xmlns:ns2="http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/">
			<ns0:requestHeader>
				<ex2:userId>
					<xsl:value-of select="//b:createSubmission/ns3:requestHeader/ex2:userId" />
				</ex2:userId>
				<ex2:systemName>
					<xsl:value-of
						select="//b:createSubmission/ns3:requestHeader/ex2:systemName" />
				</ex2:systemName>
				<ex2:messageReference>
					<xsl:value-of
						select="//b:createSubmission/ns3:requestHeader/ex2:messageReference" />
				</ex2:messageReference>
			</ns0:requestHeader>
			<xsl:apply-templates
				select="//b:createSubmission/ns3:submission/ns9:includedContractRequests" />
		</ex3:applyProductApplicationUnderwritingRules>
	</xsl:template>
	<xsl:template match="ns9:includedContractRequests">
		<ns0:requestedProducts xsi:type="ns5:NewBusinessRequest_TO"
			xmlns:ns5="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<ns9:rolesInContractRequest>
				<ns4:rolePlayerReference xsi:type="ns7:Organisation_TO"
					xmlns:ns7="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/">
					<ns7:externalReference>
						<xsl:value-of
							select="ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name = 'Underwriting Unit']/ns9:rolePlayer/ns6:externalReference" />
					</ns7:externalReference>
				</ns4:rolePlayerReference>
			</ns9:rolesInContractRequest>
			<ns5:targetProduct>
			<!-- 	<ns4:objectCategories>
					<ns4:externalReference>
						<xsl:value-of
							select="ns11:targetProduct/ns2:objectCategories[ns2:parentCategoryScheme/ns2:name = 'Exposure']/ns2:externalReference" />
					</ns4:externalReference>
					<ns4:parentCategoryScheme>
						<ns4:name>Exposure</ns4:name>
					</ns4:parentCategoryScheme>
				</ns4:objectCategories> -->
				<ns9:externalReference>
					<xsl:value-of select="ns11:targetProduct/ns9:externalReference" />
				</ns9:externalReference>
			</ns5:targetProduct>
		</ns0:requestedProducts>
	</xsl:template>
</xsl:stylesheet>
<!--Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. 
	All rights reserved. <metaInformation> <scenarios> <scenario default="yes" 
	name="Scenario1" userelativepaths="yes" externalpreview="no" url="CreateSubmission_GroupingRequest.xml" 
	htmlbaseurl="" outputurl="" processortype="saxon8" useresolver="yes" profilemode="0" 
	profiledepth="" profilelength="" urlprofilexml="" commandline="" additionalpath="" 
	additionalclasspath="" postprocessortype="none" postprocesscommandline="" 
	postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" 
	validator="internal" customvalidator=""> <advancedProp name="sInitialMode" 
	value=""/> <advancedProp name="schemaCache" value="||"/> <advancedProp name="bXsltOneIsOkay" 
	value="true"/> <advancedProp name="bSchemaAware" value="true"/> <advancedProp 
	name="bGenerateByteCode" value="true"/> <advancedProp name="bXml11" value="false"/> 
	<advancedProp name="iValidation" value="0"/> <advancedProp name="bExtensions" 
	value="true"/> <advancedProp name="iWhitespace" value="0"/> <advancedProp 
	name="sInitialTemplate" value=""/> <advancedProp name="bTinyTree" value="true"/> 
	<advancedProp name="xsltVersion" value="2.0"/> <advancedProp name="bWarnings" 
	value="true"/> <advancedProp name="bUseDTD" value="false"/> <advancedProp 
	name="iErrorHandling" value="fatal"/> </scenario> </scenarios> <MapperMetaTag> 
	<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="grouping_request.xml" 
	destSchemaRoot="S:Envelope" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"> 
	<SourceSchema srcSchemaPath="DRAFT-createSubmissionRequest.xml" srcSchemaRoot="S:Envelope" 
	AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/> 
	</MapperInfo> <MapperBlockPosition> <template match="/"> <block path="S:Envelope/S:Body/ex3:applyProductApplicationUnderwritingRules/ns3:requestHeader/ex2:userId/xsl:value-of" 
	x="332" y="144"/> <block path="S:Envelope/S:Body/ex3:applyProductApplicationUnderwritingRules/ns3:requestHeader/ex2:systemName/xsl:value-of" 
	x="372" y="162"/> <block path="S:Envelope/S:Body/ex3:applyProductApplicationUnderwritingRules/ns3:requestHeader/ex2:messageReference/xsl:value-of" 
	x="372" y="94"/> <block path="S:Envelope/S:Body/ex3:applyProductApplicationUnderwritingRules/xsl:apply-templates" 
	x="332" y="72"/> </template> <template match="includedContractRequests"></template> 
	</MapperBlockPosition> <TemplateContext></TemplateContext> <MapperFilter 
	side="source"></MapperFilter> </MapperMetaTag> </metaInformation> -->