<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">

	<xsl:template match="/">
		<recordString>
			<xsl:for-each select="/Products/Product">
				<xsl:variable name="key" select="concat(ProductID,'+',productCIID)" />
				<xsl:variable name="value"
					select="concat(paceIndicator,'+',status,'+',clearanceResult)" />

				<xsl:if test="position()!=last()">
					<xsl:value-of select="concat($key,'=',$value,',')" />
				</xsl:if>
				<xsl:if test="position()=last()">
					<xsl:value-of select="concat($key,'=',$value)" />
				</xsl:if>

			</xsl:for-each>
		</recordString>

	</xsl:template>


</xsl:stylesheet>