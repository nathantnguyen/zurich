<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:b="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/UnderwritingManagement/"
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:ns3="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/"
	xmlns:ns2="http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/"
	xmlns:ns20="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/"
	xmlns:def="http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/">
	>
	<xsl:param name="messageReference" />
	<xsl:template match="/">

		<ns3:responseHeader>
			<def:messageReference>
				<xsl:value-of select="$messageReference" />
			</def:messageReference>
			<def:transactionNotification>
				<xsl:for-each select="/Products/Product[status=0]">
					<ns2:notification>
						<ns2:notificationCategory>Error</ns2:notificationCategory>
						<ns2:code>ZSOATNO000008</ns2:code>
						<ns2:message>
							<ns2:languageCode>En</ns2:languageCode>
							<ns2:message>Automated Clearance Process not completed. Provide
								Clearance Flag.
							</ns2:message>
						</ns2:message>
						<ns2:externalReference>
							<xsl:value-of select="concat('ProductId:',ProductID,' CIID:',productCIID,' ClearanceResult:',clearanceResult)" />
						</ns2:externalReference>
						<ns2:context>IPolicyDataRecording.createSubmission</ns2:context>
					</ns2:notification>
				</xsl:for-each>
				<ns2:transactionStatus>Error</ns2:transactionStatus>
				<ns2:transactionCode></ns2:transactionCode>
				<ns2:transactionName>IPolicyDataRecording.createSubmission
				</ns2:transactionName>
			</def:transactionNotification>
		</ns3:responseHeader>


	</xsl:template>


</xsl:stylesheet>