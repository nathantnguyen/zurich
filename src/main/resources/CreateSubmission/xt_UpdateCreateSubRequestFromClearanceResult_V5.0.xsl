<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:nsp="http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/"
	exclude-result-prefixes="xsl"
	  xmlns:ns20 = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/"
            xmlns:ns11 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/"
            xmlns:ns10 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/"
            xmlns:ns9 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/"
            xmlns:ns6 = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/"
              xmlns:ns3 = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/">
	<xsl:output omit-xml-declaration="yes" indent="yes" />
	<xsl:strip-space elements="*" />



	<xsl:param name="productId" />
	<xsl:param name="CIID" />


	<xsl:template match="node()|@*" name="identity">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
		</xsl:copy>
	</xsl:template>

	<xsl:template
		match="//ns3:submission/ns9:includedContractRequests[ns11:targetProduct/ns9:externalReference=$productId and ns9:rolesInContractRequest[ns9:partyInvolvedInContractRequestRootType/ns9:name='Underwriting Unit']/ns9:rolePlayer/ns6:externalReference=$CIID]">

	</xsl:template>
</xsl:stylesheet>