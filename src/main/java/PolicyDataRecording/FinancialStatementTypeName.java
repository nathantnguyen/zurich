
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialStatementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FinancialStatementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Bill"/>
 *     &lt;enumeration value="Invoice"/>
 *     &lt;enumeration value="Business Financial Statement"/>
 *     &lt;enumeration value="Balance Sheet"/>
 *     &lt;enumeration value="Income Statement"/>
 *     &lt;enumeration value="Solvency Statement"/>
 *     &lt;enumeration value="Regulatory Solvency Statement"/>
 *     &lt;enumeration value="Statement Of Retained Earnings"/>
 *     &lt;enumeration value="Statement Of Cash Flows"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FinancialStatementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum FinancialStatementTypeName {

    @XmlEnumValue("Bill")
    BILL("Bill"),
    @XmlEnumValue("Invoice")
    INVOICE("Invoice"),
    @XmlEnumValue("Business Financial Statement")
    BUSINESS_FINANCIAL_STATEMENT("Business Financial Statement"),
    @XmlEnumValue("Balance Sheet")
    BALANCE_SHEET("Balance Sheet"),
    @XmlEnumValue("Income Statement")
    INCOME_STATEMENT("Income Statement"),
    @XmlEnumValue("Solvency Statement")
    SOLVENCY_STATEMENT("Solvency Statement"),
    @XmlEnumValue("Regulatory Solvency Statement")
    REGULATORY_SOLVENCY_STATEMENT("Regulatory Solvency Statement"),
    @XmlEnumValue("Statement Of Retained Earnings")
    STATEMENT_OF_RETAINED_EARNINGS("Statement Of Retained Earnings"),
    @XmlEnumValue("Statement Of Cash Flows")
    STATEMENT_OF_CASH_FLOWS("Statement Of Cash Flows");
    private final String value;

    FinancialStatementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FinancialStatementTypeName fromValue(String v) {
        for (FinancialStatementTypeName c: FinancialStatementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
