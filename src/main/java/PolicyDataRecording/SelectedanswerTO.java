
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for selectedanswer_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="selectedanswer_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="reason" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="selectedAnswer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}PredefinedAnswer_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "selectedanswer_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "reason",
    "selectedAnswer"
})
public class SelectedanswerTO
    extends BaseTransferObject
{

    protected String reason;
    protected PredefinedAnswerTO selectedAnswer;

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the selectedAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link PredefinedAnswerTO }
     *     
     */
    public PredefinedAnswerTO getSelectedAnswer() {
        return selectedAnswer;
    }

    /**
     * Sets the value of the selectedAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link PredefinedAnswerTO }
     *     
     */
    public void setSelectedAnswer(PredefinedAnswerTO value) {
        this.selectedAnswer = value;
    }

}
