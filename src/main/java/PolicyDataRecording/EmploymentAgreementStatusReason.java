
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmploymentAgreementStatusReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmploymentAgreementStatusReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Commenced"/>
 *     &lt;enumeration value="Leave Of Absence"/>
 *     &lt;enumeration value="Misconduct"/>
 *     &lt;enumeration value="Resignation"/>
 *     &lt;enumeration value="Retirement"/>
 *     &lt;enumeration value="Temporary Assignment"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EmploymentAgreementStatusReason", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum EmploymentAgreementStatusReason {

    @XmlEnumValue("Commenced")
    COMMENCED("Commenced"),
    @XmlEnumValue("Leave Of Absence")
    LEAVE_OF_ABSENCE("Leave Of Absence"),
    @XmlEnumValue("Misconduct")
    MISCONDUCT("Misconduct"),
    @XmlEnumValue("Resignation")
    RESIGNATION("Resignation"),
    @XmlEnumValue("Retirement")
    RETIREMENT("Retirement"),
    @XmlEnumValue("Temporary Assignment")
    TEMPORARY_ASSIGNMENT("Temporary Assignment");
    private final String value;

    EmploymentAgreementStatusReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmploymentAgreementStatusReason fromValue(String v) {
        for (EmploymentAgreementStatusReason c: EmploymentAgreementStatusReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
