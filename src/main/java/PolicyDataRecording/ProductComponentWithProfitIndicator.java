
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductComponentWithProfitIndicator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProductComponentWithProfitIndicator">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="With Profit"/>
 *     &lt;enumeration value="Without Profit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProductComponentWithProfitIndicator", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum ProductComponentWithProfitIndicator {

    @XmlEnumValue("With Profit")
    WITH_PROFIT("With Profit"),
    @XmlEnumValue("Without Profit")
    WITHOUT_PROFIT("Without Profit");
    private final String value;

    ProductComponentWithProfitIndicator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProductComponentWithProfitIndicator fromValue(String v) {
        for (ProductComponentWithProfitIndicator c: ProductComponentWithProfitIndicator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
