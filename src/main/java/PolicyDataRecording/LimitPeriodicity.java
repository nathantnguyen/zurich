
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LimitPeriodicity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LimitPeriodicity">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Life Time"/>
 *     &lt;enumeration value="Per Month"/>
 *     &lt;enumeration value="Per Single Payment"/>
 *     &lt;enumeration value="Per Year"/>
 *     &lt;enumeration value="First Loss Limited"/>
 *     &lt;enumeration value="Single Loss"/>
 *     &lt;enumeration value="Single Loss Per Location"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LimitPeriodicity", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum LimitPeriodicity {

    @XmlEnumValue("Life Time")
    LIFE_TIME("Life Time"),
    @XmlEnumValue("Per Month")
    PER_MONTH("Per Month"),
    @XmlEnumValue("Per Single Payment")
    PER_SINGLE_PAYMENT("Per Single Payment"),
    @XmlEnumValue("Per Year")
    PER_YEAR("Per Year"),
    @XmlEnumValue("First Loss Limited")
    FIRST_LOSS_LIMITED("First Loss Limited"),
    @XmlEnumValue("Single Loss")
    SINGLE_LOSS("Single Loss"),
    @XmlEnumValue("Single Loss Per Location")
    SINGLE_LOSS_PER_LOCATION("Single Loss Per Location");
    private final String value;

    LimitPeriodicity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LimitPeriodicity fromValue(String v) {
        for (LimitPeriodicity c: LimitPeriodicity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
