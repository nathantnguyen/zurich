
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskAssessmentScenario_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskAssessmentScenario_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="circumstancesDescription" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="upwardDirection" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskAssessmentScenario_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "circumstancesDescription",
    "upwardDirection"
})
@XmlSeeAlso({
    RiskAssessmentScenarioElementTO.class
})
public class RiskAssessmentScenarioTO
    extends RiskAgreementTO
{

    protected String circumstancesDescription;
    protected Boolean upwardDirection;

    /**
     * Gets the value of the circumstancesDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCircumstancesDescription() {
        return circumstancesDescription;
    }

    /**
     * Sets the value of the circumstancesDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCircumstancesDescription(String value) {
        this.circumstancesDescription = value;
    }

    /**
     * Gets the value of the upwardDirection property.
     * This getter has been renamed from isUpwardDirection() to getUpwardDirection() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUpwardDirection() {
        return upwardDirection;
    }

    /**
     * Sets the value of the upwardDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUpwardDirection(Boolean value) {
        this.upwardDirection = value;
    }

}
