
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FinancialServicesRole_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesRole_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}RoleInFinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="sharePercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="dateAccepted" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="dateRejected" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="signatureDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="agreementReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="rejectionReason" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}RejectionReason" minOccurs="0"/>
 *         &lt;element name="communicationContent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="financialServicesRoleRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRoleType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesRole_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "sharePercentage",
    "dateAccepted",
    "dateRejected",
    "signatureDate",
    "agreementReference",
    "rejectionReason",
    "communicationContent",
    "rolePlayer",
    "financialServicesRoleRootType"
})
@XmlSeeAlso({
    GuarantorTO.class,
    DerivativeWriterTO.class,
    InsurerTO.class,
    BeneficiaryTO.class,
    ServicingChannelTO.class,
    PolicyholderTO.class,
    InsuredTO.class
})
public class FinancialServicesRoleTO
    extends RoleInFinancialServicesAgreementTO
{

    protected BigDecimal sharePercentage;
    protected XMLGregorianCalendar dateAccepted;
    protected XMLGregorianCalendar dateRejected;
    protected XMLGregorianCalendar signatureDate;
    protected String agreementReference;
    protected RejectionReason rejectionReason;
    protected List<CommunicationContentTO> communicationContent;
    protected RolePlayerTO rolePlayer;
    protected FinancialServicesRoleTypeTO financialServicesRoleRootType;

    /**
     * Gets the value of the sharePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSharePercentage() {
        return sharePercentage;
    }

    /**
     * Sets the value of the sharePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSharePercentage(BigDecimal value) {
        this.sharePercentage = value;
    }

    /**
     * Gets the value of the dateAccepted property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateAccepted() {
        return dateAccepted;
    }

    /**
     * Sets the value of the dateAccepted property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateAccepted(XMLGregorianCalendar value) {
        this.dateAccepted = value;
    }

    /**
     * Gets the value of the dateRejected property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateRejected() {
        return dateRejected;
    }

    /**
     * Sets the value of the dateRejected property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateRejected(XMLGregorianCalendar value) {
        this.dateRejected = value;
    }

    /**
     * Gets the value of the signatureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSignatureDate() {
        return signatureDate;
    }

    /**
     * Sets the value of the signatureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSignatureDate(XMLGregorianCalendar value) {
        this.signatureDate = value;
    }

    /**
     * Gets the value of the agreementReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgreementReference() {
        return agreementReference;
    }

    /**
     * Sets the value of the agreementReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgreementReference(String value) {
        this.agreementReference = value;
    }

    /**
     * Gets the value of the rejectionReason property.
     * 
     * @return
     *     possible object is
     *     {@link RejectionReason }
     *     
     */
    public RejectionReason getRejectionReason() {
        return rejectionReason;
    }

    /**
     * Sets the value of the rejectionReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link RejectionReason }
     *     
     */
    public void setRejectionReason(RejectionReason value) {
        this.rejectionReason = value;
    }

    /**
     * Gets the value of the communicationContent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the communicationContent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommunicationContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationContentTO }
     * 
     * 
     */
    public List<CommunicationContentTO> getCommunicationContent() {
        if (communicationContent == null) {
            communicationContent = new ArrayList<CommunicationContentTO>();
        }
        return this.communicationContent;
    }

    /**
     * Gets the value of the rolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getRolePlayer() {
        return rolePlayer;
    }

    /**
     * Sets the value of the rolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setRolePlayer(RolePlayerTO value) {
        this.rolePlayer = value;
    }

    /**
     * Gets the value of the financialServicesRoleRootType property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesRoleTypeTO }
     *     
     */
    public FinancialServicesRoleTypeTO getFinancialServicesRoleRootType() {
        return financialServicesRoleRootType;
    }

    /**
     * Sets the value of the financialServicesRoleRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesRoleTypeTO }
     *     
     */
    public void setFinancialServicesRoleRootType(FinancialServicesRoleTypeTO value) {
        this.financialServicesRoleRootType = value;
    }

    /**
     * Sets the value of the communicationContent property.
     * 
     * @param communicationContent
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setCommunicationContent(List<CommunicationContentTO> communicationContent) {
        this.communicationContent = communicationContent;
    }

}
