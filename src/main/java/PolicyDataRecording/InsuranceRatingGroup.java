
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InsuranceRatingGroup.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InsuranceRatingGroup">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Group 4"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InsuranceRatingGroup", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum InsuranceRatingGroup {

    @XmlEnumValue("Group 4")
    GROUP_4("Group 4");
    private final String value;

    InsuranceRatingGroup(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InsuranceRatingGroup fromValue(String v) {
        for (InsuranceRatingGroup c: InsuranceRatingGroup.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
