
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerOfProductGroupStatus_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerOfProductGroupStatus_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}StatusWithCommonReason_TO">
 *       &lt;sequence>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}CustomerOfProductGroupState" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerOfProductGroupStatus_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "state"
})
public class CustomerOfProductGroupStatusTO
    extends StatusWithCommonReasonTO
{

    protected CustomerOfProductGroupState state;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerOfProductGroupState }
     *     
     */
    public CustomerOfProductGroupState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerOfProductGroupState }
     *     
     */
    public void setState(CustomerOfProductGroupState value) {
        this.state = value;
    }

}
