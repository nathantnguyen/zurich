
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketableObject_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketableObject_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="percentageOwned" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="identifiedPhysicalObject" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketableObject_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "percentageOwned",
    "identifiedPhysicalObject"
})
public class MarketableObjectTO
    extends FinancialAssetTO
{

    protected BigDecimal percentageOwned;
    protected PhysicalObjectTO identifiedPhysicalObject;

    /**
     * Gets the value of the percentageOwned property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentageOwned() {
        return percentageOwned;
    }

    /**
     * Sets the value of the percentageOwned property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentageOwned(BigDecimal value) {
        this.percentageOwned = value;
    }

    /**
     * Gets the value of the identifiedPhysicalObject property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public PhysicalObjectTO getIdentifiedPhysicalObject() {
        return identifiedPhysicalObject;
    }

    /**
     * Sets the value of the identifiedPhysicalObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setIdentifiedPhysicalObject(PhysicalObjectTO value) {
        this.identifiedPhysicalObject = value;
    }

}
