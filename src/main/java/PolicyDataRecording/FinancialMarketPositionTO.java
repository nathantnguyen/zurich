
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FinancialMarketPosition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialMarketPosition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AssetHolding_TO">
 *       &lt;sequence>
 *         &lt;element name="cleanPositionFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="netPositionFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="openPositionFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="positionAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="positionCalculationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="positionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="positionQuantity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="shortPositionFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="financialAsset" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialMarketPosition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "cleanPositionFlag",
    "netPositionFlag",
    "openPositionFlag",
    "positionAmount",
    "positionCalculationDate",
    "positionDate",
    "positionQuantity",
    "shortPositionFlag",
    "financialAsset"
})
public class FinancialMarketPositionTO
    extends AssetHoldingTO
{

    protected Boolean cleanPositionFlag;
    protected Boolean netPositionFlag;
    protected Boolean openPositionFlag;
    protected BaseCurrencyAmount positionAmount;
    protected XMLGregorianCalendar positionCalculationDate;
    protected XMLGregorianCalendar positionDate;
    protected BigInteger positionQuantity;
    protected Boolean shortPositionFlag;
    protected List<FinancialAssetTO> financialAsset;

    /**
     * Gets the value of the cleanPositionFlag property.
     * This getter has been renamed from isCleanPositionFlag() to getCleanPositionFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCleanPositionFlag() {
        return cleanPositionFlag;
    }

    /**
     * Sets the value of the cleanPositionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCleanPositionFlag(Boolean value) {
        this.cleanPositionFlag = value;
    }

    /**
     * Gets the value of the netPositionFlag property.
     * This getter has been renamed from isNetPositionFlag() to getNetPositionFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNetPositionFlag() {
        return netPositionFlag;
    }

    /**
     * Sets the value of the netPositionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNetPositionFlag(Boolean value) {
        this.netPositionFlag = value;
    }

    /**
     * Gets the value of the openPositionFlag property.
     * This getter has been renamed from isOpenPositionFlag() to getOpenPositionFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOpenPositionFlag() {
        return openPositionFlag;
    }

    /**
     * Sets the value of the openPositionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenPositionFlag(Boolean value) {
        this.openPositionFlag = value;
    }

    /**
     * Gets the value of the positionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getPositionAmount() {
        return positionAmount;
    }

    /**
     * Sets the value of the positionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setPositionAmount(BaseCurrencyAmount value) {
        this.positionAmount = value;
    }

    /**
     * Gets the value of the positionCalculationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPositionCalculationDate() {
        return positionCalculationDate;
    }

    /**
     * Sets the value of the positionCalculationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPositionCalculationDate(XMLGregorianCalendar value) {
        this.positionCalculationDate = value;
    }

    /**
     * Gets the value of the positionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPositionDate() {
        return positionDate;
    }

    /**
     * Sets the value of the positionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPositionDate(XMLGregorianCalendar value) {
        this.positionDate = value;
    }

    /**
     * Gets the value of the positionQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPositionQuantity() {
        return positionQuantity;
    }

    /**
     * Sets the value of the positionQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPositionQuantity(BigInteger value) {
        this.positionQuantity = value;
    }

    /**
     * Gets the value of the shortPositionFlag property.
     * This getter has been renamed from isShortPositionFlag() to getShortPositionFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getShortPositionFlag() {
        return shortPositionFlag;
    }

    /**
     * Sets the value of the shortPositionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShortPositionFlag(Boolean value) {
        this.shortPositionFlag = value;
    }

    /**
     * Gets the value of the financialAsset property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the financialAsset property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFinancialAsset().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialAssetTO }
     * 
     * 
     */
    public List<FinancialAssetTO> getFinancialAsset() {
        if (financialAsset == null) {
            financialAsset = new ArrayList<FinancialAssetTO>();
        }
        return this.financialAsset;
    }

    /**
     * Sets the value of the financialAsset property.
     * 
     * @param financialAsset
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setFinancialAsset(List<FinancialAssetTO> financialAsset) {
        this.financialAsset = financialAsset;
    }

}
