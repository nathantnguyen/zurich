
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectCoveredRoleInFsa_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectCoveredRoleInFsa_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}RoleInFinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="percentageInsured" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="insuredQuantity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="includedInCoverage" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="preExistingConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="coveredPhysicalObjects" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="excludedFromCoverage" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectCoveredRoleInFsa_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "percentageInsured",
    "insuredQuantity",
    "includedInCoverage",
    "preExistingConditions",
    "coveredPhysicalObjects",
    "excludedFromCoverage"
})
public class ObjectCoveredRoleInFsaTO
    extends RoleInFinancialServicesAgreementTO
{

    protected BigDecimal percentageInsured;
    protected BigInteger insuredQuantity;
    protected List<CategoryTO> includedInCoverage;
    protected List<ConditionTO> preExistingConditions;
    protected List<PhysicalObjectTO> coveredPhysicalObjects;
    protected List<CategoryTO> excludedFromCoverage;

    /**
     * Gets the value of the percentageInsured property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentageInsured() {
        return percentageInsured;
    }

    /**
     * Sets the value of the percentageInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentageInsured(BigDecimal value) {
        this.percentageInsured = value;
    }

    /**
     * Gets the value of the insuredQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInsuredQuantity() {
        return insuredQuantity;
    }

    /**
     * Sets the value of the insuredQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInsuredQuantity(BigInteger value) {
        this.insuredQuantity = value;
    }

    /**
     * Gets the value of the includedInCoverage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedInCoverage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludedInCoverage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getIncludedInCoverage() {
        if (includedInCoverage == null) {
            includedInCoverage = new ArrayList<CategoryTO>();
        }
        return this.includedInCoverage;
    }

    /**
     * Gets the value of the preExistingConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the preExistingConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPreExistingConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionTO }
     * 
     * 
     */
    public List<ConditionTO> getPreExistingConditions() {
        if (preExistingConditions == null) {
            preExistingConditions = new ArrayList<ConditionTO>();
        }
        return this.preExistingConditions;
    }

    /**
     * Gets the value of the coveredPhysicalObjects property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coveredPhysicalObjects property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoveredPhysicalObjects().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhysicalObjectTO }
     * 
     * 
     */
    public List<PhysicalObjectTO> getCoveredPhysicalObjects() {
        if (coveredPhysicalObjects == null) {
            coveredPhysicalObjects = new ArrayList<PhysicalObjectTO>();
        }
        return this.coveredPhysicalObjects;
    }

    /**
     * Gets the value of the excludedFromCoverage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the excludedFromCoverage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExcludedFromCoverage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getExcludedFromCoverage() {
        if (excludedFromCoverage == null) {
            excludedFromCoverage = new ArrayList<CategoryTO>();
        }
        return this.excludedFromCoverage;
    }

    /**
     * Sets the value of the includedInCoverage property.
     * 
     * @param includedInCoverage
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setIncludedInCoverage(List<CategoryTO> includedInCoverage) {
        this.includedInCoverage = includedInCoverage;
    }

    /**
     * Sets the value of the preExistingConditions property.
     * 
     * @param preExistingConditions
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setPreExistingConditions(List<ConditionTO> preExistingConditions) {
        this.preExistingConditions = preExistingConditions;
    }

    /**
     * Sets the value of the coveredPhysicalObjects property.
     * 
     * @param coveredPhysicalObjects
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setCoveredPhysicalObjects(List<PhysicalObjectTO> coveredPhysicalObjects) {
        this.coveredPhysicalObjects = coveredPhysicalObjects;
    }

    /**
     * Sets the value of the excludedFromCoverage property.
     * 
     * @param excludedFromCoverage
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setExcludedFromCoverage(List<CategoryTO> excludedFromCoverage) {
        this.excludedFromCoverage = excludedFromCoverage;
    }

}
