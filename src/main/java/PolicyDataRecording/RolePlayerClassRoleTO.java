
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RolePlayerClassRole_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RolePlayerClassRole_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Role_TO">
 *       &lt;sequence>
 *         &lt;element name="contextReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="rolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO" minOccurs="0"/>
 *         &lt;element name="contextNavigation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RolePlayerClassRole_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "contextReference",
    "rolePlayer",
    "contextNavigation"
})
@XmlSeeAlso({
    CommunicationContentRoleTO.class,
    ContractRoleTO.class,
    ProductGroupRoleTO.class,
    PlaceRoleTO.class,
    ActivitySpecificationRoleTO.class,
    AssessmentResultRoleTO.class,
    ActivityRoleTO.class,
    EventRoleTO.class,
    PhysicalObjectRoleTO.class,
    ModelSpecificationRoleTO.class,
    LegalActionRoleTO.class,
    MoneyProvisionRoleTO.class,
    MoneySchedulerRoleTO.class,
    AccountRoleTO.class,
    RolePlayerRoleTO.class,
    GoalAndNeedRoleTO.class
})
public class RolePlayerClassRoleTO
    extends RoleTO
{

    protected ObjectReferenceTO contextReference;
    protected BusinessModelObjectTO rolePlayer;
    protected RoleInContextClassTO contextNavigation;

    /**
     * Gets the value of the contextReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getContextReference() {
        return contextReference;
    }

    /**
     * Sets the value of the contextReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setContextReference(ObjectReferenceTO value) {
        this.contextReference = value;
    }

    /**
     * Gets the value of the rolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessModelObjectTO }
     *     
     */
    public BusinessModelObjectTO getRolePlayer() {
        return rolePlayer;
    }

    /**
     * Sets the value of the rolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessModelObjectTO }
     *     
     */
    public void setRolePlayer(BusinessModelObjectTO value) {
        this.rolePlayer = value;
    }

    /**
     * Gets the value of the contextNavigation property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInContextClassTO }
     *     
     */
    public RoleInContextClassTO getContextNavigation() {
        return contextNavigation;
    }

    /**
     * Sets the value of the contextNavigation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInContextClassTO }
     *     
     */
    public void setContextNavigation(RoleInContextClassTO value) {
        this.contextNavigation = value;
    }

}
