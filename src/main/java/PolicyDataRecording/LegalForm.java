
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LegalForm.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LegalForm">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Joint Venture"/>
 *     &lt;enumeration value="Cooperative"/>
 *     &lt;enumeration value="Corporation"/>
 *     &lt;enumeration value="Partnership"/>
 *     &lt;enumeration value="Syndicate"/>
 *     &lt;enumeration value="Sole Proprietorship"/>
 *     &lt;enumeration value="Trust"/>
 *     &lt;enumeration value="Limited Company"/>
 *     &lt;enumeration value="Public Limited Company"/>
 *     &lt;enumeration value="Association"/>
 *     &lt;enumeration value="Estate"/>
 *     &lt;enumeration value="Municipality"/>
 *     &lt;enumeration value="Public Company"/>
 *     &lt;enumeration value="Private Company"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LegalForm", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum LegalForm {

    @XmlEnumValue("Joint Venture")
    JOINT_VENTURE("Joint Venture"),
    @XmlEnumValue("Cooperative")
    COOPERATIVE("Cooperative"),
    @XmlEnumValue("Corporation")
    CORPORATION("Corporation"),
    @XmlEnumValue("Partnership")
    PARTNERSHIP("Partnership"),
    @XmlEnumValue("Syndicate")
    SYNDICATE("Syndicate"),
    @XmlEnumValue("Sole Proprietorship")
    SOLE_PROPRIETORSHIP("Sole Proprietorship"),
    @XmlEnumValue("Trust")
    TRUST("Trust"),
    @XmlEnumValue("Limited Company")
    LIMITED_COMPANY("Limited Company"),
    @XmlEnumValue("Public Limited Company")
    PUBLIC_LIMITED_COMPANY("Public Limited Company"),
    @XmlEnumValue("Association")
    ASSOCIATION("Association"),
    @XmlEnumValue("Estate")
    ESTATE("Estate"),
    @XmlEnumValue("Municipality")
    MUNICIPALITY("Municipality"),
    @XmlEnumValue("Public Company")
    PUBLIC_COMPANY("Public Company"),
    @XmlEnumValue("Private Company")
    PRIVATE_COMPANY("Private Company");
    private final String value;

    LegalForm(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LegalForm fromValue(String v) {
        for (LegalForm c: LegalForm.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
