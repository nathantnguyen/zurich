
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RateInvolvedInFsa_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RateInvolvedInFsa_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}RoleInFinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="rate" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Rate_TO" minOccurs="0"/>
 *         &lt;element name="rateInvolvedInFsaRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}RateRoleInFsaType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateInvolvedInFsa_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "rate",
    "rateInvolvedInFsaRootType"
})
public class RateInvolvedInFsaTO
    extends RoleInFinancialServicesAgreementTO
{

    protected RateTO rate;
    protected RateRoleInFsaTypeTO rateInvolvedInFsaRootType;

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link RateTO }
     *     
     */
    public RateTO getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateTO }
     *     
     */
    public void setRate(RateTO value) {
        this.rate = value;
    }

    /**
     * Gets the value of the rateInvolvedInFsaRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RateRoleInFsaTypeTO }
     *     
     */
    public RateRoleInFsaTypeTO getRateInvolvedInFsaRootType() {
        return rateInvolvedInFsaRootType;
    }

    /**
     * Sets the value of the rateInvolvedInFsaRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateRoleInFsaTypeTO }
     *     
     */
    public void setRateInvolvedInFsaRootType(RateRoleInFsaTypeTO value) {
        this.rateInvolvedInFsaRootType = value;
    }

}
