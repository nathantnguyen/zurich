
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Ship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Ship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}Vehicle_TO">
 *       &lt;sequence>
 *         &lt;element name="baleCapacity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="ballastQuantity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="draft" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="manningScale" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="shipModel" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}ShipModel_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ship_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "baleCapacity",
    "ballastQuantity",
    "draft",
    "manningScale",
    "shipModel"
})
public class ShipTO
    extends VehicleTO
{

    protected Amount baleCapacity;
    protected Amount ballastQuantity;
    protected BigDecimal draft;
    protected BigInteger manningScale;
    protected ShipModelTO shipModel;

    /**
     * Gets the value of the baleCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getBaleCapacity() {
        return baleCapacity;
    }

    /**
     * Sets the value of the baleCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setBaleCapacity(Amount value) {
        this.baleCapacity = value;
    }

    /**
     * Gets the value of the ballastQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getBallastQuantity() {
        return ballastQuantity;
    }

    /**
     * Sets the value of the ballastQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setBallastQuantity(Amount value) {
        this.ballastQuantity = value;
    }

    /**
     * Gets the value of the draft property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDraft() {
        return draft;
    }

    /**
     * Sets the value of the draft property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDraft(BigDecimal value) {
        this.draft = value;
    }

    /**
     * Gets the value of the manningScale property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getManningScale() {
        return manningScale;
    }

    /**
     * Sets the value of the manningScale property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setManningScale(BigInteger value) {
        this.manningScale = value;
    }

    /**
     * Gets the value of the shipModel property.
     * 
     * @return
     *     possible object is
     *     {@link ShipModelTO }
     *     
     */
    public ShipModelTO getShipModel() {
        return shipModel;
    }

    /**
     * Sets the value of the shipModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipModelTO }
     *     
     */
    public void setShipModel(ShipModelTO value) {
        this.shipModel = value;
    }

}
