
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for IndividualLifeInsurancePolicy_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IndividualLifeInsurancePolicy_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}LifeAndHealthPolicy_TO">
 *       &lt;sequence>
 *         &lt;element name="nonForfeitureOption" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}NonForfeitureOption" minOccurs="0"/>
 *         &lt;element name="faceAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="netDeathBenefit" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="withdrawalsLastTwelveMonths" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="totalWithdrawals" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="unscheduledPremiumsSinceAnniversary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="totalUnscheduledPremiums" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="contestabilityPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="guaranteedConvertability" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="guaranteedDeathBenefits" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="guaranteedPremium" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="guaranteedRenewability" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndividualLifeInsurancePolicy_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "nonForfeitureOption",
    "faceAmount",
    "netDeathBenefit",
    "withdrawalsLastTwelveMonths",
    "totalWithdrawals",
    "unscheduledPremiumsSinceAnniversary",
    "totalUnscheduledPremiums",
    "contestabilityPeriod",
    "guaranteedConvertability",
    "guaranteedDeathBenefits",
    "guaranteedPremium",
    "guaranteedRenewability"
})
public class IndividualLifeInsurancePolicyTO
    extends LifeAndHealthPolicyTO
{

    protected NonForfeitureOption nonForfeitureOption;
    protected BaseCurrencyAmount faceAmount;
    protected BaseCurrencyAmount netDeathBenefit;
    protected BaseCurrencyAmount withdrawalsLastTwelveMonths;
    protected BaseCurrencyAmount totalWithdrawals;
    protected BaseCurrencyAmount unscheduledPremiumsSinceAnniversary;
    protected BaseCurrencyAmount totalUnscheduledPremiums;
    protected Duration contestabilityPeriod;
    protected Boolean guaranteedConvertability;
    protected Boolean guaranteedDeathBenefits;
    protected Boolean guaranteedPremium;
    protected Boolean guaranteedRenewability;

    /**
     * Gets the value of the nonForfeitureOption property.
     * 
     * @return
     *     possible object is
     *     {@link NonForfeitureOption }
     *     
     */
    public NonForfeitureOption getNonForfeitureOption() {
        return nonForfeitureOption;
    }

    /**
     * Sets the value of the nonForfeitureOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonForfeitureOption }
     *     
     */
    public void setNonForfeitureOption(NonForfeitureOption value) {
        this.nonForfeitureOption = value;
    }

    /**
     * Gets the value of the faceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getFaceAmount() {
        return faceAmount;
    }

    /**
     * Sets the value of the faceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setFaceAmount(BaseCurrencyAmount value) {
        this.faceAmount = value;
    }

    /**
     * Gets the value of the netDeathBenefit property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getNetDeathBenefit() {
        return netDeathBenefit;
    }

    /**
     * Sets the value of the netDeathBenefit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setNetDeathBenefit(BaseCurrencyAmount value) {
        this.netDeathBenefit = value;
    }

    /**
     * Gets the value of the withdrawalsLastTwelveMonths property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getWithdrawalsLastTwelveMonths() {
        return withdrawalsLastTwelveMonths;
    }

    /**
     * Sets the value of the withdrawalsLastTwelveMonths property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setWithdrawalsLastTwelveMonths(BaseCurrencyAmount value) {
        this.withdrawalsLastTwelveMonths = value;
    }

    /**
     * Gets the value of the totalWithdrawals property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTotalWithdrawals() {
        return totalWithdrawals;
    }

    /**
     * Sets the value of the totalWithdrawals property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTotalWithdrawals(BaseCurrencyAmount value) {
        this.totalWithdrawals = value;
    }

    /**
     * Gets the value of the unscheduledPremiumsSinceAnniversary property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getUnscheduledPremiumsSinceAnniversary() {
        return unscheduledPremiumsSinceAnniversary;
    }

    /**
     * Sets the value of the unscheduledPremiumsSinceAnniversary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setUnscheduledPremiumsSinceAnniversary(BaseCurrencyAmount value) {
        this.unscheduledPremiumsSinceAnniversary = value;
    }

    /**
     * Gets the value of the totalUnscheduledPremiums property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTotalUnscheduledPremiums() {
        return totalUnscheduledPremiums;
    }

    /**
     * Sets the value of the totalUnscheduledPremiums property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTotalUnscheduledPremiums(BaseCurrencyAmount value) {
        this.totalUnscheduledPremiums = value;
    }

    /**
     * Gets the value of the contestabilityPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getContestabilityPeriod() {
        return contestabilityPeriod;
    }

    /**
     * Sets the value of the contestabilityPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setContestabilityPeriod(Duration value) {
        this.contestabilityPeriod = value;
    }

    /**
     * Gets the value of the guaranteedConvertability property.
     * This getter has been renamed from isGuaranteedConvertability() to getGuaranteedConvertability() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGuaranteedConvertability() {
        return guaranteedConvertability;
    }

    /**
     * Sets the value of the guaranteedConvertability property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGuaranteedConvertability(Boolean value) {
        this.guaranteedConvertability = value;
    }

    /**
     * Gets the value of the guaranteedDeathBenefits property.
     * This getter has been renamed from isGuaranteedDeathBenefits() to getGuaranteedDeathBenefits() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGuaranteedDeathBenefits() {
        return guaranteedDeathBenefits;
    }

    /**
     * Sets the value of the guaranteedDeathBenefits property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGuaranteedDeathBenefits(Boolean value) {
        this.guaranteedDeathBenefits = value;
    }

    /**
     * Gets the value of the guaranteedPremium property.
     * This getter has been renamed from isGuaranteedPremium() to getGuaranteedPremium() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGuaranteedPremium() {
        return guaranteedPremium;
    }

    /**
     * Sets the value of the guaranteedPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGuaranteedPremium(Boolean value) {
        this.guaranteedPremium = value;
    }

    /**
     * Gets the value of the guaranteedRenewability property.
     * This getter has been renamed from isGuaranteedRenewability() to getGuaranteedRenewability() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGuaranteedRenewability() {
        return guaranteedRenewability;
    }

    /**
     * Sets the value of the guaranteedRenewability property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGuaranteedRenewability(Boolean value) {
        this.guaranteedRenewability = value;
    }

}
