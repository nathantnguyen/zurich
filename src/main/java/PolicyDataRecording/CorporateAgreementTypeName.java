
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CorporateAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Business Portfolio Policy"/>
 *     &lt;enumeration value="Market Diversification Policy"/>
 *     &lt;enumeration value="Product Diversification Policy"/>
 *     &lt;enumeration value="Financial Directive"/>
 *     &lt;enumeration value="Capital Position Policy"/>
 *     &lt;enumeration value="Asset And Liability Management Policy"/>
 *     &lt;enumeration value="Capital Acquisition Policy"/>
 *     &lt;enumeration value="Capital Investment Policy"/>
 *     &lt;enumeration value="Operational Directive"/>
 *     &lt;enumeration value="Audit Agreement"/>
 *     &lt;enumeration value="External Audit Agreement"/>
 *     &lt;enumeration value="External Control Agreement"/>
 *     &lt;enumeration value="Control Agreement"/>
 *     &lt;enumeration value="Internal Audit Agreement"/>
 *     &lt;enumeration value="Internal Control Agreement"/>
 *     &lt;enumeration value="Outsourcing Directive"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CorporateAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum CorporateAgreementTypeName {

    @XmlEnumValue("Business Portfolio Policy")
    BUSINESS_PORTFOLIO_POLICY("Business Portfolio Policy"),
    @XmlEnumValue("Market Diversification Policy")
    MARKET_DIVERSIFICATION_POLICY("Market Diversification Policy"),
    @XmlEnumValue("Product Diversification Policy")
    PRODUCT_DIVERSIFICATION_POLICY("Product Diversification Policy"),
    @XmlEnumValue("Financial Directive")
    FINANCIAL_DIRECTIVE("Financial Directive"),
    @XmlEnumValue("Capital Position Policy")
    CAPITAL_POSITION_POLICY("Capital Position Policy"),
    @XmlEnumValue("Asset And Liability Management Policy")
    ASSET_AND_LIABILITY_MANAGEMENT_POLICY("Asset And Liability Management Policy"),
    @XmlEnumValue("Capital Acquisition Policy")
    CAPITAL_ACQUISITION_POLICY("Capital Acquisition Policy"),
    @XmlEnumValue("Capital Investment Policy")
    CAPITAL_INVESTMENT_POLICY("Capital Investment Policy"),
    @XmlEnumValue("Operational Directive")
    OPERATIONAL_DIRECTIVE("Operational Directive"),
    @XmlEnumValue("Audit Agreement")
    AUDIT_AGREEMENT("Audit Agreement"),
    @XmlEnumValue("External Audit Agreement")
    EXTERNAL_AUDIT_AGREEMENT("External Audit Agreement"),
    @XmlEnumValue("External Control Agreement")
    EXTERNAL_CONTROL_AGREEMENT("External Control Agreement"),
    @XmlEnumValue("Control Agreement")
    CONTROL_AGREEMENT("Control Agreement"),
    @XmlEnumValue("Internal Audit Agreement")
    INTERNAL_AUDIT_AGREEMENT("Internal Audit Agreement"),
    @XmlEnumValue("Internal Control Agreement")
    INTERNAL_CONTROL_AGREEMENT("Internal Control Agreement"),
    @XmlEnumValue("Outsourcing Directive")
    OUTSOURCING_DIRECTIVE("Outsourcing Directive");
    private final String value;

    CorporateAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CorporateAgreementTypeName fromValue(String v) {
        for (CorporateAgreementTypeName c: CorporateAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
