
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrganisationMembership_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrganisationMembership_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerRelationship_TO">
 *       &lt;sequence>
 *         &lt;element name="membershipLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}MembershipLevel" minOccurs="0"/>
 *         &lt;element name="membershipOrganisation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO" minOccurs="0"/>
 *         &lt;element name="connectionCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationMembership_TO", propOrder = {
    "membershipLevel",
    "membershipOrganisation",
    "connectionCount"
})
public class OrganisationMembershipTO
    extends RolePlayerRelationshipTO
{

    protected MembershipLevel membershipLevel;
    protected OrganisationTO membershipOrganisation;
    protected BigInteger connectionCount;

    /**
     * Gets the value of the membershipLevel property.
     * 
     * @return
     *     possible object is
     *     {@link MembershipLevel }
     *     
     */
    public MembershipLevel getMembershipLevel() {
        return membershipLevel;
    }

    /**
     * Sets the value of the membershipLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link MembershipLevel }
     *     
     */
    public void setMembershipLevel(MembershipLevel value) {
        this.membershipLevel = value;
    }

    /**
     * Gets the value of the membershipOrganisation property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationTO }
     *     
     */
    public OrganisationTO getMembershipOrganisation() {
        return membershipOrganisation;
    }

    /**
     * Sets the value of the membershipOrganisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationTO }
     *     
     */
    public void setMembershipOrganisation(OrganisationTO value) {
        this.membershipOrganisation = value;
    }

    /**
     * Gets the value of the connectionCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getConnectionCount() {
        return connectionCount;
    }

    /**
     * Sets the value of the connectionCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setConnectionCount(BigInteger value) {
        this.connectionCount = value;
    }

}
