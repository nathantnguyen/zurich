
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for NewBusinessRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NewBusinessRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}StatusChangeRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="createdFinancialServicesAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="targetProduct" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}MarketableProduct_TO" minOccurs="0"/>
 *         &lt;element name="requestedExpirationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NewBusinessRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "rest"
})
public class NewBusinessRequestTO
    extends StatusChangeRequestTO
{

    @XmlElementRefs({
        @XmlElementRef(name = "createdFinancialServicesAgreement", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "requestedExpirationDate", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "targetProduct", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> rest;

    /**
     * Gets the rest of the content model. 
     * 
     * <p>
     * You are getting this "catch-all" property because of the following reason: 
     * The field name "RequestedExpirationDate" is used by two different parts of a schema. See: 
     * line 630 of file:/Users/nathannguyen/AnypointStudio/workspace/zurich/src/main/wsdl/schema/FinancialServicesAgreement.xsd
     * line 437 of file:/Users/nathannguyen/AnypointStudio/workspace/zurich/src/main/wsdl/schema/ContractAndSpecification.xsd
     * <p>
     * To get rid of this property, apply a property customization to one 
     * of both of the following declarations to change their names: 
     * Gets the value of the rest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link FinancialServicesAgreementTO }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link MarketableProductTO }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getRest() {
        if (rest == null) {
            rest = new ArrayList<JAXBElement<?>>();
        }
        return this.rest;
    }

    /**
     * Gets the rest of the content model. 
     * 
     * <p>
     * You are getting this "catch-all" property because of the following reason: 
     * The field name "RequestedExpirationDate" is used by two different parts of a schema. See: 
     * line 630 of file:/Users/nathannguyen/AnypointStudio/workspace/zurich/src/main/wsdl/schema/FinancialServicesAgreement.xsd
     * line 437 of file:/Users/nathannguyen/AnypointStudio/workspace/zurich/src/main/wsdl/schema/ContractAndSpecification.xsd
     * <p>
     * To get rid of this property, apply a property customization to one 
     * of both of the following declarations to change their names: 
     * 
     * 
     * @param rest
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FinancialServicesAgreementTO }{@code >}
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     {@link JAXBElement }{@code <}{@link MarketableProductTO }{@code >}
     *     
     */
    public void setRest(List<JAXBElement<?>> rest) {
        this.rest = rest;
    }

}
