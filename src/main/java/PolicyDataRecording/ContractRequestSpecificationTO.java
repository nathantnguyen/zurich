
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractRequestSpecification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractRequestSpecification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="maximumNumberOfRequests" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="fee" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecificationStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="contractRequestSpecificationComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecificationComposition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="relationshipSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RelationshipSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ruleSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RuleSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="calculations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Calculation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="attributeSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AttributeSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="originalContractSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecification_TO" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractRequestSpecification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "frequency",
    "maximumNumberOfRequests",
    "name",
    "description",
    "externalReference",
    "fee",
    "status",
    "kind",
    "contractRequestSpecificationComponents",
    "relationshipSpecs",
    "ruleSpecs",
    "calculations",
    "attributeSpecs",
    "originalContractSpecification",
    "alternateReference"
})
public class ContractRequestSpecificationTO
    extends BusinessModelObjectTO
{

    protected Frequency frequency;
    protected BigInteger maximumNumberOfRequests;
    protected String name;
    protected String description;
    protected String externalReference;
    protected BaseCurrencyAmount fee;
    protected List<ContractRequestSpecificationStatusTO> status;
    protected String kind;
    protected List<ContractRequestSpecificationCompositionTO> contractRequestSpecificationComponents;
    protected List<RelationshipSpecTO> relationshipSpecs;
    protected List<RuleSpecTO> ruleSpecs;
    protected List<CalculationTO> calculations;
    protected List<AttributeSpecTO> attributeSpecs;
    protected ContractRequestSpecificationTO originalContractSpecification;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the maximumNumberOfRequests property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumNumberOfRequests() {
        return maximumNumberOfRequests;
    }

    /**
     * Sets the value of the maximumNumberOfRequests property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumNumberOfRequests(BigInteger value) {
        this.maximumNumberOfRequests = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the fee property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getFee() {
        return fee;
    }

    /**
     * Sets the value of the fee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setFee(BaseCurrencyAmount value) {
        this.fee = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRequestSpecificationStatusTO }
     * 
     * 
     */
    public List<ContractRequestSpecificationStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<ContractRequestSpecificationStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

    /**
     * Gets the value of the contractRequestSpecificationComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractRequestSpecificationComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractRequestSpecificationComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRequestSpecificationCompositionTO }
     * 
     * 
     */
    public List<ContractRequestSpecificationCompositionTO> getContractRequestSpecificationComponents() {
        if (contractRequestSpecificationComponents == null) {
            contractRequestSpecificationComponents = new ArrayList<ContractRequestSpecificationCompositionTO>();
        }
        return this.contractRequestSpecificationComponents;
    }

    /**
     * Gets the value of the relationshipSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relationshipSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelationshipSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelationshipSpecTO }
     * 
     * 
     */
    public List<RelationshipSpecTO> getRelationshipSpecs() {
        if (relationshipSpecs == null) {
            relationshipSpecs = new ArrayList<RelationshipSpecTO>();
        }
        return this.relationshipSpecs;
    }

    /**
     * Gets the value of the ruleSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ruleSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRuleSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RuleSpecTO }
     * 
     * 
     */
    public List<RuleSpecTO> getRuleSpecs() {
        if (ruleSpecs == null) {
            ruleSpecs = new ArrayList<RuleSpecTO>();
        }
        return this.ruleSpecs;
    }

    /**
     * Gets the value of the calculations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the calculations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCalculations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalculationTO }
     * 
     * 
     */
    public List<CalculationTO> getCalculations() {
        if (calculations == null) {
            calculations = new ArrayList<CalculationTO>();
        }
        return this.calculations;
    }

    /**
     * Gets the value of the attributeSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeSpecTO }
     * 
     * 
     */
    public List<AttributeSpecTO> getAttributeSpecs() {
        if (attributeSpecs == null) {
            attributeSpecs = new ArrayList<AttributeSpecTO>();
        }
        return this.attributeSpecs;
    }

    /**
     * Gets the value of the originalContractSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link ContractRequestSpecificationTO }
     *     
     */
    public ContractRequestSpecificationTO getOriginalContractSpecification() {
        return originalContractSpecification;
    }

    /**
     * Sets the value of the originalContractSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractRequestSpecificationTO }
     *     
     */
    public void setOriginalContractSpecification(ContractRequestSpecificationTO value) {
        this.originalContractSpecification = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link ContractRequestSpecificationStatusTO }
     *     
     */
    public void setStatus(List<ContractRequestSpecificationStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the contractRequestSpecificationComponents property.
     * 
     * @param contractRequestSpecificationComponents
     *     allowed object is
     *     {@link ContractRequestSpecificationCompositionTO }
     *     
     */
    public void setContractRequestSpecificationComponents(List<ContractRequestSpecificationCompositionTO> contractRequestSpecificationComponents) {
        this.contractRequestSpecificationComponents = contractRequestSpecificationComponents;
    }

    /**
     * Sets the value of the relationshipSpecs property.
     * 
     * @param relationshipSpecs
     *     allowed object is
     *     {@link RelationshipSpecTO }
     *     
     */
    public void setRelationshipSpecs(List<RelationshipSpecTO> relationshipSpecs) {
        this.relationshipSpecs = relationshipSpecs;
    }

    /**
     * Sets the value of the ruleSpecs property.
     * 
     * @param ruleSpecs
     *     allowed object is
     *     {@link RuleSpecTO }
     *     
     */
    public void setRuleSpecs(List<RuleSpecTO> ruleSpecs) {
        this.ruleSpecs = ruleSpecs;
    }

    /**
     * Sets the value of the calculations property.
     * 
     * @param calculations
     *     allowed object is
     *     {@link CalculationTO }
     *     
     */
    public void setCalculations(List<CalculationTO> calculations) {
        this.calculations = calculations;
    }

    /**
     * Sets the value of the attributeSpecs property.
     * 
     * @param attributeSpecs
     *     allowed object is
     *     {@link AttributeSpecTO }
     *     
     */
    public void setAttributeSpecs(List<AttributeSpecTO> attributeSpecs) {
        this.attributeSpecs = attributeSpecs;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
