
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountInvolvedInFsa_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountInvolvedInFsa_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}RoleInFinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="agreementCollateral" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Account_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountInvolvedInFsa_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "agreementCollateral"
})
public class AccountInvolvedInFsaTO
    extends RoleInFinancialServicesAgreementTO
{

    protected AccountTO agreementCollateral;

    /**
     * Gets the value of the agreementCollateral property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTO }
     *     
     */
    public AccountTO getAgreementCollateral() {
        return agreementCollateral;
    }

    /**
     * Sets the value of the agreementCollateral property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTO }
     *     
     */
    public void setAgreementCollateral(AccountTO value) {
        this.agreementCollateral = value;
    }

}
