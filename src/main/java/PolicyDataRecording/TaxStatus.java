
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TaxStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Fully Taxed"/>
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="Tax Deferred"/>
 *     &lt;enumeration value="Tax Exempt"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TaxStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Tax/TaxEnumerationsAndStates/")
@XmlEnum
public enum TaxStatus {

    @XmlEnumValue("Fully Taxed")
    FULLY_TAXED("Fully Taxed"),
    @XmlEnumValue("Other")
    OTHER("Other"),
    @XmlEnumValue("Tax Deferred")
    TAX_DEFERRED("Tax Deferred"),
    @XmlEnumValue("Tax Exempt")
    TAX_EXEMPT("Tax Exempt"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    TaxStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TaxStatus fromValue(String v) {
        for (TaxStatus c: TaxStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
