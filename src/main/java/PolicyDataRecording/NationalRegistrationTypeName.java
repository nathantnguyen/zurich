
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NationalRegistrationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NationalRegistrationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Identity Card"/>
 *     &lt;enumeration value="Passport"/>
 *     &lt;enumeration value="Social Security"/>
 *     &lt;enumeration value="Tax Registration"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NationalRegistrationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum NationalRegistrationTypeName {

    @XmlEnumValue("Identity Card")
    IDENTITY_CARD("Identity Card"),
    @XmlEnumValue("Passport")
    PASSPORT("Passport"),
    @XmlEnumValue("Social Security")
    SOCIAL_SECURITY("Social Security"),
    @XmlEnumValue("Tax Registration")
    TAX_REGISTRATION("Tax Registration");
    private final String value;

    NationalRegistrationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NationalRegistrationTypeName fromValue(String v) {
        for (NationalRegistrationTypeName c: NationalRegistrationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
