
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PublicSafety_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PublicSafety_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO">
 *       &lt;sequence>
 *         &lt;element name="staffingStructure" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}StaffingStructure" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PublicSafety_TO", propOrder = {
    "staffingStructure"
})
public class PublicSafetyTO
    extends OrganisationTO
{

    protected StaffingStructure staffingStructure;

    /**
     * Gets the value of the staffingStructure property.
     * 
     * @return
     *     possible object is
     *     {@link StaffingStructure }
     *     
     */
    public StaffingStructure getStaffingStructure() {
        return staffingStructure;
    }

    /**
     * Sets the value of the staffingStructure property.
     * 
     * @param value
     *     allowed object is
     *     {@link StaffingStructure }
     *     
     */
    public void setStaffingStructure(StaffingStructure value) {
        this.staffingStructure = value;
    }

}
