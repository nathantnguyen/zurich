
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContactPreference_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactPreference_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="checkDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="contactInstructions" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="priorityLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}PriorityLevel" minOccurs="0"/>
 *         &lt;element name="checkResult" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}CheckResult" minOccurs="0"/>
 *         &lt;element name="purpose" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}ContactPreferencePurpose" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="timingPreference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}TimingPreference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contactLimitation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactLimitation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="usage" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}ContactPreferenceUsage" minOccurs="0"/>
 *         &lt;element name="contactPoints" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPoint_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="communicationProfiles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CommunicationProfile_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="solicitable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="preferredName" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyName_TO" minOccurs="0"/>
 *         &lt;element name="isDefault" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="alternativeContactPartyNames" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyName_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="alternativeContactPartyReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="preferedContactPersonName" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PersonName_TO" minOccurs="0"/>
 *         &lt;element name="preferedContactPersonReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Person_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactPreference_TO", propOrder = {
    "checkDate",
    "contactInstructions",
    "priorityLevel",
    "checkResult",
    "purpose",
    "startDate",
    "endDate",
    "timingPreference",
    "contactLimitation",
    "rolePlayer",
    "usage",
    "contactPoints",
    "communicationProfiles",
    "solicitable",
    "preferredName",
    "isDefault",
    "alternativeContactPartyNames",
    "alternativeContactPartyReferences",
    "preferedContactPersonName",
    "preferedContactPersonReference"
})
public class ContactPreferenceTO
    extends DependentObjectTO
{

    protected XMLGregorianCalendar checkDate;
    protected String contactInstructions;
    protected PriorityLevel priorityLevel;
    protected CheckResult checkResult;
    protected ContactPreferencePurpose purpose;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected List<TimingPreferenceTO> timingPreference;
    protected List<ContactLimitationTO> contactLimitation;
    protected RolePlayerTO rolePlayer;
    protected String usage;
    protected List<ContactPointTO> contactPoints;
    protected List<CommunicationProfileTO> communicationProfiles;
    protected Boolean solicitable;
    protected PartyNameTO preferredName;
    protected Boolean isDefault;
    protected List<PartyNameTO> alternativeContactPartyNames;
    protected List<ObjectReferenceTO> alternativeContactPartyReferences;
    protected PersonNameTO preferedContactPersonName;
    protected PersonTO preferedContactPersonReference;

    /**
     * Gets the value of the checkDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCheckDate() {
        return checkDate;
    }

    /**
     * Sets the value of the checkDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCheckDate(XMLGregorianCalendar value) {
        this.checkDate = value;
    }

    /**
     * Gets the value of the contactInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactInstructions() {
        return contactInstructions;
    }

    /**
     * Sets the value of the contactInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactInstructions(String value) {
        this.contactInstructions = value;
    }

    /**
     * Gets the value of the priorityLevel property.
     * 
     * @return
     *     possible object is
     *     {@link PriorityLevel }
     *     
     */
    public PriorityLevel getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Sets the value of the priorityLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriorityLevel }
     *     
     */
    public void setPriorityLevel(PriorityLevel value) {
        this.priorityLevel = value;
    }

    /**
     * Gets the value of the checkResult property.
     * 
     * @return
     *     possible object is
     *     {@link CheckResult }
     *     
     */
    public CheckResult getCheckResult() {
        return checkResult;
    }

    /**
     * Sets the value of the checkResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckResult }
     *     
     */
    public void setCheckResult(CheckResult value) {
        this.checkResult = value;
    }

    /**
     * Gets the value of the purpose property.
     * 
     * @return
     *     possible object is
     *     {@link ContactPreferencePurpose }
     *     
     */
    public ContactPreferencePurpose getPurpose() {
        return purpose;
    }

    /**
     * Sets the value of the purpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPreferencePurpose }
     *     
     */
    public void setPurpose(ContactPreferencePurpose value) {
        this.purpose = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the timingPreference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the timingPreference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTimingPreference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TimingPreferenceTO }
     * 
     * 
     */
    public List<TimingPreferenceTO> getTimingPreference() {
        if (timingPreference == null) {
            timingPreference = new ArrayList<TimingPreferenceTO>();
        }
        return this.timingPreference;
    }

    /**
     * Gets the value of the contactLimitation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactLimitation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactLimitation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactLimitationTO }
     * 
     * 
     */
    public List<ContactLimitationTO> getContactLimitation() {
        if (contactLimitation == null) {
            contactLimitation = new ArrayList<ContactLimitationTO>();
        }
        return this.contactLimitation;
    }

    /**
     * Gets the value of the rolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getRolePlayer() {
        return rolePlayer;
    }

    /**
     * Sets the value of the rolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setRolePlayer(RolePlayerTO value) {
        this.rolePlayer = value;
    }

    /**
     * Gets the value of the usage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsage() {
        return usage;
    }

    /**
     * Sets the value of the usage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsage(String value) {
        this.usage = value;
    }

    /**
     * Gets the value of the contactPoints property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactPoints property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactPoints().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactPointTO }
     * 
     * 
     */
    public List<ContactPointTO> getContactPoints() {
        if (contactPoints == null) {
            contactPoints = new ArrayList<ContactPointTO>();
        }
        return this.contactPoints;
    }

    /**
     * Gets the value of the communicationProfiles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the communicationProfiles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommunicationProfiles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationProfileTO }
     * 
     * 
     */
    public List<CommunicationProfileTO> getCommunicationProfiles() {
        if (communicationProfiles == null) {
            communicationProfiles = new ArrayList<CommunicationProfileTO>();
        }
        return this.communicationProfiles;
    }

    /**
     * Gets the value of the solicitable property.
     * This getter has been renamed from isSolicitable() to getSolicitable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSolicitable() {
        return solicitable;
    }

    /**
     * Sets the value of the solicitable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSolicitable(Boolean value) {
        this.solicitable = value;
    }

    /**
     * Gets the value of the preferredName property.
     * 
     * @return
     *     possible object is
     *     {@link PartyNameTO }
     *     
     */
    public PartyNameTO getPreferredName() {
        return preferredName;
    }

    /**
     * Sets the value of the preferredName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyNameTO }
     *     
     */
    public void setPreferredName(PartyNameTO value) {
        this.preferredName = value;
    }

    /**
     * Gets the value of the isDefault property.
     * This getter has been renamed from isIsDefault() to getIsDefault() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     * Sets the value of the isDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDefault(Boolean value) {
        this.isDefault = value;
    }

    /**
     * Gets the value of the alternativeContactPartyNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternativeContactPartyNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternativeContactPartyNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyNameTO }
     * 
     * 
     */
    public List<PartyNameTO> getAlternativeContactPartyNames() {
        if (alternativeContactPartyNames == null) {
            alternativeContactPartyNames = new ArrayList<PartyNameTO>();
        }
        return this.alternativeContactPartyNames;
    }

    /**
     * Gets the value of the alternativeContactPartyReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternativeContactPartyReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternativeContactPartyReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getAlternativeContactPartyReferences() {
        if (alternativeContactPartyReferences == null) {
            alternativeContactPartyReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.alternativeContactPartyReferences;
    }

    /**
     * Gets the value of the preferedContactPersonName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameTO }
     *     
     */
    public PersonNameTO getPreferedContactPersonName() {
        return preferedContactPersonName;
    }

    /**
     * Sets the value of the preferedContactPersonName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameTO }
     *     
     */
    public void setPreferedContactPersonName(PersonNameTO value) {
        this.preferedContactPersonName = value;
    }

    /**
     * Gets the value of the preferedContactPersonReference property.
     * 
     * @return
     *     possible object is
     *     {@link PersonTO }
     *     
     */
    public PersonTO getPreferedContactPersonReference() {
        return preferedContactPersonReference;
    }

    /**
     * Sets the value of the preferedContactPersonReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonTO }
     *     
     */
    public void setPreferedContactPersonReference(PersonTO value) {
        this.preferedContactPersonReference = value;
    }

    /**
     * Sets the value of the timingPreference property.
     * 
     * @param timingPreference
     *     allowed object is
     *     {@link TimingPreferenceTO }
     *     
     */
    public void setTimingPreference(List<TimingPreferenceTO> timingPreference) {
        this.timingPreference = timingPreference;
    }

    /**
     * Sets the value of the contactLimitation property.
     * 
     * @param contactLimitation
     *     allowed object is
     *     {@link ContactLimitationTO }
     *     
     */
    public void setContactLimitation(List<ContactLimitationTO> contactLimitation) {
        this.contactLimitation = contactLimitation;
    }

    /**
     * Sets the value of the contactPoints property.
     * 
     * @param contactPoints
     *     allowed object is
     *     {@link ContactPointTO }
     *     
     */
    public void setContactPoints(List<ContactPointTO> contactPoints) {
        this.contactPoints = contactPoints;
    }

    /**
     * Sets the value of the communicationProfiles property.
     * 
     * @param communicationProfiles
     *     allowed object is
     *     {@link CommunicationProfileTO }
     *     
     */
    public void setCommunicationProfiles(List<CommunicationProfileTO> communicationProfiles) {
        this.communicationProfiles = communicationProfiles;
    }

    /**
     * Sets the value of the alternativeContactPartyNames property.
     * 
     * @param alternativeContactPartyNames
     *     allowed object is
     *     {@link PartyNameTO }
     *     
     */
    public void setAlternativeContactPartyNames(List<PartyNameTO> alternativeContactPartyNames) {
        this.alternativeContactPartyNames = alternativeContactPartyNames;
    }

    /**
     * Sets the value of the alternativeContactPartyReferences property.
     * 
     * @param alternativeContactPartyReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setAlternativeContactPartyReferences(List<ObjectReferenceTO> alternativeContactPartyReferences) {
        this.alternativeContactPartyReferences = alternativeContactPartyReferences;
    }

}
