
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConstantPropertySpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConstantPropertySpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AttributeSpec_TO">
 *       &lt;sequence>
 *         &lt;element name="theValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Value" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstantPropertySpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "theValue"
})
public class ConstantPropertySpecTO
    extends AttributeSpecTO
{

    protected Value theValue;

    /**
     * Gets the value of the theValue property.
     * 
     * @return
     *     possible object is
     *     {@link Value }
     *     
     */
    public Value getTheValue() {
        return theValue;
    }

    /**
     * Sets the value of the theValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Value }
     *     
     */
    public void setTheValue(Value value) {
        this.theValue = value;
    }

}
