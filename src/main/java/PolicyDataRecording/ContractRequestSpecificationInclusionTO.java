
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractRequestSpecificationInclusion_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractRequestSpecificationInclusion_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="maximumNumberOfRequests" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="contractRequestSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecification_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractRequestSpecificationInclusion_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "maximumNumberOfRequests",
    "contractRequestSpecification"
})
@XmlSeeAlso({
    ContractRequestSpecificationAllowanceTO.class,
    ContractRequestSpecificationCompositionTO.class
})
public class ContractRequestSpecificationInclusionTO
    extends BaseTransferObject
{

    protected BigInteger maximumNumberOfRequests;
    protected ContractRequestSpecificationTO contractRequestSpecification;

    /**
     * Gets the value of the maximumNumberOfRequests property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumNumberOfRequests() {
        return maximumNumberOfRequests;
    }

    /**
     * Sets the value of the maximumNumberOfRequests property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumNumberOfRequests(BigInteger value) {
        this.maximumNumberOfRequests = value;
    }

    /**
     * Gets the value of the contractRequestSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link ContractRequestSpecificationTO }
     *     
     */
    public ContractRequestSpecificationTO getContractRequestSpecification() {
        return contractRequestSpecification;
    }

    /**
     * Sets the value of the contractRequestSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractRequestSpecificationTO }
     *     
     */
    public void setContractRequestSpecification(ContractRequestSpecificationTO value) {
        this.contractRequestSpecification = value;
    }

}
