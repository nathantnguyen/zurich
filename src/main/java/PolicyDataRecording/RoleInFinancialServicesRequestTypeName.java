
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInFinancialServicesRequestTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInFinancialServicesRequestTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Source Fund"/>
 *     &lt;enumeration value="Target Fund"/>
 *     &lt;enumeration value="Target Product"/>
 *     &lt;enumeration value="Requested Assessment Result"/>
 *     &lt;enumeration value="Requested Financial Transaction"/>
 *     &lt;enumeration value="Trigger"/>
 *     &lt;enumeration value="Producer Intermediary Agreement"/>
 *     &lt;enumeration value="Included Product"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInFinancialServicesRequestTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum RoleInFinancialServicesRequestTypeName {

    @XmlEnumValue("Source Fund")
    SOURCE_FUND("Source Fund"),
    @XmlEnumValue("Target Fund")
    TARGET_FUND("Target Fund"),
    @XmlEnumValue("Target Product")
    TARGET_PRODUCT("Target Product"),
    @XmlEnumValue("Requested Assessment Result")
    REQUESTED_ASSESSMENT_RESULT("Requested Assessment Result"),
    @XmlEnumValue("Requested Financial Transaction")
    REQUESTED_FINANCIAL_TRANSACTION("Requested Financial Transaction"),
    @XmlEnumValue("Trigger")
    TRIGGER("Trigger"),
    @XmlEnumValue("Producer Intermediary Agreement")
    PRODUCER_INTERMEDIARY_AGREEMENT("Producer Intermediary Agreement"),
    @XmlEnumValue("Included Product")
    INCLUDED_PRODUCT("Included Product");
    private final String value;

    RoleInFinancialServicesRequestTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInFinancialServicesRequestTypeName fromValue(String v) {
        for (RoleInFinancialServicesRequestTypeName c: RoleInFinancialServicesRequestTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
