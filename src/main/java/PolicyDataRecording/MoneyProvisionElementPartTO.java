
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MoneyProvisionElementPart_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyProvisionElementPart_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexableCurrencyAmount_TO" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="isEstimate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="adjustments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Adjustment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="accountEntries" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AccountEntry_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="baseRate" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Rate_TO" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="moneyProvisionElementPartRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElementPartType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyProvisionElementPart_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "amount",
    "dueDate",
    "isEstimate",
    "adjustments",
    "accountEntries",
    "baseRate",
    "description",
    "moneyProvisionElementPartRootType"
})
@XmlSeeAlso({
    ReservedAmountTO.class
})
public class MoneyProvisionElementPartTO
    extends DependentObjectTO
{

    protected IndexableCurrencyAmountTO amount;
    protected XMLGregorianCalendar dueDate;
    protected Boolean isEstimate;
    protected List<AdjustmentTO> adjustments;
    protected List<AccountEntryTO> accountEntries;
    protected RateTO baseRate;
    protected String description;
    protected MoneyProvisionElementPartTypeTO moneyProvisionElementPartRootType;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public IndexableCurrencyAmountTO getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public void setAmount(IndexableCurrencyAmountTO value) {
        this.amount = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the isEstimate property.
     * This getter has been renamed from isIsEstimate() to getIsEstimate() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsEstimate() {
        return isEstimate;
    }

    /**
     * Sets the value of the isEstimate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsEstimate(Boolean value) {
        this.isEstimate = value;
    }

    /**
     * Gets the value of the adjustments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjustments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjustments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdjustmentTO }
     * 
     * 
     */
    public List<AdjustmentTO> getAdjustments() {
        if (adjustments == null) {
            adjustments = new ArrayList<AdjustmentTO>();
        }
        return this.adjustments;
    }

    /**
     * Gets the value of the accountEntries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountEntries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountEntries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountEntryTO }
     * 
     * 
     */
    public List<AccountEntryTO> getAccountEntries() {
        if (accountEntries == null) {
            accountEntries = new ArrayList<AccountEntryTO>();
        }
        return this.accountEntries;
    }

    /**
     * Gets the value of the baseRate property.
     * 
     * @return
     *     possible object is
     *     {@link RateTO }
     *     
     */
    public RateTO getBaseRate() {
        return baseRate;
    }

    /**
     * Sets the value of the baseRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateTO }
     *     
     */
    public void setBaseRate(RateTO value) {
        this.baseRate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the moneyProvisionElementPartRootType property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionElementPartTypeTO }
     *     
     */
    public MoneyProvisionElementPartTypeTO getMoneyProvisionElementPartRootType() {
        return moneyProvisionElementPartRootType;
    }

    /**
     * Sets the value of the moneyProvisionElementPartRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionElementPartTypeTO }
     *     
     */
    public void setMoneyProvisionElementPartRootType(MoneyProvisionElementPartTypeTO value) {
        this.moneyProvisionElementPartRootType = value;
    }

    /**
     * Sets the value of the adjustments property.
     * 
     * @param adjustments
     *     allowed object is
     *     {@link AdjustmentTO }
     *     
     */
    public void setAdjustments(List<AdjustmentTO> adjustments) {
        this.adjustments = adjustments;
    }

    /**
     * Sets the value of the accountEntries property.
     * 
     * @param accountEntries
     *     allowed object is
     *     {@link AccountEntryTO }
     *     
     */
    public void setAccountEntries(List<AccountEntryTO> accountEntries) {
        this.accountEntries = accountEntries;
    }

}
