
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AgreementVersion_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgreementVersion_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="creationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="replacementDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="replacementReason" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/}AgreementState" minOccurs="0"/>
 *         &lt;element name="agreementSpecDescription" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="versionNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="quotations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Agreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="topLevelAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Agreement_TO" minOccurs="0"/>
 *         &lt;element name="nextVersion" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementVersion_TO" minOccurs="0"/>
 *         &lt;element name="previousVersion" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementVersion_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgreementVersion_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "creationDate",
    "replacementDate",
    "replacementReason",
    "state",
    "agreementSpecDescription",
    "versionNumber",
    "quotations",
    "topLevelAgreement",
    "nextVersion",
    "previousVersion"
})
public class AgreementVersionTO
    extends BaseTransferObject
{

    protected XMLGregorianCalendar creationDate;
    protected XMLGregorianCalendar replacementDate;
    protected String replacementReason;
    protected AgreementState state;
    protected String agreementSpecDescription;
    protected String versionNumber;
    protected List<AgreementTO> quotations;
    protected AgreementTO topLevelAgreement;
    protected AgreementVersionTO nextVersion;
    protected AgreementVersionTO previousVersion;

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the replacementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReplacementDate() {
        return replacementDate;
    }

    /**
     * Sets the value of the replacementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReplacementDate(XMLGregorianCalendar value) {
        this.replacementDate = value;
    }

    /**
     * Gets the value of the replacementReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReplacementReason() {
        return replacementReason;
    }

    /**
     * Sets the value of the replacementReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReplacementReason(String value) {
        this.replacementReason = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementState }
     *     
     */
    public AgreementState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementState }
     *     
     */
    public void setState(AgreementState value) {
        this.state = value;
    }

    /**
     * Gets the value of the agreementSpecDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgreementSpecDescription() {
        return agreementSpecDescription;
    }

    /**
     * Sets the value of the agreementSpecDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgreementSpecDescription(String value) {
        this.agreementSpecDescription = value;
    }

    /**
     * Gets the value of the versionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionNumber() {
        return versionNumber;
    }

    /**
     * Sets the value of the versionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionNumber(String value) {
        this.versionNumber = value;
    }

    /**
     * Gets the value of the quotations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quotations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuotations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgreementTO }
     * 
     * 
     */
    public List<AgreementTO> getQuotations() {
        if (quotations == null) {
            quotations = new ArrayList<AgreementTO>();
        }
        return this.quotations;
    }

    /**
     * Gets the value of the topLevelAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementTO }
     *     
     */
    public AgreementTO getTopLevelAgreement() {
        return topLevelAgreement;
    }

    /**
     * Sets the value of the topLevelAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementTO }
     *     
     */
    public void setTopLevelAgreement(AgreementTO value) {
        this.topLevelAgreement = value;
    }

    /**
     * Gets the value of the nextVersion property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementVersionTO }
     *     
     */
    public AgreementVersionTO getNextVersion() {
        return nextVersion;
    }

    /**
     * Sets the value of the nextVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementVersionTO }
     *     
     */
    public void setNextVersion(AgreementVersionTO value) {
        this.nextVersion = value;
    }

    /**
     * Gets the value of the previousVersion property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementVersionTO }
     *     
     */
    public AgreementVersionTO getPreviousVersion() {
        return previousVersion;
    }

    /**
     * Sets the value of the previousVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementVersionTO }
     *     
     */
    public void setPreviousVersion(AgreementVersionTO value) {
        this.previousVersion = value;
    }

    /**
     * Sets the value of the quotations property.
     * 
     * @param quotations
     *     allowed object is
     *     {@link AgreementTO }
     *     
     */
    public void setQuotations(List<AgreementTO> quotations) {
        this.quotations = quotations;
    }

}
