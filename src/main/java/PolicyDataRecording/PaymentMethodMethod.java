
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentMethodMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentMethodMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Bank Transfer"/>
 *     &lt;enumeration value="Cash"/>
 *     &lt;enumeration value="Cheque"/>
 *     &lt;enumeration value="Credit Card Billing"/>
 *     &lt;enumeration value="Direct Debit"/>
 *     &lt;enumeration value="Government Allotment"/>
 *     &lt;enumeration value="Paid In Advance"/>
 *     &lt;enumeration value="Payroll Deduction"/>
 *     &lt;enumeration value="Via Intermediary"/>
 *     &lt;enumeration value="Standing Order"/>
 *     &lt;enumeration value="Internal Transfer"/>
 *     &lt;enumeration value="Offset"/>
 *     &lt;enumeration value="Unit Cancellation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentMethodMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum PaymentMethodMethod {

    @XmlEnumValue("Bank Transfer")
    BANK_TRANSFER("Bank Transfer"),
    @XmlEnumValue("Cash")
    CASH("Cash"),
    @XmlEnumValue("Cheque")
    CHEQUE("Cheque"),
    @XmlEnumValue("Credit Card Billing")
    CREDIT_CARD_BILLING("Credit Card Billing"),
    @XmlEnumValue("Direct Debit")
    DIRECT_DEBIT("Direct Debit"),
    @XmlEnumValue("Government Allotment")
    GOVERNMENT_ALLOTMENT("Government Allotment"),
    @XmlEnumValue("Paid In Advance")
    PAID_IN_ADVANCE("Paid In Advance"),
    @XmlEnumValue("Payroll Deduction")
    PAYROLL_DEDUCTION("Payroll Deduction"),
    @XmlEnumValue("Via Intermediary")
    VIA_INTERMEDIARY("Via Intermediary"),
    @XmlEnumValue("Standing Order")
    STANDING_ORDER("Standing Order"),
    @XmlEnumValue("Internal Transfer")
    INTERNAL_TRANSFER("Internal Transfer"),
    @XmlEnumValue("Offset")
    OFFSET("Offset"),
    @XmlEnumValue("Unit Cancellation")
    UNIT_CANCELLATION("Unit Cancellation");
    private final String value;

    PaymentMethodMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentMethodMethod fromValue(String v) {
        for (PaymentMethodMethod c: PaymentMethodMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
