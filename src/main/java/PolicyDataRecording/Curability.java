
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Curability.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Curability">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Fifty Percent"/>
 *     &lt;enumeration value="Curable"/>
 *     &lt;enumeration value="Not Curable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Curability", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum Curability {

    @XmlEnumValue("Fifty Percent")
    FIFTY_PERCENT("Fifty Percent"),
    @XmlEnumValue("Curable")
    CURABLE("Curable"),
    @XmlEnumValue("Not Curable")
    NOT_CURABLE("Not Curable");
    private final String value;

    Curability(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Curability fromValue(String v) {
        for (Curability c: Curability.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
