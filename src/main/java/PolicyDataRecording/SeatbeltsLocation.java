
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SeatbeltsLocation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SeatbeltsLocation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All Seats"/>
 *     &lt;enumeration value="Front And Rear"/>
 *     &lt;enumeration value="Front Only"/>
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Rear Only"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SeatbeltsLocation", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum SeatbeltsLocation {

    @XmlEnumValue("All Seats")
    ALL_SEATS("All Seats"),
    @XmlEnumValue("Front And Rear")
    FRONT_AND_REAR("Front And Rear"),
    @XmlEnumValue("Front Only")
    FRONT_ONLY("Front Only"),
    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Rear Only")
    REAR_ONLY("Rear Only");
    private final String value;

    SeatbeltsLocation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SeatbeltsLocation fromValue(String v) {
        for (SeatbeltsLocation c: SeatbeltsLocation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
