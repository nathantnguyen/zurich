
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContractRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="requestedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="requestDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="valid" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}RequestValidity" minOccurs="0"/>
 *         &lt;element name="communicationMedium" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}CommunicationMedium" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="targetContracts" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInContractRequest" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContractRequest_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contractRequestSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecification_TO" minOccurs="0"/>
 *         &lt;element name="parentContractRequest" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequest_TO" minOccurs="0"/>
 *         &lt;element name="requestReason" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="includedContractRequests" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequest_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="requestedExpirationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="renewedContractRequest" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequest_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "requestedDate",
    "requestDate",
    "valid",
    "communicationMedium",
    "externalReference",
    "targetContracts",
    "status",
    "rolesInContractRequest",
    "contractRequestSpecification",
    "parentContractRequest",
    "requestReason",
    "includedContractRequests",
    "alternateReference",
    "requestedExpirationDate",
    "renewedContractRequest"
})
@XmlSeeAlso({
    CorporateAgreementRequestTO.class,
    IntermediaryAgreementRequestTO.class,
    EmploymentAgreementRequestTO.class,
    FinancialServicesRequestTO.class
})
public class ContractRequestTO
    extends BusinessModelObjectTO
{

    protected XMLGregorianCalendar requestedDate;
    protected XMLGregorianCalendar requestDate;
    protected RequestValidity valid;
    protected CommunicationMedium communicationMedium;
    protected String externalReference;
    protected List<ContractTO> targetContracts;
    protected List<ContractRequestStatusTO> status;
    protected List<RoleInContractRequestTO> rolesInContractRequest;
    protected ContractRequestSpecificationTO contractRequestSpecification;
    protected ContractRequestTO parentContractRequest;
    protected String requestReason;
    protected List<ContractRequestTO> includedContractRequests;
    protected List<AlternateIdTO> alternateReference;
    protected XMLGregorianCalendar requestedExpirationDate;
    protected ContractRequestTO renewedContractRequest;

    /**
     * Gets the value of the requestedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestedDate() {
        return requestedDate;
    }

    /**
     * Sets the value of the requestedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestedDate(XMLGregorianCalendar value) {
        this.requestedDate = value;
    }

    /**
     * Gets the value of the requestDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestDate() {
        return requestDate;
    }

    /**
     * Sets the value of the requestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestDate(XMLGregorianCalendar value) {
        this.requestDate = value;
    }

    /**
     * Gets the value of the valid property.
     * 
     * @return
     *     possible object is
     *     {@link RequestValidity }
     *     
     */
    public RequestValidity getValid() {
        return valid;
    }

    /**
     * Sets the value of the valid property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestValidity }
     *     
     */
    public void setValid(RequestValidity value) {
        this.valid = value;
    }

    /**
     * Gets the value of the communicationMedium property.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationMedium }
     *     
     */
    public CommunicationMedium getCommunicationMedium() {
        return communicationMedium;
    }

    /**
     * Sets the value of the communicationMedium property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationMedium }
     *     
     */
    public void setCommunicationMedium(CommunicationMedium value) {
        this.communicationMedium = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the targetContracts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the targetContracts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTargetContracts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractTO }
     * 
     * 
     */
    public List<ContractTO> getTargetContracts() {
        if (targetContracts == null) {
            targetContracts = new ArrayList<ContractTO>();
        }
        return this.targetContracts;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRequestStatusTO }
     * 
     * 
     */
    public List<ContractRequestStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<ContractRequestStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the rolesInContractRequest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInContractRequest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInContractRequest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInContractRequestTO }
     * 
     * 
     */
    public List<RoleInContractRequestTO> getRolesInContractRequest() {
        if (rolesInContractRequest == null) {
            rolesInContractRequest = new ArrayList<RoleInContractRequestTO>();
        }
        return this.rolesInContractRequest;
    }

    /**
     * Gets the value of the contractRequestSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link ContractRequestSpecificationTO }
     *     
     */
    public ContractRequestSpecificationTO getContractRequestSpecification() {
        return contractRequestSpecification;
    }

    /**
     * Sets the value of the contractRequestSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractRequestSpecificationTO }
     *     
     */
    public void setContractRequestSpecification(ContractRequestSpecificationTO value) {
        this.contractRequestSpecification = value;
    }

    /**
     * Gets the value of the parentContractRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ContractRequestTO }
     *     
     */
    public ContractRequestTO getParentContractRequest() {
        return parentContractRequest;
    }

    /**
     * Sets the value of the parentContractRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractRequestTO }
     *     
     */
    public void setParentContractRequest(ContractRequestTO value) {
        this.parentContractRequest = value;
    }

    /**
     * Gets the value of the requestReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestReason() {
        return requestReason;
    }

    /**
     * Sets the value of the requestReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestReason(String value) {
        this.requestReason = value;
    }

    /**
     * Gets the value of the includedContractRequests property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedContractRequests property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludedContractRequests().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRequestTO }
     * 
     * 
     */
    public List<ContractRequestTO> getIncludedContractRequests() {
        if (includedContractRequests == null) {
            includedContractRequests = new ArrayList<ContractRequestTO>();
        }
        return this.includedContractRequests;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Gets the value of the requestedExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestedExpirationDate() {
        return requestedExpirationDate;
    }

    /**
     * Sets the value of the requestedExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestedExpirationDate(XMLGregorianCalendar value) {
        this.requestedExpirationDate = value;
    }

    /**
     * Gets the value of the renewedContractRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ContractRequestTO }
     *     
     */
    public ContractRequestTO getRenewedContractRequest() {
        return renewedContractRequest;
    }

    /**
     * Sets the value of the renewedContractRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractRequestTO }
     *     
     */
    public void setRenewedContractRequest(ContractRequestTO value) {
        this.renewedContractRequest = value;
    }

    /**
     * Sets the value of the targetContracts property.
     * 
     * @param targetContracts
     *     allowed object is
     *     {@link ContractTO }
     *     
     */
    public void setTargetContracts(List<ContractTO> targetContracts) {
        this.targetContracts = targetContracts;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link ContractRequestStatusTO }
     *     
     */
    public void setStatus(List<ContractRequestStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the rolesInContractRequest property.
     * 
     * @param rolesInContractRequest
     *     allowed object is
     *     {@link RoleInContractRequestTO }
     *     
     */
    public void setRolesInContractRequest(List<RoleInContractRequestTO> rolesInContractRequest) {
        this.rolesInContractRequest = rolesInContractRequest;
    }

    /**
     * Sets the value of the includedContractRequests property.
     * 
     * @param includedContractRequests
     *     allowed object is
     *     {@link ContractRequestTO }
     *     
     */
    public void setIncludedContractRequests(List<ContractRequestTO> includedContractRequests) {
        this.includedContractRequests = includedContractRequests;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
