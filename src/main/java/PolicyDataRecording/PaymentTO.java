
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Payment_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Payment_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}FinancialTransaction_TO">
 *       &lt;sequence>
 *         &lt;element name="clearanceDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="depositDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="rejectionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="valueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="paymentDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="authorisationId" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="authorisationCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="executeImmediately" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="receivingMoneyInScheduler" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyInScheduler_TO" minOccurs="0"/>
 *         &lt;element name="remittances" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Remittance_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}PaymentStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="claimOfferReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="remittanceIdentifier" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="fiDebitTraceNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="fiCreditTraceNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="category" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" minOccurs="0"/>
 *         &lt;element name="rejectionReason" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/}PaymentRejectionReason" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Payment_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "clearanceDate",
    "depositDate",
    "rejectionDate",
    "valueDate",
    "paymentDate",
    "authorisationId",
    "authorisationCount",
    "executeImmediately",
    "receivingMoneyInScheduler",
    "remittances",
    "status",
    "claimOfferReference",
    "remittanceIdentifier",
    "fiDebitTraceNumber",
    "fiCreditTraceNumber",
    "dueDate",
    "category",
    "rejectionReason"
})
public class PaymentTO
    extends FinancialTransactionTO
{

    protected XMLGregorianCalendar clearanceDate;
    protected XMLGregorianCalendar depositDate;
    protected XMLGregorianCalendar rejectionDate;
    protected XMLGregorianCalendar valueDate;
    protected XMLGregorianCalendar paymentDate;
    protected String authorisationId;
    protected BigInteger authorisationCount;
    protected Boolean executeImmediately;
    protected MoneyInSchedulerTO receivingMoneyInScheduler;
    protected List<RemittanceTO> remittances;
    protected List<PaymentStatusTO> status;
    protected ObjectReferenceTO claimOfferReference;
    protected String remittanceIdentifier;
    protected String fiDebitTraceNumber;
    protected String fiCreditTraceNumber;
    protected XMLGregorianCalendar dueDate;
    protected CategoryTO category;
    protected PaymentRejectionReason rejectionReason;

    /**
     * Gets the value of the clearanceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClearanceDate() {
        return clearanceDate;
    }

    /**
     * Sets the value of the clearanceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClearanceDate(XMLGregorianCalendar value) {
        this.clearanceDate = value;
    }

    /**
     * Gets the value of the depositDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepositDate() {
        return depositDate;
    }

    /**
     * Sets the value of the depositDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepositDate(XMLGregorianCalendar value) {
        this.depositDate = value;
    }

    /**
     * Gets the value of the rejectionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRejectionDate() {
        return rejectionDate;
    }

    /**
     * Sets the value of the rejectionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRejectionDate(XMLGregorianCalendar value) {
        this.rejectionDate = value;
    }

    /**
     * Gets the value of the valueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValueDate() {
        return valueDate;
    }

    /**
     * Sets the value of the valueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValueDate(XMLGregorianCalendar value) {
        this.valueDate = value;
    }

    /**
     * Gets the value of the paymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the value of the paymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDate(XMLGregorianCalendar value) {
        this.paymentDate = value;
    }

    /**
     * Gets the value of the authorisationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorisationId() {
        return authorisationId;
    }

    /**
     * Sets the value of the authorisationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorisationId(String value) {
        this.authorisationId = value;
    }

    /**
     * Gets the value of the authorisationCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAuthorisationCount() {
        return authorisationCount;
    }

    /**
     * Sets the value of the authorisationCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAuthorisationCount(BigInteger value) {
        this.authorisationCount = value;
    }

    /**
     * Gets the value of the executeImmediately property.
     * This getter has been renamed from isExecuteImmediately() to getExecuteImmediately() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExecuteImmediately() {
        return executeImmediately;
    }

    /**
     * Sets the value of the executeImmediately property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExecuteImmediately(Boolean value) {
        this.executeImmediately = value;
    }

    /**
     * Gets the value of the receivingMoneyInScheduler property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyInSchedulerTO }
     *     
     */
    public MoneyInSchedulerTO getReceivingMoneyInScheduler() {
        return receivingMoneyInScheduler;
    }

    /**
     * Sets the value of the receivingMoneyInScheduler property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyInSchedulerTO }
     *     
     */
    public void setReceivingMoneyInScheduler(MoneyInSchedulerTO value) {
        this.receivingMoneyInScheduler = value;
    }

    /**
     * Gets the value of the remittances property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remittances property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemittances().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemittanceTO }
     * 
     * 
     */
    public List<RemittanceTO> getRemittances() {
        if (remittances == null) {
            remittances = new ArrayList<RemittanceTO>();
        }
        return this.remittances;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentStatusTO }
     * 
     * 
     */
    public List<PaymentStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<PaymentStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the claimOfferReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getClaimOfferReference() {
        return claimOfferReference;
    }

    /**
     * Sets the value of the claimOfferReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setClaimOfferReference(ObjectReferenceTO value) {
        this.claimOfferReference = value;
    }

    /**
     * Gets the value of the remittanceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemittanceIdentifier() {
        return remittanceIdentifier;
    }

    /**
     * Sets the value of the remittanceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemittanceIdentifier(String value) {
        this.remittanceIdentifier = value;
    }

    /**
     * Gets the value of the fiDebitTraceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiDebitTraceNumber() {
        return fiDebitTraceNumber;
    }

    /**
     * Sets the value of the fiDebitTraceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiDebitTraceNumber(String value) {
        this.fiDebitTraceNumber = value;
    }

    /**
     * Gets the value of the fiCreditTraceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiCreditTraceNumber() {
        return fiCreditTraceNumber;
    }

    /**
     * Sets the value of the fiCreditTraceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiCreditTraceNumber(String value) {
        this.fiCreditTraceNumber = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link CategoryTO }
     *     
     */
    public CategoryTO getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setCategory(CategoryTO value) {
        this.category = value;
    }

    /**
     * Gets the value of the rejectionReason property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentRejectionReason }
     *     
     */
    public PaymentRejectionReason getRejectionReason() {
        return rejectionReason;
    }

    /**
     * Sets the value of the rejectionReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentRejectionReason }
     *     
     */
    public void setRejectionReason(PaymentRejectionReason value) {
        this.rejectionReason = value;
    }

    /**
     * Sets the value of the remittances property.
     * 
     * @param remittances
     *     allowed object is
     *     {@link RemittanceTO }
     *     
     */
    public void setRemittances(List<RemittanceTO> remittances) {
        this.remittances = remittances;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link PaymentStatusTO }
     *     
     */
    public void setStatus(List<PaymentStatusTO> status) {
        this.status = status;
    }

}
