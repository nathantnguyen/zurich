
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskPosition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskPosition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}FinancialValuation_TO">
 *       &lt;sequence>
 *         &lt;element name="valueAtRiskConfidenceLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="valueAtRiskMeasure" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskPosition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "valueAtRiskConfidenceLevel",
    "valueAtRiskMeasure"
})
public class RiskPositionTO
    extends FinancialValuationTO
{

    protected BigDecimal valueAtRiskConfidenceLevel;
    protected BaseCurrencyAmount valueAtRiskMeasure;

    /**
     * Gets the value of the valueAtRiskConfidenceLevel property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValueAtRiskConfidenceLevel() {
        return valueAtRiskConfidenceLevel;
    }

    /**
     * Sets the value of the valueAtRiskConfidenceLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValueAtRiskConfidenceLevel(BigDecimal value) {
        this.valueAtRiskConfidenceLevel = value;
    }

    /**
     * Gets the value of the valueAtRiskMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getValueAtRiskMeasure() {
        return valueAtRiskMeasure;
    }

    /**
     * Sets the value of the valueAtRiskMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setValueAtRiskMeasure(BaseCurrencyAmount value) {
        this.valueAtRiskMeasure = value;
    }

}
