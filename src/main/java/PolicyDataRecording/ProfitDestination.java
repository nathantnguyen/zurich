
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProfitDestination.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProfitDestination">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Accumulate At Interest"/>
 *     &lt;enumeration value="Add Dividend To Cash Value Ul"/>
 *     &lt;enumeration value="Economatic"/>
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="Paid In Cash"/>
 *     &lt;enumeration value="Paid Up Additions"/>
 *     &lt;enumeration value="Plan Enhancement"/>
 *     &lt;enumeration value="Policy Improvement"/>
 *     &lt;enumeration value="Reduce Loan Values"/>
 *     &lt;enumeration value="Reduce Loan Values Balance In Cash"/>
 *     &lt;enumeration value="Reduce Loan Values Balance To Accumulate"/>
 *     &lt;enumeration value="Reduce Loan Values Balance To Pua"/>
 *     &lt;enumeration value="Reduce Premium"/>
 *     &lt;enumeration value="Reduce Premium Balance In Cash"/>
 *     &lt;enumeration value="Reduce Premium Balance To Accumulate"/>
 *     &lt;enumeration value="Reduce Premium Balance To Pua"/>
 *     &lt;enumeration value="Specified Amount In Cash Balance To Pua"/>
 *     &lt;enumeration value="Term Dividend Option Full"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProfitDestination", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum ProfitDestination {

    @XmlEnumValue("Accumulate At Interest")
    ACCUMULATE_AT_INTEREST("Accumulate At Interest"),
    @XmlEnumValue("Add Dividend To Cash Value Ul")
    ADD_DIVIDEND_TO_CASH_VALUE_UL("Add Dividend To Cash Value Ul"),
    @XmlEnumValue("Economatic")
    ECONOMATIC("Economatic"),
    @XmlEnumValue("Other")
    OTHER("Other"),
    @XmlEnumValue("Paid In Cash")
    PAID_IN_CASH("Paid In Cash"),
    @XmlEnumValue("Paid Up Additions")
    PAID_UP_ADDITIONS("Paid Up Additions"),
    @XmlEnumValue("Plan Enhancement")
    PLAN_ENHANCEMENT("Plan Enhancement"),
    @XmlEnumValue("Policy Improvement")
    POLICY_IMPROVEMENT("Policy Improvement"),
    @XmlEnumValue("Reduce Loan Values")
    REDUCE_LOAN_VALUES("Reduce Loan Values"),
    @XmlEnumValue("Reduce Loan Values Balance In Cash")
    REDUCE_LOAN_VALUES_BALANCE_IN_CASH("Reduce Loan Values Balance In Cash"),
    @XmlEnumValue("Reduce Loan Values Balance To Accumulate")
    REDUCE_LOAN_VALUES_BALANCE_TO_ACCUMULATE("Reduce Loan Values Balance To Accumulate"),
    @XmlEnumValue("Reduce Loan Values Balance To Pua")
    REDUCE_LOAN_VALUES_BALANCE_TO_PUA("Reduce Loan Values Balance To Pua"),
    @XmlEnumValue("Reduce Premium")
    REDUCE_PREMIUM("Reduce Premium"),
    @XmlEnumValue("Reduce Premium Balance In Cash")
    REDUCE_PREMIUM_BALANCE_IN_CASH("Reduce Premium Balance In Cash"),
    @XmlEnumValue("Reduce Premium Balance To Accumulate")
    REDUCE_PREMIUM_BALANCE_TO_ACCUMULATE("Reduce Premium Balance To Accumulate"),
    @XmlEnumValue("Reduce Premium Balance To Pua")
    REDUCE_PREMIUM_BALANCE_TO_PUA("Reduce Premium Balance To Pua"),
    @XmlEnumValue("Specified Amount In Cash Balance To Pua")
    SPECIFIED_AMOUNT_IN_CASH_BALANCE_TO_PUA("Specified Amount In Cash Balance To Pua"),
    @XmlEnumValue("Term Dividend Option Full")
    TERM_DIVIDEND_OPTION_FULL("Term Dividend Option Full"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    ProfitDestination(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProfitDestination fromValue(String v) {
        for (ProfitDestination c: ProfitDestination.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
