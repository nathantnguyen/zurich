
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Bond_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Bond_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="totalIssuedAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="parValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="effectiveDuration" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="modifiedDuration" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="assetBackedInstrumentFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="firstRedemptionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="sinkingFundFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bond_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "totalIssuedAmount",
    "parValue",
    "effectiveDuration",
    "modifiedDuration",
    "assetBackedInstrumentFlag",
    "firstRedemptionDate",
    "sinkingFundFlag"
})
public class BondTO
    extends FinancialAssetTO
{

    protected BaseCurrencyAmount totalIssuedAmount;
    protected BaseCurrencyAmount parValue;
    protected BigDecimal effectiveDuration;
    protected BigDecimal modifiedDuration;
    protected Boolean assetBackedInstrumentFlag;
    protected XMLGregorianCalendar firstRedemptionDate;
    protected Boolean sinkingFundFlag;

    /**
     * Gets the value of the totalIssuedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTotalIssuedAmount() {
        return totalIssuedAmount;
    }

    /**
     * Sets the value of the totalIssuedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTotalIssuedAmount(BaseCurrencyAmount value) {
        this.totalIssuedAmount = value;
    }

    /**
     * Gets the value of the parValue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getParValue() {
        return parValue;
    }

    /**
     * Sets the value of the parValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setParValue(BaseCurrencyAmount value) {
        this.parValue = value;
    }

    /**
     * Gets the value of the effectiveDuration property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEffectiveDuration() {
        return effectiveDuration;
    }

    /**
     * Sets the value of the effectiveDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEffectiveDuration(BigDecimal value) {
        this.effectiveDuration = value;
    }

    /**
     * Gets the value of the modifiedDuration property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getModifiedDuration() {
        return modifiedDuration;
    }

    /**
     * Sets the value of the modifiedDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setModifiedDuration(BigDecimal value) {
        this.modifiedDuration = value;
    }

    /**
     * Gets the value of the assetBackedInstrumentFlag property.
     * This getter has been renamed from isAssetBackedInstrumentFlag() to getAssetBackedInstrumentFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAssetBackedInstrumentFlag() {
        return assetBackedInstrumentFlag;
    }

    /**
     * Sets the value of the assetBackedInstrumentFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAssetBackedInstrumentFlag(Boolean value) {
        this.assetBackedInstrumentFlag = value;
    }

    /**
     * Gets the value of the firstRedemptionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstRedemptionDate() {
        return firstRedemptionDate;
    }

    /**
     * Sets the value of the firstRedemptionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstRedemptionDate(XMLGregorianCalendar value) {
        this.firstRedemptionDate = value;
    }

    /**
     * Gets the value of the sinkingFundFlag property.
     * This getter has been renamed from isSinkingFundFlag() to getSinkingFundFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSinkingFundFlag() {
        return sinkingFundFlag;
    }

    /**
     * Sets the value of the sinkingFundFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSinkingFundFlag(Boolean value) {
        this.sinkingFundFlag = value;
    }

}
