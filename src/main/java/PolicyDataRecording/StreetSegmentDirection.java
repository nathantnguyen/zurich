
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StreetSegmentDirection.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StreetSegmentDirection">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Forward"/>
 *     &lt;enumeration value="Reversed"/>
 *     &lt;enumeration value="Both"/>
 *     &lt;enumeration value="Undetermined"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StreetSegmentDirection", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum StreetSegmentDirection {

    @XmlEnumValue("Forward")
    FORWARD("Forward"),
    @XmlEnumValue("Reversed")
    REVERSED("Reversed"),
    @XmlEnumValue("Both")
    BOTH("Both"),
    @XmlEnumValue("Undetermined")
    UNDETERMINED("Undetermined");
    private final String value;

    StreetSegmentDirection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StreetSegmentDirection fromValue(String v) {
        for (StreetSegmentDirection c: StreetSegmentDirection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
