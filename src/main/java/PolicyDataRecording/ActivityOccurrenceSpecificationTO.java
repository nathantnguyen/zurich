
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityOccurrenceSpecification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityOccurrenceSpecification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivitySpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="activityOccurrenceComponentsSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrenceSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="activityOccurencePredecessorsSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrenceSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="activityOccurenceSuccessorsSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrenceSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="compositeActivityOccurrencesSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrenceSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityOccurrenceSpecification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "activityOccurrenceComponentsSpecifications",
    "activityOccurencePredecessorsSpecification",
    "activityOccurenceSuccessorsSpecification",
    "compositeActivityOccurrencesSpecifications"
})
@XmlSeeAlso({
    TaskSpecificationTO.class
})
public class ActivityOccurrenceSpecificationTO
    extends ActivitySpecificationTO
{

    protected List<ActivityOccurrenceSpecificationTO> activityOccurrenceComponentsSpecifications;
    protected List<ActivityOccurrenceSpecificationTO> activityOccurencePredecessorsSpecification;
    protected List<ActivityOccurrenceSpecificationTO> activityOccurenceSuccessorsSpecification;
    protected List<ActivityOccurrenceSpecificationTO> compositeActivityOccurrencesSpecifications;

    /**
     * Gets the value of the activityOccurrenceComponentsSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activityOccurrenceComponentsSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivityOccurrenceComponentsSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceSpecificationTO }
     * 
     * 
     */
    public List<ActivityOccurrenceSpecificationTO> getActivityOccurrenceComponentsSpecifications() {
        if (activityOccurrenceComponentsSpecifications == null) {
            activityOccurrenceComponentsSpecifications = new ArrayList<ActivityOccurrenceSpecificationTO>();
        }
        return this.activityOccurrenceComponentsSpecifications;
    }

    /**
     * Gets the value of the activityOccurencePredecessorsSpecification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activityOccurencePredecessorsSpecification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivityOccurencePredecessorsSpecification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceSpecificationTO }
     * 
     * 
     */
    public List<ActivityOccurrenceSpecificationTO> getActivityOccurencePredecessorsSpecification() {
        if (activityOccurencePredecessorsSpecification == null) {
            activityOccurencePredecessorsSpecification = new ArrayList<ActivityOccurrenceSpecificationTO>();
        }
        return this.activityOccurencePredecessorsSpecification;
    }

    /**
     * Gets the value of the activityOccurenceSuccessorsSpecification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activityOccurenceSuccessorsSpecification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivityOccurenceSuccessorsSpecification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceSpecificationTO }
     * 
     * 
     */
    public List<ActivityOccurrenceSpecificationTO> getActivityOccurenceSuccessorsSpecification() {
        if (activityOccurenceSuccessorsSpecification == null) {
            activityOccurenceSuccessorsSpecification = new ArrayList<ActivityOccurrenceSpecificationTO>();
        }
        return this.activityOccurenceSuccessorsSpecification;
    }

    /**
     * Gets the value of the compositeActivityOccurrencesSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the compositeActivityOccurrencesSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCompositeActivityOccurrencesSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceSpecificationTO }
     * 
     * 
     */
    public List<ActivityOccurrenceSpecificationTO> getCompositeActivityOccurrencesSpecifications() {
        if (compositeActivityOccurrencesSpecifications == null) {
            compositeActivityOccurrencesSpecifications = new ArrayList<ActivityOccurrenceSpecificationTO>();
        }
        return this.compositeActivityOccurrencesSpecifications;
    }

    /**
     * Sets the value of the activityOccurrenceComponentsSpecifications property.
     * 
     * @param activityOccurrenceComponentsSpecifications
     *     allowed object is
     *     {@link ActivityOccurrenceSpecificationTO }
     *     
     */
    public void setActivityOccurrenceComponentsSpecifications(List<ActivityOccurrenceSpecificationTO> activityOccurrenceComponentsSpecifications) {
        this.activityOccurrenceComponentsSpecifications = activityOccurrenceComponentsSpecifications;
    }

    /**
     * Sets the value of the activityOccurencePredecessorsSpecification property.
     * 
     * @param activityOccurencePredecessorsSpecification
     *     allowed object is
     *     {@link ActivityOccurrenceSpecificationTO }
     *     
     */
    public void setActivityOccurencePredecessorsSpecification(List<ActivityOccurrenceSpecificationTO> activityOccurencePredecessorsSpecification) {
        this.activityOccurencePredecessorsSpecification = activityOccurencePredecessorsSpecification;
    }

    /**
     * Sets the value of the activityOccurenceSuccessorsSpecification property.
     * 
     * @param activityOccurenceSuccessorsSpecification
     *     allowed object is
     *     {@link ActivityOccurrenceSpecificationTO }
     *     
     */
    public void setActivityOccurenceSuccessorsSpecification(List<ActivityOccurrenceSpecificationTO> activityOccurenceSuccessorsSpecification) {
        this.activityOccurenceSuccessorsSpecification = activityOccurenceSuccessorsSpecification;
    }

    /**
     * Sets the value of the compositeActivityOccurrencesSpecifications property.
     * 
     * @param compositeActivityOccurrencesSpecifications
     *     allowed object is
     *     {@link ActivityOccurrenceSpecificationTO }
     *     
     */
    public void setCompositeActivityOccurrencesSpecifications(List<ActivityOccurrenceSpecificationTO> compositeActivityOccurrencesSpecifications) {
        this.compositeActivityOccurrencesSpecifications = compositeActivityOccurrencesSpecifications;
    }

}
