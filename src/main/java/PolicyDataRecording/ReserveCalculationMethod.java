
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReserveCalculationMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReserveCalculationMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Conventional"/>
 *     &lt;enumeration value="Zillmerised"/>
 *     &lt;enumeration value="Pseudo Premium"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReserveCalculationMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum ReserveCalculationMethod {

    @XmlEnumValue("Conventional")
    CONVENTIONAL("Conventional"),
    @XmlEnumValue("Zillmerised")
    ZILLMERISED("Zillmerised"),
    @XmlEnumValue("Pseudo Premium")
    PSEUDO_PREMIUM("Pseudo Premium");
    private final String value;

    ReserveCalculationMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReserveCalculationMethod fromValue(String v) {
        for (ReserveCalculationMethod c: ReserveCalculationMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
