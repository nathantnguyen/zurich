
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyRoleInLegalAction_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyRoleInLegalAction_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRole_TO">
 *       &lt;sequence>
 *         &lt;element name="isContextFor" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/DisputeResolution/}LegalAction_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyRoleInLegalAction_TO", propOrder = {
    "isContextFor"
})
public class PartyRoleInLegalActionTO
    extends PartyRoleTO
{

    protected LegalActionTO isContextFor;

    /**
     * Gets the value of the isContextFor property.
     * 
     * @return
     *     possible object is
     *     {@link LegalActionTO }
     *     
     */
    public LegalActionTO getIsContextFor() {
        return isContextFor;
    }

    /**
     * Sets the value of the isContextFor property.
     * 
     * @param value
     *     allowed object is
     *     {@link LegalActionTO }
     *     
     */
    public void setIsContextFor(LegalActionTO value) {
        this.isContextFor = value;
    }

}
