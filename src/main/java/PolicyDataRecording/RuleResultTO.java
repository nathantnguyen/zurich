
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RuleResult_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RuleResult_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="originalValue" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}TriStateLogic" minOccurs="0"/>
 *         &lt;element name="overriddenValue" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}TriStateLogic" minOccurs="0"/>
 *         &lt;element name="isOverridable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="definitionContext" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}StructuredActual_TO" minOccurs="0"/>
 *         &lt;element name="expressionResult" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ExpressionResult_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleResult_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "kind",
    "description",
    "originalValue",
    "overriddenValue",
    "isOverridable",
    "definitionContext",
    "expressionResult"
})
public class RuleResultTO
    extends BaseTransferObject
{

    protected String kind;
    protected String description;
    protected TriStateLogic originalValue;
    protected TriStateLogic overriddenValue;
    protected Boolean isOverridable;
    protected StructuredActualTO definitionContext;
    protected List<ExpressionResultTO> expressionResult;

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the originalValue property.
     * 
     * @return
     *     possible object is
     *     {@link TriStateLogic }
     *     
     */
    public TriStateLogic getOriginalValue() {
        return originalValue;
    }

    /**
     * Sets the value of the originalValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link TriStateLogic }
     *     
     */
    public void setOriginalValue(TriStateLogic value) {
        this.originalValue = value;
    }

    /**
     * Gets the value of the overriddenValue property.
     * 
     * @return
     *     possible object is
     *     {@link TriStateLogic }
     *     
     */
    public TriStateLogic getOverriddenValue() {
        return overriddenValue;
    }

    /**
     * Sets the value of the overriddenValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link TriStateLogic }
     *     
     */
    public void setOverriddenValue(TriStateLogic value) {
        this.overriddenValue = value;
    }

    /**
     * Gets the value of the isOverridable property.
     * This getter has been renamed from isIsOverridable() to getIsOverridable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOverridable() {
        return isOverridable;
    }

    /**
     * Sets the value of the isOverridable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOverridable(Boolean value) {
        this.isOverridable = value;
    }

    /**
     * Gets the value of the definitionContext property.
     * 
     * @return
     *     possible object is
     *     {@link StructuredActualTO }
     *     
     */
    public StructuredActualTO getDefinitionContext() {
        return definitionContext;
    }

    /**
     * Sets the value of the definitionContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructuredActualTO }
     *     
     */
    public void setDefinitionContext(StructuredActualTO value) {
        this.definitionContext = value;
    }

    /**
     * Gets the value of the expressionResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expressionResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpressionResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExpressionResultTO }
     * 
     * 
     */
    public List<ExpressionResultTO> getExpressionResult() {
        if (expressionResult == null) {
            expressionResult = new ArrayList<ExpressionResultTO>();
        }
        return this.expressionResult;
    }

    /**
     * Sets the value of the expressionResult property.
     * 
     * @param expressionResult
     *     allowed object is
     *     {@link ExpressionResultTO }
     *     
     */
    public void setExpressionResult(List<ExpressionResultTO> expressionResult) {
        this.expressionResult = expressionResult;
    }

}
