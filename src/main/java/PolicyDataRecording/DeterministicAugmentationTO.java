
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeterministicAugmentation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeterministicAugmentation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}AlgorithmicAugmentation_TO">
 *       &lt;sequence>
 *         &lt;element name="matchRelevancy" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="matchRelevancyScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="nonMatchRelevancy" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="nonMatchRelevancyScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeterministicAugmentation_TO", propOrder = {
    "matchRelevancy",
    "matchRelevancyScore",
    "nonMatchRelevancy",
    "nonMatchRelevancyScore"
})
public class DeterministicAugmentationTO
    extends AlgorithmicAugmentationTO
{

    protected String matchRelevancy;
    protected BigDecimal matchRelevancyScore;
    protected String nonMatchRelevancy;
    protected String nonMatchRelevancyScore;

    /**
     * Gets the value of the matchRelevancy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchRelevancy() {
        return matchRelevancy;
    }

    /**
     * Sets the value of the matchRelevancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchRelevancy(String value) {
        this.matchRelevancy = value;
    }

    /**
     * Gets the value of the matchRelevancyScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMatchRelevancyScore() {
        return matchRelevancyScore;
    }

    /**
     * Sets the value of the matchRelevancyScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMatchRelevancyScore(BigDecimal value) {
        this.matchRelevancyScore = value;
    }

    /**
     * Gets the value of the nonMatchRelevancy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonMatchRelevancy() {
        return nonMatchRelevancy;
    }

    /**
     * Sets the value of the nonMatchRelevancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonMatchRelevancy(String value) {
        this.nonMatchRelevancy = value;
    }

    /**
     * Gets the value of the nonMatchRelevancyScore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonMatchRelevancyScore() {
        return nonMatchRelevancyScore;
    }

    /**
     * Sets the value of the nonMatchRelevancyScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonMatchRelevancyScore(String value) {
        this.nonMatchRelevancyScore = value;
    }

}
