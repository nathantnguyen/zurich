
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Account_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Account_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="openingDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="closingDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="accountEntries" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AccountEntry_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="accountStatus" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AccountStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInAccount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}RoleInAccount_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="accountRelationships" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AccountRelationship_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="owners" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Party_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="financialInstitution" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Company_TO" minOccurs="0"/>
 *         &lt;element name="ownersName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ownersExternalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ownersReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="allocatedFund" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Fund_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Account_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "name",
    "openingDate",
    "closingDate",
    "description",
    "externalReference",
    "accountEntries",
    "accountStatus",
    "rolesInAccount",
    "accountRelationships",
    "owners",
    "financialInstitution",
    "ownersName",
    "ownersExternalReference",
    "ownersReference",
    "allocatedFund",
    "alternateReference"
})
@XmlSeeAlso({
    TimeAccountTO.class,
    MonetaryAccountTO.class,
    AssetHoldingTO.class
})
public class AccountTO
    extends BusinessModelObjectTO
{

    protected String name;
    protected XMLGregorianCalendar openingDate;
    protected XMLGregorianCalendar closingDate;
    protected String description;
    protected String externalReference;
    protected List<AccountEntryTO> accountEntries;
    protected List<AccountStatusTO> accountStatus;
    protected List<RoleInAccountTO> rolesInAccount;
    protected List<AccountRelationshipTO> accountRelationships;
    protected List<PartyTO> owners;
    protected CompanyTO financialInstitution;
    protected List<String> ownersName;
    protected List<String> ownersExternalReference;
    protected List<ObjectReferenceTO> ownersReference;
    protected List<FundTO> allocatedFund;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the openingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOpeningDate() {
        return openingDate;
    }

    /**
     * Sets the value of the openingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOpeningDate(XMLGregorianCalendar value) {
        this.openingDate = value;
    }

    /**
     * Gets the value of the closingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClosingDate() {
        return closingDate;
    }

    /**
     * Sets the value of the closingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClosingDate(XMLGregorianCalendar value) {
        this.closingDate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the accountEntries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountEntries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountEntries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountEntryTO }
     * 
     * 
     */
    public List<AccountEntryTO> getAccountEntries() {
        if (accountEntries == null) {
            accountEntries = new ArrayList<AccountEntryTO>();
        }
        return this.accountEntries;
    }

    /**
     * Gets the value of the accountStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountStatusTO }
     * 
     * 
     */
    public List<AccountStatusTO> getAccountStatus() {
        if (accountStatus == null) {
            accountStatus = new ArrayList<AccountStatusTO>();
        }
        return this.accountStatus;
    }

    /**
     * Gets the value of the rolesInAccount property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInAccount property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInAccountTO }
     * 
     * 
     */
    public List<RoleInAccountTO> getRolesInAccount() {
        if (rolesInAccount == null) {
            rolesInAccount = new ArrayList<RoleInAccountTO>();
        }
        return this.rolesInAccount;
    }

    /**
     * Gets the value of the accountRelationships property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountRelationships property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountRelationships().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountRelationshipTO }
     * 
     * 
     */
    public List<AccountRelationshipTO> getAccountRelationships() {
        if (accountRelationships == null) {
            accountRelationships = new ArrayList<AccountRelationshipTO>();
        }
        return this.accountRelationships;
    }

    /**
     * Gets the value of the owners property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the owners property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOwners().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyTO }
     * 
     * 
     */
    public List<PartyTO> getOwners() {
        if (owners == null) {
            owners = new ArrayList<PartyTO>();
        }
        return this.owners;
    }

    /**
     * Gets the value of the financialInstitution property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyTO }
     *     
     */
    public CompanyTO getFinancialInstitution() {
        return financialInstitution;
    }

    /**
     * Sets the value of the financialInstitution property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyTO }
     *     
     */
    public void setFinancialInstitution(CompanyTO value) {
        this.financialInstitution = value;
    }

    /**
     * Gets the value of the ownersName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ownersName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOwnersName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOwnersName() {
        if (ownersName == null) {
            ownersName = new ArrayList<String>();
        }
        return this.ownersName;
    }

    /**
     * Gets the value of the ownersExternalReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ownersExternalReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOwnersExternalReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOwnersExternalReference() {
        if (ownersExternalReference == null) {
            ownersExternalReference = new ArrayList<String>();
        }
        return this.ownersExternalReference;
    }

    /**
     * Gets the value of the ownersReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ownersReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOwnersReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getOwnersReference() {
        if (ownersReference == null) {
            ownersReference = new ArrayList<ObjectReferenceTO>();
        }
        return this.ownersReference;
    }

    /**
     * Gets the value of the allocatedFund property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allocatedFund property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocatedFund().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundTO }
     * 
     * 
     */
    public List<FundTO> getAllocatedFund() {
        if (allocatedFund == null) {
            allocatedFund = new ArrayList<FundTO>();
        }
        return this.allocatedFund;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the accountEntries property.
     * 
     * @param accountEntries
     *     allowed object is
     *     {@link AccountEntryTO }
     *     
     */
    public void setAccountEntries(List<AccountEntryTO> accountEntries) {
        this.accountEntries = accountEntries;
    }

    /**
     * Sets the value of the accountStatus property.
     * 
     * @param accountStatus
     *     allowed object is
     *     {@link AccountStatusTO }
     *     
     */
    public void setAccountStatus(List<AccountStatusTO> accountStatus) {
        this.accountStatus = accountStatus;
    }

    /**
     * Sets the value of the rolesInAccount property.
     * 
     * @param rolesInAccount
     *     allowed object is
     *     {@link RoleInAccountTO }
     *     
     */
    public void setRolesInAccount(List<RoleInAccountTO> rolesInAccount) {
        this.rolesInAccount = rolesInAccount;
    }

    /**
     * Sets the value of the accountRelationships property.
     * 
     * @param accountRelationships
     *     allowed object is
     *     {@link AccountRelationshipTO }
     *     
     */
    public void setAccountRelationships(List<AccountRelationshipTO> accountRelationships) {
        this.accountRelationships = accountRelationships;
    }

    /**
     * Sets the value of the owners property.
     * 
     * @param owners
     *     allowed object is
     *     {@link PartyTO }
     *     
     */
    public void setOwners(List<PartyTO> owners) {
        this.owners = owners;
    }

    /**
     * Sets the value of the ownersName property.
     * 
     * @param ownersName
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnersName(List<String> ownersName) {
        this.ownersName = ownersName;
    }

    /**
     * Sets the value of the ownersExternalReference property.
     * 
     * @param ownersExternalReference
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnersExternalReference(List<String> ownersExternalReference) {
        this.ownersExternalReference = ownersExternalReference;
    }

    /**
     * Sets the value of the ownersReference property.
     * 
     * @param ownersReference
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setOwnersReference(List<ObjectReferenceTO> ownersReference) {
        this.ownersReference = ownersReference;
    }

    /**
     * Sets the value of the allocatedFund property.
     * 
     * @param allocatedFund
     *     allowed object is
     *     {@link FundTO }
     *     
     */
    public void setAllocatedFund(List<FundTO> allocatedFund) {
        this.allocatedFund = allocatedFund;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
