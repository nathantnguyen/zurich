
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InterestRate_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InterestRate_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Rate_TO">
 *       &lt;sequence>
 *         &lt;element name="compoundingFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}InterestCompoundingFrequency" minOccurs="0"/>
 *         &lt;element name="postingFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}InterestPostingFrequency" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}InterestRateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterestRate_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/", propOrder = {
    "compoundingFrequency",
    "postingFrequency",
    "type"
})
@XmlSeeAlso({
    CostOfCapitalTO.class,
    RiskFreeInterestRateTO.class
})
public class InterestRateTO
    extends RateTO
{

    protected InterestCompoundingFrequency compoundingFrequency;
    protected InterestPostingFrequency postingFrequency;
    protected InterestRateType type;

    /**
     * Gets the value of the compoundingFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link InterestCompoundingFrequency }
     *     
     */
    public InterestCompoundingFrequency getCompoundingFrequency() {
        return compoundingFrequency;
    }

    /**
     * Sets the value of the compoundingFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestCompoundingFrequency }
     *     
     */
    public void setCompoundingFrequency(InterestCompoundingFrequency value) {
        this.compoundingFrequency = value;
    }

    /**
     * Gets the value of the postingFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link InterestPostingFrequency }
     *     
     */
    public InterestPostingFrequency getPostingFrequency() {
        return postingFrequency;
    }

    /**
     * Sets the value of the postingFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestPostingFrequency }
     *     
     */
    public void setPostingFrequency(InterestPostingFrequency value) {
        this.postingFrequency = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link InterestRateType }
     *     
     */
    public InterestRateType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestRateType }
     *     
     */
    public void setType(InterestRateType value) {
        this.type = value;
    }

}
