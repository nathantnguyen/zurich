
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaskSpecification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaskSpecification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrenceSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="capitalExpense" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="plannedManpower" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="compositeProcedures" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}Procedure_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskSpecification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/", propOrder = {
    "capitalExpense",
    "plannedManpower",
    "compositeProcedures"
})
@XmlSeeAlso({
    ProcedureTO.class
})
public class TaskSpecificationTO
    extends ActivityOccurrenceSpecificationTO
{

    protected BaseCurrencyAmount capitalExpense;
    protected Amount plannedManpower;
    protected List<ProcedureTO> compositeProcedures;

    /**
     * Gets the value of the capitalExpense property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCapitalExpense() {
        return capitalExpense;
    }

    /**
     * Sets the value of the capitalExpense property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCapitalExpense(BaseCurrencyAmount value) {
        this.capitalExpense = value;
    }

    /**
     * Gets the value of the plannedManpower property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getPlannedManpower() {
        return plannedManpower;
    }

    /**
     * Sets the value of the plannedManpower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setPlannedManpower(Amount value) {
        this.plannedManpower = value;
    }

    /**
     * Gets the value of the compositeProcedures property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the compositeProcedures property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCompositeProcedures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcedureTO }
     * 
     * 
     */
    public List<ProcedureTO> getCompositeProcedures() {
        if (compositeProcedures == null) {
            compositeProcedures = new ArrayList<ProcedureTO>();
        }
        return this.compositeProcedures;
    }

    /**
     * Sets the value of the compositeProcedures property.
     * 
     * @param compositeProcedures
     *     allowed object is
     *     {@link ProcedureTO }
     *     
     */
    public void setCompositeProcedures(List<ProcedureTO> compositeProcedures) {
        this.compositeProcedures = compositeProcedures;
    }

}
