
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanPurpose.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LoanPurpose">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Cash Funding Or Clean Credit"/>
 *     &lt;enumeration value="Purchase Of Consumer Goods Or Consumer Credit"/>
 *     &lt;enumeration value="Temporary Funding"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LoanPurpose", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum LoanPurpose {

    @XmlEnumValue("Cash Funding Or Clean Credit")
    CASH_FUNDING_OR_CLEAN_CREDIT("Cash Funding Or Clean Credit"),
    @XmlEnumValue("Purchase Of Consumer Goods Or Consumer Credit")
    PURCHASE_OF_CONSUMER_GOODS_OR_CONSUMER_CREDIT("Purchase Of Consumer Goods Or Consumer Credit"),
    @XmlEnumValue("Temporary Funding")
    TEMPORARY_FUNDING("Temporary Funding");
    private final String value;

    LoanPurpose(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LoanPurpose fromValue(String v) {
        for (LoanPurpose c: LoanPurpose.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
