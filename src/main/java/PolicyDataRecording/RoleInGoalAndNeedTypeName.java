
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInGoalAndNeedTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInGoalAndNeedTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Financial Target"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInGoalAndNeedTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum RoleInGoalAndNeedTypeName {

    @XmlEnumValue("Financial Target")
    FINANCIAL_TARGET("Financial Target");
    private final String value;

    RoleInGoalAndNeedTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInGoalAndNeedTypeName fromValue(String v) {
        for (RoleInGoalAndNeedTypeName c: RoleInGoalAndNeedTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
