
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialTransactionCard_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialTransactionCard_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}FinancialTransactionMedium_TO">
 *       &lt;sequence>
 *         &lt;element name="pin" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="brand" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}FinancialTransactionCardBrand" minOccurs="0"/>
 *         &lt;element name="tracks" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Binary" minOccurs="0"/>
 *         &lt;element name="holderName" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyName_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialTransactionCard_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "pin",
    "brand",
    "tracks",
    "holderName"
})
public class FinancialTransactionCardTO
    extends FinancialTransactionMediumTO
{

    protected String pin;
    protected FinancialTransactionCardBrand brand;
    protected Binary tracks;
    protected PartyNameTO holderName;

    /**
     * Gets the value of the pin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPin() {
        return pin;
    }

    /**
     * Sets the value of the pin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPin(String value) {
        this.pin = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialTransactionCardBrand }
     *     
     */
    public FinancialTransactionCardBrand getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialTransactionCardBrand }
     *     
     */
    public void setBrand(FinancialTransactionCardBrand value) {
        this.brand = value;
    }

    /**
     * Gets the value of the tracks property.
     * 
     * @return
     *     possible object is
     *     {@link Binary }
     *     
     */
    public Binary getTracks() {
        return tracks;
    }

    /**
     * Sets the value of the tracks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Binary }
     *     
     */
    public void setTracks(Binary value) {
        this.tracks = value;
    }

    /**
     * Gets the value of the holderName property.
     * 
     * @return
     *     possible object is
     *     {@link PartyNameTO }
     *     
     */
    public PartyNameTO getHolderName() {
        return holderName;
    }

    /**
     * Sets the value of the holderName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyNameTO }
     *     
     */
    public void setHolderName(PartyNameTO value) {
        this.holderName = value;
    }

}
