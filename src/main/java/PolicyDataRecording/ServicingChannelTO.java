
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServicingChannel_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServicingChannel_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRole_TO">
 *       &lt;sequence>
 *         &lt;element name="intermediaryAgreements" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}IntermediaryAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServicingChannel_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "intermediaryAgreements"
})
@XmlSeeAlso({
    BillerTO.class
})
public class ServicingChannelTO
    extends FinancialServicesRoleTO
{

    protected List<IntermediaryAgreementTO> intermediaryAgreements;

    /**
     * Gets the value of the intermediaryAgreements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intermediaryAgreements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntermediaryAgreements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntermediaryAgreementTO }
     * 
     * 
     */
    public List<IntermediaryAgreementTO> getIntermediaryAgreements() {
        if (intermediaryAgreements == null) {
            intermediaryAgreements = new ArrayList<IntermediaryAgreementTO>();
        }
        return this.intermediaryAgreements;
    }

    /**
     * Sets the value of the intermediaryAgreements property.
     * 
     * @param intermediaryAgreements
     *     allowed object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public void setIntermediaryAgreements(List<IntermediaryAgreementTO> intermediaryAgreements) {
        this.intermediaryAgreements = intermediaryAgreements;
    }

}
