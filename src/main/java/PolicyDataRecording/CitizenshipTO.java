
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Citizenship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Citizenship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}NationalRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="primaryResidence" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Citizenship_TO", propOrder = {
    "primaryResidence"
})
public class CitizenshipTO
    extends NationalRegistrationTO
{

    protected Boolean primaryResidence;

    /**
     * Gets the value of the primaryResidence property.
     * This getter has been renamed from isPrimaryResidence() to getPrimaryResidence() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPrimaryResidence() {
        return primaryResidence;
    }

    /**
     * Sets the value of the primaryResidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimaryResidence(Boolean value) {
        this.primaryResidence = value;
    }

}
