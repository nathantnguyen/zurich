
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for contractspecificationcomposition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="contractspecificationcomposition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="maximumNumberOfAgreements" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="minimumNumberOfAgreements" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contractspecificationcomposition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "maximumNumberOfAgreements",
    "minimumNumberOfAgreements"
})
@XmlSeeAlso({
    FinancialservicesproductcompositionTO.class
})
public class ContractspecificationcompositionTO
    extends BaseTransferObject
{

    protected BigInteger maximumNumberOfAgreements;
    protected BigInteger minimumNumberOfAgreements;

    /**
     * Gets the value of the maximumNumberOfAgreements property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumNumberOfAgreements() {
        return maximumNumberOfAgreements;
    }

    /**
     * Sets the value of the maximumNumberOfAgreements property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumNumberOfAgreements(BigInteger value) {
        this.maximumNumberOfAgreements = value;
    }

    /**
     * Gets the value of the minimumNumberOfAgreements property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumNumberOfAgreements() {
        return minimumNumberOfAgreements;
    }

    /**
     * Sets the value of the minimumNumberOfAgreements property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumNumberOfAgreements(BigInteger value) {
        this.minimumNumberOfAgreements = value;
    }

}
