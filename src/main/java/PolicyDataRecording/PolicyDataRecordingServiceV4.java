package PolicyDataRecording;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * [serviceVersion='4.13']
 *
 * This class was generated by Apache CXF 2.7.18
 * 2017-08-22T23:19:13.562-04:00
 * Generated source version: 2.7.18
 * 
 */
@WebServiceClient(name = "PolicyDataRecordingService_v4", 
                  wsdlLocation = "file:/Users/nathannguyen/AnypointStudio/workspace/zurich/src/main/wsdl/PolicyDataRecording.wsdl",
                  targetNamespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/PolicyAcquisition/") 
public class PolicyDataRecordingServiceV4 extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/PolicyAcquisition/", "PolicyDataRecordingService_v4");
    public final static QName IPolicyDataRecordingV4Port = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/PolicyAcquisition/", "IPolicyDataRecording_v4Port");
    static {
        URL url = null;
        try {
            url = new URL("file:/Users/nathannguyen/AnypointStudio/workspace/zurich/src/main/wsdl/PolicyDataRecording.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(PolicyDataRecordingServiceV4.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/Users/nathannguyen/AnypointStudio/workspace/zurich/src/main/wsdl/PolicyDataRecording.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public PolicyDataRecordingServiceV4(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public PolicyDataRecordingServiceV4(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public PolicyDataRecordingServiceV4() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public PolicyDataRecordingServiceV4(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public PolicyDataRecordingServiceV4(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public PolicyDataRecordingServiceV4(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns IPolicyDataRecordingV4
     */
    @WebEndpoint(name = "IPolicyDataRecording_v4Port")
    public IPolicyDataRecordingV4 getIPolicyDataRecordingV4Port() {
        return super.getPort(IPolicyDataRecordingV4Port, IPolicyDataRecordingV4.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IPolicyDataRecordingV4
     */
    @WebEndpoint(name = "IPolicyDataRecording_v4Port")
    public IPolicyDataRecordingV4 getIPolicyDataRecordingV4Port(WebServiceFeature... features) {
        return super.getPort(IPolicyDataRecordingV4Port, IPolicyDataRecordingV4.class, features);
    }

}
