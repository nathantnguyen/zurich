
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SoundAmplification.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SoundAmplification">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="High"/>
 *     &lt;enumeration value="High Frequency Only"/>
 *     &lt;enumeration value="Low"/>
 *     &lt;enumeration value="Low Frequency Only"/>
 *     &lt;enumeration value="Medium"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SoundAmplification", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum SoundAmplification {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("High")
    HIGH("High"),
    @XmlEnumValue("High Frequency Only")
    HIGH_FREQUENCY_ONLY("High Frequency Only"),
    @XmlEnumValue("Low")
    LOW("Low"),
    @XmlEnumValue("Low Frequency Only")
    LOW_FREQUENCY_ONLY("Low Frequency Only"),
    @XmlEnumValue("Medium")
    MEDIUM("Medium");
    private final String value;

    SoundAmplification(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SoundAmplification fromValue(String v) {
        for (SoundAmplification c: SoundAmplification.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
