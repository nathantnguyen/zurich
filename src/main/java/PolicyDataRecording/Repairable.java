
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Repairable.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Repairable">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Economically Not Repairable"/>
 *     &lt;enumeration value="Physically Not Repairable"/>
 *     &lt;enumeration value="Repairable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Repairable", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum Repairable {

    @XmlEnumValue("Economically Not Repairable")
    ECONOMICALLY_NOT_REPAIRABLE("Economically Not Repairable"),
    @XmlEnumValue("Physically Not Repairable")
    PHYSICALLY_NOT_REPAIRABLE("Physically Not Repairable"),
    @XmlEnumValue("Repairable")
    REPAIRABLE("Repairable");
    private final String value;

    Repairable(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Repairable fromValue(String v) {
        for (Repairable c: Repairable.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
