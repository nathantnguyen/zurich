
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerOfProductGroupState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerOfProductGroupState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Active"/>
 *     &lt;enumeration value="Disqualified"/>
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="Former"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Not Interested"/>
 *     &lt;enumeration value="Potential"/>
 *     &lt;enumeration value="Prospect"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustomerOfProductGroupState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum CustomerOfProductGroupState {

    @XmlEnumValue("Active")
    ACTIVE("Active"),
    @XmlEnumValue("Disqualified")
    DISQUALIFIED("Disqualified"),
    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("Former")
    FORMER("Former"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Not Interested")
    NOT_INTERESTED("Not Interested"),
    @XmlEnumValue("Potential")
    POTENTIAL("Potential"),
    @XmlEnumValue("Prospect")
    PROSPECT("Prospect");
    private final String value;

    CustomerOfProductGroupState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomerOfProductGroupState fromValue(String v) {
        for (CustomerOfProductGroupState c: CustomerOfProductGroupState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
