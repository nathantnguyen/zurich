
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardCreditLevelParameterName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StandardCreditLevelParameterName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Market Risk Concentration Parameter G"/>
 *     &lt;enumeration value="Credit Quality Step"/>
 *     &lt;enumeration value="Market Credit Spread Risk Ffactor"/>
 *     &lt;enumeration value="Market Credit Spread Risk Gfactor"/>
 *     &lt;enumeration value="Market Risk Concentration Threshold Ct"/>
 *     &lt;enumeration value="Probability Of Default"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StandardCreditLevelParameterName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/")
@XmlEnum
public enum StandardCreditLevelParameterName {

    @XmlEnumValue("Market Risk Concentration Parameter G")
    MARKET_RISK_CONCENTRATION_PARAMETER_G("Market Risk Concentration Parameter G"),
    @XmlEnumValue("Credit Quality Step")
    CREDIT_QUALITY_STEP("Credit Quality Step"),
    @XmlEnumValue("Market Credit Spread Risk Ffactor")
    MARKET_CREDIT_SPREAD_RISK_FFACTOR("Market Credit Spread Risk Ffactor"),
    @XmlEnumValue("Market Credit Spread Risk Gfactor")
    MARKET_CREDIT_SPREAD_RISK_GFACTOR("Market Credit Spread Risk Gfactor"),
    @XmlEnumValue("Market Risk Concentration Threshold Ct")
    MARKET_RISK_CONCENTRATION_THRESHOLD_CT("Market Risk Concentration Threshold Ct"),
    @XmlEnumValue("Probability Of Default")
    PROBABILITY_OF_DEFAULT("Probability Of Default");
    private final String value;

    StandardCreditLevelParameterName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StandardCreditLevelParameterName fromValue(String v) {
        for (StandardCreditLevelParameterName c: StandardCreditLevelParameterName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
