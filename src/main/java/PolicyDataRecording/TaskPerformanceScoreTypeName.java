
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaskPerformanceScoreTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TaskPerformanceScoreTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Control System Performance Score"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TaskPerformanceScoreTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/UtilityComponents/TaskManager/TaskManagerEnumerationsAndStates/")
@XmlEnum
public enum TaskPerformanceScoreTypeName {

    @XmlEnumValue("Control System Performance Score")
    CONTROL_SYSTEM_PERFORMANCE_SCORE("Control System Performance Score");
    private final String value;

    TaskPerformanceScoreTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TaskPerformanceScoreTypeName fromValue(String v) {
        for (TaskPerformanceScoreTypeName c: TaskPerformanceScoreTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
