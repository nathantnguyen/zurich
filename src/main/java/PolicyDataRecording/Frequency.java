
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Frequency.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Frequency">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Daily"/>
 *     &lt;enumeration value="Irregular"/>
 *     &lt;enumeration value="Sporadic"/>
 *     &lt;enumeration value="Monthly"/>
 *     &lt;enumeration value="Once"/>
 *     &lt;enumeration value="Quarterly"/>
 *     &lt;enumeration value="Yearly"/>
 *     &lt;enumeration value="Continuous"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Frequency", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/")
@XmlEnum
public enum Frequency {

    @XmlEnumValue("Daily")
    DAILY("Daily"),
    @XmlEnumValue("Irregular")
    IRREGULAR("Irregular"),
    @XmlEnumValue("Sporadic")
    SPORADIC("Sporadic"),
    @XmlEnumValue("Monthly")
    MONTHLY("Monthly"),
    @XmlEnumValue("Once")
    ONCE("Once"),
    @XmlEnumValue("Quarterly")
    QUARTERLY("Quarterly"),
    @XmlEnumValue("Yearly")
    YEARLY("Yearly"),
    @XmlEnumValue("Continuous")
    CONTINUOUS("Continuous");
    private final String value;

    Frequency(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Frequency fromValue(String v) {
        for (Frequency c: Frequency.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
