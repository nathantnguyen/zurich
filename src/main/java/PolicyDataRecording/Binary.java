
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Binary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Binary">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Value">
 *       &lt;sequence>
 *         &lt;element name="theContentType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="theBinData" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Byte" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="theBinLength" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Binary", namespace = "http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/", propOrder = {
    "theContentType",
    "theBinData",
    "theBinLength"
})
public class Binary
    extends Value
{

    protected String theContentType;
    @XmlElement(type = Byte.class)
    protected List<Byte> theBinData;
    protected BigInteger theBinLength;

    /**
     * Gets the value of the theContentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheContentType() {
        return theContentType;
    }

    /**
     * Sets the value of the theContentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheContentType(String value) {
        this.theContentType = value;
    }

    /**
     * Gets the value of the theBinData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the theBinData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTheBinData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Byte }
     * 
     * 
     */
    public List<Byte> getTheBinData() {
        if (theBinData == null) {
            theBinData = new ArrayList<Byte>();
        }
        return this.theBinData;
    }

    /**
     * Gets the value of the theBinLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTheBinLength() {
        return theBinLength;
    }

    /**
     * Sets the value of the theBinLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTheBinLength(BigInteger value) {
        this.theBinLength = value;
    }

    /**
     * Sets the value of the theBinData property.
     * 
     * @param theBinData
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setTheBinData(List<Byte> theBinData) {
        this.theBinData = theBinData;
    }

}
