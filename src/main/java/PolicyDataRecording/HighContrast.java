
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HighContrast.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HighContrast">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Black On White"/>
 *     &lt;enumeration value="Maximum Contrast Monochrome"/>
 *     &lt;enumeration value="White On Black"/>
 *     &lt;enumeration value="Yellow On Blue"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HighContrast", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum HighContrast {

    @XmlEnumValue("Black On White")
    BLACK_ON_WHITE("Black On White"),
    @XmlEnumValue("Maximum Contrast Monochrome")
    MAXIMUM_CONTRAST_MONOCHROME("Maximum Contrast Monochrome"),
    @XmlEnumValue("White On Black")
    WHITE_ON_BLACK("White On Black"),
    @XmlEnumValue("Yellow On Blue")
    YELLOW_ON_BLUE("Yellow On Blue");
    private final String value;

    HighContrast(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HighContrast fromValue(String v) {
        for (HighContrast c: HighContrast.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
