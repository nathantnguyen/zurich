
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PurchasingPower.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PurchasingPower">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Affluent"/>
 *     &lt;enumeration value="Government Assistance"/>
 *     &lt;enumeration value="Low Income"/>
 *     &lt;enumeration value="Upper Middle Income"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PurchasingPower", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum PurchasingPower {

    @XmlEnumValue("Affluent")
    AFFLUENT("Affluent"),
    @XmlEnumValue("Government Assistance")
    GOVERNMENT_ASSISTANCE("Government Assistance"),
    @XmlEnumValue("Low Income")
    LOW_INCOME("Low Income"),
    @XmlEnumValue("Upper Middle Income")
    UPPER_MIDDLE_INCOME("Upper Middle Income");
    private final String value;

    PurchasingPower(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PurchasingPower fromValue(String v) {
        for (PurchasingPower c: PurchasingPower.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
