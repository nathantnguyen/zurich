
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssetPriceQuotation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssetPriceQuotation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AssetPrice_TO">
 *       &lt;sequence>
 *         &lt;element name="priceConversionRatio" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="tickSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="tickValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssetPriceQuotation_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "priceConversionRatio",
    "tickSize",
    "tickValue"
})
public class AssetPriceQuotationTO
    extends AssetPriceTO
{

    protected BigInteger priceConversionRatio;
    protected BigDecimal tickSize;
    protected BaseCurrencyAmount tickValue;

    /**
     * Gets the value of the priceConversionRatio property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPriceConversionRatio() {
        return priceConversionRatio;
    }

    /**
     * Sets the value of the priceConversionRatio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPriceConversionRatio(BigInteger value) {
        this.priceConversionRatio = value;
    }

    /**
     * Gets the value of the tickSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTickSize() {
        return tickSize;
    }

    /**
     * Sets the value of the tickSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTickSize(BigDecimal value) {
        this.tickSize = value;
    }

    /**
     * Gets the value of the tickValue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTickValue() {
        return tickValue;
    }

    /**
     * Sets the value of the tickValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTickValue(BaseCurrencyAmount value) {
        this.tickValue = value;
    }

}
