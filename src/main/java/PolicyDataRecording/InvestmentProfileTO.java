
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvestmentProfile_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvestmentProfile_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}MoneyProvisionDeterminer_TO">
 *       &lt;sequence>
 *         &lt;element name="investmentProfileName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="fundsApplyingProfile" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Fund_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvestmentProfile_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "investmentProfileName",
    "fundsApplyingProfile"
})
public class InvestmentProfileTO
    extends MoneyProvisionDeterminerTO
{

    protected String investmentProfileName;
    protected List<FundTO> fundsApplyingProfile;

    /**
     * Gets the value of the investmentProfileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvestmentProfileName() {
        return investmentProfileName;
    }

    /**
     * Sets the value of the investmentProfileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvestmentProfileName(String value) {
        this.investmentProfileName = value;
    }

    /**
     * Gets the value of the fundsApplyingProfile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fundsApplyingProfile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFundsApplyingProfile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FundTO }
     * 
     * 
     */
    public List<FundTO> getFundsApplyingProfile() {
        if (fundsApplyingProfile == null) {
            fundsApplyingProfile = new ArrayList<FundTO>();
        }
        return this.fundsApplyingProfile;
    }

    /**
     * Sets the value of the fundsApplyingProfile property.
     * 
     * @param fundsApplyingProfile
     *     allowed object is
     *     {@link FundTO }
     *     
     */
    public void setFundsApplyingProfile(List<FundTO> fundsApplyingProfile) {
        this.fundsApplyingProfile = fundsApplyingProfile;
    }

}
