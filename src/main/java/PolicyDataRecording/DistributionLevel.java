
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DistributionLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DistributionLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Branch"/>
 *     &lt;enumeration value="Captive"/>
 *     &lt;enumeration value="Field"/>
 *     &lt;enumeration value="National"/>
 *     &lt;enumeration value="Regional"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DistributionLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/")
@XmlEnum
public enum DistributionLevel {

    @XmlEnumValue("Branch")
    BRANCH("Branch"),
    @XmlEnumValue("Captive")
    CAPTIVE("Captive"),
    @XmlEnumValue("Field")
    FIELD("Field"),
    @XmlEnumValue("National")
    NATIONAL("National"),
    @XmlEnumValue("Regional")
    REGIONAL("Regional");
    private final String value;

    DistributionLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DistributionLevel fromValue(String v) {
        for (DistributionLevel c: DistributionLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
