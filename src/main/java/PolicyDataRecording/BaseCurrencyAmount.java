
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BaseCurrencyAmount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseCurrencyAmount">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Value">
 *       &lt;sequence>
 *         &lt;element name="theAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="theCurrencyCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseCurrencyAmount", namespace = "http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/", propOrder = {
    "theAmount",
    "theCurrencyCode"
})
public class BaseCurrencyAmount
    extends Value
{

    protected BigDecimal theAmount;
    protected String theCurrencyCode;

    /**
     * Gets the value of the theAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTheAmount() {
        return theAmount;
    }

    /**
     * Sets the value of the theAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTheAmount(BigDecimal value) {
        this.theAmount = value;
    }

    /**
     * Gets the value of the theCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheCurrencyCode() {
        return theCurrencyCode;
    }

    /**
     * Sets the value of the theCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheCurrencyCode(String value) {
        this.theCurrencyCode = value;
    }

}
