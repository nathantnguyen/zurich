
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompanyDetail_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompanyDetail_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="revisedFoundationYear" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="accountNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="foundationYear" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="revisedNumberOfEmployees" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="civilJudgmentCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="dunsInquiriesCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="negativePaymentCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="positivePaymenmtCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="managmentStartYear" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="foundationYearAsString" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompanyDetail_TO", propOrder = {
    "revisedFoundationYear",
    "accountNumber",
    "foundationYear",
    "revisedNumberOfEmployees",
    "civilJudgmentCount",
    "dunsInquiriesCount",
    "negativePaymentCount",
    "positivePaymenmtCount",
    "managmentStartYear",
    "foundationYearAsString"
})
public class CompanyDetailTO
    extends DependentObjectTO
{

    protected BigInteger revisedFoundationYear;
    protected String accountNumber;
    protected BigInteger foundationYear;
    protected BigInteger revisedNumberOfEmployees;
    protected BigInteger civilJudgmentCount;
    protected BigInteger dunsInquiriesCount;
    protected BigInteger negativePaymentCount;
    protected BigInteger positivePaymenmtCount;
    protected BigInteger managmentStartYear;
    protected String foundationYearAsString;

    /**
     * Gets the value of the revisedFoundationYear property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRevisedFoundationYear() {
        return revisedFoundationYear;
    }

    /**
     * Sets the value of the revisedFoundationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRevisedFoundationYear(BigInteger value) {
        this.revisedFoundationYear = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the foundationYear property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFoundationYear() {
        return foundationYear;
    }

    /**
     * Sets the value of the foundationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFoundationYear(BigInteger value) {
        this.foundationYear = value;
    }

    /**
     * Gets the value of the revisedNumberOfEmployees property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRevisedNumberOfEmployees() {
        return revisedNumberOfEmployees;
    }

    /**
     * Sets the value of the revisedNumberOfEmployees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRevisedNumberOfEmployees(BigInteger value) {
        this.revisedNumberOfEmployees = value;
    }

    /**
     * Gets the value of the civilJudgmentCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCivilJudgmentCount() {
        return civilJudgmentCount;
    }

    /**
     * Sets the value of the civilJudgmentCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCivilJudgmentCount(BigInteger value) {
        this.civilJudgmentCount = value;
    }

    /**
     * Gets the value of the dunsInquiriesCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDunsInquiriesCount() {
        return dunsInquiriesCount;
    }

    /**
     * Sets the value of the dunsInquiriesCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDunsInquiriesCount(BigInteger value) {
        this.dunsInquiriesCount = value;
    }

    /**
     * Gets the value of the negativePaymentCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNegativePaymentCount() {
        return negativePaymentCount;
    }

    /**
     * Sets the value of the negativePaymentCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNegativePaymentCount(BigInteger value) {
        this.negativePaymentCount = value;
    }

    /**
     * Gets the value of the positivePaymenmtCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPositivePaymenmtCount() {
        return positivePaymenmtCount;
    }

    /**
     * Sets the value of the positivePaymenmtCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPositivePaymenmtCount(BigInteger value) {
        this.positivePaymenmtCount = value;
    }

    /**
     * Gets the value of the managmentStartYear property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getManagmentStartYear() {
        return managmentStartYear;
    }

    /**
     * Sets the value of the managmentStartYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setManagmentStartYear(BigInteger value) {
        this.managmentStartYear = value;
    }

    /**
     * Gets the value of the foundationYearAsString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFoundationYearAsString() {
        return foundationYearAsString;
    }

    /**
     * Sets the value of the foundationYearAsString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFoundationYearAsString(String value) {
        this.foundationYearAsString = value;
    }

}
