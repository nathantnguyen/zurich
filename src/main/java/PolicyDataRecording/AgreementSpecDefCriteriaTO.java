
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AgreementSpecDefCriteria_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgreementSpecDefCriteria_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="effectiveFrom" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="effectiveTo" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="creationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="replacementDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/}AgreementSpecState" minOccurs="0"/>
 *         &lt;element name="agreementSpec" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementSpec_TO" minOccurs="0"/>
 *         &lt;element name="container" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementSpecConcept_TO" minOccurs="0"/>
 *         &lt;element name="targetSements" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgreementSpecDefCriteria_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "effectiveFrom",
    "effectiveTo",
    "creationDate",
    "replacementDate",
    "state",
    "agreementSpec",
    "container",
    "targetSements"
})
public class AgreementSpecDefCriteriaTO
    extends BaseTransferObject
{

    protected XMLGregorianCalendar effectiveFrom;
    protected XMLGregorianCalendar effectiveTo;
    protected XMLGregorianCalendar creationDate;
    protected XMLGregorianCalendar replacementDate;
    protected AgreementSpecState state;
    protected AgreementSpecTO agreementSpec;
    protected AgreementSpecConceptTO container;
    protected List<CategoryTO> targetSements;

    /**
     * Gets the value of the effectiveFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveFrom() {
        return effectiveFrom;
    }

    /**
     * Sets the value of the effectiveFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveFrom(XMLGregorianCalendar value) {
        this.effectiveFrom = value;
    }

    /**
     * Gets the value of the effectiveTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveTo() {
        return effectiveTo;
    }

    /**
     * Sets the value of the effectiveTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveTo(XMLGregorianCalendar value) {
        this.effectiveTo = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the replacementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReplacementDate() {
        return replacementDate;
    }

    /**
     * Sets the value of the replacementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReplacementDate(XMLGregorianCalendar value) {
        this.replacementDate = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementSpecState }
     *     
     */
    public AgreementSpecState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementSpecState }
     *     
     */
    public void setState(AgreementSpecState value) {
        this.state = value;
    }

    /**
     * Gets the value of the agreementSpec property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementSpecTO }
     *     
     */
    public AgreementSpecTO getAgreementSpec() {
        return agreementSpec;
    }

    /**
     * Sets the value of the agreementSpec property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementSpecTO }
     *     
     */
    public void setAgreementSpec(AgreementSpecTO value) {
        this.agreementSpec = value;
    }

    /**
     * Gets the value of the container property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementSpecConceptTO }
     *     
     */
    public AgreementSpecConceptTO getContainer() {
        return container;
    }

    /**
     * Sets the value of the container property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementSpecConceptTO }
     *     
     */
    public void setContainer(AgreementSpecConceptTO value) {
        this.container = value;
    }

    /**
     * Gets the value of the targetSements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the targetSements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTargetSements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getTargetSements() {
        if (targetSements == null) {
            targetSements = new ArrayList<CategoryTO>();
        }
        return this.targetSements;
    }

    /**
     * Sets the value of the targetSements property.
     * 
     * @param targetSements
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setTargetSements(List<CategoryTO> targetSements) {
        this.targetSements = targetSements;
    }

}
