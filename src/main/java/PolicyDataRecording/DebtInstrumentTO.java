
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DebtInstrument_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DebtInstrument_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="debtInterestPaymentFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="interestResetFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="nextInterestPaymentDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="nextInterestResetDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebtInstrument_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "debtInterestPaymentFrequency",
    "interestResetFrequency",
    "nextInterestPaymentDate",
    "nextInterestResetDate"
})
public class DebtInstrumentTO
    extends FinancialAssetTO
{

    protected BigInteger debtInterestPaymentFrequency;
    protected BigInteger interestResetFrequency;
    protected XMLGregorianCalendar nextInterestPaymentDate;
    protected XMLGregorianCalendar nextInterestResetDate;

    /**
     * Gets the value of the debtInterestPaymentFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDebtInterestPaymentFrequency() {
        return debtInterestPaymentFrequency;
    }

    /**
     * Sets the value of the debtInterestPaymentFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDebtInterestPaymentFrequency(BigInteger value) {
        this.debtInterestPaymentFrequency = value;
    }

    /**
     * Gets the value of the interestResetFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInterestResetFrequency() {
        return interestResetFrequency;
    }

    /**
     * Sets the value of the interestResetFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInterestResetFrequency(BigInteger value) {
        this.interestResetFrequency = value;
    }

    /**
     * Gets the value of the nextInterestPaymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextInterestPaymentDate() {
        return nextInterestPaymentDate;
    }

    /**
     * Sets the value of the nextInterestPaymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextInterestPaymentDate(XMLGregorianCalendar value) {
        this.nextInterestPaymentDate = value;
    }

    /**
     * Gets the value of the nextInterestResetDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextInterestResetDate() {
        return nextInterestResetDate;
    }

    /**
     * Sets the value of the nextInterestResetDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextInterestResetDate(XMLGregorianCalendar value) {
        this.nextInterestResetDate = value;
    }

}
