
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NamedDriver_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NamedDriver_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}Insured_TO">
 *       &lt;sequence>
 *         &lt;element name="declaredClaims" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="distanceDriven" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="whenDriving" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="usedVehicles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}Vehicle_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="declaredAccidents" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="declaredDrivingViolations" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="hasNonOwnedCompanyCar" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NamedDriver_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "declaredClaims",
    "distanceDriven",
    "frequency",
    "whenDriving",
    "usedVehicles",
    "declaredAccidents",
    "declaredDrivingViolations",
    "hasNonOwnedCompanyCar"
})
public class NamedDriverTO
    extends InsuredTO
{

    protected BigInteger declaredClaims;
    protected Amount distanceDriven;
    protected Frequency frequency;
    protected String whenDriving;
    protected List<VehicleTO> usedVehicles;
    protected BigInteger declaredAccidents;
    protected BigInteger declaredDrivingViolations;
    protected Boolean hasNonOwnedCompanyCar;

    /**
     * Gets the value of the declaredClaims property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDeclaredClaims() {
        return declaredClaims;
    }

    /**
     * Sets the value of the declaredClaims property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDeclaredClaims(BigInteger value) {
        this.declaredClaims = value;
    }

    /**
     * Gets the value of the distanceDriven property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getDistanceDriven() {
        return distanceDriven;
    }

    /**
     * Sets the value of the distanceDriven property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setDistanceDriven(Amount value) {
        this.distanceDriven = value;
    }

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the whenDriving property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhenDriving() {
        return whenDriving;
    }

    /**
     * Sets the value of the whenDriving property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhenDriving(String value) {
        this.whenDriving = value;
    }

    /**
     * Gets the value of the usedVehicles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usedVehicles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsedVehicles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleTO }
     * 
     * 
     */
    public List<VehicleTO> getUsedVehicles() {
        if (usedVehicles == null) {
            usedVehicles = new ArrayList<VehicleTO>();
        }
        return this.usedVehicles;
    }

    /**
     * Gets the value of the declaredAccidents property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDeclaredAccidents() {
        return declaredAccidents;
    }

    /**
     * Sets the value of the declaredAccidents property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDeclaredAccidents(BigInteger value) {
        this.declaredAccidents = value;
    }

    /**
     * Gets the value of the declaredDrivingViolations property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDeclaredDrivingViolations() {
        return declaredDrivingViolations;
    }

    /**
     * Sets the value of the declaredDrivingViolations property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDeclaredDrivingViolations(BigInteger value) {
        this.declaredDrivingViolations = value;
    }

    /**
     * Gets the value of the hasNonOwnedCompanyCar property.
     * This getter has been renamed from isHasNonOwnedCompanyCar() to getHasNonOwnedCompanyCar() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasNonOwnedCompanyCar() {
        return hasNonOwnedCompanyCar;
    }

    /**
     * Sets the value of the hasNonOwnedCompanyCar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasNonOwnedCompanyCar(Boolean value) {
        this.hasNonOwnedCompanyCar = value;
    }

    /**
     * Sets the value of the usedVehicles property.
     * 
     * @param usedVehicles
     *     allowed object is
     *     {@link VehicleTO }
     *     
     */
    public void setUsedVehicles(List<VehicleTO> usedVehicles) {
        this.usedVehicles = usedVehicles;
    }

}
