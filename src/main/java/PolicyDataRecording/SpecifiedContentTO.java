
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SpecifiedContent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpecifiedContent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO">
 *       &lt;sequence>
 *         &lt;element name="isBasedOn" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}StandardTextSpecification_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecifiedContent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "isBasedOn"
})
@XmlSeeAlso({
    AskedQuestionTO.class
})
public class SpecifiedContentTO
    extends CommunicationContentTO
{

    protected StandardTextSpecificationTO isBasedOn;

    /**
     * Gets the value of the isBasedOn property.
     * 
     * @return
     *     possible object is
     *     {@link StandardTextSpecificationTO }
     *     
     */
    public StandardTextSpecificationTO getIsBasedOn() {
        return isBasedOn;
    }

    /**
     * Sets the value of the isBasedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardTextSpecificationTO }
     *     
     */
    public void setIsBasedOn(StandardTextSpecificationTO value) {
        this.isBasedOn = value;
    }

}
