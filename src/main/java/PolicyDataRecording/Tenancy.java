
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Tenancy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Tenancy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Let Or Leased"/>
 *     &lt;enumeration value="Owner Occupied"/>
 *     &lt;enumeration value="Sub Let Or Sub Leased"/>
 *     &lt;enumeration value="Vacation Rental"/>
 *     &lt;enumeration value="Rented"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Tenancy", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum Tenancy {

    @XmlEnumValue("Let Or Leased")
    LET_OR_LEASED("Let Or Leased"),
    @XmlEnumValue("Owner Occupied")
    OWNER_OCCUPIED("Owner Occupied"),
    @XmlEnumValue("Sub Let Or Sub Leased")
    SUB_LET_OR_SUB_LEASED("Sub Let Or Sub Leased"),
    @XmlEnumValue("Vacation Rental")
    VACATION_RENTAL("Vacation Rental"),
    @XmlEnumValue("Rented")
    RENTED("Rented");
    private final String value;

    Tenancy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Tenancy fromValue(String v) {
        for (Tenancy c: Tenancy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
