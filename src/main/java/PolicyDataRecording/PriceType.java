
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PriceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Bid"/>
 *     &lt;enumeration value="Cancellation"/>
 *     &lt;enumeration value="Creation"/>
 *     &lt;enumeration value="Mid"/>
 *     &lt;enumeration value="Offer"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PriceType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum PriceType {

    @XmlEnumValue("Bid")
    BID("Bid"),
    @XmlEnumValue("Cancellation")
    CANCELLATION("Cancellation"),
    @XmlEnumValue("Creation")
    CREATION("Creation"),
    @XmlEnumValue("Mid")
    MID("Mid"),
    @XmlEnumValue("Offer")
    OFFER("Offer");
    private final String value;

    PriceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PriceType fromValue(String v) {
        for (PriceType c: PriceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
