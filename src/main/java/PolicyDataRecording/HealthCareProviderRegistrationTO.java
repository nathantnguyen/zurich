
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HealthCareProviderRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HealthCareProviderRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="taxonomyCode" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}TaxonomyCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HealthCareProviderRegistration_TO", propOrder = {
    "taxonomyCode"
})
@XmlSeeAlso({
    HospitalLicenseTO.class
})
public class HealthCareProviderRegistrationTO
    extends PartyRegistrationTO
{

    protected String taxonomyCode;

    /**
     * Gets the value of the taxonomyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxonomyCode() {
        return taxonomyCode;
    }

    /**
     * Sets the value of the taxonomyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxonomyCode(String value) {
        this.taxonomyCode = value;
    }

}
