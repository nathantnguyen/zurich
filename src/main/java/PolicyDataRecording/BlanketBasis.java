
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BlanketBasis.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BlanketBasis">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="No Blanket"/>
 *     &lt;enumeration value="Same Kind Of Property In Two Or More Locations"/>
 *     &lt;enumeration value="Two Or More Different Kinds Of Property In The Same Location"/>
 *     &lt;enumeration value="Two Or More Different Kinds Of Property In Two Or More Different Locations"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BlanketBasis", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum BlanketBasis {

    @XmlEnumValue("No Blanket")
    NO_BLANKET("No Blanket"),
    @XmlEnumValue("Same Kind Of Property In Two Or More Locations")
    SAME_KIND_OF_PROPERTY_IN_TWO_OR_MORE_LOCATIONS("Same Kind Of Property In Two Or More Locations"),
    @XmlEnumValue("Two Or More Different Kinds Of Property In The Same Location")
    TWO_OR_MORE_DIFFERENT_KINDS_OF_PROPERTY_IN_THE_SAME_LOCATION("Two Or More Different Kinds Of Property In The Same Location"),
    @XmlEnumValue("Two Or More Different Kinds Of Property In Two Or More Different Locations")
    TWO_OR_MORE_DIFFERENT_KINDS_OF_PROPERTY_IN_TWO_OR_MORE_DIFFERENT_LOCATIONS("Two Or More Different Kinds Of Property In Two Or More Different Locations");
    private final String value;

    BlanketBasis(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BlanketBasis fromValue(String v) {
        for (BlanketBasis c: BlanketBasis.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
