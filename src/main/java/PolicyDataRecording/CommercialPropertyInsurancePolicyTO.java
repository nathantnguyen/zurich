
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommercialPropertyInsurancePolicy_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialPropertyInsurancePolicy_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CommercialAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="blanketBasis" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}BlanketBasis" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialPropertyInsurancePolicy_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "blanketBasis"
})
public class CommercialPropertyInsurancePolicyTO
    extends CommercialAgreementTO
{

    protected BlanketBasis blanketBasis;

    /**
     * Gets the value of the blanketBasis property.
     * 
     * @return
     *     possible object is
     *     {@link BlanketBasis }
     *     
     */
    public BlanketBasis getBlanketBasis() {
        return blanketBasis;
    }

    /**
     * Sets the value of the blanketBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlanketBasis }
     *     
     */
    public void setBlanketBasis(BlanketBasis value) {
        this.blanketBasis = value;
    }

}
