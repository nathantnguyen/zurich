
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for JointContingency.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="JointContingency">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Custom"/>
 *     &lt;enumeration value="Fifty Fifty"/>
 *     &lt;enumeration value="First To Die"/>
 *     &lt;enumeration value="Last To Die"/>
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="One Half"/>
 *     &lt;enumeration value="One Third"/>
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="Two Thirds"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "JointContingency", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum JointContingency {

    @XmlEnumValue("Custom")
    CUSTOM("Custom"),
    @XmlEnumValue("Fifty Fifty")
    FIFTY_FIFTY("Fifty Fifty"),
    @XmlEnumValue("First To Die")
    FIRST_TO_DIE("First To Die"),
    @XmlEnumValue("Last To Die")
    LAST_TO_DIE("Last To Die"),
    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("One Half")
    ONE_HALF("One Half"),
    @XmlEnumValue("One Third")
    ONE_THIRD("One Third"),
    @XmlEnumValue("Other")
    OTHER("Other"),
    @XmlEnumValue("Two Thirds")
    TWO_THIRDS("Two Thirds"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    JointContingency(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static JointContingency fromValue(String v) {
        for (JointContingency c: JointContingency.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
