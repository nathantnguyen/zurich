
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Property_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Property_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="specification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="conformanceType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="theValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="overriddenValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="isOverridden" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="defaultValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Binary" minOccurs="0"/>
 *         &lt;element name="isConstant" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isOverridable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isDerived" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isCalculated" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isOptional" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Property_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "kind",
    "description",
    "specification",
    "conformanceType",
    "theValue",
    "overriddenValue",
    "isOverridden",
    "defaultValue",
    "isConstant",
    "isOverridable",
    "isDerived",
    "isCalculated",
    "isOptional"
})
public class PropertyTO
    extends BaseTransferObject
{

    protected String kind;
    protected String description;
    protected ObjectReferenceTO specification;
    protected String conformanceType;
    protected String theValue;
    protected String overriddenValue;
    protected Boolean isOverridden;
    protected Binary defaultValue;
    protected Boolean isConstant;
    protected Boolean isOverridable;
    protected Boolean isDerived;
    protected Boolean isCalculated;
    protected Boolean isOptional;

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the specification property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getSpecification() {
        return specification;
    }

    /**
     * Sets the value of the specification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSpecification(ObjectReferenceTO value) {
        this.specification = value;
    }

    /**
     * Gets the value of the conformanceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConformanceType() {
        return conformanceType;
    }

    /**
     * Sets the value of the conformanceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConformanceType(String value) {
        this.conformanceType = value;
    }

    /**
     * Gets the value of the theValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheValue() {
        return theValue;
    }

    /**
     * Sets the value of the theValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheValue(String value) {
        this.theValue = value;
    }

    /**
     * Gets the value of the overriddenValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverriddenValue() {
        return overriddenValue;
    }

    /**
     * Sets the value of the overriddenValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverriddenValue(String value) {
        this.overriddenValue = value;
    }

    /**
     * Gets the value of the isOverridden property.
     * This getter has been renamed from isIsOverridden() to getIsOverridden() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOverridden() {
        return isOverridden;
    }

    /**
     * Sets the value of the isOverridden property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOverridden(Boolean value) {
        this.isOverridden = value;
    }

    /**
     * Gets the value of the defaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link Binary }
     *     
     */
    public Binary getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the value of the defaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Binary }
     *     
     */
    public void setDefaultValue(Binary value) {
        this.defaultValue = value;
    }

    /**
     * Gets the value of the isConstant property.
     * This getter has been renamed from isIsConstant() to getIsConstant() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsConstant() {
        return isConstant;
    }

    /**
     * Sets the value of the isConstant property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsConstant(Boolean value) {
        this.isConstant = value;
    }

    /**
     * Gets the value of the isOverridable property.
     * This getter has been renamed from isIsOverridable() to getIsOverridable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOverridable() {
        return isOverridable;
    }

    /**
     * Sets the value of the isOverridable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOverridable(Boolean value) {
        this.isOverridable = value;
    }

    /**
     * Gets the value of the isDerived property.
     * This getter has been renamed from isIsDerived() to getIsDerived() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsDerived() {
        return isDerived;
    }

    /**
     * Sets the value of the isDerived property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDerived(Boolean value) {
        this.isDerived = value;
    }

    /**
     * Gets the value of the isCalculated property.
     * This getter has been renamed from isIsCalculated() to getIsCalculated() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsCalculated() {
        return isCalculated;
    }

    /**
     * Sets the value of the isCalculated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCalculated(Boolean value) {
        this.isCalculated = value;
    }

    /**
     * Gets the value of the isOptional property.
     * This getter has been renamed from isIsOptional() to getIsOptional() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOptional() {
        return isOptional;
    }

    /**
     * Sets the value of the isOptional property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOptional(Boolean value) {
        this.isOptional = value;
    }

}
