
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialServicesRoleTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FinancialServicesRoleTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Premium Payer"/>
 *     &lt;enumeration value="Coinsurer"/>
 *     &lt;enumeration value="Assignee"/>
 *     &lt;enumeration value="Account Holder"/>
 *     &lt;enumeration value="Account Provider"/>
 *     &lt;enumeration value="Creditor"/>
 *     &lt;enumeration value="Signatory"/>
 *     &lt;enumeration value="Contribution Payer"/>
 *     &lt;enumeration value="Selling Channel"/>
 *     &lt;enumeration value="Reinsurer"/>
 *     &lt;enumeration value="Cedent"/>
 *     &lt;enumeration value="Medical Examiner"/>
 *     &lt;enumeration value="Assignor"/>
 *     &lt;enumeration value="Operating Cedent"/>
 *     &lt;enumeration value="Placing Broker"/>
 *     &lt;enumeration value="Producing Broker"/>
 *     &lt;enumeration value="Retrocessionaire"/>
 *     &lt;enumeration value="Credit Rating Agency"/>
 *     &lt;enumeration value="Financial Asset Issuer"/>
 *     &lt;enumeration value="Underwriter"/>
 *     &lt;enumeration value="Underwriting Assistant"/>
 *     &lt;enumeration value="Policy Administrator"/>
 *     &lt;enumeration value="Agreement Source System"/>
 *     &lt;enumeration value="Prior Insurer"/>
 *     &lt;enumeration value="Insured Contact"/>
 *     &lt;enumeration value="Business Development Leader"/>
 *     &lt;enumeration value="Agreement Pricing System"/>
 *     &lt;enumeration value="Captive Manager"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FinancialServicesRoleTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum FinancialServicesRoleTypeName {

    @XmlEnumValue("Premium Payer")
    PREMIUM_PAYER("Premium Payer"),
    @XmlEnumValue("Coinsurer")
    COINSURER("Coinsurer"),
    @XmlEnumValue("Assignee")
    ASSIGNEE("Assignee"),
    @XmlEnumValue("Account Holder")
    ACCOUNT_HOLDER("Account Holder"),
    @XmlEnumValue("Account Provider")
    ACCOUNT_PROVIDER("Account Provider"),
    @XmlEnumValue("Creditor")
    CREDITOR("Creditor"),
    @XmlEnumValue("Signatory")
    SIGNATORY("Signatory"),
    @XmlEnumValue("Contribution Payer")
    CONTRIBUTION_PAYER("Contribution Payer"),
    @XmlEnumValue("Selling Channel")
    SELLING_CHANNEL("Selling Channel"),
    @XmlEnumValue("Reinsurer")
    REINSURER("Reinsurer"),
    @XmlEnumValue("Cedent")
    CEDENT("Cedent"),
    @XmlEnumValue("Medical Examiner")
    MEDICAL_EXAMINER("Medical Examiner"),
    @XmlEnumValue("Assignor")
    ASSIGNOR("Assignor"),
    @XmlEnumValue("Operating Cedent")
    OPERATING_CEDENT("Operating Cedent"),
    @XmlEnumValue("Placing Broker")
    PLACING_BROKER("Placing Broker"),
    @XmlEnumValue("Producing Broker")
    PRODUCING_BROKER("Producing Broker"),
    @XmlEnumValue("Retrocessionaire")
    RETROCESSIONAIRE("Retrocessionaire"),
    @XmlEnumValue("Credit Rating Agency")
    CREDIT_RATING_AGENCY("Credit Rating Agency"),
    @XmlEnumValue("Financial Asset Issuer")
    FINANCIAL_ASSET_ISSUER("Financial Asset Issuer"),
    @XmlEnumValue("Underwriter")
    UNDERWRITER("Underwriter"),
    @XmlEnumValue("Underwriting Assistant")
    UNDERWRITING_ASSISTANT("Underwriting Assistant"),
    @XmlEnumValue("Policy Administrator")
    POLICY_ADMINISTRATOR("Policy Administrator"),
    @XmlEnumValue("Agreement Source System")
    AGREEMENT_SOURCE_SYSTEM("Agreement Source System"),
    @XmlEnumValue("Prior Insurer")
    PRIOR_INSURER("Prior Insurer"),
    @XmlEnumValue("Insured Contact")
    INSURED_CONTACT("Insured Contact"),
    @XmlEnumValue("Business Development Leader")
    BUSINESS_DEVELOPMENT_LEADER("Business Development Leader"),
    @XmlEnumValue("Agreement Pricing System")
    AGREEMENT_PRICING_SYSTEM("Agreement Pricing System"),
    @XmlEnumValue("Captive Manager")
    CAPTIVE_MANAGER("Captive Manager");
    private final String value;

    FinancialServicesRoleTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FinancialServicesRoleTypeName fromValue(String v) {
        for (FinancialServicesRoleTypeName c: FinancialServicesRoleTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
