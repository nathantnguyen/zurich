
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoadVehicleModel_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoadVehicleModel_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}VehicleModel_TO">
 *       &lt;sequence>
 *         &lt;element name="maximumCarryingWeight" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="maximumCarryingWeightCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="maximumCarryingWeightAsString" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="grossVehicleWeightRating" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="seatbeltsLocation" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}SeatbeltsLocation" minOccurs="0"/>
 *         &lt;element name="airbagsLocation" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}AirbagsLocation" minOccurs="0"/>
 *         &lt;element name="transmission" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="cylinders" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="wheelBaseLength" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="axelCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="usage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="bedSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="transmissionType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="transmissionSpeed" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="grossVehicleWeightRatingCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="trailerLength" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="longestWheelBaseLength" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="bedSizeCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="engineStrokeCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="trailerCapacity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="numberOfValvesPerCylinder" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="numberOfValves" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="usageCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoadVehicleModel_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "maximumCarryingWeight",
    "maximumCarryingWeightCode",
    "maximumCarryingWeightAsString",
    "grossVehicleWeightRating",
    "seatbeltsLocation",
    "airbagsLocation",
    "transmission",
    "cylinders",
    "wheelBaseLength",
    "axelCount",
    "usage",
    "bedSize",
    "transmissionType",
    "transmissionSpeed",
    "grossVehicleWeightRatingCode",
    "trailerLength",
    "longestWheelBaseLength",
    "bedSizeCode",
    "engineStrokeCount",
    "trailerCapacity",
    "numberOfValvesPerCylinder",
    "numberOfValves",
    "usageCode"
})
@XmlSeeAlso({
    TruckModelTO.class,
    CarModelTO.class
})
public class RoadVehicleModelTO
    extends VehicleModelTO
{

    protected Amount maximumCarryingWeight;
    protected String maximumCarryingWeightCode;
    protected String maximumCarryingWeightAsString;
    protected String grossVehicleWeightRating;
    protected SeatbeltsLocation seatbeltsLocation;
    protected AirbagsLocation airbagsLocation;
    protected String transmission;
    protected BigInteger cylinders;
    protected BigDecimal wheelBaseLength;
    protected BigInteger axelCount;
    protected String usage;
    protected String bedSize;
    protected String transmissionType;
    protected String transmissionSpeed;
    protected String grossVehicleWeightRatingCode;
    protected BigDecimal trailerLength;
    protected BigDecimal longestWheelBaseLength;
    protected String bedSizeCode;
    protected BigInteger engineStrokeCount;
    protected BigDecimal trailerCapacity;
    protected BigInteger numberOfValvesPerCylinder;
    protected BigInteger numberOfValves;
    protected String usageCode;

    /**
     * Gets the value of the maximumCarryingWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getMaximumCarryingWeight() {
        return maximumCarryingWeight;
    }

    /**
     * Sets the value of the maximumCarryingWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setMaximumCarryingWeight(Amount value) {
        this.maximumCarryingWeight = value;
    }

    /**
     * Gets the value of the maximumCarryingWeightCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumCarryingWeightCode() {
        return maximumCarryingWeightCode;
    }

    /**
     * Sets the value of the maximumCarryingWeightCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumCarryingWeightCode(String value) {
        this.maximumCarryingWeightCode = value;
    }

    /**
     * Gets the value of the maximumCarryingWeightAsString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumCarryingWeightAsString() {
        return maximumCarryingWeightAsString;
    }

    /**
     * Sets the value of the maximumCarryingWeightAsString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumCarryingWeightAsString(String value) {
        this.maximumCarryingWeightAsString = value;
    }

    /**
     * Gets the value of the grossVehicleWeightRating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrossVehicleWeightRating() {
        return grossVehicleWeightRating;
    }

    /**
     * Sets the value of the grossVehicleWeightRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrossVehicleWeightRating(String value) {
        this.grossVehicleWeightRating = value;
    }

    /**
     * Gets the value of the seatbeltsLocation property.
     * 
     * @return
     *     possible object is
     *     {@link SeatbeltsLocation }
     *     
     */
    public SeatbeltsLocation getSeatbeltsLocation() {
        return seatbeltsLocation;
    }

    /**
     * Sets the value of the seatbeltsLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatbeltsLocation }
     *     
     */
    public void setSeatbeltsLocation(SeatbeltsLocation value) {
        this.seatbeltsLocation = value;
    }

    /**
     * Gets the value of the airbagsLocation property.
     * 
     * @return
     *     possible object is
     *     {@link AirbagsLocation }
     *     
     */
    public AirbagsLocation getAirbagsLocation() {
        return airbagsLocation;
    }

    /**
     * Sets the value of the airbagsLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirbagsLocation }
     *     
     */
    public void setAirbagsLocation(AirbagsLocation value) {
        this.airbagsLocation = value;
    }

    /**
     * Gets the value of the transmission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmission() {
        return transmission;
    }

    /**
     * Sets the value of the transmission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmission(String value) {
        this.transmission = value;
    }

    /**
     * Gets the value of the cylinders property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCylinders() {
        return cylinders;
    }

    /**
     * Sets the value of the cylinders property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCylinders(BigInteger value) {
        this.cylinders = value;
    }

    /**
     * Gets the value of the wheelBaseLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWheelBaseLength() {
        return wheelBaseLength;
    }

    /**
     * Sets the value of the wheelBaseLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWheelBaseLength(BigDecimal value) {
        this.wheelBaseLength = value;
    }

    /**
     * Gets the value of the axelCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAxelCount() {
        return axelCount;
    }

    /**
     * Sets the value of the axelCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAxelCount(BigInteger value) {
        this.axelCount = value;
    }

    /**
     * Gets the value of the usage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsage() {
        return usage;
    }

    /**
     * Sets the value of the usage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsage(String value) {
        this.usage = value;
    }

    /**
     * Gets the value of the bedSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBedSize() {
        return bedSize;
    }

    /**
     * Sets the value of the bedSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBedSize(String value) {
        this.bedSize = value;
    }

    /**
     * Gets the value of the transmissionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionType() {
        return transmissionType;
    }

    /**
     * Sets the value of the transmissionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionType(String value) {
        this.transmissionType = value;
    }

    /**
     * Gets the value of the transmissionSpeed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionSpeed() {
        return transmissionSpeed;
    }

    /**
     * Sets the value of the transmissionSpeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionSpeed(String value) {
        this.transmissionSpeed = value;
    }

    /**
     * Gets the value of the grossVehicleWeightRatingCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrossVehicleWeightRatingCode() {
        return grossVehicleWeightRatingCode;
    }

    /**
     * Sets the value of the grossVehicleWeightRatingCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrossVehicleWeightRatingCode(String value) {
        this.grossVehicleWeightRatingCode = value;
    }

    /**
     * Gets the value of the trailerLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTrailerLength() {
        return trailerLength;
    }

    /**
     * Sets the value of the trailerLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTrailerLength(BigDecimal value) {
        this.trailerLength = value;
    }

    /**
     * Gets the value of the longestWheelBaseLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLongestWheelBaseLength() {
        return longestWheelBaseLength;
    }

    /**
     * Sets the value of the longestWheelBaseLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLongestWheelBaseLength(BigDecimal value) {
        this.longestWheelBaseLength = value;
    }

    /**
     * Gets the value of the bedSizeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBedSizeCode() {
        return bedSizeCode;
    }

    /**
     * Sets the value of the bedSizeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBedSizeCode(String value) {
        this.bedSizeCode = value;
    }

    /**
     * Gets the value of the engineStrokeCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEngineStrokeCount() {
        return engineStrokeCount;
    }

    /**
     * Sets the value of the engineStrokeCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEngineStrokeCount(BigInteger value) {
        this.engineStrokeCount = value;
    }

    /**
     * Gets the value of the trailerCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTrailerCapacity() {
        return trailerCapacity;
    }

    /**
     * Sets the value of the trailerCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTrailerCapacity(BigDecimal value) {
        this.trailerCapacity = value;
    }

    /**
     * Gets the value of the numberOfValvesPerCylinder property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfValvesPerCylinder() {
        return numberOfValvesPerCylinder;
    }

    /**
     * Sets the value of the numberOfValvesPerCylinder property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfValvesPerCylinder(BigInteger value) {
        this.numberOfValvesPerCylinder = value;
    }

    /**
     * Gets the value of the numberOfValves property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfValves() {
        return numberOfValves;
    }

    /**
     * Sets the value of the numberOfValves property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfValves(BigInteger value) {
        this.numberOfValves = value;
    }

    /**
     * Gets the value of the usageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsageCode() {
        return usageCode;
    }

    /**
     * Sets the value of the usageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsageCode(String value) {
        this.usageCode = value;
    }

}
