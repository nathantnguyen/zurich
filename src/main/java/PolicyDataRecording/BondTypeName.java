
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BondTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BondTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Catastrophe Bond"/>
 *     &lt;enumeration value="Eurobond"/>
 *     &lt;enumeration value="Municipal Bond"/>
 *     &lt;enumeration value="Treasury Bond"/>
 *     &lt;enumeration value="Corporate Bond"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BondTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum BondTypeName {

    @XmlEnumValue("Catastrophe Bond")
    CATASTROPHE_BOND("Catastrophe Bond"),
    @XmlEnumValue("Eurobond")
    EUROBOND("Eurobond"),
    @XmlEnumValue("Municipal Bond")
    MUNICIPAL_BOND("Municipal Bond"),
    @XmlEnumValue("Treasury Bond")
    TREASURY_BOND("Treasury Bond"),
    @XmlEnumValue("Corporate Bond")
    CORPORATE_BOND("Corporate Bond");
    private final String value;

    BondTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BondTypeName fromValue(String v) {
        for (BondTypeName c: BondTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
