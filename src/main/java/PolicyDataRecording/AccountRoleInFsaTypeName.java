
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountRoleInFsaTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountRoleInFsaTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Account Agreement Tracking"/>
 *     &lt;enumeration value="Agreement Collateral"/>
 *     &lt;enumeration value="Agreement Reserve"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AccountRoleInFsaTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum AccountRoleInFsaTypeName {

    @XmlEnumValue("Account Agreement Tracking")
    ACCOUNT_AGREEMENT_TRACKING("Account Agreement Tracking"),
    @XmlEnumValue("Agreement Collateral")
    AGREEMENT_COLLATERAL("Agreement Collateral"),
    @XmlEnumValue("Agreement Reserve")
    AGREEMENT_RESERVE("Agreement Reserve");
    private final String value;

    AccountRoleInFsaTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountRoleInFsaTypeName fromValue(String v) {
        for (AccountRoleInFsaTypeName c: AccountRoleInFsaTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
