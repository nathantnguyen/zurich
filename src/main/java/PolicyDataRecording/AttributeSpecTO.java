
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttributeSpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttributeSpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}SpecificationBuildingBlock_TO">
 *       &lt;sequence>
 *         &lt;element name="conformanceType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="theSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Specification_TO" minOccurs="0"/>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeSpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "conformanceType",
    "theSpecification",
    "kind"
})
@XmlSeeAlso({
    ConstantPropertySpecTO.class,
    PropertySpecTO.class
})
public class AttributeSpecTO
    extends SpecificationBuildingBlockTO
{

    protected String conformanceType;
    protected SpecificationTO theSpecification;
    protected String kind;

    /**
     * Gets the value of the conformanceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConformanceType() {
        return conformanceType;
    }

    /**
     * Sets the value of the conformanceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConformanceType(String value) {
        this.conformanceType = value;
    }

    /**
     * Gets the value of the theSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link SpecificationTO }
     *     
     */
    public SpecificationTO getTheSpecification() {
        return theSpecification;
    }

    /**
     * Sets the value of the theSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificationTO }
     *     
     */
    public void setTheSpecification(SpecificationTO value) {
        this.theSpecification = value;
    }

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

}
