
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RolePlayerRelationshipTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RolePlayerRelationshipTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Organisation Structure"/>
 *     &lt;enumeration value="Reporting Chain"/>
 *     &lt;enumeration value="Employment"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RolePlayerRelationshipTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum RolePlayerRelationshipTypeName {

    @XmlEnumValue("Organisation Structure")
    ORGANISATION_STRUCTURE("Organisation Structure"),
    @XmlEnumValue("Reporting Chain")
    REPORTING_CHAIN("Reporting Chain"),
    @XmlEnumValue("Employment")
    EMPLOYMENT("Employment");
    private final String value;

    RolePlayerRelationshipTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RolePlayerRelationshipTypeName fromValue(String v) {
        for (RolePlayerRelationshipTypeName c: RolePlayerRelationshipTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
