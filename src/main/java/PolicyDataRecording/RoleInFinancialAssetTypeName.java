
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInFinancialAssetTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInFinancialAssetTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Asset Trading Market"/>
 *     &lt;enumeration value="Financial Asset Issuer"/>
 *     &lt;enumeration value="Asset Risk Score"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInFinancialAssetTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum RoleInFinancialAssetTypeName {

    @XmlEnumValue("Asset Trading Market")
    ASSET_TRADING_MARKET("Asset Trading Market"),
    @XmlEnumValue("Financial Asset Issuer")
    FINANCIAL_ASSET_ISSUER("Financial Asset Issuer"),
    @XmlEnumValue("Asset Risk Score")
    ASSET_RISK_SCORE("Asset Risk Score");
    private final String value;

    RoleInFinancialAssetTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInFinancialAssetTypeName fromValue(String v) {
        for (RoleInFinancialAssetTypeName c: RoleInFinancialAssetTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
