
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Ethnicity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Ethnicity">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Afro American"/>
 *     &lt;enumeration value="Caucasian"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Ethnicity", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum Ethnicity {

    @XmlEnumValue("Afro American")
    AFRO_AMERICAN("Afro American"),
    @XmlEnumValue("Caucasian")
    CAUCASIAN("Caucasian");
    private final String value;

    Ethnicity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Ethnicity fromValue(String v) {
        for (Ethnicity c: Ethnicity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
