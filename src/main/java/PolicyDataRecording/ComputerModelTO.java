
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ComputerModel_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComputerModel_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}ModelSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="clockSpeed" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="internalMemory" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="ramMemory" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComputerModel_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "clockSpeed",
    "internalMemory",
    "ramMemory"
})
public class ComputerModelTO
    extends ModelSpecificationTO
{

    protected BigDecimal clockSpeed;
    protected BigInteger internalMemory;
    protected BigInteger ramMemory;

    /**
     * Gets the value of the clockSpeed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getClockSpeed() {
        return clockSpeed;
    }

    /**
     * Sets the value of the clockSpeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setClockSpeed(BigDecimal value) {
        this.clockSpeed = value;
    }

    /**
     * Gets the value of the internalMemory property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInternalMemory() {
        return internalMemory;
    }

    /**
     * Sets the value of the internalMemory property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInternalMemory(BigInteger value) {
        this.internalMemory = value;
    }

    /**
     * Gets the value of the ramMemory property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRamMemory() {
        return ramMemory;
    }

    /**
     * Sets the value of the ramMemory property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRamMemory(BigInteger value) {
        this.ramMemory = value;
    }

}
