
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectGroup_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectGroup_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO">
 *       &lt;sequence>
 *         &lt;element name="maximumAllowedNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="numberInGroup" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="memberType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObjectType_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectGroup_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "maximumAllowedNumber",
    "numberInGroup",
    "memberType"
})
@XmlSeeAlso({
    CargoTO.class
})
public class ObjectGroupTO
    extends PhysicalObjectTO
{

    protected BigInteger maximumAllowedNumber;
    protected BigInteger numberInGroup;
    protected List<PhysicalObjectTypeTO> memberType;

    /**
     * Gets the value of the maximumAllowedNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumAllowedNumber() {
        return maximumAllowedNumber;
    }

    /**
     * Sets the value of the maximumAllowedNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumAllowedNumber(BigInteger value) {
        this.maximumAllowedNumber = value;
    }

    /**
     * Gets the value of the numberInGroup property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberInGroup() {
        return numberInGroup;
    }

    /**
     * Sets the value of the numberInGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberInGroup(BigInteger value) {
        this.numberInGroup = value;
    }

    /**
     * Gets the value of the memberType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the memberType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMemberType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhysicalObjectTypeTO }
     * 
     * 
     */
    public List<PhysicalObjectTypeTO> getMemberType() {
        if (memberType == null) {
            memberType = new ArrayList<PhysicalObjectTypeTO>();
        }
        return this.memberType;
    }

    /**
     * Sets the value of the memberType property.
     * 
     * @param memberType
     *     allowed object is
     *     {@link PhysicalObjectTypeTO }
     *     
     */
    public void setMemberType(List<PhysicalObjectTypeTO> memberType) {
        this.memberType = memberType;
    }

}
