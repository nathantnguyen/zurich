
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PaymentMethod_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentMethod_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="draftDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="theMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}PaymentMethodMethod" minOccurs="0"/>
 *         &lt;element name="directDebitMandate" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}DirectDebitMandateDetails_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="paymentTransferMethod" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}PaymentTransferMethod_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="bankTransferInformation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyTransferInformation_TO" minOccurs="0"/>
 *         &lt;element name="financialTransactionMedium" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}FinancialTransactionMedium_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentMethod_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "description",
    "draftDate",
    "theMethod",
    "directDebitMandate",
    "paymentTransferMethod",
    "bankTransferInformation",
    "financialTransactionMedium"
})
public class PaymentMethodTO
    extends DependentObjectTO
{

    protected String description;
    protected XMLGregorianCalendar draftDate;
    protected PaymentMethodMethod theMethod;
    protected List<DirectDebitMandateDetailsTO> directDebitMandate;
    protected List<PaymentTransferMethodTO> paymentTransferMethod;
    protected MoneyTransferInformationTO bankTransferInformation;
    protected FinancialTransactionMediumTO financialTransactionMedium;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the draftDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDraftDate() {
        return draftDate;
    }

    /**
     * Sets the value of the draftDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDraftDate(XMLGregorianCalendar value) {
        this.draftDate = value;
    }

    /**
     * Gets the value of the theMethod property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMethodMethod }
     *     
     */
    public PaymentMethodMethod getTheMethod() {
        return theMethod;
    }

    /**
     * Sets the value of the theMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMethodMethod }
     *     
     */
    public void setTheMethod(PaymentMethodMethod value) {
        this.theMethod = value;
    }

    /**
     * Gets the value of the directDebitMandate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the directDebitMandate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDirectDebitMandate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DirectDebitMandateDetailsTO }
     * 
     * 
     */
    public List<DirectDebitMandateDetailsTO> getDirectDebitMandate() {
        if (directDebitMandate == null) {
            directDebitMandate = new ArrayList<DirectDebitMandateDetailsTO>();
        }
        return this.directDebitMandate;
    }

    /**
     * Gets the value of the paymentTransferMethod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentTransferMethod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentTransferMethod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentTransferMethodTO }
     * 
     * 
     */
    public List<PaymentTransferMethodTO> getPaymentTransferMethod() {
        if (paymentTransferMethod == null) {
            paymentTransferMethod = new ArrayList<PaymentTransferMethodTO>();
        }
        return this.paymentTransferMethod;
    }

    /**
     * Gets the value of the bankTransferInformation property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyTransferInformationTO }
     *     
     */
    public MoneyTransferInformationTO getBankTransferInformation() {
        return bankTransferInformation;
    }

    /**
     * Sets the value of the bankTransferInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyTransferInformationTO }
     *     
     */
    public void setBankTransferInformation(MoneyTransferInformationTO value) {
        this.bankTransferInformation = value;
    }

    /**
     * Gets the value of the financialTransactionMedium property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialTransactionMediumTO }
     *     
     */
    public FinancialTransactionMediumTO getFinancialTransactionMedium() {
        return financialTransactionMedium;
    }

    /**
     * Sets the value of the financialTransactionMedium property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialTransactionMediumTO }
     *     
     */
    public void setFinancialTransactionMedium(FinancialTransactionMediumTO value) {
        this.financialTransactionMedium = value;
    }

    /**
     * Sets the value of the directDebitMandate property.
     * 
     * @param directDebitMandate
     *     allowed object is
     *     {@link DirectDebitMandateDetailsTO }
     *     
     */
    public void setDirectDebitMandate(List<DirectDebitMandateDetailsTO> directDebitMandate) {
        this.directDebitMandate = directDebitMandate;
    }

    /**
     * Sets the value of the paymentTransferMethod property.
     * 
     * @param paymentTransferMethod
     *     allowed object is
     *     {@link PaymentTransferMethodTO }
     *     
     */
    public void setPaymentTransferMethod(List<PaymentTransferMethodTO> paymentTransferMethod) {
        this.paymentTransferMethod = paymentTransferMethod;
    }

}
