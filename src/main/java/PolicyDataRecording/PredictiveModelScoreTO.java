
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PredictiveModelScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PredictiveModelScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="finalModelScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="scoreElements" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PredictiveModelScoreElement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PredictiveModelScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "finalModelScore",
    "scoreElements"
})
public class PredictiveModelScoreTO
    extends ScoreTO
{

    protected BigDecimal finalModelScore;
    protected List<PredictiveModelScoreElementTO> scoreElements;

    /**
     * Gets the value of the finalModelScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFinalModelScore() {
        return finalModelScore;
    }

    /**
     * Sets the value of the finalModelScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFinalModelScore(BigDecimal value) {
        this.finalModelScore = value;
    }

    /**
     * Gets the value of the scoreElements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scoreElements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScoreElements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PredictiveModelScoreElementTO }
     * 
     * 
     */
    public List<PredictiveModelScoreElementTO> getScoreElements() {
        if (scoreElements == null) {
            scoreElements = new ArrayList<PredictiveModelScoreElementTO>();
        }
        return this.scoreElements;
    }

    /**
     * Sets the value of the scoreElements property.
     * 
     * @param scoreElements
     *     allowed object is
     *     {@link PredictiveModelScoreElementTO }
     *     
     */
    public void setScoreElements(List<PredictiveModelScoreElementTO> scoreElements) {
        this.scoreElements = scoreElements;
    }

}
