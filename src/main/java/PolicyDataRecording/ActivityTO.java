
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Activity_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Activity_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="reasonForEnding" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}ReasonForActivityEnding" minOccurs="0"/>
 *         &lt;element name="compositeActivity" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Activity_TO" minOccurs="0"/>
 *         &lt;element name="activityComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Activity_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="priorityLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="place" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *         &lt;element name="location" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}placeusageinactivity_TO" minOccurs="0"/>
 *         &lt;element name="isInsurable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="activitySpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivitySpecification_TO" minOccurs="0"/>
 *         &lt;element name="causedConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="startDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="endDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="originalRequestedDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="requestedDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Activity_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "name",
    "description",
    "startDate",
    "endDate",
    "reasonForEnding",
    "compositeActivity",
    "activityComponents",
    "priorityLevel",
    "place",
    "location",
    "isInsurable",
    "activitySpecification",
    "causedConditions",
    "startDateTime",
    "endDateTime",
    "originalRequestedDateTime",
    "requestedDateTime"
})
@XmlSeeAlso({
    ActivityOccurrenceTO.class,
    GeneralActivityTO.class
})
public class ActivityTO
    extends BusinessModelObjectTO
{

    protected String name;
    protected String description;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected ReasonForActivityEnding reasonForEnding;
    protected ActivityTO compositeActivity;
    protected List<ActivityTO> activityComponents;
    protected String priorityLevel;
    protected PlaceTO place;
    protected PlaceusageinactivityTO location;
    protected Boolean isInsurable;
    protected ActivitySpecificationTO activitySpecification;
    protected List<ConditionTO> causedConditions;
    protected XMLGregorianCalendar startDateTime;
    protected XMLGregorianCalendar endDateTime;
    protected XMLGregorianCalendar originalRequestedDateTime;
    protected XMLGregorianCalendar requestedDateTime;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the reasonForEnding property.
     * 
     * @return
     *     possible object is
     *     {@link ReasonForActivityEnding }
     *     
     */
    public ReasonForActivityEnding getReasonForEnding() {
        return reasonForEnding;
    }

    /**
     * Sets the value of the reasonForEnding property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonForActivityEnding }
     *     
     */
    public void setReasonForEnding(ReasonForActivityEnding value) {
        this.reasonForEnding = value;
    }

    /**
     * Gets the value of the compositeActivity property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityTO }
     *     
     */
    public ActivityTO getCompositeActivity() {
        return compositeActivity;
    }

    /**
     * Sets the value of the compositeActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityTO }
     *     
     */
    public void setCompositeActivity(ActivityTO value) {
        this.compositeActivity = value;
    }

    /**
     * Gets the value of the activityComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activityComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivityComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityTO }
     * 
     * 
     */
    public List<ActivityTO> getActivityComponents() {
        if (activityComponents == null) {
            activityComponents = new ArrayList<ActivityTO>();
        }
        return this.activityComponents;
    }

    /**
     * Gets the value of the priorityLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Sets the value of the priorityLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorityLevel(String value) {
        this.priorityLevel = value;
    }

    /**
     * Gets the value of the place property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getPlace() {
        return place;
    }

    /**
     * Sets the value of the place property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setPlace(PlaceTO value) {
        this.place = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceusageinactivityTO }
     *     
     */
    public PlaceusageinactivityTO getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceusageinactivityTO }
     *     
     */
    public void setLocation(PlaceusageinactivityTO value) {
        this.location = value;
    }

    /**
     * Gets the value of the isInsurable property.
     * This getter has been renamed from isIsInsurable() to getIsInsurable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsInsurable() {
        return isInsurable;
    }

    /**
     * Sets the value of the isInsurable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsInsurable(Boolean value) {
        this.isInsurable = value;
    }

    /**
     * Gets the value of the activitySpecification property.
     * 
     * @return
     *     possible object is
     *     {@link ActivitySpecificationTO }
     *     
     */
    public ActivitySpecificationTO getActivitySpecification() {
        return activitySpecification;
    }

    /**
     * Sets the value of the activitySpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivitySpecificationTO }
     *     
     */
    public void setActivitySpecification(ActivitySpecificationTO value) {
        this.activitySpecification = value;
    }

    /**
     * Gets the value of the causedConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the causedConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCausedConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionTO }
     * 
     * 
     */
    public List<ConditionTO> getCausedConditions() {
        if (causedConditions == null) {
            causedConditions = new ArrayList<ConditionTO>();
        }
        return this.causedConditions;
    }

    /**
     * Gets the value of the startDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDateTime() {
        return startDateTime;
    }

    /**
     * Sets the value of the startDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDateTime(XMLGregorianCalendar value) {
        this.startDateTime = value;
    }

    /**
     * Gets the value of the endDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDateTime() {
        return endDateTime;
    }

    /**
     * Sets the value of the endDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDateTime(XMLGregorianCalendar value) {
        this.endDateTime = value;
    }

    /**
     * Gets the value of the originalRequestedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOriginalRequestedDateTime() {
        return originalRequestedDateTime;
    }

    /**
     * Sets the value of the originalRequestedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOriginalRequestedDateTime(XMLGregorianCalendar value) {
        this.originalRequestedDateTime = value;
    }

    /**
     * Gets the value of the requestedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestedDateTime() {
        return requestedDateTime;
    }

    /**
     * Sets the value of the requestedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestedDateTime(XMLGregorianCalendar value) {
        this.requestedDateTime = value;
    }

    /**
     * Sets the value of the activityComponents property.
     * 
     * @param activityComponents
     *     allowed object is
     *     {@link ActivityTO }
     *     
     */
    public void setActivityComponents(List<ActivityTO> activityComponents) {
        this.activityComponents = activityComponents;
    }

    /**
     * Sets the value of the causedConditions property.
     * 
     * @param causedConditions
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setCausedConditions(List<ConditionTO> causedConditions) {
        this.causedConditions = causedConditions;
    }

}
