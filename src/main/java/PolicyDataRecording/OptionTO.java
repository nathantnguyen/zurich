
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Option_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Option_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Derivative_TO">
 *       &lt;sequence>
 *         &lt;element name="buySellFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="callPutIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="exoticFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="knockIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="maximumNumberOfOptionsExercisable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="minimumNumberOfOptionsExercisable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="optionStandardisedIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="optionUnderlyingAssetIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Option_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "buySellFlag",
    "callPutIndicator",
    "exoticFlag",
    "knockIndicator",
    "maximumNumberOfOptionsExercisable",
    "minimumNumberOfOptionsExercisable",
    "optionStandardisedIndicator",
    "optionUnderlyingAssetIndicator"
})
public class OptionTO
    extends DerivativeTO
{

    protected String buySellFlag;
    protected String callPutIndicator;
    protected Boolean exoticFlag;
    protected String knockIndicator;
    protected BigInteger maximumNumberOfOptionsExercisable;
    protected BigInteger minimumNumberOfOptionsExercisable;
    protected Boolean optionStandardisedIndicator;
    protected String optionUnderlyingAssetIndicator;

    /**
     * Gets the value of the buySellFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuySellFlag() {
        return buySellFlag;
    }

    /**
     * Sets the value of the buySellFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuySellFlag(String value) {
        this.buySellFlag = value;
    }

    /**
     * Gets the value of the callPutIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallPutIndicator() {
        return callPutIndicator;
    }

    /**
     * Sets the value of the callPutIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallPutIndicator(String value) {
        this.callPutIndicator = value;
    }

    /**
     * Gets the value of the exoticFlag property.
     * This getter has been renamed from isExoticFlag() to getExoticFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExoticFlag() {
        return exoticFlag;
    }

    /**
     * Sets the value of the exoticFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExoticFlag(Boolean value) {
        this.exoticFlag = value;
    }

    /**
     * Gets the value of the knockIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKnockIndicator() {
        return knockIndicator;
    }

    /**
     * Sets the value of the knockIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKnockIndicator(String value) {
        this.knockIndicator = value;
    }

    /**
     * Gets the value of the maximumNumberOfOptionsExercisable property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumNumberOfOptionsExercisable() {
        return maximumNumberOfOptionsExercisable;
    }

    /**
     * Sets the value of the maximumNumberOfOptionsExercisable property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumNumberOfOptionsExercisable(BigInteger value) {
        this.maximumNumberOfOptionsExercisable = value;
    }

    /**
     * Gets the value of the minimumNumberOfOptionsExercisable property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumNumberOfOptionsExercisable() {
        return minimumNumberOfOptionsExercisable;
    }

    /**
     * Sets the value of the minimumNumberOfOptionsExercisable property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumNumberOfOptionsExercisable(BigInteger value) {
        this.minimumNumberOfOptionsExercisable = value;
    }

    /**
     * Gets the value of the optionStandardisedIndicator property.
     * This getter has been renamed from isOptionStandardisedIndicator() to getOptionStandardisedIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOptionStandardisedIndicator() {
        return optionStandardisedIndicator;
    }

    /**
     * Sets the value of the optionStandardisedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOptionStandardisedIndicator(Boolean value) {
        this.optionStandardisedIndicator = value;
    }

    /**
     * Gets the value of the optionUnderlyingAssetIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOptionUnderlyingAssetIndicator() {
        return optionUnderlyingAssetIndicator;
    }

    /**
     * Sets the value of the optionUnderlyingAssetIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOptionUnderlyingAssetIndicator(String value) {
        this.optionUnderlyingAssetIndicator = value;
    }

}
