
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Biller_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Biller_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}ServicingChannel_TO">
 *       &lt;sequence>
 *         &lt;element name="daysToEpost" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="prenoteRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Biller_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "daysToEpost",
    "prenoteRequired"
})
public class BillerTO
    extends ServicingChannelTO
{

    protected BigInteger daysToEpost;
    protected Boolean prenoteRequired;

    /**
     * Gets the value of the daysToEpost property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDaysToEpost() {
        return daysToEpost;
    }

    /**
     * Sets the value of the daysToEpost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDaysToEpost(BigInteger value) {
        this.daysToEpost = value;
    }

    /**
     * Gets the value of the prenoteRequired property.
     * This getter has been renamed from isPrenoteRequired() to getPrenoteRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPrenoteRequired() {
        return prenoteRequired;
    }

    /**
     * Sets the value of the prenoteRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrenoteRequired(Boolean value) {
        this.prenoteRequired = value;
    }

}
