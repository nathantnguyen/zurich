
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestSpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestSpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Specification_TO">
 *       &lt;sequence>
 *         &lt;element name="requestBehaviourSpec" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RequestBehaviourSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="compositeRequestSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RequestSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="requestSpecComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RequestSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}SpecificationBuildingBlockStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestSpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "requestBehaviourSpec",
    "compositeRequestSpecs",
    "requestSpecComponents",
    "status"
})
public class RequestSpecTO
    extends SpecificationTO
{

    protected List<RequestBehaviourSpecTO> requestBehaviourSpec;
    protected List<RequestSpecTO> compositeRequestSpecs;
    protected List<RequestSpecTO> requestSpecComponents;
    protected List<SpecificationBuildingBlockStatusTO> status;

    /**
     * Gets the value of the requestBehaviourSpec property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestBehaviourSpec property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestBehaviourSpec().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestBehaviourSpecTO }
     * 
     * 
     */
    public List<RequestBehaviourSpecTO> getRequestBehaviourSpec() {
        if (requestBehaviourSpec == null) {
            requestBehaviourSpec = new ArrayList<RequestBehaviourSpecTO>();
        }
        return this.requestBehaviourSpec;
    }

    /**
     * Gets the value of the compositeRequestSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the compositeRequestSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCompositeRequestSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestSpecTO }
     * 
     * 
     */
    public List<RequestSpecTO> getCompositeRequestSpecs() {
        if (compositeRequestSpecs == null) {
            compositeRequestSpecs = new ArrayList<RequestSpecTO>();
        }
        return this.compositeRequestSpecs;
    }

    /**
     * Gets the value of the requestSpecComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestSpecComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestSpecComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestSpecTO }
     * 
     * 
     */
    public List<RequestSpecTO> getRequestSpecComponents() {
        if (requestSpecComponents == null) {
            requestSpecComponents = new ArrayList<RequestSpecTO>();
        }
        return this.requestSpecComponents;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecificationBuildingBlockStatusTO }
     * 
     * 
     */
    public List<SpecificationBuildingBlockStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<SpecificationBuildingBlockStatusTO>();
        }
        return this.status;
    }

    /**
     * Sets the value of the requestBehaviourSpec property.
     * 
     * @param requestBehaviourSpec
     *     allowed object is
     *     {@link RequestBehaviourSpecTO }
     *     
     */
    public void setRequestBehaviourSpec(List<RequestBehaviourSpecTO> requestBehaviourSpec) {
        this.requestBehaviourSpec = requestBehaviourSpec;
    }

    /**
     * Sets the value of the compositeRequestSpecs property.
     * 
     * @param compositeRequestSpecs
     *     allowed object is
     *     {@link RequestSpecTO }
     *     
     */
    public void setCompositeRequestSpecs(List<RequestSpecTO> compositeRequestSpecs) {
        this.compositeRequestSpecs = compositeRequestSpecs;
    }

    /**
     * Sets the value of the requestSpecComponents property.
     * 
     * @param requestSpecComponents
     *     allowed object is
     *     {@link RequestSpecTO }
     *     
     */
    public void setRequestSpecComponents(List<RequestSpecTO> requestSpecComponents) {
        this.requestSpecComponents = requestSpecComponents;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link SpecificationBuildingBlockStatusTO }
     *     
     */
    public void setStatus(List<SpecificationBuildingBlockStatusTO> status) {
        this.status = status;
    }

}
