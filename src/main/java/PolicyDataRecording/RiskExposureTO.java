
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RiskExposure_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskExposure_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskExposureStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="aggregatedRiskExposures" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskExposure_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="rolesInRiskExposure" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RoleInRiskExposure_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="exposedPerilTypes" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="lossEstimates" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskPosition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskExposure_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "startDate",
    "endDate",
    "status",
    "aggregatedRiskExposures",
    "amount",
    "rolesInRiskExposure",
    "exposedPerilTypes",
    "lossEstimates",
    "description",
    "externalReference",
    "alternateReference"
})
@XmlSeeAlso({
    RiskHazardTO.class
})
public class RiskExposureTO
    extends BusinessModelObjectTO
{

    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected List<RiskExposureStatusTO> status;
    protected List<RiskExposureTO> aggregatedRiskExposures;
    protected BaseCurrencyAmount amount;
    protected List<RoleInRiskExposureTO> rolesInRiskExposure;
    protected List<TypeTO> exposedPerilTypes;
    protected List<RiskPositionTO> lossEstimates;
    protected String description;
    protected String externalReference;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskExposureStatusTO }
     * 
     * 
     */
    public List<RiskExposureStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<RiskExposureStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the aggregatedRiskExposures property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aggregatedRiskExposures property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAggregatedRiskExposures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskExposureTO }
     * 
     * 
     */
    public List<RiskExposureTO> getAggregatedRiskExposures() {
        if (aggregatedRiskExposures == null) {
            aggregatedRiskExposures = new ArrayList<RiskExposureTO>();
        }
        return this.aggregatedRiskExposures;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAmount(BaseCurrencyAmount value) {
        this.amount = value;
    }

    /**
     * Gets the value of the rolesInRiskExposure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInRiskExposure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInRiskExposure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInRiskExposureTO }
     * 
     * 
     */
    public List<RoleInRiskExposureTO> getRolesInRiskExposure() {
        if (rolesInRiskExposure == null) {
            rolesInRiskExposure = new ArrayList<RoleInRiskExposureTO>();
        }
        return this.rolesInRiskExposure;
    }

    /**
     * Gets the value of the exposedPerilTypes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exposedPerilTypes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExposedPerilTypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeTO }
     * 
     * 
     */
    public List<TypeTO> getExposedPerilTypes() {
        if (exposedPerilTypes == null) {
            exposedPerilTypes = new ArrayList<TypeTO>();
        }
        return this.exposedPerilTypes;
    }

    /**
     * Gets the value of the lossEstimates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lossEstimates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLossEstimates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskPositionTO }
     * 
     * 
     */
    public List<RiskPositionTO> getLossEstimates() {
        if (lossEstimates == null) {
            lossEstimates = new ArrayList<RiskPositionTO>();
        }
        return this.lossEstimates;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link RiskExposureStatusTO }
     *     
     */
    public void setStatus(List<RiskExposureStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the aggregatedRiskExposures property.
     * 
     * @param aggregatedRiskExposures
     *     allowed object is
     *     {@link RiskExposureTO }
     *     
     */
    public void setAggregatedRiskExposures(List<RiskExposureTO> aggregatedRiskExposures) {
        this.aggregatedRiskExposures = aggregatedRiskExposures;
    }

    /**
     * Sets the value of the rolesInRiskExposure property.
     * 
     * @param rolesInRiskExposure
     *     allowed object is
     *     {@link RoleInRiskExposureTO }
     *     
     */
    public void setRolesInRiskExposure(List<RoleInRiskExposureTO> rolesInRiskExposure) {
        this.rolesInRiskExposure = rolesInRiskExposure;
    }

    /**
     * Sets the value of the exposedPerilTypes property.
     * 
     * @param exposedPerilTypes
     *     allowed object is
     *     {@link TypeTO }
     *     
     */
    public void setExposedPerilTypes(List<TypeTO> exposedPerilTypes) {
        this.exposedPerilTypes = exposedPerilTypes;
    }

    /**
     * Sets the value of the lossEstimates property.
     * 
     * @param lossEstimates
     *     allowed object is
     *     {@link RiskPositionTO }
     *     
     */
    public void setLossEstimates(List<RiskPositionTO> lossEstimates) {
        this.lossEstimates = lossEstimates;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
