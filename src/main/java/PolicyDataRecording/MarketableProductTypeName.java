
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketableProductTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MarketableProductTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Group Product"/>
 *     &lt;enumeration value="Life Insurance Product"/>
 *     &lt;enumeration value="Policy Loan Product"/>
 *     &lt;enumeration value="Insurance Product"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MarketableProductTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum MarketableProductTypeName {

    @XmlEnumValue("Group Product")
    GROUP_PRODUCT("Group Product"),
    @XmlEnumValue("Life Insurance Product")
    LIFE_INSURANCE_PRODUCT("Life Insurance Product"),
    @XmlEnumValue("Policy Loan Product")
    POLICY_LOAN_PRODUCT("Policy Loan Product"),
    @XmlEnumValue("Insurance Product")
    INSURANCE_PRODUCT("Insurance Product");
    private final String value;

    MarketableProductTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MarketableProductTypeName fromValue(String v) {
        for (MarketableProductTypeName c: MarketableProductTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
