
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Calculation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Calculation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}SpecificationBuildingBlock_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="inputParameters" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}NavigationPath_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="outputParameters" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}NavigationPath_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="formalDefinition" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Algorithm_TO" minOccurs="0"/>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/}CalculationSpecificationKind" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Calculation_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "name",
    "inputParameters",
    "outputParameters",
    "formalDefinition",
    "kind"
})
public class CalculationTO
    extends SpecificationBuildingBlockTO
{

    protected String name;
    protected List<NavigationPathTO> inputParameters;
    protected List<NavigationPathTO> outputParameters;
    protected AlgorithmTO formalDefinition;
    protected CalculationSpecificationKind kind;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the inputParameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inputParameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInputParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NavigationPathTO }
     * 
     * 
     */
    public List<NavigationPathTO> getInputParameters() {
        if (inputParameters == null) {
            inputParameters = new ArrayList<NavigationPathTO>();
        }
        return this.inputParameters;
    }

    /**
     * Gets the value of the outputParameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the outputParameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOutputParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NavigationPathTO }
     * 
     * 
     */
    public List<NavigationPathTO> getOutputParameters() {
        if (outputParameters == null) {
            outputParameters = new ArrayList<NavigationPathTO>();
        }
        return this.outputParameters;
    }

    /**
     * Gets the value of the formalDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link AlgorithmTO }
     *     
     */
    public AlgorithmTO getFormalDefinition() {
        return formalDefinition;
    }

    /**
     * Sets the value of the formalDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link AlgorithmTO }
     *     
     */
    public void setFormalDefinition(AlgorithmTO value) {
        this.formalDefinition = value;
    }

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link CalculationSpecificationKind }
     *     
     */
    public CalculationSpecificationKind getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalculationSpecificationKind }
     *     
     */
    public void setKind(CalculationSpecificationKind value) {
        this.kind = value;
    }

    /**
     * Sets the value of the inputParameters property.
     * 
     * @param inputParameters
     *     allowed object is
     *     {@link NavigationPathTO }
     *     
     */
    public void setInputParameters(List<NavigationPathTO> inputParameters) {
        this.inputParameters = inputParameters;
    }

    /**
     * Sets the value of the outputParameters property.
     * 
     * @param outputParameters
     *     allowed object is
     *     {@link NavigationPathTO }
     *     
     */
    public void setOutputParameters(List<NavigationPathTO> outputParameters) {
        this.outputParameters = outputParameters;
    }

}
