
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SanctionsTransactions_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SanctionsTransactions_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="sanctionsTransaction" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskAssessment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SanctionsTransactions_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "sanctionsTransaction"
})
public class SanctionsTransactionsTO
    extends BusinessModelObjectTO
{

    protected List<RiskAssessmentTO> sanctionsTransaction;

    /**
     * Gets the value of the sanctionsTransaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sanctionsTransaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSanctionsTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskAssessmentTO }
     * 
     * 
     */
    public List<RiskAssessmentTO> getSanctionsTransaction() {
        if (sanctionsTransaction == null) {
            sanctionsTransaction = new ArrayList<RiskAssessmentTO>();
        }
        return this.sanctionsTransaction;
    }

    /**
     * Sets the value of the sanctionsTransaction property.
     * 
     * @param sanctionsTransaction
     *     allowed object is
     *     {@link RiskAssessmentTO }
     *     
     */
    public void setSanctionsTransaction(List<RiskAssessmentTO> sanctionsTransaction) {
        this.sanctionsTransaction = sanctionsTransaction;
    }

}
