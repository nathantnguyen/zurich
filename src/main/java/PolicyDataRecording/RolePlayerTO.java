
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RolePlayer_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RolePlayer_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="allNames" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyName_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="partyRoles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRole_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="needsProfiles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GoalAndNeed_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contactPreferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPreference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="performedActivity" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeneralActivity_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="suspectDuplicate" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}SuspectDuplicate_TO" minOccurs="0"/>
 *         &lt;element name="relatedFromRolePlayers" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerRelationship_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="relatedToRolePlayers" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerRelationship_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolePlayerScores" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerScore_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="assessmentResults" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="defaultName" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyName_TO" minOccurs="0"/>
 *         &lt;element name="goalsAndNeeds" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GoalAndNeed_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="involvedLifeEvents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}LifeEvent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInRolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInRolePlayer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RolePlayer_TO", propOrder = {
    "description",
    "allNames",
    "partyRoles",
    "needsProfiles",
    "contactPreferences",
    "performedActivity",
    "suspectDuplicate",
    "relatedFromRolePlayers",
    "relatedToRolePlayers",
    "rolePlayerScores",
    "assessmentResults",
    "defaultName",
    "goalsAndNeeds",
    "involvedLifeEvents",
    "rolesInRolePlayer"
})
@XmlSeeAlso({
    VirtualPartyTO.class,
    PartyRoleTO.class,
    PartyTO.class
})
public class RolePlayerTO
    extends BusinessModelObjectTO
{

    protected String description;
    protected List<PartyNameTO> allNames;
    protected List<PartyRoleTO> partyRoles;
    protected List<GoalAndNeedTO> needsProfiles;
    protected List<ContactPreferenceTO> contactPreferences;
    protected List<GeneralActivityTO> performedActivity;
    protected SuspectDuplicateTO suspectDuplicate;
    protected List<RolePlayerRelationshipTO> relatedFromRolePlayers;
    protected List<RolePlayerRelationshipTO> relatedToRolePlayers;
    protected List<RolePlayerScoreTO> rolePlayerScores;
    protected List<AssessmentResultTO> assessmentResults;
    protected PartyNameTO defaultName;
    protected List<GoalAndNeedTO> goalsAndNeeds;
    protected List<LifeEventTO> involvedLifeEvents;
    protected List<RoleInRolePlayerTO> rolesInRolePlayer;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the allNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyNameTO }
     * 
     * 
     */
    public List<PartyNameTO> getAllNames() {
        if (allNames == null) {
            allNames = new ArrayList<PartyNameTO>();
        }
        return this.allNames;
    }

    /**
     * Gets the value of the partyRoles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyRoles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyRoleTO }
     * 
     * 
     */
    public List<PartyRoleTO> getPartyRoles() {
        if (partyRoles == null) {
            partyRoles = new ArrayList<PartyRoleTO>();
        }
        return this.partyRoles;
    }

    /**
     * Gets the value of the needsProfiles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the needsProfiles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNeedsProfiles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GoalAndNeedTO }
     * 
     * 
     */
    public List<GoalAndNeedTO> getNeedsProfiles() {
        if (needsProfiles == null) {
            needsProfiles = new ArrayList<GoalAndNeedTO>();
        }
        return this.needsProfiles;
    }

    /**
     * Gets the value of the contactPreferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactPreferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactPreferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactPreferenceTO }
     * 
     * 
     */
    public List<ContactPreferenceTO> getContactPreferences() {
        if (contactPreferences == null) {
            contactPreferences = new ArrayList<ContactPreferenceTO>();
        }
        return this.contactPreferences;
    }

    /**
     * Gets the value of the performedActivity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the performedActivity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPerformedActivity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeneralActivityTO }
     * 
     * 
     */
    public List<GeneralActivityTO> getPerformedActivity() {
        if (performedActivity == null) {
            performedActivity = new ArrayList<GeneralActivityTO>();
        }
        return this.performedActivity;
    }

    /**
     * Gets the value of the suspectDuplicate property.
     * 
     * @return
     *     possible object is
     *     {@link SuspectDuplicateTO }
     *     
     */
    public SuspectDuplicateTO getSuspectDuplicate() {
        return suspectDuplicate;
    }

    /**
     * Sets the value of the suspectDuplicate property.
     * 
     * @param value
     *     allowed object is
     *     {@link SuspectDuplicateTO }
     *     
     */
    public void setSuspectDuplicate(SuspectDuplicateTO value) {
        this.suspectDuplicate = value;
    }

    /**
     * Gets the value of the relatedFromRolePlayers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedFromRolePlayers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedFromRolePlayers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerRelationshipTO }
     * 
     * 
     */
    public List<RolePlayerRelationshipTO> getRelatedFromRolePlayers() {
        if (relatedFromRolePlayers == null) {
            relatedFromRolePlayers = new ArrayList<RolePlayerRelationshipTO>();
        }
        return this.relatedFromRolePlayers;
    }

    /**
     * Gets the value of the relatedToRolePlayers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedToRolePlayers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedToRolePlayers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerRelationshipTO }
     * 
     * 
     */
    public List<RolePlayerRelationshipTO> getRelatedToRolePlayers() {
        if (relatedToRolePlayers == null) {
            relatedToRolePlayers = new ArrayList<RolePlayerRelationshipTO>();
        }
        return this.relatedToRolePlayers;
    }

    /**
     * Gets the value of the rolePlayerScores property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolePlayerScores property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolePlayerScores().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerScoreTO }
     * 
     * 
     */
    public List<RolePlayerScoreTO> getRolePlayerScores() {
        if (rolePlayerScores == null) {
            rolePlayerScores = new ArrayList<RolePlayerScoreTO>();
        }
        return this.rolePlayerScores;
    }

    /**
     * Gets the value of the assessmentResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assessmentResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssessmentResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssessmentResultTO }
     * 
     * 
     */
    public List<AssessmentResultTO> getAssessmentResults() {
        if (assessmentResults == null) {
            assessmentResults = new ArrayList<AssessmentResultTO>();
        }
        return this.assessmentResults;
    }

    /**
     * Gets the value of the defaultName property.
     * 
     * @return
     *     possible object is
     *     {@link PartyNameTO }
     *     
     */
    public PartyNameTO getDefaultName() {
        return defaultName;
    }

    /**
     * Sets the value of the defaultName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyNameTO }
     *     
     */
    public void setDefaultName(PartyNameTO value) {
        this.defaultName = value;
    }

    /**
     * Gets the value of the goalsAndNeeds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the goalsAndNeeds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGoalsAndNeeds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GoalAndNeedTO }
     * 
     * 
     */
    public List<GoalAndNeedTO> getGoalsAndNeeds() {
        if (goalsAndNeeds == null) {
            goalsAndNeeds = new ArrayList<GoalAndNeedTO>();
        }
        return this.goalsAndNeeds;
    }

    /**
     * Gets the value of the involvedLifeEvents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedLifeEvents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedLifeEvents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LifeEventTO }
     * 
     * 
     */
    public List<LifeEventTO> getInvolvedLifeEvents() {
        if (involvedLifeEvents == null) {
            involvedLifeEvents = new ArrayList<LifeEventTO>();
        }
        return this.involvedLifeEvents;
    }

    /**
     * Gets the value of the rolesInRolePlayer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInRolePlayer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInRolePlayer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInRolePlayerTO }
     * 
     * 
     */
    public List<RoleInRolePlayerTO> getRolesInRolePlayer() {
        if (rolesInRolePlayer == null) {
            rolesInRolePlayer = new ArrayList<RoleInRolePlayerTO>();
        }
        return this.rolesInRolePlayer;
    }

    /**
     * Sets the value of the allNames property.
     * 
     * @param allNames
     *     allowed object is
     *     {@link PartyNameTO }
     *     
     */
    public void setAllNames(List<PartyNameTO> allNames) {
        this.allNames = allNames;
    }

    /**
     * Sets the value of the partyRoles property.
     * 
     * @param partyRoles
     *     allowed object is
     *     {@link PartyRoleTO }
     *     
     */
    public void setPartyRoles(List<PartyRoleTO> partyRoles) {
        this.partyRoles = partyRoles;
    }

    /**
     * Sets the value of the needsProfiles property.
     * 
     * @param needsProfiles
     *     allowed object is
     *     {@link GoalAndNeedTO }
     *     
     */
    public void setNeedsProfiles(List<GoalAndNeedTO> needsProfiles) {
        this.needsProfiles = needsProfiles;
    }

    /**
     * Sets the value of the contactPreferences property.
     * 
     * @param contactPreferences
     *     allowed object is
     *     {@link ContactPreferenceTO }
     *     
     */
    public void setContactPreferences(List<ContactPreferenceTO> contactPreferences) {
        this.contactPreferences = contactPreferences;
    }

    /**
     * Sets the value of the performedActivity property.
     * 
     * @param performedActivity
     *     allowed object is
     *     {@link GeneralActivityTO }
     *     
     */
    public void setPerformedActivity(List<GeneralActivityTO> performedActivity) {
        this.performedActivity = performedActivity;
    }

    /**
     * Sets the value of the relatedFromRolePlayers property.
     * 
     * @param relatedFromRolePlayers
     *     allowed object is
     *     {@link RolePlayerRelationshipTO }
     *     
     */
    public void setRelatedFromRolePlayers(List<RolePlayerRelationshipTO> relatedFromRolePlayers) {
        this.relatedFromRolePlayers = relatedFromRolePlayers;
    }

    /**
     * Sets the value of the relatedToRolePlayers property.
     * 
     * @param relatedToRolePlayers
     *     allowed object is
     *     {@link RolePlayerRelationshipTO }
     *     
     */
    public void setRelatedToRolePlayers(List<RolePlayerRelationshipTO> relatedToRolePlayers) {
        this.relatedToRolePlayers = relatedToRolePlayers;
    }

    /**
     * Sets the value of the rolePlayerScores property.
     * 
     * @param rolePlayerScores
     *     allowed object is
     *     {@link RolePlayerScoreTO }
     *     
     */
    public void setRolePlayerScores(List<RolePlayerScoreTO> rolePlayerScores) {
        this.rolePlayerScores = rolePlayerScores;
    }

    /**
     * Sets the value of the assessmentResults property.
     * 
     * @param assessmentResults
     *     allowed object is
     *     {@link AssessmentResultTO }
     *     
     */
    public void setAssessmentResults(List<AssessmentResultTO> assessmentResults) {
        this.assessmentResults = assessmentResults;
    }

    /**
     * Sets the value of the goalsAndNeeds property.
     * 
     * @param goalsAndNeeds
     *     allowed object is
     *     {@link GoalAndNeedTO }
     *     
     */
    public void setGoalsAndNeeds(List<GoalAndNeedTO> goalsAndNeeds) {
        this.goalsAndNeeds = goalsAndNeeds;
    }

    /**
     * Sets the value of the involvedLifeEvents property.
     * 
     * @param involvedLifeEvents
     *     allowed object is
     *     {@link LifeEventTO }
     *     
     */
    public void setInvolvedLifeEvents(List<LifeEventTO> involvedLifeEvents) {
        this.involvedLifeEvents = involvedLifeEvents;
    }

    /**
     * Sets the value of the rolesInRolePlayer property.
     * 
     * @param rolesInRolePlayer
     *     allowed object is
     *     {@link RoleInRolePlayerTO }
     *     
     */
    public void setRolesInRolePlayer(List<RoleInRolePlayerTO> rolesInRolePlayer) {
        this.rolesInRolePlayer = rolesInRolePlayer;
    }

}
