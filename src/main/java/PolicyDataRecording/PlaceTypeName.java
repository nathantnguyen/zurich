
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlaceTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlaceTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Flood Area"/>
 *     &lt;enumeration value="Country Element"/>
 *     &lt;enumeration value="Country"/>
 *     &lt;enumeration value="Municipality"/>
 *     &lt;enumeration value="City"/>
 *     &lt;enumeration value="Village"/>
 *     &lt;enumeration value="Region"/>
 *     &lt;enumeration value="County"/>
 *     &lt;enumeration value="Province"/>
 *     &lt;enumeration value="State"/>
 *     &lt;enumeration value="Risk Area"/>
 *     &lt;enumeration value="CensusStatistical Area"/>
 *     &lt;enumeration value="Postal Code"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PlaceTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum PlaceTypeName {

    @XmlEnumValue("Flood Area")
    FLOOD_AREA("Flood Area"),
    @XmlEnumValue("Country Element")
    COUNTRY_ELEMENT("Country Element"),
    @XmlEnumValue("Country")
    COUNTRY("Country"),
    @XmlEnumValue("Municipality")
    MUNICIPALITY("Municipality"),
    @XmlEnumValue("City")
    CITY("City"),
    @XmlEnumValue("Village")
    VILLAGE("Village"),
    @XmlEnumValue("Region")
    REGION("Region"),
    @XmlEnumValue("County")
    COUNTY("County"),
    @XmlEnumValue("Province")
    PROVINCE("Province"),
    @XmlEnumValue("State")
    STATE("State"),
    @XmlEnumValue("Risk Area")
    RISK_AREA("Risk Area"),
    @XmlEnumValue("CensusStatistical Area")
    CENSUS_STATISTICAL_AREA("CensusStatistical Area"),
    @XmlEnumValue("Postal Code")
    POSTAL_CODE("Postal Code");
    private final String value;

    PlaceTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlaceTypeName fromValue(String v) {
        for (PlaceTypeName c: PlaceTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
