
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ChannelRole_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChannelRole_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}PartyInvolvedInIntermediaryAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="independenceCategory" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/}IndependenceCategory" minOccurs="0"/>
 *         &lt;element name="authorityLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/}AuthorityLevel" minOccurs="0"/>
 *         &lt;element name="underwritingAuthority" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/}UnderwritingAuthority" minOccurs="0"/>
 *         &lt;element name="underwritingAuthorityStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="claimsManagementAuthority" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="educationSupport" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="infrastructureSupport" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="marketingSupport" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="commercialTargets" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="trainingLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/}TrainingLevel" minOccurs="0"/>
 *         &lt;element name="prospectsQuota" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="level" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/}SeniorityLevel" minOccurs="0"/>
 *         &lt;element name="targetMarketSegment" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}MarketSegment_TO" minOccurs="0"/>
 *         &lt;element name="salesTerritories" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prospectingTerritories" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="distributionLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/}DistributionLevel" minOccurs="0"/>
 *         &lt;element name="channelRoleRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}ChannelRoleType_TO" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChannelRole_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/", propOrder = {
    "independenceCategory",
    "authorityLevel",
    "underwritingAuthority",
    "underwritingAuthorityStartDate",
    "claimsManagementAuthority",
    "educationSupport",
    "infrastructureSupport",
    "marketingSupport",
    "commercialTargets",
    "externalReference",
    "trainingLevel",
    "prospectsQuota",
    "level",
    "targetMarketSegment",
    "salesTerritories",
    "prospectingTerritories",
    "distributionLevel",
    "channelRoleRootType",
    "alternateReference"
})
public class ChannelRoleTO
    extends PartyInvolvedInIntermediaryAgreementTO
{

    protected IndependenceCategory independenceCategory;
    protected AuthorityLevel authorityLevel;
    protected UnderwritingAuthority underwritingAuthority;
    protected XMLGregorianCalendar underwritingAuthorityStartDate;
    protected String claimsManagementAuthority;
    protected String educationSupport;
    protected String infrastructureSupport;
    protected String marketingSupport;
    protected String commercialTargets;
    protected String externalReference;
    protected TrainingLevel trainingLevel;
    protected BigInteger prospectsQuota;
    protected SeniorityLevel level;
    protected MarketSegmentTO targetMarketSegment;
    protected List<PlaceTO> salesTerritories;
    protected List<PlaceTO> prospectingTerritories;
    protected DistributionLevel distributionLevel;
    protected ChannelRoleTypeTO channelRoleRootType;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the independenceCategory property.
     * 
     * @return
     *     possible object is
     *     {@link IndependenceCategory }
     *     
     */
    public IndependenceCategory getIndependenceCategory() {
        return independenceCategory;
    }

    /**
     * Sets the value of the independenceCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndependenceCategory }
     *     
     */
    public void setIndependenceCategory(IndependenceCategory value) {
        this.independenceCategory = value;
    }

    /**
     * Gets the value of the authorityLevel property.
     * 
     * @return
     *     possible object is
     *     {@link AuthorityLevel }
     *     
     */
    public AuthorityLevel getAuthorityLevel() {
        return authorityLevel;
    }

    /**
     * Sets the value of the authorityLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorityLevel }
     *     
     */
    public void setAuthorityLevel(AuthorityLevel value) {
        this.authorityLevel = value;
    }

    /**
     * Gets the value of the underwritingAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link UnderwritingAuthority }
     *     
     */
    public UnderwritingAuthority getUnderwritingAuthority() {
        return underwritingAuthority;
    }

    /**
     * Sets the value of the underwritingAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnderwritingAuthority }
     *     
     */
    public void setUnderwritingAuthority(UnderwritingAuthority value) {
        this.underwritingAuthority = value;
    }

    /**
     * Gets the value of the underwritingAuthorityStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUnderwritingAuthorityStartDate() {
        return underwritingAuthorityStartDate;
    }

    /**
     * Sets the value of the underwritingAuthorityStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUnderwritingAuthorityStartDate(XMLGregorianCalendar value) {
        this.underwritingAuthorityStartDate = value;
    }

    /**
     * Gets the value of the claimsManagementAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimsManagementAuthority() {
        return claimsManagementAuthority;
    }

    /**
     * Sets the value of the claimsManagementAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimsManagementAuthority(String value) {
        this.claimsManagementAuthority = value;
    }

    /**
     * Gets the value of the educationSupport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEducationSupport() {
        return educationSupport;
    }

    /**
     * Sets the value of the educationSupport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEducationSupport(String value) {
        this.educationSupport = value;
    }

    /**
     * Gets the value of the infrastructureSupport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfrastructureSupport() {
        return infrastructureSupport;
    }

    /**
     * Sets the value of the infrastructureSupport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfrastructureSupport(String value) {
        this.infrastructureSupport = value;
    }

    /**
     * Gets the value of the marketingSupport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketingSupport() {
        return marketingSupport;
    }

    /**
     * Sets the value of the marketingSupport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketingSupport(String value) {
        this.marketingSupport = value;
    }

    /**
     * Gets the value of the commercialTargets property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialTargets() {
        return commercialTargets;
    }

    /**
     * Sets the value of the commercialTargets property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialTargets(String value) {
        this.commercialTargets = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the trainingLevel property.
     * 
     * @return
     *     possible object is
     *     {@link TrainingLevel }
     *     
     */
    public TrainingLevel getTrainingLevel() {
        return trainingLevel;
    }

    /**
     * Sets the value of the trainingLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrainingLevel }
     *     
     */
    public void setTrainingLevel(TrainingLevel value) {
        this.trainingLevel = value;
    }

    /**
     * Gets the value of the prospectsQuota property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getProspectsQuota() {
        return prospectsQuota;
    }

    /**
     * Sets the value of the prospectsQuota property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setProspectsQuota(BigInteger value) {
        this.prospectsQuota = value;
    }

    /**
     * Gets the value of the level property.
     * 
     * @return
     *     possible object is
     *     {@link SeniorityLevel }
     *     
     */
    public SeniorityLevel getLevel() {
        return level;
    }

    /**
     * Sets the value of the level property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeniorityLevel }
     *     
     */
    public void setLevel(SeniorityLevel value) {
        this.level = value;
    }

    /**
     * Gets the value of the targetMarketSegment property.
     * 
     * @return
     *     possible object is
     *     {@link MarketSegmentTO }
     *     
     */
    public MarketSegmentTO getTargetMarketSegment() {
        return targetMarketSegment;
    }

    /**
     * Sets the value of the targetMarketSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketSegmentTO }
     *     
     */
    public void setTargetMarketSegment(MarketSegmentTO value) {
        this.targetMarketSegment = value;
    }

    /**
     * Gets the value of the salesTerritories property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the salesTerritories property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSalesTerritories().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlaceTO }
     * 
     * 
     */
    public List<PlaceTO> getSalesTerritories() {
        if (salesTerritories == null) {
            salesTerritories = new ArrayList<PlaceTO>();
        }
        return this.salesTerritories;
    }

    /**
     * Gets the value of the prospectingTerritories property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prospectingTerritories property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProspectingTerritories().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlaceTO }
     * 
     * 
     */
    public List<PlaceTO> getProspectingTerritories() {
        if (prospectingTerritories == null) {
            prospectingTerritories = new ArrayList<PlaceTO>();
        }
        return this.prospectingTerritories;
    }

    /**
     * Gets the value of the distributionLevel property.
     * 
     * @return
     *     possible object is
     *     {@link DistributionLevel }
     *     
     */
    public DistributionLevel getDistributionLevel() {
        return distributionLevel;
    }

    /**
     * Sets the value of the distributionLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link DistributionLevel }
     *     
     */
    public void setDistributionLevel(DistributionLevel value) {
        this.distributionLevel = value;
    }

    /**
     * Gets the value of the channelRoleRootType property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelRoleTypeTO }
     *     
     */
    public ChannelRoleTypeTO getChannelRoleRootType() {
        return channelRoleRootType;
    }

    /**
     * Sets the value of the channelRoleRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelRoleTypeTO }
     *     
     */
    public void setChannelRoleRootType(ChannelRoleTypeTO value) {
        this.channelRoleRootType = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the salesTerritories property.
     * 
     * @param salesTerritories
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setSalesTerritories(List<PlaceTO> salesTerritories) {
        this.salesTerritories = salesTerritories;
    }

    /**
     * Sets the value of the prospectingTerritories property.
     * 
     * @param prospectingTerritories
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setProspectingTerritories(List<PlaceTO> prospectingTerritories) {
        this.prospectingTerritories = prospectingTerritories;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
