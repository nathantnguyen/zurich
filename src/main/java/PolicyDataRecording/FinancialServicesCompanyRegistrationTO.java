
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialServicesCompanyRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesCompanyRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CompanyRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="registeredSellableProductGroup" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ProductGroup_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="productGroups" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ProductGroup_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesCompanyRegistration_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "registeredSellableProductGroup",
    "productGroups"
})
public class FinancialServicesCompanyRegistrationTO
    extends CompanyRegistrationTO
{

    protected List<ProductGroupTO> registeredSellableProductGroup;
    protected List<ProductGroupTO> productGroups;

    /**
     * Gets the value of the registeredSellableProductGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the registeredSellableProductGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegisteredSellableProductGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductGroupTO }
     * 
     * 
     */
    public List<ProductGroupTO> getRegisteredSellableProductGroup() {
        if (registeredSellableProductGroup == null) {
            registeredSellableProductGroup = new ArrayList<ProductGroupTO>();
        }
        return this.registeredSellableProductGroup;
    }

    /**
     * Gets the value of the productGroups property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productGroups property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductGroups().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductGroupTO }
     * 
     * 
     */
    public List<ProductGroupTO> getProductGroups() {
        if (productGroups == null) {
            productGroups = new ArrayList<ProductGroupTO>();
        }
        return this.productGroups;
    }

    /**
     * Sets the value of the registeredSellableProductGroup property.
     * 
     * @param registeredSellableProductGroup
     *     allowed object is
     *     {@link ProductGroupTO }
     *     
     */
    public void setRegisteredSellableProductGroup(List<ProductGroupTO> registeredSellableProductGroup) {
        this.registeredSellableProductGroup = registeredSellableProductGroup;
    }

    /**
     * Sets the value of the productGroups property.
     * 
     * @param productGroups
     *     allowed object is
     *     {@link ProductGroupTO }
     *     
     */
    public void setProductGroups(List<ProductGroupTO> productGroups) {
        this.productGroups = productGroups;
    }

}
