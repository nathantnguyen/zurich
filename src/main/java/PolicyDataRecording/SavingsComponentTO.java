
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SavingsComponent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SavingsComponent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreementComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="purpose" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}SavingsPurpose" minOccurs="0"/>
 *         &lt;element name="interestCompoundingFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}InterestCompoundingFrequency" minOccurs="0"/>
 *         &lt;element name="estimatedGrowthRate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="surrenderChargeEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="investmentProfileSelectionMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}InvestmentProfileSelectionMethod" minOccurs="0"/>
 *         &lt;element name="accumulationPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="installedSavingsComponent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}SavingsComponent_TO" minOccurs="0"/>
 *         &lt;element name="repaidSavingsComponent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}SavingsComponent_TO" minOccurs="0"/>
 *         &lt;element name="providedSavingsComponent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}SavingsComponent_TO" minOccurs="0"/>
 *         &lt;element name="surrenderCharge" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="surrenderChargeTargetPremium" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="combinedAge" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="holdingFund" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Fund_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SavingsComponent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "purpose",
    "interestCompoundingFrequency",
    "estimatedGrowthRate",
    "surrenderChargeEndDate",
    "investmentProfileSelectionMethod",
    "accumulationPeriod",
    "installedSavingsComponent",
    "repaidSavingsComponent",
    "providedSavingsComponent",
    "surrenderCharge",
    "surrenderChargeTargetPremium",
    "combinedAge",
    "holdingFund"
})
public class SavingsComponentTO
    extends FinancialServicesAgreementComponentTO
{

    protected SavingsPurpose purpose;
    protected InterestCompoundingFrequency interestCompoundingFrequency;
    protected BigDecimal estimatedGrowthRate;
    protected XMLGregorianCalendar surrenderChargeEndDate;
    protected InvestmentProfileSelectionMethod investmentProfileSelectionMethod;
    protected Duration accumulationPeriod;
    protected SavingsComponentTO installedSavingsComponent;
    protected SavingsComponentTO repaidSavingsComponent;
    protected SavingsComponentTO providedSavingsComponent;
    protected BaseCurrencyAmount surrenderCharge;
    protected BaseCurrencyAmount surrenderChargeTargetPremium;
    protected BigDecimal combinedAge;
    protected FundTO holdingFund;

    /**
     * Gets the value of the purpose property.
     * 
     * @return
     *     possible object is
     *     {@link SavingsPurpose }
     *     
     */
    public SavingsPurpose getPurpose() {
        return purpose;
    }

    /**
     * Sets the value of the purpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link SavingsPurpose }
     *     
     */
    public void setPurpose(SavingsPurpose value) {
        this.purpose = value;
    }

    /**
     * Gets the value of the interestCompoundingFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link InterestCompoundingFrequency }
     *     
     */
    public InterestCompoundingFrequency getInterestCompoundingFrequency() {
        return interestCompoundingFrequency;
    }

    /**
     * Sets the value of the interestCompoundingFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestCompoundingFrequency }
     *     
     */
    public void setInterestCompoundingFrequency(InterestCompoundingFrequency value) {
        this.interestCompoundingFrequency = value;
    }

    /**
     * Gets the value of the estimatedGrowthRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedGrowthRate() {
        return estimatedGrowthRate;
    }

    /**
     * Sets the value of the estimatedGrowthRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedGrowthRate(BigDecimal value) {
        this.estimatedGrowthRate = value;
    }

    /**
     * Gets the value of the surrenderChargeEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSurrenderChargeEndDate() {
        return surrenderChargeEndDate;
    }

    /**
     * Sets the value of the surrenderChargeEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSurrenderChargeEndDate(XMLGregorianCalendar value) {
        this.surrenderChargeEndDate = value;
    }

    /**
     * Gets the value of the investmentProfileSelectionMethod property.
     * 
     * @return
     *     possible object is
     *     {@link InvestmentProfileSelectionMethod }
     *     
     */
    public InvestmentProfileSelectionMethod getInvestmentProfileSelectionMethod() {
        return investmentProfileSelectionMethod;
    }

    /**
     * Sets the value of the investmentProfileSelectionMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvestmentProfileSelectionMethod }
     *     
     */
    public void setInvestmentProfileSelectionMethod(InvestmentProfileSelectionMethod value) {
        this.investmentProfileSelectionMethod = value;
    }

    /**
     * Gets the value of the accumulationPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getAccumulationPeriod() {
        return accumulationPeriod;
    }

    /**
     * Sets the value of the accumulationPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setAccumulationPeriod(Duration value) {
        this.accumulationPeriod = value;
    }

    /**
     * Gets the value of the installedSavingsComponent property.
     * 
     * @return
     *     possible object is
     *     {@link SavingsComponentTO }
     *     
     */
    public SavingsComponentTO getInstalledSavingsComponent() {
        return installedSavingsComponent;
    }

    /**
     * Sets the value of the installedSavingsComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link SavingsComponentTO }
     *     
     */
    public void setInstalledSavingsComponent(SavingsComponentTO value) {
        this.installedSavingsComponent = value;
    }

    /**
     * Gets the value of the repaidSavingsComponent property.
     * 
     * @return
     *     possible object is
     *     {@link SavingsComponentTO }
     *     
     */
    public SavingsComponentTO getRepaidSavingsComponent() {
        return repaidSavingsComponent;
    }

    /**
     * Sets the value of the repaidSavingsComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link SavingsComponentTO }
     *     
     */
    public void setRepaidSavingsComponent(SavingsComponentTO value) {
        this.repaidSavingsComponent = value;
    }

    /**
     * Gets the value of the providedSavingsComponent property.
     * 
     * @return
     *     possible object is
     *     {@link SavingsComponentTO }
     *     
     */
    public SavingsComponentTO getProvidedSavingsComponent() {
        return providedSavingsComponent;
    }

    /**
     * Sets the value of the providedSavingsComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link SavingsComponentTO }
     *     
     */
    public void setProvidedSavingsComponent(SavingsComponentTO value) {
        this.providedSavingsComponent = value;
    }

    /**
     * Gets the value of the surrenderCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getSurrenderCharge() {
        return surrenderCharge;
    }

    /**
     * Sets the value of the surrenderCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setSurrenderCharge(BaseCurrencyAmount value) {
        this.surrenderCharge = value;
    }

    /**
     * Gets the value of the surrenderChargeTargetPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getSurrenderChargeTargetPremium() {
        return surrenderChargeTargetPremium;
    }

    /**
     * Sets the value of the surrenderChargeTargetPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setSurrenderChargeTargetPremium(BaseCurrencyAmount value) {
        this.surrenderChargeTargetPremium = value;
    }

    /**
     * Gets the value of the combinedAge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCombinedAge() {
        return combinedAge;
    }

    /**
     * Sets the value of the combinedAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCombinedAge(BigDecimal value) {
        this.combinedAge = value;
    }

    /**
     * Gets the value of the holdingFund property.
     * 
     * @return
     *     possible object is
     *     {@link FundTO }
     *     
     */
    public FundTO getHoldingFund() {
        return holdingFund;
    }

    /**
     * Sets the value of the holdingFund property.
     * 
     * @param value
     *     allowed object is
     *     {@link FundTO }
     *     
     */
    public void setHoldingFund(FundTO value) {
        this.holdingFund = value;
    }

}
