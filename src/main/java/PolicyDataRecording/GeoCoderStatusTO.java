
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeoCoderStatus_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeoCoderStatus_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}StatusWithCommonReason_TO">
 *       &lt;sequence>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}GeoCoderStatus"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeoCoderStatus_TO", propOrder = {
    "state"
})
public class GeoCoderStatusTO
    extends StatusWithCommonReasonTO
{

    @XmlElement(required = true)
    protected GeoCoderStatus state;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link GeoCoderStatus }
     *     
     */
    public GeoCoderStatus getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeoCoderStatus }
     *     
     */
    public void setState(GeoCoderStatus value) {
        this.state = value;
    }

}
