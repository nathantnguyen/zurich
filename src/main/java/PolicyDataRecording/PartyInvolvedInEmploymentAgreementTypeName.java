
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyInvolvedInEmploymentAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PartyInvolvedInEmploymentAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Employee Involved In Employment Agreement"/>
 *     &lt;enumeration value="Employer Involved In Employment Agreement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PartyInvolvedInEmploymentAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum PartyInvolvedInEmploymentAgreementTypeName {

    @XmlEnumValue("Employee Involved In Employment Agreement")
    EMPLOYEE_INVOLVED_IN_EMPLOYMENT_AGREEMENT("Employee Involved In Employment Agreement"),
    @XmlEnumValue("Employer Involved In Employment Agreement")
    EMPLOYER_INVOLVED_IN_EMPLOYMENT_AGREEMENT("Employer Involved In Employment Agreement");
    private final String value;

    PartyInvolvedInEmploymentAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartyInvolvedInEmploymentAgreementTypeName fromValue(String v) {
        for (PartyInvolvedInEmploymentAgreementTypeName c: PartyInvolvedInEmploymentAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
