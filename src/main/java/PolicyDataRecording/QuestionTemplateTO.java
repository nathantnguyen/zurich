
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuestionTemplate_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuestionTemplate_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}StaticTextBlock_TO">
 *       &lt;sequence>
 *         &lt;element name="possibleAnswers" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}PredefinedAnswer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="defaultAnswer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}PredefinedAnswer_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuestionTemplate_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "possibleAnswers",
    "defaultAnswer"
})
public class QuestionTemplateTO
    extends StaticTextBlockTO
{

    protected List<PredefinedAnswerTO> possibleAnswers;
    protected PredefinedAnswerTO defaultAnswer;

    /**
     * Gets the value of the possibleAnswers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the possibleAnswers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPossibleAnswers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PredefinedAnswerTO }
     * 
     * 
     */
    public List<PredefinedAnswerTO> getPossibleAnswers() {
        if (possibleAnswers == null) {
            possibleAnswers = new ArrayList<PredefinedAnswerTO>();
        }
        return this.possibleAnswers;
    }

    /**
     * Gets the value of the defaultAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link PredefinedAnswerTO }
     *     
     */
    public PredefinedAnswerTO getDefaultAnswer() {
        return defaultAnswer;
    }

    /**
     * Sets the value of the defaultAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link PredefinedAnswerTO }
     *     
     */
    public void setDefaultAnswer(PredefinedAnswerTO value) {
        this.defaultAnswer = value;
    }

    /**
     * Sets the value of the possibleAnswers property.
     * 
     * @param possibleAnswers
     *     allowed object is
     *     {@link PredefinedAnswerTO }
     *     
     */
    public void setPossibleAnswers(List<PredefinedAnswerTO> possibleAnswers) {
        this.possibleAnswers = possibleAnswers;
    }

}
