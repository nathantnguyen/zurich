
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractSpecificationRelationship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractSpecificationRelationship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Relationship_TO">
 *       &lt;sequence>
 *         &lt;element name="relatedToContractSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractSpecificationRelationship_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "relatedToContractSpecification"
})
public class ContractSpecificationRelationshipTO
    extends RelationshipTO
{

    protected List<ContractSpecificationTO> relatedToContractSpecification;

    /**
     * Gets the value of the relatedToContractSpecification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedToContractSpecification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedToContractSpecification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractSpecificationTO }
     * 
     * 
     */
    public List<ContractSpecificationTO> getRelatedToContractSpecification() {
        if (relatedToContractSpecification == null) {
            relatedToContractSpecification = new ArrayList<ContractSpecificationTO>();
        }
        return this.relatedToContractSpecification;
    }

    /**
     * Sets the value of the relatedToContractSpecification property.
     * 
     * @param relatedToContractSpecification
     *     allowed object is
     *     {@link ContractSpecificationTO }
     *     
     */
    public void setRelatedToContractSpecification(List<ContractSpecificationTO> relatedToContractSpecification) {
        this.relatedToContractSpecification = relatedToContractSpecification;
    }

}
