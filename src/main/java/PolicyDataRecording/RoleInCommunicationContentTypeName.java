
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInCommunicationContentTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInCommunicationContentTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Content Owner"/>
 *     &lt;enumeration value="Declaration"/>
 *     &lt;enumeration value="Communication Content Approval"/>
 *     &lt;enumeration value="Content Manager"/>
 *     &lt;enumeration value="Content Author"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInCommunicationContentTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum RoleInCommunicationContentTypeName {

    @XmlEnumValue("Content Owner")
    CONTENT_OWNER("Content Owner"),
    @XmlEnumValue("Declaration")
    DECLARATION("Declaration"),
    @XmlEnumValue("Communication Content Approval")
    COMMUNICATION_CONTENT_APPROVAL("Communication Content Approval"),
    @XmlEnumValue("Content Manager")
    CONTENT_MANAGER("Content Manager"),
    @XmlEnumValue("Content Author")
    CONTENT_AUTHOR("Content Author");
    private final String value;

    RoleInCommunicationContentTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInCommunicationContentTypeName fromValue(String v) {
        for (RoleInCommunicationContentTypeName c: RoleInCommunicationContentTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
