
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectTreatmentTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ObjectTreatmentTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Vehicle Repair"/>
 *     &lt;enumeration value="Vehicle Storage"/>
 *     &lt;enumeration value="Vehicle Towing"/>
 *     &lt;enumeration value="Material Handling"/>
 *     &lt;enumeration value="Modification Activity"/>
 *     &lt;enumeration value="Construction Activity"/>
 *     &lt;enumeration value="Household Content Storage"/>
 *     &lt;enumeration value="Disaster Assistance"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ObjectTreatmentTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum ObjectTreatmentTypeName {

    @XmlEnumValue("Vehicle Repair")
    VEHICLE_REPAIR("Vehicle Repair"),
    @XmlEnumValue("Vehicle Storage")
    VEHICLE_STORAGE("Vehicle Storage"),
    @XmlEnumValue("Vehicle Towing")
    VEHICLE_TOWING("Vehicle Towing"),
    @XmlEnumValue("Material Handling")
    MATERIAL_HANDLING("Material Handling"),
    @XmlEnumValue("Modification Activity")
    MODIFICATION_ACTIVITY("Modification Activity"),
    @XmlEnumValue("Construction Activity")
    CONSTRUCTION_ACTIVITY("Construction Activity"),
    @XmlEnumValue("Household Content Storage")
    HOUSEHOLD_CONTENT_STORAGE("Household Content Storage"),
    @XmlEnumValue("Disaster Assistance")
    DISASTER_ASSISTANCE("Disaster Assistance");
    private final String value;

    ObjectTreatmentTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ObjectTreatmentTypeName fromValue(String v) {
        for (ObjectTreatmentTypeName c: ObjectTreatmentTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
