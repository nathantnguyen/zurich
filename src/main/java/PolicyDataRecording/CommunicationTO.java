
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Communication_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Communication_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="appliedCommunicationProfiles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CommunicationProfile_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="plannedStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="plannedEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="actualStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="actualEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="direction" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/}CommunicationDirection" minOccurs="0"/>
 *         &lt;element name="purpose" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/}CommunicationPurpose" minOccurs="0"/>
 *         &lt;element name="priority" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/}CommunicationPriority" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="language" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}Language" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="original" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}Communication_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="replies" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}Communication_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="communicationContents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="precedingCommunications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}Communication_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="followUpCommunications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}Communication_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="sender" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="receiver" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="origin" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPoint_TO" minOccurs="0"/>
 *         &lt;element name="mediator" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPoint_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="destination" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPoint_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="returnDestinations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPoint_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="assessingScore" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationEffectivenessScore_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Communication_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "appliedCommunicationProfiles",
    "plannedStartDate",
    "plannedEndDate",
    "actualStartDate",
    "actualEndDate",
    "direction",
    "purpose",
    "priority",
    "description",
    "language",
    "externalReference",
    "original",
    "replies",
    "communicationContents",
    "precedingCommunications",
    "followUpCommunications",
    "sender",
    "receiver",
    "origin",
    "mediator",
    "destination",
    "status",
    "returnDestinations",
    "assessingScore",
    "alternateReference"
})
public class CommunicationTO
    extends BusinessModelObjectTO
{

    protected List<CommunicationProfileTO> appliedCommunicationProfiles;
    protected XMLGregorianCalendar plannedStartDate;
    protected XMLGregorianCalendar plannedEndDate;
    protected XMLGregorianCalendar actualStartDate;
    protected XMLGregorianCalendar actualEndDate;
    protected CommunicationDirection direction;
    protected CommunicationPurpose purpose;
    protected CommunicationPriority priority;
    protected String description;
    protected Language language;
    protected String externalReference;
    protected List<CommunicationTO> original;
    protected List<CommunicationTO> replies;
    protected List<CommunicationContentTO> communicationContents;
    protected List<CommunicationTO> precedingCommunications;
    protected List<CommunicationTO> followUpCommunications;
    protected List<RolePlayerTO> sender;
    protected List<RolePlayerTO> receiver;
    protected ContactPointTO origin;
    protected List<ContactPointTO> mediator;
    protected List<ContactPointTO> destination;
    protected List<CommunicationStatusTO> status;
    protected List<ContactPointTO> returnDestinations;
    protected List<CommunicationEffectivenessScoreTO> assessingScore;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the appliedCommunicationProfiles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appliedCommunicationProfiles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppliedCommunicationProfiles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationProfileTO }
     * 
     * 
     */
    public List<CommunicationProfileTO> getAppliedCommunicationProfiles() {
        if (appliedCommunicationProfiles == null) {
            appliedCommunicationProfiles = new ArrayList<CommunicationProfileTO>();
        }
        return this.appliedCommunicationProfiles;
    }

    /**
     * Gets the value of the plannedStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlannedStartDate() {
        return plannedStartDate;
    }

    /**
     * Sets the value of the plannedStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlannedStartDate(XMLGregorianCalendar value) {
        this.plannedStartDate = value;
    }

    /**
     * Gets the value of the plannedEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlannedEndDate() {
        return plannedEndDate;
    }

    /**
     * Sets the value of the plannedEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlannedEndDate(XMLGregorianCalendar value) {
        this.plannedEndDate = value;
    }

    /**
     * Gets the value of the actualStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualStartDate() {
        return actualStartDate;
    }

    /**
     * Sets the value of the actualStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualStartDate(XMLGregorianCalendar value) {
        this.actualStartDate = value;
    }

    /**
     * Gets the value of the actualEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualEndDate() {
        return actualEndDate;
    }

    /**
     * Sets the value of the actualEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualEndDate(XMLGregorianCalendar value) {
        this.actualEndDate = value;
    }

    /**
     * Gets the value of the direction property.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationDirection }
     *     
     */
    public CommunicationDirection getDirection() {
        return direction;
    }

    /**
     * Sets the value of the direction property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationDirection }
     *     
     */
    public void setDirection(CommunicationDirection value) {
        this.direction = value;
    }

    /**
     * Gets the value of the purpose property.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationPurpose }
     *     
     */
    public CommunicationPurpose getPurpose() {
        return purpose;
    }

    /**
     * Sets the value of the purpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationPurpose }
     *     
     */
    public void setPurpose(CommunicationPurpose value) {
        this.purpose = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationPriority }
     *     
     */
    public CommunicationPriority getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationPriority }
     *     
     */
    public void setPriority(CommunicationPriority value) {
        this.priority = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link Language }
     *     
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link Language }
     *     
     */
    public void setLanguage(Language value) {
        this.language = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the original property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the original property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationTO }
     * 
     * 
     */
    public List<CommunicationTO> getOriginal() {
        if (original == null) {
            original = new ArrayList<CommunicationTO>();
        }
        return this.original;
    }

    /**
     * Gets the value of the replies property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the replies property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReplies().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationTO }
     * 
     * 
     */
    public List<CommunicationTO> getReplies() {
        if (replies == null) {
            replies = new ArrayList<CommunicationTO>();
        }
        return this.replies;
    }

    /**
     * Gets the value of the communicationContents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the communicationContents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommunicationContents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationContentTO }
     * 
     * 
     */
    public List<CommunicationContentTO> getCommunicationContents() {
        if (communicationContents == null) {
            communicationContents = new ArrayList<CommunicationContentTO>();
        }
        return this.communicationContents;
    }

    /**
     * Gets the value of the precedingCommunications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the precedingCommunications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrecedingCommunications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationTO }
     * 
     * 
     */
    public List<CommunicationTO> getPrecedingCommunications() {
        if (precedingCommunications == null) {
            precedingCommunications = new ArrayList<CommunicationTO>();
        }
        return this.precedingCommunications;
    }

    /**
     * Gets the value of the followUpCommunications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the followUpCommunications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFollowUpCommunications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationTO }
     * 
     * 
     */
    public List<CommunicationTO> getFollowUpCommunications() {
        if (followUpCommunications == null) {
            followUpCommunications = new ArrayList<CommunicationTO>();
        }
        return this.followUpCommunications;
    }

    /**
     * Gets the value of the sender property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sender property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSender().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerTO }
     * 
     * 
     */
    public List<RolePlayerTO> getSender() {
        if (sender == null) {
            sender = new ArrayList<RolePlayerTO>();
        }
        return this.sender;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receiver property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceiver().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerTO }
     * 
     * 
     */
    public List<RolePlayerTO> getReceiver() {
        if (receiver == null) {
            receiver = new ArrayList<RolePlayerTO>();
        }
        return this.receiver;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link ContactPointTO }
     *     
     */
    public ContactPointTO getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPointTO }
     *     
     */
    public void setOrigin(ContactPointTO value) {
        this.origin = value;
    }

    /**
     * Gets the value of the mediator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mediator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMediator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactPointTO }
     * 
     * 
     */
    public List<ContactPointTO> getMediator() {
        if (mediator == null) {
            mediator = new ArrayList<ContactPointTO>();
        }
        return this.mediator;
    }

    /**
     * Gets the value of the destination property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destination property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestination().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactPointTO }
     * 
     * 
     */
    public List<ContactPointTO> getDestination() {
        if (destination == null) {
            destination = new ArrayList<ContactPointTO>();
        }
        return this.destination;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationStatusTO }
     * 
     * 
     */
    public List<CommunicationStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<CommunicationStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the returnDestinations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the returnDestinations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReturnDestinations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactPointTO }
     * 
     * 
     */
    public List<ContactPointTO> getReturnDestinations() {
        if (returnDestinations == null) {
            returnDestinations = new ArrayList<ContactPointTO>();
        }
        return this.returnDestinations;
    }

    /**
     * Gets the value of the assessingScore property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assessingScore property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssessingScore().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationEffectivenessScoreTO }
     * 
     * 
     */
    public List<CommunicationEffectivenessScoreTO> getAssessingScore() {
        if (assessingScore == null) {
            assessingScore = new ArrayList<CommunicationEffectivenessScoreTO>();
        }
        return this.assessingScore;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the appliedCommunicationProfiles property.
     * 
     * @param appliedCommunicationProfiles
     *     allowed object is
     *     {@link CommunicationProfileTO }
     *     
     */
    public void setAppliedCommunicationProfiles(List<CommunicationProfileTO> appliedCommunicationProfiles) {
        this.appliedCommunicationProfiles = appliedCommunicationProfiles;
    }

    /**
     * Sets the value of the original property.
     * 
     * @param original
     *     allowed object is
     *     {@link CommunicationTO }
     *     
     */
    public void setOriginal(List<CommunicationTO> original) {
        this.original = original;
    }

    /**
     * Sets the value of the replies property.
     * 
     * @param replies
     *     allowed object is
     *     {@link CommunicationTO }
     *     
     */
    public void setReplies(List<CommunicationTO> replies) {
        this.replies = replies;
    }

    /**
     * Sets the value of the communicationContents property.
     * 
     * @param communicationContents
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setCommunicationContents(List<CommunicationContentTO> communicationContents) {
        this.communicationContents = communicationContents;
    }

    /**
     * Sets the value of the precedingCommunications property.
     * 
     * @param precedingCommunications
     *     allowed object is
     *     {@link CommunicationTO }
     *     
     */
    public void setPrecedingCommunications(List<CommunicationTO> precedingCommunications) {
        this.precedingCommunications = precedingCommunications;
    }

    /**
     * Sets the value of the followUpCommunications property.
     * 
     * @param followUpCommunications
     *     allowed object is
     *     {@link CommunicationTO }
     *     
     */
    public void setFollowUpCommunications(List<CommunicationTO> followUpCommunications) {
        this.followUpCommunications = followUpCommunications;
    }

    /**
     * Sets the value of the sender property.
     * 
     * @param sender
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setSender(List<RolePlayerTO> sender) {
        this.sender = sender;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param receiver
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setReceiver(List<RolePlayerTO> receiver) {
        this.receiver = receiver;
    }

    /**
     * Sets the value of the mediator property.
     * 
     * @param mediator
     *     allowed object is
     *     {@link ContactPointTO }
     *     
     */
    public void setMediator(List<ContactPointTO> mediator) {
        this.mediator = mediator;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param destination
     *     allowed object is
     *     {@link ContactPointTO }
     *     
     */
    public void setDestination(List<ContactPointTO> destination) {
        this.destination = destination;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link CommunicationStatusTO }
     *     
     */
    public void setStatus(List<CommunicationStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the returnDestinations property.
     * 
     * @param returnDestinations
     *     allowed object is
     *     {@link ContactPointTO }
     *     
     */
    public void setReturnDestinations(List<ContactPointTO> returnDestinations) {
        this.returnDestinations = returnDestinations;
    }

    /**
     * Sets the value of the assessingScore property.
     * 
     * @param assessingScore
     *     allowed object is
     *     {@link CommunicationEffectivenessScoreTO }
     *     
     */
    public void setAssessingScore(List<CommunicationEffectivenessScoreTO> assessingScore) {
        this.assessingScore = assessingScore;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
