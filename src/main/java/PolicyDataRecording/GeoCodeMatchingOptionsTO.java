
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeoCodeMatchingOptions_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeoCodeMatchingOptions_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="closeMatchesOnly" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="fullInputMatchRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="houseNumberMatchRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="streetMatchRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="postalCodeMatchRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="cityMatchRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="regionMatchRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeoCodeMatchingOptions_TO", propOrder = {
    "closeMatchesOnly",
    "fullInputMatchRequired",
    "houseNumberMatchRequired",
    "streetMatchRequired",
    "postalCodeMatchRequired",
    "cityMatchRequired",
    "regionMatchRequired"
})
@XmlSeeAlso({
    CanadaGeoCodeMatchingOptionsTO.class
})
public class GeoCodeMatchingOptionsTO
    extends DependentObjectTO
{

    protected Boolean closeMatchesOnly;
    protected Boolean fullInputMatchRequired;
    protected Boolean houseNumberMatchRequired;
    protected Boolean streetMatchRequired;
    protected Boolean postalCodeMatchRequired;
    protected Boolean cityMatchRequired;
    protected Boolean regionMatchRequired;

    /**
     * Gets the value of the closeMatchesOnly property.
     * This getter has been renamed from isCloseMatchesOnly() to getCloseMatchesOnly() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCloseMatchesOnly() {
        return closeMatchesOnly;
    }

    /**
     * Sets the value of the closeMatchesOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCloseMatchesOnly(Boolean value) {
        this.closeMatchesOnly = value;
    }

    /**
     * Gets the value of the fullInputMatchRequired property.
     * This getter has been renamed from isFullInputMatchRequired() to getFullInputMatchRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFullInputMatchRequired() {
        return fullInputMatchRequired;
    }

    /**
     * Sets the value of the fullInputMatchRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFullInputMatchRequired(Boolean value) {
        this.fullInputMatchRequired = value;
    }

    /**
     * Gets the value of the houseNumberMatchRequired property.
     * This getter has been renamed from isHouseNumberMatchRequired() to getHouseNumberMatchRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHouseNumberMatchRequired() {
        return houseNumberMatchRequired;
    }

    /**
     * Sets the value of the houseNumberMatchRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHouseNumberMatchRequired(Boolean value) {
        this.houseNumberMatchRequired = value;
    }

    /**
     * Gets the value of the streetMatchRequired property.
     * This getter has been renamed from isStreetMatchRequired() to getStreetMatchRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getStreetMatchRequired() {
        return streetMatchRequired;
    }

    /**
     * Sets the value of the streetMatchRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStreetMatchRequired(Boolean value) {
        this.streetMatchRequired = value;
    }

    /**
     * Gets the value of the postalCodeMatchRequired property.
     * This getter has been renamed from isPostalCodeMatchRequired() to getPostalCodeMatchRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPostalCodeMatchRequired() {
        return postalCodeMatchRequired;
    }

    /**
     * Sets the value of the postalCodeMatchRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPostalCodeMatchRequired(Boolean value) {
        this.postalCodeMatchRequired = value;
    }

    /**
     * Gets the value of the cityMatchRequired property.
     * This getter has been renamed from isCityMatchRequired() to getCityMatchRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCityMatchRequired() {
        return cityMatchRequired;
    }

    /**
     * Sets the value of the cityMatchRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCityMatchRequired(Boolean value) {
        this.cityMatchRequired = value;
    }

    /**
     * Gets the value of the regionMatchRequired property.
     * This getter has been renamed from isRegionMatchRequired() to getRegionMatchRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRegionMatchRequired() {
        return regionMatchRequired;
    }

    /**
     * Sets the value of the regionMatchRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRegionMatchRequired(Boolean value) {
        this.regionMatchRequired = value;
    }

}
