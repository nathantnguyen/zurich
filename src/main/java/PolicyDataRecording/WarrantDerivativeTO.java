
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WarrantDerivative_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WarrantDerivative_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Derivative_TO">
 *       &lt;sequence>
 *         &lt;element name="conversionRatio" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="separationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="warrantCallPutIndictor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="warrantIssueIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WarrantDerivative_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "conversionRatio",
    "separationDate",
    "warrantCallPutIndictor",
    "warrantIssueIndicator"
})
public class WarrantDerivativeTO
    extends DerivativeTO
{

    protected BigInteger conversionRatio;
    protected XMLGregorianCalendar separationDate;
    protected Boolean warrantCallPutIndictor;
    protected Boolean warrantIssueIndicator;

    /**
     * Gets the value of the conversionRatio property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getConversionRatio() {
        return conversionRatio;
    }

    /**
     * Sets the value of the conversionRatio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setConversionRatio(BigInteger value) {
        this.conversionRatio = value;
    }

    /**
     * Gets the value of the separationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSeparationDate() {
        return separationDate;
    }

    /**
     * Sets the value of the separationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSeparationDate(XMLGregorianCalendar value) {
        this.separationDate = value;
    }

    /**
     * Gets the value of the warrantCallPutIndictor property.
     * This getter has been renamed from isWarrantCallPutIndictor() to getWarrantCallPutIndictor() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getWarrantCallPutIndictor() {
        return warrantCallPutIndictor;
    }

    /**
     * Sets the value of the warrantCallPutIndictor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWarrantCallPutIndictor(Boolean value) {
        this.warrantCallPutIndictor = value;
    }

    /**
     * Gets the value of the warrantIssueIndicator property.
     * This getter has been renamed from isWarrantIssueIndicator() to getWarrantIssueIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getWarrantIssueIndicator() {
        return warrantIssueIndicator;
    }

    /**
     * Sets the value of the warrantIssueIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWarrantIssueIndicator(Boolean value) {
        this.warrantIssueIndicator = value;
    }

}
