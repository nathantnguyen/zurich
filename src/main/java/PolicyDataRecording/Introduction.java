
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Introduction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Introduction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Advertisement"/>
 *     &lt;enumeration value="Business Partner"/>
 *     &lt;enumeration value="Mailshot Campaign"/>
 *     &lt;enumeration value="Newspaper"/>
 *     &lt;enumeration value="Personal Recommendation"/>
 *     &lt;enumeration value="Television Advertisement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Introduction", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum Introduction {

    @XmlEnumValue("Advertisement")
    ADVERTISEMENT("Advertisement"),
    @XmlEnumValue("Business Partner")
    BUSINESS_PARTNER("Business Partner"),
    @XmlEnumValue("Mailshot Campaign")
    MAILSHOT_CAMPAIGN("Mailshot Campaign"),
    @XmlEnumValue("Newspaper")
    NEWSPAPER("Newspaper"),
    @XmlEnumValue("Personal Recommendation")
    PERSONAL_RECOMMENDATION("Personal Recommendation"),
    @XmlEnumValue("Television Advertisement")
    TELEVISION_ADVERTISEMENT("Television Advertisement");
    private final String value;

    Introduction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Introduction fromValue(String v) {
        for (Introduction c: Introduction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
