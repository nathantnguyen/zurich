
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractGroup_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractGroup_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO">
 *       &lt;sequence>
 *         &lt;element name="includedContracts" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractGroup_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "includedContracts"
})
public class ContractGroupTO
    extends ContractTO
{

    protected List<ContractTO> includedContracts;

    /**
     * Gets the value of the includedContracts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedContracts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludedContracts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractTO }
     * 
     * 
     */
    public List<ContractTO> getIncludedContracts() {
        if (includedContracts == null) {
            includedContracts = new ArrayList<ContractTO>();
        }
        return this.includedContracts;
    }

    /**
     * Sets the value of the includedContracts property.
     * 
     * @param includedContracts
     *     allowed object is
     *     {@link ContractTO }
     *     
     */
    public void setIncludedContracts(List<ContractTO> includedContracts) {
        this.includedContracts = includedContracts;
    }

}
