
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommercialAgreementDetail_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialAgreementDetail_TO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="directAssumedCededCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="billTypeCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="reinsuranceProgramTypeName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialAgreementDetail_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "directAssumedCededCode",
    "billTypeCode",
    "reinsuranceProgramTypeName"
})
public class CommercialAgreementDetailTO {

    protected String directAssumedCededCode;
    protected String billTypeCode;
    protected String reinsuranceProgramTypeName;

    /**
     * Gets the value of the directAssumedCededCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectAssumedCededCode() {
        return directAssumedCededCode;
    }

    /**
     * Sets the value of the directAssumedCededCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectAssumedCededCode(String value) {
        this.directAssumedCededCode = value;
    }

    /**
     * Gets the value of the billTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillTypeCode() {
        return billTypeCode;
    }

    /**
     * Sets the value of the billTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillTypeCode(String value) {
        this.billTypeCode = value;
    }

    /**
     * Gets the value of the reinsuranceProgramTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReinsuranceProgramTypeName() {
        return reinsuranceProgramTypeName;
    }

    /**
     * Sets the value of the reinsuranceProgramTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReinsuranceProgramTypeName(String value) {
        this.reinsuranceProgramTypeName = value;
    }

}
