
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmploymentAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmploymentAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO">
 *       &lt;sequence>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}EmploymentAgreementStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="employmentAgreementComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}EmploymentAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmploymentAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "status",
    "employmentAgreementComponents"
})
public class EmploymentAgreementTO
    extends ContractTO
{

    protected List<EmploymentAgreementStatusTO> status;
    protected List<EmploymentAgreementTO> employmentAgreementComponents;

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmploymentAgreementStatusTO }
     * 
     * 
     */
    public List<EmploymentAgreementStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<EmploymentAgreementStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the employmentAgreementComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the employmentAgreementComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmploymentAgreementComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmploymentAgreementTO }
     * 
     * 
     */
    public List<EmploymentAgreementTO> getEmploymentAgreementComponents() {
        if (employmentAgreementComponents == null) {
            employmentAgreementComponents = new ArrayList<EmploymentAgreementTO>();
        }
        return this.employmentAgreementComponents;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link EmploymentAgreementStatusTO }
     *     
     */
    public void setStatus(List<EmploymentAgreementStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the employmentAgreementComponents property.
     * 
     * @param employmentAgreementComponents
     *     allowed object is
     *     {@link EmploymentAgreementTO }
     *     
     */
    public void setEmploymentAgreementComponents(List<EmploymentAgreementTO> employmentAgreementComponents) {
        this.employmentAgreementComponents = employmentAgreementComponents;
    }

}
