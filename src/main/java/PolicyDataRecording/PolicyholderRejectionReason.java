
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyholderRejectionReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PolicyholderRejectionReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Different Agreement"/>
 *     &lt;enumeration value="Different Insurer"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PolicyholderRejectionReason", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum PolicyholderRejectionReason {

    @XmlEnumValue("Different Agreement")
    DIFFERENT_AGREEMENT("Different Agreement"),
    @XmlEnumValue("Different Insurer")
    DIFFERENT_INSURER("Different Insurer");
    private final String value;

    PolicyholderRejectionReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PolicyholderRejectionReason fromValue(String v) {
        for (PolicyholderRejectionReason c: PolicyholderRejectionReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
