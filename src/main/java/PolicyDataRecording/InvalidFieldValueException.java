
package PolicyDataRecording;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * <p>Java class for InvalidFieldValueException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvalidFieldValueException">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}ServiceException">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="fieldName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}QName" form="qualified"/>
 *         &lt;element name="fieldValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvalidFieldValueException", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/", propOrder = {
    "fieldNameAndFieldValue"
})
public class InvalidFieldValueException
    extends ServiceException
{

    @XmlElements({
        @XmlElement(name = "fieldName", required = true, type = QName.class),
        @XmlElement(name = "fieldValue", required = true, type = String.class)
    })
    protected List<Serializable> fieldNameAndFieldValue;

    /**
     * Gets the value of the fieldNameAndFieldValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldNameAndFieldValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldNameAndFieldValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QName }
     * {@link String }
     * 
     * 
     */
    public List<Serializable> getFieldNameAndFieldValue() {
        if (fieldNameAndFieldValue == null) {
            fieldNameAndFieldValue = new ArrayList<Serializable>();
        }
        return this.fieldNameAndFieldValue;
    }

    /**
     * Sets the value of the fieldNameAndFieldValue property.
     * 
     * @param fieldNameAndFieldValue
     *     allowed object is
     *     {@link QName }
     *     {@link String }
     *     
     */
    public void setFieldNameAndFieldValue(List<Serializable> fieldNameAndFieldValue) {
        this.fieldNameAndFieldValue = fieldNameAndFieldValue;
    }

}
