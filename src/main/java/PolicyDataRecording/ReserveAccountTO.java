
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReserveAccount_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReserveAccount_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}MonetaryAccount_TO">
 *       &lt;sequence>
 *         &lt;element name="natureOfProvision" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/}NatureOfProvision" minOccurs="0"/>
 *         &lt;element name="calculationMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/}ReserveCalculationMethod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReserveAccount_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "natureOfProvision",
    "calculationMethod"
})
public class ReserveAccountTO
    extends MonetaryAccountTO
{

    protected NatureOfProvision natureOfProvision;
    protected ReserveCalculationMethod calculationMethod;

    /**
     * Gets the value of the natureOfProvision property.
     * 
     * @return
     *     possible object is
     *     {@link NatureOfProvision }
     *     
     */
    public NatureOfProvision getNatureOfProvision() {
        return natureOfProvision;
    }

    /**
     * Sets the value of the natureOfProvision property.
     * 
     * @param value
     *     allowed object is
     *     {@link NatureOfProvision }
     *     
     */
    public void setNatureOfProvision(NatureOfProvision value) {
        this.natureOfProvision = value;
    }

    /**
     * Gets the value of the calculationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link ReserveCalculationMethod }
     *     
     */
    public ReserveCalculationMethod getCalculationMethod() {
        return calculationMethod;
    }

    /**
     * Sets the value of the calculationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReserveCalculationMethod }
     *     
     */
    public void setCalculationMethod(ReserveCalculationMethod value) {
        this.calculationMethod = value;
    }

}
