
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DerivativeTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DerivativeTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Asset Backed Derivative"/>
 *     &lt;enumeration value="Credit Derivative"/>
 *     &lt;enumeration value="Forward Rate Agreement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DerivativeTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum DerivativeTypeName {

    @XmlEnumValue("Asset Backed Derivative")
    ASSET_BACKED_DERIVATIVE("Asset Backed Derivative"),
    @XmlEnumValue("Credit Derivative")
    CREDIT_DERIVATIVE("Credit Derivative"),
    @XmlEnumValue("Forward Rate Agreement")
    FORWARD_RATE_AGREEMENT("Forward Rate Agreement");
    private final String value;

    DerivativeTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DerivativeTypeName fromValue(String v) {
        for (DerivativeTypeName c: DerivativeTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
