
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BuildingOccupancy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BuildingOccupancy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Owner Occupied"/>
 *     &lt;enumeration value="Squatter Occupied"/>
 *     &lt;enumeration value="Tenant Occupied"/>
 *     &lt;enumeration value="Unoccupied"/>
 *     &lt;enumeration value="Vacant Less 60 Days"/>
 *     &lt;enumeration value="Vacant Longer 60 Days"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BuildingOccupancy", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum BuildingOccupancy {

    @XmlEnumValue("Owner Occupied")
    OWNER_OCCUPIED("Owner Occupied"),
    @XmlEnumValue("Squatter Occupied")
    SQUATTER_OCCUPIED("Squatter Occupied"),
    @XmlEnumValue("Tenant Occupied")
    TENANT_OCCUPIED("Tenant Occupied"),
    @XmlEnumValue("Unoccupied")
    UNOCCUPIED("Unoccupied"),
    @XmlEnumValue("Vacant Less 60 Days")
    VACANT_LESS_60_DAYS("Vacant Less 60 Days"),
    @XmlEnumValue("Vacant Longer 60 Days")
    VACANT_LONGER_60_DAYS("Vacant Longer 60 Days");
    private final String value;

    BuildingOccupancy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BuildingOccupancy fromValue(String v) {
        for (BuildingOccupancy c: BuildingOccupancy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
