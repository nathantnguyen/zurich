
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoofType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoofType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Corrugated Iron"/>
 *     &lt;enumeration value="Slate"/>
 *     &lt;enumeration value="Standard Shingles"/>
 *     &lt;enumeration value="Straw Thatch"/>
 *     &lt;enumeration value="Tile"/>
 *     &lt;enumeration value="Asbestos Cement Slabs"/>
 *     &lt;enumeration value="Concrete Slabs"/>
 *     &lt;enumeration value="Metal"/>
 *     &lt;enumeration value="Reed Thatch"/>
 *     &lt;enumeration value="Tar Paper"/>
 *     &lt;enumeration value="Wood"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoofType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum RoofType {

    @XmlEnumValue("Corrugated Iron")
    CORRUGATED_IRON("Corrugated Iron"),
    @XmlEnumValue("Slate")
    SLATE("Slate"),
    @XmlEnumValue("Standard Shingles")
    STANDARD_SHINGLES("Standard Shingles"),
    @XmlEnumValue("Straw Thatch")
    STRAW_THATCH("Straw Thatch"),
    @XmlEnumValue("Tile")
    TILE("Tile"),
    @XmlEnumValue("Asbestos Cement Slabs")
    ASBESTOS_CEMENT_SLABS("Asbestos Cement Slabs"),
    @XmlEnumValue("Concrete Slabs")
    CONCRETE_SLABS("Concrete Slabs"),
    @XmlEnumValue("Metal")
    METAL("Metal"),
    @XmlEnumValue("Reed Thatch")
    REED_THATCH("Reed Thatch"),
    @XmlEnumValue("Tar Paper")
    TAR_PAPER("Tar Paper"),
    @XmlEnumValue("Wood")
    WOOD("Wood");
    private final String value;

    RoofType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoofType fromValue(String v) {
        for (RoofType c: RoofType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
