
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VehicleScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VehicleScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="recordedMileage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="annualMileage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="dailyMileage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="journeyMileage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "recordedMileage",
    "annualMileage",
    "dailyMileage",
    "journeyMileage"
})
public class VehicleScoreTO
    extends ScoreTO
{

    protected BigDecimal recordedMileage;
    protected Amount annualMileage;
    protected Amount dailyMileage;
    protected Amount journeyMileage;

    /**
     * Gets the value of the recordedMileage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRecordedMileage() {
        return recordedMileage;
    }

    /**
     * Sets the value of the recordedMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRecordedMileage(BigDecimal value) {
        this.recordedMileage = value;
    }

    /**
     * Gets the value of the annualMileage property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getAnnualMileage() {
        return annualMileage;
    }

    /**
     * Sets the value of the annualMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setAnnualMileage(Amount value) {
        this.annualMileage = value;
    }

    /**
     * Gets the value of the dailyMileage property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getDailyMileage() {
        return dailyMileage;
    }

    /**
     * Sets the value of the dailyMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setDailyMileage(Amount value) {
        this.dailyMileage = value;
    }

    /**
     * Gets the value of the journeyMileage property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getJourneyMileage() {
        return journeyMileage;
    }

    /**
     * Sets the value of the journeyMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setJourneyMileage(Amount value) {
        this.journeyMileage = value;
    }

}
