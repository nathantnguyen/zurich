
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInContract_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInContract_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="assessmentResults" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contextContract" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO" minOccurs="0"/>
 *         &lt;element name="rootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContractType_TO" minOccurs="0"/>
 *         &lt;element name="place" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInContract_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "assessmentResults",
    "contextContract",
    "rootType",
    "place"
})
@XmlSeeAlso({
    MoneyProvisionInvolvedInContractTO.class,
    ActivityProvidedByContractTO.class,
    RoleInCorporateAgreementTO.class,
    RoleInIntermediaryAgreementTO.class,
    RoleInEmploymentAgreementTO.class,
    RoleInFinancialServicesAgreementTO.class
})
public class RoleInContractTO
    extends RoleInContextClassTO
{

    protected List<AssessmentResultTO> assessmentResults;
    protected ContractTO contextContract;
    protected RoleInContractTypeTO rootType;
    protected PlaceTO place;

    /**
     * Gets the value of the assessmentResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assessmentResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssessmentResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssessmentResultTO }
     * 
     * 
     */
    public List<AssessmentResultTO> getAssessmentResults() {
        if (assessmentResults == null) {
            assessmentResults = new ArrayList<AssessmentResultTO>();
        }
        return this.assessmentResults;
    }

    /**
     * Gets the value of the contextContract property.
     * 
     * @return
     *     possible object is
     *     {@link ContractTO }
     *     
     */
    public ContractTO getContextContract() {
        return contextContract;
    }

    /**
     * Sets the value of the contextContract property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractTO }
     *     
     */
    public void setContextContract(ContractTO value) {
        this.contextContract = value;
    }

    /**
     * Gets the value of the rootType property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInContractTypeTO }
     *     
     */
    public RoleInContractTypeTO getRootType() {
        return rootType;
    }

    /**
     * Sets the value of the rootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInContractTypeTO }
     *     
     */
    public void setRootType(RoleInContractTypeTO value) {
        this.rootType = value;
    }

    /**
     * Gets the value of the place property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getPlace() {
        return place;
    }

    /**
     * Sets the value of the place property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setPlace(PlaceTO value) {
        this.place = value;
    }

    /**
     * Sets the value of the assessmentResults property.
     * 
     * @param assessmentResults
     *     allowed object is
     *     {@link AssessmentResultTO }
     *     
     */
    public void setAssessmentResults(List<AssessmentResultTO> assessmentResults) {
        this.assessmentResults = assessmentResults;
    }

}
