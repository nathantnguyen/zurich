
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Vehicle_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Vehicle_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}ManufacturedItem_TO">
 *       &lt;sequence>
 *         &lt;element name="statedValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="serialNumberEtching" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="cylinders" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="engineCapacity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="antiqueVehicle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="classicVehicle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="highPerformance" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="highTheft" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="airbagsLocation" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}AirbagsLocation" minOccurs="0"/>
 *         &lt;element name="antiLockBrakes" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="antiTheftDevice" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="parkingPlace" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}ParkingPlace" minOccurs="0"/>
 *         &lt;element name="seatbeltsLocation" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}SeatbeltsLocation" minOccurs="0"/>
 *         &lt;element name="daytimeRunningLights" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="insuranceRatingGroup" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}InsuranceRatingGroup" minOccurs="0"/>
 *         &lt;element name="horsepower" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="displacementSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="vehicleModelSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}VehicleModel_TO" minOccurs="0"/>
 *         &lt;element name="vehicleRegistration" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}VehicleRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="isClassicVehicle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isHighPerformance" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isTotalLoss" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="numberOfAirbags" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="hasAlarm" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="vinPosition" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="vinPattern" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="derivedVehicleIndentificationNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="originDescription" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="vehicleAgeCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="originCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="vinWorldManufacturerIdentifier" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="vinVehicleDescriptorSection" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="vinNorthAmericanCheckDigit" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="vinModelYearEncoding" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="vinPlantCodeIdentifier" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="vinProductionNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vehicle_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "statedValue",
    "serialNumberEtching",
    "cylinders",
    "engineCapacity",
    "antiqueVehicle",
    "classicVehicle",
    "highPerformance",
    "highTheft",
    "airbagsLocation",
    "antiLockBrakes",
    "antiTheftDevice",
    "parkingPlace",
    "seatbeltsLocation",
    "daytimeRunningLights",
    "insuranceRatingGroup",
    "horsepower",
    "displacementSize",
    "vehicleModelSpecification",
    "vehicleRegistration",
    "isClassicVehicle",
    "isHighPerformance",
    "isTotalLoss",
    "numberOfAirbags",
    "hasAlarm",
    "vinPosition",
    "vinPattern",
    "derivedVehicleIndentificationNumber",
    "originDescription",
    "vehicleAgeCode",
    "originCode",
    "vinWorldManufacturerIdentifier",
    "vinVehicleDescriptorSection",
    "vinNorthAmericanCheckDigit",
    "vinModelYearEncoding",
    "vinPlantCodeIdentifier",
    "vinProductionNumber"
})
@XmlSeeAlso({
    ShipTO.class
})
public class VehicleTO
    extends ManufacturedItemTO
{

    protected BaseCurrencyAmount statedValue;
    protected Boolean serialNumberEtching;
    protected BigInteger cylinders;
    protected Amount engineCapacity;
    protected Boolean antiqueVehicle;
    protected Boolean classicVehicle;
    protected Boolean highPerformance;
    protected Boolean highTheft;
    protected AirbagsLocation airbagsLocation;
    protected Boolean antiLockBrakes;
    protected Boolean antiTheftDevice;
    protected ParkingPlace parkingPlace;
    protected SeatbeltsLocation seatbeltsLocation;
    protected Boolean daytimeRunningLights;
    protected InsuranceRatingGroup insuranceRatingGroup;
    protected Amount horsepower;
    protected Amount displacementSize;
    protected VehicleModelTO vehicleModelSpecification;
    protected List<VehicleRegistrationTO> vehicleRegistration;
    protected Boolean isClassicVehicle;
    protected Boolean isHighPerformance;
    protected Boolean isTotalLoss;
    protected BigInteger numberOfAirbags;
    protected Boolean hasAlarm;
    protected String vinPosition;
    protected String vinPattern;
    protected String derivedVehicleIndentificationNumber;
    protected String originDescription;
    protected String vehicleAgeCode;
    protected String originCode;
    protected String vinWorldManufacturerIdentifier;
    protected String vinVehicleDescriptorSection;
    protected String vinNorthAmericanCheckDigit;
    protected String vinModelYearEncoding;
    protected String vinPlantCodeIdentifier;
    protected String vinProductionNumber;

    /**
     * Gets the value of the statedValue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getStatedValue() {
        return statedValue;
    }

    /**
     * Sets the value of the statedValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setStatedValue(BaseCurrencyAmount value) {
        this.statedValue = value;
    }

    /**
     * Gets the value of the serialNumberEtching property.
     * This getter has been renamed from isSerialNumberEtching() to getSerialNumberEtching() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSerialNumberEtching() {
        return serialNumberEtching;
    }

    /**
     * Sets the value of the serialNumberEtching property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSerialNumberEtching(Boolean value) {
        this.serialNumberEtching = value;
    }

    /**
     * Gets the value of the cylinders property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCylinders() {
        return cylinders;
    }

    /**
     * Sets the value of the cylinders property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCylinders(BigInteger value) {
        this.cylinders = value;
    }

    /**
     * Gets the value of the engineCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getEngineCapacity() {
        return engineCapacity;
    }

    /**
     * Sets the value of the engineCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setEngineCapacity(Amount value) {
        this.engineCapacity = value;
    }

    /**
     * Gets the value of the antiqueVehicle property.
     * This getter has been renamed from isAntiqueVehicle() to getAntiqueVehicle() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAntiqueVehicle() {
        return antiqueVehicle;
    }

    /**
     * Sets the value of the antiqueVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAntiqueVehicle(Boolean value) {
        this.antiqueVehicle = value;
    }

    /**
     * Gets the value of the classicVehicle property.
     * This getter has been renamed from isClassicVehicle() to getClassicVehicle() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getClassicVehicle() {
        return classicVehicle;
    }

    /**
     * Sets the value of the classicVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClassicVehicle(Boolean value) {
        this.classicVehicle = value;
    }

    /**
     * Gets the value of the highPerformance property.
     * This getter has been renamed from isHighPerformance() to getHighPerformance() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHighPerformance() {
        return highPerformance;
    }

    /**
     * Sets the value of the highPerformance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHighPerformance(Boolean value) {
        this.highPerformance = value;
    }

    /**
     * Gets the value of the highTheft property.
     * This getter has been renamed from isHighTheft() to getHighTheft() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHighTheft() {
        return highTheft;
    }

    /**
     * Sets the value of the highTheft property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHighTheft(Boolean value) {
        this.highTheft = value;
    }

    /**
     * Gets the value of the airbagsLocation property.
     * 
     * @return
     *     possible object is
     *     {@link AirbagsLocation }
     *     
     */
    public AirbagsLocation getAirbagsLocation() {
        return airbagsLocation;
    }

    /**
     * Sets the value of the airbagsLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirbagsLocation }
     *     
     */
    public void setAirbagsLocation(AirbagsLocation value) {
        this.airbagsLocation = value;
    }

    /**
     * Gets the value of the antiLockBrakes property.
     * This getter has been renamed from isAntiLockBrakes() to getAntiLockBrakes() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAntiLockBrakes() {
        return antiLockBrakes;
    }

    /**
     * Sets the value of the antiLockBrakes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAntiLockBrakes(Boolean value) {
        this.antiLockBrakes = value;
    }

    /**
     * Gets the value of the antiTheftDevice property.
     * This getter has been renamed from isAntiTheftDevice() to getAntiTheftDevice() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAntiTheftDevice() {
        return antiTheftDevice;
    }

    /**
     * Sets the value of the antiTheftDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAntiTheftDevice(Boolean value) {
        this.antiTheftDevice = value;
    }

    /**
     * Gets the value of the parkingPlace property.
     * 
     * @return
     *     possible object is
     *     {@link ParkingPlace }
     *     
     */
    public ParkingPlace getParkingPlace() {
        return parkingPlace;
    }

    /**
     * Sets the value of the parkingPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParkingPlace }
     *     
     */
    public void setParkingPlace(ParkingPlace value) {
        this.parkingPlace = value;
    }

    /**
     * Gets the value of the seatbeltsLocation property.
     * 
     * @return
     *     possible object is
     *     {@link SeatbeltsLocation }
     *     
     */
    public SeatbeltsLocation getSeatbeltsLocation() {
        return seatbeltsLocation;
    }

    /**
     * Sets the value of the seatbeltsLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatbeltsLocation }
     *     
     */
    public void setSeatbeltsLocation(SeatbeltsLocation value) {
        this.seatbeltsLocation = value;
    }

    /**
     * Gets the value of the daytimeRunningLights property.
     * This getter has been renamed from isDaytimeRunningLights() to getDaytimeRunningLights() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDaytimeRunningLights() {
        return daytimeRunningLights;
    }

    /**
     * Sets the value of the daytimeRunningLights property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDaytimeRunningLights(Boolean value) {
        this.daytimeRunningLights = value;
    }

    /**
     * Gets the value of the insuranceRatingGroup property.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceRatingGroup }
     *     
     */
    public InsuranceRatingGroup getInsuranceRatingGroup() {
        return insuranceRatingGroup;
    }

    /**
     * Sets the value of the insuranceRatingGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceRatingGroup }
     *     
     */
    public void setInsuranceRatingGroup(InsuranceRatingGroup value) {
        this.insuranceRatingGroup = value;
    }

    /**
     * Gets the value of the horsepower property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getHorsepower() {
        return horsepower;
    }

    /**
     * Sets the value of the horsepower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setHorsepower(Amount value) {
        this.horsepower = value;
    }

    /**
     * Gets the value of the displacementSize property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getDisplacementSize() {
        return displacementSize;
    }

    /**
     * Sets the value of the displacementSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setDisplacementSize(Amount value) {
        this.displacementSize = value;
    }

    /**
     * Gets the value of the vehicleModelSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleModelTO }
     *     
     */
    public VehicleModelTO getVehicleModelSpecification() {
        return vehicleModelSpecification;
    }

    /**
     * Sets the value of the vehicleModelSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleModelTO }
     *     
     */
    public void setVehicleModelSpecification(VehicleModelTO value) {
        this.vehicleModelSpecification = value;
    }

    /**
     * Gets the value of the vehicleRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicleRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicleRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VehicleRegistrationTO }
     * 
     * 
     */
    public List<VehicleRegistrationTO> getVehicleRegistration() {
        if (vehicleRegistration == null) {
            vehicleRegistration = new ArrayList<VehicleRegistrationTO>();
        }
        return this.vehicleRegistration;
    }

    /**
     * Gets the value of the isClassicVehicle property.
     * This getter has been renamed from isIsClassicVehicle() to getIsClassicVehicle() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsClassicVehicle() {
        return isClassicVehicle;
    }

    /**
     * Sets the value of the isClassicVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsClassicVehicle(Boolean value) {
        this.isClassicVehicle = value;
    }

    /**
     * Gets the value of the isHighPerformance property.
     * This getter has been renamed from isIsHighPerformance() to getIsHighPerformance() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsHighPerformance() {
        return isHighPerformance;
    }

    /**
     * Sets the value of the isHighPerformance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsHighPerformance(Boolean value) {
        this.isHighPerformance = value;
    }

    /**
     * Gets the value of the isTotalLoss property.
     * This getter has been renamed from isIsTotalLoss() to getIsTotalLoss() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsTotalLoss() {
        return isTotalLoss;
    }

    /**
     * Sets the value of the isTotalLoss property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTotalLoss(Boolean value) {
        this.isTotalLoss = value;
    }

    /**
     * Gets the value of the numberOfAirbags property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfAirbags() {
        return numberOfAirbags;
    }

    /**
     * Sets the value of the numberOfAirbags property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfAirbags(BigInteger value) {
        this.numberOfAirbags = value;
    }

    /**
     * Gets the value of the hasAlarm property.
     * This getter has been renamed from isHasAlarm() to getHasAlarm() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasAlarm() {
        return hasAlarm;
    }

    /**
     * Sets the value of the hasAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasAlarm(Boolean value) {
        this.hasAlarm = value;
    }

    /**
     * Gets the value of the vinPosition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinPosition() {
        return vinPosition;
    }

    /**
     * Sets the value of the vinPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinPosition(String value) {
        this.vinPosition = value;
    }

    /**
     * Gets the value of the vinPattern property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinPattern() {
        return vinPattern;
    }

    /**
     * Sets the value of the vinPattern property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinPattern(String value) {
        this.vinPattern = value;
    }

    /**
     * Gets the value of the derivedVehicleIndentificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDerivedVehicleIndentificationNumber() {
        return derivedVehicleIndentificationNumber;
    }

    /**
     * Sets the value of the derivedVehicleIndentificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDerivedVehicleIndentificationNumber(String value) {
        this.derivedVehicleIndentificationNumber = value;
    }

    /**
     * Gets the value of the originDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginDescription() {
        return originDescription;
    }

    /**
     * Sets the value of the originDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginDescription(String value) {
        this.originDescription = value;
    }

    /**
     * Gets the value of the vehicleAgeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleAgeCode() {
        return vehicleAgeCode;
    }

    /**
     * Sets the value of the vehicleAgeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleAgeCode(String value) {
        this.vehicleAgeCode = value;
    }

    /**
     * Gets the value of the originCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginCode() {
        return originCode;
    }

    /**
     * Sets the value of the originCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginCode(String value) {
        this.originCode = value;
    }

    /**
     * Gets the value of the vinWorldManufacturerIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinWorldManufacturerIdentifier() {
        return vinWorldManufacturerIdentifier;
    }

    /**
     * Sets the value of the vinWorldManufacturerIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinWorldManufacturerIdentifier(String value) {
        this.vinWorldManufacturerIdentifier = value;
    }

    /**
     * Gets the value of the vinVehicleDescriptorSection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinVehicleDescriptorSection() {
        return vinVehicleDescriptorSection;
    }

    /**
     * Sets the value of the vinVehicleDescriptorSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinVehicleDescriptorSection(String value) {
        this.vinVehicleDescriptorSection = value;
    }

    /**
     * Gets the value of the vinNorthAmericanCheckDigit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinNorthAmericanCheckDigit() {
        return vinNorthAmericanCheckDigit;
    }

    /**
     * Sets the value of the vinNorthAmericanCheckDigit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinNorthAmericanCheckDigit(String value) {
        this.vinNorthAmericanCheckDigit = value;
    }

    /**
     * Gets the value of the vinModelYearEncoding property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinModelYearEncoding() {
        return vinModelYearEncoding;
    }

    /**
     * Sets the value of the vinModelYearEncoding property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinModelYearEncoding(String value) {
        this.vinModelYearEncoding = value;
    }

    /**
     * Gets the value of the vinPlantCodeIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinPlantCodeIdentifier() {
        return vinPlantCodeIdentifier;
    }

    /**
     * Sets the value of the vinPlantCodeIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinPlantCodeIdentifier(String value) {
        this.vinPlantCodeIdentifier = value;
    }

    /**
     * Gets the value of the vinProductionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinProductionNumber() {
        return vinProductionNumber;
    }

    /**
     * Sets the value of the vinProductionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinProductionNumber(String value) {
        this.vinProductionNumber = value;
    }

    /**
     * Sets the value of the vehicleRegistration property.
     * 
     * @param vehicleRegistration
     *     allowed object is
     *     {@link VehicleRegistrationTO }
     *     
     */
    public void setVehicleRegistration(List<VehicleRegistrationTO> vehicleRegistration) {
        this.vehicleRegistration = vehicleRegistration;
    }

}
