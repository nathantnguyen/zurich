
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInFinancialServicesAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInFinancialServicesAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContract_TO">
 *       &lt;sequence>
 *         &lt;element name="roleInFinancialServicesAgreementRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}RoleInFinancialServicesAgreementType_TO" minOccurs="0"/>
 *         &lt;element name="riskExposures" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskExposure_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInFinancialServicesAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "roleInFinancialServicesAgreementRootType",
    "riskExposures"
})
@XmlSeeAlso({
    AccountInvolvedInFsaTO.class,
    ObjectCoveredRoleInFsaTO.class,
    RateInvolvedInFsaTO.class,
    FinancialServicesRoleTO.class
})
public class RoleInFinancialServicesAgreementTO
    extends RoleInContractTO
{

    protected RoleInFinancialServicesAgreementTypeTO roleInFinancialServicesAgreementRootType;
    protected List<RiskExposureTO> riskExposures;

    /**
     * Gets the value of the roleInFinancialServicesAgreementRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInFinancialServicesAgreementTypeTO }
     *     
     */
    public RoleInFinancialServicesAgreementTypeTO getRoleInFinancialServicesAgreementRootType() {
        return roleInFinancialServicesAgreementRootType;
    }

    /**
     * Sets the value of the roleInFinancialServicesAgreementRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInFinancialServicesAgreementTypeTO }
     *     
     */
    public void setRoleInFinancialServicesAgreementRootType(RoleInFinancialServicesAgreementTypeTO value) {
        this.roleInFinancialServicesAgreementRootType = value;
    }

    /**
     * Gets the value of the riskExposures property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riskExposures property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRiskExposures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskExposureTO }
     * 
     * 
     */
    public List<RiskExposureTO> getRiskExposures() {
        if (riskExposures == null) {
            riskExposures = new ArrayList<RiskExposureTO>();
        }
        return this.riskExposures;
    }

    /**
     * Sets the value of the riskExposures property.
     * 
     * @param riskExposures
     *     allowed object is
     *     {@link RiskExposureTO }
     *     
     */
    public void setRiskExposures(List<RiskExposureTO> riskExposures) {
        this.riskExposures = riskExposures;
    }

}
