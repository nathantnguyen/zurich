
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReserveAccountTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReserveAccountTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Risk Capital Reserve Account"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReserveAccountTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum ReserveAccountTypeName {

    @XmlEnumValue("Risk Capital Reserve Account")
    RISK_CAPITAL_RESERVE_ACCOUNT("Risk Capital Reserve Account");
    private final String value;

    ReserveAccountTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReserveAccountTypeName fromValue(String v) {
        for (ReserveAccountTypeName c: ReserveAccountTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
