
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO">
 *       &lt;sequence>
 *         &lt;element name="maximumCardinality" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="minimumCardinality" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "maximumCardinality",
    "minimumCardinality"
})
@XmlSeeAlso({
    RoleInCommunicationContentTypeTO.class,
    MoneyProvisionRoleInContractTypeTO.class,
    PartyInvolvedInFinancialServicesProductTypeTO.class,
    RoleInProductGroupTypeTO.class,
    PartyInvolvedInContractRequestTypeTO.class,
    AssessmentResultInvolvedInContractRequestTypeTO.class,
    RoleInFinancialServicesProductTypeTO.class,
    StandardTextSpecificationRoleInContractSpecificationTypeTO.class,
    RoleInContractRequestTypeTO.class,
    RoleInContractTypeTO.class,
    PartyInvolvedInCorporateAgreementTypeTO.class,
    PartyInvolvedInRiskAgreementTypeTO.class,
    RoleInCorporateAgreementTypeTO.class,
    RoleInRiskAgreementTypeTO.class,
    RoleInCorporateAgreementRequestTypeTO.class,
    RoleInRiskExposureTypeTO.class,
    PartyInvolvedInIntermediaryAgreementTypeTO.class,
    ChannelRoleTypeTO.class,
    RoleInStatisticsTypeTO.class,
    PartyInvolvedInEmploymentAgreementTypeTO.class,
    AccountRoleInFsaTypeTO.class,
    RoleInFinancialServicesRequestTypeTO.class,
    ServicingChannelTypeTO.class,
    RateRoleInFsaTypeTO.class,
    RoleInFinancialServicesAgreementTypeTO.class,
    FinancialServicesRoleTypeTO.class,
    RoleInTaskTypeTO.class,
    RoleInLegalActionTypeTO.class,
    RoleInFinancialTransactionTypeTO.class,
    RoleInAccountTypeTO.class,
    RoleInFinancialAssetTypeTO.class,
    PartyRoleInLegalActionTypeTO.class,
    EmploymentRoleTypeTO.class,
    RoleInGoalAndNeedTypeTO.class,
    RoleInRolePlayerTypeTO.class,
    ProviderRoleTypeTO.class
})
public class RoleTypeTO
    extends TypeTO
{

    protected BigInteger maximumCardinality;
    protected BigInteger minimumCardinality;

    /**
     * Gets the value of the maximumCardinality property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumCardinality() {
        return maximumCardinality;
    }

    /**
     * Sets the value of the maximumCardinality property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumCardinality(BigInteger value) {
        this.maximumCardinality = value;
    }

    /**
     * Gets the value of the minimumCardinality property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumCardinality() {
        return minimumCardinality;
    }

    /**
     * Sets the value of the minimumCardinality property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumCardinality(BigInteger value) {
        this.minimumCardinality = value;
    }

}
