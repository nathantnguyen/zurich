
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Domestication.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Domestication">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Wild Animal"/>
 *     &lt;enumeration value="Farm Animal"/>
 *     &lt;enumeration value="Domestic Animal"/>
 *     &lt;enumeration value="Domestic Inhouse Animal"/>
 *     &lt;enumeration value="Domestic Outhouse Animal"/>
 *     &lt;enumeration value="Domesticated Wild Animal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Domestication", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum Domestication {

    @XmlEnumValue("Wild Animal")
    WILD_ANIMAL("Wild Animal"),
    @XmlEnumValue("Farm Animal")
    FARM_ANIMAL("Farm Animal"),
    @XmlEnumValue("Domestic Animal")
    DOMESTIC_ANIMAL("Domestic Animal"),
    @XmlEnumValue("Domestic Inhouse Animal")
    DOMESTIC_INHOUSE_ANIMAL("Domestic Inhouse Animal"),
    @XmlEnumValue("Domestic Outhouse Animal")
    DOMESTIC_OUTHOUSE_ANIMAL("Domestic Outhouse Animal"),
    @XmlEnumValue("Domesticated Wild Animal")
    DOMESTICATED_WILD_ANIMAL("Domesticated Wild Animal");
    private final String value;

    Domestication(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Domestication fromValue(String v) {
        for (Domestication c: Domestication.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
