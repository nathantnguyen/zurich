
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInCommunicationContent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInCommunicationContent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="context" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" minOccurs="0"/>
 *         &lt;element name="owner" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="rolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="roleInCommunicationContentRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}RoleInCommunicationContentType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInCommunicationContent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "context",
    "owner",
    "rolePlayer",
    "roleInCommunicationContentRootType"
})
public class RoleInCommunicationContentTO
    extends RoleInContextClassTO
{

    protected CommunicationContentTO context;
    protected RolePlayerTO owner;
    protected RolePlayerTO rolePlayer;
    protected RoleInCommunicationContentTypeTO roleInCommunicationContentRootType;

    /**
     * Gets the value of the context property.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationContentTO }
     *     
     */
    public CommunicationContentTO getContext() {
        return context;
    }

    /**
     * Sets the value of the context property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setContext(CommunicationContentTO value) {
        this.context = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setOwner(RolePlayerTO value) {
        this.owner = value;
    }

    /**
     * Gets the value of the rolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getRolePlayer() {
        return rolePlayer;
    }

    /**
     * Sets the value of the rolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setRolePlayer(RolePlayerTO value) {
        this.rolePlayer = value;
    }

    /**
     * Gets the value of the roleInCommunicationContentRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInCommunicationContentTypeTO }
     *     
     */
    public RoleInCommunicationContentTypeTO getRoleInCommunicationContentRootType() {
        return roleInCommunicationContentRootType;
    }

    /**
     * Sets the value of the roleInCommunicationContentRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInCommunicationContentTypeTO }
     *     
     */
    public void setRoleInCommunicationContentRootType(RoleInCommunicationContentTypeTO value) {
        this.roleInCommunicationContentRootType = value;
    }

}
