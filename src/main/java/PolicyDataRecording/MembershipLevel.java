
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MembershipLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MembershipLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="First Head"/>
 *     &lt;enumeration value="Junior Member"/>
 *     &lt;enumeration value="Second Head"/>
 *     &lt;enumeration value="Head Of Household"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MembershipLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum MembershipLevel {

    @XmlEnumValue("First Head")
    FIRST_HEAD("First Head"),
    @XmlEnumValue("Junior Member")
    JUNIOR_MEMBER("Junior Member"),
    @XmlEnumValue("Second Head")
    SECOND_HEAD("Second Head"),
    @XmlEnumValue("Head Of Household")
    HEAD_OF_HOUSEHOLD("Head Of Household");
    private final String value;

    MembershipLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MembershipLevel fromValue(String v) {
        for (MembershipLevel c: MembershipLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
