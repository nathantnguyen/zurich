
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RuleSpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RuleSpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}SpecificationBuildingBlock_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="canBeOverwritten" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="commentOnFailure" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="inputParameters" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}NavigationPath_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="expressionSpec" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ExpressionSpec_TO" minOccurs="0"/>
 *         &lt;element name="ruleImplementation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessRule_TO" minOccurs="0"/>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/}RuleKind" minOccurs="0"/>
 *         &lt;element name="expression" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleSpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "name",
    "canBeOverwritten",
    "commentOnFailure",
    "inputParameters",
    "expressionSpec",
    "ruleImplementation",
    "kind",
    "expression"
})
public class RuleSpecTO
    extends SpecificationBuildingBlockTO
{

    protected String name;
    protected Boolean canBeOverwritten;
    protected String commentOnFailure;
    protected List<NavigationPathTO> inputParameters;
    protected ExpressionSpecTO expressionSpec;
    protected BusinessRuleTO ruleImplementation;
    protected RuleKind kind;
    protected String expression;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the canBeOverwritten property.
     * This getter has been renamed from isCanBeOverwritten() to getCanBeOverwritten() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCanBeOverwritten() {
        return canBeOverwritten;
    }

    /**
     * Sets the value of the canBeOverwritten property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanBeOverwritten(Boolean value) {
        this.canBeOverwritten = value;
    }

    /**
     * Gets the value of the commentOnFailure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentOnFailure() {
        return commentOnFailure;
    }

    /**
     * Sets the value of the commentOnFailure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentOnFailure(String value) {
        this.commentOnFailure = value;
    }

    /**
     * Gets the value of the inputParameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inputParameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInputParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NavigationPathTO }
     * 
     * 
     */
    public List<NavigationPathTO> getInputParameters() {
        if (inputParameters == null) {
            inputParameters = new ArrayList<NavigationPathTO>();
        }
        return this.inputParameters;
    }

    /**
     * Gets the value of the expressionSpec property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionSpecTO }
     *     
     */
    public ExpressionSpecTO getExpressionSpec() {
        return expressionSpec;
    }

    /**
     * Sets the value of the expressionSpec property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionSpecTO }
     *     
     */
    public void setExpressionSpec(ExpressionSpecTO value) {
        this.expressionSpec = value;
    }

    /**
     * Gets the value of the ruleImplementation property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessRuleTO }
     *     
     */
    public BusinessRuleTO getRuleImplementation() {
        return ruleImplementation;
    }

    /**
     * Sets the value of the ruleImplementation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessRuleTO }
     *     
     */
    public void setRuleImplementation(BusinessRuleTO value) {
        this.ruleImplementation = value;
    }

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link RuleKind }
     *     
     */
    public RuleKind getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleKind }
     *     
     */
    public void setKind(RuleKind value) {
        this.kind = value;
    }

    /**
     * Gets the value of the expression property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpression() {
        return expression;
    }

    /**
     * Sets the value of the expression property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpression(String value) {
        this.expression = value;
    }

    /**
     * Sets the value of the inputParameters property.
     * 
     * @param inputParameters
     *     allowed object is
     *     {@link NavigationPathTO }
     *     
     */
    public void setInputParameters(List<NavigationPathTO> inputParameters) {
        this.inputParameters = inputParameters;
    }

}
