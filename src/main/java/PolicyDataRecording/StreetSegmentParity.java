
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StreetSegmentParity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StreetSegmentParity">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Left side"/>
 *     &lt;enumeration value="Right side"/>
 *     &lt;enumeration value="Both sides"/>
 *     &lt;enumeration value="Undetermined"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StreetSegmentParity", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum StreetSegmentParity {

    @XmlEnumValue("Left side")
    LEFT_SIDE("Left side"),
    @XmlEnumValue("Right side")
    RIGHT_SIDE("Right side"),
    @XmlEnumValue("Both sides")
    BOTH_SIDES("Both sides"),
    @XmlEnumValue("Undetermined")
    UNDETERMINED("Undetermined");
    private final String value;

    StreetSegmentParity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StreetSegmentParity fromValue(String v) {
        for (StreetSegmentParity c: StreetSegmentParity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
