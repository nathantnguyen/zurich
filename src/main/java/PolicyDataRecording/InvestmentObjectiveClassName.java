
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvestmentObjectiveClassName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InvestmentObjectiveClassName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Technology Shares In Euro Market"/>
 *     &lt;enumeration value="Euro Bonds"/>
 *     &lt;enumeration value="Usd Bonds"/>
 *     &lt;enumeration value="Shares In Bric Markets"/>
 *     &lt;enumeration value="S&amp;p 500 Index Tracking"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InvestmentObjectiveClassName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum InvestmentObjectiveClassName {

    @XmlEnumValue("Technology Shares In Euro Market")
    TECHNOLOGY_SHARES_IN_EURO_MARKET("Technology Shares In Euro Market"),
    @XmlEnumValue("Euro Bonds")
    EURO_BONDS("Euro Bonds"),
    @XmlEnumValue("Usd Bonds")
    USD_BONDS("Usd Bonds"),
    @XmlEnumValue("Shares In Bric Markets")
    SHARES_IN_BRIC_MARKETS("Shares In Bric Markets"),
    @XmlEnumValue("S&p 500 Index Tracking")
    S_P_500_INDEX_TRACKING("S&p 500 Index Tracking");
    private final String value;

    InvestmentObjectiveClassName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InvestmentObjectiveClassName fromValue(String v) {
        for (InvestmentObjectiveClassName c: InvestmentObjectiveClassName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
