
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountRelationshipTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountRelationshipTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Account Shadowing"/>
 *     &lt;enumeration value="Account Consolidation"/>
 *     &lt;enumeration value="Account Hedging"/>
 *     &lt;enumeration value="Account Composition"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AccountRelationshipTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum AccountRelationshipTypeName {

    @XmlEnumValue("Account Shadowing")
    ACCOUNT_SHADOWING("Account Shadowing"),
    @XmlEnumValue("Account Consolidation")
    ACCOUNT_CONSOLIDATION("Account Consolidation"),
    @XmlEnumValue("Account Hedging")
    ACCOUNT_HEDGING("Account Hedging"),
    @XmlEnumValue("Account Composition")
    ACCOUNT_COMPOSITION("Account Composition");
    private final String value;

    AccountRelationshipTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountRelationshipTypeName fromValue(String v) {
        for (AccountRelationshipTypeName c: AccountRelationshipTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
