
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoveragePriority.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CoveragePriority">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Excess"/>
 *     &lt;enumeration value="Contribution Per Limit Ratio"/>
 *     &lt;enumeration value="Equal Share"/>
 *     &lt;enumeration value="Primary"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CoveragePriority", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum CoveragePriority {

    @XmlEnumValue("Excess")
    EXCESS("Excess"),
    @XmlEnumValue("Contribution Per Limit Ratio")
    CONTRIBUTION_PER_LIMIT_RATIO("Contribution Per Limit Ratio"),
    @XmlEnumValue("Equal Share")
    EQUAL_SHARE("Equal Share"),
    @XmlEnumValue("Primary")
    PRIMARY("Primary");
    private final String value;

    CoveragePriority(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CoveragePriority fromValue(String v) {
        for (CoveragePriority c: CoveragePriority.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
