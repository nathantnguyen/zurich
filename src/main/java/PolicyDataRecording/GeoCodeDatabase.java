
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeoCodeDatabase.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GeoCodeDatabase">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="StandardOnly"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GeoCodeDatabase", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum GeoCodeDatabase {

    @XmlEnumValue("StandardOnly")
    STANDARD_ONLY("StandardOnly");
    private final String value;

    GeoCodeDatabase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GeoCodeDatabase fromValue(String v) {
        for (GeoCodeDatabase c: GeoCodeDatabase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
