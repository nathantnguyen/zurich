
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialValuationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FinancialValuationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Market Value"/>
 *     &lt;enumeration value="Lifetime Value"/>
 *     &lt;enumeration value="Loss Estimate"/>
 *     &lt;enumeration value="Cash Flow Estimate"/>
 *     &lt;enumeration value="Actuarial Valuation"/>
 *     &lt;enumeration value="Claims Outstanding Best Estimate"/>
 *     &lt;enumeration value="Future Premium Expenses"/>
 *     &lt;enumeration value="Future Premium Gross Of Reinsurance"/>
 *     &lt;enumeration value="Future Premium Net Of Reinsurance"/>
 *     &lt;enumeration value="Net Earned Premiums Best Estimate"/>
 *     &lt;enumeration value="Net Written Premiums Best Estimate"/>
 *     &lt;enumeration value="Present Value Of Annuities Payable"/>
 *     &lt;enumeration value="Present Value Of Future Payments"/>
 *     &lt;enumeration value="Present Value Of Net Premiums"/>
 *     &lt;enumeration value="Technical Provisions Best Estimate"/>
 *     &lt;enumeration value="Gross Earned Premium Best Estimate"/>
 *     &lt;enumeration value="Reinstatement Premium Best Estimate"/>
 *     &lt;enumeration value="Reinsurance Recoverables Best Estimate"/>
 *     &lt;enumeration value="Revenue Override"/>
 *     &lt;enumeration value="Total Insured Value"/>
 *     &lt;enumeration value="Credit Valuation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FinancialValuationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum FinancialValuationTypeName {

    @XmlEnumValue("Market Value")
    MARKET_VALUE("Market Value"),
    @XmlEnumValue("Lifetime Value")
    LIFETIME_VALUE("Lifetime Value"),
    @XmlEnumValue("Loss Estimate")
    LOSS_ESTIMATE("Loss Estimate"),
    @XmlEnumValue("Cash Flow Estimate")
    CASH_FLOW_ESTIMATE("Cash Flow Estimate"),
    @XmlEnumValue("Actuarial Valuation")
    ACTUARIAL_VALUATION("Actuarial Valuation"),
    @XmlEnumValue("Claims Outstanding Best Estimate")
    CLAIMS_OUTSTANDING_BEST_ESTIMATE("Claims Outstanding Best Estimate"),
    @XmlEnumValue("Future Premium Expenses")
    FUTURE_PREMIUM_EXPENSES("Future Premium Expenses"),
    @XmlEnumValue("Future Premium Gross Of Reinsurance")
    FUTURE_PREMIUM_GROSS_OF_REINSURANCE("Future Premium Gross Of Reinsurance"),
    @XmlEnumValue("Future Premium Net Of Reinsurance")
    FUTURE_PREMIUM_NET_OF_REINSURANCE("Future Premium Net Of Reinsurance"),
    @XmlEnumValue("Net Earned Premiums Best Estimate")
    NET_EARNED_PREMIUMS_BEST_ESTIMATE("Net Earned Premiums Best Estimate"),
    @XmlEnumValue("Net Written Premiums Best Estimate")
    NET_WRITTEN_PREMIUMS_BEST_ESTIMATE("Net Written Premiums Best Estimate"),
    @XmlEnumValue("Present Value Of Annuities Payable")
    PRESENT_VALUE_OF_ANNUITIES_PAYABLE("Present Value Of Annuities Payable"),
    @XmlEnumValue("Present Value Of Future Payments")
    PRESENT_VALUE_OF_FUTURE_PAYMENTS("Present Value Of Future Payments"),
    @XmlEnumValue("Present Value Of Net Premiums")
    PRESENT_VALUE_OF_NET_PREMIUMS("Present Value Of Net Premiums"),
    @XmlEnumValue("Technical Provisions Best Estimate")
    TECHNICAL_PROVISIONS_BEST_ESTIMATE("Technical Provisions Best Estimate"),
    @XmlEnumValue("Gross Earned Premium Best Estimate")
    GROSS_EARNED_PREMIUM_BEST_ESTIMATE("Gross Earned Premium Best Estimate"),
    @XmlEnumValue("Reinstatement Premium Best Estimate")
    REINSTATEMENT_PREMIUM_BEST_ESTIMATE("Reinstatement Premium Best Estimate"),
    @XmlEnumValue("Reinsurance Recoverables Best Estimate")
    REINSURANCE_RECOVERABLES_BEST_ESTIMATE("Reinsurance Recoverables Best Estimate"),
    @XmlEnumValue("Revenue Override")
    REVENUE_OVERRIDE("Revenue Override"),
    @XmlEnumValue("Total Insured Value")
    TOTAL_INSURED_VALUE("Total Insured Value"),
    @XmlEnumValue("Credit Valuation")
    CREDIT_VALUATION("Credit Valuation");
    private final String value;

    FinancialValuationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FinancialValuationTypeName fromValue(String v) {
        for (FinancialValuationTypeName c: FinancialValuationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
