
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FinancialServicesRequestDetail_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesRequestDetail_TO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="zurichSharePercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="inOfficeDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="quoteDueDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesRequestDetail_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "zurichSharePercentage",
    "inOfficeDate",
    "quoteDueDateTime"
})
public class FinancialServicesRequestDetailTO {

    protected BigDecimal zurichSharePercentage;
    protected XMLGregorianCalendar inOfficeDate;
    protected XMLGregorianCalendar quoteDueDateTime;

    /**
     * Gets the value of the zurichSharePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getZurichSharePercentage() {
        return zurichSharePercentage;
    }

    /**
     * Sets the value of the zurichSharePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setZurichSharePercentage(BigDecimal value) {
        this.zurichSharePercentage = value;
    }

    /**
     * Gets the value of the inOfficeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInOfficeDate() {
        return inOfficeDate;
    }

    /**
     * Sets the value of the inOfficeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInOfficeDate(XMLGregorianCalendar value) {
        this.inOfficeDate = value;
    }

    /**
     * Gets the value of the quoteDueDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQuoteDueDateTime() {
        return quoteDueDateTime;
    }

    /**
     * Sets the value of the quoteDueDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQuoteDueDateTime(XMLGregorianCalendar value) {
        this.quoteDueDateTime = value;
    }

}
