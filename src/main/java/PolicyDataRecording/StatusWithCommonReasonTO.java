
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StatusWithCommonReason_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StatusWithCommonReason_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Status_TO">
 *       &lt;sequence>
 *         &lt;element name="reason" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusWithCommonReason_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "reason"
})
@XmlSeeAlso({
    CommunicationStatusTO.class,
    ContractRequestStatusTO.class,
    ContractRequestSpecificationStatusTO.class,
    ContractSpecificationStatusTO.class,
    CustomerOfProductGroupStatusTO.class,
    RiskExposureStatusTO.class,
    ActivityOccurrenceStatusTO.class,
    AssessmentResultStatusTO.class,
    RegistrationStatusTO.class,
    IntermediaryAgreementStatusTO.class,
    LifeFormStatusTO.class,
    AccountAgreementStatusTO.class,
    InsurancePolicyStatusTO.class,
    AgreementSpecStatusTO.class,
    SpecificationBuildingBlockStatusTO.class,
    FinancialStatementStatusTO.class,
    PaymentStatusTO.class,
    PaymentDueStatusTO.class,
    AccountStatusTO.class,
    CustomerStatusTO.class,
    EmploymentStatusTO.class,
    GeoCoderStatusTO.class,
    SuspectDuplicateStatusTO.class
})
public class StatusWithCommonReasonTO
    extends StatusTO
{

    protected String reason;

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
