
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountStatement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountStatement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}FinancialStatement_TO">
 *       &lt;sequence>
 *         &lt;element name="marketingInformation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="billingRoles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}RoleInFinancialTransaction_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="electronicAddress" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ElectronicAddress_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="accountAgreementReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountStatement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "marketingInformation",
    "billingRoles",
    "electronicAddress",
    "accountAgreementReference"
})
@XmlSeeAlso({
    CommissionStatementTO.class,
    CreditCardStatementTO.class
})
public class AccountStatementTO
    extends FinancialStatementTO
{

    protected String marketingInformation;
    protected List<RoleInFinancialTransactionTO> billingRoles;
    protected List<ElectronicAddressTO> electronicAddress;
    protected ObjectReferenceTO accountAgreementReference;

    /**
     * Gets the value of the marketingInformation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketingInformation() {
        return marketingInformation;
    }

    /**
     * Sets the value of the marketingInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketingInformation(String value) {
        this.marketingInformation = value;
    }

    /**
     * Gets the value of the billingRoles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the billingRoles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBillingRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInFinancialTransactionTO }
     * 
     * 
     */
    public List<RoleInFinancialTransactionTO> getBillingRoles() {
        if (billingRoles == null) {
            billingRoles = new ArrayList<RoleInFinancialTransactionTO>();
        }
        return this.billingRoles;
    }

    /**
     * Gets the value of the electronicAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the electronicAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElectronicAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElectronicAddressTO }
     * 
     * 
     */
    public List<ElectronicAddressTO> getElectronicAddress() {
        if (electronicAddress == null) {
            electronicAddress = new ArrayList<ElectronicAddressTO>();
        }
        return this.electronicAddress;
    }

    /**
     * Gets the value of the accountAgreementReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getAccountAgreementReference() {
        return accountAgreementReference;
    }

    /**
     * Sets the value of the accountAgreementReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setAccountAgreementReference(ObjectReferenceTO value) {
        this.accountAgreementReference = value;
    }

    /**
     * Sets the value of the billingRoles property.
     * 
     * @param billingRoles
     *     allowed object is
     *     {@link RoleInFinancialTransactionTO }
     *     
     */
    public void setBillingRoles(List<RoleInFinancialTransactionTO> billingRoles) {
        this.billingRoles = billingRoles;
    }

    /**
     * Sets the value of the electronicAddress property.
     * 
     * @param electronicAddress
     *     allowed object is
     *     {@link ElectronicAddressTO }
     *     
     */
    public void setElectronicAddress(List<ElectronicAddressTO> electronicAddress) {
        this.electronicAddress = electronicAddress;
    }

}
