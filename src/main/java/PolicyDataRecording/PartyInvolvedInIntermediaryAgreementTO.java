
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyInvolvedInIntermediaryAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyInvolvedInIntermediaryAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}RoleInIntermediaryAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="rolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Party_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyInvolvedInIntermediaryAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/", propOrder = {
    "rolePlayer"
})
@XmlSeeAlso({
    ChannelRoleTO.class
})
public class PartyInvolvedInIntermediaryAgreementTO
    extends RoleInIntermediaryAgreementTO
{

    protected PartyTO rolePlayer;

    /**
     * Gets the value of the rolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link PartyTO }
     *     
     */
    public PartyTO getRolePlayer() {
        return rolePlayer;
    }

    /**
     * Sets the value of the rolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyTO }
     *     
     */
    public void setRolePlayer(PartyTO value) {
        this.rolePlayer = value;
    }

}
