
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DebtInstrumentTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DebtInstrumentTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Bank Bill Of Exchange"/>
 *     &lt;enumeration value="Bank Guarantee"/>
 *     &lt;enumeration value="Bill Instrument"/>
 *     &lt;enumeration value="Commercial Bill"/>
 *     &lt;enumeration value="Promissory Note"/>
 *     &lt;enumeration value="Bank Note"/>
 *     &lt;enumeration value="Banker Acceptance"/>
 *     &lt;enumeration value="Commercial Paper"/>
 *     &lt;enumeration value="Corporate Note"/>
 *     &lt;enumeration value="Deposit Note"/>
 *     &lt;enumeration value="Treasury Note"/>
 *     &lt;enumeration value="Trade Bill Of Exchange"/>
 *     &lt;enumeration value="Treasury Bill"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DebtInstrumentTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum DebtInstrumentTypeName {

    @XmlEnumValue("Bank Bill Of Exchange")
    BANK_BILL_OF_EXCHANGE("Bank Bill Of Exchange"),
    @XmlEnumValue("Bank Guarantee")
    BANK_GUARANTEE("Bank Guarantee"),
    @XmlEnumValue("Bill Instrument")
    BILL_INSTRUMENT("Bill Instrument"),
    @XmlEnumValue("Commercial Bill")
    COMMERCIAL_BILL("Commercial Bill"),
    @XmlEnumValue("Promissory Note")
    PROMISSORY_NOTE("Promissory Note"),
    @XmlEnumValue("Bank Note")
    BANK_NOTE("Bank Note"),
    @XmlEnumValue("Banker Acceptance")
    BANKER_ACCEPTANCE("Banker Acceptance"),
    @XmlEnumValue("Commercial Paper")
    COMMERCIAL_PAPER("Commercial Paper"),
    @XmlEnumValue("Corporate Note")
    CORPORATE_NOTE("Corporate Note"),
    @XmlEnumValue("Deposit Note")
    DEPOSIT_NOTE("Deposit Note"),
    @XmlEnumValue("Treasury Note")
    TREASURY_NOTE("Treasury Note"),
    @XmlEnumValue("Trade Bill Of Exchange")
    TRADE_BILL_OF_EXCHANGE("Trade Bill Of Exchange"),
    @XmlEnumValue("Treasury Bill")
    TREASURY_BILL("Treasury Bill");
    private final String value;

    DebtInstrumentTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DebtInstrumentTypeName fromValue(String v) {
        for (DebtInstrumentTypeName c: DebtInstrumentTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
