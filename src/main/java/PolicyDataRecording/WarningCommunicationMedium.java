
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WarningCommunicationMedium.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WarningCommunicationMedium">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Email"/>
 *     &lt;enumeration value="Sms"/>
 *     &lt;enumeration value="Telephone"/>
 *     &lt;enumeration value="Wap"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "WarningCommunicationMedium", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum WarningCommunicationMedium {

    @XmlEnumValue("Email")
    EMAIL("Email"),
    @XmlEnumValue("Sms")
    SMS("Sms"),
    @XmlEnumValue("Telephone")
    TELEPHONE("Telephone"),
    @XmlEnumValue("Wap")
    WAP("Wap");
    private final String value;

    WarningCommunicationMedium(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WarningCommunicationMedium fromValue(String v) {
        for (WarningCommunicationMedium c: WarningCommunicationMedium.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
