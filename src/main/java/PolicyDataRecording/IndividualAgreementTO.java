
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IndividualAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IndividualAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}InsurancePolicy_TO">
 *       &lt;sequence>
 *         &lt;element name="rateClass" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="discountCategory" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}DiscountCategory" minOccurs="0"/>
 *         &lt;element name="multiLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndividualAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "rateClass",
    "discountCategory",
    "multiLevel"
})
@XmlSeeAlso({
    IndividualInvestmentPolicyTO.class,
    AutomobileInsurancePolicyTO.class,
    LifeAndHealthPolicyTO.class
})
public class IndividualAgreementTO
    extends InsurancePolicyTO
{

    protected String rateClass;
    protected DiscountCategory discountCategory;
    protected Boolean multiLevel;

    /**
     * Gets the value of the rateClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateClass() {
        return rateClass;
    }

    /**
     * Sets the value of the rateClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateClass(String value) {
        this.rateClass = value;
    }

    /**
     * Gets the value of the discountCategory property.
     * 
     * @return
     *     possible object is
     *     {@link DiscountCategory }
     *     
     */
    public DiscountCategory getDiscountCategory() {
        return discountCategory;
    }

    /**
     * Sets the value of the discountCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link DiscountCategory }
     *     
     */
    public void setDiscountCategory(DiscountCategory value) {
        this.discountCategory = value;
    }

    /**
     * Gets the value of the multiLevel property.
     * This getter has been renamed from isMultiLevel() to getMultiLevel() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultiLevel() {
        return multiLevel;
    }

    /**
     * Sets the value of the multiLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiLevel(Boolean value) {
        this.multiLevel = value;
    }

}
