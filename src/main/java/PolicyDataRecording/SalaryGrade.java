
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalaryGrade.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SalaryGrade">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Junior"/>
 *     &lt;enumeration value="Senior"/>
 *     &lt;enumeration value="Entry Level"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SalaryGrade", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum SalaryGrade {

    @XmlEnumValue("Junior")
    JUNIOR("Junior"),
    @XmlEnumValue("Senior")
    SENIOR("Senior"),
    @XmlEnumValue("Entry Level")
    ENTRY_LEVEL("Entry Level");
    private final String value;

    SalaryGrade(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SalaryGrade fromValue(String v) {
        for (SalaryGrade c: SalaryGrade.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
