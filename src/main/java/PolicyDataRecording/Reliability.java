
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Reliability.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Reliability">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Estimated"/>
 *     &lt;enumeration value="Calculated"/>
 *     &lt;enumeration value="Scored"/>
 *     &lt;enumeration value="Known"/>
 *     &lt;enumeration value="Appraised"/>
 *     &lt;enumeration value="Assumed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Reliability", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum Reliability {

    @XmlEnumValue("Estimated")
    ESTIMATED("Estimated"),
    @XmlEnumValue("Calculated")
    CALCULATED("Calculated"),
    @XmlEnumValue("Scored")
    SCORED("Scored"),
    @XmlEnumValue("Known")
    KNOWN("Known"),
    @XmlEnumValue("Appraised")
    APPRAISED("Appraised"),
    @XmlEnumValue("Assumed")
    ASSUMED("Assumed");
    private final String value;

    Reliability(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Reliability fromValue(String v) {
        for (Reliability c: Reliability.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
