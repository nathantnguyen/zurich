
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntermediaryAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntermediaryAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO">
 *       &lt;sequence>
 *         &lt;element name="intermediaryAgreementComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}IntermediaryAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="commissionSchedules" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}CommissionSchedule_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInIntermediaryAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}RoleInIntermediaryAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="businessUnitSpecificIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntermediaryAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/", propOrder = {
    "intermediaryAgreementComponents",
    "status",
    "commissionSchedules",
    "rolesInIntermediaryAgreement",
    "businessUnitSpecificIndicator"
})
public class IntermediaryAgreementTO
    extends ContractTO
{

    protected List<IntermediaryAgreementTO> intermediaryAgreementComponents;
    protected List<String> status;
    protected List<CommissionScheduleTO> commissionSchedules;
    protected List<RoleInIntermediaryAgreementTO> rolesInIntermediaryAgreement;
    protected String businessUnitSpecificIndicator;

    /**
     * Gets the value of the intermediaryAgreementComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intermediaryAgreementComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntermediaryAgreementComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntermediaryAgreementTO }
     * 
     * 
     */
    public List<IntermediaryAgreementTO> getIntermediaryAgreementComponents() {
        if (intermediaryAgreementComponents == null) {
            intermediaryAgreementComponents = new ArrayList<IntermediaryAgreementTO>();
        }
        return this.intermediaryAgreementComponents;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getStatus() {
        if (status == null) {
            status = new ArrayList<String>();
        }
        return this.status;
    }

    /**
     * Gets the value of the commissionSchedules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commissionSchedules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommissionSchedules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommissionScheduleTO }
     * 
     * 
     */
    public List<CommissionScheduleTO> getCommissionSchedules() {
        if (commissionSchedules == null) {
            commissionSchedules = new ArrayList<CommissionScheduleTO>();
        }
        return this.commissionSchedules;
    }

    /**
     * Gets the value of the rolesInIntermediaryAgreement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInIntermediaryAgreement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInIntermediaryAgreement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInIntermediaryAgreementTO }
     * 
     * 
     */
    public List<RoleInIntermediaryAgreementTO> getRolesInIntermediaryAgreement() {
        if (rolesInIntermediaryAgreement == null) {
            rolesInIntermediaryAgreement = new ArrayList<RoleInIntermediaryAgreementTO>();
        }
        return this.rolesInIntermediaryAgreement;
    }

    /**
     * Gets the value of the businessUnitSpecificIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessUnitSpecificIndicator() {
        return businessUnitSpecificIndicator;
    }

    /**
     * Sets the value of the businessUnitSpecificIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessUnitSpecificIndicator(String value) {
        this.businessUnitSpecificIndicator = value;
    }

    /**
     * Sets the value of the intermediaryAgreementComponents property.
     * 
     * @param intermediaryAgreementComponents
     *     allowed object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public void setIntermediaryAgreementComponents(List<IntermediaryAgreementTO> intermediaryAgreementComponents) {
        this.intermediaryAgreementComponents = intermediaryAgreementComponents;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(List<String> status) {
        this.status = status;
    }

    /**
     * Sets the value of the commissionSchedules property.
     * 
     * @param commissionSchedules
     *     allowed object is
     *     {@link CommissionScheduleTO }
     *     
     */
    public void setCommissionSchedules(List<CommissionScheduleTO> commissionSchedules) {
        this.commissionSchedules = commissionSchedules;
    }

    /**
     * Sets the value of the rolesInIntermediaryAgreement property.
     * 
     * @param rolesInIntermediaryAgreement
     *     allowed object is
     *     {@link RoleInIntermediaryAgreementTO }
     *     
     */
    public void setRolesInIntermediaryAgreement(List<RoleInIntermediaryAgreementTO> rolesInIntermediaryAgreement) {
        this.rolesInIntermediaryAgreement = rolesInIntermediaryAgreement;
    }

}
