
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScoreRange_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScoreRange_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="lowerLimit" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="upperLimit" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="appliedOn" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="appliedOnElement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PredictiveModelScoreElement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScoreRange_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "lowerLimit",
    "upperLimit",
    "description",
    "appliedOn",
    "appliedOnElement"
})
public class ScoreRangeTO
    extends DependentObjectTO
{

    protected String lowerLimit;
    protected String upperLimit;
    protected String description;
    protected List<ScoreTO> appliedOn;
    protected List<PredictiveModelScoreElementTO> appliedOnElement;

    /**
     * Gets the value of the lowerLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLowerLimit() {
        return lowerLimit;
    }

    /**
     * Sets the value of the lowerLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLowerLimit(String value) {
        this.lowerLimit = value;
    }

    /**
     * Gets the value of the upperLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpperLimit() {
        return upperLimit;
    }

    /**
     * Sets the value of the upperLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpperLimit(String value) {
        this.upperLimit = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the appliedOn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appliedOn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppliedOn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScoreTO }
     * 
     * 
     */
    public List<ScoreTO> getAppliedOn() {
        if (appliedOn == null) {
            appliedOn = new ArrayList<ScoreTO>();
        }
        return this.appliedOn;
    }

    /**
     * Gets the value of the appliedOnElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appliedOnElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppliedOnElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PredictiveModelScoreElementTO }
     * 
     * 
     */
    public List<PredictiveModelScoreElementTO> getAppliedOnElement() {
        if (appliedOnElement == null) {
            appliedOnElement = new ArrayList<PredictiveModelScoreElementTO>();
        }
        return this.appliedOnElement;
    }

    /**
     * Sets the value of the appliedOn property.
     * 
     * @param appliedOn
     *     allowed object is
     *     {@link ScoreTO }
     *     
     */
    public void setAppliedOn(List<ScoreTO> appliedOn) {
        this.appliedOn = appliedOn;
    }

    /**
     * Sets the value of the appliedOnElement property.
     * 
     * @param appliedOnElement
     *     allowed object is
     *     {@link PredictiveModelScoreElementTO }
     *     
     */
    public void setAppliedOnElement(List<PredictiveModelScoreElementTO> appliedOnElement) {
        this.appliedOnElement = appliedOnElement;
    }

}
