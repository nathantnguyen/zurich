
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractSpecificationRelationshipTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContractSpecificationRelationshipTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Product Conversion"/>
 *     &lt;enumeration value="Specification Customisation"/>
 *     &lt;enumeration value="Specification Master"/>
 *     &lt;enumeration value="Product Umbrella"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContractSpecificationRelationshipTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum ContractSpecificationRelationshipTypeName {

    @XmlEnumValue("Product Conversion")
    PRODUCT_CONVERSION("Product Conversion"),
    @XmlEnumValue("Specification Customisation")
    SPECIFICATION_CUSTOMISATION("Specification Customisation"),
    @XmlEnumValue("Specification Master")
    SPECIFICATION_MASTER("Specification Master"),
    @XmlEnumValue("Product Umbrella")
    PRODUCT_UMBRELLA("Product Umbrella");
    private final String value;

    ContractSpecificationRelationshipTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContractSpecificationRelationshipTypeName fromValue(String v) {
        for (ContractSpecificationRelationshipTypeName c: ContractSpecificationRelationshipTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
