
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Occupation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Occupation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeneralActivity_TO">
 *       &lt;sequence>
 *         &lt;element name="occupationPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="professionalTitle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="occupationCode" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}OccupationCode" minOccurs="0"/>
 *         &lt;element name="income" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="incomeIsEstimated" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="estimatedRetirementDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="isRetired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Occupation_TO", propOrder = {
    "occupationPercentage",
    "professionalTitle",
    "occupationCode",
    "income",
    "incomeIsEstimated",
    "estimatedRetirementDate",
    "isRetired"
})
public class OccupationTO
    extends GeneralActivityTO
{

    protected BigDecimal occupationPercentage;
    protected String professionalTitle;
    protected String occupationCode;
    protected BaseCurrencyAmount income;
    protected Boolean incomeIsEstimated;
    protected XMLGregorianCalendar estimatedRetirementDate;
    protected Boolean isRetired;

    /**
     * Gets the value of the occupationPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOccupationPercentage() {
        return occupationPercentage;
    }

    /**
     * Sets the value of the occupationPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOccupationPercentage(BigDecimal value) {
        this.occupationPercentage = value;
    }

    /**
     * Gets the value of the professionalTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfessionalTitle() {
        return professionalTitle;
    }

    /**
     * Sets the value of the professionalTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfessionalTitle(String value) {
        this.professionalTitle = value;
    }

    /**
     * Gets the value of the occupationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationCode() {
        return occupationCode;
    }

    /**
     * Sets the value of the occupationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationCode(String value) {
        this.occupationCode = value;
    }

    /**
     * Gets the value of the income property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getIncome() {
        return income;
    }

    /**
     * Sets the value of the income property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setIncome(BaseCurrencyAmount value) {
        this.income = value;
    }

    /**
     * Gets the value of the incomeIsEstimated property.
     * This getter has been renamed from isIncomeIsEstimated() to getIncomeIsEstimated() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIncomeIsEstimated() {
        return incomeIsEstimated;
    }

    /**
     * Sets the value of the incomeIsEstimated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncomeIsEstimated(Boolean value) {
        this.incomeIsEstimated = value;
    }

    /**
     * Gets the value of the estimatedRetirementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEstimatedRetirementDate() {
        return estimatedRetirementDate;
    }

    /**
     * Sets the value of the estimatedRetirementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEstimatedRetirementDate(XMLGregorianCalendar value) {
        this.estimatedRetirementDate = value;
    }

    /**
     * Gets the value of the isRetired property.
     * This getter has been renamed from isIsRetired() to getIsRetired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsRetired() {
        return isRetired;
    }

    /**
     * Sets the value of the isRetired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRetired(Boolean value) {
        this.isRetired = value;
    }

}
