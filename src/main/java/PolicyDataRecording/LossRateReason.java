
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LossRateReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LossRateReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Total Loss Burnt Out"/>
 *     &lt;enumeration value="Total Loss Economically Not Repairable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LossRateReason", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum LossRateReason {

    @XmlEnumValue("Total Loss Burnt Out")
    TOTAL_LOSS_BURNT_OUT("Total Loss Burnt Out"),
    @XmlEnumValue("Total Loss Economically Not Repairable")
    TOTAL_LOSS_ECONOMICALLY_NOT_REPAIRABLE("Total Loss Economically Not Repairable");
    private final String value;

    LossRateReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LossRateReason fromValue(String v) {
        for (LossRateReason c: LossRateReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
