
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyRegistrationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PartyRegistrationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Third Party Company Identification"/>
 *     &lt;enumeration value="Divorce Decree"/>
 *     &lt;enumeration value="Conviction"/>
 *     &lt;enumeration value="Domestic Partnership Registration"/>
 *     &lt;enumeration value="Dun And Bradstreet"/>
 *     &lt;enumeration value="Tax Registration"/>
 *     &lt;enumeration value="United States Department of Transporation"/>
 *     &lt;enumeration value="Interstate Commerce Commission"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PartyRegistrationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum PartyRegistrationTypeName {

    @XmlEnumValue("Third Party Company Identification")
    THIRD_PARTY_COMPANY_IDENTIFICATION("Third Party Company Identification"),
    @XmlEnumValue("Divorce Decree")
    DIVORCE_DECREE("Divorce Decree"),
    @XmlEnumValue("Conviction")
    CONVICTION("Conviction"),
    @XmlEnumValue("Domestic Partnership Registration")
    DOMESTIC_PARTNERSHIP_REGISTRATION("Domestic Partnership Registration"),
    @XmlEnumValue("Dun And Bradstreet")
    DUN_AND_BRADSTREET("Dun And Bradstreet"),
    @XmlEnumValue("Tax Registration")
    TAX_REGISTRATION("Tax Registration"),
    @XmlEnumValue("United States Department of Transporation")
    UNITED_STATES_DEPARTMENT_OF_TRANSPORATION("United States Department of Transporation"),
    @XmlEnumValue("Interstate Commerce Commission")
    INTERSTATE_COMMERCE_COMMISSION("Interstate Commerce Commission");
    private final String value;

    PartyRegistrationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartyRegistrationTypeName fromValue(String v) {
        for (PartyRegistrationTypeName c: PartyRegistrationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
