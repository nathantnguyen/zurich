
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DwellingDesignStyle.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DwellingDesignStyle">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Cape Cod"/>
 *     &lt;enumeration value="Colonial"/>
 *     &lt;enumeration value="Duplex"/>
 *     &lt;enumeration value="High Rise Multiple Units"/>
 *     &lt;enumeration value="Low Rise Multiple Units"/>
 *     &lt;enumeration value="Single Family Detached Four Square"/>
 *     &lt;enumeration value="Single Family Detached Ranch"/>
 *     &lt;enumeration value="Single Family Row Home"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DwellingDesignStyle", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum DwellingDesignStyle {

    @XmlEnumValue("Cape Cod")
    CAPE_COD("Cape Cod"),
    @XmlEnumValue("Colonial")
    COLONIAL("Colonial"),
    @XmlEnumValue("Duplex")
    DUPLEX("Duplex"),
    @XmlEnumValue("High Rise Multiple Units")
    HIGH_RISE_MULTIPLE_UNITS("High Rise Multiple Units"),
    @XmlEnumValue("Low Rise Multiple Units")
    LOW_RISE_MULTIPLE_UNITS("Low Rise Multiple Units"),
    @XmlEnumValue("Single Family Detached Four Square")
    SINGLE_FAMILY_DETACHED_FOUR_SQUARE("Single Family Detached Four Square"),
    @XmlEnumValue("Single Family Detached Ranch")
    SINGLE_FAMILY_DETACHED_RANCH("Single Family Detached Ranch"),
    @XmlEnumValue("Single Family Row Home")
    SINGLE_FAMILY_ROW_HOME("Single Family Row Home");
    private final String value;

    DwellingDesignStyle(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DwellingDesignStyle fromValue(String v) {
        for (DwellingDesignStyle c: DwellingDesignStyle.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
