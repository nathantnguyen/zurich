
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrganisationStatus_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrganisationStatus_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Status_TO">
 *       &lt;sequence>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="reason" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}OrganisationStatusReason" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationStatus_TO", propOrder = {
    "state",
    "reason"
})
public class OrganisationStatusTO
    extends StatusTO
{

    protected String state;
    protected OrganisationStatusReason reason;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationStatusReason }
     *     
     */
    public OrganisationStatusReason getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationStatusReason }
     *     
     */
    public void setReason(OrganisationStatusReason value) {
        this.reason = value;
    }

}
