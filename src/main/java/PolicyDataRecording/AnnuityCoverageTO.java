
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AnnuityCoverage_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnnuityCoverage_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CoverageComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="jointContingency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}JointContingency" minOccurs="0"/>
 *         &lt;element name="annuityDurationType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}AnnuityDurationType" minOccurs="0"/>
 *         &lt;element name="isDeferred" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="taxWithheld" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="annuityReductionPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="nextIndexationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnnuityCoverage_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "jointContingency",
    "annuityDurationType",
    "isDeferred",
    "taxWithheld",
    "annuityReductionPercentage",
    "nextIndexationDate"
})
public class AnnuityCoverageTO
    extends CoverageComponentTO
{

    protected JointContingency jointContingency;
    protected AnnuityDurationType annuityDurationType;
    protected Boolean isDeferred;
    protected Boolean taxWithheld;
    protected BigDecimal annuityReductionPercentage;
    protected XMLGregorianCalendar nextIndexationDate;

    /**
     * Gets the value of the jointContingency property.
     * 
     * @return
     *     possible object is
     *     {@link JointContingency }
     *     
     */
    public JointContingency getJointContingency() {
        return jointContingency;
    }

    /**
     * Sets the value of the jointContingency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JointContingency }
     *     
     */
    public void setJointContingency(JointContingency value) {
        this.jointContingency = value;
    }

    /**
     * Gets the value of the annuityDurationType property.
     * 
     * @return
     *     possible object is
     *     {@link AnnuityDurationType }
     *     
     */
    public AnnuityDurationType getAnnuityDurationType() {
        return annuityDurationType;
    }

    /**
     * Sets the value of the annuityDurationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnuityDurationType }
     *     
     */
    public void setAnnuityDurationType(AnnuityDurationType value) {
        this.annuityDurationType = value;
    }

    /**
     * Gets the value of the isDeferred property.
     * This getter has been renamed from isIsDeferred() to getIsDeferred() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsDeferred() {
        return isDeferred;
    }

    /**
     * Sets the value of the isDeferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDeferred(Boolean value) {
        this.isDeferred = value;
    }

    /**
     * Gets the value of the taxWithheld property.
     * This getter has been renamed from isTaxWithheld() to getTaxWithheld() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getTaxWithheld() {
        return taxWithheld;
    }

    /**
     * Sets the value of the taxWithheld property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxWithheld(Boolean value) {
        this.taxWithheld = value;
    }

    /**
     * Gets the value of the annuityReductionPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAnnuityReductionPercentage() {
        return annuityReductionPercentage;
    }

    /**
     * Sets the value of the annuityReductionPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAnnuityReductionPercentage(BigDecimal value) {
        this.annuityReductionPercentage = value;
    }

    /**
     * Gets the value of the nextIndexationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextIndexationDate() {
        return nextIndexationDate;
    }

    /**
     * Sets the value of the nextIndexationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextIndexationDate(XMLGregorianCalendar value) {
        this.nextIndexationDate = value;
    }

}
