
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyRoleInLegalActionTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PartyRoleInLegalActionTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Plaintiff"/>
 *     &lt;enumeration value="Defendant"/>
 *     &lt;enumeration value="Arbitrator"/>
 *     &lt;enumeration value="Codefendant"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PartyRoleInLegalActionTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum PartyRoleInLegalActionTypeName {

    @XmlEnumValue("Plaintiff")
    PLAINTIFF("Plaintiff"),
    @XmlEnumValue("Defendant")
    DEFENDANT("Defendant"),
    @XmlEnumValue("Arbitrator")
    ARBITRATOR("Arbitrator"),
    @XmlEnumValue("Codefendant")
    CODEFENDANT("Codefendant");
    private final String value;

    PartyRoleInLegalActionTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartyRoleInLegalActionTypeName fromValue(String v) {
        for (PartyRoleInLegalActionTypeName c: PartyRoleInLegalActionTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
