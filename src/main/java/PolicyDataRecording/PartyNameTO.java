
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PartyName_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyName_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="language" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}Language" minOccurs="0"/>
 *         &lt;element name="rolePlayers" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="careOfAddresses" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CareOfAddress_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="partyRegistrations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="isDefault" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="partyNameRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyNameType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyName_TO", propOrder = {
    "startDate",
    "endDate",
    "description",
    "language",
    "rolePlayers",
    "careOfAddresses",
    "partyRegistrations",
    "isDefault",
    "partyNameRootType"
})
@XmlSeeAlso({
    PersonNameTO.class,
    UnstructuredNameTO.class
})
public class PartyNameTO
    extends DependentObjectTO
{

    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected String description;
    protected Language language;
    protected List<RolePlayerTO> rolePlayers;
    protected List<CareOfAddressTO> careOfAddresses;
    protected List<PartyRegistrationTO> partyRegistrations;
    protected Boolean isDefault;
    protected PartyNameTypeTO partyNameRootType;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link Language }
     *     
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link Language }
     *     
     */
    public void setLanguage(Language value) {
        this.language = value;
    }

    /**
     * Gets the value of the rolePlayers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolePlayers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolePlayers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerTO }
     * 
     * 
     */
    public List<RolePlayerTO> getRolePlayers() {
        if (rolePlayers == null) {
            rolePlayers = new ArrayList<RolePlayerTO>();
        }
        return this.rolePlayers;
    }

    /**
     * Gets the value of the careOfAddresses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the careOfAddresses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCareOfAddresses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CareOfAddressTO }
     * 
     * 
     */
    public List<CareOfAddressTO> getCareOfAddresses() {
        if (careOfAddresses == null) {
            careOfAddresses = new ArrayList<CareOfAddressTO>();
        }
        return this.careOfAddresses;
    }

    /**
     * Gets the value of the partyRegistrations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyRegistrations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyRegistrations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyRegistrationTO }
     * 
     * 
     */
    public List<PartyRegistrationTO> getPartyRegistrations() {
        if (partyRegistrations == null) {
            partyRegistrations = new ArrayList<PartyRegistrationTO>();
        }
        return this.partyRegistrations;
    }

    /**
     * Gets the value of the isDefault property.
     * This getter has been renamed from isIsDefault() to getIsDefault() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     * Sets the value of the isDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDefault(Boolean value) {
        this.isDefault = value;
    }

    /**
     * Gets the value of the partyNameRootType property.
     * 
     * @return
     *     possible object is
     *     {@link PartyNameTypeTO }
     *     
     */
    public PartyNameTypeTO getPartyNameRootType() {
        return partyNameRootType;
    }

    /**
     * Sets the value of the partyNameRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyNameTypeTO }
     *     
     */
    public void setPartyNameRootType(PartyNameTypeTO value) {
        this.partyNameRootType = value;
    }

    /**
     * Sets the value of the rolePlayers property.
     * 
     * @param rolePlayers
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setRolePlayers(List<RolePlayerTO> rolePlayers) {
        this.rolePlayers = rolePlayers;
    }

    /**
     * Sets the value of the careOfAddresses property.
     * 
     * @param careOfAddresses
     *     allowed object is
     *     {@link CareOfAddressTO }
     *     
     */
    public void setCareOfAddresses(List<CareOfAddressTO> careOfAddresses) {
        this.careOfAddresses = careOfAddresses;
    }

    /**
     * Sets the value of the partyRegistrations property.
     * 
     * @param partyRegistrations
     *     allowed object is
     *     {@link PartyRegistrationTO }
     *     
     */
    public void setPartyRegistrations(List<PartyRegistrationTO> partyRegistrations) {
        this.partyRegistrations = partyRegistrations;
    }

}
