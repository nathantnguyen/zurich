
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Household_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Household_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO">
 *       &lt;sequence>
 *         &lt;element name="family" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="disposableIncome" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="grossIncome" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="homeOwnership" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}HomeOwnership" minOccurs="0"/>
 *         &lt;element name="numberOfDependentChildren" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Byte" minOccurs="0"/>
 *         &lt;element name="numberOfDependentAdults" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Household_TO", propOrder = {
    "family",
    "disposableIncome",
    "grossIncome",
    "homeOwnership",
    "numberOfDependentChildren",
    "numberOfDependentAdults"
})
public class HouseholdTO
    extends OrganisationTO
{

    protected Boolean family;
    protected BaseCurrencyAmount disposableIncome;
    protected BaseCurrencyAmount grossIncome;
    protected HomeOwnership homeOwnership;
    protected Byte numberOfDependentChildren;
    protected BigDecimal numberOfDependentAdults;

    /**
     * Gets the value of the family property.
     * This getter has been renamed from isFamily() to getFamily() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFamily() {
        return family;
    }

    /**
     * Sets the value of the family property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFamily(Boolean value) {
        this.family = value;
    }

    /**
     * Gets the value of the disposableIncome property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getDisposableIncome() {
        return disposableIncome;
    }

    /**
     * Sets the value of the disposableIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setDisposableIncome(BaseCurrencyAmount value) {
        this.disposableIncome = value;
    }

    /**
     * Gets the value of the grossIncome property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getGrossIncome() {
        return grossIncome;
    }

    /**
     * Sets the value of the grossIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setGrossIncome(BaseCurrencyAmount value) {
        this.grossIncome = value;
    }

    /**
     * Gets the value of the homeOwnership property.
     * 
     * @return
     *     possible object is
     *     {@link HomeOwnership }
     *     
     */
    public HomeOwnership getHomeOwnership() {
        return homeOwnership;
    }

    /**
     * Sets the value of the homeOwnership property.
     * 
     * @param value
     *     allowed object is
     *     {@link HomeOwnership }
     *     
     */
    public void setHomeOwnership(HomeOwnership value) {
        this.homeOwnership = value;
    }

    /**
     * Gets the value of the numberOfDependentChildren property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getNumberOfDependentChildren() {
        return numberOfDependentChildren;
    }

    /**
     * Sets the value of the numberOfDependentChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setNumberOfDependentChildren(Byte value) {
        this.numberOfDependentChildren = value;
    }

    /**
     * Gets the value of the numberOfDependentAdults property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumberOfDependentAdults() {
        return numberOfDependentAdults;
    }

    /**
     * Sets the value of the numberOfDependentAdults property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumberOfDependentAdults(BigDecimal value) {
        this.numberOfDependentAdults = value;
    }

}
