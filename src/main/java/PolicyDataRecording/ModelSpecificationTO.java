
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ModelSpecification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModelSpecification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="lastManufacturedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="listPriceNew" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="make" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="model" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="modelYear" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="estimatedMarketValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="firstManufacturedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="modelIdentificationNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="featuringModelSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}ModelSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="series" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="modelSpecificationRootType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="trimLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="modelYearAsNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModelSpecification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "description",
    "lastManufacturedDate",
    "listPriceNew",
    "make",
    "model",
    "modelYear",
    "estimatedMarketValue",
    "firstManufacturedDate",
    "modelIdentificationNumber",
    "featuringModelSpecifications",
    "series",
    "modelSpecificationRootType",
    "trimLevel",
    "modelYearAsNumber",
    "externalReference",
    "alternateReference"
})
@XmlSeeAlso({
    ComputerMonitorModelTO.class,
    DrugSpecificationTO.class,
    VehicleModelTO.class,
    ComputerModelTO.class
})
public class ModelSpecificationTO
    extends BusinessModelObjectTO
{

    protected String description;
    protected XMLGregorianCalendar lastManufacturedDate;
    protected BaseCurrencyAmount listPriceNew;
    protected String make;
    protected String model;
    protected XMLGregorianCalendar modelYear;
    protected BaseCurrencyAmount estimatedMarketValue;
    protected XMLGregorianCalendar firstManufacturedDate;
    protected String modelIdentificationNumber;
    protected List<ModelSpecificationTO> featuringModelSpecifications;
    protected String series;
    protected String modelSpecificationRootType;
    protected String trimLevel;
    protected BigInteger modelYearAsNumber;
    protected String externalReference;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the lastManufacturedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastManufacturedDate() {
        return lastManufacturedDate;
    }

    /**
     * Sets the value of the lastManufacturedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastManufacturedDate(XMLGregorianCalendar value) {
        this.lastManufacturedDate = value;
    }

    /**
     * Gets the value of the listPriceNew property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getListPriceNew() {
        return listPriceNew;
    }

    /**
     * Sets the value of the listPriceNew property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setListPriceNew(BaseCurrencyAmount value) {
        this.listPriceNew = value;
    }

    /**
     * Gets the value of the make property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMake() {
        return make;
    }

    /**
     * Sets the value of the make property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMake(String value) {
        this.make = value;
    }

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModel(String value) {
        this.model = value;
    }

    /**
     * Gets the value of the modelYear property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModelYear() {
        return modelYear;
    }

    /**
     * Sets the value of the modelYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModelYear(XMLGregorianCalendar value) {
        this.modelYear = value;
    }

    /**
     * Gets the value of the estimatedMarketValue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getEstimatedMarketValue() {
        return estimatedMarketValue;
    }

    /**
     * Sets the value of the estimatedMarketValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setEstimatedMarketValue(BaseCurrencyAmount value) {
        this.estimatedMarketValue = value;
    }

    /**
     * Gets the value of the firstManufacturedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstManufacturedDate() {
        return firstManufacturedDate;
    }

    /**
     * Sets the value of the firstManufacturedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstManufacturedDate(XMLGregorianCalendar value) {
        this.firstManufacturedDate = value;
    }

    /**
     * Gets the value of the modelIdentificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelIdentificationNumber() {
        return modelIdentificationNumber;
    }

    /**
     * Sets the value of the modelIdentificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelIdentificationNumber(String value) {
        this.modelIdentificationNumber = value;
    }

    /**
     * Gets the value of the featuringModelSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the featuringModelSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeaturingModelSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ModelSpecificationTO }
     * 
     * 
     */
    public List<ModelSpecificationTO> getFeaturingModelSpecifications() {
        if (featuringModelSpecifications == null) {
            featuringModelSpecifications = new ArrayList<ModelSpecificationTO>();
        }
        return this.featuringModelSpecifications;
    }

    /**
     * Gets the value of the series property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeries() {
        return series;
    }

    /**
     * Sets the value of the series property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeries(String value) {
        this.series = value;
    }

    /**
     * Gets the value of the modelSpecificationRootType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelSpecificationRootType() {
        return modelSpecificationRootType;
    }

    /**
     * Sets the value of the modelSpecificationRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelSpecificationRootType(String value) {
        this.modelSpecificationRootType = value;
    }

    /**
     * Gets the value of the trimLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrimLevel() {
        return trimLevel;
    }

    /**
     * Sets the value of the trimLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrimLevel(String value) {
        this.trimLevel = value;
    }

    /**
     * Gets the value of the modelYearAsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getModelYearAsNumber() {
        return modelYearAsNumber;
    }

    /**
     * Sets the value of the modelYearAsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setModelYearAsNumber(BigInteger value) {
        this.modelYearAsNumber = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the featuringModelSpecifications property.
     * 
     * @param featuringModelSpecifications
     *     allowed object is
     *     {@link ModelSpecificationTO }
     *     
     */
    public void setFeaturingModelSpecifications(List<ModelSpecificationTO> featuringModelSpecifications) {
        this.featuringModelSpecifications = featuringModelSpecifications;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
