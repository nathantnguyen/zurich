
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScenarioValuationBasis.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ScenarioValuationBasis">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Central Fsa Basis"/>
 *     &lt;enumeration value="High Mortality Sensitivity"/>
 *     &lt;enumeration value="High Lapse Rate Sensitivity"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ScenarioValuationBasis", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum ScenarioValuationBasis {

    @XmlEnumValue("Central Fsa Basis")
    CENTRAL_FSA_BASIS("Central Fsa Basis"),
    @XmlEnumValue("High Mortality Sensitivity")
    HIGH_MORTALITY_SENSITIVITY("High Mortality Sensitivity"),
    @XmlEnumValue("High Lapse Rate Sensitivity")
    HIGH_LAPSE_RATE_SENSITIVITY("High Lapse Rate Sensitivity");
    private final String value;

    ScenarioValuationBasis(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ScenarioValuationBasis fromValue(String v) {
        for (ScenarioValuationBasis c: ScenarioValuationBasis.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
