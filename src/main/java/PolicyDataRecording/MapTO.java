
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Map_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Map_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PlaceCategory_TO">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Map_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/")
@XmlSeeAlso({
    ElectronicNetworkMapTO.class,
    CountryMapTO.class,
    RiskPlaceMapTO.class,
    PostalOrganisationMapTO.class
})
public class MapTO
    extends PlaceCategoryTO
{


}
