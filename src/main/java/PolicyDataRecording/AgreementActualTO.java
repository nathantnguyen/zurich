
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AgreementActual_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgreementActual_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Actual_TO">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="raisedRequests" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Request_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="availableRequests" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AvailableRequest_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgreementActual_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "startDate",
    "endDate",
    "raisedRequests",
    "availableRequests"
})
@XmlSeeAlso({
    AgreementPartTO.class,
    AgreementTO.class
})
public class AgreementActualTO
    extends ActualTO
{

    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected List<RequestTO> raisedRequests;
    protected List<AvailableRequestTO> availableRequests;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the raisedRequests property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the raisedRequests property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRaisedRequests().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestTO }
     * 
     * 
     */
    public List<RequestTO> getRaisedRequests() {
        if (raisedRequests == null) {
            raisedRequests = new ArrayList<RequestTO>();
        }
        return this.raisedRequests;
    }

    /**
     * Gets the value of the availableRequests property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the availableRequests property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAvailableRequests().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AvailableRequestTO }
     * 
     * 
     */
    public List<AvailableRequestTO> getAvailableRequests() {
        if (availableRequests == null) {
            availableRequests = new ArrayList<AvailableRequestTO>();
        }
        return this.availableRequests;
    }

    /**
     * Sets the value of the raisedRequests property.
     * 
     * @param raisedRequests
     *     allowed object is
     *     {@link RequestTO }
     *     
     */
    public void setRaisedRequests(List<RequestTO> raisedRequests) {
        this.raisedRequests = raisedRequests;
    }

    /**
     * Sets the value of the availableRequests property.
     * 
     * @param availableRequests
     *     allowed object is
     *     {@link AvailableRequestTO }
     *     
     */
    public void setAvailableRequests(List<AvailableRequestTO> availableRequests) {
        this.availableRequests = availableRequests;
    }

}
