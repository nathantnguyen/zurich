
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RiskAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Contingency Plan"/>
 *     &lt;enumeration value="Diversification Policy"/>
 *     &lt;enumeration value="Insurance Portfolio Diversification Policy"/>
 *     &lt;enumeration value="Investment Diversification Policy"/>
 *     &lt;enumeration value="Reinsurance Management Policy"/>
 *     &lt;enumeration value="Risk Assessment Calibration Set"/>
 *     &lt;enumeration value="Risk Assessment Scenario"/>
 *     &lt;enumeration value="Risk Event Assumption"/>
 *     &lt;enumeration value="Risk Mitigation Agreement"/>
 *     &lt;enumeration value="Stress Test Scenario"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RiskAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum RiskAgreementTypeName {

    @XmlEnumValue("Contingency Plan")
    CONTINGENCY_PLAN("Contingency Plan"),
    @XmlEnumValue("Diversification Policy")
    DIVERSIFICATION_POLICY("Diversification Policy"),
    @XmlEnumValue("Insurance Portfolio Diversification Policy")
    INSURANCE_PORTFOLIO_DIVERSIFICATION_POLICY("Insurance Portfolio Diversification Policy"),
    @XmlEnumValue("Investment Diversification Policy")
    INVESTMENT_DIVERSIFICATION_POLICY("Investment Diversification Policy"),
    @XmlEnumValue("Reinsurance Management Policy")
    REINSURANCE_MANAGEMENT_POLICY("Reinsurance Management Policy"),
    @XmlEnumValue("Risk Assessment Calibration Set")
    RISK_ASSESSMENT_CALIBRATION_SET("Risk Assessment Calibration Set"),
    @XmlEnumValue("Risk Assessment Scenario")
    RISK_ASSESSMENT_SCENARIO("Risk Assessment Scenario"),
    @XmlEnumValue("Risk Event Assumption")
    RISK_EVENT_ASSUMPTION("Risk Event Assumption"),
    @XmlEnumValue("Risk Mitigation Agreement")
    RISK_MITIGATION_AGREEMENT("Risk Mitigation Agreement"),
    @XmlEnumValue("Stress Test Scenario")
    STRESS_TEST_SCENARIO("Stress Test Scenario");
    private final String value;

    RiskAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RiskAgreementTypeName fromValue(String v) {
        for (RiskAgreementTypeName c: RiskAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
