
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NationalRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NationalRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="countryCode" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}CountryCode" minOccurs="0"/>
 *         &lt;element name="nationalRegistrationRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}NationalRegistrationType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NationalRegistration_TO", propOrder = {
    "countryCode",
    "nationalRegistrationRootType"
})
@XmlSeeAlso({
    CitizenshipTO.class
})
public class NationalRegistrationTO
    extends PartyRegistrationTO
{

    protected CountryCode countryCode;
    protected NationalRegistrationTypeTO nationalRegistrationRootType;

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link CountryCode }
     *     
     */
    public CountryCode getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCode }
     *     
     */
    public void setCountryCode(CountryCode value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the nationalRegistrationRootType property.
     * 
     * @return
     *     possible object is
     *     {@link NationalRegistrationTypeTO }
     *     
     */
    public NationalRegistrationTypeTO getNationalRegistrationRootType() {
        return nationalRegistrationRootType;
    }

    /**
     * Sets the value of the nationalRegistrationRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link NationalRegistrationTypeTO }
     *     
     */
    public void setNationalRegistrationRootType(NationalRegistrationTypeTO value) {
        this.nationalRegistrationRootType = value;
    }

}
