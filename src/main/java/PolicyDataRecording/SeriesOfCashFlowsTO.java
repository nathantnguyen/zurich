
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SeriesOfCashFlows_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SeriesOfCashFlows_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElement_TO">
 *       &lt;sequence>
 *         &lt;element name="growthRate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="growthType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/}GrowthType" minOccurs="0"/>
 *         &lt;element name="growthAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="anniversaryDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="frequencyLoading" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="growthEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="duration" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="numberOfCashFlows" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="modalAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeriesOfCashFlows_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "growthRate",
    "growthType",
    "growthAmount",
    "anniversaryDate",
    "frequency",
    "frequencyLoading",
    "growthEndDate",
    "duration",
    "numberOfCashFlows",
    "modalAmount"
})
public class SeriesOfCashFlowsTO
    extends MoneyProvisionElementTO
{

    protected BigDecimal growthRate;
    protected GrowthType growthType;
    protected BaseCurrencyAmount growthAmount;
    protected XMLGregorianCalendar anniversaryDate;
    protected Frequency frequency;
    protected BigDecimal frequencyLoading;
    protected XMLGregorianCalendar growthEndDate;
    protected BigInteger duration;
    protected Duration numberOfCashFlows;
    protected BaseCurrencyAmount modalAmount;

    /**
     * Gets the value of the growthRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGrowthRate() {
        return growthRate;
    }

    /**
     * Sets the value of the growthRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGrowthRate(BigDecimal value) {
        this.growthRate = value;
    }

    /**
     * Gets the value of the growthType property.
     * 
     * @return
     *     possible object is
     *     {@link GrowthType }
     *     
     */
    public GrowthType getGrowthType() {
        return growthType;
    }

    /**
     * Sets the value of the growthType property.
     * 
     * @param value
     *     allowed object is
     *     {@link GrowthType }
     *     
     */
    public void setGrowthType(GrowthType value) {
        this.growthType = value;
    }

    /**
     * Gets the value of the growthAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getGrowthAmount() {
        return growthAmount;
    }

    /**
     * Sets the value of the growthAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setGrowthAmount(BaseCurrencyAmount value) {
        this.growthAmount = value;
    }

    /**
     * Gets the value of the anniversaryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAnniversaryDate() {
        return anniversaryDate;
    }

    /**
     * Sets the value of the anniversaryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAnniversaryDate(XMLGregorianCalendar value) {
        this.anniversaryDate = value;
    }

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the frequencyLoading property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFrequencyLoading() {
        return frequencyLoading;
    }

    /**
     * Sets the value of the frequencyLoading property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFrequencyLoading(BigDecimal value) {
        this.frequencyLoading = value;
    }

    /**
     * Gets the value of the growthEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGrowthEndDate() {
        return growthEndDate;
    }

    /**
     * Sets the value of the growthEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGrowthEndDate(XMLGregorianCalendar value) {
        this.growthEndDate = value;
    }

    /**
     * Gets the value of the duration property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDuration(BigInteger value) {
        this.duration = value;
    }

    /**
     * Gets the value of the numberOfCashFlows property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getNumberOfCashFlows() {
        return numberOfCashFlows;
    }

    /**
     * Sets the value of the numberOfCashFlows property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setNumberOfCashFlows(Duration value) {
        this.numberOfCashFlows = value;
    }

    /**
     * Gets the value of the modalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getModalAmount() {
        return modalAmount;
    }

    /**
     * Sets the value of the modalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setModalAmount(BaseCurrencyAmount value) {
        this.modalAmount = value;
    }

}
