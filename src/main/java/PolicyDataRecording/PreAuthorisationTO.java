
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PreAuthorisation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PreAuthorisation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Authorisation_TO">
 *       &lt;sequence>
 *         &lt;element name="appealingPreAuthorisation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PreAuthorisation_TO" minOccurs="0"/>
 *         &lt;element name="appealedPreAuthorisation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PreAuthorisation_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreAuthorisation_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "appealingPreAuthorisation",
    "appealedPreAuthorisation"
})
public class PreAuthorisationTO
    extends AuthorisationTO
{

    protected PreAuthorisationTO appealingPreAuthorisation;
    protected PreAuthorisationTO appealedPreAuthorisation;

    /**
     * Gets the value of the appealingPreAuthorisation property.
     * 
     * @return
     *     possible object is
     *     {@link PreAuthorisationTO }
     *     
     */
    public PreAuthorisationTO getAppealingPreAuthorisation() {
        return appealingPreAuthorisation;
    }

    /**
     * Sets the value of the appealingPreAuthorisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreAuthorisationTO }
     *     
     */
    public void setAppealingPreAuthorisation(PreAuthorisationTO value) {
        this.appealingPreAuthorisation = value;
    }

    /**
     * Gets the value of the appealedPreAuthorisation property.
     * 
     * @return
     *     possible object is
     *     {@link PreAuthorisationTO }
     *     
     */
    public PreAuthorisationTO getAppealedPreAuthorisation() {
        return appealedPreAuthorisation;
    }

    /**
     * Sets the value of the appealedPreAuthorisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreAuthorisationTO }
     *     
     */
    public void setAppealedPreAuthorisation(PreAuthorisationTO value) {
        this.appealedPreAuthorisation = value;
    }

}
