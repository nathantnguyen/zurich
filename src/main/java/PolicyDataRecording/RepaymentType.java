
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RepaymentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RepaymentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Amortisable"/>
 *     &lt;enumeration value="Fixed Instalment"/>
 *     &lt;enumeration value="Fixed Redemption"/>
 *     &lt;enumeration value="With Interest"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RepaymentType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum RepaymentType {

    @XmlEnumValue("Amortisable")
    AMORTISABLE("Amortisable"),
    @XmlEnumValue("Fixed Instalment")
    FIXED_INSTALMENT("Fixed Instalment"),
    @XmlEnumValue("Fixed Redemption")
    FIXED_REDEMPTION("Fixed Redemption"),
    @XmlEnumValue("With Interest")
    WITH_INTEREST("With Interest");
    private final String value;

    RepaymentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RepaymentType fromValue(String v) {
        for (RepaymentType c: RepaymentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
