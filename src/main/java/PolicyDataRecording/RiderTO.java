
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Rider_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Rider_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CoverageComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="hasWaiver" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="conversionPrivilege" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="policyholderExpiryAge" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rider_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "hasWaiver",
    "conversionPrivilege",
    "policyholderExpiryAge"
})
public class RiderTO
    extends CoverageComponentTO
{

    protected Boolean hasWaiver;
    protected Boolean conversionPrivilege;
    protected String policyholderExpiryAge;

    /**
     * Gets the value of the hasWaiver property.
     * This getter has been renamed from isHasWaiver() to getHasWaiver() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasWaiver() {
        return hasWaiver;
    }

    /**
     * Sets the value of the hasWaiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasWaiver(Boolean value) {
        this.hasWaiver = value;
    }

    /**
     * Gets the value of the conversionPrivilege property.
     * This getter has been renamed from isConversionPrivilege() to getConversionPrivilege() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getConversionPrivilege() {
        return conversionPrivilege;
    }

    /**
     * Sets the value of the conversionPrivilege property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConversionPrivilege(Boolean value) {
        this.conversionPrivilege = value;
    }

    /**
     * Gets the value of the policyholderExpiryAge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyholderExpiryAge() {
        return policyholderExpiryAge;
    }

    /**
     * Sets the value of the policyholderExpiryAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyholderExpiryAge(String value) {
        this.policyholderExpiryAge = value;
    }

}
