
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardTextSpecificationRoleInContractSpecificationType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandardTextSpecificationRoleInContractSpecificationType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleType_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}StandardTextSpecificationRoleInContractSpecificationTypeName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardTextSpecificationRoleInContractSpecificationType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "name"
})
public class StandardTextSpecificationRoleInContractSpecificationTypeTO
    extends RoleTypeTO
{

    protected StandardTextSpecificationRoleInContractSpecificationTypeName name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link StandardTextSpecificationRoleInContractSpecificationTypeName }
     *     
     */
    public StandardTextSpecificationRoleInContractSpecificationTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardTextSpecificationRoleInContractSpecificationTypeName }
     *     
     */
    public void setName(StandardTextSpecificationRoleInContractSpecificationTypeName value) {
        this.name = value;
    }

}
