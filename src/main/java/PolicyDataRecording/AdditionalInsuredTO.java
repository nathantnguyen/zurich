
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdditionalInsured_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdditionalInsured_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}Insured_TO">
 *       &lt;sequence>
 *         &lt;element name="insuredDesignation" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}InsuredDesignation" minOccurs="0"/>
 *         &lt;element name="relationshipType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}MainInsuredRelation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalInsured_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "insuredDesignation",
    "relationshipType"
})
public class AdditionalInsuredTO
    extends InsuredTO
{

    protected InsuredDesignation insuredDesignation;
    protected MainInsuredRelation relationshipType;

    /**
     * Gets the value of the insuredDesignation property.
     * 
     * @return
     *     possible object is
     *     {@link InsuredDesignation }
     *     
     */
    public InsuredDesignation getInsuredDesignation() {
        return insuredDesignation;
    }

    /**
     * Sets the value of the insuredDesignation property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuredDesignation }
     *     
     */
    public void setInsuredDesignation(InsuredDesignation value) {
        this.insuredDesignation = value;
    }

    /**
     * Gets the value of the relationshipType property.
     * 
     * @return
     *     possible object is
     *     {@link MainInsuredRelation }
     *     
     */
    public MainInsuredRelation getRelationshipType() {
        return relationshipType;
    }

    /**
     * Sets the value of the relationshipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MainInsuredRelation }
     *     
     */
    public void setRelationshipType(MainInsuredRelation value) {
        this.relationshipType = value;
    }

}
