
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommissionSchedule_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommissionSchedule_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}GenericMoneyProvision_TO">
 *       &lt;sequence>
 *         &lt;element name="pertinentProduct" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}FinancialServicesProduct_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="moneyProvisionElementPartType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElementPartType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommissionSchedule_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/", propOrder = {
    "pertinentProduct",
    "moneyProvisionElementPartType"
})
public class CommissionScheduleTO
    extends GenericMoneyProvisionTO
{

    protected List<FinancialServicesProductTO> pertinentProduct;
    protected MoneyProvisionElementPartTypeTO moneyProvisionElementPartType;

    /**
     * Gets the value of the pertinentProduct property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pertinentProduct property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPertinentProduct().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialServicesProductTO }
     * 
     * 
     */
    public List<FinancialServicesProductTO> getPertinentProduct() {
        if (pertinentProduct == null) {
            pertinentProduct = new ArrayList<FinancialServicesProductTO>();
        }
        return this.pertinentProduct;
    }

    /**
     * Gets the value of the moneyProvisionElementPartType property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionElementPartTypeTO }
     *     
     */
    public MoneyProvisionElementPartTypeTO getMoneyProvisionElementPartType() {
        return moneyProvisionElementPartType;
    }

    /**
     * Sets the value of the moneyProvisionElementPartType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionElementPartTypeTO }
     *     
     */
    public void setMoneyProvisionElementPartType(MoneyProvisionElementPartTypeTO value) {
        this.moneyProvisionElementPartType = value;
    }

    /**
     * Sets the value of the pertinentProduct property.
     * 
     * @param pertinentProduct
     *     allowed object is
     *     {@link FinancialServicesProductTO }
     *     
     */
    public void setPertinentProduct(List<FinancialServicesProductTO> pertinentProduct) {
        this.pertinentProduct = pertinentProduct;
    }

}
