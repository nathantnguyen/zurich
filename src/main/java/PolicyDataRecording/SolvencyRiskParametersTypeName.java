
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SolvencyRiskParametersTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SolvencyRiskParametersTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Catastrophe Risk Parameter"/>
 *     &lt;enumeration value="Counter Cyclical Premium Risk Parameter"/>
 *     &lt;enumeration value="Counterparty Default Risk Parameter"/>
 *     &lt;enumeration value="Currency Risk Parameter"/>
 *     &lt;enumeration value="Disability Morbidity Risk Parameter"/>
 *     &lt;enumeration value="Equity Risk Parameter"/>
 *     &lt;enumeration value="Expense Risk Parameter"/>
 *     &lt;enumeration value="Interest Rate Risk Parameter"/>
 *     &lt;enumeration value="Lapse Risk Parameter"/>
 *     &lt;enumeration value="Longevity Risk Parameter"/>
 *     &lt;enumeration value="Mortality Risk Parameter"/>
 *     &lt;enumeration value="Property Risk Parameter"/>
 *     &lt;enumeration value="Revision Risk Parameter"/>
 *     &lt;enumeration value="Spread Risk Parameter"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SolvencyRiskParametersTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/")
@XmlEnum
public enum SolvencyRiskParametersTypeName {

    @XmlEnumValue("Catastrophe Risk Parameter")
    CATASTROPHE_RISK_PARAMETER("Catastrophe Risk Parameter"),
    @XmlEnumValue("Counter Cyclical Premium Risk Parameter")
    COUNTER_CYCLICAL_PREMIUM_RISK_PARAMETER("Counter Cyclical Premium Risk Parameter"),
    @XmlEnumValue("Counterparty Default Risk Parameter")
    COUNTERPARTY_DEFAULT_RISK_PARAMETER("Counterparty Default Risk Parameter"),
    @XmlEnumValue("Currency Risk Parameter")
    CURRENCY_RISK_PARAMETER("Currency Risk Parameter"),
    @XmlEnumValue("Disability Morbidity Risk Parameter")
    DISABILITY_MORBIDITY_RISK_PARAMETER("Disability Morbidity Risk Parameter"),
    @XmlEnumValue("Equity Risk Parameter")
    EQUITY_RISK_PARAMETER("Equity Risk Parameter"),
    @XmlEnumValue("Expense Risk Parameter")
    EXPENSE_RISK_PARAMETER("Expense Risk Parameter"),
    @XmlEnumValue("Interest Rate Risk Parameter")
    INTEREST_RATE_RISK_PARAMETER("Interest Rate Risk Parameter"),
    @XmlEnumValue("Lapse Risk Parameter")
    LAPSE_RISK_PARAMETER("Lapse Risk Parameter"),
    @XmlEnumValue("Longevity Risk Parameter")
    LONGEVITY_RISK_PARAMETER("Longevity Risk Parameter"),
    @XmlEnumValue("Mortality Risk Parameter")
    MORTALITY_RISK_PARAMETER("Mortality Risk Parameter"),
    @XmlEnumValue("Property Risk Parameter")
    PROPERTY_RISK_PARAMETER("Property Risk Parameter"),
    @XmlEnumValue("Revision Risk Parameter")
    REVISION_RISK_PARAMETER("Revision Risk Parameter"),
    @XmlEnumValue("Spread Risk Parameter")
    SPREAD_RISK_PARAMETER("Spread Risk Parameter");
    private final String value;

    SolvencyRiskParametersTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SolvencyRiskParametersTypeName fromValue(String v) {
        for (SolvencyRiskParametersTypeName c: SolvencyRiskParametersTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
