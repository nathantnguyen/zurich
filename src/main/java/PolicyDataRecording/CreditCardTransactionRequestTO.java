
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditCardTransactionRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardTransactionRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}PaymentFacilityRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="referencedSalesSlip" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransactionRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "referencedSalesSlip"
})
public class CreditCardTransactionRequestTO
    extends PaymentFacilityRequestTO
{

    protected ObjectReferenceTO referencedSalesSlip;

    /**
     * Gets the value of the referencedSalesSlip property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getReferencedSalesSlip() {
        return referencedSalesSlip;
    }

    /**
     * Sets the value of the referencedSalesSlip property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setReferencedSalesSlip(ObjectReferenceTO value) {
        this.referencedSalesSlip = value;
    }

}
