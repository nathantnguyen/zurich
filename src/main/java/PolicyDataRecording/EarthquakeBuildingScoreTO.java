
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EarthquakeBuildingScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EarthquakeBuildingScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}StructureScore_TO">
 *       &lt;sequence>
 *         &lt;element name="earthquakeBuildingClass" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="earthquakeBuildingGroundCondition" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="earthquakeRetrofitCondition" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="retrofitForEarthquakeIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EarthquakeBuildingScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "earthquakeBuildingClass",
    "earthquakeBuildingGroundCondition",
    "earthquakeRetrofitCondition",
    "retrofitForEarthquakeIndicator"
})
public class EarthquakeBuildingScoreTO
    extends StructureScoreTO
{

    protected String earthquakeBuildingClass;
    protected String earthquakeBuildingGroundCondition;
    protected String earthquakeRetrofitCondition;
    protected Boolean retrofitForEarthquakeIndicator;

    /**
     * Gets the value of the earthquakeBuildingClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarthquakeBuildingClass() {
        return earthquakeBuildingClass;
    }

    /**
     * Sets the value of the earthquakeBuildingClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarthquakeBuildingClass(String value) {
        this.earthquakeBuildingClass = value;
    }

    /**
     * Gets the value of the earthquakeBuildingGroundCondition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarthquakeBuildingGroundCondition() {
        return earthquakeBuildingGroundCondition;
    }

    /**
     * Sets the value of the earthquakeBuildingGroundCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarthquakeBuildingGroundCondition(String value) {
        this.earthquakeBuildingGroundCondition = value;
    }

    /**
     * Gets the value of the earthquakeRetrofitCondition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarthquakeRetrofitCondition() {
        return earthquakeRetrofitCondition;
    }

    /**
     * Sets the value of the earthquakeRetrofitCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarthquakeRetrofitCondition(String value) {
        this.earthquakeRetrofitCondition = value;
    }

    /**
     * Gets the value of the retrofitForEarthquakeIndicator property.
     * This getter has been renamed from isRetrofitForEarthquakeIndicator() to getRetrofitForEarthquakeIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRetrofitForEarthquakeIndicator() {
        return retrofitForEarthquakeIndicator;
    }

    /**
     * Sets the value of the retrofitForEarthquakeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetrofitForEarthquakeIndicator(Boolean value) {
        this.retrofitForEarthquakeIndicator = value;
    }

}
