
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssessmentResultRegistrationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssessmentResultRegistrationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Medical Certificate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssessmentResultRegistrationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum AssessmentResultRegistrationTypeName {

    @XmlEnumValue("Medical Certificate")
    MEDICAL_CERTIFICATE("Medical Certificate");
    private final String value;

    AssessmentResultRegistrationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssessmentResultRegistrationTypeName fromValue(String v) {
        for (AssessmentResultRegistrationTypeName c: AssessmentResultRegistrationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
