
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HomeOwnership.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HomeOwnership">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Own"/>
 *     &lt;enumeration value="Rent"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HomeOwnership", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum HomeOwnership {

    @XmlEnumValue("Own")
    OWN("Own"),
    @XmlEnumValue("Rent")
    RENT("Rent");
    private final String value;

    HomeOwnership(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HomeOwnership fromValue(String v) {
        for (HomeOwnership c: HomeOwnership.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
