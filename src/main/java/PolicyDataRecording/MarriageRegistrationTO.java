
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarriageRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarriageRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="maritalRegime" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}MaritalRegime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarriageRegistration_TO", propOrder = {
    "maritalRegime"
})
public class MarriageRegistrationTO
    extends PartyRegistrationTO
{

    protected MaritalRegime maritalRegime;

    /**
     * Gets the value of the maritalRegime property.
     * 
     * @return
     *     possible object is
     *     {@link MaritalRegime }
     *     
     */
    public MaritalRegime getMaritalRegime() {
        return maritalRegime;
    }

    /**
     * Sets the value of the maritalRegime property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaritalRegime }
     *     
     */
    public void setMaritalRegime(MaritalRegime value) {
        this.maritalRegime = value;
    }

}
