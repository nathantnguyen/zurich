
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmploymentRoleTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmploymentRoleTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Employer"/>
 *     &lt;enumeration value="Unemployed Role"/>
 *     &lt;enumeration value="Retiree"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EmploymentRoleTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum EmploymentRoleTypeName {

    @XmlEnumValue("Employer")
    EMPLOYER("Employer"),
    @XmlEnumValue("Unemployed Role")
    UNEMPLOYED_ROLE("Unemployed Role"),
    @XmlEnumValue("Retiree")
    RETIREE("Retiree");
    private final String value;

    EmploymentRoleTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmploymentRoleTypeName fromValue(String v) {
        for (EmploymentRoleTypeName c: EmploymentRoleTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
