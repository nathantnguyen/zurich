
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShipScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="annualNauticalMileage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="dailyNauticalMileage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="recordedNauticalMileage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "annualNauticalMileage",
    "dailyNauticalMileage",
    "recordedNauticalMileage"
})
public class ShipScoreTO
    extends ScoreTO
{

    protected BigInteger annualNauticalMileage;
    protected BigInteger dailyNauticalMileage;
    protected BigInteger recordedNauticalMileage;

    /**
     * Gets the value of the annualNauticalMileage property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAnnualNauticalMileage() {
        return annualNauticalMileage;
    }

    /**
     * Sets the value of the annualNauticalMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAnnualNauticalMileage(BigInteger value) {
        this.annualNauticalMileage = value;
    }

    /**
     * Gets the value of the dailyNauticalMileage property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDailyNauticalMileage() {
        return dailyNauticalMileage;
    }

    /**
     * Sets the value of the dailyNauticalMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDailyNauticalMileage(BigInteger value) {
        this.dailyNauticalMileage = value;
    }

    /**
     * Gets the value of the recordedNauticalMileage property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRecordedNauticalMileage() {
        return recordedNauticalMileage;
    }

    /**
     * Sets the value of the recordedNauticalMileage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRecordedNauticalMileage(BigInteger value) {
        this.recordedNauticalMileage = value;
    }

}
