
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Task_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Task_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO">
 *       &lt;sequence>
 *         &lt;element name="plannedManpower" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="actualTimeSpent" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="outsourced" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="actualCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="context" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="legalAction" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/DisputeResolution/}LegalAction_TO" minOccurs="0"/>
 *         &lt;element name="rolesInTask" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}RoleInTask_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Task_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/", propOrder = {
    "plannedManpower",
    "actualTimeSpent",
    "outsourced",
    "actualCount",
    "context",
    "legalAction",
    "rolesInTask"
})
public class TaskTO
    extends ActivityOccurrenceTO
{

    protected Amount plannedManpower;
    protected Amount actualTimeSpent;
    protected Boolean outsourced;
    protected BigInteger actualCount;
    protected List<BusinessModelObjectTO> context;
    protected LegalActionTO legalAction;
    protected List<RoleInTaskTO> rolesInTask;

    /**
     * Gets the value of the plannedManpower property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getPlannedManpower() {
        return plannedManpower;
    }

    /**
     * Sets the value of the plannedManpower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setPlannedManpower(Amount value) {
        this.plannedManpower = value;
    }

    /**
     * Gets the value of the actualTimeSpent property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getActualTimeSpent() {
        return actualTimeSpent;
    }

    /**
     * Sets the value of the actualTimeSpent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setActualTimeSpent(Amount value) {
        this.actualTimeSpent = value;
    }

    /**
     * Gets the value of the outsourced property.
     * This getter has been renamed from isOutsourced() to getOutsourced() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOutsourced() {
        return outsourced;
    }

    /**
     * Sets the value of the outsourced property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOutsourced(Boolean value) {
        this.outsourced = value;
    }

    /**
     * Gets the value of the actualCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getActualCount() {
        return actualCount;
    }

    /**
     * Sets the value of the actualCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setActualCount(BigInteger value) {
        this.actualCount = value;
    }

    /**
     * Gets the value of the context property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the context property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContext().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessModelObjectTO }
     * 
     * 
     */
    public List<BusinessModelObjectTO> getContext() {
        if (context == null) {
            context = new ArrayList<BusinessModelObjectTO>();
        }
        return this.context;
    }

    /**
     * Gets the value of the legalAction property.
     * 
     * @return
     *     possible object is
     *     {@link LegalActionTO }
     *     
     */
    public LegalActionTO getLegalAction() {
        return legalAction;
    }

    /**
     * Sets the value of the legalAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link LegalActionTO }
     *     
     */
    public void setLegalAction(LegalActionTO value) {
        this.legalAction = value;
    }

    /**
     * Gets the value of the rolesInTask property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInTask property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInTask().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInTaskTO }
     * 
     * 
     */
    public List<RoleInTaskTO> getRolesInTask() {
        if (rolesInTask == null) {
            rolesInTask = new ArrayList<RoleInTaskTO>();
        }
        return this.rolesInTask;
    }

    /**
     * Sets the value of the context property.
     * 
     * @param context
     *     allowed object is
     *     {@link BusinessModelObjectTO }
     *     
     */
    public void setContext(List<BusinessModelObjectTO> context) {
        this.context = context;
    }

    /**
     * Sets the value of the rolesInTask property.
     * 
     * @param rolesInTask
     *     allowed object is
     *     {@link RoleInTaskTO }
     *     
     */
    public void setRolesInTask(List<RoleInTaskTO> rolesInTask) {
        this.rolesInTask = rolesInTask;
    }

}
