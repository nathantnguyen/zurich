
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SuspectDuplicateSources.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SuspectDuplicateSources">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="System Marked"/>
 *     &lt;enumeration value="User Marked"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SuspectDuplicateSources", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum SuspectDuplicateSources {

    @XmlEnumValue("System Marked")
    SYSTEM_MARKED("System Marked"),
    @XmlEnumValue("User Marked")
    USER_MARKED("User Marked");
    private final String value;

    SuspectDuplicateSources(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SuspectDuplicateSources fromValue(String v) {
        for (SuspectDuplicateSources c: SuspectDuplicateSources.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
