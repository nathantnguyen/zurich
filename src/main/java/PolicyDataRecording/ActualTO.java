
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Actual_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Actual_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}StructuredActual_TO">
 *       &lt;sequence>
 *         &lt;element name="canBeComponent" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="compositeActual" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ComponentList_TO" minOccurs="0"/>
 *         &lt;element name="componentLists" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ComponentList_TO" minOccurs="0"/>
 *         &lt;element name="roleLists" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RoleList_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Actual_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "canBeComponent",
    "compositeActual",
    "componentLists",
    "roleLists"
})
@XmlSeeAlso({
    RequestTO.class,
    AgreementActualTO.class
})
public class ActualTO
    extends StructuredActualTO
{

    protected Boolean canBeComponent;
    protected ComponentListTO compositeActual;
    protected ComponentListTO componentLists;
    protected RoleListTO roleLists;

    /**
     * Gets the value of the canBeComponent property.
     * This getter has been renamed from isCanBeComponent() to getCanBeComponent() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCanBeComponent() {
        return canBeComponent;
    }

    /**
     * Sets the value of the canBeComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanBeComponent(Boolean value) {
        this.canBeComponent = value;
    }

    /**
     * Gets the value of the compositeActual property.
     * 
     * @return
     *     possible object is
     *     {@link ComponentListTO }
     *     
     */
    public ComponentListTO getCompositeActual() {
        return compositeActual;
    }

    /**
     * Sets the value of the compositeActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentListTO }
     *     
     */
    public void setCompositeActual(ComponentListTO value) {
        this.compositeActual = value;
    }

    /**
     * Gets the value of the componentLists property.
     * 
     * @return
     *     possible object is
     *     {@link ComponentListTO }
     *     
     */
    public ComponentListTO getComponentLists() {
        return componentLists;
    }

    /**
     * Sets the value of the componentLists property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentListTO }
     *     
     */
    public void setComponentLists(ComponentListTO value) {
        this.componentLists = value;
    }

    /**
     * Gets the value of the roleLists property.
     * 
     * @return
     *     possible object is
     *     {@link RoleListTO }
     *     
     */
    public RoleListTO getRoleLists() {
        return roleLists;
    }

    /**
     * Sets the value of the roleLists property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleListTO }
     *     
     */
    public void setRoleLists(RoleListTO value) {
        this.roleLists = value;
    }

}
