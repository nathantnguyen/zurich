
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for recordSubmissionDocumentDetailsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="recordSubmissionDocumentDetailsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestHeader" type="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}RequestHeader"/>
 *         &lt;element name="electronicDocument" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}ElectronicDocument_TO"/>
 *         &lt;element name="commercialAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CommercialAgreement_TO" minOccurs="0"/>
 *         &lt;element name="intakeCaseNumber" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}Task_TO" minOccurs="0"/>
 *         &lt;element name="currentCaseNumber" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}Task_TO" minOccurs="0"/>
 *         &lt;element name="repositoryCountryCode" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Country_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "recordSubmissionDocumentDetailsRequest", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/", propOrder = {
    "requestHeader",
    "electronicDocument",
    "commercialAgreement",
    "intakeCaseNumber",
    "currentCaseNumber",
    "repositoryCountryCode"
})
public class RecordSubmissionDocumentDetailsRequest {

    @XmlElement(required = true)
    protected RequestHeader requestHeader;
    @XmlElement(required = true)
    protected ElectronicDocumentTO electronicDocument;
    protected CommercialAgreementTO commercialAgreement;
    protected TaskTO intakeCaseNumber;
    protected TaskTO currentCaseNumber;
    protected CountryTO repositoryCountryCode;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the electronicDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ElectronicDocumentTO }
     *     
     */
    public ElectronicDocumentTO getElectronicDocument() {
        return electronicDocument;
    }

    /**
     * Sets the value of the electronicDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElectronicDocumentTO }
     *     
     */
    public void setElectronicDocument(ElectronicDocumentTO value) {
        this.electronicDocument = value;
    }

    /**
     * Gets the value of the commercialAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialAgreementTO }
     *     
     */
    public CommercialAgreementTO getCommercialAgreement() {
        return commercialAgreement;
    }

    /**
     * Sets the value of the commercialAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialAgreementTO }
     *     
     */
    public void setCommercialAgreement(CommercialAgreementTO value) {
        this.commercialAgreement = value;
    }

    /**
     * Gets the value of the intakeCaseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link TaskTO }
     *     
     */
    public TaskTO getIntakeCaseNumber() {
        return intakeCaseNumber;
    }

    /**
     * Sets the value of the intakeCaseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskTO }
     *     
     */
    public void setIntakeCaseNumber(TaskTO value) {
        this.intakeCaseNumber = value;
    }

    /**
     * Gets the value of the currentCaseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link TaskTO }
     *     
     */
    public TaskTO getCurrentCaseNumber() {
        return currentCaseNumber;
    }

    /**
     * Sets the value of the currentCaseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskTO }
     *     
     */
    public void setCurrentCaseNumber(TaskTO value) {
        this.currentCaseNumber = value;
    }

    /**
     * Gets the value of the repositoryCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link CountryTO }
     *     
     */
    public CountryTO getRepositoryCountryCode() {
        return repositoryCountryCode;
    }

    /**
     * Sets the value of the repositoryCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryTO }
     *     
     */
    public void setRepositoryCountryCode(CountryTO value) {
        this.repositoryCountryCode = value;
    }

}
