
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ManufacturedItem_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ManufacturedItem_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO">
 *       &lt;sequence>
 *         &lt;element name="serialNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="domesticManufacturer" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="domesticManufacturing" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="colour" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Colour" minOccurs="0"/>
 *         &lt;element name="size" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="modelSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}ModelSpecification_TO" minOccurs="0"/>
 *         &lt;element name="manufacturerName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="firstManufacturedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="featureNames" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="manufacturedItemRootType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="manufacturerCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManufacturedItem_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "serialNumber",
    "domesticManufacturer",
    "domesticManufacturing",
    "colour",
    "size",
    "modelSpecification",
    "manufacturerName",
    "firstManufacturedDate",
    "featureNames",
    "manufacturedItemRootType",
    "manufacturerCode"
})
@XmlSeeAlso({
    DrugTO.class,
    VehicleTO.class,
    ManufacturedItemPartTO.class
})
public class ManufacturedItemTO
    extends PhysicalObjectTO
{

    protected String serialNumber;
    protected Boolean domesticManufacturer;
    protected Boolean domesticManufacturing;
    protected Colour colour;
    protected Amount size;
    protected ModelSpecificationTO modelSpecification;
    protected String manufacturerName;
    protected XMLGregorianCalendar firstManufacturedDate;
    protected List<String> featureNames;
    protected String manufacturedItemRootType;
    protected String manufacturerCode;

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the domesticManufacturer property.
     * This getter has been renamed from isDomesticManufacturer() to getDomesticManufacturer() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDomesticManufacturer() {
        return domesticManufacturer;
    }

    /**
     * Sets the value of the domesticManufacturer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDomesticManufacturer(Boolean value) {
        this.domesticManufacturer = value;
    }

    /**
     * Gets the value of the domesticManufacturing property.
     * This getter has been renamed from isDomesticManufacturing() to getDomesticManufacturing() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDomesticManufacturing() {
        return domesticManufacturing;
    }

    /**
     * Sets the value of the domesticManufacturing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDomesticManufacturing(Boolean value) {
        this.domesticManufacturing = value;
    }

    /**
     * Gets the value of the colour property.
     * 
     * @return
     *     possible object is
     *     {@link Colour }
     *     
     */
    public Colour getColour() {
        return colour;
    }

    /**
     * Sets the value of the colour property.
     * 
     * @param value
     *     allowed object is
     *     {@link Colour }
     *     
     */
    public void setColour(Colour value) {
        this.colour = value;
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setSize(Amount value) {
        this.size = value;
    }

    /**
     * Gets the value of the modelSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link ModelSpecificationTO }
     *     
     */
    public ModelSpecificationTO getModelSpecification() {
        return modelSpecification;
    }

    /**
     * Sets the value of the modelSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelSpecificationTO }
     *     
     */
    public void setModelSpecification(ModelSpecificationTO value) {
        this.modelSpecification = value;
    }

    /**
     * Gets the value of the manufacturerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturerName() {
        return manufacturerName;
    }

    /**
     * Sets the value of the manufacturerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturerName(String value) {
        this.manufacturerName = value;
    }

    /**
     * Gets the value of the firstManufacturedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstManufacturedDate() {
        return firstManufacturedDate;
    }

    /**
     * Sets the value of the firstManufacturedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstManufacturedDate(XMLGregorianCalendar value) {
        this.firstManufacturedDate = value;
    }

    /**
     * Gets the value of the featureNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the featureNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatureNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFeatureNames() {
        if (featureNames == null) {
            featureNames = new ArrayList<String>();
        }
        return this.featureNames;
    }

    /**
     * Gets the value of the manufacturedItemRootType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturedItemRootType() {
        return manufacturedItemRootType;
    }

    /**
     * Sets the value of the manufacturedItemRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturedItemRootType(String value) {
        this.manufacturedItemRootType = value;
    }

    /**
     * Gets the value of the manufacturerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturerCode() {
        return manufacturerCode;
    }

    /**
     * Sets the value of the manufacturerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturerCode(String value) {
        this.manufacturerCode = value;
    }

    /**
     * Sets the value of the featureNames property.
     * 
     * @param featureNames
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureNames(List<String> featureNames) {
        this.featureNames = featureNames;
    }

}
