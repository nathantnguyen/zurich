
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PhysicalObjectOwnershipForm.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PhysicalObjectOwnershipForm">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Freehold"/>
 *     &lt;enumeration value="Rental"/>
 *     &lt;enumeration value="Lease"/>
 *     &lt;enumeration value="Full Individual Ownership"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PhysicalObjectOwnershipForm", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum PhysicalObjectOwnershipForm {

    @XmlEnumValue("Freehold")
    FREEHOLD("Freehold"),
    @XmlEnumValue("Rental")
    RENTAL("Rental"),
    @XmlEnumValue("Lease")
    LEASE("Lease"),
    @XmlEnumValue("Full Individual Ownership")
    FULL_INDIVIDUAL_OWNERSHIP("Full Individual Ownership");
    private final String value;

    PhysicalObjectOwnershipForm(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PhysicalObjectOwnershipForm fromValue(String v) {
        for (PhysicalObjectOwnershipForm c: PhysicalObjectOwnershipForm.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
