
package PolicyDataRecording;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the PolicyDataRecording package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpdateSubmissionAndPartszSOAFault_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "updateSubmissionAndPartszSOAFault");
    private final static QName _BindPolicy_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "bindPolicy");
    private final static QName _CreateRenewalSubmissionzSOAFault_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "createRenewalSubmissionzSOAFault");
    private final static QName _RecordSubmissionDocumentDetailsResponse_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "recordSubmissionDocumentDetailsResponse");
    private final static QName _CreateRenewalSubmissionResponse_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "createRenewalSubmissionResponse");
    private final static QName _ZSOAFault_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "zSOAFault");
    private final static QName _CreateRenewalSubmission_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "createRenewalSubmission");
    private final static QName _UpdateSubmissionAndPartsResponse_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "updateSubmissionAndPartsResponse");
    private final static QName _CreateSubmissionzSOAFault_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "createSubmissionzSOAFault");
    private final static QName _CreateSubmissionResponse_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "createSubmissionResponse");
    private final static QName _CreateSubmission_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "createSubmission");
    private final static QName _RecordSubmissionDocumentDetails_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "recordSubmissionDocumentDetails");
    private final static QName _UpdateSubmissionAndParts_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "updateSubmissionAndParts");
    private final static QName _BindPolicyResponse_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "bindPolicyResponse");
    private final static QName _BindPolicyzSOAFault_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", "bindPolicyzSOAFault");
    private final static QName _NewBusinessRequestTOCreatedFinancialServicesAgreement_QNAME = new QName("http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", "createdFinancialServicesAgreement");
    private final static QName _NewBusinessRequestTOTargetProduct_QNAME = new QName("http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", "targetProduct");
    private final static QName _NewBusinessRequestTORequestedExpirationDate_QNAME = new QName("http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", "requestedExpirationDate");
    private final static QName _ZSOAFaultTimeStamp_QNAME = new QName("http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/", "timeStamp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: PolicyDataRecording
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DynamicTextBlockTO }
     * 
     */
    public DynamicTextBlockTO createDynamicTextBlockTO() {
        return new DynamicTextBlockTO();
    }

    /**
     * Create an instance of {@link QuestionnaireTO }
     * 
     */
    public QuestionnaireTO createQuestionnaireTO() {
        return new QuestionnaireTO();
    }

    /**
     * Create an instance of {@link UnspecifiedContentTO }
     * 
     */
    public UnspecifiedContentTO createUnspecifiedContentTO() {
        return new UnspecifiedContentTO();
    }

    /**
     * Create an instance of {@link CommunicationContentRoleTO }
     * 
     */
    public CommunicationContentRoleTO createCommunicationContentRoleTO() {
        return new CommunicationContentRoleTO();
    }

    /**
     * Create an instance of {@link InformationScoreTO }
     * 
     */
    public InformationScoreTO createInformationScoreTO() {
        return new InformationScoreTO();
    }

    /**
     * Create an instance of {@link CommunicationCategoryTO }
     * 
     */
    public CommunicationCategoryTO createCommunicationCategoryTO() {
        return new CommunicationCategoryTO();
    }

    /**
     * Create an instance of {@link RoleInCommunicationContentTypeTO }
     * 
     */
    public RoleInCommunicationContentTypeTO createRoleInCommunicationContentTypeTO() {
        return new RoleInCommunicationContentTypeTO();
    }

    /**
     * Create an instance of {@link NoteTypeTO }
     * 
     */
    public NoteTypeTO createNoteTypeTO() {
        return new NoteTypeTO();
    }

    /**
     * Create an instance of {@link CommunicationTO }
     * 
     */
    public CommunicationTO createCommunicationTO() {
        return new CommunicationTO();
    }

    /**
     * Create an instance of {@link PredefinedAnswerTO }
     * 
     */
    public PredefinedAnswerTO createPredefinedAnswerTO() {
        return new PredefinedAnswerTO();
    }

    /**
     * Create an instance of {@link SelectedanswerTO }
     * 
     */
    public SelectedanswerTO createSelectedanswerTO() {
        return new SelectedanswerTO();
    }

    /**
     * Create an instance of {@link AskedQuestionTO }
     * 
     */
    public AskedQuestionTO createAskedQuestionTO() {
        return new AskedQuestionTO();
    }

    /**
     * Create an instance of {@link RoleInCommunicationContentTO }
     * 
     */
    public RoleInCommunicationContentTO createRoleInCommunicationContentTO() {
        return new RoleInCommunicationContentTO();
    }

    /**
     * Create an instance of {@link DocumentTemplateTO }
     * 
     */
    public DocumentTemplateTO createDocumentTemplateTO() {
        return new DocumentTemplateTO();
    }

    /**
     * Create an instance of {@link CompositeTextBlockTO }
     * 
     */
    public CompositeTextBlockTO createCompositeTextBlockTO() {
        return new CompositeTextBlockTO();
    }

    /**
     * Create an instance of {@link PhysicalDocumentTO }
     * 
     */
    public PhysicalDocumentTO createPhysicalDocumentTO() {
        return new PhysicalDocumentTO();
    }

    /**
     * Create an instance of {@link StaticTextBlockTO }
     * 
     */
    public StaticTextBlockTO createStaticTextBlockTO() {
        return new StaticTextBlockTO();
    }

    /**
     * Create an instance of {@link DocumentTO }
     * 
     */
    public DocumentTO createDocumentTO() {
        return new DocumentTO();
    }

    /**
     * Create an instance of {@link StandardTextSpecificationTO }
     * 
     */
    public StandardTextSpecificationTO createStandardTextSpecificationTO() {
        return new StandardTextSpecificationTO();
    }

    /**
     * Create an instance of {@link TextspecificationcompositionTO }
     * 
     */
    public TextspecificationcompositionTO createTextspecificationcompositionTO() {
        return new TextspecificationcompositionTO();
    }

    /**
     * Create an instance of {@link DocumentSearchIndexTO }
     * 
     */
    public DocumentSearchIndexTO createDocumentSearchIndexTO() {
        return new DocumentSearchIndexTO();
    }

    /**
     * Create an instance of {@link FreeTextAnswerTO }
     * 
     */
    public FreeTextAnswerTO createFreeTextAnswerTO() {
        return new FreeTextAnswerTO();
    }

    /**
     * Create an instance of {@link GeneratedDocumentTO }
     * 
     */
    public GeneratedDocumentTO createGeneratedDocumentTO() {
        return new GeneratedDocumentTO();
    }

    /**
     * Create an instance of {@link SpecifiedContentTO }
     * 
     */
    public SpecifiedContentTO createSpecifiedContentTO() {
        return new SpecifiedContentTO();
    }

    /**
     * Create an instance of {@link ElectronicDocumentTO }
     * 
     */
    public ElectronicDocumentTO createElectronicDocumentTO() {
        return new ElectronicDocumentTO();
    }

    /**
     * Create an instance of {@link CommunicationEffectivenessScoreTO }
     * 
     */
    public CommunicationEffectivenessScoreTO createCommunicationEffectivenessScoreTO() {
        return new CommunicationEffectivenessScoreTO();
    }

    /**
     * Create an instance of {@link CommunicationContentTO }
     * 
     */
    public CommunicationContentTO createCommunicationContentTO() {
        return new CommunicationContentTO();
    }

    /**
     * Create an instance of {@link DocumentTemplateTypeTO }
     * 
     */
    public DocumentTemplateTypeTO createDocumentTemplateTypeTO() {
        return new DocumentTemplateTypeTO();
    }

    /**
     * Create an instance of {@link QuestionTemplateTO }
     * 
     */
    public QuestionTemplateTO createQuestionTemplateTO() {
        return new QuestionTemplateTO();
    }

    /**
     * Create an instance of {@link CommunicationStatusTO }
     * 
     */
    public CommunicationStatusTO createCommunicationStatusTO() {
        return new CommunicationStatusTO();
    }

    /**
     * Create an instance of {@link IllegalStateException }
     * 
     */
    public IllegalStateException createIllegalStateException() {
        return new IllegalStateException();
    }

    /**
     * Create an instance of {@link ResponseHeader }
     * 
     */
    public ResponseHeader createResponseHeader() {
        return new ResponseHeader();
    }

    /**
     * Create an instance of {@link IndexOutOfBoundsException }
     * 
     */
    public IndexOutOfBoundsException createIndexOutOfBoundsException() {
        return new IndexOutOfBoundsException();
    }

    /**
     * Create an instance of {@link InvalidFieldValueException }
     * 
     */
    public InvalidFieldValueException createInvalidFieldValueException() {
        return new InvalidFieldValueException();
    }

    /**
     * Create an instance of {@link ZSOAFault }
     * 
     */
    public ZSOAFault createZSOAFault() {
        return new ZSOAFault();
    }

    /**
     * Create an instance of {@link InvariantViolationException }
     * 
     */
    public InvariantViolationException createInvariantViolationException() {
        return new InvariantViolationException();
    }

    /**
     * Create an instance of {@link ServiceException }
     * 
     */
    public ServiceException createServiceException() {
        return new ServiceException();
    }

    /**
     * Create an instance of {@link RequestHeader }
     * 
     */
    public RequestHeader createRequestHeader() {
        return new RequestHeader();
    }

    /**
     * Create an instance of {@link ResourceException }
     * 
     */
    public ResourceException createResourceException() {
        return new ResourceException();
    }

    /**
     * Create an instance of {@link UnimplementedException }
     * 
     */
    public UnimplementedException createUnimplementedException() {
        return new UnimplementedException();
    }

    /**
     * Create an instance of {@link DatabaseException }
     * 
     */
    public DatabaseException createDatabaseException() {
        return new DatabaseException();
    }

    /**
     * Create an instance of {@link IllegalArgumentException }
     * 
     */
    public IllegalArgumentException createIllegalArgumentException() {
        return new IllegalArgumentException();
    }

    /**
     * Create an instance of {@link UnsupportedOperationException }
     * 
     */
    public UnsupportedOperationException createUnsupportedOperationException() {
        return new UnsupportedOperationException();
    }

    /**
     * Create an instance of {@link ContractRequestStatusTO }
     * 
     */
    public ContractRequestStatusTO createContractRequestStatusTO() {
        return new ContractRequestStatusTO();
    }

    /**
     * Create an instance of {@link ContractRoleTO }
     * 
     */
    public ContractRoleTO createContractRoleTO() {
        return new ContractRoleTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionRoleInContractTypeTO }
     * 
     */
    public MoneyProvisionRoleInContractTypeTO createMoneyProvisionRoleInContractTypeTO() {
        return new MoneyProvisionRoleInContractTypeTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInFinancialServicesProductTypeTO }
     * 
     */
    public PartyInvolvedInFinancialServicesProductTypeTO createPartyInvolvedInFinancialServicesProductTypeTO() {
        return new PartyInvolvedInFinancialServicesProductTypeTO();
    }

    /**
     * Create an instance of {@link RoleInProductGroupTypeTO }
     * 
     */
    public RoleInProductGroupTypeTO createRoleInProductGroupTypeTO() {
        return new RoleInProductGroupTypeTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionInvolvedInContractTO }
     * 
     */
    public MoneyProvisionInvolvedInContractTO createMoneyProvisionInvolvedInContractTO() {
        return new MoneyProvisionInvolvedInContractTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionInvolvedInContractRequestTO }
     * 
     */
    public MoneyProvisionInvolvedInContractRequestTO createMoneyProvisionInvolvedInContractRequestTO() {
        return new MoneyProvisionInvolvedInContractRequestTO();
    }

    /**
     * Create an instance of {@link ProductGroupRoleTO }
     * 
     */
    public ProductGroupRoleTO createProductGroupRoleTO() {
        return new ProductGroupRoleTO();
    }

    /**
     * Create an instance of {@link MarketeerOfProductGroupTO }
     * 
     */
    public MarketeerOfProductGroupTO createMarketeerOfProductGroupTO() {
        return new MarketeerOfProductGroupTO();
    }

    /**
     * Create an instance of {@link ContractRequestSpecificationStatusTO }
     * 
     */
    public ContractRequestSpecificationStatusTO createContractRequestSpecificationStatusTO() {
        return new ContractRequestSpecificationStatusTO();
    }

    /**
     * Create an instance of {@link FinancialservicesproductcompositionTO }
     * 
     */
    public FinancialservicesproductcompositionTO createFinancialservicesproductcompositionTO() {
        return new FinancialservicesproductcompositionTO();
    }

    /**
     * Create an instance of {@link ContractRequestSpecificationAllowanceTO }
     * 
     */
    public ContractRequestSpecificationAllowanceTO createContractRequestSpecificationAllowanceTO() {
        return new ContractRequestSpecificationAllowanceTO();
    }

    /**
     * Create an instance of {@link RoleInContractTO }
     * 
     */
    public RoleInContractTO createRoleInContractTO() {
        return new RoleInContractTO();
    }

    /**
     * Create an instance of {@link ProductGroupTO }
     * 
     */
    public ProductGroupTO createProductGroupTO() {
        return new ProductGroupTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInContractRequestTO }
     * 
     */
    public PartyInvolvedInContractRequestTO createPartyInvolvedInContractRequestTO() {
        return new PartyInvolvedInContractRequestTO();
    }

    /**
     * Create an instance of {@link MarketableProductTypeTO }
     * 
     */
    public MarketableProductTypeTO createMarketableProductTypeTO() {
        return new MarketableProductTypeTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInContractRequestTypeTO }
     * 
     */
    public PartyInvolvedInContractRequestTypeTO createPartyInvolvedInContractRequestTypeTO() {
        return new PartyInvolvedInContractRequestTypeTO();
    }

    /**
     * Create an instance of {@link StandardTextSpecificationInvolvedInContractSpecificationTO }
     * 
     */
    public StandardTextSpecificationInvolvedInContractSpecificationTO createStandardTextSpecificationInvolvedInContractSpecificationTO() {
        return new StandardTextSpecificationInvolvedInContractSpecificationTO();
    }

    /**
     * Create an instance of {@link AssessmentResultInvolvedInContractRequestTypeTO }
     * 
     */
    public AssessmentResultInvolvedInContractRequestTypeTO createAssessmentResultInvolvedInContractRequestTypeTO() {
        return new AssessmentResultInvolvedInContractRequestTypeTO();
    }

    /**
     * Create an instance of {@link ContractRequestSpecificationInclusionTO }
     * 
     */
    public ContractRequestSpecificationInclusionTO createContractRequestSpecificationInclusionTO() {
        return new ContractRequestSpecificationInclusionTO();
    }

    /**
     * Create an instance of {@link ContractRelationshipTO }
     * 
     */
    public ContractRelationshipTO createContractRelationshipTO() {
        return new ContractRelationshipTO();
    }

    /**
     * Create an instance of {@link CustomerOfProductGroupTO }
     * 
     */
    public CustomerOfProductGroupTO createCustomerOfProductGroupTO() {
        return new CustomerOfProductGroupTO();
    }

    /**
     * Create an instance of {@link ContractSpecificationStatusTO }
     * 
     */
    public ContractSpecificationStatusTO createContractSpecificationStatusTO() {
        return new ContractSpecificationStatusTO();
    }

    /**
     * Create an instance of {@link ContractGroupTO }
     * 
     */
    public ContractGroupTO createContractGroupTO() {
        return new ContractGroupTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialServicesProductTypeTO }
     * 
     */
    public RoleInFinancialServicesProductTypeTO createRoleInFinancialServicesProductTypeTO() {
        return new RoleInFinancialServicesProductTypeTO();
    }

    /**
     * Create an instance of {@link CustomerOfProductGroupStatusTO }
     * 
     */
    public CustomerOfProductGroupStatusTO createCustomerOfProductGroupStatusTO() {
        return new CustomerOfProductGroupStatusTO();
    }

    /**
     * Create an instance of {@link ContractSpecificationRegistrationTO }
     * 
     */
    public ContractSpecificationRegistrationTO createContractSpecificationRegistrationTO() {
        return new ContractSpecificationRegistrationTO();
    }

    /**
     * Create an instance of {@link ContractSpecificationRelationshipTypeTO }
     * 
     */
    public ContractSpecificationRelationshipTypeTO createContractSpecificationRelationshipTypeTO() {
        return new ContractSpecificationRelationshipTypeTO();
    }

    /**
     * Create an instance of {@link ContractRequestSpecificationCompositionTO }
     * 
     */
    public ContractRequestSpecificationCompositionTO createContractRequestSpecificationCompositionTO() {
        return new ContractRequestSpecificationCompositionTO();
    }

    /**
     * Create an instance of {@link ContractRequestTO }
     * 
     */
    public ContractRequestTO createContractRequestTO() {
        return new ContractRequestTO();
    }

    /**
     * Create an instance of {@link ContractSpecificationTypeTO }
     * 
     */
    public ContractSpecificationTypeTO createContractSpecificationTypeTO() {
        return new ContractSpecificationTypeTO();
    }

    /**
     * Create an instance of {@link StandardTextSpecificationRoleInContractSpecificationTypeTO }
     * 
     */
    public StandardTextSpecificationRoleInContractSpecificationTypeTO createStandardTextSpecificationRoleInContractSpecificationTypeTO() {
        return new StandardTextSpecificationRoleInContractSpecificationTypeTO();
    }

    /**
     * Create an instance of {@link AssessmentResultInvolvedInContractRequestTO }
     * 
     */
    public AssessmentResultInvolvedInContractRequestTO createAssessmentResultInvolvedInContractRequestTO() {
        return new AssessmentResultInvolvedInContractRequestTO();
    }

    /**
     * Create an instance of {@link RoleInContractRequestTO }
     * 
     */
    public RoleInContractRequestTO createRoleInContractRequestTO() {
        return new RoleInContractRequestTO();
    }

    /**
     * Create an instance of {@link ContractSpecificationTO }
     * 
     */
    public ContractSpecificationTO createContractSpecificationTO() {
        return new ContractSpecificationTO();
    }

    /**
     * Create an instance of {@link RoleInContractSpecificationTO }
     * 
     */
    public RoleInContractSpecificationTO createRoleInContractSpecificationTO() {
        return new RoleInContractSpecificationTO();
    }

    /**
     * Create an instance of {@link RoleInProductGroupTO }
     * 
     */
    public RoleInProductGroupTO createRoleInProductGroupTO() {
        return new RoleInProductGroupTO();
    }

    /**
     * Create an instance of {@link FinancialServicesProductTO }
     * 
     */
    public FinancialServicesProductTO createFinancialServicesProductTO() {
        return new FinancialServicesProductTO();
    }

    /**
     * Create an instance of {@link ActivityProvidedByContractTO }
     * 
     */
    public ActivityProvidedByContractTO createActivityProvidedByContractTO() {
        return new ActivityProvidedByContractTO();
    }

    /**
     * Create an instance of {@link ContractRegistrationTO }
     * 
     */
    public ContractRegistrationTO createContractRegistrationTO() {
        return new ContractRegistrationTO();
    }

    /**
     * Create an instance of {@link RoleInContractRequestTypeTO }
     * 
     */
    public RoleInContractRequestTypeTO createRoleInContractRequestTypeTO() {
        return new RoleInContractRequestTypeTO();
    }

    /**
     * Create an instance of {@link FinancialServicesProductComponentTO }
     * 
     */
    public FinancialServicesProductComponentTO createFinancialServicesProductComponentTO() {
        return new FinancialServicesProductComponentTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInFinancialServicesProductTO }
     * 
     */
    public PartyInvolvedInFinancialServicesProductTO createPartyInvolvedInFinancialServicesProductTO() {
        return new PartyInvolvedInFinancialServicesProductTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialServicesProductTO }
     * 
     */
    public RoleInFinancialServicesProductTO createRoleInFinancialServicesProductTO() {
        return new RoleInFinancialServicesProductTO();
    }

    /**
     * Create an instance of {@link ContractRelationshipTypeTO }
     * 
     */
    public ContractRelationshipTypeTO createContractRelationshipTypeTO() {
        return new ContractRelationshipTypeTO();
    }

    /**
     * Create an instance of {@link ContractTO }
     * 
     */
    public ContractTO createContractTO() {
        return new ContractTO();
    }

    /**
     * Create an instance of {@link SupervisionTO }
     * 
     */
    public SupervisionTO createSupervisionTO() {
        return new SupervisionTO();
    }

    /**
     * Create an instance of {@link RoleInContractTypeTO }
     * 
     */
    public RoleInContractTypeTO createRoleInContractTypeTO() {
        return new RoleInContractTypeTO();
    }

    /**
     * Create an instance of {@link ContractSpecificationRelationshipTO }
     * 
     */
    public ContractSpecificationRelationshipTO createContractSpecificationRelationshipTO() {
        return new ContractSpecificationRelationshipTO();
    }

    /**
     * Create an instance of {@link ContractspecificationcompositionTO }
     * 
     */
    public ContractspecificationcompositionTO createContractspecificationcompositionTO() {
        return new ContractspecificationcompositionTO();
    }

    /**
     * Create an instance of {@link MarketableProductTO }
     * 
     */
    public MarketableProductTO createMarketableProductTO() {
        return new MarketableProductTO();
    }

    /**
     * Create an instance of {@link ContractRequestSpecificationTO }
     * 
     */
    public ContractRequestSpecificationTO createContractRequestSpecificationTO() {
        return new ContractRequestSpecificationTO();
    }

    /**
     * Create an instance of {@link RoleInCorporateAgreementRequestTO }
     * 
     */
    public RoleInCorporateAgreementRequestTO createRoleInCorporateAgreementRequestTO() {
        return new RoleInCorporateAgreementRequestTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInRiskAgreementTO }
     * 
     */
    public PartyInvolvedInRiskAgreementTO createPartyInvolvedInRiskAgreementTO() {
        return new PartyInvolvedInRiskAgreementTO();
    }

    /**
     * Create an instance of {@link RiskAgreementTypeTO }
     * 
     */
    public RiskAgreementTypeTO createRiskAgreementTypeTO() {
        return new RiskAgreementTypeTO();
    }

    /**
     * Create an instance of {@link TaxRegulatoryAgreementTO }
     * 
     */
    public TaxRegulatoryAgreementTO createTaxRegulatoryAgreementTO() {
        return new TaxRegulatoryAgreementTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInCorporateAgreementTypeTO }
     * 
     */
    public PartyInvolvedInCorporateAgreementTypeTO createPartyInvolvedInCorporateAgreementTypeTO() {
        return new PartyInvolvedInCorporateAgreementTypeTO();
    }

    /**
     * Create an instance of {@link CorporateAgreementTO }
     * 
     */
    public CorporateAgreementTO createCorporateAgreementTO() {
        return new CorporateAgreementTO();
    }

    /**
     * Create an instance of {@link RiskHazardTO }
     * 
     */
    public RiskHazardTO createRiskHazardTO() {
        return new RiskHazardTO();
    }

    /**
     * Create an instance of {@link RoleInRiskAgreementTO }
     * 
     */
    public RoleInRiskAgreementTO createRoleInRiskAgreementTO() {
        return new RoleInRiskAgreementTO();
    }

    /**
     * Create an instance of {@link RiskManagementAgreementTypeTO }
     * 
     */
    public RiskManagementAgreementTypeTO createRiskManagementAgreementTypeTO() {
        return new RiskManagementAgreementTypeTO();
    }

    /**
     * Create an instance of {@link CorporateAgreementTypeTO }
     * 
     */
    public CorporateAgreementTypeTO createCorporateAgreementTypeTO() {
        return new CorporateAgreementTypeTO();
    }

    /**
     * Create an instance of {@link InsuranceMarketRiskFactorScoreTO }
     * 
     */
    public InsuranceMarketRiskFactorScoreTO createInsuranceMarketRiskFactorScoreTO() {
        return new InsuranceMarketRiskFactorScoreTO();
    }

    /**
     * Create an instance of {@link RiskAssessmentTypeTO }
     * 
     */
    public RiskAssessmentTypeTO createRiskAssessmentTypeTO() {
        return new RiskAssessmentTypeTO();
    }

    /**
     * Create an instance of {@link CatastrophicRiskFactorScoreTO }
     * 
     */
    public CatastrophicRiskFactorScoreTO createCatastrophicRiskFactorScoreTO() {
        return new CatastrophicRiskFactorScoreTO();
    }

    /**
     * Create an instance of {@link ActuarialAnalysisTO }
     * 
     */
    public ActuarialAnalysisTO createActuarialAnalysisTO() {
        return new ActuarialAnalysisTO();
    }

    /**
     * Create an instance of {@link CurrencyRiskFactorScoreTO }
     * 
     */
    public CurrencyRiskFactorScoreTO createCurrencyRiskFactorScoreTO() {
        return new CurrencyRiskFactorScoreTO();
    }

    /**
     * Create an instance of {@link SanctionsTransactionsTO }
     * 
     */
    public SanctionsTransactionsTO createSanctionsTransactionsTO() {
        return new SanctionsTransactionsTO();
    }

    /**
     * Create an instance of {@link RiskAgreementSpecificationTO }
     * 
     */
    public RiskAgreementSpecificationTO createRiskAgreementSpecificationTO() {
        return new RiskAgreementSpecificationTO();
    }

    /**
     * Create an instance of {@link RiskTypeTO }
     * 
     */
    public RiskTypeTO createRiskTypeTO() {
        return new RiskTypeTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInRiskAgreementTypeTO }
     * 
     */
    public PartyInvolvedInRiskAgreementTypeTO createPartyInvolvedInRiskAgreementTypeTO() {
        return new PartyInvolvedInRiskAgreementTypeTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInCorporateAgreementTO }
     * 
     */
    public PartyInvolvedInCorporateAgreementTO createPartyInvolvedInCorporateAgreementTO() {
        return new PartyInvolvedInCorporateAgreementTO();
    }

    /**
     * Create an instance of {@link CorporateAgreementRequestTypeTO }
     * 
     */
    public CorporateAgreementRequestTypeTO createCorporateAgreementRequestTypeTO() {
        return new CorporateAgreementRequestTypeTO();
    }

    /**
     * Create an instance of {@link RiskAgreementTO }
     * 
     */
    public RiskAgreementTO createRiskAgreementTO() {
        return new RiskAgreementTO();
    }

    /**
     * Create an instance of {@link RiskExposureTypeTO }
     * 
     */
    public RiskExposureTypeTO createRiskExposureTypeTO() {
        return new RiskExposureTypeTO();
    }

    /**
     * Create an instance of {@link RoleInRiskAssessmentTO }
     * 
     */
    public RoleInRiskAssessmentTO createRoleInRiskAssessmentTO() {
        return new RoleInRiskAssessmentTO();
    }

    /**
     * Create an instance of {@link TaxRegulatorySpecificationTO }
     * 
     */
    public TaxRegulatorySpecificationTO createTaxRegulatorySpecificationTO() {
        return new TaxRegulatorySpecificationTO();
    }

    /**
     * Create an instance of {@link RoleInCorporateAgreementTypeTO }
     * 
     */
    public RoleInCorporateAgreementTypeTO createRoleInCorporateAgreementTypeTO() {
        return new RoleInCorporateAgreementTypeTO();
    }

    /**
     * Create an instance of {@link AccountingSpecificationTO }
     * 
     */
    public AccountingSpecificationTO createAccountingSpecificationTO() {
        return new AccountingSpecificationTO();
    }

    /**
     * Create an instance of {@link RiskAssessmentTO }
     * 
     */
    public RiskAssessmentTO createRiskAssessmentTO() {
        return new RiskAssessmentTO();
    }

    /**
     * Create an instance of {@link RiskAssessmentScenarioTO }
     * 
     */
    public RiskAssessmentScenarioTO createRiskAssessmentScenarioTO() {
        return new RiskAssessmentScenarioTO();
    }

    /**
     * Create an instance of {@link WorkersCompensationExperienceRiskFactorScoreTO }
     * 
     */
    public WorkersCompensationExperienceRiskFactorScoreTO createWorkersCompensationExperienceRiskFactorScoreTO() {
        return new WorkersCompensationExperienceRiskFactorScoreTO();
    }

    /**
     * Create an instance of {@link RoleInRiskExposureTO }
     * 
     */
    public RoleInRiskExposureTO createRoleInRiskExposureTO() {
        return new RoleInRiskExposureTO();
    }

    /**
     * Create an instance of {@link CorporateAgreementSpecificationTO }
     * 
     */
    public CorporateAgreementSpecificationTO createCorporateAgreementSpecificationTO() {
        return new CorporateAgreementSpecificationTO();
    }

    /**
     * Create an instance of {@link RoleInRiskAgreementTypeTO }
     * 
     */
    public RoleInRiskAgreementTypeTO createRoleInRiskAgreementTypeTO() {
        return new RoleInRiskAgreementTypeTO();
    }

    /**
     * Create an instance of {@link RoleInCorporateAgreementTO }
     * 
     */
    public RoleInCorporateAgreementTO createRoleInCorporateAgreementTO() {
        return new RoleInCorporateAgreementTO();
    }

    /**
     * Create an instance of {@link RiskFactorScoreTypeTO }
     * 
     */
    public RiskFactorScoreTypeTO createRiskFactorScoreTypeTO() {
        return new RiskFactorScoreTypeTO();
    }

    /**
     * Create an instance of {@link RiskAssessmentScenarioElementTO }
     * 
     */
    public RiskAssessmentScenarioElementTO createRiskAssessmentScenarioElementTO() {
        return new RiskAssessmentScenarioElementTO();
    }

    /**
     * Create an instance of {@link RiskExposureTO }
     * 
     */
    public RiskExposureTO createRiskExposureTO() {
        return new RiskExposureTO();
    }

    /**
     * Create an instance of {@link RoleInCorporateAgreementRequestTypeTO }
     * 
     */
    public RoleInCorporateAgreementRequestTypeTO createRoleInCorporateAgreementRequestTypeTO() {
        return new RoleInCorporateAgreementRequestTypeTO();
    }

    /**
     * Create an instance of {@link RiskFactorScoreAssessmentTypeTO }
     * 
     */
    public RiskFactorScoreAssessmentTypeTO createRiskFactorScoreAssessmentTypeTO() {
        return new RiskFactorScoreAssessmentTypeTO();
    }

    /**
     * Create an instance of {@link ProfitAndLossAttributionTO }
     * 
     */
    public ProfitAndLossAttributionTO createProfitAndLossAttributionTO() {
        return new ProfitAndLossAttributionTO();
    }

    /**
     * Create an instance of {@link CorporateAgreementRequestTO }
     * 
     */
    public CorporateAgreementRequestTO createCorporateAgreementRequestTO() {
        return new CorporateAgreementRequestTO();
    }

    /**
     * Create an instance of {@link RiskPositionAssessmentTypeTO }
     * 
     */
    public RiskPositionAssessmentTypeTO createRiskPositionAssessmentTypeTO() {
        return new RiskPositionAssessmentTypeTO();
    }

    /**
     * Create an instance of {@link TaxRegulatoryAgreementTypeTO }
     * 
     */
    public TaxRegulatoryAgreementTypeTO createTaxRegulatoryAgreementTypeTO() {
        return new TaxRegulatoryAgreementTypeTO();
    }

    /**
     * Create an instance of {@link RiskExposureStatusTO }
     * 
     */
    public RiskExposureStatusTO createRiskExposureStatusTO() {
        return new RiskExposureStatusTO();
    }

    /**
     * Create an instance of {@link RoleInRiskExposureTypeTO }
     * 
     */
    public RoleInRiskExposureTypeTO createRoleInRiskExposureTypeTO() {
        return new RoleInRiskExposureTypeTO();
    }

    /**
     * Create an instance of {@link AssessmentScenarioTO }
     * 
     */
    public AssessmentScenarioTO createAssessmentScenarioTO() {
        return new AssessmentScenarioTO();
    }

    /**
     * Create an instance of {@link RiskPositionTO }
     * 
     */
    public RiskPositionTO createRiskPositionTO() {
        return new RiskPositionTO();
    }

    /**
     * Create an instance of {@link RolePlayerInvolvedInRiskAssessmentTO }
     * 
     */
    public RolePlayerInvolvedInRiskAssessmentTO createRolePlayerInvolvedInRiskAssessmentTO() {
        return new RolePlayerInvolvedInRiskAssessmentTO();
    }

    /**
     * Create an instance of {@link RiskPositionTypeTO }
     * 
     */
    public RiskPositionTypeTO createRiskPositionTypeTO() {
        return new RiskPositionTypeTO();
    }

    /**
     * Create an instance of {@link RiskFactorScoreTO }
     * 
     */
    public RiskFactorScoreTO createRiskFactorScoreTO() {
        return new RiskFactorScoreTO();
    }

    /**
     * Create an instance of {@link RiskManagementAgreementTO }
     * 
     */
    public RiskManagementAgreementTO createRiskManagementAgreementTO() {
        return new RiskManagementAgreementTO();
    }

    /**
     * Create an instance of {@link RoleTypeTO }
     * 
     */
    public RoleTypeTO createRoleTypeTO() {
        return new RoleTypeTO();
    }

    /**
     * Create an instance of {@link StatusTO }
     * 
     */
    public StatusTO createStatusTO() {
        return new StatusTO();
    }

    /**
     * Create an instance of {@link IndexableCurrencyAmountValueTO }
     * 
     */
    public IndexableCurrencyAmountValueTO createIndexableCurrencyAmountValueTO() {
        return new IndexableCurrencyAmountValueTO();
    }

    /**
     * Create an instance of {@link CategorisableObjectTO }
     * 
     */
    public CategorisableObjectTO createCategorisableObjectTO() {
        return new CategorisableObjectTO();
    }

    /**
     * Create an instance of {@link IndexValueTypeTO }
     * 
     */
    public IndexValueTypeTO createIndexValueTypeTO() {
        return new IndexValueTypeTO();
    }

    /**
     * Create an instance of {@link RoleTO }
     * 
     */
    public RoleTO createRoleTO() {
        return new RoleTO();
    }

    /**
     * Create an instance of {@link StatusWithCommonReasonTO }
     * 
     */
    public StatusWithCommonReasonTO createStatusWithCommonReasonTO() {
        return new StatusWithCommonReasonTO();
    }

    /**
     * Create an instance of {@link BusinessRuleTO }
     * 
     */
    public BusinessRuleTO createBusinessRuleTO() {
        return new BusinessRuleTO();
    }

    /**
     * Create an instance of {@link PropertyTO }
     * 
     */
    public PropertyTO createPropertyTO() {
        return new PropertyTO();
    }

    /**
     * Create an instance of {@link CommentTO }
     * 
     */
    public CommentTO createCommentTO() {
        return new CommentTO();
    }

    /**
     * Create an instance of {@link ObjectReferenceTO }
     * 
     */
    public ObjectReferenceTO createObjectReferenceTO() {
        return new ObjectReferenceTO();
    }

    /**
     * Create an instance of {@link MessageTO }
     * 
     */
    public MessageTO createMessageTO() {
        return new MessageTO();
    }

    /**
     * Create an instance of {@link BusinessModelObjectTO }
     * 
     */
    public BusinessModelObjectTO createBusinessModelObjectTO() {
        return new BusinessModelObjectTO();
    }

    /**
     * Create an instance of {@link TypeTO }
     * 
     */
    public TypeTO createTypeTO() {
        return new TypeTO();
    }

    /**
     * Create an instance of {@link CategorySchemeTO }
     * 
     */
    public CategorySchemeTO createCategorySchemeTO() {
        return new CategorySchemeTO();
    }

    /**
     * Create an instance of {@link NoteTO }
     * 
     */
    public NoteTO createNoteTO() {
        return new NoteTO();
    }

    /**
     * Create an instance of {@link CategoryTO }
     * 
     */
    public CategoryTO createCategoryTO() {
        return new CategoryTO();
    }

    /**
     * Create an instance of {@link RelationshipTO }
     * 
     */
    public RelationshipTO createRelationshipTO() {
        return new RelationshipTO();
    }

    /**
     * Create an instance of {@link DependentObjectTO }
     * 
     */
    public DependentObjectTO createDependentObjectTO() {
        return new DependentObjectTO();
    }

    /**
     * Create an instance of {@link IndexableCurrencyAmountTO }
     * 
     */
    public IndexableCurrencyAmountTO createIndexableCurrencyAmountTO() {
        return new IndexableCurrencyAmountTO();
    }

    /**
     * Create an instance of {@link RoleInContextClassTO }
     * 
     */
    public RoleInContextClassTO createRoleInContextClassTO() {
        return new RoleInContextClassTO();
    }

    /**
     * Create an instance of {@link PublishedEventTypeTO }
     * 
     */
    public PublishedEventTypeTO createPublishedEventTypeTO() {
        return new PublishedEventTypeTO();
    }

    /**
     * Create an instance of {@link RelationshipTypeTO }
     * 
     */
    public RelationshipTypeTO createRelationshipTypeTO() {
        return new RelationshipTypeTO();
    }

    /**
     * Create an instance of {@link NotificationTO }
     * 
     */
    public NotificationTO createNotificationTO() {
        return new NotificationTO();
    }

    /**
     * Create an instance of {@link IndexValueTO }
     * 
     */
    public IndexValueTO createIndexValueTO() {
        return new IndexValueTO();
    }

    /**
     * Create an instance of {@link AlternateIdTO }
     * 
     */
    public AlternateIdTO createAlternateIdTO() {
        return new AlternateIdTO();
    }

    /**
     * Create an instance of {@link RolePlayerClassRoleTO }
     * 
     */
    public RolePlayerClassRoleTO createRolePlayerClassRoleTO() {
        return new RolePlayerClassRoleTO();
    }

    /**
     * Create an instance of {@link TransactionNotificationTO }
     * 
     */
    public TransactionNotificationTO createTransactionNotificationTO() {
        return new TransactionNotificationTO();
    }

    /**
     * Create an instance of {@link AutoPolicyLossDataScoreTO }
     * 
     */
    public AutoPolicyLossDataScoreTO createAutoPolicyLossDataScoreTO() {
        return new AutoPolicyLossDataScoreTO();
    }

    /**
     * Create an instance of {@link FinancialValuationTypeTO }
     * 
     */
    public FinancialValuationTypeTO createFinancialValuationTypeTO() {
        return new FinancialValuationTypeTO();
    }

    /**
     * Create an instance of {@link TrainingTO }
     * 
     */
    public TrainingTO createTrainingTO() {
        return new TrainingTO();
    }

    /**
     * Create an instance of {@link ScoreTO }
     * 
     */
    public ScoreTO createScoreTO() {
        return new ScoreTO();
    }

    /**
     * Create an instance of {@link StatisticalCodeSummaryTO }
     * 
     */
    public StatisticalCodeSummaryTO createStatisticalCodeSummaryTO() {
        return new StatisticalCodeSummaryTO();
    }

    /**
     * Create an instance of {@link AssessmentActivityTO }
     * 
     */
    public AssessmentActivityTO createAssessmentActivityTO() {
        return new AssessmentActivityTO();
    }

    /**
     * Create an instance of {@link PlacePositioningTO }
     * 
     */
    public PlacePositioningTO createPlacePositioningTO() {
        return new PlacePositioningTO();
    }

    /**
     * Create an instance of {@link InsurerLossDataScoreTO }
     * 
     */
    public InsurerLossDataScoreTO createInsurerLossDataScoreTO() {
        return new InsurerLossDataScoreTO();
    }

    /**
     * Create an instance of {@link FloodMapTO }
     * 
     */
    public FloodMapTO createFloodMapTO() {
        return new FloodMapTO();
    }

    /**
     * Create an instance of {@link EventTO }
     * 
     */
    public EventTO createEventTO() {
        return new EventTO();
    }

    /**
     * Create an instance of {@link ElectronicNetworkMapTO }
     * 
     */
    public ElectronicNetworkMapTO createElectronicNetworkMapTO() {
        return new ElectronicNetworkMapTO();
    }

    /**
     * Create an instance of {@link ConditionTypeTO }
     * 
     */
    public ConditionTypeTO createConditionTypeTO() {
        return new ConditionTypeTO();
    }

    /**
     * Create an instance of {@link ActivityOccurrenceTypeTO }
     * 
     */
    public ActivityOccurrenceTypeTO createActivityOccurrenceTypeTO() {
        return new ActivityOccurrenceTypeTO();
    }

    /**
     * Create an instance of {@link AssessmentResultRegistrationTypeTO }
     * 
     */
    public AssessmentResultRegistrationTypeTO createAssessmentResultRegistrationTypeTO() {
        return new AssessmentResultRegistrationTypeTO();
    }

    /**
     * Create an instance of {@link InsuranceRegulationMemberStateTO }
     * 
     */
    public InsuranceRegulationMemberStateTO createInsuranceRegulationMemberStateTO() {
        return new InsuranceRegulationMemberStateTO();
    }

    /**
     * Create an instance of {@link ActivityOccurrenceStatusTO }
     * 
     */
    public ActivityOccurrenceStatusTO createActivityOccurrenceStatusTO() {
        return new ActivityOccurrenceStatusTO();
    }

    /**
     * Create an instance of {@link PlaceRoleTO }
     * 
     */
    public PlaceRoleTO createPlaceRoleTO() {
        return new PlaceRoleTO();
    }

    /**
     * Create an instance of {@link PredictiveModelScoreReasonTO }
     * 
     */
    public PredictiveModelScoreReasonTO createPredictiveModelScoreReasonTO() {
        return new PredictiveModelScoreReasonTO();
    }

    /**
     * Create an instance of {@link WebPageEventTO }
     * 
     */
    public WebPageEventTO createWebPageEventTO() {
        return new WebPageEventTO();
    }

    /**
     * Create an instance of {@link FinancialValuationTO }
     * 
     */
    public FinancialValuationTO createFinancialValuationTO() {
        return new FinancialValuationTO();
    }

    /**
     * Create an instance of {@link RiskPlaceMapTO }
     * 
     */
    public RiskPlaceMapTO createRiskPlaceMapTO() {
        return new RiskPlaceMapTO();
    }

    /**
     * Create an instance of {@link ConditionTO }
     * 
     */
    public ConditionTO createConditionTO() {
        return new ConditionTO();
    }

    /**
     * Create an instance of {@link CountryMapTO }
     * 
     */
    public CountryMapTO createCountryMapTO() {
        return new CountryMapTO();
    }

    /**
     * Create an instance of {@link SocialActivityTO }
     * 
     */
    public SocialActivityTO createSocialActivityTO() {
        return new SocialActivityTO();
    }

    /**
     * Create an instance of {@link ActivitySpecificationRoleTO }
     * 
     */
    public ActivitySpecificationRoleTO createActivitySpecificationRoleTO() {
        return new ActivitySpecificationRoleTO();
    }

    /**
     * Create an instance of {@link PlaceRegistrationTO }
     * 
     */
    public PlaceRegistrationTO createPlaceRegistrationTO() {
        return new PlaceRegistrationTO();
    }

    /**
     * Create an instance of {@link MapTO }
     * 
     */
    public MapTO createMapTO() {
        return new MapTO();
    }

    /**
     * Create an instance of {@link AuditTypeTO }
     * 
     */
    public AuditTypeTO createAuditTypeTO() {
        return new AuditTypeTO();
    }

    /**
     * Create an instance of {@link PredictiveModelScoreTO }
     * 
     */
    public PredictiveModelScoreTO createPredictiveModelScoreTO() {
        return new PredictiveModelScoreTO();
    }

    /**
     * Create an instance of {@link InsuranceRegulationForeignStateTO }
     * 
     */
    public InsuranceRegulationForeignStateTO createInsuranceRegulationForeignStateTO() {
        return new InsuranceRegulationForeignStateTO();
    }

    /**
     * Create an instance of {@link AssessmentResultStatusTO }
     * 
     */
    public AssessmentResultStatusTO createAssessmentResultStatusTO() {
        return new AssessmentResultStatusTO();
    }

    /**
     * Create an instance of {@link TransportationTypeTO }
     * 
     */
    public TransportationTypeTO createTransportationTypeTO() {
        return new TransportationTypeTO();
    }

    /**
     * Create an instance of {@link ActivityTO }
     * 
     */
    public ActivityTO createActivityTO() {
        return new ActivityTO();
    }

    /**
     * Create an instance of {@link AuthorisationTypeTO }
     * 
     */
    public AuthorisationTypeTO createAuthorisationTypeTO() {
        return new AuthorisationTypeTO();
    }

    /**
     * Create an instance of {@link ScoreRangeTO }
     * 
     */
    public ScoreRangeTO createScoreRangeTO() {
        return new ScoreRangeTO();
    }

    /**
     * Create an instance of {@link ActivityOccurrenceSpecificationTO }
     * 
     */
    public ActivityOccurrenceSpecificationTO createActivityOccurrenceSpecificationTO() {
        return new ActivityOccurrenceSpecificationTO();
    }

    /**
     * Create an instance of {@link TransportationTO }
     * 
     */
    public TransportationTO createTransportationTO() {
        return new TransportationTO();
    }

    /**
     * Create an instance of {@link PlaceTypeTO }
     * 
     */
    public PlaceTypeTO createPlaceTypeTO() {
        return new PlaceTypeTO();
    }

    /**
     * Create an instance of {@link PreAuthorisationTO }
     * 
     */
    public PreAuthorisationTO createPreAuthorisationTO() {
        return new PreAuthorisationTO();
    }

    /**
     * Create an instance of {@link AssessmentResultRoleTO }
     * 
     */
    public AssessmentResultRoleTO createAssessmentResultRoleTO() {
        return new AssessmentResultRoleTO();
    }

    /**
     * Create an instance of {@link CountryRiskMapTO }
     * 
     */
    public CountryRiskMapTO createCountryRiskMapTO() {
        return new CountryRiskMapTO();
    }

    /**
     * Create an instance of {@link DriverActivityScoreTO }
     * 
     */
    public DriverActivityScoreTO createDriverActivityScoreTO() {
        return new DriverActivityScoreTO();
    }

    /**
     * Create an instance of {@link CountryTO }
     * 
     */
    public CountryTO createCountryTO() {
        return new CountryTO();
    }

    /**
     * Create an instance of {@link MedicalConditionTypeTO }
     * 
     */
    public MedicalConditionTypeTO createMedicalConditionTypeTO() {
        return new MedicalConditionTypeTO();
    }

    /**
     * Create an instance of {@link ActivityRoleTO }
     * 
     */
    public ActivityRoleTO createActivityRoleTO() {
        return new ActivityRoleTO();
    }

    /**
     * Create an instance of {@link EventCategoryTO }
     * 
     */
    public EventCategoryTO createEventCategoryTO() {
        return new EventCategoryTO();
    }

    /**
     * Create an instance of {@link ActivityCategoryTO }
     * 
     */
    public ActivityCategoryTO createActivityCategoryTO() {
        return new ActivityCategoryTO();
    }

    /**
     * Create an instance of {@link DataEventTO }
     * 
     */
    public DataEventTO createDataEventTO() {
        return new DataEventTO();
    }

    /**
     * Create an instance of {@link PredictiveModelScoreElementTO }
     * 
     */
    public PredictiveModelScoreElementTO createPredictiveModelScoreElementTO() {
        return new PredictiveModelScoreElementTO();
    }

    /**
     * Create an instance of {@link ObjectTreatmentTypeTO }
     * 
     */
    public ObjectTreatmentTypeTO createObjectTreatmentTypeTO() {
        return new ObjectTreatmentTypeTO();
    }

    /**
     * Create an instance of {@link AssessmentResultRegistrationTO }
     * 
     */
    public AssessmentResultRegistrationTO createAssessmentResultRegistrationTO() {
        return new AssessmentResultRegistrationTO();
    }

    /**
     * Create an instance of {@link MedicalTransportationTO }
     * 
     */
    public MedicalTransportationTO createMedicalTransportationTO() {
        return new MedicalTransportationTO();
    }

    /**
     * Create an instance of {@link GeometricShapeTO }
     * 
     */
    public GeometricShapeTO createGeometricShapeTO() {
        return new GeometricShapeTO();
    }

    /**
     * Create an instance of {@link AssessmentResultTO }
     * 
     */
    public AssessmentResultTO createAssessmentResultTO() {
        return new AssessmentResultTO();
    }

    /**
     * Create an instance of {@link DemographicScoreTO }
     * 
     */
    public DemographicScoreTO createDemographicScoreTO() {
        return new DemographicScoreTO();
    }

    /**
     * Create an instance of {@link ScoreFactorTO }
     * 
     */
    public ScoreFactorTO createScoreFactorTO() {
        return new ScoreFactorTO();
    }

    /**
     * Create an instance of {@link RegistrationTO }
     * 
     */
    public RegistrationTO createRegistrationTO() {
        return new RegistrationTO();
    }

    /**
     * Create an instance of {@link ActivityOccurrenceTO }
     * 
     */
    public ActivityOccurrenceTO createActivityOccurrenceTO() {
        return new ActivityOccurrenceTO();
    }

    /**
     * Create an instance of {@link ScoreTypeTO }
     * 
     */
    public ScoreTypeTO createScoreTypeTO() {
        return new ScoreTypeTO();
    }

    /**
     * Create an instance of {@link PlaceusageinactivityTO }
     * 
     */
    public PlaceusageinactivityTO createPlaceusageinactivityTO() {
        return new PlaceusageinactivityTO();
    }

    /**
     * Create an instance of {@link AssessmentActivityTypeTO }
     * 
     */
    public AssessmentActivityTypeTO createAssessmentActivityTypeTO() {
        return new AssessmentActivityTypeTO();
    }

    /**
     * Create an instance of {@link InsuranceRegulationAreaTO }
     * 
     */
    public InsuranceRegulationAreaTO createInsuranceRegulationAreaTO() {
        return new InsuranceRegulationAreaTO();
    }

    /**
     * Create an instance of {@link PostalOrganisationMapTO }
     * 
     */
    public PostalOrganisationMapTO createPostalOrganisationMapTO() {
        return new PostalOrganisationMapTO();
    }

    /**
     * Create an instance of {@link PredictiveModelPartTO }
     * 
     */
    public PredictiveModelPartTO createPredictiveModelPartTO() {
        return new PredictiveModelPartTO();
    }

    /**
     * Create an instance of {@link GeographicCoordinatesTO }
     * 
     */
    public GeographicCoordinatesTO createGeographicCoordinatesTO() {
        return new GeographicCoordinatesTO();
    }

    /**
     * Create an instance of {@link RegistrationStatusTO }
     * 
     */
    public RegistrationStatusTO createRegistrationStatusTO() {
        return new RegistrationStatusTO();
    }

    /**
     * Create an instance of {@link RelayServiceTO }
     * 
     */
    public RelayServiceTO createRelayServiceTO() {
        return new RelayServiceTO();
    }

    /**
     * Create an instance of {@link MedicalTreatmentTO }
     * 
     */
    public MedicalTreatmentTO createMedicalTreatmentTO() {
        return new MedicalTreatmentTO();
    }

    /**
     * Create an instance of {@link ScoreReasonTO }
     * 
     */
    public ScoreReasonTO createScoreReasonTO() {
        return new ScoreReasonTO();
    }

    /**
     * Create an instance of {@link PlaceTO }
     * 
     */
    public PlaceTO createPlaceTO() {
        return new PlaceTO();
    }

    /**
     * Create an instance of {@link EventRoleTO }
     * 
     */
    public EventRoleTO createEventRoleTO() {
        return new EventRoleTO();
    }

    /**
     * Create an instance of {@link PlaceCategoryTO }
     * 
     */
    public PlaceCategoryTO createPlaceCategoryTO() {
        return new PlaceCategoryTO();
    }

    /**
     * Create an instance of {@link RoleInEventTO }
     * 
     */
    public RoleInEventTO createRoleInEventTO() {
        return new RoleInEventTO();
    }

    /**
     * Create an instance of {@link ActivitySpecificationTO }
     * 
     */
    public ActivitySpecificationTO createActivitySpecificationTO() {
        return new ActivitySpecificationTO();
    }

    /**
     * Create an instance of {@link AuditTO }
     * 
     */
    public AuditTO createAuditTO() {
        return new AuditTO();
    }

    /**
     * Create an instance of {@link PredictiveModelScoreElementPartGroupTO }
     * 
     */
    public PredictiveModelScoreElementPartGroupTO createPredictiveModelScoreElementPartGroupTO() {
        return new PredictiveModelScoreElementPartGroupTO();
    }

    /**
     * Create an instance of {@link ObjectTreatmentTO }
     * 
     */
    public ObjectTreatmentTO createObjectTreatmentTO() {
        return new ObjectTreatmentTO();
    }

    /**
     * Create an instance of {@link AuthorisationTO }
     * 
     */
    public AuthorisationTO createAuthorisationTO() {
        return new AuthorisationTO();
    }

    /**
     * Create an instance of {@link DrivingActionTO }
     * 
     */
    public DrivingActionTO createDrivingActionTO() {
        return new DrivingActionTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInIntermediaryAgreementTO }
     * 
     */
    public PartyInvolvedInIntermediaryAgreementTO createPartyInvolvedInIntermediaryAgreementTO() {
        return new PartyInvolvedInIntermediaryAgreementTO();
    }

    /**
     * Create an instance of {@link CommissionStatementTO }
     * 
     */
    public CommissionStatementTO createCommissionStatementTO() {
        return new CommissionStatementTO();
    }

    /**
     * Create an instance of {@link AgentLicenseTO }
     * 
     */
    public AgentLicenseTO createAgentLicenseTO() {
        return new AgentLicenseTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInIntermediaryAgreementTypeTO }
     * 
     */
    public PartyInvolvedInIntermediaryAgreementTypeTO createPartyInvolvedInIntermediaryAgreementTypeTO() {
        return new PartyInvolvedInIntermediaryAgreementTypeTO();
    }

    /**
     * Create an instance of {@link SpecificationInvolvedInIntermediaryAgreementTO }
     * 
     */
    public SpecificationInvolvedInIntermediaryAgreementTO createSpecificationInvolvedInIntermediaryAgreementTO() {
        return new SpecificationInvolvedInIntermediaryAgreementTO();
    }

    /**
     * Create an instance of {@link IntermediaryAgreementStatusChangeRequestTO }
     * 
     */
    public IntermediaryAgreementStatusChangeRequestTO createIntermediaryAgreementStatusChangeRequestTO() {
        return new IntermediaryAgreementStatusChangeRequestTO();
    }

    /**
     * Create an instance of {@link IntermediaryAgreementStatusTO }
     * 
     */
    public IntermediaryAgreementStatusTO createIntermediaryAgreementStatusTO() {
        return new IntermediaryAgreementStatusTO();
    }

    /**
     * Create an instance of {@link IntermediaryAgreementSpecificationTO }
     * 
     */
    public IntermediaryAgreementSpecificationTO createIntermediaryAgreementSpecificationTO() {
        return new IntermediaryAgreementSpecificationTO();
    }

    /**
     * Create an instance of {@link IntermediaryAgreementTO }
     * 
     */
    public IntermediaryAgreementTO createIntermediaryAgreementTO() {
        return new IntermediaryAgreementTO();
    }

    /**
     * Create an instance of {@link CommissionDetailTO }
     * 
     */
    public CommissionDetailTO createCommissionDetailTO() {
        return new CommissionDetailTO();
    }

    /**
     * Create an instance of {@link RoleInIntermediaryAgreementTO }
     * 
     */
    public RoleInIntermediaryAgreementTO createRoleInIntermediaryAgreementTO() {
        return new RoleInIntermediaryAgreementTO();
    }

    /**
     * Create an instance of {@link ChannelRoleTO }
     * 
     */
    public ChannelRoleTO createChannelRoleTO() {
        return new ChannelRoleTO();
    }

    /**
     * Create an instance of {@link IntermediaryAgreementRequestTO }
     * 
     */
    public IntermediaryAgreementRequestTO createIntermediaryAgreementRequestTO() {
        return new IntermediaryAgreementRequestTO();
    }

    /**
     * Create an instance of {@link ChannelRoleTypeTO }
     * 
     */
    public ChannelRoleTypeTO createChannelRoleTypeTO() {
        return new ChannelRoleTypeTO();
    }

    /**
     * Create an instance of {@link CommissionScheduleTO }
     * 
     */
    public CommissionScheduleTO createCommissionScheduleTO() {
        return new CommissionScheduleTO();
    }

    /**
     * Create an instance of {@link UpdateSubmissionAndPartsRequest }
     * 
     */
    public UpdateSubmissionAndPartsRequest createUpdateSubmissionAndPartsRequest() {
        return new UpdateSubmissionAndPartsRequest();
    }

    /**
     * Create an instance of {@link BindPolicyRequest }
     * 
     */
    public BindPolicyRequest createBindPolicyRequest() {
        return new BindPolicyRequest();
    }

    /**
     * Create an instance of {@link BindPolicyResponse }
     * 
     */
    public BindPolicyResponse createBindPolicyResponse() {
        return new BindPolicyResponse();
    }

    /**
     * Create an instance of {@link CreateSubmissionRequest }
     * 
     */
    public CreateSubmissionRequest createCreateSubmissionRequest() {
        return new CreateSubmissionRequest();
    }

    /**
     * Create an instance of {@link UpdateSubmissionAndPartsResponse }
     * 
     */
    public UpdateSubmissionAndPartsResponse createUpdateSubmissionAndPartsResponse() {
        return new UpdateSubmissionAndPartsResponse();
    }

    /**
     * Create an instance of {@link CreateRenewalSubmissionRequest }
     * 
     */
    public CreateRenewalSubmissionRequest createCreateRenewalSubmissionRequest() {
        return new CreateRenewalSubmissionRequest();
    }

    /**
     * Create an instance of {@link CreateRenewalSubmissionResponse }
     * 
     */
    public CreateRenewalSubmissionResponse createCreateRenewalSubmissionResponse() {
        return new CreateRenewalSubmissionResponse();
    }

    /**
     * Create an instance of {@link RecordSubmissionDocumentDetailsResponse }
     * 
     */
    public RecordSubmissionDocumentDetailsResponse createRecordSubmissionDocumentDetailsResponse() {
        return new RecordSubmissionDocumentDetailsResponse();
    }

    /**
     * Create an instance of {@link RecordSubmissionDocumentDetailsRequest }
     * 
     */
    public RecordSubmissionDocumentDetailsRequest createRecordSubmissionDocumentDetailsRequest() {
        return new RecordSubmissionDocumentDetailsRequest();
    }

    /**
     * Create an instance of {@link CreateSubmissionResponse }
     * 
     */
    public CreateSubmissionResponse createCreateSubmissionResponse() {
        return new CreateSubmissionResponse();
    }

    /**
     * Create an instance of {@link BaseCurrencyAmount }
     * 
     */
    public BaseCurrencyAmount createBaseCurrencyAmount() {
        return new BaseCurrencyAmount();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link Value }
     * 
     */
    public Value createValue() {
        return new Value();
    }

    /**
     * Create an instance of {@link Binary }
     * 
     */
    public Binary createBinary() {
        return new Binary();
    }

    /**
     * Create an instance of {@link ComputerMonitorModelTO }
     * 
     */
    public ComputerMonitorModelTO createComputerMonitorModelTO() {
        return new ComputerMonitorModelTO();
    }

    /**
     * Create an instance of {@link HumanBodyTO }
     * 
     */
    public HumanBodyTO createHumanBodyTO() {
        return new HumanBodyTO();
    }

    /**
     * Create an instance of {@link DrugTO }
     * 
     */
    public DrugTO createDrugTO() {
        return new DrugTO();
    }

    /**
     * Create an instance of {@link TelematicConditionTO }
     * 
     */
    public TelematicConditionTO createTelematicConditionTO() {
        return new TelematicConditionTO();
    }

    /**
     * Create an instance of {@link StructureBuiltinTO }
     * 
     */
    public StructureBuiltinTO createStructureBuiltinTO() {
        return new StructureBuiltinTO();
    }

    /**
     * Create an instance of {@link EarthquakeBuildingScoreTO }
     * 
     */
    public EarthquakeBuildingScoreTO createEarthquakeBuildingScoreTO() {
        return new EarthquakeBuildingScoreTO();
    }

    /**
     * Create an instance of {@link VehicleRegistrationTO }
     * 
     */
    public VehicleRegistrationTO createVehicleRegistrationTO() {
        return new VehicleRegistrationTO();
    }

    /**
     * Create an instance of {@link PhysicalObjectPlaceUsageTO }
     * 
     */
    public PhysicalObjectPlaceUsageTO createPhysicalObjectPlaceUsageTO() {
        return new PhysicalObjectPlaceUsageTO();
    }

    /**
     * Create an instance of {@link ManufacturedItemTO }
     * 
     */
    public ManufacturedItemTO createManufacturedItemTO() {
        return new ManufacturedItemTO();
    }

    /**
     * Create an instance of {@link DrugSpecificationTO }
     * 
     */
    public DrugSpecificationTO createDrugSpecificationTO() {
        return new DrugSpecificationTO();
    }

    /**
     * Create an instance of {@link PhysicalConditionTO }
     * 
     */
    public PhysicalConditionTO createPhysicalConditionTO() {
        return new PhysicalConditionTO();
    }

    /**
     * Create an instance of {@link BodyElementTO }
     * 
     */
    public BodyElementTO createBodyElementTO() {
        return new BodyElementTO();
    }

    /**
     * Create an instance of {@link HeatingUnitTO }
     * 
     */
    public HeatingUnitTO createHeatingUnitTO() {
        return new HeatingUnitTO();
    }

    /**
     * Create an instance of {@link TruckModelTO }
     * 
     */
    public TruckModelTO createTruckModelTO() {
        return new TruckModelTO();
    }

    /**
     * Create an instance of {@link FloodingRiskScoreTO }
     * 
     */
    public FloodingRiskScoreTO createFloodingRiskScoreTO() {
        return new FloodingRiskScoreTO();
    }

    /**
     * Create an instance of {@link PhysicalObjectRoleTO }
     * 
     */
    public PhysicalObjectRoleTO createPhysicalObjectRoleTO() {
        return new PhysicalObjectRoleTO();
    }

    /**
     * Create an instance of {@link ModelSpecificationTypeTO }
     * 
     */
    public ModelSpecificationTypeTO createModelSpecificationTypeTO() {
        return new ModelSpecificationTypeTO();
    }

    /**
     * Create an instance of {@link ShipTO }
     * 
     */
    public ShipTO createShipTO() {
        return new ShipTO();
    }

    /**
     * Create an instance of {@link BulkMaterialTO }
     * 
     */
    public BulkMaterialTO createBulkMaterialTO() {
        return new BulkMaterialTO();
    }

    /**
     * Create an instance of {@link ModelSpecificationRegistrationTO }
     * 
     */
    public ModelSpecificationRegistrationTO createModelSpecificationRegistrationTO() {
        return new ModelSpecificationRegistrationTO();
    }

    /**
     * Create an instance of {@link VehicleTO }
     * 
     */
    public VehicleTO createVehicleTO() {
        return new VehicleTO();
    }

    /**
     * Create an instance of {@link PhysicalObjectTO }
     * 
     */
    public PhysicalObjectTO createPhysicalObjectTO() {
        return new PhysicalObjectTO();
    }

    /**
     * Create an instance of {@link PhysicalObjectRegistrationTO }
     * 
     */
    public PhysicalObjectRegistrationTO createPhysicalObjectRegistrationTO() {
        return new PhysicalObjectRegistrationTO();
    }

    /**
     * Create an instance of {@link LandTO }
     * 
     */
    public LandTO createLandTO() {
        return new LandTO();
    }

    /**
     * Create an instance of {@link ManufacturedItemTypeTO }
     * 
     */
    public ManufacturedItemTypeTO createManufacturedItemTypeTO() {
        return new ManufacturedItemTypeTO();
    }

    /**
     * Create an instance of {@link OfficeBlockTO }
     * 
     */
    public OfficeBlockTO createOfficeBlockTO() {
        return new OfficeBlockTO();
    }

    /**
     * Create an instance of {@link StructureScoreTO }
     * 
     */
    public StructureScoreTO createStructureScoreTO() {
        return new StructureScoreTO();
    }

    /**
     * Create an instance of {@link CarModelTO }
     * 
     */
    public CarModelTO createCarModelTO() {
        return new CarModelTO();
    }

    /**
     * Create an instance of {@link RoleInPhysicalObjectTO }
     * 
     */
    public RoleInPhysicalObjectTO createRoleInPhysicalObjectTO() {
        return new RoleInPhysicalObjectTO();
    }

    /**
     * Create an instance of {@link ShipScoreTO }
     * 
     */
    public ShipScoreTO createShipScoreTO() {
        return new ShipScoreTO();
    }

    /**
     * Create an instance of {@link ModelSpecificationRoleTO }
     * 
     */
    public ModelSpecificationRoleTO createModelSpecificationRoleTO() {
        return new ModelSpecificationRoleTO();
    }

    /**
     * Create an instance of {@link GarageTO }
     * 
     */
    public GarageTO createGarageTO() {
        return new GarageTO();
    }

    /**
     * Create an instance of {@link ShipModelTO }
     * 
     */
    public ShipModelTO createShipModelTO() {
        return new ShipModelTO();
    }

    /**
     * Create an instance of {@link BodyPartTO }
     * 
     */
    public BodyPartTO createBodyPartTO() {
        return new BodyPartTO();
    }

    /**
     * Create an instance of {@link PhysicalObjectTypeTO }
     * 
     */
    public PhysicalObjectTypeTO createPhysicalObjectTypeTO() {
        return new PhysicalObjectTypeTO();
    }

    /**
     * Create an instance of {@link VehicleScoreTO }
     * 
     */
    public VehicleScoreTO createVehicleScoreTO() {
        return new VehicleScoreTO();
    }

    /**
     * Create an instance of {@link AircraftModelTO }
     * 
     */
    public AircraftModelTO createAircraftModelTO() {
        return new AircraftModelTO();
    }

    /**
     * Create an instance of {@link VehicleModelTO }
     * 
     */
    public VehicleModelTO createVehicleModelTO() {
        return new VehicleModelTO();
    }

    /**
     * Create an instance of {@link StructureTO }
     * 
     */
    public StructureTO createStructureTO() {
        return new StructureTO();
    }

    /**
     * Create an instance of {@link PhysicalObjectActivityOccurrenceUsageTO }
     * 
     */
    public PhysicalObjectActivityOccurrenceUsageTO createPhysicalObjectActivityOccurrenceUsageTO() {
        return new PhysicalObjectActivityOccurrenceUsageTO();
    }

    /**
     * Create an instance of {@link ObjectGroupTypeTO }
     * 
     */
    public ObjectGroupTypeTO createObjectGroupTypeTO() {
        return new ObjectGroupTypeTO();
    }

    /**
     * Create an instance of {@link RoadVehicleModelTO }
     * 
     */
    public RoadVehicleModelTO createRoadVehicleModelTO() {
        return new RoadVehicleModelTO();
    }

    /**
     * Create an instance of {@link DrugRegistrationTO }
     * 
     */
    public DrugRegistrationTO createDrugRegistrationTO() {
        return new DrugRegistrationTO();
    }

    /**
     * Create an instance of {@link ComputerModelTO }
     * 
     */
    public ComputerModelTO createComputerModelTO() {
        return new ComputerModelTO();
    }

    /**
     * Create an instance of {@link AnimalTO }
     * 
     */
    public AnimalTO createAnimalTO() {
        return new AnimalTO();
    }

    /**
     * Create an instance of {@link DwellingTO }
     * 
     */
    public DwellingTO createDwellingTO() {
        return new DwellingTO();
    }

    /**
     * Create an instance of {@link LifeFormTO }
     * 
     */
    public LifeFormTO createLifeFormTO() {
        return new LifeFormTO();
    }

    /**
     * Create an instance of {@link CargoTO }
     * 
     */
    public CargoTO createCargoTO() {
        return new CargoTO();
    }

    /**
     * Create an instance of {@link ObjectGroupTO }
     * 
     */
    public ObjectGroupTO createObjectGroupTO() {
        return new ObjectGroupTO();
    }

    /**
     * Create an instance of {@link MedicalConditionTO }
     * 
     */
    public MedicalConditionTO createMedicalConditionTO() {
        return new MedicalConditionTO();
    }

    /**
     * Create an instance of {@link LifeFormStatusTO }
     * 
     */
    public LifeFormStatusTO createLifeFormStatusTO() {
        return new LifeFormStatusTO();
    }

    /**
     * Create an instance of {@link ModelSpecificationTO }
     * 
     */
    public ModelSpecificationTO createModelSpecificationTO() {
        return new ModelSpecificationTO();
    }

    /**
     * Create an instance of {@link StructureRegistrationTO }
     * 
     */
    public StructureRegistrationTO createStructureRegistrationTO() {
        return new StructureRegistrationTO();
    }

    /**
     * Create an instance of {@link ManufacturedItemPartTO }
     * 
     */
    public ManufacturedItemPartTO createManufacturedItemPartTO() {
        return new ManufacturedItemPartTO();
    }

    /**
     * Create an instance of {@link CostOfCapitalTO }
     * 
     */
    public CostOfCapitalTO createCostOfCapitalTO() {
        return new CostOfCapitalTO();
    }

    /**
     * Create an instance of {@link BenefitRateTO }
     * 
     */
    public BenefitRateTO createBenefitRateTO() {
        return new BenefitRateTO();
    }

    /**
     * Create an instance of {@link RiskFreeInterestRateTO }
     * 
     */
    public RiskFreeInterestRateTO createRiskFreeInterestRateTO() {
        return new RiskFreeInterestRateTO();
    }

    /**
     * Create an instance of {@link ExchangeRateIndexTO }
     * 
     */
    public ExchangeRateIndexTO createExchangeRateIndexTO() {
        return new ExchangeRateIndexTO();
    }

    /**
     * Create an instance of {@link IndexTO }
     * 
     */
    public IndexTO createIndexTO() {
        return new IndexTO();
    }

    /**
     * Create an instance of {@link InflationIndexTO }
     * 
     */
    public InflationIndexTO createInflationIndexTO() {
        return new InflationIndexTO();
    }

    /**
     * Create an instance of {@link ActuarialStatisticsTypeTO }
     * 
     */
    public ActuarialStatisticsTypeTO createActuarialStatisticsTypeTO() {
        return new ActuarialStatisticsTypeTO();
    }

    /**
     * Create an instance of {@link SolvencyRiskParametersTO }
     * 
     */
    public SolvencyRiskParametersTO createSolvencyRiskParametersTO() {
        return new SolvencyRiskParametersTO();
    }

    /**
     * Create an instance of {@link RateTO }
     * 
     */
    public RateTO createRateTO() {
        return new RateTO();
    }

    /**
     * Create an instance of {@link ChargeRateTO }
     * 
     */
    public ChargeRateTO createChargeRateTO() {
        return new ChargeRateTO();
    }

    /**
     * Create an instance of {@link RoleInStatisticsTO }
     * 
     */
    public RoleInStatisticsTO createRoleInStatisticsTO() {
        return new RoleInStatisticsTO();
    }

    /**
     * Create an instance of {@link InterestRateTO }
     * 
     */
    public InterestRateTO createInterestRateTO() {
        return new InterestRateTO();
    }

    /**
     * Create an instance of {@link BonusRateTO }
     * 
     */
    public BonusRateTO createBonusRateTO() {
        return new BonusRateTO();
    }

    /**
     * Create an instance of {@link DistributionRangeTO }
     * 
     */
    public DistributionRangeTO createDistributionRangeTO() {
        return new DistributionRangeTO();
    }

    /**
     * Create an instance of {@link EscalationIndexValueTO }
     * 
     */
    public EscalationIndexValueTO createEscalationIndexValueTO() {
        return new EscalationIndexValueTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionDeterminerTO }
     * 
     */
    public MoneyProvisionDeterminerTO createMoneyProvisionDeterminerTO() {
        return new MoneyProvisionDeterminerTO();
    }

    /**
     * Create an instance of {@link StandardCreditLevelTO }
     * 
     */
    public StandardCreditLevelTO createStandardCreditLevelTO() {
        return new StandardCreditLevelTO();
    }

    /**
     * Create an instance of {@link ActuarialStatisticsTO }
     * 
     */
    public ActuarialStatisticsTO createActuarialStatisticsTO() {
        return new ActuarialStatisticsTO();
    }

    /**
     * Create an instance of {@link StandardCreditLevelParameterTO }
     * 
     */
    public StandardCreditLevelParameterTO createStandardCreditLevelParameterTO() {
        return new StandardCreditLevelParameterTO();
    }

    /**
     * Create an instance of {@link StatisticsTO }
     * 
     */
    public StatisticsTO createStatisticsTO() {
        return new StatisticsTO();
    }

    /**
     * Create an instance of {@link SolvencyCorrelationParametersTO }
     * 
     */
    public SolvencyCorrelationParametersTO createSolvencyCorrelationParametersTO() {
        return new SolvencyCorrelationParametersTO();
    }

    /**
     * Create an instance of {@link RoleInStatisticsTypeTO }
     * 
     */
    public RoleInStatisticsTypeTO createRoleInStatisticsTypeTO() {
        return new RoleInStatisticsTypeTO();
    }

    /**
     * Create an instance of {@link SolvencyRiskParametersTypeTO }
     * 
     */
    public SolvencyRiskParametersTypeTO createSolvencyRiskParametersTypeTO() {
        return new SolvencyRiskParametersTypeTO();
    }

    /**
     * Create an instance of {@link CoverageComponentDetailTO }
     * 
     */
    public CoverageComponentDetailTO createCoverageComponentDetailTO() {
        return new CoverageComponentDetailTO();
    }

    /**
     * Create an instance of {@link ConditionalRecurringTransferTO }
     * 
     */
    public ConditionalRecurringTransferTO createConditionalRecurringTransferTO() {
        return new ConditionalRecurringTransferTO();
    }

    /**
     * Create an instance of {@link PaymentFacilityRequestTO }
     * 
     */
    public PaymentFacilityRequestTO createPaymentFacilityRequestTO() {
        return new PaymentFacilityRequestTO();
    }

    /**
     * Create an instance of {@link EmploymentAgreementRequestTO }
     * 
     */
    public EmploymentAgreementRequestTO createEmploymentAgreementRequestTO() {
        return new EmploymentAgreementRequestTO();
    }

    /**
     * Create an instance of {@link CommercialPropertyInsurancePolicyTO }
     * 
     */
    public CommercialPropertyInsurancePolicyTO createCommercialPropertyInsurancePolicyTO() {
        return new CommercialPropertyInsurancePolicyTO();
    }

    /**
     * Create an instance of {@link NamedDriverTO }
     * 
     */
    public NamedDriverTO createNamedDriverTO() {
        return new NamedDriverTO();
    }

    /**
     * Create an instance of {@link CustomerAccountAgreementTO }
     * 
     */
    public CustomerAccountAgreementTO createCustomerAccountAgreementTO() {
        return new CustomerAccountAgreementTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialServicesAgreementTO }
     * 
     */
    public RoleInFinancialServicesAgreementTO createRoleInFinancialServicesAgreementTO() {
        return new RoleInFinancialServicesAgreementTO();
    }

    /**
     * Create an instance of {@link GuarantorTO }
     * 
     */
    public GuarantorTO createGuarantorTO() {
        return new GuarantorTO();
    }

    /**
     * Create an instance of {@link ObligationComponentTO }
     * 
     */
    public ObligationComponentTO createObligationComponentTO() {
        return new ObligationComponentTO();
    }

    /**
     * Create an instance of {@link LifeCoverageRiskMarginOptionalSegmentTO }
     * 
     */
    public LifeCoverageRiskMarginOptionalSegmentTO createLifeCoverageRiskMarginOptionalSegmentTO() {
        return new LifeCoverageRiskMarginOptionalSegmentTO();
    }

    /**
     * Create an instance of {@link DisabilityCoverageTO }
     * 
     */
    public DisabilityCoverageTO createDisabilityCoverageTO() {
        return new DisabilityCoverageTO();
    }

    /**
     * Create an instance of {@link ServiceComponentTO }
     * 
     */
    public ServiceComponentTO createServiceComponentTO() {
        return new ServiceComponentTO();
    }

    /**
     * Create an instance of {@link UnitHoldingTO }
     * 
     */
    public UnitHoldingTO createUnitHoldingTO() {
        return new UnitHoldingTO();
    }

    /**
     * Create an instance of {@link DerivativeWriterTO }
     * 
     */
    public DerivativeWriterTO createDerivativeWriterTO() {
        return new DerivativeWriterTO();
    }

    /**
     * Create an instance of {@link StructuralComponentTO }
     * 
     */
    public StructuralComponentTO createStructuralComponentTO() {
        return new StructuralComponentTO();
    }

    /**
     * Create an instance of {@link FinancialServicesRoleTO }
     * 
     */
    public FinancialServicesRoleTO createFinancialServicesRoleTO() {
        return new FinancialServicesRoleTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInEmploymentAgreementTypeTO }
     * 
     */
    public PartyInvolvedInEmploymentAgreementTypeTO createPartyInvolvedInEmploymentAgreementTypeTO() {
        return new PartyInvolvedInEmploymentAgreementTypeTO();
    }

    /**
     * Create an instance of {@link CommercialAgreementTypeTO }
     * 
     */
    public CommercialAgreementTypeTO createCommercialAgreementTypeTO() {
        return new CommercialAgreementTypeTO();
    }

    /**
     * Create an instance of {@link FinancialServicesRequestTypeTO }
     * 
     */
    public FinancialServicesRequestTypeTO createFinancialServicesRequestTypeTO() {
        return new FinancialServicesRequestTypeTO();
    }

    /**
     * Create an instance of {@link WorkersCompensationInsurancePolicyTO }
     * 
     */
    public WorkersCompensationInsurancePolicyTO createWorkersCompensationInsurancePolicyTO() {
        return new WorkersCompensationInsurancePolicyTO();
    }

    /**
     * Create an instance of {@link AccountRoleInFsaTypeTO }
     * 
     */
    public AccountRoleInFsaTypeTO createAccountRoleInFsaTypeTO() {
        return new AccountRoleInFsaTypeTO();
    }

    /**
     * Create an instance of {@link EmploymentCategoryTO }
     * 
     */
    public EmploymentCategoryTO createEmploymentCategoryTO() {
        return new EmploymentCategoryTO();
    }

    /**
     * Create an instance of {@link FinancialMarketRequestTO }
     * 
     */
    public FinancialMarketRequestTO createFinancialMarketRequestTO() {
        return new FinancialMarketRequestTO();
    }

    /**
     * Create an instance of {@link AdditionalInsuredTO }
     * 
     */
    public AdditionalInsuredTO createAdditionalInsuredTO() {
        return new AdditionalInsuredTO();
    }

    /**
     * Create an instance of {@link TransferFacilityTO }
     * 
     */
    public TransferFacilityTO createTransferFacilityTO() {
        return new TransferFacilityTO();
    }

    /**
     * Create an instance of {@link SellRequestTO }
     * 
     */
    public SellRequestTO createSellRequestTO() {
        return new SellRequestTO();
    }

    /**
     * Create an instance of {@link SavingsComponentTO }
     * 
     */
    public SavingsComponentTO createSavingsComponentTO() {
        return new SavingsComponentTO();
    }

    /**
     * Create an instance of {@link IndividualInvestmentPolicyTO }
     * 
     */
    public IndividualInvestmentPolicyTO createIndividualInvestmentPolicyTO() {
        return new IndividualInvestmentPolicyTO();
    }

    /**
     * Create an instance of {@link AnnuityCoverageTO }
     * 
     */
    public AnnuityCoverageTO createAnnuityCoverageTO() {
        return new AnnuityCoverageTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialServicesRequestTO }
     * 
     */
    public RoleInFinancialServicesRequestTO createRoleInFinancialServicesRequestTO() {
        return new RoleInFinancialServicesRequestTO();
    }

    /**
     * Create an instance of {@link AccountInvolvedInFsaTO }
     * 
     */
    public AccountInvolvedInFsaTO createAccountInvolvedInFsaTO() {
        return new AccountInvolvedInFsaTO();
    }

    /**
     * Create an instance of {@link InsurerTO }
     * 
     */
    public InsurerTO createInsurerTO() {
        return new InsurerTO();
    }

    /**
     * Create an instance of {@link NonlifeCoverageSegmentTO }
     * 
     */
    public NonlifeCoverageSegmentTO createNonlifeCoverageSegmentTO() {
        return new NonlifeCoverageSegmentTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialServicesRequestTypeTO }
     * 
     */
    public RoleInFinancialServicesRequestTypeTO createRoleInFinancialServicesRequestTypeTO() {
        return new RoleInFinancialServicesRequestTypeTO();
    }

    /**
     * Create an instance of {@link AnnuityTO }
     * 
     */
    public AnnuityTO createAnnuityTO() {
        return new AnnuityTO();
    }

    /**
     * Create an instance of {@link AutomobileInsurancePolicyTO }
     * 
     */
    public AutomobileInsurancePolicyTO createAutomobileInsurancePolicyTO() {
        return new AutomobileInsurancePolicyTO();
    }

    /**
     * Create an instance of {@link NewBusinessRequestTO }
     * 
     */
    public NewBusinessRequestTO createNewBusinessRequestTO() {
        return new NewBusinessRequestTO();
    }

    /**
     * Create an instance of {@link BenefitObligationTO }
     * 
     */
    public BenefitObligationTO createBenefitObligationTO() {
        return new BenefitObligationTO();
    }

    /**
     * Create an instance of {@link AgreementCollectionTO }
     * 
     */
    public AgreementCollectionTO createAgreementCollectionTO() {
        return new AgreementCollectionTO();
    }

    /**
     * Create an instance of {@link StandingOrderTO }
     * 
     */
    public StandingOrderTO createStandingOrderTO() {
        return new StandingOrderTO();
    }

    /**
     * Create an instance of {@link ReinsuranceAgreementComponentTO }
     * 
     */
    public ReinsuranceAgreementComponentTO createReinsuranceAgreementComponentTO() {
        return new ReinsuranceAgreementComponentTO();
    }

    /**
     * Create an instance of {@link InvestmentPortfolioAgreementTO }
     * 
     */
    public InvestmentPortfolioAgreementTO createInvestmentPortfolioAgreementTO() {
        return new InvestmentPortfolioAgreementTO();
    }

    /**
     * Create an instance of {@link PaymentFacilityTO }
     * 
     */
    public PaymentFacilityTO createPaymentFacilityTO() {
        return new PaymentFacilityTO();
    }

    /**
     * Create an instance of {@link ReportingFacilityTO }
     * 
     */
    public ReportingFacilityTO createReportingFacilityTO() {
        return new ReportingFacilityTO();
    }

    /**
     * Create an instance of {@link CoverageComponentTypeTO }
     * 
     */
    public CoverageComponentTypeTO createCoverageComponentTypeTO() {
        return new CoverageComponentTypeTO();
    }

    /**
     * Create an instance of {@link FinancialServicesCompanyRegistrationTO }
     * 
     */
    public FinancialServicesCompanyRegistrationTO createFinancialServicesCompanyRegistrationTO() {
        return new FinancialServicesCompanyRegistrationTO();
    }

    /**
     * Create an instance of {@link EmploymentAgreementStatusTO }
     * 
     */
    public EmploymentAgreementStatusTO createEmploymentAgreementStatusTO() {
        return new EmploymentAgreementStatusTO();
    }

    /**
     * Create an instance of {@link ProfitSharingComponentTO }
     * 
     */
    public ProfitSharingComponentTO createProfitSharingComponentTO() {
        return new ProfitSharingComponentTO();
    }

    /**
     * Create an instance of {@link PremiumObligationTO }
     * 
     */
    public PremiumObligationTO createPremiumObligationTO() {
        return new PremiumObligationTO();
    }

    /**
     * Create an instance of {@link RiderTO }
     * 
     */
    public RiderTO createRiderTO() {
        return new RiderTO();
    }

    /**
     * Create an instance of {@link LifeAndHealthPolicyTO }
     * 
     */
    public LifeAndHealthPolicyTO createLifeAndHealthPolicyTO() {
        return new LifeAndHealthPolicyTO();
    }

    /**
     * Create an instance of {@link AccountAgreementStatusTO }
     * 
     */
    public AccountAgreementStatusTO createAccountAgreementStatusTO() {
        return new AccountAgreementStatusTO();
    }

    /**
     * Create an instance of {@link LoanAccountAgreementTO }
     * 
     */
    public LoanAccountAgreementTO createLoanAccountAgreementTO() {
        return new LoanAccountAgreementTO();
    }

    /**
     * Create an instance of {@link BeneficiaryTO }
     * 
     */
    public BeneficiaryTO createBeneficiaryTO() {
        return new BeneficiaryTO();
    }

    /**
     * Create an instance of {@link DerivativeContractTypeTO }
     * 
     */
    public DerivativeContractTypeTO createDerivativeContractTypeTO() {
        return new DerivativeContractTypeTO();
    }

    /**
     * Create an instance of {@link InsurancePolicyTO }
     * 
     */
    public InsurancePolicyTO createInsurancePolicyTO() {
        return new InsurancePolicyTO();
    }

    /**
     * Create an instance of {@link ServicingChannelTypeTO }
     * 
     */
    public ServicingChannelTypeTO createServicingChannelTypeTO() {
        return new ServicingChannelTypeTO();
    }

    /**
     * Create an instance of {@link LargeRiskAgreementsTO }
     * 
     */
    public LargeRiskAgreementsTO createLargeRiskAgreementsTO() {
        return new LargeRiskAgreementsTO();
    }

    /**
     * Create an instance of {@link InsuredTypeTO }
     * 
     */
    public InsuredTypeTO createInsuredTypeTO() {
        return new InsuredTypeTO();
    }

    /**
     * Create an instance of {@link FinancialMovementRequestTO }
     * 
     */
    public FinancialMovementRequestTO createFinancialMovementRequestTO() {
        return new FinancialMovementRequestTO();
    }

    /**
     * Create an instance of {@link RateRoleInFsaTypeTO }
     * 
     */
    public RateRoleInFsaTypeTO createRateRoleInFsaTypeTO() {
        return new RateRoleInFsaTypeTO();
    }

    /**
     * Create an instance of {@link LifeCoverageSecondarySegmentTO }
     * 
     */
    public LifeCoverageSecondarySegmentTO createLifeCoverageSecondarySegmentTO() {
        return new LifeCoverageSecondarySegmentTO();
    }

    /**
     * Create an instance of {@link ServiceComponentTypeTO }
     * 
     */
    public ServiceComponentTypeTO createServiceComponentTypeTO() {
        return new ServiceComponentTypeTO();
    }

    /**
     * Create an instance of {@link FinancialServicesAgreementComponentTO }
     * 
     */
    public FinancialServicesAgreementComponentTO createFinancialServicesAgreementComponentTO() {
        return new FinancialServicesAgreementComponentTO();
    }

    /**
     * Create an instance of {@link CommercialAutoInsurancePolicyTO }
     * 
     */
    public CommercialAutoInsurancePolicyTO createCommercialAutoInsurancePolicyTO() {
        return new CommercialAutoInsurancePolicyTO();
    }

    /**
     * Create an instance of {@link ApplicantTO }
     * 
     */
    public ApplicantTO createApplicantTO() {
        return new ApplicantTO();
    }

    /**
     * Create an instance of {@link DirectDebitMandateTO }
     * 
     */
    public DirectDebitMandateTO createDirectDebitMandateTO() {
        return new DirectDebitMandateTO();
    }

    /**
     * Create an instance of {@link FinancialServicesAgreementTO }
     * 
     */
    public FinancialServicesAgreementTO createFinancialServicesAgreementTO() {
        return new FinancialServicesAgreementTO();
    }

    /**
     * Create an instance of {@link AccountAgreementTO }
     * 
     */
    public AccountAgreementTO createAccountAgreementTO() {
        return new AccountAgreementTO();
    }

    /**
     * Create an instance of {@link CreditCardTransactionRequestTO }
     * 
     */
    public CreditCardTransactionRequestTO createCreditCardTransactionRequestTO() {
        return new CreditCardTransactionRequestTO();
    }

    /**
     * Create an instance of {@link StatusChangeRequestTO }
     * 
     */
    public StatusChangeRequestTO createStatusChangeRequestTO() {
        return new StatusChangeRequestTO();
    }

    /**
     * Create an instance of {@link CommercialAgreementTO }
     * 
     */
    public CommercialAgreementTO createCommercialAgreementTO() {
        return new CommercialAgreementTO();
    }

    /**
     * Create an instance of {@link CoverageComponentTO }
     * 
     */
    public CoverageComponentTO createCoverageComponentTO() {
        return new CoverageComponentTO();
    }

    /**
     * Create an instance of {@link ServicingChannelTO }
     * 
     */
    public ServicingChannelTO createServicingChannelTO() {
        return new ServicingChannelTO();
    }

    /**
     * Create an instance of {@link RoleInEmploymentAgreementTO }
     * 
     */
    public RoleInEmploymentAgreementTO createRoleInEmploymentAgreementTO() {
        return new RoleInEmploymentAgreementTO();
    }

    /**
     * Create an instance of {@link CommercialAgreementDetailTO }
     * 
     */
    public CommercialAgreementDetailTO createCommercialAgreementDetailTO() {
        return new CommercialAgreementDetailTO();
    }

    /**
     * Create an instance of {@link BillerTO }
     * 
     */
    public BillerTO createBillerTO() {
        return new BillerTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialServicesAgreementTypeTO }
     * 
     */
    public RoleInFinancialServicesAgreementTypeTO createRoleInFinancialServicesAgreementTypeTO() {
        return new RoleInFinancialServicesAgreementTypeTO();
    }

    /**
     * Create an instance of {@link EmploymentAgreementSpecificationTO }
     * 
     */
    public EmploymentAgreementSpecificationTO createEmploymentAgreementSpecificationTO() {
        return new EmploymentAgreementSpecificationTO();
    }

    /**
     * Create an instance of {@link GuaranteeTO }
     * 
     */
    public GuaranteeTO createGuaranteeTO() {
        return new GuaranteeTO();
    }

    /**
     * Create an instance of {@link LifeCoveragePrimarySegmentTO }
     * 
     */
    public LifeCoveragePrimarySegmentTO createLifeCoveragePrimarySegmentTO() {
        return new LifeCoveragePrimarySegmentTO();
    }

    /**
     * Create an instance of {@link AccountFacilityComponentTO }
     * 
     */
    public AccountFacilityComponentTO createAccountFacilityComponentTO() {
        return new AccountFacilityComponentTO();
    }

    /**
     * Create an instance of {@link FinancialServicesRoleTypeTO }
     * 
     */
    public FinancialServicesRoleTypeTO createFinancialServicesRoleTypeTO() {
        return new FinancialServicesRoleTypeTO();
    }

    /**
     * Create an instance of {@link FinancialServicesRequestDetailTO }
     * 
     */
    public FinancialServicesRequestDetailTO createFinancialServicesRequestDetailTO() {
        return new FinancialServicesRequestDetailTO();
    }

    /**
     * Create an instance of {@link DepositAccountAgreementTO }
     * 
     */
    public DepositAccountAgreementTO createDepositAccountAgreementTO() {
        return new DepositAccountAgreementTO();
    }

    /**
     * Create an instance of {@link CommercialVehicleCoverageTO }
     * 
     */
    public CommercialVehicleCoverageTO createCommercialVehicleCoverageTO() {
        return new CommercialVehicleCoverageTO();
    }

    /**
     * Create an instance of {@link AssessmentRequestTO }
     * 
     */
    public AssessmentRequestTO createAssessmentRequestTO() {
        return new AssessmentRequestTO();
    }

    /**
     * Create an instance of {@link CommutationAgreementTO }
     * 
     */
    public CommutationAgreementTO createCommutationAgreementTO() {
        return new CommutationAgreementTO();
    }

    /**
     * Create an instance of {@link SingleTransferRequestTO }
     * 
     */
    public SingleTransferRequestTO createSingleTransferRequestTO() {
        return new SingleTransferRequestTO();
    }

    /**
     * Create an instance of {@link LifeCoverageRiskMarginStandardSegmentTO }
     * 
     */
    public LifeCoverageRiskMarginStandardSegmentTO createLifeCoverageRiskMarginStandardSegmentTO() {
        return new LifeCoverageRiskMarginStandardSegmentTO();
    }

    /**
     * Create an instance of {@link TopLevelFinancialServicesAgreementTO }
     * 
     */
    public TopLevelFinancialServicesAgreementTO createTopLevelFinancialServicesAgreementTO() {
        return new TopLevelFinancialServicesAgreementTO();
    }

    /**
     * Create an instance of {@link AccessFacilityTO }
     * 
     */
    public AccessFacilityTO createAccessFacilityTO() {
        return new AccessFacilityTO();
    }

    /**
     * Create an instance of {@link DerivativeContractTO }
     * 
     */
    public DerivativeContractTO createDerivativeContractTO() {
        return new DerivativeContractTO();
    }

    /**
     * Create an instance of {@link ObjectCoveredRoleInFsaTO }
     * 
     */
    public ObjectCoveredRoleInFsaTO createObjectCoveredRoleInFsaTO() {
        return new ObjectCoveredRoleInFsaTO();
    }

    /**
     * Create an instance of {@link PolicyholderTO }
     * 
     */
    public PolicyholderTO createPolicyholderTO() {
        return new PolicyholderTO();
    }

    /**
     * Create an instance of {@link CreditCardAgreementTO }
     * 
     */
    public CreditCardAgreementTO createCreditCardAgreementTO() {
        return new CreditCardAgreementTO();
    }

    /**
     * Create an instance of {@link PaymentFacilityTypeTO }
     * 
     */
    public PaymentFacilityTypeTO createPaymentFacilityTypeTO() {
        return new PaymentFacilityTypeTO();
    }

    /**
     * Create an instance of {@link DeathCoverageTO }
     * 
     */
    public DeathCoverageTO createDeathCoverageTO() {
        return new DeathCoverageTO();
    }

    /**
     * Create an instance of {@link InsurancePolicyStatusTO }
     * 
     */
    public InsurancePolicyStatusTO createInsurancePolicyStatusTO() {
        return new InsurancePolicyStatusTO();
    }

    /**
     * Create an instance of {@link PaymentAuthorisationRequestTO }
     * 
     */
    public PaymentAuthorisationRequestTO createPaymentAuthorisationRequestTO() {
        return new PaymentAuthorisationRequestTO();
    }

    /**
     * Create an instance of {@link IndividualLifeInsurancePolicyTO }
     * 
     */
    public IndividualLifeInsurancePolicyTO createIndividualLifeInsurancePolicyTO() {
        return new IndividualLifeInsurancePolicyTO();
    }

    /**
     * Create an instance of {@link FundSwitchRequestTO }
     * 
     */
    public FundSwitchRequestTO createFundSwitchRequestTO() {
        return new FundSwitchRequestTO();
    }

    /**
     * Create an instance of {@link CurrentAccountAgreementTO }
     * 
     */
    public CurrentAccountAgreementTO createCurrentAccountAgreementTO() {
        return new CurrentAccountAgreementTO();
    }

    /**
     * Create an instance of {@link IndividualAgreementTO }
     * 
     */
    public IndividualAgreementTO createIndividualAgreementTO() {
        return new IndividualAgreementTO();
    }

    /**
     * Create an instance of {@link RateInvolvedInFsaTO }
     * 
     */
    public RateInvolvedInFsaTO createRateInvolvedInFsaTO() {
        return new RateInvolvedInFsaTO();
    }

    /**
     * Create an instance of {@link PartyInvolvedInEmploymentAgreementTO }
     * 
     */
    public PartyInvolvedInEmploymentAgreementTO createPartyInvolvedInEmploymentAgreementTO() {
        return new PartyInvolvedInEmploymentAgreementTO();
    }

    /**
     * Create an instance of {@link InsuredTO }
     * 
     */
    public InsuredTO createInsuredTO() {
        return new InsuredTO();
    }

    /**
     * Create an instance of {@link EmploymentAgreementTO }
     * 
     */
    public EmploymentAgreementTO createEmploymentAgreementTO() {
        return new EmploymentAgreementTO();
    }

    /**
     * Create an instance of {@link FinancialMovementRequestTypeTO }
     * 
     */
    public FinancialMovementRequestTypeTO createFinancialMovementRequestTypeTO() {
        return new FinancialMovementRequestTypeTO();
    }

    /**
     * Create an instance of {@link FinancialServicesRequestTO }
     * 
     */
    public FinancialServicesRequestTO createFinancialServicesRequestTO() {
        return new FinancialServicesRequestTO();
    }

    /**
     * Create an instance of {@link CoverageGroupTO }
     * 
     */
    public CoverageGroupTO createCoverageGroupTO() {
        return new CoverageGroupTO();
    }

    /**
     * Create an instance of {@link PurchaseRequestTO }
     * 
     */
    public PurchaseRequestTO createPurchaseRequestTO() {
        return new PurchaseRequestTO();
    }

    /**
     * Create an instance of {@link GroupAgreementTO }
     * 
     */
    public GroupAgreementTO createGroupAgreementTO() {
        return new GroupAgreementTO();
    }

    /**
     * Create an instance of {@link LoanComponentTO }
     * 
     */
    public LoanComponentTO createLoanComponentTO() {
        return new LoanComponentTO();
    }

    /**
     * Create an instance of {@link TaskSpecificationTypeTO }
     * 
     */
    public TaskSpecificationTypeTO createTaskSpecificationTypeTO() {
        return new TaskSpecificationTypeTO();
    }

    /**
     * Create an instance of {@link TaskPerformanceScoreTO }
     * 
     */
    public TaskPerformanceScoreTO createTaskPerformanceScoreTO() {
        return new TaskPerformanceScoreTO();
    }

    /**
     * Create an instance of {@link TaskTypeTO }
     * 
     */
    public TaskTypeTO createTaskTypeTO() {
        return new TaskTypeTO();
    }

    /**
     * Create an instance of {@link RoleInTaskTypeTO }
     * 
     */
    public RoleInTaskTypeTO createRoleInTaskTypeTO() {
        return new RoleInTaskTypeTO();
    }

    /**
     * Create an instance of {@link TaskSpecificationTO }
     * 
     */
    public TaskSpecificationTO createTaskSpecificationTO() {
        return new TaskSpecificationTO();
    }

    /**
     * Create an instance of {@link TaskScoreTO }
     * 
     */
    public TaskScoreTO createTaskScoreTO() {
        return new TaskScoreTO();
    }

    /**
     * Create an instance of {@link RoleInTaskTO }
     * 
     */
    public RoleInTaskTO createRoleInTaskTO() {
        return new RoleInTaskTO();
    }

    /**
     * Create an instance of {@link ProcedureTO }
     * 
     */
    public ProcedureTO createProcedureTO() {
        return new ProcedureTO();
    }

    /**
     * Create an instance of {@link TaskPerformanceScoreTypeTO }
     * 
     */
    public TaskPerformanceScoreTypeTO createTaskPerformanceScoreTypeTO() {
        return new TaskPerformanceScoreTypeTO();
    }

    /**
     * Create an instance of {@link TaskTO }
     * 
     */
    public TaskTO createTaskTO() {
        return new TaskTO();
    }

    /**
     * Create an instance of {@link KeyPerformanceIndicatorTO }
     * 
     */
    public KeyPerformanceIndicatorTO createKeyPerformanceIndicatorTO() {
        return new KeyPerformanceIndicatorTO();
    }

    /**
     * Create an instance of {@link AttributeSpecTO }
     * 
     */
    public AttributeSpecTO createAttributeSpecTO() {
        return new AttributeSpecTO();
    }

    /**
     * Create an instance of {@link AgreementSpecStatusTO }
     * 
     */
    public AgreementSpecStatusTO createAgreementSpecStatusTO() {
        return new AgreementSpecStatusTO();
    }

    /**
     * Create an instance of {@link RequestTO }
     * 
     */
    public RequestTO createRequestTO() {
        return new RequestTO();
    }

    /**
     * Create an instance of {@link RelationshipSpecTO }
     * 
     */
    public RelationshipSpecTO createRelationshipSpecTO() {
        return new RelationshipSpecTO();
    }

    /**
     * Create an instance of {@link ComponentListTO }
     * 
     */
    public ComponentListTO createComponentListTO() {
        return new ComponentListTO();
    }

    /**
     * Create an instance of {@link AlgorithmTO }
     * 
     */
    public AlgorithmTO createAlgorithmTO() {
        return new AlgorithmTO();
    }

    /**
     * Create an instance of {@link RuleSpecTO }
     * 
     */
    public RuleSpecTO createRuleSpecTO() {
        return new RuleSpecTO();
    }

    /**
     * Create an instance of {@link ExtendedAlgorithmTO }
     * 
     */
    public ExtendedAlgorithmTO createExtendedAlgorithmTO() {
        return new ExtendedAlgorithmTO();
    }

    /**
     * Create an instance of {@link ExtendedNavigationPathTO }
     * 
     */
    public ExtendedNavigationPathTO createExtendedNavigationPathTO() {
        return new ExtendedNavigationPathTO();
    }

    /**
     * Create an instance of {@link SpecificationBuildingBlockStatusTO }
     * 
     */
    public SpecificationBuildingBlockStatusTO createSpecificationBuildingBlockStatusTO() {
        return new SpecificationBuildingBlockStatusTO();
    }

    /**
     * Create an instance of {@link RequestBehaviourSpecTO }
     * 
     */
    public RequestBehaviourSpecTO createRequestBehaviourSpecTO() {
        return new RequestBehaviourSpecTO();
    }

    /**
     * Create an instance of {@link CalculationSpecificationCategoryTO }
     * 
     */
    public CalculationSpecificationCategoryTO createCalculationSpecificationCategoryTO() {
        return new CalculationSpecificationCategoryTO();
    }

    /**
     * Create an instance of {@link AgreementActualTO }
     * 
     */
    public AgreementActualTO createAgreementActualTO() {
        return new AgreementActualTO();
    }

    /**
     * Create an instance of {@link ExpressionSpecTO }
     * 
     */
    public ExpressionSpecTO createExpressionSpecTO() {
        return new ExpressionSpecTO();
    }

    /**
     * Create an instance of {@link CompositeExpressionSpecTO }
     * 
     */
    public CompositeExpressionSpecTO createCompositeExpressionSpecTO() {
        return new CompositeExpressionSpecTO();
    }

    /**
     * Create an instance of {@link RoleSpecTO }
     * 
     */
    public RoleSpecTO createRoleSpecTO() {
        return new RoleSpecTO();
    }

    /**
     * Create an instance of {@link ParseableNavigationPathTO }
     * 
     */
    public ParseableNavigationPathTO createParseableNavigationPathTO() {
        return new ParseableNavigationPathTO();
    }

    /**
     * Create an instance of {@link AgreementSpecDefCriteriaTO }
     * 
     */
    public AgreementSpecDefCriteriaTO createAgreementSpecDefCriteriaTO() {
        return new AgreementSpecDefCriteriaTO();
    }

    /**
     * Create an instance of {@link ActualTO }
     * 
     */
    public ActualTO createActualTO() {
        return new ActualTO();
    }

    /**
     * Create an instance of {@link ExternalAlgorithmTO }
     * 
     */
    public ExternalAlgorithmTO createExternalAlgorithmTO() {
        return new ExternalAlgorithmTO();
    }

    /**
     * Create an instance of {@link AgreementSpecTO }
     * 
     */
    public AgreementSpecTO createAgreementSpecTO() {
        return new AgreementSpecTO();
    }

    /**
     * Create an instance of {@link RoleInAgreementTO }
     * 
     */
    public RoleInAgreementTO createRoleInAgreementTO() {
        return new RoleInAgreementTO();
    }

    /**
     * Create an instance of {@link AvailableRequestTO }
     * 
     */
    public AvailableRequestTO createAvailableRequestTO() {
        return new AvailableRequestTO();
    }

    /**
     * Create an instance of {@link NavigationPathTO }
     * 
     */
    public NavigationPathTO createNavigationPathTO() {
        return new NavigationPathTO();
    }

    /**
     * Create an instance of {@link AgreementVersionTO }
     * 
     */
    public AgreementVersionTO createAgreementVersionTO() {
        return new AgreementVersionTO();
    }

    /**
     * Create an instance of {@link AgreementPartTO }
     * 
     */
    public AgreementPartTO createAgreementPartTO() {
        return new AgreementPartTO();
    }

    /**
     * Create an instance of {@link RoleListTO }
     * 
     */
    public RoleListTO createRoleListTO() {
        return new RoleListTO();
    }

    /**
     * Create an instance of {@link ElementaryExpressionSpecTO }
     * 
     */
    public ElementaryExpressionSpecTO createElementaryExpressionSpecTO() {
        return new ElementaryExpressionSpecTO();
    }

    /**
     * Create an instance of {@link ConstantRoleSpecTO }
     * 
     */
    public ConstantRoleSpecTO createConstantRoleSpecTO() {
        return new ConstantRoleSpecTO();
    }

    /**
     * Create an instance of {@link AgreementSpecConceptTO }
     * 
     */
    public AgreementSpecConceptTO createAgreementSpecConceptTO() {
        return new AgreementSpecConceptTO();
    }

    /**
     * Create an instance of {@link ParseableAlgorithmTO }
     * 
     */
    public ParseableAlgorithmTO createParseableAlgorithmTO() {
        return new ParseableAlgorithmTO();
    }

    /**
     * Create an instance of {@link ExpressionResultTO }
     * 
     */
    public ExpressionResultTO createExpressionResultTO() {
        return new ExpressionResultTO();
    }

    /**
     * Create an instance of {@link ConstantPropertySpecTO }
     * 
     */
    public ConstantPropertySpecTO createConstantPropertySpecTO() {
        return new ConstantPropertySpecTO();
    }

    /**
     * Create an instance of {@link PropertySpecTO }
     * 
     */
    public PropertySpecTO createPropertySpecTO() {
        return new PropertySpecTO();
    }

    /**
     * Create an instance of {@link CalculationTO }
     * 
     */
    public CalculationTO createCalculationTO() {
        return new CalculationTO();
    }

    /**
     * Create an instance of {@link SpecificationBuildingBlockTO }
     * 
     */
    public SpecificationBuildingBlockTO createSpecificationBuildingBlockTO() {
        return new SpecificationBuildingBlockTO();
    }

    /**
     * Create an instance of {@link MultiplicityListTO }
     * 
     */
    public MultiplicityListTO createMultiplicityListTO() {
        return new MultiplicityListTO();
    }

    /**
     * Create an instance of {@link RuleResultTO }
     * 
     */
    public RuleResultTO createRuleResultTO() {
        return new RuleResultTO();
    }

    /**
     * Create an instance of {@link StructuredActualTO }
     * 
     */
    public StructuredActualTO createStructuredActualTO() {
        return new StructuredActualTO();
    }

    /**
     * Create an instance of {@link RequestSpecTO }
     * 
     */
    public RequestSpecTO createRequestSpecTO() {
        return new RequestSpecTO();
    }

    /**
     * Create an instance of {@link SpecificationTO }
     * 
     */
    public SpecificationTO createSpecificationTO() {
        return new SpecificationTO();
    }

    /**
     * Create an instance of {@link AgreementTO }
     * 
     */
    public AgreementTO createAgreementTO() {
        return new AgreementTO();
    }

    /**
     * Create an instance of {@link BaseTransferObject }
     * 
     */
    public BaseTransferObject createBaseTransferObject() {
        return new BaseTransferObject();
    }

    /**
     * Create an instance of {@link LegalActionTO }
     * 
     */
    public LegalActionTO createLegalActionTO() {
        return new LegalActionTO();
    }

    /**
     * Create an instance of {@link RoleInLegalActionTypeTO }
     * 
     */
    public RoleInLegalActionTypeTO createRoleInLegalActionTypeTO() {
        return new RoleInLegalActionTypeTO();
    }

    /**
     * Create an instance of {@link LegalActionRoleTO }
     * 
     */
    public LegalActionRoleTO createLegalActionRoleTO() {
        return new LegalActionRoleTO();
    }

    /**
     * Create an instance of {@link RoleInLegalActionTO }
     * 
     */
    public RoleInLegalActionTO createRoleInLegalActionTO() {
        return new RoleInLegalActionTO();
    }

    /**
     * Create an instance of {@link LegalActionTypeTO }
     * 
     */
    public LegalActionTypeTO createLegalActionTypeTO() {
        return new LegalActionTypeTO();
    }

    /**
     * Create an instance of {@link PaymentTO }
     * 
     */
    public PaymentTO createPaymentTO() {
        return new PaymentTO();
    }

    /**
     * Create an instance of {@link FinancialTransactionCardTO }
     * 
     */
    public FinancialTransactionCardTO createFinancialTransactionCardTO() {
        return new FinancialTransactionCardTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionRoleTO }
     * 
     */
    public MoneyProvisionRoleTO createMoneyProvisionRoleTO() {
        return new MoneyProvisionRoleTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionElementPartTO }
     * 
     */
    public MoneyProvisionElementPartTO createMoneyProvisionElementPartTO() {
        return new MoneyProvisionElementPartTO();
    }

    /**
     * Create an instance of {@link FinancialTransactionRelationshipTO }
     * 
     */
    public FinancialTransactionRelationshipTO createFinancialTransactionRelationshipTO() {
        return new FinancialTransactionRelationshipTO();
    }

    /**
     * Create an instance of {@link CashFlowCategoryTO }
     * 
     */
    public CashFlowCategoryTO createCashFlowCategoryTO() {
        return new CashFlowCategoryTO();
    }

    /**
     * Create an instance of {@link ReservationStrategyTO }
     * 
     */
    public ReservationStrategyTO createReservationStrategyTO() {
        return new ReservationStrategyTO();
    }

    /**
     * Create an instance of {@link FinancialStatementStatusTO }
     * 
     */
    public FinancialStatementStatusTO createFinancialStatementStatusTO() {
        return new FinancialStatementStatusTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionRelationshipTypeTO }
     * 
     */
    public MoneyProvisionRelationshipTypeTO createMoneyProvisionRelationshipTypeTO() {
        return new MoneyProvisionRelationshipTypeTO();
    }

    /**
     * Create an instance of {@link MoneySchedulerTO }
     * 
     */
    public MoneySchedulerTO createMoneySchedulerTO() {
        return new MoneySchedulerTO();
    }

    /**
     * Create an instance of {@link StatementLineTO }
     * 
     */
    public StatementLineTO createStatementLineTO() {
        return new StatementLineTO();
    }

    /**
     * Create an instance of {@link ChequeTO }
     * 
     */
    public ChequeTO createChequeTO() {
        return new ChequeTO();
    }

    /**
     * Create an instance of {@link CreditCardStatementTO }
     * 
     */
    public CreditCardStatementTO createCreditCardStatementTO() {
        return new CreditCardStatementTO();
    }

    /**
     * Create an instance of {@link MoneyTransferInformationTO }
     * 
     */
    public MoneyTransferInformationTO createMoneyTransferInformationTO() {
        return new MoneyTransferInformationTO();
    }

    /**
     * Create an instance of {@link DirectDebitMandateDetailsTO }
     * 
     */
    public DirectDebitMandateDetailsTO createDirectDebitMandateDetailsTO() {
        return new DirectDebitMandateDetailsTO();
    }

    /**
     * Create an instance of {@link PaymentStatusTO }
     * 
     */
    public PaymentStatusTO createPaymentStatusTO() {
        return new PaymentStatusTO();
    }

    /**
     * Create an instance of {@link FinancialTransactionMediumTO }
     * 
     */
    public FinancialTransactionMediumTO createFinancialTransactionMediumTO() {
        return new FinancialTransactionMediumTO();
    }

    /**
     * Create an instance of {@link FinancialStatementTO }
     * 
     */
    public FinancialStatementTO createFinancialStatementTO() {
        return new FinancialStatementTO();
    }

    /**
     * Create an instance of {@link ParticularMoneyProvisionTO }
     * 
     */
    public ParticularMoneyProvisionTO createParticularMoneyProvisionTO() {
        return new ParticularMoneyProvisionTO();
    }

    /**
     * Create an instance of {@link PaymentMethodTO }
     * 
     */
    public PaymentMethodTO createPaymentMethodTO() {
        return new PaymentMethodTO();
    }

    /**
     * Create an instance of {@link InvoiceTO }
     * 
     */
    public InvoiceTO createInvoiceTO() {
        return new InvoiceTO();
    }

    /**
     * Create an instance of {@link BusinessFinancialStatementTO }
     * 
     */
    public BusinessFinancialStatementTO createBusinessFinancialStatementTO() {
        return new BusinessFinancialStatementTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionRelationshipTO }
     * 
     */
    public MoneyProvisionRelationshipTO createMoneyProvisionRelationshipTO() {
        return new MoneyProvisionRelationshipTO();
    }

    /**
     * Create an instance of {@link BalanceTO }
     * 
     */
    public BalanceTO createBalanceTO() {
        return new BalanceTO();
    }

    /**
     * Create an instance of {@link RemittanceTO }
     * 
     */
    public RemittanceTO createRemittanceTO() {
        return new RemittanceTO();
    }

    /**
     * Create an instance of {@link SingleCashFlowTO }
     * 
     */
    public SingleCashFlowTO createSingleCashFlowTO() {
        return new SingleCashFlowTO();
    }

    /**
     * Create an instance of {@link GenericMoneyProvisionTO }
     * 
     */
    public GenericMoneyProvisionTO createGenericMoneyProvisionTO() {
        return new GenericMoneyProvisionTO();
    }

    /**
     * Create an instance of {@link FinancialTransactionPayeeTO }
     * 
     */
    public FinancialTransactionPayeeTO createFinancialTransactionPayeeTO() {
        return new FinancialTransactionPayeeTO();
    }

    /**
     * Create an instance of {@link ReservedAmountTypeTO }
     * 
     */
    public ReservedAmountTypeTO createReservedAmountTypeTO() {
        return new ReservedAmountTypeTO();
    }

    /**
     * Create an instance of {@link SubtotalTO }
     * 
     */
    public SubtotalTO createSubtotalTO() {
        return new SubtotalTO();
    }

    /**
     * Create an instance of {@link RoleInMoneyProvisionTypeTO }
     * 
     */
    public RoleInMoneyProvisionTypeTO createRoleInMoneyProvisionTypeTO() {
        return new RoleInMoneyProvisionTypeTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionElementPartTypeTO }
     * 
     */
    public MoneyProvisionElementPartTypeTO createMoneyProvisionElementPartTypeTO() {
        return new MoneyProvisionElementPartTypeTO();
    }

    /**
     * Create an instance of {@link ClaimCostCodeTO }
     * 
     */
    public ClaimCostCodeTO createClaimCostCodeTO() {
        return new ClaimCostCodeTO();
    }

    /**
     * Create an instance of {@link PaymentDueStatusTO }
     * 
     */
    public PaymentDueStatusTO createPaymentDueStatusTO() {
        return new PaymentDueStatusTO();
    }

    /**
     * Create an instance of {@link AdjustmentTO }
     * 
     */
    public AdjustmentTO createAdjustmentTO() {
        return new AdjustmentTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialTransactionTO }
     * 
     */
    public RoleInFinancialTransactionTO createRoleInFinancialTransactionTO() {
        return new RoleInFinancialTransactionTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionElementTO }
     * 
     */
    public MoneyProvisionElementTO createMoneyProvisionElementTO() {
        return new MoneyProvisionElementTO();
    }

    /**
     * Create an instance of {@link BillTO }
     * 
     */
    public BillTO createBillTO() {
        return new BillTO();
    }

    /**
     * Create an instance of {@link MoneySchedulerRoleTO }
     * 
     */
    public MoneySchedulerRoleTO createMoneySchedulerRoleTO() {
        return new MoneySchedulerRoleTO();
    }

    /**
     * Create an instance of {@link DeductibleTO }
     * 
     */
    public DeductibleTO createDeductibleTO() {
        return new DeductibleTO();
    }

    /**
     * Create an instance of {@link SeriesOfCashFlowsTO }
     * 
     */
    public SeriesOfCashFlowsTO createSeriesOfCashFlowsTO() {
        return new SeriesOfCashFlowsTO();
    }

    /**
     * Create an instance of {@link PaymentTransferMethodTO }
     * 
     */
    public PaymentTransferMethodTO createPaymentTransferMethodTO() {
        return new PaymentTransferMethodTO();
    }

    /**
     * Create an instance of {@link StatementElementTO }
     * 
     */
    public StatementElementTO createStatementElementTO() {
        return new StatementElementTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialTransactionTypeTO }
     * 
     */
    public RoleInFinancialTransactionTypeTO createRoleInFinancialTransactionTypeTO() {
        return new RoleInFinancialTransactionTypeTO();
    }

    /**
     * Create an instance of {@link LimitTO }
     * 
     */
    public LimitTO createLimitTO() {
        return new LimitTO();
    }

    /**
     * Create an instance of {@link DelinquencyTO }
     * 
     */
    public DelinquencyTO createDelinquencyTO() {
        return new DelinquencyTO();
    }

    /**
     * Create an instance of {@link FinancialTransactionTO }
     * 
     */
    public FinancialTransactionTO createFinancialTransactionTO() {
        return new FinancialTransactionTO();
    }

    /**
     * Create an instance of {@link AccountStatementTO }
     * 
     */
    public AccountStatementTO createAccountStatementTO() {
        return new AccountStatementTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionElementTypeTO }
     * 
     */
    public MoneyProvisionElementTypeTO createMoneyProvisionElementTypeTO() {
        return new MoneyProvisionElementTypeTO();
    }

    /**
     * Create an instance of {@link MoneyInSchedulerTO }
     * 
     */
    public MoneyInSchedulerTO createMoneyInSchedulerTO() {
        return new MoneyInSchedulerTO();
    }

    /**
     * Create an instance of {@link FinancialStatementTypeTO }
     * 
     */
    public FinancialStatementTypeTO createFinancialStatementTypeTO() {
        return new FinancialStatementTypeTO();
    }

    /**
     * Create an instance of {@link MoneyOutSchedulerTO }
     * 
     */
    public MoneyOutSchedulerTO createMoneyOutSchedulerTO() {
        return new MoneyOutSchedulerTO();
    }

    /**
     * Create an instance of {@link PaymentDueTO }
     * 
     */
    public PaymentDueTO createPaymentDueTO() {
        return new PaymentDueTO();
    }

    /**
     * Create an instance of {@link RiskToleranceLimitTO }
     * 
     */
    public RiskToleranceLimitTO createRiskToleranceLimitTO() {
        return new RiskToleranceLimitTO();
    }

    /**
     * Create an instance of {@link MoneyProvisionTO }
     * 
     */
    public MoneyProvisionTO createMoneyProvisionTO() {
        return new MoneyProvisionTO();
    }

    /**
     * Create an instance of {@link ReservedAmountTO }
     * 
     */
    public ReservedAmountTO createReservedAmountTO() {
        return new ReservedAmountTO();
    }

    /**
     * Create an instance of {@link RoleInMoneyProvisionTO }
     * 
     */
    public RoleInMoneyProvisionTO createRoleInMoneyProvisionTO() {
        return new RoleInMoneyProvisionTO();
    }

    /**
     * Create an instance of {@link SecurityRegistrationTO }
     * 
     */
    public SecurityRegistrationTO createSecurityRegistrationTO() {
        return new SecurityRegistrationTO();
    }

    /**
     * Create an instance of {@link AccessRegistrationTO }
     * 
     */
    public AccessRegistrationTO createAccessRegistrationTO() {
        return new AccessRegistrationTO();
    }

    /**
     * Create an instance of {@link CompoundAccessRegistrationTO }
     * 
     */
    public CompoundAccessRegistrationTO createCompoundAccessRegistrationTO() {
        return new CompoundAccessRegistrationTO();
    }

    /**
     * Create an instance of {@link SecurityKeyTO }
     * 
     */
    public SecurityKeyTO createSecurityKeyTO() {
        return new SecurityKeyTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialAssetTO }
     * 
     */
    public RoleInFinancialAssetTO createRoleInFinancialAssetTO() {
        return new RoleInFinancialAssetTO();
    }

    /**
     * Create an instance of {@link FinancialAssetTO }
     * 
     */
    public FinancialAssetTO createFinancialAssetTO() {
        return new FinancialAssetTO();
    }

    /**
     * Create an instance of {@link AccountTO }
     * 
     */
    public AccountTO createAccountTO() {
        return new AccountTO();
    }

    /**
     * Create an instance of {@link SolvencyReportingAssetCategoryTO }
     * 
     */
    public SolvencyReportingAssetCategoryTO createSolvencyReportingAssetCategoryTO() {
        return new SolvencyReportingAssetCategoryTO();
    }

    /**
     * Create an instance of {@link DebtInstrumentTO }
     * 
     */
    public DebtInstrumentTO createDebtInstrumentTO() {
        return new DebtInstrumentTO();
    }

    /**
     * Create an instance of {@link DerivativeUsageCategoryTO }
     * 
     */
    public DerivativeUsageCategoryTO createDerivativeUsageCategoryTO() {
        return new DerivativeUsageCategoryTO();
    }

    /**
     * Create an instance of {@link CapitalInstrumentTypeTO }
     * 
     */
    public CapitalInstrumentTypeTO createCapitalInstrumentTypeTO() {
        return new CapitalInstrumentTypeTO();
    }

    /**
     * Create an instance of {@link AssetCategoryTO }
     * 
     */
    public AssetCategoryTO createAssetCategoryTO() {
        return new AssetCategoryTO();
    }

    /**
     * Create an instance of {@link MarketableObjectTO }
     * 
     */
    public MarketableObjectTO createMarketableObjectTO() {
        return new MarketableObjectTO();
    }

    /**
     * Create an instance of {@link OptionTO }
     * 
     */
    public OptionTO createOptionTO() {
        return new OptionTO();
    }

    /**
     * Create an instance of {@link MonetaryAccountTO }
     * 
     */
    public MonetaryAccountTO createMonetaryAccountTO() {
        return new MonetaryAccountTO();
    }

    /**
     * Create an instance of {@link StructuredProductTO }
     * 
     */
    public StructuredProductTO createStructuredProductTO() {
        return new StructuredProductTO();
    }

    /**
     * Create an instance of {@link CapitalInstrumentTO }
     * 
     */
    public CapitalInstrumentTO createCapitalInstrumentTO() {
        return new CapitalInstrumentTO();
    }

    /**
     * Create an instance of {@link AccountStatusTO }
     * 
     */
    public AccountStatusTO createAccountStatusTO() {
        return new AccountStatusTO();
    }

    /**
     * Create an instance of {@link MarketableObjectTypeTO }
     * 
     */
    public MarketableObjectTypeTO createMarketableObjectTypeTO() {
        return new MarketableObjectTypeTO();
    }

    /**
     * Create an instance of {@link ContractInstrumentTypeTO }
     * 
     */
    public ContractInstrumentTypeTO createContractInstrumentTypeTO() {
        return new ContractInstrumentTypeTO();
    }

    /**
     * Create an instance of {@link MonetaryAccountTypeTO }
     * 
     */
    public MonetaryAccountTypeTO createMonetaryAccountTypeTO() {
        return new MonetaryAccountTypeTO();
    }

    /**
     * Create an instance of {@link FundRegistrationTO }
     * 
     */
    public FundRegistrationTO createFundRegistrationTO() {
        return new FundRegistrationTO();
    }

    /**
     * Create an instance of {@link WarrantDerivativeTO }
     * 
     */
    public WarrantDerivativeTO createWarrantDerivativeTO() {
        return new WarrantDerivativeTO();
    }

    /**
     * Create an instance of {@link FundTypeTO }
     * 
     */
    public FundTypeTO createFundTypeTO() {
        return new FundTypeTO();
    }

    /**
     * Create an instance of {@link RoleInAccountTypeTO }
     * 
     */
    public RoleInAccountTypeTO createRoleInAccountTypeTO() {
        return new RoleInAccountTypeTO();
    }

    /**
     * Create an instance of {@link StockMarketTO }
     * 
     */
    public StockMarketTO createStockMarketTO() {
        return new StockMarketTO();
    }

    /**
     * Create an instance of {@link AccountRoleTO }
     * 
     */
    public AccountRoleTO createAccountRoleTO() {
        return new AccountRoleTO();
    }

    /**
     * Create an instance of {@link BondTypeTO }
     * 
     */
    public BondTypeTO createBondTypeTO() {
        return new BondTypeTO();
    }

    /**
     * Create an instance of {@link FinancialAssetTypeTO }
     * 
     */
    public FinancialAssetTypeTO createFinancialAssetTypeTO() {
        return new FinancialAssetTypeTO();
    }

    /**
     * Create an instance of {@link AccountEntryTypeTO }
     * 
     */
    public AccountEntryTypeTO createAccountEntryTypeTO() {
        return new AccountEntryTypeTO();
    }

    /**
     * Create an instance of {@link SecurityAssetRegistrationTO }
     * 
     */
    public SecurityAssetRegistrationTO createSecurityAssetRegistrationTO() {
        return new SecurityAssetRegistrationTO();
    }

    /**
     * Create an instance of {@link AccountRelationshipTypeTO }
     * 
     */
    public AccountRelationshipTypeTO createAccountRelationshipTypeTO() {
        return new AccountRelationshipTypeTO();
    }

    /**
     * Create an instance of {@link ContractInstrumentTO }
     * 
     */
    public ContractInstrumentTO createContractInstrumentTO() {
        return new ContractInstrumentTO();
    }

    /**
     * Create an instance of {@link AccountEntryTO }
     * 
     */
    public AccountEntryTO createAccountEntryTO() {
        return new AccountEntryTO();
    }

    /**
     * Create an instance of {@link SwapTO }
     * 
     */
    public SwapTO createSwapTO() {
        return new SwapTO();
    }

    /**
     * Create an instance of {@link CollateralAssetCategoryTO }
     * 
     */
    public CollateralAssetCategoryTO createCollateralAssetCategoryTO() {
        return new CollateralAssetCategoryTO();
    }

    /**
     * Create an instance of {@link TimeAccountTO }
     * 
     */
    public TimeAccountTO createTimeAccountTO() {
        return new TimeAccountTO();
    }

    /**
     * Create an instance of {@link AssetMarketInformationTO }
     * 
     */
    public AssetMarketInformationTO createAssetMarketInformationTO() {
        return new AssetMarketInformationTO();
    }

    /**
     * Create an instance of {@link FundTO }
     * 
     */
    public FundTO createFundTO() {
        return new FundTO();
    }

    /**
     * Create an instance of {@link FinancialAssetRegistrationTO }
     * 
     */
    public FinancialAssetRegistrationTO createFinancialAssetRegistrationTO() {
        return new FinancialAssetRegistrationTO();
    }

    /**
     * Create an instance of {@link AccountRelationshipTO }
     * 
     */
    public AccountRelationshipTO createAccountRelationshipTO() {
        return new AccountRelationshipTO();
    }

    /**
     * Create an instance of {@link BondTO }
     * 
     */
    public BondTO createBondTO() {
        return new BondTO();
    }

    /**
     * Create an instance of {@link FutureTypeTO }
     * 
     */
    public FutureTypeTO createFutureTypeTO() {
        return new FutureTypeTO();
    }

    /**
     * Create an instance of {@link FutureTO }
     * 
     */
    public FutureTO createFutureTO() {
        return new FutureTO();
    }

    /**
     * Create an instance of {@link InvestmentPortfolioTO }
     * 
     */
    public InvestmentPortfolioTO createInvestmentPortfolioTO() {
        return new InvestmentPortfolioTO();
    }

    /**
     * Create an instance of {@link AssetPriceTO }
     * 
     */
    public AssetPriceTO createAssetPriceTO() {
        return new AssetPriceTO();
    }

    /**
     * Create an instance of {@link AssetPriceQuotationTO }
     * 
     */
    public AssetPriceQuotationTO createAssetPriceQuotationTO() {
        return new AssetPriceQuotationTO();
    }

    /**
     * Create an instance of {@link HedgingCategoryTO }
     * 
     */
    public HedgingCategoryTO createHedgingCategoryTO() {
        return new HedgingCategoryTO();
    }

    /**
     * Create an instance of {@link InvestmentProfileTO }
     * 
     */
    public InvestmentProfileTO createInvestmentProfileTO() {
        return new InvestmentProfileTO();
    }

    /**
     * Create an instance of {@link ReserveAccountTO }
     * 
     */
    public ReserveAccountTO createReserveAccountTO() {
        return new ReserveAccountTO();
    }

    /**
     * Create an instance of {@link DebtInstrumentTypeTO }
     * 
     */
    public DebtInstrumentTypeTO createDebtInstrumentTypeTO() {
        return new DebtInstrumentTypeTO();
    }

    /**
     * Create an instance of {@link AssetFeeTO }
     * 
     */
    public AssetFeeTO createAssetFeeTO() {
        return new AssetFeeTO();
    }

    /**
     * Create an instance of {@link AssetHoldingTO }
     * 
     */
    public AssetHoldingTO createAssetHoldingTO() {
        return new AssetHoldingTO();
    }

    /**
     * Create an instance of {@link RoleInFinancialAssetTypeTO }
     * 
     */
    public RoleInFinancialAssetTypeTO createRoleInFinancialAssetTypeTO() {
        return new RoleInFinancialAssetTypeTO();
    }

    /**
     * Create an instance of {@link RoleInAccountTO }
     * 
     */
    public RoleInAccountTO createRoleInAccountTO() {
        return new RoleInAccountTO();
    }

    /**
     * Create an instance of {@link ReserveAccountTypeTO }
     * 
     */
    public ReserveAccountTypeTO createReserveAccountTypeTO() {
        return new ReserveAccountTypeTO();
    }

    /**
     * Create an instance of {@link DerivativeTO }
     * 
     */
    public DerivativeTO createDerivativeTO() {
        return new DerivativeTO();
    }

    /**
     * Create an instance of {@link FinancialMarketPositionTO }
     * 
     */
    public FinancialMarketPositionTO createFinancialMarketPositionTO() {
        return new FinancialMarketPositionTO();
    }

    /**
     * Create an instance of {@link CompositeInstrumentTypeTO }
     * 
     */
    public CompositeInstrumentTypeTO createCompositeInstrumentTypeTO() {
        return new CompositeInstrumentTypeTO();
    }

    /**
     * Create an instance of {@link DerivativeTypeTO }
     * 
     */
    public DerivativeTypeTO createDerivativeTypeTO() {
        return new DerivativeTypeTO();
    }

    /**
     * Create an instance of {@link SwapTypeTO }
     * 
     */
    public SwapTypeTO createSwapTypeTO() {
        return new SwapTypeTO();
    }

    /**
     * Create an instance of {@link AccountingCategoryTO }
     * 
     */
    public AccountingCategoryTO createAccountingCategoryTO() {
        return new AccountingCategoryTO();
    }

    /**
     * Create an instance of {@link AllocationKeyTO }
     * 
     */
    public AllocationKeyTO createAllocationKeyTO() {
        return new AllocationKeyTO();
    }

    /**
     * Create an instance of {@link ShareTO }
     * 
     */
    public ShareTO createShareTO() {
        return new ShareTO();
    }

    /**
     * Create an instance of {@link OptionTypeTO }
     * 
     */
    public OptionTypeTO createOptionTypeTO() {
        return new OptionTypeTO();
    }

    /**
     * Create an instance of {@link CompositeInstrumentTO }
     * 
     */
    public CompositeInstrumentTO createCompositeInstrumentTO() {
        return new CompositeInstrumentTO();
    }

    /**
     * Create an instance of {@link VirtualPartyTO }
     * 
     */
    public VirtualPartyTO createVirtualPartyTO() {
        return new VirtualPartyTO();
    }

    /**
     * Create an instance of {@link ObjectUsageTO }
     * 
     */
    public ObjectUsageTO createObjectUsageTO() {
        return new ObjectUsageTO();
    }

    /**
     * Create an instance of {@link PublicSafetyTO }
     * 
     */
    public PublicSafetyTO createPublicSafetyTO() {
        return new PublicSafetyTO();
    }

    /**
     * Create an instance of {@link RoleInRolePlayerTO }
     * 
     */
    public RoleInRolePlayerTO createRoleInRolePlayerTO() {
        return new RoleInRolePlayerTO();
    }

    /**
     * Create an instance of {@link CreditRatingTO }
     * 
     */
    public CreditRatingTO createCreditRatingTO() {
        return new CreditRatingTO();
    }

    /**
     * Create an instance of {@link PlaceUsageInGeneralActivityTO }
     * 
     */
    public PlaceUsageInGeneralActivityTO createPlaceUsageInGeneralActivityTO() {
        return new PlaceUsageInGeneralActivityTO();
    }

    /**
     * Create an instance of {@link GeneralActivityTO }
     * 
     */
    public GeneralActivityTO createGeneralActivityTO() {
        return new GeneralActivityTO();
    }

    /**
     * Create an instance of {@link EmploymentPositionAssignmentTO }
     * 
     */
    public EmploymentPositionAssignmentTO createEmploymentPositionAssignmentTO() {
        return new EmploymentPositionAssignmentTO();
    }

    /**
     * Create an instance of {@link IndustryTO }
     * 
     */
    public IndustryTO createIndustryTO() {
        return new IndustryTO();
    }

    /**
     * Create an instance of {@link CustomerStatusTO }
     * 
     */
    public CustomerStatusTO createCustomerStatusTO() {
        return new CustomerStatusTO();
    }

    /**
     * Create an instance of {@link GoalAndNeedTypeTO }
     * 
     */
    public GoalAndNeedTypeTO createGoalAndNeedTypeTO() {
        return new GoalAndNeedTypeTO();
    }

    /**
     * Create an instance of {@link RolePlayerPlaceUsageTO }
     * 
     */
    public RolePlayerPlaceUsageTO createRolePlayerPlaceUsageTO() {
        return new RolePlayerPlaceUsageTO();
    }

    /**
     * Create an instance of {@link ActivityOfDailyLivingCapabilityTO }
     * 
     */
    public ActivityOfDailyLivingCapabilityTO createActivityOfDailyLivingCapabilityTO() {
        return new ActivityOfDailyLivingCapabilityTO();
    }

    /**
     * Create an instance of {@link ProfitabilityScoreTO }
     * 
     */
    public ProfitabilityScoreTO createProfitabilityScoreTO() {
        return new ProfitabilityScoreTO();
    }

    /**
     * Create an instance of {@link RolePlayerRelationshipTO }
     * 
     */
    public RolePlayerRelationshipTO createRolePlayerRelationshipTO() {
        return new RolePlayerRelationshipTO();
    }

    /**
     * Create an instance of {@link OrganisationLevelTO }
     * 
     */
    public OrganisationLevelTO createOrganisationLevelTO() {
        return new OrganisationLevelTO();
    }

    /**
     * Create an instance of {@link RoleInGoalAndNeedTO }
     * 
     */
    public RoleInGoalAndNeedTO createRoleInGoalAndNeedTO() {
        return new RoleInGoalAndNeedTO();
    }

    /**
     * Create an instance of {@link LoyaltyScoreTO }
     * 
     */
    public LoyaltyScoreTO createLoyaltyScoreTO() {
        return new LoyaltyScoreTO();
    }

    /**
     * Create an instance of {@link PartyRoleInLegalActionTypeTO }
     * 
     */
    public PartyRoleInLegalActionTypeTO createPartyRoleInLegalActionTypeTO() {
        return new PartyRoleInLegalActionTypeTO();
    }

    /**
     * Create an instance of {@link PartyNameTO }
     * 
     */
    public PartyNameTO createPartyNameTO() {
        return new PartyNameTO();
    }

    /**
     * Create an instance of {@link OrganisationTO }
     * 
     */
    public OrganisationTO createOrganisationTO() {
        return new OrganisationTO();
    }

    /**
     * Create an instance of {@link DrivingLicenseTO }
     * 
     */
    public DrivingLicenseTO createDrivingLicenseTO() {
        return new DrivingLicenseTO();
    }

    /**
     * Create an instance of {@link EmploymentRoleTypeTO }
     * 
     */
    public EmploymentRoleTypeTO createEmploymentRoleTypeTO() {
        return new EmploymentRoleTypeTO();
    }

    /**
     * Create an instance of {@link ResidenceTO }
     * 
     */
    public ResidenceTO createResidenceTO() {
        return new ResidenceTO();
    }

    /**
     * Create an instance of {@link GeoCodingOptionsTO }
     * 
     */
    public GeoCodingOptionsTO createGeoCodingOptionsTO() {
        return new GeoCodingOptionsTO();
    }

    /**
     * Create an instance of {@link PartyTO }
     * 
     */
    public PartyTO createPartyTO() {
        return new PartyTO();
    }

    /**
     * Create an instance of {@link HumanAugmentationTO }
     * 
     */
    public HumanAugmentationTO createHumanAugmentationTO() {
        return new HumanAugmentationTO();
    }

    /**
     * Create an instance of {@link NationalRegistrationTypeTO }
     * 
     */
    public NationalRegistrationTypeTO createNationalRegistrationTypeTO() {
        return new NationalRegistrationTypeTO();
    }

    /**
     * Create an instance of {@link OrganisationMembershipTO }
     * 
     */
    public OrganisationMembershipTO createOrganisationMembershipTO() {
        return new OrganisationMembershipTO();
    }

    /**
     * Create an instance of {@link SuspectDuplicateAugmentationTO }
     * 
     */
    public SuspectDuplicateAugmentationTO createSuspectDuplicateAugmentationTO() {
        return new SuspectDuplicateAugmentationTO();
    }

    /**
     * Create an instance of {@link FamilyRelationshipTO }
     * 
     */
    public FamilyRelationshipTO createFamilyRelationshipTO() {
        return new FamilyRelationshipTO();
    }

    /**
     * Create an instance of {@link ReportingChainTO }
     * 
     */
    public ReportingChainTO createReportingChainTO() {
        return new ReportingChainTO();
    }

    /**
     * Create an instance of {@link PartyRegistrationTypeTO }
     * 
     */
    public PartyRegistrationTypeTO createPartyRegistrationTypeTO() {
        return new PartyRegistrationTypeTO();
    }

    /**
     * Create an instance of {@link VisualProfileTO }
     * 
     */
    public VisualProfileTO createVisualProfileTO() {
        return new VisualProfileTO();
    }

    /**
     * Create an instance of {@link LinkedPartyTO }
     * 
     */
    public LinkedPartyTO createLinkedPartyTO() {
        return new LinkedPartyTO();
    }

    /**
     * Create an instance of {@link FinancialStressScoreTO }
     * 
     */
    public FinancialStressScoreTO createFinancialStressScoreTO() {
        return new FinancialStressScoreTO();
    }

    /**
     * Create an instance of {@link CommunicationProfileTO }
     * 
     */
    public CommunicationProfileTO createCommunicationProfileTO() {
        return new CommunicationProfileTO();
    }

    /**
     * Create an instance of {@link ContactPreferenceTO }
     * 
     */
    public ContactPreferenceTO createContactPreferenceTO() {
        return new ContactPreferenceTO();
    }

    /**
     * Create an instance of {@link PartyNameTypeTO }
     * 
     */
    public PartyNameTypeTO createPartyNameTypeTO() {
        return new PartyNameTypeTO();
    }

    /**
     * Create an instance of {@link PropensityScoreTO }
     * 
     */
    public PropensityScoreTO createPropensityScoreTO() {
        return new PropensityScoreTO();
    }

    /**
     * Create an instance of {@link RolePlayerRoleTO }
     * 
     */
    public RolePlayerRoleTO createRolePlayerRoleTO() {
        return new RolePlayerRoleTO();
    }

    /**
     * Create an instance of {@link AudioProfileTO }
     * 
     */
    public AudioProfileTO createAudioProfileTO() {
        return new AudioProfileTO();
    }

    /**
     * Create an instance of {@link LifeEventTO }
     * 
     */
    public LifeEventTO createLifeEventTO() {
        return new LifeEventTO();
    }

    /**
     * Create an instance of {@link ContactLimitationTO }
     * 
     */
    public ContactLimitationTO createContactLimitationTO() {
        return new ContactLimitationTO();
    }

    /**
     * Create an instance of {@link NetWorthTO }
     * 
     */
    public NetWorthTO createNetWorthTO() {
        return new NetWorthTO();
    }

    /**
     * Create an instance of {@link RolePlayerPlaceUsageTypeTO }
     * 
     */
    public RolePlayerPlaceUsageTypeTO createRolePlayerPlaceUsageTypeTO() {
        return new RolePlayerPlaceUsageTypeTO();
    }

    /**
     * Create an instance of {@link StructureOccupationTO }
     * 
     */
    public StructureOccupationTO createStructureOccupationTO() {
        return new StructureOccupationTO();
    }

    /**
     * Create an instance of {@link CompanyRegistrationTO }
     * 
     */
    public CompanyRegistrationTO createCompanyRegistrationTO() {
        return new CompanyRegistrationTO();
    }

    /**
     * Create an instance of {@link DeathCertificateTO }
     * 
     */
    public DeathCertificateTO createDeathCertificateTO() {
        return new DeathCertificateTO();
    }

    /**
     * Create an instance of {@link SuspectDuplicateTO }
     * 
     */
    public SuspectDuplicateTO createSuspectDuplicateTO() {
        return new SuspectDuplicateTO();
    }

    /**
     * Create an instance of {@link OrganisationUnitTO }
     * 
     */
    public OrganisationUnitTO createOrganisationUnitTO() {
        return new OrganisationUnitTO();
    }

    /**
     * Create an instance of {@link PartyRoleInContractTO }
     * 
     */
    public PartyRoleInContractTO createPartyRoleInContractTO() {
        return new PartyRoleInContractTO();
    }

    /**
     * Create an instance of {@link PersonNameTO }
     * 
     */
    public PersonNameTO createPersonNameTO() {
        return new PersonNameTO();
    }

    /**
     * Create an instance of {@link BirthCertificateTO }
     * 
     */
    public BirthCertificateTO createBirthCertificateTO() {
        return new BirthCertificateTO();
    }

    /**
     * Create an instance of {@link ScenarioTO }
     * 
     */
    public ScenarioTO createScenarioTO() {
        return new ScenarioTO();
    }

    /**
     * Create an instance of {@link HospitalLicenseTO }
     * 
     */
    public HospitalLicenseTO createHospitalLicenseTO() {
        return new HospitalLicenseTO();
    }

    /**
     * Create an instance of {@link NationalRegistrationTO }
     * 
     */
    public NationalRegistrationTO createNationalRegistrationTO() {
        return new NationalRegistrationTO();
    }

    /**
     * Create an instance of {@link OrganisationTypeTO }
     * 
     */
    public OrganisationTypeTO createOrganisationTypeTO() {
        return new OrganisationTypeTO();
    }

    /**
     * Create an instance of {@link CreditRatingCategoryTO }
     * 
     */
    public CreditRatingCategoryTO createCreditRatingCategoryTO() {
        return new CreditRatingCategoryTO();
    }

    /**
     * Create an instance of {@link EmploymentPositionTO }
     * 
     */
    public EmploymentPositionTO createEmploymentPositionTO() {
        return new EmploymentPositionTO();
    }

    /**
     * Create an instance of {@link OrganisationSizeClassTO }
     * 
     */
    public OrganisationSizeClassTO createOrganisationSizeClassTO() {
        return new OrganisationSizeClassTO();
    }

    /**
     * Create an instance of {@link EmploymentStatusTO }
     * 
     */
    public EmploymentStatusTO createEmploymentStatusTO() {
        return new EmploymentStatusTO();
    }

    /**
     * Create an instance of {@link EmployeeTO }
     * 
     */
    public EmployeeTO createEmployeeTO() {
        return new EmployeeTO();
    }

    /**
     * Create an instance of {@link PostalAddressRangeTO }
     * 
     */
    public PostalAddressRangeTO createPostalAddressRangeTO() {
        return new PostalAddressRangeTO();
    }

    /**
     * Create an instance of {@link OrganisationOwnershipTO }
     * 
     */
    public OrganisationOwnershipTO createOrganisationOwnershipTO() {
        return new OrganisationOwnershipTO();
    }

    /**
     * Create an instance of {@link ElectronicAddressTO }
     * 
     */
    public ElectronicAddressTO createElectronicAddressTO() {
        return new ElectronicAddressTO();
    }

    /**
     * Create an instance of {@link AttitudeTO }
     * 
     */
    public AttitudeTO createAttitudeTO() {
        return new AttitudeTO();
    }

    /**
     * Create an instance of {@link VirtualPartyTypeTO }
     * 
     */
    public VirtualPartyTypeTO createVirtualPartyTypeTO() {
        return new VirtualPartyTypeTO();
    }

    /**
     * Create an instance of {@link TimingPreferenceTO }
     * 
     */
    public TimingPreferenceTO createTimingPreferenceTO() {
        return new TimingPreferenceTO();
    }

    /**
     * Create an instance of {@link UnstructuredNameTO }
     * 
     */
    public UnstructuredNameTO createUnstructuredNameTO() {
        return new UnstructuredNameTO();
    }

    /**
     * Create an instance of {@link EmploymentRoleTO }
     * 
     */
    public EmploymentRoleTO createEmploymentRoleTO() {
        return new EmploymentRoleTO();
    }

    /**
     * Create an instance of {@link CensusStreetSegmentTO }
     * 
     */
    public CensusStreetSegmentTO createCensusStreetSegmentTO() {
        return new CensusStreetSegmentTO();
    }

    /**
     * Create an instance of {@link InsuranceCompanyCategoryTO }
     * 
     */
    public InsuranceCompanyCategoryTO createInsuranceCompanyCategoryTO() {
        return new InsuranceCompanyCategoryTO();
    }

    /**
     * Create an instance of {@link HealthCareProviderRegistrationTO }
     * 
     */
    public HealthCareProviderRegistrationTO createHealthCareProviderRegistrationTO() {
        return new HealthCareProviderRegistrationTO();
    }

    /**
     * Create an instance of {@link CompanyDetailTO }
     * 
     */
    public CompanyDetailTO createCompanyDetailTO() {
        return new CompanyDetailTO();
    }

    /**
     * Create an instance of {@link CanadaGeoCodeMatchingOptionsTO }
     * 
     */
    public CanadaGeoCodeMatchingOptionsTO createCanadaGeoCodeMatchingOptionsTO() {
        return new CanadaGeoCodeMatchingOptionsTO();
    }

    /**
     * Create an instance of {@link ProviderRoleTO }
     * 
     */
    public ProviderRoleTO createProviderRoleTO() {
        return new ProviderRoleTO();
    }

    /**
     * Create an instance of {@link OrganisationStatusTO }
     * 
     */
    public OrganisationStatusTO createOrganisationStatusTO() {
        return new OrganisationStatusTO();
    }

    /**
     * Create an instance of {@link GeneralActivityTypeTO }
     * 
     */
    public GeneralActivityTypeTO createGeneralActivityTypeTO() {
        return new GeneralActivityTypeTO();
    }

    /**
     * Create an instance of {@link PartyScoreTO }
     * 
     */
    public PartyScoreTO createPartyScoreTO() {
        return new PartyScoreTO();
    }

    /**
     * Create an instance of {@link PartyRoleInBankingAgreementTO }
     * 
     */
    public PartyRoleInBankingAgreementTO createPartyRoleInBankingAgreementTO() {
        return new PartyRoleInBankingAgreementTO();
    }

    /**
     * Create an instance of {@link HabitTO }
     * 
     */
    public HabitTO createHabitTO() {
        return new HabitTO();
    }

    /**
     * Create an instance of {@link GeoCodeMatchingOptionsTO }
     * 
     */
    public GeoCodeMatchingOptionsTO createGeoCodeMatchingOptionsTO() {
        return new GeoCodeMatchingOptionsTO();
    }

    /**
     * Create an instance of {@link LifeEventTypeTO }
     * 
     */
    public LifeEventTypeTO createLifeEventTypeTO() {
        return new LifeEventTypeTO();
    }

    /**
     * Create an instance of {@link RoleInGoalAndNeedTypeTO }
     * 
     */
    public RoleInGoalAndNeedTypeTO createRoleInGoalAndNeedTypeTO() {
        return new RoleInGoalAndNeedTypeTO();
    }

    /**
     * Create an instance of {@link CustomerTO }
     * 
     */
    public CustomerTO createCustomerTO() {
        return new CustomerTO();
    }

    /**
     * Create an instance of {@link MobilityProfileTO }
     * 
     */
    public MobilityProfileTO createMobilityProfileTO() {
        return new MobilityProfileTO();
    }

    /**
     * Create an instance of {@link PhysicalObjectUsageTO }
     * 
     */
    public PhysicalObjectUsageTO createPhysicalObjectUsageTO() {
        return new PhysicalObjectUsageTO();
    }

    /**
     * Create an instance of {@link CitizenshipTO }
     * 
     */
    public CitizenshipTO createCitizenshipTO() {
        return new CitizenshipTO();
    }

    /**
     * Create an instance of {@link HouseholdTO }
     * 
     */
    public HouseholdTO createHouseholdTO() {
        return new HouseholdTO();
    }

    /**
     * Create an instance of {@link PartyRoleTO }
     * 
     */
    public PartyRoleTO createPartyRoleTO() {
        return new PartyRoleTO();
    }

    /**
     * Create an instance of {@link DeterministicAugmentationTO }
     * 
     */
    public DeterministicAugmentationTO createDeterministicAugmentationTO() {
        return new DeterministicAugmentationTO();
    }

    /**
     * Create an instance of {@link AssociationTO }
     * 
     */
    public AssociationTO createAssociationTO() {
        return new AssociationTO();
    }

    /**
     * Create an instance of {@link LanguageSkillTO }
     * 
     */
    public LanguageSkillTO createLanguageSkillTO() {
        return new LanguageSkillTO();
    }

    /**
     * Create an instance of {@link RoleInRolePlayerTypeTO }
     * 
     */
    public RoleInRolePlayerTypeTO createRoleInRolePlayerTypeTO() {
        return new RoleInRolePlayerTypeTO();
    }

    /**
     * Create an instance of {@link RolePlayerScoreTO }
     * 
     */
    public RolePlayerScoreTO createRolePlayerScoreTO() {
        return new RolePlayerScoreTO();
    }

    /**
     * Create an instance of {@link ParsedPostalAddressTO }
     * 
     */
    public ParsedPostalAddressTO createParsedPostalAddressTO() {
        return new ParsedPostalAddressTO();
    }

    /**
     * Create an instance of {@link GeoCodeProcessingOptionsTO }
     * 
     */
    public GeoCodeProcessingOptionsTO createGeoCodeProcessingOptionsTO() {
        return new GeoCodeProcessingOptionsTO();
    }

    /**
     * Create an instance of {@link RoleInGeneralActivityTO }
     * 
     */
    public RoleInGeneralActivityTO createRoleInGeneralActivityTO() {
        return new RoleInGeneralActivityTO();
    }

    /**
     * Create an instance of {@link InsuranceRegulationLocationCategoryTO }
     * 
     */
    public InsuranceRegulationLocationCategoryTO createInsuranceRegulationLocationCategoryTO() {
        return new InsuranceRegulationLocationCategoryTO();
    }

    /**
     * Create an instance of {@link ProcessingEngineCategoryTO }
     * 
     */
    public ProcessingEngineCategoryTO createProcessingEngineCategoryTO() {
        return new ProcessingEngineCategoryTO();
    }

    /**
     * Create an instance of {@link PostalAddressTO }
     * 
     */
    public PostalAddressTO createPostalAddressTO() {
        return new PostalAddressTO();
    }

    /**
     * Create an instance of {@link ObjectOwnershipTO }
     * 
     */
    public ObjectOwnershipTO createObjectOwnershipTO() {
        return new ObjectOwnershipTO();
    }

    /**
     * Create an instance of {@link MarriageRegistrationTO }
     * 
     */
    public MarriageRegistrationTO createMarriageRegistrationTO() {
        return new MarriageRegistrationTO();
    }

    /**
     * Create an instance of {@link AlgorithmicAugmentationTO }
     * 
     */
    public AlgorithmicAugmentationTO createAlgorithmicAugmentationTO() {
        return new AlgorithmicAugmentationTO();
    }

    /**
     * Create an instance of {@link ContactPointTO }
     * 
     */
    public ContactPointTO createContactPointTO() {
        return new ContactPointTO();
    }

    /**
     * Create an instance of {@link PartyMatchingEngineCategoryTO }
     * 
     */
    public PartyMatchingEngineCategoryTO createPartyMatchingEngineCategoryTO() {
        return new PartyMatchingEngineCategoryTO();
    }

    /**
     * Create an instance of {@link GeoCodeTO }
     * 
     */
    public GeoCodeTO createGeoCodeTO() {
        return new GeoCodeTO();
    }

    /**
     * Create an instance of {@link RolePlayerRelationshipTypeTO }
     * 
     */
    public RolePlayerRelationshipTypeTO createRolePlayerRelationshipTypeTO() {
        return new RolePlayerRelationshipTypeTO();
    }

    /**
     * Create an instance of {@link SkillTO }
     * 
     */
    public SkillTO createSkillTO() {
        return new SkillTO();
    }

    /**
     * Create an instance of {@link ProbabilisticAugmentationTO }
     * 
     */
    public ProbabilisticAugmentationTO createProbabilisticAugmentationTO() {
        return new ProbabilisticAugmentationTO();
    }

    /**
     * Create an instance of {@link PersonTO }
     * 
     */
    public PersonTO createPersonTO() {
        return new PersonTO();
    }

    /**
     * Create an instance of {@link ProviderRoleTypeTO }
     * 
     */
    public ProviderRoleTypeTO createProviderRoleTypeTO() {
        return new ProviderRoleTypeTO();
    }

    /**
     * Create an instance of {@link MarketSegmentTO }
     * 
     */
    public MarketSegmentTO createMarketSegmentTO() {
        return new MarketSegmentTO();
    }

    /**
     * Create an instance of {@link RolePlayerCategoryTO }
     * 
     */
    public RolePlayerCategoryTO createRolePlayerCategoryTO() {
        return new RolePlayerCategoryTO();
    }

    /**
     * Create an instance of {@link RelationshipStageTO }
     * 
     */
    public RelationshipStageTO createRelationshipStageTO() {
        return new RelationshipStageTO();
    }

    /**
     * Create an instance of {@link CompanyTO }
     * 
     */
    public CompanyTO createCompanyTO() {
        return new CompanyTO();
    }

    /**
     * Create an instance of {@link PartyRoleInLegalActionTO }
     * 
     */
    public PartyRoleInLegalActionTO createPartyRoleInLegalActionTO() {
        return new PartyRoleInLegalActionTO();
    }

    /**
     * Create an instance of {@link MarriageRelationshipTO }
     * 
     */
    public MarriageRelationshipTO createMarriageRelationshipTO() {
        return new MarriageRelationshipTO();
    }

    /**
     * Create an instance of {@link CareOfAddressTO }
     * 
     */
    public CareOfAddressTO createCareOfAddressTO() {
        return new CareOfAddressTO();
    }

    /**
     * Create an instance of {@link GeoCoderStatusTO }
     * 
     */
    public GeoCoderStatusTO createGeoCoderStatusTO() {
        return new GeoCoderStatusTO();
    }

    /**
     * Create an instance of {@link HealthCareProviderRoleTO }
     * 
     */
    public HealthCareProviderRoleTO createHealthCareProviderRoleTO() {
        return new HealthCareProviderRoleTO();
    }

    /**
     * Create an instance of {@link GoalAndNeedTO }
     * 
     */
    public GoalAndNeedTO createGoalAndNeedTO() {
        return new GoalAndNeedTO();
    }

    /**
     * Create an instance of {@link TelephoneNumberTO }
     * 
     */
    public TelephoneNumberTO createTelephoneNumberTO() {
        return new TelephoneNumberTO();
    }

    /**
     * Create an instance of {@link CognitiveProfileTO }
     * 
     */
    public CognitiveProfileTO createCognitiveProfileTO() {
        return new CognitiveProfileTO();
    }

    /**
     * Create an instance of {@link PartyRegistrationTO }
     * 
     */
    public PartyRegistrationTO createPartyRegistrationTO() {
        return new PartyRegistrationTO();
    }

    /**
     * Create an instance of {@link CanadaGeoCodingOptionsTO }
     * 
     */
    public CanadaGeoCodingOptionsTO createCanadaGeoCodingOptionsTO() {
        return new CanadaGeoCodingOptionsTO();
    }

    /**
     * Create an instance of {@link UnstructuredContactPointTO }
     * 
     */
    public UnstructuredContactPointTO createUnstructuredContactPointTO() {
        return new UnstructuredContactPointTO();
    }

    /**
     * Create an instance of {@link OrganisationCategoryTO }
     * 
     */
    public OrganisationCategoryTO createOrganisationCategoryTO() {
        return new OrganisationCategoryTO();
    }

    /**
     * Create an instance of {@link RolePlayerTO }
     * 
     */
    public RolePlayerTO createRolePlayerTO() {
        return new RolePlayerTO();
    }

    /**
     * Create an instance of {@link GoalAndNeedRoleTO }
     * 
     */
    public GoalAndNeedRoleTO createGoalAndNeedRoleTO() {
        return new GoalAndNeedRoleTO();
    }

    /**
     * Create an instance of {@link MarketTO }
     * 
     */
    public MarketTO createMarketTO() {
        return new MarketTO();
    }

    /**
     * Create an instance of {@link PartyRoleInClaimTO }
     * 
     */
    public PartyRoleInClaimTO createPartyRoleInClaimTO() {
        return new PartyRoleInClaimTO();
    }

    /**
     * Create an instance of {@link SatisfactionScoreTO }
     * 
     */
    public SatisfactionScoreTO createSatisfactionScoreTO() {
        return new SatisfactionScoreTO();
    }

    /**
     * Create an instance of {@link OccupationTO }
     * 
     */
    public OccupationTO createOccupationTO() {
        return new OccupationTO();
    }

    /**
     * Create an instance of {@link AffinityGroupTO }
     * 
     */
    public AffinityGroupTO createAffinityGroupTO() {
        return new AffinityGroupTO();
    }

    /**
     * Create an instance of {@link SuspectDuplicateStatusTO }
     * 
     */
    public SuspectDuplicateStatusTO createSuspectDuplicateStatusTO() {
        return new SuspectDuplicateStatusTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZSOAFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "updateSubmissionAndPartszSOAFault")
    public JAXBElement<ZSOAFault> createUpdateSubmissionAndPartszSOAFault(ZSOAFault value) {
        return new JAXBElement<ZSOAFault>(_UpdateSubmissionAndPartszSOAFault_QNAME, ZSOAFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BindPolicyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "bindPolicy")
    public JAXBElement<BindPolicyRequest> createBindPolicy(BindPolicyRequest value) {
        return new JAXBElement<BindPolicyRequest>(_BindPolicy_QNAME, BindPolicyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZSOAFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "createRenewalSubmissionzSOAFault")
    public JAXBElement<ZSOAFault> createCreateRenewalSubmissionzSOAFault(ZSOAFault value) {
        return new JAXBElement<ZSOAFault>(_CreateRenewalSubmissionzSOAFault_QNAME, ZSOAFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecordSubmissionDocumentDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "recordSubmissionDocumentDetailsResponse")
    public JAXBElement<RecordSubmissionDocumentDetailsResponse> createRecordSubmissionDocumentDetailsResponse(RecordSubmissionDocumentDetailsResponse value) {
        return new JAXBElement<RecordSubmissionDocumentDetailsResponse>(_RecordSubmissionDocumentDetailsResponse_QNAME, RecordSubmissionDocumentDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateRenewalSubmissionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "createRenewalSubmissionResponse")
    public JAXBElement<CreateRenewalSubmissionResponse> createCreateRenewalSubmissionResponse(CreateRenewalSubmissionResponse value) {
        return new JAXBElement<CreateRenewalSubmissionResponse>(_CreateRenewalSubmissionResponse_QNAME, CreateRenewalSubmissionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZSOAFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "zSOAFault")
    public JAXBElement<ZSOAFault> createZSOAFault(ZSOAFault value) {
        return new JAXBElement<ZSOAFault>(_ZSOAFault_QNAME, ZSOAFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateRenewalSubmissionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "createRenewalSubmission")
    public JAXBElement<CreateRenewalSubmissionRequest> createCreateRenewalSubmission(CreateRenewalSubmissionRequest value) {
        return new JAXBElement<CreateRenewalSubmissionRequest>(_CreateRenewalSubmission_QNAME, CreateRenewalSubmissionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSubmissionAndPartsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "updateSubmissionAndPartsResponse")
    public JAXBElement<UpdateSubmissionAndPartsResponse> createUpdateSubmissionAndPartsResponse(UpdateSubmissionAndPartsResponse value) {
        return new JAXBElement<UpdateSubmissionAndPartsResponse>(_UpdateSubmissionAndPartsResponse_QNAME, UpdateSubmissionAndPartsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZSOAFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "createSubmissionzSOAFault")
    public JAXBElement<ZSOAFault> createCreateSubmissionzSOAFault(ZSOAFault value) {
        return new JAXBElement<ZSOAFault>(_CreateSubmissionzSOAFault_QNAME, ZSOAFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSubmissionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "createSubmissionResponse")
    public JAXBElement<CreateSubmissionResponse> createCreateSubmissionResponse(CreateSubmissionResponse value) {
        return new JAXBElement<CreateSubmissionResponse>(_CreateSubmissionResponse_QNAME, CreateSubmissionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSubmissionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "createSubmission")
    public JAXBElement<CreateSubmissionRequest> createCreateSubmission(CreateSubmissionRequest value) {
        return new JAXBElement<CreateSubmissionRequest>(_CreateSubmission_QNAME, CreateSubmissionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecordSubmissionDocumentDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "recordSubmissionDocumentDetails")
    public JAXBElement<RecordSubmissionDocumentDetailsRequest> createRecordSubmissionDocumentDetails(RecordSubmissionDocumentDetailsRequest value) {
        return new JAXBElement<RecordSubmissionDocumentDetailsRequest>(_RecordSubmissionDocumentDetails_QNAME, RecordSubmissionDocumentDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSubmissionAndPartsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "updateSubmissionAndParts")
    public JAXBElement<UpdateSubmissionAndPartsRequest> createUpdateSubmissionAndParts(UpdateSubmissionAndPartsRequest value) {
        return new JAXBElement<UpdateSubmissionAndPartsRequest>(_UpdateSubmissionAndParts_QNAME, UpdateSubmissionAndPartsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BindPolicyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "bindPolicyResponse")
    public JAXBElement<BindPolicyResponse> createBindPolicyResponse(BindPolicyResponse value) {
        return new JAXBElement<BindPolicyResponse>(_BindPolicyResponse_QNAME, BindPolicyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZSOAFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/", name = "bindPolicyzSOAFault")
    public JAXBElement<ZSOAFault> createBindPolicyzSOAFault(ZSOAFault value) {
        return new JAXBElement<ZSOAFault>(_BindPolicyzSOAFault_QNAME, ZSOAFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancialServicesAgreementTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", name = "createdFinancialServicesAgreement", scope = NewBusinessRequestTO.class)
    public JAXBElement<FinancialServicesAgreementTO> createNewBusinessRequestTOCreatedFinancialServicesAgreement(FinancialServicesAgreementTO value) {
        return new JAXBElement<FinancialServicesAgreementTO>(_NewBusinessRequestTOCreatedFinancialServicesAgreement_QNAME, FinancialServicesAgreementTO.class, NewBusinessRequestTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MarketableProductTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", name = "targetProduct", scope = NewBusinessRequestTO.class)
    public JAXBElement<MarketableProductTO> createNewBusinessRequestTOTargetProduct(MarketableProductTO value) {
        return new JAXBElement<MarketableProductTO>(_NewBusinessRequestTOTargetProduct_QNAME, MarketableProductTO.class, NewBusinessRequestTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", name = "requestedExpirationDate", scope = NewBusinessRequestTO.class)
    public JAXBElement<XMLGregorianCalendar> createNewBusinessRequestTORequestedExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_NewBusinessRequestTORequestedExpirationDate_QNAME, XMLGregorianCalendar.class, NewBusinessRequestTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/", name = "timeStamp", scope = ZSOAFault.class)
    public JAXBElement<XMLGregorianCalendar> createZSOAFaultTimeStamp(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ZSOAFaultTimeStamp_QNAME, XMLGregorianCalendar.class, ZSOAFault.class, value);
    }

}
