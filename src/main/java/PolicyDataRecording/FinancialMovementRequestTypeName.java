
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialMovementRequestTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FinancialMovementRequestTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Repayment Request"/>
 *     &lt;enumeration value="Advance Request"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FinancialMovementRequestTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum FinancialMovementRequestTypeName {

    @XmlEnumValue("Repayment Request")
    REPAYMENT_REQUEST("Repayment Request"),
    @XmlEnumValue("Advance Request")
    ADVANCE_REQUEST("Advance Request");
    private final String value;

    FinancialMovementRequestTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FinancialMovementRequestTypeName fromValue(String v) {
        for (FinancialMovementRequestTypeName c: FinancialMovementRequestTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
