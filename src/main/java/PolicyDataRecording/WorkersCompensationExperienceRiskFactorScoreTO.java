
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkersCompensationExperienceRiskFactorScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkersCompensationExperienceRiskFactorScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskFactorScore_TO">
 *       &lt;sequence>
 *         &lt;element name="assignedRiskAdjustmentProgramFactor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="massachusettsAllRiskAdjustmentProgramFactor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="simplifiedAssignedRiskAdjustmentProgramFactor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="contingent" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="floridaAssignedRiskAdjustmentProgramFactor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkersCompensationExperienceRiskFactorScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "assignedRiskAdjustmentProgramFactor",
    "massachusettsAllRiskAdjustmentProgramFactor",
    "simplifiedAssignedRiskAdjustmentProgramFactor",
    "contingent",
    "floridaAssignedRiskAdjustmentProgramFactor"
})
public class WorkersCompensationExperienceRiskFactorScoreTO
    extends RiskFactorScoreTO
{

    protected BigDecimal assignedRiskAdjustmentProgramFactor;
    protected BigDecimal massachusettsAllRiskAdjustmentProgramFactor;
    protected BigDecimal simplifiedAssignedRiskAdjustmentProgramFactor;
    protected Boolean contingent;
    protected BigDecimal floridaAssignedRiskAdjustmentProgramFactor;

    /**
     * Gets the value of the assignedRiskAdjustmentProgramFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAssignedRiskAdjustmentProgramFactor() {
        return assignedRiskAdjustmentProgramFactor;
    }

    /**
     * Sets the value of the assignedRiskAdjustmentProgramFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAssignedRiskAdjustmentProgramFactor(BigDecimal value) {
        this.assignedRiskAdjustmentProgramFactor = value;
    }

    /**
     * Gets the value of the massachusettsAllRiskAdjustmentProgramFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMassachusettsAllRiskAdjustmentProgramFactor() {
        return massachusettsAllRiskAdjustmentProgramFactor;
    }

    /**
     * Sets the value of the massachusettsAllRiskAdjustmentProgramFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMassachusettsAllRiskAdjustmentProgramFactor(BigDecimal value) {
        this.massachusettsAllRiskAdjustmentProgramFactor = value;
    }

    /**
     * Gets the value of the simplifiedAssignedRiskAdjustmentProgramFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSimplifiedAssignedRiskAdjustmentProgramFactor() {
        return simplifiedAssignedRiskAdjustmentProgramFactor;
    }

    /**
     * Sets the value of the simplifiedAssignedRiskAdjustmentProgramFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSimplifiedAssignedRiskAdjustmentProgramFactor(BigDecimal value) {
        this.simplifiedAssignedRiskAdjustmentProgramFactor = value;
    }

    /**
     * Gets the value of the contingent property.
     * This getter has been renamed from isContingent() to getContingent() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getContingent() {
        return contingent;
    }

    /**
     * Sets the value of the contingent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContingent(Boolean value) {
        this.contingent = value;
    }

    /**
     * Gets the value of the floridaAssignedRiskAdjustmentProgramFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFloridaAssignedRiskAdjustmentProgramFactor() {
        return floridaAssignedRiskAdjustmentProgramFactor;
    }

    /**
     * Sets the value of the floridaAssignedRiskAdjustmentProgramFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFloridaAssignedRiskAdjustmentProgramFactor(BigDecimal value) {
        this.floridaAssignedRiskAdjustmentProgramFactor = value;
    }

}
