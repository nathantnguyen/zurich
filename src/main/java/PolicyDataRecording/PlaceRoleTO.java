
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlaceRole_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PlaceRole_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RolePlayerClassRole_TO">
 *       &lt;sequence>
 *         &lt;element name="rolePlayerPlace" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlaceRole_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "rolePlayerPlace"
})
public class PlaceRoleTO
    extends RolePlayerClassRoleTO
{

    protected PlaceTO rolePlayerPlace;

    /**
     * Gets the value of the rolePlayerPlace property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getRolePlayerPlace() {
        return rolePlayerPlace;
    }

    /**
     * Sets the value of the rolePlayerPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setRolePlayerPlace(PlaceTO value) {
        this.rolePlayerPlace = value;
    }

}
