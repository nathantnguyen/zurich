
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInAccountTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInAccountTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Account Ownership"/>
 *     &lt;enumeration value="Account Risk Assessment"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInAccountTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum RoleInAccountTypeName {

    @XmlEnumValue("Account Ownership")
    ACCOUNT_OWNERSHIP("Account Ownership"),
    @XmlEnumValue("Account Risk Assessment")
    ACCOUNT_RISK_ASSESSMENT("Account Risk Assessment");
    private final String value;

    RoleInAccountTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInAccountTypeName fromValue(String v) {
        for (RoleInAccountTypeName c: RoleInAccountTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
