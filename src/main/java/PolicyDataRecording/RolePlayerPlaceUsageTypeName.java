
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RolePlayerPlaceUsageTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RolePlayerPlaceUsageTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Birth Place"/>
 *     &lt;enumeration value="Work Place"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RolePlayerPlaceUsageTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum RolePlayerPlaceUsageTypeName {

    @XmlEnumValue("Birth Place")
    BIRTH_PLACE("Birth Place"),
    @XmlEnumValue("Work Place")
    WORK_PLACE("Work Place");
    private final String value;

    RolePlayerPlaceUsageTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RolePlayerPlaceUsageTypeName fromValue(String v) {
        for (RolePlayerPlaceUsageTypeName c: RolePlayerPlaceUsageTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
