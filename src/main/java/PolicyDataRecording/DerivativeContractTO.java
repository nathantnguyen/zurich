
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DerivativeContract_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DerivativeContract_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}TopLevelFinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="underlyingAsset" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" minOccurs="0"/>
 *         &lt;element name="underlyingDerivatives" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Derivative_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DerivativeContract_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "underlyingAsset",
    "underlyingDerivatives"
})
public class DerivativeContractTO
    extends TopLevelFinancialServicesAgreementTO
{

    protected FinancialAssetTO underlyingAsset;
    protected List<DerivativeTO> underlyingDerivatives;

    /**
     * Gets the value of the underlyingAsset property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAssetTO }
     *     
     */
    public FinancialAssetTO getUnderlyingAsset() {
        return underlyingAsset;
    }

    /**
     * Sets the value of the underlyingAsset property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setUnderlyingAsset(FinancialAssetTO value) {
        this.underlyingAsset = value;
    }

    /**
     * Gets the value of the underlyingDerivatives property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the underlyingDerivatives property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnderlyingDerivatives().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DerivativeTO }
     * 
     * 
     */
    public List<DerivativeTO> getUnderlyingDerivatives() {
        if (underlyingDerivatives == null) {
            underlyingDerivatives = new ArrayList<DerivativeTO>();
        }
        return this.underlyingDerivatives;
    }

    /**
     * Sets the value of the underlyingDerivatives property.
     * 
     * @param underlyingDerivatives
     *     allowed object is
     *     {@link DerivativeTO }
     *     
     */
    public void setUnderlyingDerivatives(List<DerivativeTO> underlyingDerivatives) {
        this.underlyingDerivatives = underlyingDerivatives;
    }

}
