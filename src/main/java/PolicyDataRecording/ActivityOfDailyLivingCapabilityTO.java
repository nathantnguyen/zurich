
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityOfDailyLivingCapability_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityOfDailyLivingCapability_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInGeneralActivity_TO">
 *       &lt;sequence>
 *         &lt;element name="capabilityLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}ActivityOfDailyLivingCapabilityLevel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityOfDailyLivingCapability_TO", propOrder = {
    "capabilityLevel"
})
public class ActivityOfDailyLivingCapabilityTO
    extends RoleInGeneralActivityTO
{

    protected ActivityOfDailyLivingCapabilityLevel capabilityLevel;

    /**
     * Gets the value of the capabilityLevel property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityOfDailyLivingCapabilityLevel }
     *     
     */
    public ActivityOfDailyLivingCapabilityLevel getCapabilityLevel() {
        return capabilityLevel;
    }

    /**
     * Sets the value of the capabilityLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityOfDailyLivingCapabilityLevel }
     *     
     */
    public void setCapabilityLevel(ActivityOfDailyLivingCapabilityLevel value) {
        this.capabilityLevel = value;
    }

}
