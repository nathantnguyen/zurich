
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInRolePlayer_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInRolePlayer_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="roleInRolePlayerRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInRolePlayerType_TO" minOccurs="0"/>
 *         &lt;element name="contextRolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="activityOccurrences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInRolePlayer_TO", propOrder = {
    "roleInRolePlayerRootType",
    "contextRolePlayer",
    "activityOccurrences"
})
@XmlSeeAlso({
    ObjectUsageTO.class,
    RolePlayerPlaceUsageTO.class,
    StructureOccupationTO.class,
    ObjectOwnershipTO.class
})
public class RoleInRolePlayerTO
    extends RoleInContextClassTO
{

    protected RoleInRolePlayerTypeTO roleInRolePlayerRootType;
    protected List<RolePlayerTO> contextRolePlayer;
    protected List<ActivityOccurrenceTO> activityOccurrences;

    /**
     * Gets the value of the roleInRolePlayerRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInRolePlayerTypeTO }
     *     
     */
    public RoleInRolePlayerTypeTO getRoleInRolePlayerRootType() {
        return roleInRolePlayerRootType;
    }

    /**
     * Sets the value of the roleInRolePlayerRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInRolePlayerTypeTO }
     *     
     */
    public void setRoleInRolePlayerRootType(RoleInRolePlayerTypeTO value) {
        this.roleInRolePlayerRootType = value;
    }

    /**
     * Gets the value of the contextRolePlayer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contextRolePlayer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContextRolePlayer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerTO }
     * 
     * 
     */
    public List<RolePlayerTO> getContextRolePlayer() {
        if (contextRolePlayer == null) {
            contextRolePlayer = new ArrayList<RolePlayerTO>();
        }
        return this.contextRolePlayer;
    }

    /**
     * Gets the value of the activityOccurrences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activityOccurrences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivityOccurrences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceTO }
     * 
     * 
     */
    public List<ActivityOccurrenceTO> getActivityOccurrences() {
        if (activityOccurrences == null) {
            activityOccurrences = new ArrayList<ActivityOccurrenceTO>();
        }
        return this.activityOccurrences;
    }

    /**
     * Sets the value of the contextRolePlayer property.
     * 
     * @param contextRolePlayer
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setContextRolePlayer(List<RolePlayerTO> contextRolePlayer) {
        this.contextRolePlayer = contextRolePlayer;
    }

    /**
     * Sets the value of the activityOccurrences property.
     * 
     * @param activityOccurrences
     *     allowed object is
     *     {@link ActivityOccurrenceTO }
     *     
     */
    public void setActivityOccurrences(List<ActivityOccurrenceTO> activityOccurrences) {
        this.activityOccurrences = activityOccurrences;
    }

}
