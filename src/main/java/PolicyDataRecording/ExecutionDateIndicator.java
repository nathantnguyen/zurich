
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExecutionDateIndicator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ExecutionDateIndicator">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Next Working Day"/>
 *     &lt;enumeration value="Previous Working Day"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ExecutionDateIndicator", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum ExecutionDateIndicator {

    @XmlEnumValue("Next Working Day")
    NEXT_WORKING_DAY("Next Working Day"),
    @XmlEnumValue("Previous Working Day")
    PREVIOUS_WORKING_DAY("Previous Working Day");
    private final String value;

    ExecutionDateIndicator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExecutionDateIndicator fromValue(String v) {
        for (ExecutionDateIndicator c: ExecutionDateIndicator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
