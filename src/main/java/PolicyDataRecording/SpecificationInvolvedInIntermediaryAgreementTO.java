
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SpecificationInvolvedInIntermediaryAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpecificationInvolvedInIntermediaryAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}RoleInIntermediaryAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="specification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Specification_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecificationInvolvedInIntermediaryAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/", propOrder = {
    "specification"
})
public class SpecificationInvolvedInIntermediaryAgreementTO
    extends RoleInIntermediaryAgreementTO
{

    protected SpecificationTO specification;

    /**
     * Gets the value of the specification property.
     * 
     * @return
     *     possible object is
     *     {@link SpecificationTO }
     *     
     */
    public SpecificationTO getSpecification() {
        return specification;
    }

    /**
     * Sets the value of the specification property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificationTO }
     *     
     */
    public void setSpecification(SpecificationTO value) {
        this.specification = value;
    }

}
