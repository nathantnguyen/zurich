
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountEntryTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountEntryTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Time Account Detail"/>
 *     &lt;enumeration value="Asset Holding Detail"/>
 *     &lt;enumeration value="Monetary Account Detail"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AccountEntryTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum AccountEntryTypeName {

    @XmlEnumValue("Time Account Detail")
    TIME_ACCOUNT_DETAIL("Time Account Detail"),
    @XmlEnumValue("Asset Holding Detail")
    ASSET_HOLDING_DETAIL("Asset Holding Detail"),
    @XmlEnumValue("Monetary Account Detail")
    MONETARY_ACCOUNT_DETAIL("Monetary Account Detail");
    private final String value;

    AccountEntryTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountEntryTypeName fromValue(String v) {
        for (AccountEntryTypeName c: AccountEntryTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
