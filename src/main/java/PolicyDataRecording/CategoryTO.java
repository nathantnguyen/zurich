
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Category_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Category_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="onlyRuleDriven" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="maximumSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="refreshDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="isRuleDriven" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="parentCategoryScheme" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}CategoryScheme_TO" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="categorisedObjects" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}CategorisableObject_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="startDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Category_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "name",
    "description",
    "onlyRuleDriven",
    "maximumSize",
    "refreshDate",
    "externalReference",
    "startDate",
    "endDate",
    "isRuleDriven",
    "parentCategoryScheme",
    "alternateReference",
    "categorisedObjects",
    "startDateTime"
})
@XmlSeeAlso({
    CommunicationCategoryTO.class,
    ProductGroupTO.class,
    AssessmentScenarioTO.class,
    EventCategoryTO.class,
    ActivityCategoryTO.class,
    PlaceCategoryTO.class,
    PredictiveModelScoreElementPartGroupTO.class,
    AgreementCollectionTO.class,
    CoverageGroupTO.class,
    CalculationSpecificationCategoryTO.class,
    CashFlowCategoryTO.class,
    AssetCategoryTO.class,
    HedgingCategoryTO.class,
    AccountingCategoryTO.class,
    CreditRatingCategoryTO.class,
    ProcessingEngineCategoryTO.class,
    MarketSegmentTO.class,
    RolePlayerCategoryTO.class
})
public class CategoryTO
    extends BaseTransferObject
{

    protected String name;
    protected String description;
    protected Boolean onlyRuleDriven;
    protected BigInteger maximumSize;
    protected XMLGregorianCalendar refreshDate;
    protected String externalReference;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected Boolean isRuleDriven;
    protected CategorySchemeTO parentCategoryScheme;
    protected List<AlternateIdTO> alternateReference;
    protected List<CategorisableObjectTO> categorisedObjects;
    protected XMLGregorianCalendar startDateTime;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the onlyRuleDriven property.
     * This getter has been renamed from isOnlyRuleDriven() to getOnlyRuleDriven() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOnlyRuleDriven() {
        return onlyRuleDriven;
    }

    /**
     * Sets the value of the onlyRuleDriven property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOnlyRuleDriven(Boolean value) {
        this.onlyRuleDriven = value;
    }

    /**
     * Gets the value of the maximumSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumSize() {
        return maximumSize;
    }

    /**
     * Sets the value of the maximumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumSize(BigInteger value) {
        this.maximumSize = value;
    }

    /**
     * Gets the value of the refreshDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRefreshDate() {
        return refreshDate;
    }

    /**
     * Sets the value of the refreshDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRefreshDate(XMLGregorianCalendar value) {
        this.refreshDate = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the isRuleDriven property.
     * This getter has been renamed from isIsRuleDriven() to getIsRuleDriven() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsRuleDriven() {
        return isRuleDriven;
    }

    /**
     * Sets the value of the isRuleDriven property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRuleDriven(Boolean value) {
        this.isRuleDriven = value;
    }

    /**
     * Gets the value of the parentCategoryScheme property.
     * 
     * @return
     *     possible object is
     *     {@link CategorySchemeTO }
     *     
     */
    public CategorySchemeTO getParentCategoryScheme() {
        return parentCategoryScheme;
    }

    /**
     * Sets the value of the parentCategoryScheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategorySchemeTO }
     *     
     */
    public void setParentCategoryScheme(CategorySchemeTO value) {
        this.parentCategoryScheme = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Gets the value of the categorisedObjects property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the categorisedObjects property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategorisedObjects().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategorisableObjectTO }
     * 
     * 
     */
    public List<CategorisableObjectTO> getCategorisedObjects() {
        if (categorisedObjects == null) {
            categorisedObjects = new ArrayList<CategorisableObjectTO>();
        }
        return this.categorisedObjects;
    }

    /**
     * Gets the value of the startDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDateTime() {
        return startDateTime;
    }

    /**
     * Sets the value of the startDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDateTime(XMLGregorianCalendar value) {
        this.startDateTime = value;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

    /**
     * Sets the value of the categorisedObjects property.
     * 
     * @param categorisedObjects
     *     allowed object is
     *     {@link CategorisableObjectTO }
     *     
     */
    public void setCategorisedObjects(List<CategorisableObjectTO> categorisedObjects) {
        this.categorisedObjects = categorisedObjects;
    }

}
