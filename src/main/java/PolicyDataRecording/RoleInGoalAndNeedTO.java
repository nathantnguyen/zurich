
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInGoalAndNeed_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInGoalAndNeed_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="contextGoalAndNeed" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GoalAndNeed_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInGoalAndNeed_TO", propOrder = {
    "contextGoalAndNeed"
})
public class RoleInGoalAndNeedTO
    extends RoleInContextClassTO
{

    protected GoalAndNeedTO contextGoalAndNeed;

    /**
     * Gets the value of the contextGoalAndNeed property.
     * 
     * @return
     *     possible object is
     *     {@link GoalAndNeedTO }
     *     
     */
    public GoalAndNeedTO getContextGoalAndNeed() {
        return contextGoalAndNeed;
    }

    /**
     * Sets the value of the contextGoalAndNeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link GoalAndNeedTO }
     *     
     */
    public void setContextGoalAndNeed(GoalAndNeedTO value) {
        this.contextGoalAndNeed = value;
    }

}
