
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialValuation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialValuation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO">
 *       &lt;sequence>
 *         &lt;element name="valueRangeStart" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="financialValuationRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}FinancialValuationType_TO" minOccurs="0"/>
 *         &lt;element name="valueRangeEnd" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="financialValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="appliedScores" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="aggregatedFinancialValuations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}FinancialValuation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="aggregateFinancialValuations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}FinancialValuation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subjectContractreference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="valuedFinancialServicesAgreementReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="financialDataSource" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialValuation_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "valueRangeStart",
    "financialValuationRootType",
    "valueRangeEnd",
    "financialValue",
    "appliedScores",
    "aggregatedFinancialValuations",
    "aggregateFinancialValuations",
    "subjectContractreference",
    "valuedFinancialServicesAgreementReference",
    "financialDataSource"
})
@XmlSeeAlso({
    RiskPositionTO.class,
    NetWorthTO.class
})
public class FinancialValuationTO
    extends AssessmentResultTO
{

    protected BaseCurrencyAmount valueRangeStart;
    protected FinancialValuationTypeTO financialValuationRootType;
    protected BaseCurrencyAmount valueRangeEnd;
    protected BaseCurrencyAmount financialValue;
    protected List<ScoreTO> appliedScores;
    protected List<FinancialValuationTO> aggregatedFinancialValuations;
    protected List<FinancialValuationTO> aggregateFinancialValuations;
    protected ObjectReferenceTO subjectContractreference;
    protected ObjectReferenceTO valuedFinancialServicesAgreementReference;
    protected String financialDataSource;

    /**
     * Gets the value of the valueRangeStart property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getValueRangeStart() {
        return valueRangeStart;
    }

    /**
     * Sets the value of the valueRangeStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setValueRangeStart(BaseCurrencyAmount value) {
        this.valueRangeStart = value;
    }

    /**
     * Gets the value of the financialValuationRootType property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialValuationTypeTO }
     *     
     */
    public FinancialValuationTypeTO getFinancialValuationRootType() {
        return financialValuationRootType;
    }

    /**
     * Sets the value of the financialValuationRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialValuationTypeTO }
     *     
     */
    public void setFinancialValuationRootType(FinancialValuationTypeTO value) {
        this.financialValuationRootType = value;
    }

    /**
     * Gets the value of the valueRangeEnd property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getValueRangeEnd() {
        return valueRangeEnd;
    }

    /**
     * Sets the value of the valueRangeEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setValueRangeEnd(BaseCurrencyAmount value) {
        this.valueRangeEnd = value;
    }

    /**
     * Gets the value of the financialValue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getFinancialValue() {
        return financialValue;
    }

    /**
     * Sets the value of the financialValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setFinancialValue(BaseCurrencyAmount value) {
        this.financialValue = value;
    }

    /**
     * Gets the value of the appliedScores property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appliedScores property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppliedScores().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScoreTO }
     * 
     * 
     */
    public List<ScoreTO> getAppliedScores() {
        if (appliedScores == null) {
            appliedScores = new ArrayList<ScoreTO>();
        }
        return this.appliedScores;
    }

    /**
     * Gets the value of the aggregatedFinancialValuations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aggregatedFinancialValuations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAggregatedFinancialValuations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialValuationTO }
     * 
     * 
     */
    public List<FinancialValuationTO> getAggregatedFinancialValuations() {
        if (aggregatedFinancialValuations == null) {
            aggregatedFinancialValuations = new ArrayList<FinancialValuationTO>();
        }
        return this.aggregatedFinancialValuations;
    }

    /**
     * Gets the value of the aggregateFinancialValuations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aggregateFinancialValuations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAggregateFinancialValuations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialValuationTO }
     * 
     * 
     */
    public List<FinancialValuationTO> getAggregateFinancialValuations() {
        if (aggregateFinancialValuations == null) {
            aggregateFinancialValuations = new ArrayList<FinancialValuationTO>();
        }
        return this.aggregateFinancialValuations;
    }

    /**
     * Gets the value of the subjectContractreference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getSubjectContractreference() {
        return subjectContractreference;
    }

    /**
     * Sets the value of the subjectContractreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSubjectContractreference(ObjectReferenceTO value) {
        this.subjectContractreference = value;
    }

    /**
     * Gets the value of the valuedFinancialServicesAgreementReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getValuedFinancialServicesAgreementReference() {
        return valuedFinancialServicesAgreementReference;
    }

    /**
     * Sets the value of the valuedFinancialServicesAgreementReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setValuedFinancialServicesAgreementReference(ObjectReferenceTO value) {
        this.valuedFinancialServicesAgreementReference = value;
    }

    /**
     * Gets the value of the financialDataSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancialDataSource() {
        return financialDataSource;
    }

    /**
     * Sets the value of the financialDataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancialDataSource(String value) {
        this.financialDataSource = value;
    }

    /**
     * Sets the value of the appliedScores property.
     * 
     * @param appliedScores
     *     allowed object is
     *     {@link ScoreTO }
     *     
     */
    public void setAppliedScores(List<ScoreTO> appliedScores) {
        this.appliedScores = appliedScores;
    }

    /**
     * Sets the value of the aggregatedFinancialValuations property.
     * 
     * @param aggregatedFinancialValuations
     *     allowed object is
     *     {@link FinancialValuationTO }
     *     
     */
    public void setAggregatedFinancialValuations(List<FinancialValuationTO> aggregatedFinancialValuations) {
        this.aggregatedFinancialValuations = aggregatedFinancialValuations;
    }

    /**
     * Sets the value of the aggregateFinancialValuations property.
     * 
     * @param aggregateFinancialValuations
     *     allowed object is
     *     {@link FinancialValuationTO }
     *     
     */
    public void setAggregateFinancialValuations(List<FinancialValuationTO> aggregateFinancialValuations) {
        this.aggregateFinancialValuations = aggregateFinancialValuations;
    }

}
