
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectGroupType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectGroupType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}ObjectGroupTypeName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectGroupType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "name"
})
public class ObjectGroupTypeTO
    extends TypeTO
{

    protected ObjectGroupTypeName name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectGroupTypeName }
     *     
     */
    public ObjectGroupTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectGroupTypeName }
     *     
     */
    public void setName(ObjectGroupTypeName value) {
        this.name = value;
    }

}
