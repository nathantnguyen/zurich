
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MoneyProvisionElement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyProvisionElement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="baseAmount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexableCurrencyAmount_TO" minOccurs="0"/>
 *         &lt;element name="moneyProvisionElementRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElementType_TO" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="advanceArrearsIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/}AdvanceArrearsIndicator" minOccurs="0"/>
 *         &lt;element name="isEstimate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="moneyProvisionElementParts" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElementPart_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="paymentCondition" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessRule_TO" minOccurs="0"/>
 *         &lt;element name="adjustments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}MoneyProvisionDeterminer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="baseAmountAsString" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyProvisionElement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "baseAmount",
    "moneyProvisionElementRootType",
    "startDate",
    "endDate",
    "advanceArrearsIndicator",
    "isEstimate",
    "moneyProvisionElementParts",
    "paymentCondition",
    "adjustments",
    "description",
    "kind",
    "baseAmountAsString"
})
@XmlSeeAlso({
    SingleCashFlowTO.class,
    SeriesOfCashFlowsTO.class
})
public class MoneyProvisionElementTO
    extends DependentObjectTO
{

    protected IndexableCurrencyAmountTO baseAmount;
    protected MoneyProvisionElementTypeTO moneyProvisionElementRootType;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected AdvanceArrearsIndicator advanceArrearsIndicator;
    protected Boolean isEstimate;
    protected List<MoneyProvisionElementPartTO> moneyProvisionElementParts;
    protected BusinessRuleTO paymentCondition;
    protected List<MoneyProvisionDeterminerTO> adjustments;
    protected String description;
    protected String kind;
    protected String baseAmountAsString;

    /**
     * Gets the value of the baseAmount property.
     * 
     * @return
     *     possible object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public IndexableCurrencyAmountTO getBaseAmount() {
        return baseAmount;
    }

    /**
     * Sets the value of the baseAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public void setBaseAmount(IndexableCurrencyAmountTO value) {
        this.baseAmount = value;
    }

    /**
     * Gets the value of the moneyProvisionElementRootType property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionElementTypeTO }
     *     
     */
    public MoneyProvisionElementTypeTO getMoneyProvisionElementRootType() {
        return moneyProvisionElementRootType;
    }

    /**
     * Sets the value of the moneyProvisionElementRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionElementTypeTO }
     *     
     */
    public void setMoneyProvisionElementRootType(MoneyProvisionElementTypeTO value) {
        this.moneyProvisionElementRootType = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the advanceArrearsIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link AdvanceArrearsIndicator }
     *     
     */
    public AdvanceArrearsIndicator getAdvanceArrearsIndicator() {
        return advanceArrearsIndicator;
    }

    /**
     * Sets the value of the advanceArrearsIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdvanceArrearsIndicator }
     *     
     */
    public void setAdvanceArrearsIndicator(AdvanceArrearsIndicator value) {
        this.advanceArrearsIndicator = value;
    }

    /**
     * Gets the value of the isEstimate property.
     * This getter has been renamed from isIsEstimate() to getIsEstimate() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsEstimate() {
        return isEstimate;
    }

    /**
     * Sets the value of the isEstimate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsEstimate(Boolean value) {
        this.isEstimate = value;
    }

    /**
     * Gets the value of the moneyProvisionElementParts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moneyProvisionElementParts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoneyProvisionElementParts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionElementPartTO }
     * 
     * 
     */
    public List<MoneyProvisionElementPartTO> getMoneyProvisionElementParts() {
        if (moneyProvisionElementParts == null) {
            moneyProvisionElementParts = new ArrayList<MoneyProvisionElementPartTO>();
        }
        return this.moneyProvisionElementParts;
    }

    /**
     * Gets the value of the paymentCondition property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessRuleTO }
     *     
     */
    public BusinessRuleTO getPaymentCondition() {
        return paymentCondition;
    }

    /**
     * Sets the value of the paymentCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessRuleTO }
     *     
     */
    public void setPaymentCondition(BusinessRuleTO value) {
        this.paymentCondition = value;
    }

    /**
     * Gets the value of the adjustments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjustments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjustments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionDeterminerTO }
     * 
     * 
     */
    public List<MoneyProvisionDeterminerTO> getAdjustments() {
        if (adjustments == null) {
            adjustments = new ArrayList<MoneyProvisionDeterminerTO>();
        }
        return this.adjustments;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

    /**
     * Gets the value of the baseAmountAsString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseAmountAsString() {
        return baseAmountAsString;
    }

    /**
     * Sets the value of the baseAmountAsString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseAmountAsString(String value) {
        this.baseAmountAsString = value;
    }

    /**
     * Sets the value of the moneyProvisionElementParts property.
     * 
     * @param moneyProvisionElementParts
     *     allowed object is
     *     {@link MoneyProvisionElementPartTO }
     *     
     */
    public void setMoneyProvisionElementParts(List<MoneyProvisionElementPartTO> moneyProvisionElementParts) {
        this.moneyProvisionElementParts = moneyProvisionElementParts;
    }

    /**
     * Sets the value of the adjustments property.
     * 
     * @param adjustments
     *     allowed object is
     *     {@link MoneyProvisionDeterminerTO }
     *     
     */
    public void setAdjustments(List<MoneyProvisionDeterminerTO> adjustments) {
        this.adjustments = adjustments;
    }

}
