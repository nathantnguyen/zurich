
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssessmentActivityTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssessmentActivityTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Medical Examination"/>
 *     &lt;enumeration value="Claim Fraud Investigation"/>
 *     &lt;enumeration value="Diagnosis"/>
 *     &lt;enumeration value="Underwriting"/>
 *     &lt;enumeration value="Information Quality Assessment"/>
 *     &lt;enumeration value="Suspect Duplicate Processing"/>
 *     &lt;enumeration value="Market Research And Analysis"/>
 *     &lt;enumeration value="Control Activity"/>
 *     &lt;enumeration value="Internal Control Activity"/>
 *     &lt;enumeration value="Actuarial Control Activity"/>
 *     &lt;enumeration value="Supervisor Control Activity"/>
 *     &lt;enumeration value="Monitoring Activity"/>
 *     &lt;enumeration value="Risk Agreement Compare"/>
 *     &lt;enumeration value="Risk Identification Activity"/>
 *     &lt;enumeration value="Risk Prioritisation"/>
 *     &lt;enumeration value="Risk Profile Deviation Assessment"/>
 *     &lt;enumeration value="Investment Analysis"/>
 *     &lt;enumeration value="Party Identification Activity"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssessmentActivityTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum AssessmentActivityTypeName {

    @XmlEnumValue("Medical Examination")
    MEDICAL_EXAMINATION("Medical Examination"),
    @XmlEnumValue("Claim Fraud Investigation")
    CLAIM_FRAUD_INVESTIGATION("Claim Fraud Investigation"),
    @XmlEnumValue("Diagnosis")
    DIAGNOSIS("Diagnosis"),
    @XmlEnumValue("Underwriting")
    UNDERWRITING("Underwriting"),
    @XmlEnumValue("Information Quality Assessment")
    INFORMATION_QUALITY_ASSESSMENT("Information Quality Assessment"),
    @XmlEnumValue("Suspect Duplicate Processing")
    SUSPECT_DUPLICATE_PROCESSING("Suspect Duplicate Processing"),
    @XmlEnumValue("Market Research And Analysis")
    MARKET_RESEARCH_AND_ANALYSIS("Market Research And Analysis"),
    @XmlEnumValue("Control Activity")
    CONTROL_ACTIVITY("Control Activity"),
    @XmlEnumValue("Internal Control Activity")
    INTERNAL_CONTROL_ACTIVITY("Internal Control Activity"),
    @XmlEnumValue("Actuarial Control Activity")
    ACTUARIAL_CONTROL_ACTIVITY("Actuarial Control Activity"),
    @XmlEnumValue("Supervisor Control Activity")
    SUPERVISOR_CONTROL_ACTIVITY("Supervisor Control Activity"),
    @XmlEnumValue("Monitoring Activity")
    MONITORING_ACTIVITY("Monitoring Activity"),
    @XmlEnumValue("Risk Agreement Compare")
    RISK_AGREEMENT_COMPARE("Risk Agreement Compare"),
    @XmlEnumValue("Risk Identification Activity")
    RISK_IDENTIFICATION_ACTIVITY("Risk Identification Activity"),
    @XmlEnumValue("Risk Prioritisation")
    RISK_PRIORITISATION("Risk Prioritisation"),
    @XmlEnumValue("Risk Profile Deviation Assessment")
    RISK_PROFILE_DEVIATION_ASSESSMENT("Risk Profile Deviation Assessment"),
    @XmlEnumValue("Investment Analysis")
    INVESTMENT_ANALYSIS("Investment Analysis"),
    @XmlEnumValue("Party Identification Activity")
    PARTY_IDENTIFICATION_ACTIVITY("Party Identification Activity");
    private final String value;

    AssessmentActivityTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssessmentActivityTypeName fromValue(String v) {
        for (AssessmentActivityTypeName c: AssessmentActivityTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
