
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Event_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Event_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="eventTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Time" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="riskScenarioSimulation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="causedClaimReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="involvedRolePlayerReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="triggeredFinancialServicesRequestReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Event_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "externalReference",
    "startDate",
    "endDate",
    "eventTime",
    "name",
    "description",
    "riskScenarioSimulation",
    "causedClaimReferences",
    "involvedRolePlayerReferences",
    "triggeredFinancialServicesRequestReferences",
    "alternateReference"
})
@XmlSeeAlso({
    WebPageEventTO.class,
    DataEventTO.class,
    LifeEventTO.class
})
public class EventTO
    extends BusinessModelObjectTO
{

    protected String externalReference;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected XMLGregorianCalendar eventTime;
    protected String name;
    protected String description;
    protected Boolean riskScenarioSimulation;
    protected List<ObjectReferenceTO> causedClaimReferences;
    protected List<ObjectReferenceTO> involvedRolePlayerReferences;
    protected List<ObjectReferenceTO> triggeredFinancialServicesRequestReferences;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the eventTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventTime() {
        return eventTime;
    }

    /**
     * Sets the value of the eventTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventTime(XMLGregorianCalendar value) {
        this.eventTime = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the riskScenarioSimulation property.
     * This getter has been renamed from isRiskScenarioSimulation() to getRiskScenarioSimulation() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRiskScenarioSimulation() {
        return riskScenarioSimulation;
    }

    /**
     * Sets the value of the riskScenarioSimulation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRiskScenarioSimulation(Boolean value) {
        this.riskScenarioSimulation = value;
    }

    /**
     * Gets the value of the causedClaimReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the causedClaimReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCausedClaimReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getCausedClaimReferences() {
        if (causedClaimReferences == null) {
            causedClaimReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.causedClaimReferences;
    }

    /**
     * Gets the value of the involvedRolePlayerReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedRolePlayerReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedRolePlayerReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getInvolvedRolePlayerReferences() {
        if (involvedRolePlayerReferences == null) {
            involvedRolePlayerReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.involvedRolePlayerReferences;
    }

    /**
     * Gets the value of the triggeredFinancialServicesRequestReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the triggeredFinancialServicesRequestReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTriggeredFinancialServicesRequestReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getTriggeredFinancialServicesRequestReferences() {
        if (triggeredFinancialServicesRequestReferences == null) {
            triggeredFinancialServicesRequestReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.triggeredFinancialServicesRequestReferences;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the causedClaimReferences property.
     * 
     * @param causedClaimReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setCausedClaimReferences(List<ObjectReferenceTO> causedClaimReferences) {
        this.causedClaimReferences = causedClaimReferences;
    }

    /**
     * Sets the value of the involvedRolePlayerReferences property.
     * 
     * @param involvedRolePlayerReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setInvolvedRolePlayerReferences(List<ObjectReferenceTO> involvedRolePlayerReferences) {
        this.involvedRolePlayerReferences = involvedRolePlayerReferences;
    }

    /**
     * Sets the value of the triggeredFinancialServicesRequestReferences property.
     * 
     * @param triggeredFinancialServicesRequestReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setTriggeredFinancialServicesRequestReferences(List<ObjectReferenceTO> triggeredFinancialServicesRequestReferences) {
        this.triggeredFinancialServicesRequestReferences = triggeredFinancialServicesRequestReferences;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
