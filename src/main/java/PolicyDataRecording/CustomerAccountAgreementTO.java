
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerAccountAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerAccountAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}DepositAccountAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="accountFunding" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyScheduler_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerAccountAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "accountFunding"
})
public class CustomerAccountAgreementTO
    extends DepositAccountAgreementTO
{

    protected MoneySchedulerTO accountFunding;

    /**
     * Gets the value of the accountFunding property.
     * 
     * @return
     *     possible object is
     *     {@link MoneySchedulerTO }
     *     
     */
    public MoneySchedulerTO getAccountFunding() {
        return accountFunding;
    }

    /**
     * Sets the value of the accountFunding property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneySchedulerTO }
     *     
     */
    public void setAccountFunding(MoneySchedulerTO value) {
        this.accountFunding = value;
    }

}
