
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LanguageSkillLanguage.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LanguageSkillLanguage">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="American Sign Language"/>
 *     &lt;enumeration value="Chinese"/>
 *     &lt;enumeration value="Dutch"/>
 *     &lt;enumeration value="English Sign Language"/>
 *     &lt;enumeration value="French"/>
 *     &lt;enumeration value="French Sign Language"/>
 *     &lt;enumeration value="German"/>
 *     &lt;enumeration value="Japanese"/>
 *     &lt;enumeration value="Spanish"/>
 *     &lt;enumeration value="Uk English"/>
 *     &lt;enumeration value="Us English"/>
 *     &lt;enumeration value="Welsh"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LanguageSkillLanguage", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum LanguageSkillLanguage {

    @XmlEnumValue("American Sign Language")
    AMERICAN_SIGN_LANGUAGE("American Sign Language"),
    @XmlEnumValue("Chinese")
    CHINESE("Chinese"),
    @XmlEnumValue("Dutch")
    DUTCH("Dutch"),
    @XmlEnumValue("English Sign Language")
    ENGLISH_SIGN_LANGUAGE("English Sign Language"),
    @XmlEnumValue("French")
    FRENCH("French"),
    @XmlEnumValue("French Sign Language")
    FRENCH_SIGN_LANGUAGE("French Sign Language"),
    @XmlEnumValue("German")
    GERMAN("German"),
    @XmlEnumValue("Japanese")
    JAPANESE("Japanese"),
    @XmlEnumValue("Spanish")
    SPANISH("Spanish"),
    @XmlEnumValue("Uk English")
    UK_ENGLISH("Uk English"),
    @XmlEnumValue("Us English")
    US_ENGLISH("Us English"),
    @XmlEnumValue("Welsh")
    WELSH("Welsh");
    private final String value;

    LanguageSkillLanguage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LanguageSkillLanguage fromValue(String v) {
        for (LanguageSkillLanguage c: LanguageSkillLanguage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
