
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LegalActionTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LegalActionTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Arbitration"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LegalActionTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/DisputeResolution/DisputeResolutionEnumerationsAndStates/")
@XmlEnum
public enum LegalActionTypeName {

    @XmlEnumValue("Arbitration")
    ARBITRATION("Arbitration");
    private final String value;

    LegalActionTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LegalActionTypeName fromValue(String v) {
        for (LegalActionTypeName c: LegalActionTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
