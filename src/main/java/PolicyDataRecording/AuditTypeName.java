
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuditTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AuditTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="External Audit"/>
 *     &lt;enumeration value="External Supervisor Audit"/>
 *     &lt;enumeration value="Internal Audit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AuditTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum AuditTypeName {

    @XmlEnumValue("External Audit")
    EXTERNAL_AUDIT("External Audit"),
    @XmlEnumValue("External Supervisor Audit")
    EXTERNAL_SUPERVISOR_AUDIT("External Supervisor Audit"),
    @XmlEnumValue("Internal Audit")
    INTERNAL_AUDIT("Internal Audit");
    private final String value;

    AuditTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AuditTypeName fromValue(String v) {
        for (AuditTypeName c: AuditTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
