
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationStatus_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommunicationStatus_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}StatusWithCommonReason_TO">
 *       &lt;sequence>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/}CommunicationState" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationStatus_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "state"
})
public class CommunicationStatusTO
    extends StatusWithCommonReasonTO
{

    protected CommunicationState state;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationState }
     *     
     */
    public CommunicationState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationState }
     *     
     */
    public void setState(CommunicationState value) {
        this.state = value;
    }

}
