
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataEvent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataEvent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Event_TO">
 *       &lt;sequence>
 *         &lt;element name="businessPayload" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dataPayload" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataEvent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "businessPayload",
    "dataPayload"
})
public class DataEventTO
    extends EventTO
{

    protected List<BusinessModelObjectTO> businessPayload;
    protected List<Object> dataPayload;

    /**
     * Gets the value of the businessPayload property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the businessPayload property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusinessPayload().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessModelObjectTO }
     * 
     * 
     */
    public List<BusinessModelObjectTO> getBusinessPayload() {
        if (businessPayload == null) {
            businessPayload = new ArrayList<BusinessModelObjectTO>();
        }
        return this.businessPayload;
    }

    /**
     * Gets the value of the dataPayload property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataPayload property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataPayload().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getDataPayload() {
        if (dataPayload == null) {
            dataPayload = new ArrayList<Object>();
        }
        return this.dataPayload;
    }

    /**
     * Sets the value of the businessPayload property.
     * 
     * @param businessPayload
     *     allowed object is
     *     {@link BusinessModelObjectTO }
     *     
     */
    public void setBusinessPayload(List<BusinessModelObjectTO> businessPayload) {
        this.businessPayload = businessPayload;
    }

    /**
     * Sets the value of the dataPayload property.
     * 
     * @param dataPayload
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setDataPayload(List<Object> dataPayload) {
        this.dataPayload = dataPayload;
    }

}
