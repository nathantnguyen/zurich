
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommercialAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}InsurancePolicy_TO">
 *       &lt;sequence>
 *         &lt;element name="pricingTier" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}PricingTier" minOccurs="0"/>
 *         &lt;element name="commercialAgreementDetail" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CommercialAgreementDetail_TO" minOccurs="0"/>
 *         &lt;element name="rateClass" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "pricingTier",
    "commercialAgreementDetail",
    "rateClass"
})
@XmlSeeAlso({
    CommercialPropertyInsurancePolicyTO.class,
    WorkersCompensationInsurancePolicyTO.class,
    CommercialAutoInsurancePolicyTO.class
})
public class CommercialAgreementTO
    extends InsurancePolicyTO
{

    protected PricingTier pricingTier;
    protected CommercialAgreementDetailTO commercialAgreementDetail;
    protected String rateClass;

    /**
     * Gets the value of the pricingTier property.
     * 
     * @return
     *     possible object is
     *     {@link PricingTier }
     *     
     */
    public PricingTier getPricingTier() {
        return pricingTier;
    }

    /**
     * Sets the value of the pricingTier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingTier }
     *     
     */
    public void setPricingTier(PricingTier value) {
        this.pricingTier = value;
    }

    /**
     * Gets the value of the commercialAgreementDetail property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialAgreementDetailTO }
     *     
     */
    public CommercialAgreementDetailTO getCommercialAgreementDetail() {
        return commercialAgreementDetail;
    }

    /**
     * Sets the value of the commercialAgreementDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialAgreementDetailTO }
     *     
     */
    public void setCommercialAgreementDetail(CommercialAgreementDetailTO value) {
        this.commercialAgreementDetail = value;
    }

    /**
     * Gets the value of the rateClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateClass() {
        return rateClass;
    }

    /**
     * Sets the value of the rateClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateClass(String value) {
        this.rateClass = value;
    }

}
