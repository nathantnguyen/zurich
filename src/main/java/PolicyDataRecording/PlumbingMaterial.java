
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlumbingMaterial.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlumbingMaterial">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Plastic"/>
 *     &lt;enumeration value="Copper"/>
 *     &lt;enumeration value="Lead"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PlumbingMaterial", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum PlumbingMaterial {

    @XmlEnumValue("Plastic")
    PLASTIC("Plastic"),
    @XmlEnumValue("Copper")
    COPPER("Copper"),
    @XmlEnumValue("Lead")
    LEAD("Lead");
    private final String value;

    PlumbingMaterial(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlumbingMaterial fromValue(String v) {
        for (PlumbingMaterial c: PlumbingMaterial.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
