
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PhysicalObjectPlaceUsage_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PhysicalObjectPlaceUsage_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}RoleInPhysicalObject_TO">
 *       &lt;sequence>
 *         &lt;element name="place" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *         &lt;element name="physicalObjectPlaceUsageRootType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhysicalObjectPlaceUsage_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "place",
    "physicalObjectPlaceUsageRootType"
})
public class PhysicalObjectPlaceUsageTO
    extends RoleInPhysicalObjectTO
{

    protected PlaceTO place;
    protected String physicalObjectPlaceUsageRootType;

    /**
     * Gets the value of the place property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getPlace() {
        return place;
    }

    /**
     * Sets the value of the place property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setPlace(PlaceTO value) {
        this.place = value;
    }

    /**
     * Gets the value of the physicalObjectPlaceUsageRootType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhysicalObjectPlaceUsageRootType() {
        return physicalObjectPlaceUsageRootType;
    }

    /**
     * Sets the value of the physicalObjectPlaceUsageRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhysicalObjectPlaceUsageRootType(String value) {
        this.physicalObjectPlaceUsageRootType = value;
    }

}
