
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationPurpose.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunicationPurpose">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Campaign Mailing"/>
 *     &lt;enumeration value="Complaint"/>
 *     &lt;enumeration value="Demographic Data"/>
 *     &lt;enumeration value="Quotation Request"/>
 *     &lt;enumeration value="Regular Statement"/>
 *     &lt;enumeration value="Returned Mail"/>
 *     &lt;enumeration value="Sale"/>
 *     &lt;enumeration value="Complaint Handling"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommunicationPurpose", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum CommunicationPurpose {

    @XmlEnumValue("Campaign Mailing")
    CAMPAIGN_MAILING("Campaign Mailing"),
    @XmlEnumValue("Complaint")
    COMPLAINT("Complaint"),
    @XmlEnumValue("Demographic Data")
    DEMOGRAPHIC_DATA("Demographic Data"),
    @XmlEnumValue("Quotation Request")
    QUOTATION_REQUEST("Quotation Request"),
    @XmlEnumValue("Regular Statement")
    REGULAR_STATEMENT("Regular Statement"),
    @XmlEnumValue("Returned Mail")
    RETURNED_MAIL("Returned Mail"),
    @XmlEnumValue("Sale")
    SALE("Sale"),
    @XmlEnumValue("Complaint Handling")
    COMPLAINT_HANDLING("Complaint Handling");
    private final String value;

    CommunicationPurpose(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationPurpose fromValue(String v) {
        for (CommunicationPurpose c: CommunicationPurpose.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
