
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Industry_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Industry_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeneralActivity_TO">
 *       &lt;sequence>
 *         &lt;element name="industryCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="codeIssuer" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}IndustryCodeIssuer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Industry_TO", propOrder = {
    "industryCode",
    "codeIssuer"
})
public class IndustryTO
    extends GeneralActivityTO
{

    protected String industryCode;
    protected IndustryCodeIssuer codeIssuer;

    /**
     * Gets the value of the industryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustryCode() {
        return industryCode;
    }

    /**
     * Sets the value of the industryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustryCode(String value) {
        this.industryCode = value;
    }

    /**
     * Gets the value of the codeIssuer property.
     * 
     * @return
     *     possible object is
     *     {@link IndustryCodeIssuer }
     *     
     */
    public IndustryCodeIssuer getCodeIssuer() {
        return codeIssuer;
    }

    /**
     * Sets the value of the codeIssuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndustryCodeIssuer }
     *     
     */
    public void setCodeIssuer(IndustryCodeIssuer value) {
        this.codeIssuer = value;
    }

}
