
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Deductible_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Deductible_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}MoneyProvisionDeterminer_TO">
 *       &lt;sequence>
 *         &lt;element name="minimumAmount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexableCurrencyAmount_TO" minOccurs="0"/>
 *         &lt;element name="maximumAmount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexableCurrencyAmount_TO" minOccurs="0"/>
 *         &lt;element name="rate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="anniversaryDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="periodicity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/}DeductiblePeriodicity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Deductible_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "minimumAmount",
    "maximumAmount",
    "rate",
    "anniversaryDate",
    "periodicity"
})
public class DeductibleTO
    extends MoneyProvisionDeterminerTO
{

    protected IndexableCurrencyAmountTO minimumAmount;
    protected IndexableCurrencyAmountTO maximumAmount;
    protected BigDecimal rate;
    protected XMLGregorianCalendar anniversaryDate;
    protected DeductiblePeriodicity periodicity;

    /**
     * Gets the value of the minimumAmount property.
     * 
     * @return
     *     possible object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public IndexableCurrencyAmountTO getMinimumAmount() {
        return minimumAmount;
    }

    /**
     * Sets the value of the minimumAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public void setMinimumAmount(IndexableCurrencyAmountTO value) {
        this.minimumAmount = value;
    }

    /**
     * Gets the value of the maximumAmount property.
     * 
     * @return
     *     possible object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public IndexableCurrencyAmountTO getMaximumAmount() {
        return maximumAmount;
    }

    /**
     * Sets the value of the maximumAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public void setMaximumAmount(IndexableCurrencyAmountTO value) {
        this.maximumAmount = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Gets the value of the anniversaryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAnniversaryDate() {
        return anniversaryDate;
    }

    /**
     * Sets the value of the anniversaryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAnniversaryDate(XMLGregorianCalendar value) {
        this.anniversaryDate = value;
    }

    /**
     * Gets the value of the periodicity property.
     * 
     * @return
     *     possible object is
     *     {@link DeductiblePeriodicity }
     *     
     */
    public DeductiblePeriodicity getPeriodicity() {
        return periodicity;
    }

    /**
     * Sets the value of the periodicity property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeductiblePeriodicity }
     *     
     */
    public void setPeriodicity(DeductiblePeriodicity value) {
        this.periodicity = value;
    }

}
