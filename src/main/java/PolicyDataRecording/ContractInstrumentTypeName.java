
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractInstrumentTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContractInstrumentTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Commodity Receipt"/>
 *     &lt;enumeration value="Depository Receipt"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContractInstrumentTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum ContractInstrumentTypeName {

    @XmlEnumValue("Commodity Receipt")
    COMMODITY_RECEIPT("Commodity Receipt"),
    @XmlEnumValue("Depository Receipt")
    DEPOSITORY_RECEIPT("Depository Receipt");
    private final String value;

    ContractInstrumentTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContractInstrumentTypeName fromValue(String v) {
        for (ContractInstrumentTypeName c: ContractInstrumentTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
