
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyInvolvedInRiskAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PartyInvolvedInRiskAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Risk Agreement Owner"/>
 *     &lt;enumeration value="Risk Agreement Approver"/>
 *     &lt;enumeration value="Risk Manager"/>
 *     &lt;enumeration value="Internal Risk Auditor"/>
 *     &lt;enumeration value="External Risk Auditor"/>
 *     &lt;enumeration value="Risk Management Provider"/>
 *     &lt;enumeration value="Risk Agreement Subject"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PartyInvolvedInRiskAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum PartyInvolvedInRiskAgreementTypeName {

    @XmlEnumValue("Risk Agreement Owner")
    RISK_AGREEMENT_OWNER("Risk Agreement Owner"),
    @XmlEnumValue("Risk Agreement Approver")
    RISK_AGREEMENT_APPROVER("Risk Agreement Approver"),
    @XmlEnumValue("Risk Manager")
    RISK_MANAGER("Risk Manager"),
    @XmlEnumValue("Internal Risk Auditor")
    INTERNAL_RISK_AUDITOR("Internal Risk Auditor"),
    @XmlEnumValue("External Risk Auditor")
    EXTERNAL_RISK_AUDITOR("External Risk Auditor"),
    @XmlEnumValue("Risk Management Provider")
    RISK_MANAGEMENT_PROVIDER("Risk Management Provider"),
    @XmlEnumValue("Risk Agreement Subject")
    RISK_AGREEMENT_SUBJECT("Risk Agreement Subject");
    private final String value;

    PartyInvolvedInRiskAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartyInvolvedInRiskAgreementTypeName fromValue(String v) {
        for (PartyInvolvedInRiskAgreementTypeName c: PartyInvolvedInRiskAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
