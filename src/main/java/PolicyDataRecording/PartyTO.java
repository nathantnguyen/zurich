
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Party_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Party_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO">
 *       &lt;sequence>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="preferredPaymentMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}PaymentMethodMethod" minOccurs="0"/>
 *         &lt;element name="preferredProvider" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}HealthCareProviderRole_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="securityRegistrations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/}SecurityRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="partyRegistrations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="creditRating" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CreditRating_TO" minOccurs="0"/>
 *         &lt;element name="isDebtorCreditor" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}DebitCreditStatus" minOccurs="0"/>
 *         &lt;element name="amountOwed" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="introduction" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}Introduction" minOccurs="0"/>
 *         &lt;element name="primeRole" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="previousRefusal" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Party_TO", propOrder = {
    "externalReference",
    "preferredPaymentMethod",
    "preferredProvider",
    "securityRegistrations",
    "partyRegistrations",
    "creditRating",
    "isDebtorCreditor",
    "amountOwed",
    "introduction",
    "primeRole",
    "previousRefusal",
    "alternateReference"
})
@XmlSeeAlso({
    PersonTO.class,
    OrganisationTO.class
})
public class PartyTO
    extends RolePlayerTO
{

    protected String externalReference;
    protected PaymentMethodMethod preferredPaymentMethod;
    protected List<HealthCareProviderRoleTO> preferredProvider;
    protected List<SecurityRegistrationTO> securityRegistrations;
    protected List<PartyRegistrationTO> partyRegistrations;
    protected CreditRatingTO creditRating;
    protected DebitCreditStatus isDebtorCreditor;
    protected BaseCurrencyAmount amountOwed;
    protected Introduction introduction;
    protected String primeRole;
    protected Boolean previousRefusal;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the preferredPaymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMethodMethod }
     *     
     */
    public PaymentMethodMethod getPreferredPaymentMethod() {
        return preferredPaymentMethod;
    }

    /**
     * Sets the value of the preferredPaymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMethodMethod }
     *     
     */
    public void setPreferredPaymentMethod(PaymentMethodMethod value) {
        this.preferredPaymentMethod = value;
    }

    /**
     * Gets the value of the preferredProvider property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the preferredProvider property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPreferredProvider().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HealthCareProviderRoleTO }
     * 
     * 
     */
    public List<HealthCareProviderRoleTO> getPreferredProvider() {
        if (preferredProvider == null) {
            preferredProvider = new ArrayList<HealthCareProviderRoleTO>();
        }
        return this.preferredProvider;
    }

    /**
     * Gets the value of the securityRegistrations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the securityRegistrations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecurityRegistrations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityRegistrationTO }
     * 
     * 
     */
    public List<SecurityRegistrationTO> getSecurityRegistrations() {
        if (securityRegistrations == null) {
            securityRegistrations = new ArrayList<SecurityRegistrationTO>();
        }
        return this.securityRegistrations;
    }

    /**
     * Gets the value of the partyRegistrations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyRegistrations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyRegistrations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyRegistrationTO }
     * 
     * 
     */
    public List<PartyRegistrationTO> getPartyRegistrations() {
        if (partyRegistrations == null) {
            partyRegistrations = new ArrayList<PartyRegistrationTO>();
        }
        return this.partyRegistrations;
    }

    /**
     * Gets the value of the creditRating property.
     * 
     * @return
     *     possible object is
     *     {@link CreditRatingTO }
     *     
     */
    public CreditRatingTO getCreditRating() {
        return creditRating;
    }

    /**
     * Sets the value of the creditRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditRatingTO }
     *     
     */
    public void setCreditRating(CreditRatingTO value) {
        this.creditRating = value;
    }

    /**
     * Gets the value of the isDebtorCreditor property.
     * 
     * @return
     *     possible object is
     *     {@link DebitCreditStatus }
     *     
     */
    public DebitCreditStatus getIsDebtorCreditor() {
        return isDebtorCreditor;
    }

    /**
     * Sets the value of the isDebtorCreditor property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitCreditStatus }
     *     
     */
    public void setIsDebtorCreditor(DebitCreditStatus value) {
        this.isDebtorCreditor = value;
    }

    /**
     * Gets the value of the amountOwed property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAmountOwed() {
        return amountOwed;
    }

    /**
     * Sets the value of the amountOwed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAmountOwed(BaseCurrencyAmount value) {
        this.amountOwed = value;
    }

    /**
     * Gets the value of the introduction property.
     * 
     * @return
     *     possible object is
     *     {@link Introduction }
     *     
     */
    public Introduction getIntroduction() {
        return introduction;
    }

    /**
     * Sets the value of the introduction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Introduction }
     *     
     */
    public void setIntroduction(Introduction value) {
        this.introduction = value;
    }

    /**
     * Gets the value of the primeRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimeRole() {
        return primeRole;
    }

    /**
     * Sets the value of the primeRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimeRole(String value) {
        this.primeRole = value;
    }

    /**
     * Gets the value of the previousRefusal property.
     * This getter has been renamed from isPreviousRefusal() to getPreviousRefusal() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPreviousRefusal() {
        return previousRefusal;
    }

    /**
     * Sets the value of the previousRefusal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreviousRefusal(Boolean value) {
        this.previousRefusal = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the preferredProvider property.
     * 
     * @param preferredProvider
     *     allowed object is
     *     {@link HealthCareProviderRoleTO }
     *     
     */
    public void setPreferredProvider(List<HealthCareProviderRoleTO> preferredProvider) {
        this.preferredProvider = preferredProvider;
    }

    /**
     * Sets the value of the securityRegistrations property.
     * 
     * @param securityRegistrations
     *     allowed object is
     *     {@link SecurityRegistrationTO }
     *     
     */
    public void setSecurityRegistrations(List<SecurityRegistrationTO> securityRegistrations) {
        this.securityRegistrations = securityRegistrations;
    }

    /**
     * Sets the value of the partyRegistrations property.
     * 
     * @param partyRegistrations
     *     allowed object is
     *     {@link PartyRegistrationTO }
     *     
     */
    public void setPartyRegistrations(List<PartyRegistrationTO> partyRegistrations) {
        this.partyRegistrations = partyRegistrations;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
