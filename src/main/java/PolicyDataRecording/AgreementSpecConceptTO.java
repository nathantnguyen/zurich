
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgreementSpecConcept_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgreementSpecConcept_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="compositeSpecConcept" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="agreementSpecVersions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementSpecDefCriteria_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgreementSpecConcept_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "kind",
    "description",
    "compositeSpecConcept",
    "agreementSpecVersions"
})
public class AgreementSpecConceptTO
    extends BusinessModelObjectTO
{

    protected String kind;
    protected String description;
    protected List<AgreementSpecTO> compositeSpecConcept;
    protected List<AgreementSpecDefCriteriaTO> agreementSpecVersions;

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the compositeSpecConcept property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the compositeSpecConcept property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCompositeSpecConcept().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgreementSpecTO }
     * 
     * 
     */
    public List<AgreementSpecTO> getCompositeSpecConcept() {
        if (compositeSpecConcept == null) {
            compositeSpecConcept = new ArrayList<AgreementSpecTO>();
        }
        return this.compositeSpecConcept;
    }

    /**
     * Gets the value of the agreementSpecVersions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agreementSpecVersions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgreementSpecVersions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgreementSpecDefCriteriaTO }
     * 
     * 
     */
    public List<AgreementSpecDefCriteriaTO> getAgreementSpecVersions() {
        if (agreementSpecVersions == null) {
            agreementSpecVersions = new ArrayList<AgreementSpecDefCriteriaTO>();
        }
        return this.agreementSpecVersions;
    }

    /**
     * Sets the value of the compositeSpecConcept property.
     * 
     * @param compositeSpecConcept
     *     allowed object is
     *     {@link AgreementSpecTO }
     *     
     */
    public void setCompositeSpecConcept(List<AgreementSpecTO> compositeSpecConcept) {
        this.compositeSpecConcept = compositeSpecConcept;
    }

    /**
     * Sets the value of the agreementSpecVersions property.
     * 
     * @param agreementSpecVersions
     *     allowed object is
     *     {@link AgreementSpecDefCriteriaTO }
     *     
     */
    public void setAgreementSpecVersions(List<AgreementSpecDefCriteriaTO> agreementSpecVersions) {
        this.agreementSpecVersions = agreementSpecVersions;
    }

}
