
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NoteTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NoteTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Explanation Note"/>
 *     &lt;enumeration value="Findings Note"/>
 *     &lt;enumeration value="Recommendation Note"/>
 *     &lt;enumeration value="Confirmation Note"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NoteTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum NoteTypeName {

    @XmlEnumValue("Explanation Note")
    EXPLANATION_NOTE("Explanation Note"),
    @XmlEnumValue("Findings Note")
    FINDINGS_NOTE("Findings Note"),
    @XmlEnumValue("Recommendation Note")
    RECOMMENDATION_NOTE("Recommendation Note"),
    @XmlEnumValue("Confirmation Note")
    CONFIRMATION_NOTE("Confirmation Note");
    private final String value;

    NoteTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NoteTypeName fromValue(String v) {
        for (NoteTypeName c: NoteTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
