
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ManufacturedItemTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ManufacturedItemTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Vehicle Item"/>
 *     &lt;enumeration value="Household Item"/>
 *     &lt;enumeration value="Aircraft"/>
 *     &lt;enumeration value="Road Vehicle"/>
 *     &lt;enumeration value="Device"/>
 *     &lt;enumeration value="Sensor"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ManufacturedItemTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum ManufacturedItemTypeName {

    @XmlEnumValue("Vehicle Item")
    VEHICLE_ITEM("Vehicle Item"),
    @XmlEnumValue("Household Item")
    HOUSEHOLD_ITEM("Household Item"),
    @XmlEnumValue("Aircraft")
    AIRCRAFT("Aircraft"),
    @XmlEnumValue("Road Vehicle")
    ROAD_VEHICLE("Road Vehicle"),
    @XmlEnumValue("Device")
    DEVICE("Device"),
    @XmlEnumValue("Sensor")
    SENSOR("Sensor");
    private final String value;

    ManufacturedItemTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ManufacturedItemTypeName fromValue(String v) {
        for (ManufacturedItemTypeName c: ManufacturedItemTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
