
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompositeInstrumentTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CompositeInstrumentTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Exchange Traded Fund Instrument"/>
 *     &lt;enumeration value="Financial Index Instrument"/>
 *     &lt;enumeration value="Mutual Fund Instrument"/>
 *     &lt;enumeration value="Unit Instrument"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CompositeInstrumentTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum CompositeInstrumentTypeName {

    @XmlEnumValue("Exchange Traded Fund Instrument")
    EXCHANGE_TRADED_FUND_INSTRUMENT("Exchange Traded Fund Instrument"),
    @XmlEnumValue("Financial Index Instrument")
    FINANCIAL_INDEX_INSTRUMENT("Financial Index Instrument"),
    @XmlEnumValue("Mutual Fund Instrument")
    MUTUAL_FUND_INSTRUMENT("Mutual Fund Instrument"),
    @XmlEnumValue("Unit Instrument")
    UNIT_INSTRUMENT("Unit Instrument");
    private final String value;

    CompositeInstrumentTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CompositeInstrumentTypeName fromValue(String v) {
        for (CompositeInstrumentTypeName c: CompositeInstrumentTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
