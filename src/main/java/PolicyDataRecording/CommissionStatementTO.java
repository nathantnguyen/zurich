
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommissionStatement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommissionStatement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}AccountStatement_TO">
 *       &lt;sequence>
 *         &lt;element name="totalEarningsCurrentYear" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="producerAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}IntermediaryAgreement_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommissionStatement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/", propOrder = {
    "totalEarningsCurrentYear",
    "producerAgreement"
})
public class CommissionStatementTO
    extends AccountStatementTO
{

    protected BaseCurrencyAmount totalEarningsCurrentYear;
    protected IntermediaryAgreementTO producerAgreement;

    /**
     * Gets the value of the totalEarningsCurrentYear property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTotalEarningsCurrentYear() {
        return totalEarningsCurrentYear;
    }

    /**
     * Sets the value of the totalEarningsCurrentYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTotalEarningsCurrentYear(BaseCurrencyAmount value) {
        this.totalEarningsCurrentYear = value;
    }

    /**
     * Gets the value of the producerAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public IntermediaryAgreementTO getProducerAgreement() {
        return producerAgreement;
    }

    /**
     * Sets the value of the producerAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public void setProducerAgreement(IntermediaryAgreementTO value) {
        this.producerAgreement = value;
    }

}
