
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BusinessModelObject_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessModelObject_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}CategorisableObject_TO">
 *       &lt;sequence>
 *         &lt;element name="basicDataIncomplete" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="systemCreationDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="systemUpdateDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="typeName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="categoryMembershipName" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInContext" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesPlayed" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RolePlayerClassRole_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dynamicProperties" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Property_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="notes" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Note_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="systemCreationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="systemUpdateDateTimeAsString" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="systemCreationById" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessModelObject_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "basicDataIncomplete",
    "systemCreationDateTime",
    "systemUpdateDateTime",
    "typeName",
    "categoryMembershipName",
    "rolesInContext",
    "rolesPlayed",
    "dynamicProperties",
    "notes",
    "systemCreationDate",
    "systemUpdateDateTimeAsString",
    "systemCreationById"
})
@XmlSeeAlso({
    CommunicationTO.class,
    DocumentTO.class,
    CommunicationContentTO.class,
    StandardTextSpecificationTO.class,
    ContractRequestSpecificationTO.class,
    SanctionsTransactionsTO.class,
    RiskExposureTO.class,
    BusinessRuleTO.class,
    GeographicCoordinatesTO.class,
    ModelSpecificationTO.class,
    StatisticsTO.class,
    ContractSpecificationTO.class,
    ContractRequestTO.class,
    ContractTO.class,
    ActivitySpecificationTO.class,
    AgreementSpecConceptTO.class,
    SpecificationTO.class,
    StructuredActualTO.class,
    LegalActionTO.class,
    MoneyTransferInformationTO.class,
    PhysicalObjectTO.class,
    PaymentTransferMethodTO.class,
    FinancialStatementTO.class,
    MoneySchedulerTO.class,
    FinancialTransactionTO.class,
    MoneyProvisionTO.class,
    SecurityRegistrationTO.class,
    AccountTO.class,
    MoneyProvisionDeterminerTO.class,
    FinancialAssetTO.class,
    EventTO.class,
    PlaceTO.class,
    ContactPointTO.class,
    DependentObjectTO.class,
    AssessmentResultTO.class,
    ActivityTO.class,
    RolePlayerTO.class
})
public class BusinessModelObjectTO
    extends CategorisableObjectTO
{

    protected Boolean basicDataIncomplete;
    protected XMLGregorianCalendar systemCreationDateTime;
    protected XMLGregorianCalendar systemUpdateDateTime;
    protected String typeName;
    protected List<ObjectReferenceTO> categoryMembershipName;
    protected List<RoleInContextClassTO> rolesInContext;
    protected List<RolePlayerClassRoleTO> rolesPlayed;
    protected List<PropertyTO> dynamicProperties;
    protected List<NoteTO> notes;
    protected XMLGregorianCalendar systemCreationDate;
    protected String systemUpdateDateTimeAsString;
    protected String systemCreationById;

    /**
     * Gets the value of the basicDataIncomplete property.
     * This getter has been renamed from isBasicDataIncomplete() to getBasicDataIncomplete() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBasicDataIncomplete() {
        return basicDataIncomplete;
    }

    /**
     * Sets the value of the basicDataIncomplete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBasicDataIncomplete(Boolean value) {
        this.basicDataIncomplete = value;
    }

    /**
     * Gets the value of the systemCreationDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSystemCreationDateTime() {
        return systemCreationDateTime;
    }

    /**
     * Sets the value of the systemCreationDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSystemCreationDateTime(XMLGregorianCalendar value) {
        this.systemCreationDateTime = value;
    }

    /**
     * Gets the value of the systemUpdateDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSystemUpdateDateTime() {
        return systemUpdateDateTime;
    }

    /**
     * Sets the value of the systemUpdateDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSystemUpdateDateTime(XMLGregorianCalendar value) {
        this.systemUpdateDateTime = value;
    }

    /**
     * Gets the value of the typeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets the value of the typeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeName(String value) {
        this.typeName = value;
    }

    /**
     * Gets the value of the categoryMembershipName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the categoryMembershipName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategoryMembershipName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getCategoryMembershipName() {
        if (categoryMembershipName == null) {
            categoryMembershipName = new ArrayList<ObjectReferenceTO>();
        }
        return this.categoryMembershipName;
    }

    /**
     * Gets the value of the rolesInContext property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInContext property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInContext().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInContextClassTO }
     * 
     * 
     */
    public List<RoleInContextClassTO> getRolesInContext() {
        if (rolesInContext == null) {
            rolesInContext = new ArrayList<RoleInContextClassTO>();
        }
        return this.rolesInContext;
    }

    /**
     * Gets the value of the rolesPlayed property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesPlayed property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesPlayed().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerClassRoleTO }
     * 
     * 
     */
    public List<RolePlayerClassRoleTO> getRolesPlayed() {
        if (rolesPlayed == null) {
            rolesPlayed = new ArrayList<RolePlayerClassRoleTO>();
        }
        return this.rolesPlayed;
    }

    /**
     * Gets the value of the dynamicProperties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dynamicProperties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDynamicProperties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyTO }
     * 
     * 
     */
    public List<PropertyTO> getDynamicProperties() {
        if (dynamicProperties == null) {
            dynamicProperties = new ArrayList<PropertyTO>();
        }
        return this.dynamicProperties;
    }

    /**
     * Gets the value of the notes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoteTO }
     * 
     * 
     */
    public List<NoteTO> getNotes() {
        if (notes == null) {
            notes = new ArrayList<NoteTO>();
        }
        return this.notes;
    }

    /**
     * Gets the value of the systemCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSystemCreationDate() {
        return systemCreationDate;
    }

    /**
     * Sets the value of the systemCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSystemCreationDate(XMLGregorianCalendar value) {
        this.systemCreationDate = value;
    }

    /**
     * Gets the value of the systemUpdateDateTimeAsString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemUpdateDateTimeAsString() {
        return systemUpdateDateTimeAsString;
    }

    /**
     * Sets the value of the systemUpdateDateTimeAsString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemUpdateDateTimeAsString(String value) {
        this.systemUpdateDateTimeAsString = value;
    }

    /**
     * Gets the value of the systemCreationById property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemCreationById() {
        return systemCreationById;
    }

    /**
     * Sets the value of the systemCreationById property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemCreationById(String value) {
        this.systemCreationById = value;
    }

    /**
     * Sets the value of the categoryMembershipName property.
     * 
     * @param categoryMembershipName
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setCategoryMembershipName(List<ObjectReferenceTO> categoryMembershipName) {
        this.categoryMembershipName = categoryMembershipName;
    }

    /**
     * Sets the value of the rolesInContext property.
     * 
     * @param rolesInContext
     *     allowed object is
     *     {@link RoleInContextClassTO }
     *     
     */
    public void setRolesInContext(List<RoleInContextClassTO> rolesInContext) {
        this.rolesInContext = rolesInContext;
    }

    /**
     * Sets the value of the rolesPlayed property.
     * 
     * @param rolesPlayed
     *     allowed object is
     *     {@link RolePlayerClassRoleTO }
     *     
     */
    public void setRolesPlayed(List<RolePlayerClassRoleTO> rolesPlayed) {
        this.rolesPlayed = rolesPlayed;
    }

    /**
     * Sets the value of the dynamicProperties property.
     * 
     * @param dynamicProperties
     *     allowed object is
     *     {@link PropertyTO }
     *     
     */
    public void setDynamicProperties(List<PropertyTO> dynamicProperties) {
        this.dynamicProperties = dynamicProperties;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param notes
     *     allowed object is
     *     {@link NoteTO }
     *     
     */
    public void setNotes(List<NoteTO> notes) {
        this.notes = notes;
    }

}
