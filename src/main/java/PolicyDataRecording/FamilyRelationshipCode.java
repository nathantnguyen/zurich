
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FamilyRelationshipCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FamilyRelationshipCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Brother"/>
 *     &lt;enumeration value="Sister"/>
 *     &lt;enumeration value="Daughter"/>
 *     &lt;enumeration value="Son"/>
 *     &lt;enumeration value="Grandfather"/>
 *     &lt;enumeration value="Grandmother"/>
 *     &lt;enumeration value="Father"/>
 *     &lt;enumeration value="Mother"/>
 *     &lt;enumeration value="Nephew"/>
 *     &lt;enumeration value="Uncle"/>
 *     &lt;enumeration value="Aunt"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FamilyRelationshipCode", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum FamilyRelationshipCode {

    @XmlEnumValue("Brother")
    BROTHER("Brother"),
    @XmlEnumValue("Sister")
    SISTER("Sister"),
    @XmlEnumValue("Daughter")
    DAUGHTER("Daughter"),
    @XmlEnumValue("Son")
    SON("Son"),
    @XmlEnumValue("Grandfather")
    GRANDFATHER("Grandfather"),
    @XmlEnumValue("Grandmother")
    GRANDMOTHER("Grandmother"),
    @XmlEnumValue("Father")
    FATHER("Father"),
    @XmlEnumValue("Mother")
    MOTHER("Mother"),
    @XmlEnumValue("Nephew")
    NEPHEW("Nephew"),
    @XmlEnumValue("Uncle")
    UNCLE("Uncle"),
    @XmlEnumValue("Aunt")
    AUNT("Aunt");
    private final String value;

    FamilyRelationshipCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FamilyRelationshipCode fromValue(String v) {
        for (FamilyRelationshipCode c: FamilyRelationshipCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
