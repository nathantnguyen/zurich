
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Fund_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Fund_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="investmentScope" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/}InvestmentScope" minOccurs="0"/>
 *         &lt;element name="investmentObjective" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="numberOfUnits" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="trackingAccounts" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Account_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="appliedProfile" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}InvestmentProfile_TO" minOccurs="0"/>
 *         &lt;element name="investmentStrategy" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="statementOfInvestmentPrinciples" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="unitValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="unitValueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="trackingAccountReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="investmentObjectiveClass" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/}InvestmentObjectiveClassName" minOccurs="0"/>
 *         &lt;element name="indexLinked" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="unitLinked" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="fundRegistration" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FundRegistration_TO" minOccurs="0"/>
 *         &lt;element name="minimumInvestment" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="assetCategory" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AssetCategory_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Fund_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "investmentScope",
    "investmentObjective",
    "numberOfUnits",
    "trackingAccounts",
    "appliedProfile",
    "investmentStrategy",
    "statementOfInvestmentPrinciples",
    "unitValue",
    "unitValueDate",
    "trackingAccountReferences",
    "investmentObjectiveClass",
    "indexLinked",
    "unitLinked",
    "fundRegistration",
    "minimumInvestment",
    "assetCategory"
})
public class FundTO
    extends FinancialAssetTO
{

    protected InvestmentScope investmentScope;
    protected String investmentObjective;
    protected BigInteger numberOfUnits;
    protected List<AccountTO> trackingAccounts;
    protected InvestmentProfileTO appliedProfile;
    protected String investmentStrategy;
    protected String statementOfInvestmentPrinciples;
    protected BaseCurrencyAmount unitValue;
    protected XMLGregorianCalendar unitValueDate;
    protected List<ObjectReferenceTO> trackingAccountReferences;
    protected InvestmentObjectiveClassName investmentObjectiveClass;
    protected Boolean indexLinked;
    protected Boolean unitLinked;
    protected FundRegistrationTO fundRegistration;
    protected BaseCurrencyAmount minimumInvestment;
    protected AssetCategoryTO assetCategory;

    /**
     * Gets the value of the investmentScope property.
     * 
     * @return
     *     possible object is
     *     {@link InvestmentScope }
     *     
     */
    public InvestmentScope getInvestmentScope() {
        return investmentScope;
    }

    /**
     * Sets the value of the investmentScope property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvestmentScope }
     *     
     */
    public void setInvestmentScope(InvestmentScope value) {
        this.investmentScope = value;
    }

    /**
     * Gets the value of the investmentObjective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvestmentObjective() {
        return investmentObjective;
    }

    /**
     * Sets the value of the investmentObjective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvestmentObjective(String value) {
        this.investmentObjective = value;
    }

    /**
     * Gets the value of the numberOfUnits property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfUnits() {
        return numberOfUnits;
    }

    /**
     * Sets the value of the numberOfUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfUnits(BigInteger value) {
        this.numberOfUnits = value;
    }

    /**
     * Gets the value of the trackingAccounts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trackingAccounts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrackingAccounts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountTO }
     * 
     * 
     */
    public List<AccountTO> getTrackingAccounts() {
        if (trackingAccounts == null) {
            trackingAccounts = new ArrayList<AccountTO>();
        }
        return this.trackingAccounts;
    }

    /**
     * Gets the value of the appliedProfile property.
     * 
     * @return
     *     possible object is
     *     {@link InvestmentProfileTO }
     *     
     */
    public InvestmentProfileTO getAppliedProfile() {
        return appliedProfile;
    }

    /**
     * Sets the value of the appliedProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvestmentProfileTO }
     *     
     */
    public void setAppliedProfile(InvestmentProfileTO value) {
        this.appliedProfile = value;
    }

    /**
     * Gets the value of the investmentStrategy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvestmentStrategy() {
        return investmentStrategy;
    }

    /**
     * Sets the value of the investmentStrategy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvestmentStrategy(String value) {
        this.investmentStrategy = value;
    }

    /**
     * Gets the value of the statementOfInvestmentPrinciples property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementOfInvestmentPrinciples() {
        return statementOfInvestmentPrinciples;
    }

    /**
     * Sets the value of the statementOfInvestmentPrinciples property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementOfInvestmentPrinciples(String value) {
        this.statementOfInvestmentPrinciples = value;
    }

    /**
     * Gets the value of the unitValue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getUnitValue() {
        return unitValue;
    }

    /**
     * Sets the value of the unitValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setUnitValue(BaseCurrencyAmount value) {
        this.unitValue = value;
    }

    /**
     * Gets the value of the unitValueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUnitValueDate() {
        return unitValueDate;
    }

    /**
     * Sets the value of the unitValueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUnitValueDate(XMLGregorianCalendar value) {
        this.unitValueDate = value;
    }

    /**
     * Gets the value of the trackingAccountReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trackingAccountReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrackingAccountReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getTrackingAccountReferences() {
        if (trackingAccountReferences == null) {
            trackingAccountReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.trackingAccountReferences;
    }

    /**
     * Gets the value of the investmentObjectiveClass property.
     * 
     * @return
     *     possible object is
     *     {@link InvestmentObjectiveClassName }
     *     
     */
    public InvestmentObjectiveClassName getInvestmentObjectiveClass() {
        return investmentObjectiveClass;
    }

    /**
     * Sets the value of the investmentObjectiveClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvestmentObjectiveClassName }
     *     
     */
    public void setInvestmentObjectiveClass(InvestmentObjectiveClassName value) {
        this.investmentObjectiveClass = value;
    }

    /**
     * Gets the value of the indexLinked property.
     * This getter has been renamed from isIndexLinked() to getIndexLinked() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIndexLinked() {
        return indexLinked;
    }

    /**
     * Sets the value of the indexLinked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIndexLinked(Boolean value) {
        this.indexLinked = value;
    }

    /**
     * Gets the value of the unitLinked property.
     * This getter has been renamed from isUnitLinked() to getUnitLinked() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUnitLinked() {
        return unitLinked;
    }

    /**
     * Sets the value of the unitLinked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnitLinked(Boolean value) {
        this.unitLinked = value;
    }

    /**
     * Gets the value of the fundRegistration property.
     * 
     * @return
     *     possible object is
     *     {@link FundRegistrationTO }
     *     
     */
    public FundRegistrationTO getFundRegistration() {
        return fundRegistration;
    }

    /**
     * Sets the value of the fundRegistration property.
     * 
     * @param value
     *     allowed object is
     *     {@link FundRegistrationTO }
     *     
     */
    public void setFundRegistration(FundRegistrationTO value) {
        this.fundRegistration = value;
    }

    /**
     * Gets the value of the minimumInvestment property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMinimumInvestment() {
        return minimumInvestment;
    }

    /**
     * Sets the value of the minimumInvestment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMinimumInvestment(BaseCurrencyAmount value) {
        this.minimumInvestment = value;
    }

    /**
     * Gets the value of the assetCategory property.
     * 
     * @return
     *     possible object is
     *     {@link AssetCategoryTO }
     *     
     */
    public AssetCategoryTO getAssetCategory() {
        return assetCategory;
    }

    /**
     * Sets the value of the assetCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssetCategoryTO }
     *     
     */
    public void setAssetCategory(AssetCategoryTO value) {
        this.assetCategory = value;
    }

    /**
     * Sets the value of the trackingAccounts property.
     * 
     * @param trackingAccounts
     *     allowed object is
     *     {@link AccountTO }
     *     
     */
    public void setTrackingAccounts(List<AccountTO> trackingAccounts) {
        this.trackingAccounts = trackingAccounts;
    }

    /**
     * Sets the value of the trackingAccountReferences property.
     * 
     * @param trackingAccountReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setTrackingAccountReferences(List<ObjectReferenceTO> trackingAccountReferences) {
        this.trackingAccountReferences = trackingAccountReferences;
    }

}
