
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createSubmissionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createSubmissionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestHeader" type="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}RequestHeader"/>
 *         &lt;element name="submission" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRequest_TO"/>
 *         &lt;element name="customer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Customer_TO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createSubmissionRequest", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/", propOrder = {
    "requestHeader",
    "submission",
    "customer"
})
public class CreateSubmissionRequest {

    @XmlElement(required = true)
    protected RequestHeader requestHeader;
    @XmlElement(required = true)
    protected FinancialServicesRequestTO submission;
    @XmlElement(required = true)
    protected CustomerTO customer;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the submission property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesRequestTO }
     *     
     */
    public FinancialServicesRequestTO getSubmission() {
        return submission;
    }

    /**
     * Sets the value of the submission property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesRequestTO }
     *     
     */
    public void setSubmission(FinancialServicesRequestTO value) {
        this.submission = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerTO }
     *     
     */
    public CustomerTO getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerTO }
     *     
     */
    public void setCustomer(CustomerTO value) {
        this.customer = value;
    }

}
