
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaskPerformanceScoreType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaskPerformanceScoreType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/UtilityComponents/TaskManager/TaskManagerEnumerationsAndStates/}TaskPerformanceScoreTypeName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskPerformanceScoreType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/", propOrder = {
    "name"
})
public class TaskPerformanceScoreTypeTO
    extends TypeTO
{

    protected TaskPerformanceScoreTypeName name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link TaskPerformanceScoreTypeName }
     *     
     */
    public TaskPerformanceScoreTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskPerformanceScoreTypeName }
     *     
     */
    public void setName(TaskPerformanceScoreTypeName value) {
        this.name = value;
    }

}
