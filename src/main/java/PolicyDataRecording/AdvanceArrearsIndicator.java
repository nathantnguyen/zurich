
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdvanceArrearsIndicator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AdvanceArrearsIndicator">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="In Advance"/>
 *     &lt;enumeration value="In Arrears"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AdvanceArrearsIndicator", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum AdvanceArrearsIndicator {

    @XmlEnumValue("In Advance")
    IN_ADVANCE("In Advance"),
    @XmlEnumValue("In Arrears")
    IN_ARREARS("In Arrears");
    private final String value;

    AdvanceArrearsIndicator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdvanceArrearsIndicator fromValue(String v) {
        for (AdvanceArrearsIndicator c: AdvanceArrearsIndicator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
