
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlaceUsageInGeneralActivity_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PlaceUsageInGeneralActivity_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInGeneralActivity_TO">
 *       &lt;sequence>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="itinerary" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}Itinerary" minOccurs="0"/>
 *         &lt;element name="placeUsed" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlaceUsageInGeneralActivity_TO", propOrder = {
    "frequency",
    "itinerary",
    "placeUsed"
})
public class PlaceUsageInGeneralActivityTO
    extends RoleInGeneralActivityTO
{

    protected Frequency frequency;
    protected Itinerary itinerary;
    protected PlaceTO placeUsed;

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the itinerary property.
     * 
     * @return
     *     possible object is
     *     {@link Itinerary }
     *     
     */
    public Itinerary getItinerary() {
        return itinerary;
    }

    /**
     * Sets the value of the itinerary property.
     * 
     * @param value
     *     allowed object is
     *     {@link Itinerary }
     *     
     */
    public void setItinerary(Itinerary value) {
        this.itinerary = value;
    }

    /**
     * Gets the value of the placeUsed property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getPlaceUsed() {
        return placeUsed;
    }

    /**
     * Sets the value of the placeUsed property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setPlaceUsed(PlaceTO value) {
        this.placeUsed = value;
    }

}
