
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExchangeBasis.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ExchangeBasis">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Mid"/>
 *     &lt;enumeration value="Offer"/>
 *     &lt;enumeration value="Bid"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ExchangeBasis", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/")
@XmlEnum
public enum ExchangeBasis {

    @XmlEnumValue("Mid")
    MID("Mid"),
    @XmlEnumValue("Offer")
    OFFER("Offer"),
    @XmlEnumValue("Bid")
    BID("Bid");
    private final String value;

    ExchangeBasis(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExchangeBasis fromValue(String v) {
        for (ExchangeBasis c: ExchangeBasis.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
