
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DynamicTextBlock_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DynamicTextBlock_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}StandardTextSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="variableExpression" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DynamicTextBlock_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "variableExpression"
})
public class DynamicTextBlockTO
    extends StandardTextSpecificationTO
{

    protected String variableExpression;

    /**
     * Gets the value of the variableExpression property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVariableExpression() {
        return variableExpression;
    }

    /**
     * Sets the value of the variableExpression property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVariableExpression(String value) {
        this.variableExpression = value;
    }

}
