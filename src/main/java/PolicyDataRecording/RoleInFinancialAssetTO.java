
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInFinancialAsset_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInFinancialAsset_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="contextFinancialAsset" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInFinancialAsset_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "contextFinancialAsset"
})
public class RoleInFinancialAssetTO
    extends RoleInContextClassTO
{

    protected FinancialAssetTO contextFinancialAsset;

    /**
     * Gets the value of the contextFinancialAsset property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAssetTO }
     *     
     */
    public FinancialAssetTO getContextFinancialAsset() {
        return contextFinancialAsset;
    }

    /**
     * Sets the value of the contextFinancialAsset property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setContextFinancialAsset(FinancialAssetTO value) {
        this.contextFinancialAsset = value;
    }

}
