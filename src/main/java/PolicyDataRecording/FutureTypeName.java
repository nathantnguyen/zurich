
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FutureTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FutureTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Currency Future"/>
 *     &lt;enumeration value="Equity Future"/>
 *     &lt;enumeration value="Index Future"/>
 *     &lt;enumeration value="Interest Rate Future"/>
 *     &lt;enumeration value="Commodity Future"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FutureTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum FutureTypeName {

    @XmlEnumValue("Currency Future")
    CURRENCY_FUTURE("Currency Future"),
    @XmlEnumValue("Equity Future")
    EQUITY_FUTURE("Equity Future"),
    @XmlEnumValue("Index Future")
    INDEX_FUTURE("Index Future"),
    @XmlEnumValue("Interest Rate Future")
    INTEREST_RATE_FUTURE("Interest Rate Future"),
    @XmlEnumValue("Commodity Future")
    COMMODITY_FUTURE("Commodity Future");
    private final String value;

    FutureTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FutureTypeName fromValue(String v) {
        for (FutureTypeName c: FutureTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
