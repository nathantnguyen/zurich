
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MoneyProvision_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyProvision_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="moneyScheduler" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyScheduler_TO" minOccurs="0"/>
 *         &lt;element name="moneyProvisionRoles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionRole_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="moneyProvisionRelationships" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionRelationship_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="adjustments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}MoneyProvisionDeterminer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="baseIndex" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexValue_TO" minOccurs="0"/>
 *         &lt;element name="rolesInMoneyProvision" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}RoleInMoneyProvision_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyProvision_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "description",
    "startDate",
    "endDate",
    "moneyScheduler",
    "moneyProvisionRoles",
    "moneyProvisionRelationships",
    "adjustments",
    "baseIndex",
    "rolesInMoneyProvision"
})
@XmlSeeAlso({
    ParticularMoneyProvisionTO.class,
    GenericMoneyProvisionTO.class
})
public class MoneyProvisionTO
    extends BusinessModelObjectTO
{

    protected String description;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected MoneySchedulerTO moneyScheduler;
    protected List<MoneyProvisionRoleTO> moneyProvisionRoles;
    protected List<MoneyProvisionRelationshipTO> moneyProvisionRelationships;
    protected List<MoneyProvisionDeterminerTO> adjustments;
    protected IndexValueTO baseIndex;
    protected List<RoleInMoneyProvisionTO> rolesInMoneyProvision;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the moneyScheduler property.
     * 
     * @return
     *     possible object is
     *     {@link MoneySchedulerTO }
     *     
     */
    public MoneySchedulerTO getMoneyScheduler() {
        return moneyScheduler;
    }

    /**
     * Sets the value of the moneyScheduler property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneySchedulerTO }
     *     
     */
    public void setMoneyScheduler(MoneySchedulerTO value) {
        this.moneyScheduler = value;
    }

    /**
     * Gets the value of the moneyProvisionRoles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moneyProvisionRoles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoneyProvisionRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionRoleTO }
     * 
     * 
     */
    public List<MoneyProvisionRoleTO> getMoneyProvisionRoles() {
        if (moneyProvisionRoles == null) {
            moneyProvisionRoles = new ArrayList<MoneyProvisionRoleTO>();
        }
        return this.moneyProvisionRoles;
    }

    /**
     * Gets the value of the moneyProvisionRelationships property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moneyProvisionRelationships property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoneyProvisionRelationships().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionRelationshipTO }
     * 
     * 
     */
    public List<MoneyProvisionRelationshipTO> getMoneyProvisionRelationships() {
        if (moneyProvisionRelationships == null) {
            moneyProvisionRelationships = new ArrayList<MoneyProvisionRelationshipTO>();
        }
        return this.moneyProvisionRelationships;
    }

    /**
     * Gets the value of the adjustments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjustments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjustments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionDeterminerTO }
     * 
     * 
     */
    public List<MoneyProvisionDeterminerTO> getAdjustments() {
        if (adjustments == null) {
            adjustments = new ArrayList<MoneyProvisionDeterminerTO>();
        }
        return this.adjustments;
    }

    /**
     * Gets the value of the baseIndex property.
     * 
     * @return
     *     possible object is
     *     {@link IndexValueTO }
     *     
     */
    public IndexValueTO getBaseIndex() {
        return baseIndex;
    }

    /**
     * Sets the value of the baseIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexValueTO }
     *     
     */
    public void setBaseIndex(IndexValueTO value) {
        this.baseIndex = value;
    }

    /**
     * Gets the value of the rolesInMoneyProvision property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInMoneyProvision property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInMoneyProvision().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInMoneyProvisionTO }
     * 
     * 
     */
    public List<RoleInMoneyProvisionTO> getRolesInMoneyProvision() {
        if (rolesInMoneyProvision == null) {
            rolesInMoneyProvision = new ArrayList<RoleInMoneyProvisionTO>();
        }
        return this.rolesInMoneyProvision;
    }

    /**
     * Sets the value of the moneyProvisionRoles property.
     * 
     * @param moneyProvisionRoles
     *     allowed object is
     *     {@link MoneyProvisionRoleTO }
     *     
     */
    public void setMoneyProvisionRoles(List<MoneyProvisionRoleTO> moneyProvisionRoles) {
        this.moneyProvisionRoles = moneyProvisionRoles;
    }

    /**
     * Sets the value of the moneyProvisionRelationships property.
     * 
     * @param moneyProvisionRelationships
     *     allowed object is
     *     {@link MoneyProvisionRelationshipTO }
     *     
     */
    public void setMoneyProvisionRelationships(List<MoneyProvisionRelationshipTO> moneyProvisionRelationships) {
        this.moneyProvisionRelationships = moneyProvisionRelationships;
    }

    /**
     * Sets the value of the adjustments property.
     * 
     * @param adjustments
     *     allowed object is
     *     {@link MoneyProvisionDeterminerTO }
     *     
     */
    public void setAdjustments(List<MoneyProvisionDeterminerTO> adjustments) {
        this.adjustments = adjustments;
    }

    /**
     * Sets the value of the rolesInMoneyProvision property.
     * 
     * @param rolesInMoneyProvision
     *     allowed object is
     *     {@link RoleInMoneyProvisionTO }
     *     
     */
    public void setRolesInMoneyProvision(List<RoleInMoneyProvisionTO> rolesInMoneyProvision) {
        this.rolesInMoneyProvision = rolesInMoneyProvision;
    }

}
