
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NonForfeitureOption.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NonForfeitureOption">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Apl"/>
 *     &lt;enumeration value="Extended Term"/>
 *     &lt;enumeration value="Forfeit Policy For Cash Surrender Value"/>
 *     &lt;enumeration value="Reduced Paid Up"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NonForfeitureOption", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum NonForfeitureOption {

    @XmlEnumValue("Apl")
    APL("Apl"),
    @XmlEnumValue("Extended Term")
    EXTENDED_TERM("Extended Term"),
    @XmlEnumValue("Forfeit Policy For Cash Surrender Value")
    FORFEIT_POLICY_FOR_CASH_SURRENDER_VALUE("Forfeit Policy For Cash Surrender Value"),
    @XmlEnumValue("Reduced Paid Up")
    REDUCED_PAID_UP("Reduced Paid Up");
    private final String value;

    NonForfeitureOption(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NonForfeitureOption fromValue(String v) {
        for (NonForfeitureOption c: NonForfeitureOption.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
