
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Request_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Request_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Actual_TO">
 *       &lt;sequence>
 *         &lt;element name="requestDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="requestedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="isValidRequest" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}TriStateLogic" minOccurs="0"/>
 *         &lt;element name="targetActual" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementActual_TO" minOccurs="0"/>
 *         &lt;element name="resultActual" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementActual_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Request_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "requestDate",
    "requestedDate",
    "isValidRequest",
    "targetActual",
    "resultActual"
})
public class RequestTO
    extends ActualTO
{

    protected XMLGregorianCalendar requestDate;
    protected XMLGregorianCalendar requestedDate;
    protected TriStateLogic isValidRequest;
    protected AgreementActualTO targetActual;
    protected AgreementActualTO resultActual;

    /**
     * Gets the value of the requestDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestDate() {
        return requestDate;
    }

    /**
     * Sets the value of the requestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestDate(XMLGregorianCalendar value) {
        this.requestDate = value;
    }

    /**
     * Gets the value of the requestedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestedDate() {
        return requestedDate;
    }

    /**
     * Sets the value of the requestedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestedDate(XMLGregorianCalendar value) {
        this.requestedDate = value;
    }

    /**
     * Gets the value of the isValidRequest property.
     * 
     * @return
     *     possible object is
     *     {@link TriStateLogic }
     *     
     */
    public TriStateLogic getIsValidRequest() {
        return isValidRequest;
    }

    /**
     * Sets the value of the isValidRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link TriStateLogic }
     *     
     */
    public void setIsValidRequest(TriStateLogic value) {
        this.isValidRequest = value;
    }

    /**
     * Gets the value of the targetActual property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementActualTO }
     *     
     */
    public AgreementActualTO getTargetActual() {
        return targetActual;
    }

    /**
     * Sets the value of the targetActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementActualTO }
     *     
     */
    public void setTargetActual(AgreementActualTO value) {
        this.targetActual = value;
    }

    /**
     * Gets the value of the resultActual property.
     * 
     * @return
     *     possible object is
     *     {@link AgreementActualTO }
     *     
     */
    public AgreementActualTO getResultActual() {
        return resultActual;
    }

    /**
     * Sets the value of the resultActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgreementActualTO }
     *     
     */
    public void setResultActual(AgreementActualTO value) {
        this.resultActual = value;
    }

}
