
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractRelationshipTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContractRelationshipTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Contract Facultative Reinsurance"/>
 *     &lt;enumeration value="Contract Integration"/>
 *     &lt;enumeration value="Contract Payment"/>
 *     &lt;enumeration value="Embedded Derivative"/>
 *     &lt;enumeration value="Contract Administration"/>
 *     &lt;enumeration value="Contract Conversion"/>
 *     &lt;enumeration value="Contract Replacement"/>
 *     &lt;enumeration value="Contract Commutation"/>
 *     &lt;enumeration value="Contract Master"/>
 *     &lt;enumeration value="Contract Reference"/>
 *     &lt;enumeration value="Financial Services Agreement Producer"/>
 *     &lt;enumeration value="Financial Services Agreement Lifecycle Transition"/>
 *     &lt;enumeration value="Contract Renewal"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContractRelationshipTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum ContractRelationshipTypeName {

    @XmlEnumValue("Contract Facultative Reinsurance")
    CONTRACT_FACULTATIVE_REINSURANCE("Contract Facultative Reinsurance"),
    @XmlEnumValue("Contract Integration")
    CONTRACT_INTEGRATION("Contract Integration"),
    @XmlEnumValue("Contract Payment")
    CONTRACT_PAYMENT("Contract Payment"),
    @XmlEnumValue("Embedded Derivative")
    EMBEDDED_DERIVATIVE("Embedded Derivative"),
    @XmlEnumValue("Contract Administration")
    CONTRACT_ADMINISTRATION("Contract Administration"),
    @XmlEnumValue("Contract Conversion")
    CONTRACT_CONVERSION("Contract Conversion"),
    @XmlEnumValue("Contract Replacement")
    CONTRACT_REPLACEMENT("Contract Replacement"),
    @XmlEnumValue("Contract Commutation")
    CONTRACT_COMMUTATION("Contract Commutation"),
    @XmlEnumValue("Contract Master")
    CONTRACT_MASTER("Contract Master"),
    @XmlEnumValue("Contract Reference")
    CONTRACT_REFERENCE("Contract Reference"),
    @XmlEnumValue("Financial Services Agreement Producer")
    FINANCIAL_SERVICES_AGREEMENT_PRODUCER("Financial Services Agreement Producer"),
    @XmlEnumValue("Financial Services Agreement Lifecycle Transition")
    FINANCIAL_SERVICES_AGREEMENT_LIFECYCLE_TRANSITION("Financial Services Agreement Lifecycle Transition"),
    @XmlEnumValue("Contract Renewal")
    CONTRACT_RENEWAL("Contract Renewal");
    private final String value;

    ContractRelationshipTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContractRelationshipTypeName fromValue(String v) {
        for (ContractRelationshipTypeName c: ContractRelationshipTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
