
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInIntermediaryAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInIntermediaryAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContract_TO">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInIntermediaryAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/")
@XmlSeeAlso({
    SpecificationInvolvedInIntermediaryAgreementTO.class,
    PartyInvolvedInIntermediaryAgreementTO.class
})
public class RoleInIntermediaryAgreementTO
    extends RoleInContractTO
{


}
