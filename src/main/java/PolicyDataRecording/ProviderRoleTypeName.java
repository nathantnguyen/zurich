
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProviderRoleTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProviderRoleTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Supplier"/>
 *     &lt;enumeration value="Fund Manager"/>
 *     &lt;enumeration value="Mutual Funds Provider"/>
 *     &lt;enumeration value="Information Provider"/>
 *     &lt;enumeration value="Credit Specialist"/>
 *     &lt;enumeration value="Financial Adviser"/>
 *     &lt;enumeration value="Legal Adviser"/>
 *     &lt;enumeration value="Auditor"/>
 *     &lt;enumeration value="Financial Analyst"/>
 *     &lt;enumeration value="Buyer"/>
 *     &lt;enumeration value="Carrier"/>
 *     &lt;enumeration value="Contractor"/>
 *     &lt;enumeration value="Subcontractor"/>
 *     &lt;enumeration value="Lessee"/>
 *     &lt;enumeration value="Lessor"/>
 *     &lt;enumeration value="Outsourcing Service Provider"/>
 *     &lt;enumeration value="Social Media Service Provider"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProviderRoleTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum ProviderRoleTypeName {

    @XmlEnumValue("Supplier")
    SUPPLIER("Supplier"),
    @XmlEnumValue("Fund Manager")
    FUND_MANAGER("Fund Manager"),
    @XmlEnumValue("Mutual Funds Provider")
    MUTUAL_FUNDS_PROVIDER("Mutual Funds Provider"),
    @XmlEnumValue("Information Provider")
    INFORMATION_PROVIDER("Information Provider"),
    @XmlEnumValue("Credit Specialist")
    CREDIT_SPECIALIST("Credit Specialist"),
    @XmlEnumValue("Financial Adviser")
    FINANCIAL_ADVISER("Financial Adviser"),
    @XmlEnumValue("Legal Adviser")
    LEGAL_ADVISER("Legal Adviser"),
    @XmlEnumValue("Auditor")
    AUDITOR("Auditor"),
    @XmlEnumValue("Financial Analyst")
    FINANCIAL_ANALYST("Financial Analyst"),
    @XmlEnumValue("Buyer")
    BUYER("Buyer"),
    @XmlEnumValue("Carrier")
    CARRIER("Carrier"),
    @XmlEnumValue("Contractor")
    CONTRACTOR("Contractor"),
    @XmlEnumValue("Subcontractor")
    SUBCONTRACTOR("Subcontractor"),
    @XmlEnumValue("Lessee")
    LESSEE("Lessee"),
    @XmlEnumValue("Lessor")
    LESSOR("Lessor"),
    @XmlEnumValue("Outsourcing Service Provider")
    OUTSOURCING_SERVICE_PROVIDER("Outsourcing Service Provider"),
    @XmlEnumValue("Social Media Service Provider")
    SOCIAL_MEDIA_SERVICE_PROVIDER("Social Media Service Provider");
    private final String value;

    ProviderRoleTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProviderRoleTypeName fromValue(String v) {
        for (ProviderRoleTypeName c: ProviderRoleTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
