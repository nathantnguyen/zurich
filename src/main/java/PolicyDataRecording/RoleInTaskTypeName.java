
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInTaskTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInTaskTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Litigation Activity In Legal Action"/>
 *     &lt;enumeration value="Task Risk Assessment"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInTaskTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/UtilityComponents/TaskManager/TaskManagerEnumerationsAndStates/")
@XmlEnum
public enum RoleInTaskTypeName {

    @XmlEnumValue("Litigation Activity In Legal Action")
    LITIGATION_ACTIVITY_IN_LEGAL_ACTION("Litigation Activity In Legal Action"),
    @XmlEnumValue("Task Risk Assessment")
    TASK_RISK_ASSESSMENT("Task Risk Assessment");
    private final String value;

    RoleInTaskTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInTaskTypeName fromValue(String v) {
        for (RoleInTaskTypeName c: RoleInTaskTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
