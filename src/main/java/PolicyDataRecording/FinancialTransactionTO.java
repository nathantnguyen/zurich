
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FinancialTransaction_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialTransaction_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="postedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="postedToAccountEntries" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AccountEntry_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}PaymentMethod_TO" minOccurs="0"/>
 *         &lt;element name="financialTransactionRelationships" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}FinancialTransactionRelationship_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInFinancialTransaction" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}RoleInFinancialTransaction_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="paymentFacilityReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="paymentFacilityRequestReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="payerReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="payerName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="payerExternalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="payeeReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="payeeName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="payeeExternalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="moneyScheduler" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyScheduler_TO" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialTransaction_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "amount",
    "externalReference",
    "postedDate",
    "description",
    "postedToAccountEntries",
    "paymentMethod",
    "financialTransactionRelationships",
    "rolesInFinancialTransaction",
    "paymentFacilityReference",
    "paymentFacilityRequestReferences",
    "payerReference",
    "payerName",
    "payerExternalReference",
    "payeeReference",
    "payeeName",
    "payeeExternalReference",
    "moneyScheduler",
    "alternateReference"
})
@XmlSeeAlso({
    PaymentTO.class,
    PaymentDueTO.class
})
public class FinancialTransactionTO
    extends BusinessModelObjectTO
{

    protected BaseCurrencyAmount amount;
    protected String externalReference;
    protected XMLGregorianCalendar postedDate;
    protected String description;
    protected List<AccountEntryTO> postedToAccountEntries;
    protected PaymentMethodTO paymentMethod;
    protected List<FinancialTransactionRelationshipTO> financialTransactionRelationships;
    protected List<RoleInFinancialTransactionTO> rolesInFinancialTransaction;
    protected ObjectReferenceTO paymentFacilityReference;
    protected List<ObjectReferenceTO> paymentFacilityRequestReferences;
    protected ObjectReferenceTO payerReference;
    protected String payerName;
    protected String payerExternalReference;
    protected ObjectReferenceTO payeeReference;
    protected String payeeName;
    protected String payeeExternalReference;
    protected MoneySchedulerTO moneyScheduler;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAmount(BaseCurrencyAmount value) {
        this.amount = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the postedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPostedDate() {
        return postedDate;
    }

    /**
     * Sets the value of the postedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPostedDate(XMLGregorianCalendar value) {
        this.postedDate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the postedToAccountEntries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the postedToAccountEntries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPostedToAccountEntries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountEntryTO }
     * 
     * 
     */
    public List<AccountEntryTO> getPostedToAccountEntries() {
        if (postedToAccountEntries == null) {
            postedToAccountEntries = new ArrayList<AccountEntryTO>();
        }
        return this.postedToAccountEntries;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMethodTO }
     *     
     */
    public PaymentMethodTO getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMethodTO }
     *     
     */
    public void setPaymentMethod(PaymentMethodTO value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the financialTransactionRelationships property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the financialTransactionRelationships property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFinancialTransactionRelationships().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialTransactionRelationshipTO }
     * 
     * 
     */
    public List<FinancialTransactionRelationshipTO> getFinancialTransactionRelationships() {
        if (financialTransactionRelationships == null) {
            financialTransactionRelationships = new ArrayList<FinancialTransactionRelationshipTO>();
        }
        return this.financialTransactionRelationships;
    }

    /**
     * Gets the value of the rolesInFinancialTransaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInFinancialTransaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInFinancialTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInFinancialTransactionTO }
     * 
     * 
     */
    public List<RoleInFinancialTransactionTO> getRolesInFinancialTransaction() {
        if (rolesInFinancialTransaction == null) {
            rolesInFinancialTransaction = new ArrayList<RoleInFinancialTransactionTO>();
        }
        return this.rolesInFinancialTransaction;
    }

    /**
     * Gets the value of the paymentFacilityReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getPaymentFacilityReference() {
        return paymentFacilityReference;
    }

    /**
     * Sets the value of the paymentFacilityReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setPaymentFacilityReference(ObjectReferenceTO value) {
        this.paymentFacilityReference = value;
    }

    /**
     * Gets the value of the paymentFacilityRequestReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentFacilityRequestReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentFacilityRequestReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getPaymentFacilityRequestReferences() {
        if (paymentFacilityRequestReferences == null) {
            paymentFacilityRequestReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.paymentFacilityRequestReferences;
    }

    /**
     * Gets the value of the payerReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getPayerReference() {
        return payerReference;
    }

    /**
     * Sets the value of the payerReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setPayerReference(ObjectReferenceTO value) {
        this.payerReference = value;
    }

    /**
     * Gets the value of the payerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayerName() {
        return payerName;
    }

    /**
     * Sets the value of the payerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayerName(String value) {
        this.payerName = value;
    }

    /**
     * Gets the value of the payerExternalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayerExternalReference() {
        return payerExternalReference;
    }

    /**
     * Sets the value of the payerExternalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayerExternalReference(String value) {
        this.payerExternalReference = value;
    }

    /**
     * Gets the value of the payeeReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getPayeeReference() {
        return payeeReference;
    }

    /**
     * Sets the value of the payeeReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setPayeeReference(ObjectReferenceTO value) {
        this.payeeReference = value;
    }

    /**
     * Gets the value of the payeeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayeeName() {
        return payeeName;
    }

    /**
     * Sets the value of the payeeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayeeName(String value) {
        this.payeeName = value;
    }

    /**
     * Gets the value of the payeeExternalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayeeExternalReference() {
        return payeeExternalReference;
    }

    /**
     * Sets the value of the payeeExternalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayeeExternalReference(String value) {
        this.payeeExternalReference = value;
    }

    /**
     * Gets the value of the moneyScheduler property.
     * 
     * @return
     *     possible object is
     *     {@link MoneySchedulerTO }
     *     
     */
    public MoneySchedulerTO getMoneyScheduler() {
        return moneyScheduler;
    }

    /**
     * Sets the value of the moneyScheduler property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneySchedulerTO }
     *     
     */
    public void setMoneyScheduler(MoneySchedulerTO value) {
        this.moneyScheduler = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the postedToAccountEntries property.
     * 
     * @param postedToAccountEntries
     *     allowed object is
     *     {@link AccountEntryTO }
     *     
     */
    public void setPostedToAccountEntries(List<AccountEntryTO> postedToAccountEntries) {
        this.postedToAccountEntries = postedToAccountEntries;
    }

    /**
     * Sets the value of the financialTransactionRelationships property.
     * 
     * @param financialTransactionRelationships
     *     allowed object is
     *     {@link FinancialTransactionRelationshipTO }
     *     
     */
    public void setFinancialTransactionRelationships(List<FinancialTransactionRelationshipTO> financialTransactionRelationships) {
        this.financialTransactionRelationships = financialTransactionRelationships;
    }

    /**
     * Sets the value of the rolesInFinancialTransaction property.
     * 
     * @param rolesInFinancialTransaction
     *     allowed object is
     *     {@link RoleInFinancialTransactionTO }
     *     
     */
    public void setRolesInFinancialTransaction(List<RoleInFinancialTransactionTO> rolesInFinancialTransaction) {
        this.rolesInFinancialTransaction = rolesInFinancialTransaction;
    }

    /**
     * Sets the value of the paymentFacilityRequestReferences property.
     * 
     * @param paymentFacilityRequestReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setPaymentFacilityRequestReferences(List<ObjectReferenceTO> paymentFacilityRequestReferences) {
        this.paymentFacilityRequestReferences = paymentFacilityRequestReferences;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
