
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConstantRoleSpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConstantRoleSpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RelationshipSpec_TO">
 *       &lt;sequence>
 *         &lt;element name="constantProperties" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ConstantPropertySpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstantRoleSpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "constantProperties"
})
public class ConstantRoleSpecTO
    extends RelationshipSpecTO
{

    protected List<ConstantPropertySpecTO> constantProperties;

    /**
     * Gets the value of the constantProperties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the constantProperties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConstantProperties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConstantPropertySpecTO }
     * 
     * 
     */
    public List<ConstantPropertySpecTO> getConstantProperties() {
        if (constantProperties == null) {
            constantProperties = new ArrayList<ConstantPropertySpecTO>();
        }
        return this.constantProperties;
    }

    /**
     * Sets the value of the constantProperties property.
     * 
     * @param constantProperties
     *     allowed object is
     *     {@link ConstantPropertySpecTO }
     *     
     */
    public void setConstantProperties(List<ConstantPropertySpecTO> constantProperties) {
        this.constantProperties = constantProperties;
    }

}
