
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VisualProfile_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VisualProfile_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CommunicationProfile_TO">
 *       &lt;sequence>
 *         &lt;element name="textSize" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}TextSize" minOccurs="0"/>
 *         &lt;element name="fontSelection" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}FontSelection" minOccurs="0"/>
 *         &lt;element name="textColour" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Colour" minOccurs="0"/>
 *         &lt;element name="imageReplacement" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="highContrast" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}HighContrast" minOccurs="0"/>
 *         &lt;element name="colourAvoidance" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Colour" minOccurs="0"/>
 *         &lt;element name="braille" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="backgroundColour" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Colour" minOccurs="0"/>
 *         &lt;element name="animationAvoidance" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="colourCombinationAvoidance" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}ColourCombinationAvoidance" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VisualProfile_TO", propOrder = {
    "textSize",
    "fontSelection",
    "textColour",
    "imageReplacement",
    "highContrast",
    "colourAvoidance",
    "braille",
    "backgroundColour",
    "animationAvoidance",
    "colourCombinationAvoidance"
})
public class VisualProfileTO
    extends CommunicationProfileTO
{

    protected TextSize textSize;
    protected FontSelection fontSelection;
    protected Colour textColour;
    protected Boolean imageReplacement;
    protected HighContrast highContrast;
    protected Colour colourAvoidance;
    protected Boolean braille;
    protected Colour backgroundColour;
    protected Boolean animationAvoidance;
    protected ColourCombinationAvoidance colourCombinationAvoidance;

    /**
     * Gets the value of the textSize property.
     * 
     * @return
     *     possible object is
     *     {@link TextSize }
     *     
     */
    public TextSize getTextSize() {
        return textSize;
    }

    /**
     * Sets the value of the textSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextSize }
     *     
     */
    public void setTextSize(TextSize value) {
        this.textSize = value;
    }

    /**
     * Gets the value of the fontSelection property.
     * 
     * @return
     *     possible object is
     *     {@link FontSelection }
     *     
     */
    public FontSelection getFontSelection() {
        return fontSelection;
    }

    /**
     * Sets the value of the fontSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link FontSelection }
     *     
     */
    public void setFontSelection(FontSelection value) {
        this.fontSelection = value;
    }

    /**
     * Gets the value of the textColour property.
     * 
     * @return
     *     possible object is
     *     {@link Colour }
     *     
     */
    public Colour getTextColour() {
        return textColour;
    }

    /**
     * Sets the value of the textColour property.
     * 
     * @param value
     *     allowed object is
     *     {@link Colour }
     *     
     */
    public void setTextColour(Colour value) {
        this.textColour = value;
    }

    /**
     * Gets the value of the imageReplacement property.
     * This getter has been renamed from isImageReplacement() to getImageReplacement() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getImageReplacement() {
        return imageReplacement;
    }

    /**
     * Sets the value of the imageReplacement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setImageReplacement(Boolean value) {
        this.imageReplacement = value;
    }

    /**
     * Gets the value of the highContrast property.
     * 
     * @return
     *     possible object is
     *     {@link HighContrast }
     *     
     */
    public HighContrast getHighContrast() {
        return highContrast;
    }

    /**
     * Sets the value of the highContrast property.
     * 
     * @param value
     *     allowed object is
     *     {@link HighContrast }
     *     
     */
    public void setHighContrast(HighContrast value) {
        this.highContrast = value;
    }

    /**
     * Gets the value of the colourAvoidance property.
     * 
     * @return
     *     possible object is
     *     {@link Colour }
     *     
     */
    public Colour getColourAvoidance() {
        return colourAvoidance;
    }

    /**
     * Sets the value of the colourAvoidance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Colour }
     *     
     */
    public void setColourAvoidance(Colour value) {
        this.colourAvoidance = value;
    }

    /**
     * Gets the value of the braille property.
     * This getter has been renamed from isBraille() to getBraille() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBraille() {
        return braille;
    }

    /**
     * Sets the value of the braille property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBraille(Boolean value) {
        this.braille = value;
    }

    /**
     * Gets the value of the backgroundColour property.
     * 
     * @return
     *     possible object is
     *     {@link Colour }
     *     
     */
    public Colour getBackgroundColour() {
        return backgroundColour;
    }

    /**
     * Sets the value of the backgroundColour property.
     * 
     * @param value
     *     allowed object is
     *     {@link Colour }
     *     
     */
    public void setBackgroundColour(Colour value) {
        this.backgroundColour = value;
    }

    /**
     * Gets the value of the animationAvoidance property.
     * This getter has been renamed from isAnimationAvoidance() to getAnimationAvoidance() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAnimationAvoidance() {
        return animationAvoidance;
    }

    /**
     * Sets the value of the animationAvoidance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAnimationAvoidance(Boolean value) {
        this.animationAvoidance = value;
    }

    /**
     * Gets the value of the colourCombinationAvoidance property.
     * 
     * @return
     *     possible object is
     *     {@link ColourCombinationAvoidance }
     *     
     */
    public ColourCombinationAvoidance getColourCombinationAvoidance() {
        return colourCombinationAvoidance;
    }

    /**
     * Sets the value of the colourCombinationAvoidance property.
     * 
     * @param value
     *     allowed object is
     *     {@link ColourCombinationAvoidance }
     *     
     */
    public void setColourCombinationAvoidance(ColourCombinationAvoidance value) {
        this.colourCombinationAvoidance = value;
    }

}
