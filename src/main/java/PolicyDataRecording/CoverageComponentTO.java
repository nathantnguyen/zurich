
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CoverageComponent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoverageComponent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreementComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="exposureAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="retroactiveStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="coveragePriority" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}CoveragePriority" minOccurs="0"/>
 *         &lt;element name="eligibilityPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="extendedReportingPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="cumulativePremiumAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="totalPremiumAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="totalNetPremiumAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="riskExposures" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskExposure_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="coverageComponentDetail" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CoverageComponentDetail_TO" minOccurs="0"/>
 *         &lt;element name="coverageSymbol" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoverageComponent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "exposureAmount",
    "retroactiveStartDate",
    "coveragePriority",
    "eligibilityPeriod",
    "extendedReportingPeriod",
    "cumulativePremiumAmount",
    "totalPremiumAmount",
    "totalNetPremiumAmount",
    "riskExposures",
    "coverageComponentDetail",
    "coverageSymbol"
})
@XmlSeeAlso({
    DisabilityCoverageTO.class,
    AnnuityCoverageTO.class,
    RiderTO.class,
    CommercialVehicleCoverageTO.class,
    DeathCoverageTO.class
})
public class CoverageComponentTO
    extends FinancialServicesAgreementComponentTO
{

    protected Amount exposureAmount;
    protected XMLGregorianCalendar retroactiveStartDate;
    protected CoveragePriority coveragePriority;
    protected Duration eligibilityPeriod;
    protected Duration extendedReportingPeriod;
    protected BaseCurrencyAmount cumulativePremiumAmount;
    protected BaseCurrencyAmount totalPremiumAmount;
    protected BaseCurrencyAmount totalNetPremiumAmount;
    protected List<RiskExposureTO> riskExposures;
    protected CoverageComponentDetailTO coverageComponentDetail;
    protected List<String> coverageSymbol;

    /**
     * Gets the value of the exposureAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getExposureAmount() {
        return exposureAmount;
    }

    /**
     * Sets the value of the exposureAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setExposureAmount(Amount value) {
        this.exposureAmount = value;
    }

    /**
     * Gets the value of the retroactiveStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRetroactiveStartDate() {
        return retroactiveStartDate;
    }

    /**
     * Sets the value of the retroactiveStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRetroactiveStartDate(XMLGregorianCalendar value) {
        this.retroactiveStartDate = value;
    }

    /**
     * Gets the value of the coveragePriority property.
     * 
     * @return
     *     possible object is
     *     {@link CoveragePriority }
     *     
     */
    public CoveragePriority getCoveragePriority() {
        return coveragePriority;
    }

    /**
     * Sets the value of the coveragePriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoveragePriority }
     *     
     */
    public void setCoveragePriority(CoveragePriority value) {
        this.coveragePriority = value;
    }

    /**
     * Gets the value of the eligibilityPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getEligibilityPeriod() {
        return eligibilityPeriod;
    }

    /**
     * Sets the value of the eligibilityPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setEligibilityPeriod(Duration value) {
        this.eligibilityPeriod = value;
    }

    /**
     * Gets the value of the extendedReportingPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getExtendedReportingPeriod() {
        return extendedReportingPeriod;
    }

    /**
     * Sets the value of the extendedReportingPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setExtendedReportingPeriod(Duration value) {
        this.extendedReportingPeriod = value;
    }

    /**
     * Gets the value of the cumulativePremiumAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCumulativePremiumAmount() {
        return cumulativePremiumAmount;
    }

    /**
     * Sets the value of the cumulativePremiumAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCumulativePremiumAmount(BaseCurrencyAmount value) {
        this.cumulativePremiumAmount = value;
    }

    /**
     * Gets the value of the totalPremiumAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTotalPremiumAmount() {
        return totalPremiumAmount;
    }

    /**
     * Sets the value of the totalPremiumAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTotalPremiumAmount(BaseCurrencyAmount value) {
        this.totalPremiumAmount = value;
    }

    /**
     * Gets the value of the totalNetPremiumAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTotalNetPremiumAmount() {
        return totalNetPremiumAmount;
    }

    /**
     * Sets the value of the totalNetPremiumAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTotalNetPremiumAmount(BaseCurrencyAmount value) {
        this.totalNetPremiumAmount = value;
    }

    /**
     * Gets the value of the riskExposures property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riskExposures property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRiskExposures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskExposureTO }
     * 
     * 
     */
    public List<RiskExposureTO> getRiskExposures() {
        if (riskExposures == null) {
            riskExposures = new ArrayList<RiskExposureTO>();
        }
        return this.riskExposures;
    }

    /**
     * Gets the value of the coverageComponentDetail property.
     * 
     * @return
     *     possible object is
     *     {@link CoverageComponentDetailTO }
     *     
     */
    public CoverageComponentDetailTO getCoverageComponentDetail() {
        return coverageComponentDetail;
    }

    /**
     * Sets the value of the coverageComponentDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverageComponentDetailTO }
     *     
     */
    public void setCoverageComponentDetail(CoverageComponentDetailTO value) {
        this.coverageComponentDetail = value;
    }

    /**
     * Gets the value of the coverageSymbol property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coverageSymbol property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoverageSymbol().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCoverageSymbol() {
        if (coverageSymbol == null) {
            coverageSymbol = new ArrayList<String>();
        }
        return this.coverageSymbol;
    }

    /**
     * Sets the value of the riskExposures property.
     * 
     * @param riskExposures
     *     allowed object is
     *     {@link RiskExposureTO }
     *     
     */
    public void setRiskExposures(List<RiskExposureTO> riskExposures) {
        this.riskExposures = riskExposures;
    }

    /**
     * Sets the value of the coverageSymbol property.
     * 
     * @param coverageSymbol
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverageSymbol(List<String> coverageSymbol) {
        this.coverageSymbol = coverageSymbol;
    }

}
