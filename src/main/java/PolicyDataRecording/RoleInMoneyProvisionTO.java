
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInMoneyProvision_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInMoneyProvision_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="roleInMoneyProvisionRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}RoleInMoneyProvisionType_TO" minOccurs="0"/>
 *         &lt;element name="contextMoneyProvision" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInMoneyProvision_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "roleInMoneyProvisionRootType",
    "contextMoneyProvision"
})
public class RoleInMoneyProvisionTO
    extends RoleInContextClassTO
{

    protected RoleInMoneyProvisionTypeTO roleInMoneyProvisionRootType;
    protected MoneyProvisionTO contextMoneyProvision;

    /**
     * Gets the value of the roleInMoneyProvisionRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInMoneyProvisionTypeTO }
     *     
     */
    public RoleInMoneyProvisionTypeTO getRoleInMoneyProvisionRootType() {
        return roleInMoneyProvisionRootType;
    }

    /**
     * Sets the value of the roleInMoneyProvisionRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInMoneyProvisionTypeTO }
     *     
     */
    public void setRoleInMoneyProvisionRootType(RoleInMoneyProvisionTypeTO value) {
        this.roleInMoneyProvisionRootType = value;
    }

    /**
     * Gets the value of the contextMoneyProvision property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public MoneyProvisionTO getContextMoneyProvision() {
        return contextMoneyProvision;
    }

    /**
     * Sets the value of the contextMoneyProvision property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public void setContextMoneyProvision(MoneyProvisionTO value) {
        this.contextMoneyProvision = value;
    }

}
