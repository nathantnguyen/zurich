
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BenefitObligation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BenefitObligation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}ObligationComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="benefitStructure" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}BenefitStructure" minOccurs="0"/>
 *         &lt;element name="benefitType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}BenefitType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BenefitObligation_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "benefitStructure",
    "benefitType"
})
public class BenefitObligationTO
    extends ObligationComponentTO
{

    protected BenefitStructure benefitStructure;
    protected BenefitType benefitType;

    /**
     * Gets the value of the benefitStructure property.
     * 
     * @return
     *     possible object is
     *     {@link BenefitStructure }
     *     
     */
    public BenefitStructure getBenefitStructure() {
        return benefitStructure;
    }

    /**
     * Sets the value of the benefitStructure property.
     * 
     * @param value
     *     allowed object is
     *     {@link BenefitStructure }
     *     
     */
    public void setBenefitStructure(BenefitStructure value) {
        this.benefitStructure = value;
    }

    /**
     * Gets the value of the benefitType property.
     * 
     * @return
     *     possible object is
     *     {@link BenefitType }
     *     
     */
    public BenefitType getBenefitType() {
        return benefitType;
    }

    /**
     * Sets the value of the benefitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BenefitType }
     *     
     */
    public void setBenefitType(BenefitType value) {
        this.benefitType = value;
    }

}
