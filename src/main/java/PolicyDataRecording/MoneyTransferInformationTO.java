
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyTransferInformation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyTransferInformation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="accountNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="bank" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO" minOccurs="0"/>
 *         &lt;element name="fee" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElementPart_TO" minOccurs="0"/>
 *         &lt;element name="accountHolder" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Party_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyTransferInformation_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "accountNumber",
    "bank",
    "fee",
    "accountHolder"
})
@XmlSeeAlso({
    DirectDebitMandateDetailsTO.class
})
public class MoneyTransferInformationTO
    extends BusinessModelObjectTO
{

    protected String accountNumber;
    protected OrganisationTO bank;
    protected MoneyProvisionElementPartTO fee;
    protected List<PartyTO> accountHolder;

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the bank property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationTO }
     *     
     */
    public OrganisationTO getBank() {
        return bank;
    }

    /**
     * Sets the value of the bank property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationTO }
     *     
     */
    public void setBank(OrganisationTO value) {
        this.bank = value;
    }

    /**
     * Gets the value of the fee property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionElementPartTO }
     *     
     */
    public MoneyProvisionElementPartTO getFee() {
        return fee;
    }

    /**
     * Sets the value of the fee property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionElementPartTO }
     *     
     */
    public void setFee(MoneyProvisionElementPartTO value) {
        this.fee = value;
    }

    /**
     * Gets the value of the accountHolder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountHolder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountHolder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyTO }
     * 
     * 
     */
    public List<PartyTO> getAccountHolder() {
        if (accountHolder == null) {
            accountHolder = new ArrayList<PartyTO>();
        }
        return this.accountHolder;
    }

    /**
     * Sets the value of the accountHolder property.
     * 
     * @param accountHolder
     *     allowed object is
     *     {@link PartyTO }
     *     
     */
    public void setAccountHolder(List<PartyTO> accountHolder) {
        this.accountHolder = accountHolder;
    }

}
