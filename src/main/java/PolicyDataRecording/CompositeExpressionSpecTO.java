
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompositeExpressionSpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompositeExpressionSpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ExpressionSpec_TO">
 *       &lt;sequence>
 *         &lt;element name="operator" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/}Operator" minOccurs="0"/>
 *         &lt;element name="operands" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ExpressionSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompositeExpressionSpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "operator",
    "operands"
})
public class CompositeExpressionSpecTO
    extends ExpressionSpecTO
{

    protected Operator operator;
    protected List<ExpressionSpecTO> operands;

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link Operator }
     *     
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Operator }
     *     
     */
    public void setOperator(Operator value) {
        this.operator = value;
    }

    /**
     * Gets the value of the operands property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the operands property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperands().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExpressionSpecTO }
     * 
     * 
     */
    public List<ExpressionSpecTO> getOperands() {
        if (operands == null) {
            operands = new ArrayList<ExpressionSpecTO>();
        }
        return this.operands;
    }

    /**
     * Sets the value of the operands property.
     * 
     * @param operands
     *     allowed object is
     *     {@link ExpressionSpecTO }
     *     
     */
    public void setOperands(List<ExpressionSpecTO> operands) {
        this.operands = operands;
    }

}
