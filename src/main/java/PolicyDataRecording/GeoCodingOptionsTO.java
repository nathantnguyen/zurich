
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeoCodingOptions_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeoCodingOptions_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="preferedDatabase" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}GeoCodeDatabase" minOccurs="0"/>
 *         &lt;element name="offsetFromStreet" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="offsetFromCorner" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="coordinateSystem" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}GeographicCoordinateSystemType" minOccurs="0"/>
 *         &lt;element name="fallbackToPostalCentroid" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="fallBackToGeographicCentroid" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="performAddressPointInterpolation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="offsetUnits" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeoCodingOptions_TO", propOrder = {
    "preferedDatabase",
    "offsetFromStreet",
    "offsetFromCorner",
    "coordinateSystem",
    "fallbackToPostalCentroid",
    "fallBackToGeographicCentroid",
    "performAddressPointInterpolation",
    "offsetUnits"
})
@XmlSeeAlso({
    CanadaGeoCodingOptionsTO.class
})
public class GeoCodingOptionsTO
    extends DependentObjectTO
{

    protected GeoCodeDatabase preferedDatabase;
    protected String offsetFromStreet;
    protected String offsetFromCorner;
    protected GeographicCoordinateSystemType coordinateSystem;
    protected Boolean fallbackToPostalCentroid;
    protected Boolean fallBackToGeographicCentroid;
    protected Boolean performAddressPointInterpolation;
    protected String offsetUnits;

    /**
     * Gets the value of the preferedDatabase property.
     * 
     * @return
     *     possible object is
     *     {@link GeoCodeDatabase }
     *     
     */
    public GeoCodeDatabase getPreferedDatabase() {
        return preferedDatabase;
    }

    /**
     * Sets the value of the preferedDatabase property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeoCodeDatabase }
     *     
     */
    public void setPreferedDatabase(GeoCodeDatabase value) {
        this.preferedDatabase = value;
    }

    /**
     * Gets the value of the offsetFromStreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffsetFromStreet() {
        return offsetFromStreet;
    }

    /**
     * Sets the value of the offsetFromStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffsetFromStreet(String value) {
        this.offsetFromStreet = value;
    }

    /**
     * Gets the value of the offsetFromCorner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffsetFromCorner() {
        return offsetFromCorner;
    }

    /**
     * Sets the value of the offsetFromCorner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffsetFromCorner(String value) {
        this.offsetFromCorner = value;
    }

    /**
     * Gets the value of the coordinateSystem property.
     * 
     * @return
     *     possible object is
     *     {@link GeographicCoordinateSystemType }
     *     
     */
    public GeographicCoordinateSystemType getCoordinateSystem() {
        return coordinateSystem;
    }

    /**
     * Sets the value of the coordinateSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeographicCoordinateSystemType }
     *     
     */
    public void setCoordinateSystem(GeographicCoordinateSystemType value) {
        this.coordinateSystem = value;
    }

    /**
     * Gets the value of the fallbackToPostalCentroid property.
     * This getter has been renamed from isFallbackToPostalCentroid() to getFallbackToPostalCentroid() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFallbackToPostalCentroid() {
        return fallbackToPostalCentroid;
    }

    /**
     * Sets the value of the fallbackToPostalCentroid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFallbackToPostalCentroid(Boolean value) {
        this.fallbackToPostalCentroid = value;
    }

    /**
     * Gets the value of the fallBackToGeographicCentroid property.
     * This getter has been renamed from isFallBackToGeographicCentroid() to getFallBackToGeographicCentroid() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFallBackToGeographicCentroid() {
        return fallBackToGeographicCentroid;
    }

    /**
     * Sets the value of the fallBackToGeographicCentroid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFallBackToGeographicCentroid(Boolean value) {
        this.fallBackToGeographicCentroid = value;
    }

    /**
     * Gets the value of the performAddressPointInterpolation property.
     * This getter has been renamed from isPerformAddressPointInterpolation() to getPerformAddressPointInterpolation() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPerformAddressPointInterpolation() {
        return performAddressPointInterpolation;
    }

    /**
     * Sets the value of the performAddressPointInterpolation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPerformAddressPointInterpolation(Boolean value) {
        this.performAddressPointInterpolation = value;
    }

    /**
     * Gets the value of the offsetUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffsetUnits() {
        return offsetUnits;
    }

    /**
     * Sets the value of the offsetUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffsetUnits(String value) {
        this.offsetUnits = value;
    }

}
