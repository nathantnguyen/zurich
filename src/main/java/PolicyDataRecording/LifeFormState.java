
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LifeFormState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LifeFormState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Alive"/>
 *     &lt;enumeration value="Dead"/>
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Missing"/>
 *     &lt;enumeration value="Viable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LifeFormState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum LifeFormState {

    @XmlEnumValue("Alive")
    ALIVE("Alive"),
    @XmlEnumValue("Dead")
    DEAD("Dead"),
    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Missing")
    MISSING("Missing"),
    @XmlEnumValue("Viable")
    VIABLE("Viable");
    private final String value;

    LifeFormState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LifeFormState fromValue(String v) {
        for (LifeFormState c: LifeFormState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
