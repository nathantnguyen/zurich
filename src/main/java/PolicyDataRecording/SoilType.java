
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SoilType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SoilType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Sand"/>
 *     &lt;enumeration value="Lime"/>
 *     &lt;enumeration value="Rock"/>
 *     &lt;enumeration value="Swamp"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SoilType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum SoilType {

    @XmlEnumValue("Sand")
    SAND("Sand"),
    @XmlEnumValue("Lime")
    LIME("Lime"),
    @XmlEnumValue("Rock")
    ROCK("Rock"),
    @XmlEnumValue("Swamp")
    SWAMP("Swamp");
    private final String value;

    SoilType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SoilType fromValue(String v) {
        for (SoilType c: SoilType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
