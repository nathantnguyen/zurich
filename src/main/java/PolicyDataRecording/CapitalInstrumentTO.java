
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CapitalInstrument_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CapitalInstrument_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="buyBackDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="firstCallDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="grandfatheredIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="issueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="maturityDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="noticePeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="allowableUnderTransition" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="capitalAmountAuthorized" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="capitalAmountIssued" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="detailsOfFurtherCalls" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="detailsOfIncentivesToRedeem" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="issuePaymentStatusIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="ownershipRestrictionIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="settlementSharesFactor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="settlementSharesVolume" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="votingRightsIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="votingRightsPerShare" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="capitalAmountOutstanding" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CapitalInstrument_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "buyBackDate",
    "firstCallDate",
    "grandfatheredIndicator",
    "issueDate",
    "maturityDate",
    "noticePeriod",
    "allowableUnderTransition",
    "capitalAmountAuthorized",
    "capitalAmountIssued",
    "detailsOfFurtherCalls",
    "detailsOfIncentivesToRedeem",
    "issuePaymentStatusIndicator",
    "ownershipRestrictionIndicator",
    "settlementSharesFactor",
    "settlementSharesVolume",
    "votingRightsIndicator",
    "votingRightsPerShare",
    "capitalAmountOutstanding"
})
public class CapitalInstrumentTO
    extends FinancialAssetTO
{

    protected XMLGregorianCalendar buyBackDate;
    protected XMLGregorianCalendar firstCallDate;
    protected Boolean grandfatheredIndicator;
    protected XMLGregorianCalendar issueDate;
    protected XMLGregorianCalendar maturityDate;
    protected Duration noticePeriod;
    protected Boolean allowableUnderTransition;
    protected BaseCurrencyAmount capitalAmountAuthorized;
    protected BaseCurrencyAmount capitalAmountIssued;
    protected String detailsOfFurtherCalls;
    protected String detailsOfIncentivesToRedeem;
    protected String issuePaymentStatusIndicator;
    protected String ownershipRestrictionIndicator;
    protected String settlementSharesFactor;
    protected BigInteger settlementSharesVolume;
    protected String votingRightsIndicator;
    protected BigInteger votingRightsPerShare;
    protected BaseCurrencyAmount capitalAmountOutstanding;

    /**
     * Gets the value of the buyBackDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBuyBackDate() {
        return buyBackDate;
    }

    /**
     * Sets the value of the buyBackDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBuyBackDate(XMLGregorianCalendar value) {
        this.buyBackDate = value;
    }

    /**
     * Gets the value of the firstCallDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstCallDate() {
        return firstCallDate;
    }

    /**
     * Sets the value of the firstCallDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstCallDate(XMLGregorianCalendar value) {
        this.firstCallDate = value;
    }

    /**
     * Gets the value of the grandfatheredIndicator property.
     * This getter has been renamed from isGrandfatheredIndicator() to getGrandfatheredIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGrandfatheredIndicator() {
        return grandfatheredIndicator;
    }

    /**
     * Sets the value of the grandfatheredIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGrandfatheredIndicator(Boolean value) {
        this.grandfatheredIndicator = value;
    }

    /**
     * Gets the value of the issueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIssueDate() {
        return issueDate;
    }

    /**
     * Sets the value of the issueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIssueDate(XMLGregorianCalendar value) {
        this.issueDate = value;
    }

    /**
     * Gets the value of the maturityDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMaturityDate() {
        return maturityDate;
    }

    /**
     * Sets the value of the maturityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMaturityDate(XMLGregorianCalendar value) {
        this.maturityDate = value;
    }

    /**
     * Gets the value of the noticePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getNoticePeriod() {
        return noticePeriod;
    }

    /**
     * Sets the value of the noticePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setNoticePeriod(Duration value) {
        this.noticePeriod = value;
    }

    /**
     * Gets the value of the allowableUnderTransition property.
     * This getter has been renamed from isAllowableUnderTransition() to getAllowableUnderTransition() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAllowableUnderTransition() {
        return allowableUnderTransition;
    }

    /**
     * Sets the value of the allowableUnderTransition property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowableUnderTransition(Boolean value) {
        this.allowableUnderTransition = value;
    }

    /**
     * Gets the value of the capitalAmountAuthorized property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCapitalAmountAuthorized() {
        return capitalAmountAuthorized;
    }

    /**
     * Sets the value of the capitalAmountAuthorized property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCapitalAmountAuthorized(BaseCurrencyAmount value) {
        this.capitalAmountAuthorized = value;
    }

    /**
     * Gets the value of the capitalAmountIssued property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCapitalAmountIssued() {
        return capitalAmountIssued;
    }

    /**
     * Sets the value of the capitalAmountIssued property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCapitalAmountIssued(BaseCurrencyAmount value) {
        this.capitalAmountIssued = value;
    }

    /**
     * Gets the value of the detailsOfFurtherCalls property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailsOfFurtherCalls() {
        return detailsOfFurtherCalls;
    }

    /**
     * Sets the value of the detailsOfFurtherCalls property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailsOfFurtherCalls(String value) {
        this.detailsOfFurtherCalls = value;
    }

    /**
     * Gets the value of the detailsOfIncentivesToRedeem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailsOfIncentivesToRedeem() {
        return detailsOfIncentivesToRedeem;
    }

    /**
     * Sets the value of the detailsOfIncentivesToRedeem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailsOfIncentivesToRedeem(String value) {
        this.detailsOfIncentivesToRedeem = value;
    }

    /**
     * Gets the value of the issuePaymentStatusIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuePaymentStatusIndicator() {
        return issuePaymentStatusIndicator;
    }

    /**
     * Sets the value of the issuePaymentStatusIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuePaymentStatusIndicator(String value) {
        this.issuePaymentStatusIndicator = value;
    }

    /**
     * Gets the value of the ownershipRestrictionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnershipRestrictionIndicator() {
        return ownershipRestrictionIndicator;
    }

    /**
     * Sets the value of the ownershipRestrictionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnershipRestrictionIndicator(String value) {
        this.ownershipRestrictionIndicator = value;
    }

    /**
     * Gets the value of the settlementSharesFactor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSettlementSharesFactor() {
        return settlementSharesFactor;
    }

    /**
     * Sets the value of the settlementSharesFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSettlementSharesFactor(String value) {
        this.settlementSharesFactor = value;
    }

    /**
     * Gets the value of the settlementSharesVolume property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSettlementSharesVolume() {
        return settlementSharesVolume;
    }

    /**
     * Sets the value of the settlementSharesVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSettlementSharesVolume(BigInteger value) {
        this.settlementSharesVolume = value;
    }

    /**
     * Gets the value of the votingRightsIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVotingRightsIndicator() {
        return votingRightsIndicator;
    }

    /**
     * Sets the value of the votingRightsIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVotingRightsIndicator(String value) {
        this.votingRightsIndicator = value;
    }

    /**
     * Gets the value of the votingRightsPerShare property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVotingRightsPerShare() {
        return votingRightsPerShare;
    }

    /**
     * Sets the value of the votingRightsPerShare property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVotingRightsPerShare(BigInteger value) {
        this.votingRightsPerShare = value;
    }

    /**
     * Gets the value of the capitalAmountOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCapitalAmountOutstanding() {
        return capitalAmountOutstanding;
    }

    /**
     * Sets the value of the capitalAmountOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCapitalAmountOutstanding(BaseCurrencyAmount value) {
        this.capitalAmountOutstanding = value;
    }

}
