
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DrivingRestrictions.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DrivingRestrictions">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Corrective Lenses"/>
 *     &lt;enumeration value="Miscellaneous Restriction"/>
 *     &lt;enumeration value="No Driving On Weekend"/>
 *     &lt;enumeration value="No Passengers"/>
 *     &lt;enumeration value="Temporary License"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DrivingRestrictions", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum DrivingRestrictions {

    @XmlEnumValue("Corrective Lenses")
    CORRECTIVE_LENSES("Corrective Lenses"),
    @XmlEnumValue("Miscellaneous Restriction")
    MISCELLANEOUS_RESTRICTION("Miscellaneous Restriction"),
    @XmlEnumValue("No Driving On Weekend")
    NO_DRIVING_ON_WEEKEND("No Driving On Weekend"),
    @XmlEnumValue("No Passengers")
    NO_PASSENGERS("No Passengers"),
    @XmlEnumValue("Temporary License")
    TEMPORARY_LICENSE("Temporary License");
    private final String value;

    DrivingRestrictions(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DrivingRestrictions fromValue(String v) {
        for (DrivingRestrictions c: DrivingRestrictions.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
