
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcquisitionMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AcquisitionMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Heritage"/>
 *     &lt;enumeration value="Merger"/>
 *     &lt;enumeration value="Asset Purchase"/>
 *     &lt;enumeration value="Stock Purchase"/>
 *     &lt;enumeration value="Startup"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AcquisitionMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum AcquisitionMethod {

    @XmlEnumValue("Heritage")
    HERITAGE("Heritage"),
    @XmlEnumValue("Merger")
    MERGER("Merger"),
    @XmlEnumValue("Asset Purchase")
    ASSET_PURCHASE("Asset Purchase"),
    @XmlEnumValue("Stock Purchase")
    STOCK_PURCHASE("Stock Purchase"),
    @XmlEnumValue("Startup")
    STARTUP("Startup");
    private final String value;

    AcquisitionMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AcquisitionMethod fromValue(String v) {
        for (AcquisitionMethod c: AcquisitionMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
