
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectUsageUsage.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ObjectUsageUsage">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Business"/>
 *     &lt;enumeration value="Habitation"/>
 *     &lt;enumeration value="Leisure"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ObjectUsageUsage", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum ObjectUsageUsage {

    @XmlEnumValue("Business")
    BUSINESS("Business"),
    @XmlEnumValue("Habitation")
    HABITATION("Habitation"),
    @XmlEnumValue("Leisure")
    LEISURE("Leisure");
    private final String value;

    ObjectUsageUsage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ObjectUsageUsage fromValue(String v) {
        for (ObjectUsageUsage c: ObjectUsageUsage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
