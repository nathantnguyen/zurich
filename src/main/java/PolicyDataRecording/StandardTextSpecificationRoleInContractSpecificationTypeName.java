
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardTextSpecificationRoleInContractSpecificationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StandardTextSpecificationRoleInContractSpecificationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Document Template Usage"/>
 *     &lt;enumeration value="Risk Report Template"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StandardTextSpecificationRoleInContractSpecificationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum StandardTextSpecificationRoleInContractSpecificationTypeName {

    @XmlEnumValue("Document Template Usage")
    DOCUMENT_TEMPLATE_USAGE("Document Template Usage"),
    @XmlEnumValue("Risk Report Template")
    RISK_REPORT_TEMPLATE("Risk Report Template");
    private final String value;

    StandardTextSpecificationRoleInContractSpecificationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StandardTextSpecificationRoleInContractSpecificationTypeName fromValue(String v) {
        for (StandardTextSpecificationRoleInContractSpecificationTypeName c: StandardTextSpecificationRoleInContractSpecificationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
