
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ActuarialStatistics_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActuarialStatistics_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Statistics_TO">
 *       &lt;sequence>
 *         &lt;element name="derivationMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}DerivationMethod" minOccurs="0"/>
 *         &lt;element name="samplePeriodBeginDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="samplePeriodEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="selectionKind" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}SelectionKind" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActuarialStatistics_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/", propOrder = {
    "derivationMethod",
    "samplePeriodBeginDate",
    "samplePeriodEndDate",
    "selectionKind"
})
public class ActuarialStatisticsTO
    extends StatisticsTO
{

    protected DerivationMethod derivationMethod;
    protected XMLGregorianCalendar samplePeriodBeginDate;
    protected XMLGregorianCalendar samplePeriodEndDate;
    protected SelectionKind selectionKind;

    /**
     * Gets the value of the derivationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link DerivationMethod }
     *     
     */
    public DerivationMethod getDerivationMethod() {
        return derivationMethod;
    }

    /**
     * Sets the value of the derivationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link DerivationMethod }
     *     
     */
    public void setDerivationMethod(DerivationMethod value) {
        this.derivationMethod = value;
    }

    /**
     * Gets the value of the samplePeriodBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSamplePeriodBeginDate() {
        return samplePeriodBeginDate;
    }

    /**
     * Sets the value of the samplePeriodBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSamplePeriodBeginDate(XMLGregorianCalendar value) {
        this.samplePeriodBeginDate = value;
    }

    /**
     * Gets the value of the samplePeriodEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSamplePeriodEndDate() {
        return samplePeriodEndDate;
    }

    /**
     * Sets the value of the samplePeriodEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSamplePeriodEndDate(XMLGregorianCalendar value) {
        this.samplePeriodEndDate = value;
    }

    /**
     * Gets the value of the selectionKind property.
     * 
     * @return
     *     possible object is
     *     {@link SelectionKind }
     *     
     */
    public SelectionKind getSelectionKind() {
        return selectionKind;
    }

    /**
     * Sets the value of the selectionKind property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectionKind }
     *     
     */
    public void setSelectionKind(SelectionKind value) {
        this.selectionKind = value;
    }

}
