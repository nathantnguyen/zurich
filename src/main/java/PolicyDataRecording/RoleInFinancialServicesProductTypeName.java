
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInFinancialServicesProductTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInFinancialServicesProductTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Product Description"/>
 *     &lt;enumeration value="Used Statistics"/>
 *     &lt;enumeration value="Available Assets"/>
 *     &lt;enumeration value="Product Claim Cost Code"/>
 *     &lt;enumeration value="Product Place Limitation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInFinancialServicesProductTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum RoleInFinancialServicesProductTypeName {

    @XmlEnumValue("Product Description")
    PRODUCT_DESCRIPTION("Product Description"),
    @XmlEnumValue("Used Statistics")
    USED_STATISTICS("Used Statistics"),
    @XmlEnumValue("Available Assets")
    AVAILABLE_ASSETS("Available Assets"),
    @XmlEnumValue("Product Claim Cost Code")
    PRODUCT_CLAIM_COST_CODE("Product Claim Cost Code"),
    @XmlEnumValue("Product Place Limitation")
    PRODUCT_PLACE_LIMITATION("Product Place Limitation");
    private final String value;

    RoleInFinancialServicesProductTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInFinancialServicesProductTypeName fromValue(String v) {
        for (RoleInFinancialServicesProductTypeName c: RoleInFinancialServicesProductTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
