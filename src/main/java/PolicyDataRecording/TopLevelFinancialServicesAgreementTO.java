
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TopLevelFinancialServicesAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TopLevelFinancialServicesAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="trackingAccount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Account_TO" minOccurs="0"/>
 *         &lt;element name="collateral" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Account_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopLevelFinancialServicesAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "trackingAccount",
    "collateral"
})
@XmlSeeAlso({
    CommutationAgreementTO.class,
    DerivativeContractTO.class,
    AccountAgreementTO.class,
    InsurancePolicyTO.class
})
public class TopLevelFinancialServicesAgreementTO
    extends FinancialServicesAgreementTO
{

    protected AccountTO trackingAccount;
    protected List<AccountTO> collateral;

    /**
     * Gets the value of the trackingAccount property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTO }
     *     
     */
    public AccountTO getTrackingAccount() {
        return trackingAccount;
    }

    /**
     * Sets the value of the trackingAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTO }
     *     
     */
    public void setTrackingAccount(AccountTO value) {
        this.trackingAccount = value;
    }

    /**
     * Gets the value of the collateral property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the collateral property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCollateral().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountTO }
     * 
     * 
     */
    public List<AccountTO> getCollateral() {
        if (collateral == null) {
            collateral = new ArrayList<AccountTO>();
        }
        return this.collateral;
    }

    /**
     * Sets the value of the collateral property.
     * 
     * @param collateral
     *     allowed object is
     *     {@link AccountTO }
     *     
     */
    public void setCollateral(List<AccountTO> collateral) {
        this.collateral = collateral;
    }

}
