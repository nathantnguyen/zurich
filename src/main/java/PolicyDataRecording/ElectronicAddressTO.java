
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ElectronicAddress_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ElectronicAddress_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPoint_TO">
 *       &lt;sequence>
 *         &lt;element name="node" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="domain" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="userid" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="electronicType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}EmailElectronicType" minOccurs="0"/>
 *         &lt;element name="attachmentsAllowed" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="maximumMessageSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="textOnly" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElectronicAddress_TO", propOrder = {
    "node",
    "domain",
    "userid",
    "electronicType",
    "attachmentsAllowed",
    "maximumMessageSize",
    "textOnly"
})
public class ElectronicAddressTO
    extends ContactPointTO
{

    protected String node;
    protected String domain;
    protected String userid;
    protected EmailElectronicType electronicType;
    protected Boolean attachmentsAllowed;
    protected BigInteger maximumMessageSize;
    protected Boolean textOnly;

    /**
     * Gets the value of the node property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNode() {
        return node;
    }

    /**
     * Sets the value of the node property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNode(String value) {
        this.node = value;
    }

    /**
     * Gets the value of the domain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomain() {
        return domain;
    }

    /**
     * Sets the value of the domain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomain(String value) {
        this.domain = value;
    }

    /**
     * Gets the value of the userid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserid() {
        return userid;
    }

    /**
     * Sets the value of the userid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserid(String value) {
        this.userid = value;
    }

    /**
     * Gets the value of the electronicType property.
     * 
     * @return
     *     possible object is
     *     {@link EmailElectronicType }
     *     
     */
    public EmailElectronicType getElectronicType() {
        return electronicType;
    }

    /**
     * Sets the value of the electronicType property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailElectronicType }
     *     
     */
    public void setElectronicType(EmailElectronicType value) {
        this.electronicType = value;
    }

    /**
     * Gets the value of the attachmentsAllowed property.
     * This getter has been renamed from isAttachmentsAllowed() to getAttachmentsAllowed() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAttachmentsAllowed() {
        return attachmentsAllowed;
    }

    /**
     * Sets the value of the attachmentsAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAttachmentsAllowed(Boolean value) {
        this.attachmentsAllowed = value;
    }

    /**
     * Gets the value of the maximumMessageSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumMessageSize() {
        return maximumMessageSize;
    }

    /**
     * Sets the value of the maximumMessageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumMessageSize(BigInteger value) {
        this.maximumMessageSize = value;
    }

    /**
     * Gets the value of the textOnly property.
     * This getter has been renamed from isTextOnly() to getTextOnly() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getTextOnly() {
        return textOnly;
    }

    /**
     * Sets the value of the textOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTextOnly(Boolean value) {
        this.textOnly = value;
    }

}
