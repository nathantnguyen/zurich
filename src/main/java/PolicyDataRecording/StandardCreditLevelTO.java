
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardCreditLevel_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandardCreditLevel_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Statistics_TO">
 *       &lt;sequence>
 *         &lt;element name="parameters" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}StandardCreditLevelParameter_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="standardisedSolvencyLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="lossGivenDefaultFactor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardCreditLevel_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/", propOrder = {
    "parameters",
    "standardisedSolvencyLevel",
    "lossGivenDefaultFactor"
})
public class StandardCreditLevelTO
    extends StatisticsTO
{

    protected List<StandardCreditLevelParameterTO> parameters;
    protected BigInteger standardisedSolvencyLevel;
    protected BigDecimal lossGivenDefaultFactor;

    /**
     * Gets the value of the parameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StandardCreditLevelParameterTO }
     * 
     * 
     */
    public List<StandardCreditLevelParameterTO> getParameters() {
        if (parameters == null) {
            parameters = new ArrayList<StandardCreditLevelParameterTO>();
        }
        return this.parameters;
    }

    /**
     * Gets the value of the standardisedSolvencyLevel property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStandardisedSolvencyLevel() {
        return standardisedSolvencyLevel;
    }

    /**
     * Sets the value of the standardisedSolvencyLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStandardisedSolvencyLevel(BigInteger value) {
        this.standardisedSolvencyLevel = value;
    }

    /**
     * Gets the value of the lossGivenDefaultFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLossGivenDefaultFactor() {
        return lossGivenDefaultFactor;
    }

    /**
     * Sets the value of the lossGivenDefaultFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLossGivenDefaultFactor(BigDecimal value) {
        this.lossGivenDefaultFactor = value;
    }

    /**
     * Sets the value of the parameters property.
     * 
     * @param parameters
     *     allowed object is
     *     {@link StandardCreditLevelParameterTO }
     *     
     */
    public void setParameters(List<StandardCreditLevelParameterTO> parameters) {
        this.parameters = parameters;
    }

}
