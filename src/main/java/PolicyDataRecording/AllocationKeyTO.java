
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AllocationKey_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AllocationKey_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}MoneyProvisionDeterminer_TO">
 *       &lt;sequence>
 *         &lt;element name="allocationAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="allocationCurrency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="allocationPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="allocationUnits" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="remainder" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="FinancialAsset" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllocationKey_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "allocationAmount",
    "allocationCurrency",
    "allocationPercentage",
    "allocationUnits",
    "remainder",
    "financialAsset"
})
public class AllocationKeyTO
    extends MoneyProvisionDeterminerTO
{

    protected BaseCurrencyAmount allocationAmount;
    protected CurrencyCode allocationCurrency;
    protected BigDecimal allocationPercentage;
    protected BigDecimal allocationUnits;
    protected Boolean remainder;
    @XmlElement(name = "FinancialAsset")
    protected FinancialAssetTO financialAsset;

    /**
     * Gets the value of the allocationAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAllocationAmount() {
        return allocationAmount;
    }

    /**
     * Sets the value of the allocationAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAllocationAmount(BaseCurrencyAmount value) {
        this.allocationAmount = value;
    }

    /**
     * Gets the value of the allocationCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCode }
     *     
     */
    public CurrencyCode getAllocationCurrency() {
        return allocationCurrency;
    }

    /**
     * Sets the value of the allocationCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCode }
     *     
     */
    public void setAllocationCurrency(CurrencyCode value) {
        this.allocationCurrency = value;
    }

    /**
     * Gets the value of the allocationPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAllocationPercentage() {
        return allocationPercentage;
    }

    /**
     * Sets the value of the allocationPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAllocationPercentage(BigDecimal value) {
        this.allocationPercentage = value;
    }

    /**
     * Gets the value of the allocationUnits property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAllocationUnits() {
        return allocationUnits;
    }

    /**
     * Sets the value of the allocationUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAllocationUnits(BigDecimal value) {
        this.allocationUnits = value;
    }

    /**
     * Gets the value of the remainder property.
     * This getter has been renamed from isRemainder() to getRemainder() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRemainder() {
        return remainder;
    }

    /**
     * Sets the value of the remainder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRemainder(Boolean value) {
        this.remainder = value;
    }

    /**
     * Gets the value of the financialAsset property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAssetTO }
     *     
     */
    public FinancialAssetTO getFinancialAsset() {
        return financialAsset;
    }

    /**
     * Sets the value of the financialAsset property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setFinancialAsset(FinancialAssetTO value) {
        this.financialAsset = value;
    }

}
