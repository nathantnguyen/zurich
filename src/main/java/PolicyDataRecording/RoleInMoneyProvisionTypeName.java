
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInMoneyProvisionTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInMoneyProvisionTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Requested Money For Activity Occurrence"/>
 *     &lt;enumeration value="Money Provision Payee"/>
 *     &lt;enumeration value="Money Provision Payer"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInMoneyProvisionTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum RoleInMoneyProvisionTypeName {

    @XmlEnumValue("Requested Money For Activity Occurrence")
    REQUESTED_MONEY_FOR_ACTIVITY_OCCURRENCE("Requested Money For Activity Occurrence"),
    @XmlEnumValue("Money Provision Payee")
    MONEY_PROVISION_PAYEE("Money Provision Payee"),
    @XmlEnumValue("Money Provision Payer")
    MONEY_PROVISION_PAYER("Money Provision Payer");
    private final String value;

    RoleInMoneyProvisionTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInMoneyProvisionTypeName fromValue(String v) {
        for (RoleInMoneyProvisionTypeName c: RoleInMoneyProvisionTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
