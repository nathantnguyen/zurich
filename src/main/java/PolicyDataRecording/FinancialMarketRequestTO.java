
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FinancialMarketRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialMarketRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialMovementRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="unitValorisationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="bestExecutionFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="contractualGrossAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="internalDealFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="legIdentifier" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="normalMarketSizeFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="originalPurchasePrice" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="setoffOfCollateralFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="shortSaleFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="tradeIdentifier" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="tradedPrice" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="financialAsset" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="financialMarketTransactionHoldingPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialMarketRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "unitValorisationDate",
    "bestExecutionFlag",
    "contractualGrossAmount",
    "internalDealFlag",
    "legIdentifier",
    "normalMarketSizeFlag",
    "originalPurchasePrice",
    "setoffOfCollateralFlag",
    "shortSaleFlag",
    "tradeIdentifier",
    "tradedPrice",
    "financialAsset",
    "financialMarketTransactionHoldingPeriod"
})
@XmlSeeAlso({
    SellRequestTO.class,
    FundSwitchRequestTO.class,
    PurchaseRequestTO.class
})
public class FinancialMarketRequestTO
    extends FinancialMovementRequestTO
{

    protected XMLGregorianCalendar unitValorisationDate;
    protected Boolean bestExecutionFlag;
    protected BaseCurrencyAmount contractualGrossAmount;
    protected Boolean internalDealFlag;
    protected BigInteger legIdentifier;
    protected Boolean normalMarketSizeFlag;
    protected BaseCurrencyAmount originalPurchasePrice;
    protected Boolean setoffOfCollateralFlag;
    protected Boolean shortSaleFlag;
    protected String tradeIdentifier;
    protected BaseCurrencyAmount tradedPrice;
    protected List<FinancialAssetTO> financialAsset;
    protected Duration financialMarketTransactionHoldingPeriod;

    /**
     * Gets the value of the unitValorisationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUnitValorisationDate() {
        return unitValorisationDate;
    }

    /**
     * Sets the value of the unitValorisationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUnitValorisationDate(XMLGregorianCalendar value) {
        this.unitValorisationDate = value;
    }

    /**
     * Gets the value of the bestExecutionFlag property.
     * This getter has been renamed from isBestExecutionFlag() to getBestExecutionFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBestExecutionFlag() {
        return bestExecutionFlag;
    }

    /**
     * Sets the value of the bestExecutionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBestExecutionFlag(Boolean value) {
        this.bestExecutionFlag = value;
    }

    /**
     * Gets the value of the contractualGrossAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getContractualGrossAmount() {
        return contractualGrossAmount;
    }

    /**
     * Sets the value of the contractualGrossAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setContractualGrossAmount(BaseCurrencyAmount value) {
        this.contractualGrossAmount = value;
    }

    /**
     * Gets the value of the internalDealFlag property.
     * This getter has been renamed from isInternalDealFlag() to getInternalDealFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInternalDealFlag() {
        return internalDealFlag;
    }

    /**
     * Sets the value of the internalDealFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInternalDealFlag(Boolean value) {
        this.internalDealFlag = value;
    }

    /**
     * Gets the value of the legIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLegIdentifier() {
        return legIdentifier;
    }

    /**
     * Sets the value of the legIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLegIdentifier(BigInteger value) {
        this.legIdentifier = value;
    }

    /**
     * Gets the value of the normalMarketSizeFlag property.
     * This getter has been renamed from isNormalMarketSizeFlag() to getNormalMarketSizeFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNormalMarketSizeFlag() {
        return normalMarketSizeFlag;
    }

    /**
     * Sets the value of the normalMarketSizeFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNormalMarketSizeFlag(Boolean value) {
        this.normalMarketSizeFlag = value;
    }

    /**
     * Gets the value of the originalPurchasePrice property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getOriginalPurchasePrice() {
        return originalPurchasePrice;
    }

    /**
     * Sets the value of the originalPurchasePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setOriginalPurchasePrice(BaseCurrencyAmount value) {
        this.originalPurchasePrice = value;
    }

    /**
     * Gets the value of the setoffOfCollateralFlag property.
     * This getter has been renamed from isSetoffOfCollateralFlag() to getSetoffOfCollateralFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSetoffOfCollateralFlag() {
        return setoffOfCollateralFlag;
    }

    /**
     * Sets the value of the setoffOfCollateralFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSetoffOfCollateralFlag(Boolean value) {
        this.setoffOfCollateralFlag = value;
    }

    /**
     * Gets the value of the shortSaleFlag property.
     * This getter has been renamed from isShortSaleFlag() to getShortSaleFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getShortSaleFlag() {
        return shortSaleFlag;
    }

    /**
     * Sets the value of the shortSaleFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShortSaleFlag(Boolean value) {
        this.shortSaleFlag = value;
    }

    /**
     * Gets the value of the tradeIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeIdentifier() {
        return tradeIdentifier;
    }

    /**
     * Sets the value of the tradeIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeIdentifier(String value) {
        this.tradeIdentifier = value;
    }

    /**
     * Gets the value of the tradedPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTradedPrice() {
        return tradedPrice;
    }

    /**
     * Sets the value of the tradedPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTradedPrice(BaseCurrencyAmount value) {
        this.tradedPrice = value;
    }

    /**
     * Gets the value of the financialAsset property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the financialAsset property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFinancialAsset().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialAssetTO }
     * 
     * 
     */
    public List<FinancialAssetTO> getFinancialAsset() {
        if (financialAsset == null) {
            financialAsset = new ArrayList<FinancialAssetTO>();
        }
        return this.financialAsset;
    }

    /**
     * Gets the value of the financialMarketTransactionHoldingPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getFinancialMarketTransactionHoldingPeriod() {
        return financialMarketTransactionHoldingPeriod;
    }

    /**
     * Sets the value of the financialMarketTransactionHoldingPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setFinancialMarketTransactionHoldingPeriod(Duration value) {
        this.financialMarketTransactionHoldingPeriod = value;
    }

    /**
     * Sets the value of the financialAsset property.
     * 
     * @param financialAsset
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setFinancialAsset(List<FinancialAssetTO> financialAsset) {
        this.financialAsset = financialAsset;
    }

}
