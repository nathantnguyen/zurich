
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleSpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleSpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RelationshipSpec_TO">
 *       &lt;sequence>
 *         &lt;element name="ruleSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RuleSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="attributeSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AttributeSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="calculations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Calculation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="minimumNumberOfRoles" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="maximumNumberOfRoles" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="conformanceTypeNames" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleSpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "ruleSpecs",
    "attributeSpecs",
    "calculations",
    "minimumNumberOfRoles",
    "maximumNumberOfRoles",
    "conformanceTypeNames"
})
public class RoleSpecTO
    extends RelationshipSpecTO
{

    protected List<RuleSpecTO> ruleSpecs;
    protected List<AttributeSpecTO> attributeSpecs;
    protected List<CalculationTO> calculations;
    protected BigInteger minimumNumberOfRoles;
    protected BigInteger maximumNumberOfRoles;
    protected List<String> conformanceTypeNames;

    /**
     * Gets the value of the ruleSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ruleSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRuleSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RuleSpecTO }
     * 
     * 
     */
    public List<RuleSpecTO> getRuleSpecs() {
        if (ruleSpecs == null) {
            ruleSpecs = new ArrayList<RuleSpecTO>();
        }
        return this.ruleSpecs;
    }

    /**
     * Gets the value of the attributeSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeSpecTO }
     * 
     * 
     */
    public List<AttributeSpecTO> getAttributeSpecs() {
        if (attributeSpecs == null) {
            attributeSpecs = new ArrayList<AttributeSpecTO>();
        }
        return this.attributeSpecs;
    }

    /**
     * Gets the value of the calculations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the calculations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCalculations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalculationTO }
     * 
     * 
     */
    public List<CalculationTO> getCalculations() {
        if (calculations == null) {
            calculations = new ArrayList<CalculationTO>();
        }
        return this.calculations;
    }

    /**
     * Gets the value of the minimumNumberOfRoles property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumNumberOfRoles() {
        return minimumNumberOfRoles;
    }

    /**
     * Sets the value of the minimumNumberOfRoles property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumNumberOfRoles(BigInteger value) {
        this.minimumNumberOfRoles = value;
    }

    /**
     * Gets the value of the maximumNumberOfRoles property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximumNumberOfRoles() {
        return maximumNumberOfRoles;
    }

    /**
     * Sets the value of the maximumNumberOfRoles property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximumNumberOfRoles(BigInteger value) {
        this.maximumNumberOfRoles = value;
    }

    /**
     * Gets the value of the conformanceTypeNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conformanceTypeNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConformanceTypeNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getConformanceTypeNames() {
        if (conformanceTypeNames == null) {
            conformanceTypeNames = new ArrayList<String>();
        }
        return this.conformanceTypeNames;
    }

    /**
     * Sets the value of the ruleSpecs property.
     * 
     * @param ruleSpecs
     *     allowed object is
     *     {@link RuleSpecTO }
     *     
     */
    public void setRuleSpecs(List<RuleSpecTO> ruleSpecs) {
        this.ruleSpecs = ruleSpecs;
    }

    /**
     * Sets the value of the attributeSpecs property.
     * 
     * @param attributeSpecs
     *     allowed object is
     *     {@link AttributeSpecTO }
     *     
     */
    public void setAttributeSpecs(List<AttributeSpecTO> attributeSpecs) {
        this.attributeSpecs = attributeSpecs;
    }

    /**
     * Sets the value of the calculations property.
     * 
     * @param calculations
     *     allowed object is
     *     {@link CalculationTO }
     *     
     */
    public void setCalculations(List<CalculationTO> calculations) {
        this.calculations = calculations;
    }

    /**
     * Sets the value of the conformanceTypeNames property.
     * 
     * @param conformanceTypeNames
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConformanceTypeNames(List<String> conformanceTypeNames) {
        this.conformanceTypeNames = conformanceTypeNames;
    }

}
