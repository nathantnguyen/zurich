
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeathBenefitDetermination.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DeathBenefitDetermination">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Cumulative Premiums Paid"/>
 *     &lt;enumeration value="Fixed Coverage Amount"/>
 *     &lt;enumeration value="Given Premium"/>
 *     &lt;enumeration value="Loan Balance"/>
 *     &lt;enumeration value="Other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DeathBenefitDetermination", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum DeathBenefitDetermination {

    @XmlEnumValue("Cumulative Premiums Paid")
    CUMULATIVE_PREMIUMS_PAID("Cumulative Premiums Paid"),
    @XmlEnumValue("Fixed Coverage Amount")
    FIXED_COVERAGE_AMOUNT("Fixed Coverage Amount"),
    @XmlEnumValue("Given Premium")
    GIVEN_PREMIUM("Given Premium"),
    @XmlEnumValue("Loan Balance")
    LOAN_BALANCE("Loan Balance"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    DeathBenefitDetermination(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeathBenefitDetermination fromValue(String v) {
        for (DeathBenefitDetermination c: DeathBenefitDetermination.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
