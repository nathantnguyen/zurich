
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyOutScheduler_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyOutScheduler_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyScheduler_TO">
 *       &lt;sequence>
 *         &lt;element name="generatedPayments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Payment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="incomingBill" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}PaymentDue_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="offsetToMoneyInSchedulers" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyInScheduler_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyOutScheduler_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "generatedPayments",
    "incomingBill",
    "offsetToMoneyInSchedulers"
})
public class MoneyOutSchedulerTO
    extends MoneySchedulerTO
{

    protected List<PaymentTO> generatedPayments;
    protected List<PaymentDueTO> incomingBill;
    protected List<MoneyInSchedulerTO> offsetToMoneyInSchedulers;

    /**
     * Gets the value of the generatedPayments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the generatedPayments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeneratedPayments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentTO }
     * 
     * 
     */
    public List<PaymentTO> getGeneratedPayments() {
        if (generatedPayments == null) {
            generatedPayments = new ArrayList<PaymentTO>();
        }
        return this.generatedPayments;
    }

    /**
     * Gets the value of the incomingBill property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the incomingBill property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncomingBill().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentDueTO }
     * 
     * 
     */
    public List<PaymentDueTO> getIncomingBill() {
        if (incomingBill == null) {
            incomingBill = new ArrayList<PaymentDueTO>();
        }
        return this.incomingBill;
    }

    /**
     * Gets the value of the offsetToMoneyInSchedulers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offsetToMoneyInSchedulers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOffsetToMoneyInSchedulers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyInSchedulerTO }
     * 
     * 
     */
    public List<MoneyInSchedulerTO> getOffsetToMoneyInSchedulers() {
        if (offsetToMoneyInSchedulers == null) {
            offsetToMoneyInSchedulers = new ArrayList<MoneyInSchedulerTO>();
        }
        return this.offsetToMoneyInSchedulers;
    }

    /**
     * Sets the value of the generatedPayments property.
     * 
     * @param generatedPayments
     *     allowed object is
     *     {@link PaymentTO }
     *     
     */
    public void setGeneratedPayments(List<PaymentTO> generatedPayments) {
        this.generatedPayments = generatedPayments;
    }

    /**
     * Sets the value of the incomingBill property.
     * 
     * @param incomingBill
     *     allowed object is
     *     {@link PaymentDueTO }
     *     
     */
    public void setIncomingBill(List<PaymentDueTO> incomingBill) {
        this.incomingBill = incomingBill;
    }

    /**
     * Sets the value of the offsetToMoneyInSchedulers property.
     * 
     * @param offsetToMoneyInSchedulers
     *     allowed object is
     *     {@link MoneyInSchedulerTO }
     *     
     */
    public void setOffsetToMoneyInSchedulers(List<MoneyInSchedulerTO> offsetToMoneyInSchedulers) {
        this.offsetToMoneyInSchedulers = offsetToMoneyInSchedulers;
    }

}
