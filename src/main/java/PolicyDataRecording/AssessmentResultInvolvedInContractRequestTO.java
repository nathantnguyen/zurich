
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssessmentResultInvolvedInContractRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssessmentResultInvolvedInContractRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContractRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="assessmentResultInvolvedInContractRequestRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}AssessmentResultInvolvedInContractRequestType_TO" minOccurs="0"/>
 *         &lt;element name="assessmentResult" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssessmentResultInvolvedInContractRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "assessmentResultInvolvedInContractRequestRootType",
    "assessmentResult"
})
public class AssessmentResultInvolvedInContractRequestTO
    extends RoleInContractRequestTO
{

    protected AssessmentResultInvolvedInContractRequestTypeTO assessmentResultInvolvedInContractRequestRootType;
    protected AssessmentResultTO assessmentResult;

    /**
     * Gets the value of the assessmentResultInvolvedInContractRequestRootType property.
     * 
     * @return
     *     possible object is
     *     {@link AssessmentResultInvolvedInContractRequestTypeTO }
     *     
     */
    public AssessmentResultInvolvedInContractRequestTypeTO getAssessmentResultInvolvedInContractRequestRootType() {
        return assessmentResultInvolvedInContractRequestRootType;
    }

    /**
     * Sets the value of the assessmentResultInvolvedInContractRequestRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssessmentResultInvolvedInContractRequestTypeTO }
     *     
     */
    public void setAssessmentResultInvolvedInContractRequestRootType(AssessmentResultInvolvedInContractRequestTypeTO value) {
        this.assessmentResultInvolvedInContractRequestRootType = value;
    }

    /**
     * Gets the value of the assessmentResult property.
     * 
     * @return
     *     possible object is
     *     {@link AssessmentResultTO }
     *     
     */
    public AssessmentResultTO getAssessmentResult() {
        return assessmentResult;
    }

    /**
     * Sets the value of the assessmentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssessmentResultTO }
     *     
     */
    public void setAssessmentResult(AssessmentResultTO value) {
        this.assessmentResult = value;
    }

}
