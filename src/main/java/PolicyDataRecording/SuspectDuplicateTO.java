
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SuspectDuplicate_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SuspectDuplicate_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="adjustedMatchCategory" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}MatchCategory" minOccurs="0"/>
 *         &lt;element name="adjustmentReason" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="bestMatch" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="matchCategory" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}MatchCategory" minOccurs="0"/>
 *         &lt;element name="sourceType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}SuspectDuplicateSources" minOccurs="0"/>
 *         &lt;element name="partyDuplicates" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="matchEngineType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="createdByExternalRef" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="suspectParties" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Party_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="matchRelevancy" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="matchRelevancyScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="nonMatchRelevancy" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="nonMatchRelevancyScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SuspectDuplicate_TO", propOrder = {
    "adjustedMatchCategory",
    "adjustmentReason",
    "bestMatch",
    "matchCategory",
    "sourceType",
    "partyDuplicates",
    "matchEngineType",
    "createdByExternalRef",
    "suspectParties",
    "matchRelevancy",
    "matchRelevancyScore",
    "nonMatchRelevancy",
    "nonMatchRelevancyScore"
})
public class SuspectDuplicateTO
    extends ScoreTO
{

    protected MatchCategory adjustedMatchCategory;
    protected String adjustmentReason;
    protected Boolean bestMatch;
    protected MatchCategory matchCategory;
    protected SuspectDuplicateSources sourceType;
    protected List<RolePlayerTO> partyDuplicates;
    protected String matchEngineType;
    protected String createdByExternalRef;
    protected List<PartyTO> suspectParties;
    protected String matchRelevancy;
    protected BigDecimal matchRelevancyScore;
    protected String nonMatchRelevancy;
    protected BigDecimal nonMatchRelevancyScore;

    /**
     * Gets the value of the adjustedMatchCategory property.
     * 
     * @return
     *     possible object is
     *     {@link MatchCategory }
     *     
     */
    public MatchCategory getAdjustedMatchCategory() {
        return adjustedMatchCategory;
    }

    /**
     * Sets the value of the adjustedMatchCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchCategory }
     *     
     */
    public void setAdjustedMatchCategory(MatchCategory value) {
        this.adjustedMatchCategory = value;
    }

    /**
     * Gets the value of the adjustmentReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustmentReason() {
        return adjustmentReason;
    }

    /**
     * Sets the value of the adjustmentReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustmentReason(String value) {
        this.adjustmentReason = value;
    }

    /**
     * Gets the value of the bestMatch property.
     * This getter has been renamed from isBestMatch() to getBestMatch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBestMatch() {
        return bestMatch;
    }

    /**
     * Sets the value of the bestMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBestMatch(Boolean value) {
        this.bestMatch = value;
    }

    /**
     * Gets the value of the matchCategory property.
     * 
     * @return
     *     possible object is
     *     {@link MatchCategory }
     *     
     */
    public MatchCategory getMatchCategory() {
        return matchCategory;
    }

    /**
     * Sets the value of the matchCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchCategory }
     *     
     */
    public void setMatchCategory(MatchCategory value) {
        this.matchCategory = value;
    }

    /**
     * Gets the value of the sourceType property.
     * 
     * @return
     *     possible object is
     *     {@link SuspectDuplicateSources }
     *     
     */
    public SuspectDuplicateSources getSourceType() {
        return sourceType;
    }

    /**
     * Sets the value of the sourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SuspectDuplicateSources }
     *     
     */
    public void setSourceType(SuspectDuplicateSources value) {
        this.sourceType = value;
    }

    /**
     * Gets the value of the partyDuplicates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyDuplicates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyDuplicates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RolePlayerTO }
     * 
     * 
     */
    public List<RolePlayerTO> getPartyDuplicates() {
        if (partyDuplicates == null) {
            partyDuplicates = new ArrayList<RolePlayerTO>();
        }
        return this.partyDuplicates;
    }

    /**
     * Gets the value of the matchEngineType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchEngineType() {
        return matchEngineType;
    }

    /**
     * Sets the value of the matchEngineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchEngineType(String value) {
        this.matchEngineType = value;
    }

    /**
     * Gets the value of the createdByExternalRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedByExternalRef() {
        return createdByExternalRef;
    }

    /**
     * Sets the value of the createdByExternalRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedByExternalRef(String value) {
        this.createdByExternalRef = value;
    }

    /**
     * Gets the value of the suspectParties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the suspectParties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSuspectParties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyTO }
     * 
     * 
     */
    public List<PartyTO> getSuspectParties() {
        if (suspectParties == null) {
            suspectParties = new ArrayList<PartyTO>();
        }
        return this.suspectParties;
    }

    /**
     * Gets the value of the matchRelevancy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchRelevancy() {
        return matchRelevancy;
    }

    /**
     * Sets the value of the matchRelevancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchRelevancy(String value) {
        this.matchRelevancy = value;
    }

    /**
     * Gets the value of the matchRelevancyScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMatchRelevancyScore() {
        return matchRelevancyScore;
    }

    /**
     * Sets the value of the matchRelevancyScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMatchRelevancyScore(BigDecimal value) {
        this.matchRelevancyScore = value;
    }

    /**
     * Gets the value of the nonMatchRelevancy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonMatchRelevancy() {
        return nonMatchRelevancy;
    }

    /**
     * Sets the value of the nonMatchRelevancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonMatchRelevancy(String value) {
        this.nonMatchRelevancy = value;
    }

    /**
     * Gets the value of the nonMatchRelevancyScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNonMatchRelevancyScore() {
        return nonMatchRelevancyScore;
    }

    /**
     * Sets the value of the nonMatchRelevancyScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNonMatchRelevancyScore(BigDecimal value) {
        this.nonMatchRelevancyScore = value;
    }

    /**
     * Sets the value of the partyDuplicates property.
     * 
     * @param partyDuplicates
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setPartyDuplicates(List<RolePlayerTO> partyDuplicates) {
        this.partyDuplicates = partyDuplicates;
    }

    /**
     * Sets the value of the suspectParties property.
     * 
     * @param suspectParties
     *     allowed object is
     *     {@link PartyTO }
     *     
     */
    public void setSuspectParties(List<PartyTO> suspectParties) {
        this.suspectParties = suspectParties;
    }

}
