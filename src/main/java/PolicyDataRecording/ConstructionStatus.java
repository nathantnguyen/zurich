
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConstructionStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ConstructionStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Not Yet Started"/>
 *     &lt;enumeration value="Completed"/>
 *     &lt;enumeration value="Under Construction"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ConstructionStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum ConstructionStatus {

    @XmlEnumValue("Not Yet Started")
    NOT_YET_STARTED("Not Yet Started"),
    @XmlEnumValue("Completed")
    COMPLETED("Completed"),
    @XmlEnumValue("Under Construction")
    UNDER_CONSTRUCTION("Under Construction");
    private final String value;

    ConstructionStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ConstructionStatus fromValue(String v) {
        for (ConstructionStatus c: ConstructionStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
