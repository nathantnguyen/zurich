
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InsuranceMarketRiskFactorScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuranceMarketRiskFactorScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskFactorScore_TO">
 *       &lt;sequence>
 *         &lt;element name="marketSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceMarketRiskFactorScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "marketSize"
})
public class InsuranceMarketRiskFactorScoreTO
    extends RiskFactorScoreTO
{

    protected Amount marketSize;

    /**
     * Gets the value of the marketSize property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getMarketSize() {
        return marketSize;
    }

    /**
     * Sets the value of the marketSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setMarketSize(Amount value) {
        this.marketSize = value;
    }

}
