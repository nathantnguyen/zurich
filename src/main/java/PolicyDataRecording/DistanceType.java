
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DistanceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DistanceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Average Distance"/>
 *     &lt;enumeration value="Maximum Distance"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DistanceType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum DistanceType {

    @XmlEnumValue("Average Distance")
    AVERAGE_DISTANCE("Average Distance"),
    @XmlEnumValue("Maximum Distance")
    MAXIMUM_DISTANCE("Maximum Distance");
    private final String value;

    DistanceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DistanceType fromValue(String v) {
        for (DistanceType c: DistanceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
