
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssessmentActivity_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssessmentActivity_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO">
 *       &lt;sequence>
 *         &lt;element name="method" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}AssessmentMethod" minOccurs="0"/>
 *         &lt;element name="cumulatedFinancialValuation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="averageFinancialValuation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="assessmentResults" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="aggregationLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="aggregationReason" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="aggregationReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="numberOfSamples" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssessmentActivity_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "method",
    "cumulatedFinancialValuation",
    "averageFinancialValuation",
    "assessmentResults",
    "aggregationLevel",
    "aggregationReason",
    "aggregationReference",
    "numberOfSamples"
})
@XmlSeeAlso({
    ActuarialAnalysisTO.class,
    RiskAssessmentTO.class,
    AuditTO.class
})
public class AssessmentActivityTO
    extends ActivityOccurrenceTO
{

    protected AssessmentMethod method;
    protected BaseCurrencyAmount cumulatedFinancialValuation;
    protected BaseCurrencyAmount averageFinancialValuation;
    protected List<AssessmentResultTO> assessmentResults;
    protected String aggregationLevel;
    protected String aggregationReason;
    protected String aggregationReference;
    protected BigInteger numberOfSamples;

    /**
     * Gets the value of the method property.
     * 
     * @return
     *     possible object is
     *     {@link AssessmentMethod }
     *     
     */
    public AssessmentMethod getMethod() {
        return method;
    }

    /**
     * Sets the value of the method property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssessmentMethod }
     *     
     */
    public void setMethod(AssessmentMethod value) {
        this.method = value;
    }

    /**
     * Gets the value of the cumulatedFinancialValuation property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCumulatedFinancialValuation() {
        return cumulatedFinancialValuation;
    }

    /**
     * Sets the value of the cumulatedFinancialValuation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCumulatedFinancialValuation(BaseCurrencyAmount value) {
        this.cumulatedFinancialValuation = value;
    }

    /**
     * Gets the value of the averageFinancialValuation property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAverageFinancialValuation() {
        return averageFinancialValuation;
    }

    /**
     * Sets the value of the averageFinancialValuation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAverageFinancialValuation(BaseCurrencyAmount value) {
        this.averageFinancialValuation = value;
    }

    /**
     * Gets the value of the assessmentResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assessmentResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssessmentResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssessmentResultTO }
     * 
     * 
     */
    public List<AssessmentResultTO> getAssessmentResults() {
        if (assessmentResults == null) {
            assessmentResults = new ArrayList<AssessmentResultTO>();
        }
        return this.assessmentResults;
    }

    /**
     * Gets the value of the aggregationLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAggregationLevel() {
        return aggregationLevel;
    }

    /**
     * Sets the value of the aggregationLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAggregationLevel(String value) {
        this.aggregationLevel = value;
    }

    /**
     * Gets the value of the aggregationReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAggregationReason() {
        return aggregationReason;
    }

    /**
     * Sets the value of the aggregationReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAggregationReason(String value) {
        this.aggregationReason = value;
    }

    /**
     * Gets the value of the aggregationReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAggregationReference() {
        return aggregationReference;
    }

    /**
     * Sets the value of the aggregationReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAggregationReference(String value) {
        this.aggregationReference = value;
    }

    /**
     * Gets the value of the numberOfSamples property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfSamples() {
        return numberOfSamples;
    }

    /**
     * Sets the value of the numberOfSamples property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfSamples(BigInteger value) {
        this.numberOfSamples = value;
    }

    /**
     * Sets the value of the assessmentResults property.
     * 
     * @param assessmentResults
     *     allowed object is
     *     {@link AssessmentResultTO }
     *     
     */
    public void setAssessmentResults(List<AssessmentResultTO> assessmentResults) {
        this.assessmentResults = assessmentResults;
    }

}
