
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StructuredActual_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StructuredActual_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="isTemplate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="specification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="properties" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Property_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="overwrittenRules" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RuleResult_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuredActual_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "kind",
    "description",
    "isTemplate",
    "specification",
    "properties",
    "overwrittenRules"
})
@XmlSeeAlso({
    RoleInAgreementTO.class,
    ActualTO.class
})
public class StructuredActualTO
    extends BusinessModelObjectTO
{

    protected String kind;
    protected String description;
    protected Boolean isTemplate;
    protected ObjectReferenceTO specification;
    protected List<PropertyTO> properties;
    protected List<RuleResultTO> overwrittenRules;

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the isTemplate property.
     * This getter has been renamed from isIsTemplate() to getIsTemplate() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsTemplate() {
        return isTemplate;
    }

    /**
     * Sets the value of the isTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTemplate(Boolean value) {
        this.isTemplate = value;
    }

    /**
     * Gets the value of the specification property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getSpecification() {
        return specification;
    }

    /**
     * Sets the value of the specification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSpecification(ObjectReferenceTO value) {
        this.specification = value;
    }

    /**
     * Gets the value of the properties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the properties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProperties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PropertyTO }
     * 
     * 
     */
    public List<PropertyTO> getProperties() {
        if (properties == null) {
            properties = new ArrayList<PropertyTO>();
        }
        return this.properties;
    }

    /**
     * Gets the value of the overwrittenRules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the overwrittenRules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOverwrittenRules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RuleResultTO }
     * 
     * 
     */
    public List<RuleResultTO> getOverwrittenRules() {
        if (overwrittenRules == null) {
            overwrittenRules = new ArrayList<RuleResultTO>();
        }
        return this.overwrittenRules;
    }

    /**
     * Sets the value of the properties property.
     * 
     * @param properties
     *     allowed object is
     *     {@link PropertyTO }
     *     
     */
    public void setProperties(List<PropertyTO> properties) {
        this.properties = properties;
    }

    /**
     * Sets the value of the overwrittenRules property.
     * 
     * @param overwrittenRules
     *     allowed object is
     *     {@link RuleResultTO }
     *     
     */
    public void setOverwrittenRules(List<RuleResultTO> overwrittenRules) {
        this.overwrittenRules = overwrittenRules;
    }

}
