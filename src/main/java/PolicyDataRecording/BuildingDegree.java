
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BuildingDegree.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BuildingDegree">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Exurban"/>
 *     &lt;enumeration value="Rural"/>
 *     &lt;enumeration value="Suburban"/>
 *     &lt;enumeration value="Urban"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BuildingDegree", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum BuildingDegree {

    @XmlEnumValue("Exurban")
    EXURBAN("Exurban"),
    @XmlEnumValue("Rural")
    RURAL("Rural"),
    @XmlEnumValue("Suburban")
    SUBURBAN("Suburban"),
    @XmlEnumValue("Urban")
    URBAN("Urban");
    private final String value;

    BuildingDegree(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BuildingDegree fromValue(String v) {
        for (BuildingDegree c: BuildingDegree.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
