
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SuspectDuplicateState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SuspectDuplicateState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Created"/>
 *     &lt;enumeration value="Critical Data Difference Resolved"/>
 *     &lt;enumeration value="Duplicates Collapsed"/>
 *     &lt;enumeration value="Duplicates Do Not Collapse"/>
 *     &lt;enumeration value="Investigated Duplicates"/>
 *     &lt;enumeration value="Investigated Not Duplicates"/>
 *     &lt;enumeration value="Under Investigation"/>
 *     &lt;enumeration value="Under Investigation Of Critical Data Differences"/>
 *     &lt;enumeration value="Sure Duplicate Resolution Pending"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SuspectDuplicateState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum SuspectDuplicateState {

    @XmlEnumValue("Created")
    CREATED("Created"),
    @XmlEnumValue("Critical Data Difference Resolved")
    CRITICAL_DATA_DIFFERENCE_RESOLVED("Critical Data Difference Resolved"),
    @XmlEnumValue("Duplicates Collapsed")
    DUPLICATES_COLLAPSED("Duplicates Collapsed"),
    @XmlEnumValue("Duplicates Do Not Collapse")
    DUPLICATES_DO_NOT_COLLAPSE("Duplicates Do Not Collapse"),
    @XmlEnumValue("Investigated Duplicates")
    INVESTIGATED_DUPLICATES("Investigated Duplicates"),
    @XmlEnumValue("Investigated Not Duplicates")
    INVESTIGATED_NOT_DUPLICATES("Investigated Not Duplicates"),
    @XmlEnumValue("Under Investigation")
    UNDER_INVESTIGATION("Under Investigation"),
    @XmlEnumValue("Under Investigation Of Critical Data Differences")
    UNDER_INVESTIGATION_OF_CRITICAL_DATA_DIFFERENCES("Under Investigation Of Critical Data Differences"),
    @XmlEnumValue("Sure Duplicate Resolution Pending")
    SURE_DUPLICATE_RESOLUTION_PENDING("Sure Duplicate Resolution Pending");
    private final String value;

    SuspectDuplicateState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SuspectDuplicateState fromValue(String v) {
        for (SuspectDuplicateState c: SuspectDuplicateState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
