
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransferFacility_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransferFacility_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}AccountFacilityComponent_TO">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransferFacility_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/")
@XmlSeeAlso({
    ConditionalRecurringTransferTO.class,
    StandingOrderTO.class,
    DirectDebitMandateTO.class
})
public class TransferFacilityTO
    extends AccountFacilityComponentTO
{


}
