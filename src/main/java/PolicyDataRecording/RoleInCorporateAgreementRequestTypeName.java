
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInCorporateAgreementRequestTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInCorporateAgreementRequestTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Cause Of Risk Scenario Event"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInCorporateAgreementRequestTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum RoleInCorporateAgreementRequestTypeName {

    @XmlEnumValue("Cause Of Risk Scenario Event")
    CAUSE_OF_RISK_SCENARIO_EVENT("Cause Of Risk Scenario Event");
    private final String value;

    RoleInCorporateAgreementRequestTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInCorporateAgreementRequestTypeName fromValue(String v) {
        for (RoleInCorporateAgreementRequestTypeName c: RoleInCorporateAgreementRequestTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
