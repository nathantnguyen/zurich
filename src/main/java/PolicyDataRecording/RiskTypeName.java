
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RiskTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Liquidity Risk"/>
 *     &lt;enumeration value="Interest Rate Risk"/>
 *     &lt;enumeration value="Equity Risk"/>
 *     &lt;enumeration value="Credit Risk"/>
 *     &lt;enumeration value="Economic Risk"/>
 *     &lt;enumeration value="Concentration Risk"/>
 *     &lt;enumeration value="Catastrophic Risk"/>
 *     &lt;enumeration value="Currency Risk"/>
 *     &lt;enumeration value="Market Risk"/>
 *     &lt;enumeration value="Financial Risk"/>
 *     &lt;enumeration value="Operational Risk"/>
 *     &lt;enumeration value="Asset Liquidity Risk"/>
 *     &lt;enumeration value="Corporate Strategy Risk"/>
 *     &lt;enumeration value="Health Underwriting Risk"/>
 *     &lt;enumeration value="Image And Branding Risk"/>
 *     &lt;enumeration value="Insurance Market Risk"/>
 *     &lt;enumeration value="Investment Risk"/>
 *     &lt;enumeration value="Liability Cash Flow Risk"/>
 *     &lt;enumeration value="Life Underwriting Risk"/>
 *     &lt;enumeration value="Orsa"/>
 *     &lt;enumeration value="Outsourcing Risk"/>
 *     &lt;enumeration value="Own Investment Risk"/>
 *     &lt;enumeration value="Pandc Underwriting Risk"/>
 *     &lt;enumeration value="Policyholder Investment Risk"/>
 *     &lt;enumeration value="Regulatory Solvency Risk"/>
 *     &lt;enumeration value="Reputation Risk"/>
 *     &lt;enumeration value="Reserve Risk"/>
 *     &lt;enumeration value="Solvency Risk"/>
 *     &lt;enumeration value="Stakeholders Relations Risk"/>
 *     &lt;enumeration value="Standard Regulatory Solvency Risk"/>
 *     &lt;enumeration value="Underwriting Risk"/>
 *     &lt;enumeration value="Non Life Underwriting Risk"/>
 *     &lt;enumeration value="Property Market Value Risk"/>
 *     &lt;enumeration value="Spread Risk"/>
 *     &lt;enumeration value="Control"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RiskTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum RiskTypeName {

    @XmlEnumValue("Liquidity Risk")
    LIQUIDITY_RISK("Liquidity Risk"),
    @XmlEnumValue("Interest Rate Risk")
    INTEREST_RATE_RISK("Interest Rate Risk"),
    @XmlEnumValue("Equity Risk")
    EQUITY_RISK("Equity Risk"),
    @XmlEnumValue("Credit Risk")
    CREDIT_RISK("Credit Risk"),
    @XmlEnumValue("Economic Risk")
    ECONOMIC_RISK("Economic Risk"),
    @XmlEnumValue("Concentration Risk")
    CONCENTRATION_RISK("Concentration Risk"),
    @XmlEnumValue("Catastrophic Risk")
    CATASTROPHIC_RISK("Catastrophic Risk"),
    @XmlEnumValue("Currency Risk")
    CURRENCY_RISK("Currency Risk"),
    @XmlEnumValue("Market Risk")
    MARKET_RISK("Market Risk"),
    @XmlEnumValue("Financial Risk")
    FINANCIAL_RISK("Financial Risk"),
    @XmlEnumValue("Operational Risk")
    OPERATIONAL_RISK("Operational Risk"),
    @XmlEnumValue("Asset Liquidity Risk")
    ASSET_LIQUIDITY_RISK("Asset Liquidity Risk"),
    @XmlEnumValue("Corporate Strategy Risk")
    CORPORATE_STRATEGY_RISK("Corporate Strategy Risk"),
    @XmlEnumValue("Health Underwriting Risk")
    HEALTH_UNDERWRITING_RISK("Health Underwriting Risk"),
    @XmlEnumValue("Image And Branding Risk")
    IMAGE_AND_BRANDING_RISK("Image And Branding Risk"),
    @XmlEnumValue("Insurance Market Risk")
    INSURANCE_MARKET_RISK("Insurance Market Risk"),
    @XmlEnumValue("Investment Risk")
    INVESTMENT_RISK("Investment Risk"),
    @XmlEnumValue("Liability Cash Flow Risk")
    LIABILITY_CASH_FLOW_RISK("Liability Cash Flow Risk"),
    @XmlEnumValue("Life Underwriting Risk")
    LIFE_UNDERWRITING_RISK("Life Underwriting Risk"),
    @XmlEnumValue("Orsa")
    ORSA("Orsa"),
    @XmlEnumValue("Outsourcing Risk")
    OUTSOURCING_RISK("Outsourcing Risk"),
    @XmlEnumValue("Own Investment Risk")
    OWN_INVESTMENT_RISK("Own Investment Risk"),
    @XmlEnumValue("Pandc Underwriting Risk")
    PANDC_UNDERWRITING_RISK("Pandc Underwriting Risk"),
    @XmlEnumValue("Policyholder Investment Risk")
    POLICYHOLDER_INVESTMENT_RISK("Policyholder Investment Risk"),
    @XmlEnumValue("Regulatory Solvency Risk")
    REGULATORY_SOLVENCY_RISK("Regulatory Solvency Risk"),
    @XmlEnumValue("Reputation Risk")
    REPUTATION_RISK("Reputation Risk"),
    @XmlEnumValue("Reserve Risk")
    RESERVE_RISK("Reserve Risk"),
    @XmlEnumValue("Solvency Risk")
    SOLVENCY_RISK("Solvency Risk"),
    @XmlEnumValue("Stakeholders Relations Risk")
    STAKEHOLDERS_RELATIONS_RISK("Stakeholders Relations Risk"),
    @XmlEnumValue("Standard Regulatory Solvency Risk")
    STANDARD_REGULATORY_SOLVENCY_RISK("Standard Regulatory Solvency Risk"),
    @XmlEnumValue("Underwriting Risk")
    UNDERWRITING_RISK("Underwriting Risk"),
    @XmlEnumValue("Non Life Underwriting Risk")
    NON_LIFE_UNDERWRITING_RISK("Non Life Underwriting Risk"),
    @XmlEnumValue("Property Market Value Risk")
    PROPERTY_MARKET_VALUE_RISK("Property Market Value Risk"),
    @XmlEnumValue("Spread Risk")
    SPREAD_RISK("Spread Risk"),
    @XmlEnumValue("Control")
    CONTROL("Control");
    private final String value;

    RiskTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RiskTypeName fromValue(String v) {
        for (RiskTypeName c: RiskTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
