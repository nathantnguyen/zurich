
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductGroup_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductGroup_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO">
 *       &lt;sequence>
 *         &lt;element name="usedStatistics" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Statistics_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductGroup_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "usedStatistics"
})
public class ProductGroupTO
    extends CategoryTO
{

    protected List<StatisticsTO> usedStatistics;

    /**
     * Gets the value of the usedStatistics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usedStatistics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsedStatistics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatisticsTO }
     * 
     * 
     */
    public List<StatisticsTO> getUsedStatistics() {
        if (usedStatistics == null) {
            usedStatistics = new ArrayList<StatisticsTO>();
        }
        return this.usedStatistics;
    }

    /**
     * Sets the value of the usedStatistics property.
     * 
     * @param usedStatistics
     *     allowed object is
     *     {@link StatisticsTO }
     *     
     */
    public void setUsedStatistics(List<StatisticsTO> usedStatistics) {
        this.usedStatistics = usedStatistics;
    }

}
