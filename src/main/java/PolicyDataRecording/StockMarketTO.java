
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for StockMarket_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StockMarket_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Market_TO">
 *       &lt;sequence>
 *         &lt;element name="averageDailyTurnover" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="averageValueOfTransactions" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="firstTradingDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="liquidityFactor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="normalMarketSizeQuantity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="stopLossVolume" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="tickValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="financialAsset" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StockMarket_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "averageDailyTurnover",
    "averageValueOfTransactions",
    "firstTradingDate",
    "liquidityFactor",
    "normalMarketSizeQuantity",
    "stopLossVolume",
    "tickValue",
    "financialAsset"
})
public class StockMarketTO
    extends MarketTO
{

    protected BigInteger averageDailyTurnover;
    protected BaseCurrencyAmount averageValueOfTransactions;
    protected XMLGregorianCalendar firstTradingDate;
    protected BigInteger liquidityFactor;
    protected BigInteger normalMarketSizeQuantity;
    protected BigInteger stopLossVolume;
    protected BaseCurrencyAmount tickValue;
    protected FinancialAssetTO financialAsset;

    /**
     * Gets the value of the averageDailyTurnover property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageDailyTurnover() {
        return averageDailyTurnover;
    }

    /**
     * Sets the value of the averageDailyTurnover property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageDailyTurnover(BigInteger value) {
        this.averageDailyTurnover = value;
    }

    /**
     * Gets the value of the averageValueOfTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAverageValueOfTransactions() {
        return averageValueOfTransactions;
    }

    /**
     * Sets the value of the averageValueOfTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAverageValueOfTransactions(BaseCurrencyAmount value) {
        this.averageValueOfTransactions = value;
    }

    /**
     * Gets the value of the firstTradingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstTradingDate() {
        return firstTradingDate;
    }

    /**
     * Sets the value of the firstTradingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstTradingDate(XMLGregorianCalendar value) {
        this.firstTradingDate = value;
    }

    /**
     * Gets the value of the liquidityFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLiquidityFactor() {
        return liquidityFactor;
    }

    /**
     * Sets the value of the liquidityFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLiquidityFactor(BigInteger value) {
        this.liquidityFactor = value;
    }

    /**
     * Gets the value of the normalMarketSizeQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNormalMarketSizeQuantity() {
        return normalMarketSizeQuantity;
    }

    /**
     * Sets the value of the normalMarketSizeQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNormalMarketSizeQuantity(BigInteger value) {
        this.normalMarketSizeQuantity = value;
    }

    /**
     * Gets the value of the stopLossVolume property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStopLossVolume() {
        return stopLossVolume;
    }

    /**
     * Sets the value of the stopLossVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStopLossVolume(BigInteger value) {
        this.stopLossVolume = value;
    }

    /**
     * Gets the value of the tickValue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTickValue() {
        return tickValue;
    }

    /**
     * Sets the value of the tickValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTickValue(BaseCurrencyAmount value) {
        this.tickValue = value;
    }

    /**
     * Gets the value of the financialAsset property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAssetTO }
     *     
     */
    public FinancialAssetTO getFinancialAsset() {
        return financialAsset;
    }

    /**
     * Sets the value of the financialAsset property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setFinancialAsset(FinancialAssetTO value) {
        this.financialAsset = value;
    }

}
