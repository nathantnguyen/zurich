
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Delinquency_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Delinquency_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}StatementElement_TO">
 *       &lt;sequence>
 *         &lt;element name="aging" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/}DelinquencyAging" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Delinquency_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "aging"
})
public class DelinquencyTO
    extends StatementElementTO
{

    protected DelinquencyAging aging;

    /**
     * Gets the value of the aging property.
     * 
     * @return
     *     possible object is
     *     {@link DelinquencyAging }
     *     
     */
    public DelinquencyAging getAging() {
        return aging;
    }

    /**
     * Sets the value of the aging property.
     * 
     * @param value
     *     allowed object is
     *     {@link DelinquencyAging }
     *     
     */
    public void setAging(DelinquencyAging value) {
        this.aging = value;
    }

}
