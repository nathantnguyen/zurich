
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Insurer_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Insurer_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRole_TO">
 *       &lt;sequence>
 *         &lt;element name="insurerRejectionReason" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}InsurerRejectionReason" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Insurer_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "insurerRejectionReason"
})
public class InsurerTO
    extends FinancialServicesRoleTO
{

    protected InsurerRejectionReason insurerRejectionReason;

    /**
     * Gets the value of the insurerRejectionReason property.
     * 
     * @return
     *     possible object is
     *     {@link InsurerRejectionReason }
     *     
     */
    public InsurerRejectionReason getInsurerRejectionReason() {
        return insurerRejectionReason;
    }

    /**
     * Sets the value of the insurerRejectionReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsurerRejectionReason }
     *     
     */
    public void setInsurerRejectionReason(InsurerRejectionReason value) {
        this.insurerRejectionReason = value;
    }

}
