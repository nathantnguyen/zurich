
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyProvisionInvolvedInContractRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyProvisionInvolvedInContractRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContractRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="requestMoney" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyProvisionInvolvedInContractRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "requestMoney"
})
public class MoneyProvisionInvolvedInContractRequestTO
    extends RoleInContractRequestTO
{

    protected MoneyProvisionTO requestMoney;

    /**
     * Gets the value of the requestMoney property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public MoneyProvisionTO getRequestMoney() {
        return requestMoney;
    }

    /**
     * Sets the value of the requestMoney property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public void setRequestMoney(MoneyProvisionTO value) {
        this.requestMoney = value;
    }

}
