
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaskPerformanceScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaskPerformanceScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}TaskScore_TO">
 *       &lt;sequence>
 *         &lt;element name="includesKpi" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}KeyPerformanceIndicator_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaskPerformanceScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/", propOrder = {
    "includesKpi"
})
public class TaskPerformanceScoreTO
    extends TaskScoreTO
{

    protected List<KeyPerformanceIndicatorTO> includesKpi;

    /**
     * Gets the value of the includesKpi property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includesKpi property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludesKpi().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyPerformanceIndicatorTO }
     * 
     * 
     */
    public List<KeyPerformanceIndicatorTO> getIncludesKpi() {
        if (includesKpi == null) {
            includesKpi = new ArrayList<KeyPerformanceIndicatorTO>();
        }
        return this.includesKpi;
    }

    /**
     * Sets the value of the includesKpi property.
     * 
     * @param includesKpi
     *     allowed object is
     *     {@link KeyPerformanceIndicatorTO }
     *     
     */
    public void setIncludesKpi(List<KeyPerformanceIndicatorTO> includesKpi) {
        this.includesKpi = includesKpi;
    }

}
