
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ComponentList_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComponentList_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}MultiplicityList_TO">
 *       &lt;sequence>
 *         &lt;element name="componentListComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Actual_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contextActual" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Actual_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentList_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "componentListComponents",
    "contextActual"
})
public class ComponentListTO
    extends MultiplicityListTO
{

    protected List<ActualTO> componentListComponents;
    protected ActualTO contextActual;

    /**
     * Gets the value of the componentListComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the componentListComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponentListComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActualTO }
     * 
     * 
     */
    public List<ActualTO> getComponentListComponents() {
        if (componentListComponents == null) {
            componentListComponents = new ArrayList<ActualTO>();
        }
        return this.componentListComponents;
    }

    /**
     * Gets the value of the contextActual property.
     * 
     * @return
     *     possible object is
     *     {@link ActualTO }
     *     
     */
    public ActualTO getContextActual() {
        return contextActual;
    }

    /**
     * Sets the value of the contextActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActualTO }
     *     
     */
    public void setContextActual(ActualTO value) {
        this.contextActual = value;
    }

    /**
     * Sets the value of the componentListComponents property.
     * 
     * @param componentListComponents
     *     allowed object is
     *     {@link ActualTO }
     *     
     */
    public void setComponentListComponents(List<ActualTO> componentListComponents) {
        this.componentListComponents = componentListComponents;
    }

}
