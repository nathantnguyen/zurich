
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskHazard_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskHazard_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskExposure_TO">
 *       &lt;sequence>
 *         &lt;element name="assessmentResults" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="generalActivities" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeneralActivity_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskHazard_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "assessmentResults",
    "generalActivities"
})
public class RiskHazardTO
    extends RiskExposureTO
{

    protected List<AssessmentResultTO> assessmentResults;
    protected List<GeneralActivityTO> generalActivities;

    /**
     * Gets the value of the assessmentResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assessmentResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssessmentResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssessmentResultTO }
     * 
     * 
     */
    public List<AssessmentResultTO> getAssessmentResults() {
        if (assessmentResults == null) {
            assessmentResults = new ArrayList<AssessmentResultTO>();
        }
        return this.assessmentResults;
    }

    /**
     * Gets the value of the generalActivities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the generalActivities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeneralActivities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeneralActivityTO }
     * 
     * 
     */
    public List<GeneralActivityTO> getGeneralActivities() {
        if (generalActivities == null) {
            generalActivities = new ArrayList<GeneralActivityTO>();
        }
        return this.generalActivities;
    }

    /**
     * Sets the value of the assessmentResults property.
     * 
     * @param assessmentResults
     *     allowed object is
     *     {@link AssessmentResultTO }
     *     
     */
    public void setAssessmentResults(List<AssessmentResultTO> assessmentResults) {
        this.assessmentResults = assessmentResults;
    }

    /**
     * Sets the value of the generalActivities property.
     * 
     * @param generalActivities
     *     allowed object is
     *     {@link GeneralActivityTO }
     *     
     */
    public void setGeneralActivities(List<GeneralActivityTO> generalActivities) {
        this.generalActivities = generalActivities;
    }

}
