
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExternalAlgorithm_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExternalAlgorithm_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Algorithm_TO">
 *       &lt;sequence>
 *         &lt;element name="externalInvocationCommands" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExternalAlgorithm_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "externalInvocationCommands"
})
public class ExternalAlgorithmTO
    extends AlgorithmTO
{

    protected String externalInvocationCommands;

    /**
     * Gets the value of the externalInvocationCommands property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalInvocationCommands() {
        return externalInvocationCommands;
    }

    /**
     * Sets the value of the externalInvocationCommands property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalInvocationCommands(String value) {
        this.externalInvocationCommands = value;
    }

}
