
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ParseableAlgorithm_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ParseableAlgorithm_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Algorithm_TO">
 *       &lt;sequence>
 *         &lt;element name="formalAlgorithmDefinition" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParseableAlgorithm_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "formalAlgorithmDefinition"
})
public class ParseableAlgorithmTO
    extends AlgorithmTO
{

    protected String formalAlgorithmDefinition;

    /**
     * Gets the value of the formalAlgorithmDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormalAlgorithmDefinition() {
        return formalAlgorithmDefinition;
    }

    /**
     * Sets the value of the formalAlgorithmDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormalAlgorithmDefinition(String value) {
        this.formalAlgorithmDefinition = value;
    }

}
