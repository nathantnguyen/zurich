
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PhysicalObject_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PhysicalObject_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="inspection" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="physicalObjectComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="compositePhysicalObject" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" minOccurs="0"/>
 *         &lt;element name="shelteringStructure" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}Structure_TO" minOccurs="0"/>
 *         &lt;element name="features" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObjectType_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="physicalConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalCondition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="goodsDeliveryAuthorisation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Authorisation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="undergoneActivityOccurrences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valueNew" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="preExistingConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalCondition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInPhysicalObject" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}RoleInPhysicalObject_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="manufacturingStructure" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}Structure_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhysicalObject_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "endDate",
    "startDate",
    "externalReference",
    "inspection",
    "name",
    "description",
    "physicalObjectComponents",
    "compositePhysicalObject",
    "shelteringStructure",
    "features",
    "physicalConditions",
    "goodsDeliveryAuthorisation",
    "undergoneActivityOccurrences",
    "valueNew",
    "preExistingConditions",
    "rolesInPhysicalObject",
    "alternateReference",
    "manufacturingStructure"
})
@XmlSeeAlso({
    StructureBuiltinTO.class,
    BulkMaterialTO.class,
    LandTO.class,
    StructureTO.class,
    BodyElementTO.class,
    ObjectGroupTO.class,
    ManufacturedItemTO.class,
    FinancialTransactionMediumTO.class
})
public class PhysicalObjectTO
    extends BusinessModelObjectTO
{

    protected XMLGregorianCalendar endDate;
    protected XMLGregorianCalendar startDate;
    protected String externalReference;
    protected Boolean inspection;
    protected String name;
    protected String description;
    protected List<PhysicalObjectTO> physicalObjectComponents;
    protected PhysicalObjectTO compositePhysicalObject;
    protected StructureTO shelteringStructure;
    protected List<PhysicalObjectTypeTO> features;
    protected List<PhysicalConditionTO> physicalConditions;
    protected List<AuthorisationTO> goodsDeliveryAuthorisation;
    protected List<ActivityOccurrenceTO> undergoneActivityOccurrences;
    protected BaseCurrencyAmount valueNew;
    protected List<PhysicalConditionTO> preExistingConditions;
    protected List<RoleInPhysicalObjectTO> rolesInPhysicalObject;
    protected List<AlternateIdTO> alternateReference;
    protected StructureTO manufacturingStructure;

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the inspection property.
     * This getter has been renamed from isInspection() to getInspection() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInspection() {
        return inspection;
    }

    /**
     * Sets the value of the inspection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInspection(Boolean value) {
        this.inspection = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the physicalObjectComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the physicalObjectComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhysicalObjectComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhysicalObjectTO }
     * 
     * 
     */
    public List<PhysicalObjectTO> getPhysicalObjectComponents() {
        if (physicalObjectComponents == null) {
            physicalObjectComponents = new ArrayList<PhysicalObjectTO>();
        }
        return this.physicalObjectComponents;
    }

    /**
     * Gets the value of the compositePhysicalObject property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public PhysicalObjectTO getCompositePhysicalObject() {
        return compositePhysicalObject;
    }

    /**
     * Sets the value of the compositePhysicalObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setCompositePhysicalObject(PhysicalObjectTO value) {
        this.compositePhysicalObject = value;
    }

    /**
     * Gets the value of the shelteringStructure property.
     * 
     * @return
     *     possible object is
     *     {@link StructureTO }
     *     
     */
    public StructureTO getShelteringStructure() {
        return shelteringStructure;
    }

    /**
     * Sets the value of the shelteringStructure property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureTO }
     *     
     */
    public void setShelteringStructure(StructureTO value) {
        this.shelteringStructure = value;
    }

    /**
     * Gets the value of the features property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the features property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhysicalObjectTypeTO }
     * 
     * 
     */
    public List<PhysicalObjectTypeTO> getFeatures() {
        if (features == null) {
            features = new ArrayList<PhysicalObjectTypeTO>();
        }
        return this.features;
    }

    /**
     * Gets the value of the physicalConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the physicalConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhysicalConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhysicalConditionTO }
     * 
     * 
     */
    public List<PhysicalConditionTO> getPhysicalConditions() {
        if (physicalConditions == null) {
            physicalConditions = new ArrayList<PhysicalConditionTO>();
        }
        return this.physicalConditions;
    }

    /**
     * Gets the value of the goodsDeliveryAuthorisation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the goodsDeliveryAuthorisation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGoodsDeliveryAuthorisation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AuthorisationTO }
     * 
     * 
     */
    public List<AuthorisationTO> getGoodsDeliveryAuthorisation() {
        if (goodsDeliveryAuthorisation == null) {
            goodsDeliveryAuthorisation = new ArrayList<AuthorisationTO>();
        }
        return this.goodsDeliveryAuthorisation;
    }

    /**
     * Gets the value of the undergoneActivityOccurrences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the undergoneActivityOccurrences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUndergoneActivityOccurrences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceTO }
     * 
     * 
     */
    public List<ActivityOccurrenceTO> getUndergoneActivityOccurrences() {
        if (undergoneActivityOccurrences == null) {
            undergoneActivityOccurrences = new ArrayList<ActivityOccurrenceTO>();
        }
        return this.undergoneActivityOccurrences;
    }

    /**
     * Gets the value of the valueNew property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getValueNew() {
        return valueNew;
    }

    /**
     * Sets the value of the valueNew property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setValueNew(BaseCurrencyAmount value) {
        this.valueNew = value;
    }

    /**
     * Gets the value of the preExistingConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the preExistingConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPreExistingConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhysicalConditionTO }
     * 
     * 
     */
    public List<PhysicalConditionTO> getPreExistingConditions() {
        if (preExistingConditions == null) {
            preExistingConditions = new ArrayList<PhysicalConditionTO>();
        }
        return this.preExistingConditions;
    }

    /**
     * Gets the value of the rolesInPhysicalObject property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInPhysicalObject property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInPhysicalObject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInPhysicalObjectTO }
     * 
     * 
     */
    public List<RoleInPhysicalObjectTO> getRolesInPhysicalObject() {
        if (rolesInPhysicalObject == null) {
            rolesInPhysicalObject = new ArrayList<RoleInPhysicalObjectTO>();
        }
        return this.rolesInPhysicalObject;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Gets the value of the manufacturingStructure property.
     * 
     * @return
     *     possible object is
     *     {@link StructureTO }
     *     
     */
    public StructureTO getManufacturingStructure() {
        return manufacturingStructure;
    }

    /**
     * Sets the value of the manufacturingStructure property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureTO }
     *     
     */
    public void setManufacturingStructure(StructureTO value) {
        this.manufacturingStructure = value;
    }

    /**
     * Sets the value of the physicalObjectComponents property.
     * 
     * @param physicalObjectComponents
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setPhysicalObjectComponents(List<PhysicalObjectTO> physicalObjectComponents) {
        this.physicalObjectComponents = physicalObjectComponents;
    }

    /**
     * Sets the value of the features property.
     * 
     * @param features
     *     allowed object is
     *     {@link PhysicalObjectTypeTO }
     *     
     */
    public void setFeatures(List<PhysicalObjectTypeTO> features) {
        this.features = features;
    }

    /**
     * Sets the value of the physicalConditions property.
     * 
     * @param physicalConditions
     *     allowed object is
     *     {@link PhysicalConditionTO }
     *     
     */
    public void setPhysicalConditions(List<PhysicalConditionTO> physicalConditions) {
        this.physicalConditions = physicalConditions;
    }

    /**
     * Sets the value of the goodsDeliveryAuthorisation property.
     * 
     * @param goodsDeliveryAuthorisation
     *     allowed object is
     *     {@link AuthorisationTO }
     *     
     */
    public void setGoodsDeliveryAuthorisation(List<AuthorisationTO> goodsDeliveryAuthorisation) {
        this.goodsDeliveryAuthorisation = goodsDeliveryAuthorisation;
    }

    /**
     * Sets the value of the undergoneActivityOccurrences property.
     * 
     * @param undergoneActivityOccurrences
     *     allowed object is
     *     {@link ActivityOccurrenceTO }
     *     
     */
    public void setUndergoneActivityOccurrences(List<ActivityOccurrenceTO> undergoneActivityOccurrences) {
        this.undergoneActivityOccurrences = undergoneActivityOccurrences;
    }

    /**
     * Sets the value of the preExistingConditions property.
     * 
     * @param preExistingConditions
     *     allowed object is
     *     {@link PhysicalConditionTO }
     *     
     */
    public void setPreExistingConditions(List<PhysicalConditionTO> preExistingConditions) {
        this.preExistingConditions = preExistingConditions;
    }

    /**
     * Sets the value of the rolesInPhysicalObject property.
     * 
     * @param rolesInPhysicalObject
     *     allowed object is
     *     {@link RoleInPhysicalObjectTO }
     *     
     */
    public void setRolesInPhysicalObject(List<RoleInPhysicalObjectTO> rolesInPhysicalObject) {
        this.rolesInPhysicalObject = rolesInPhysicalObject;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
