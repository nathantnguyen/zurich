
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CustomerOfProductGroup_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerOfProductGroup_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInProductGroup_TO">
 *       &lt;sequence>
 *         &lt;element name="ncdLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="ncdLevelDuration" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="ncdLevelDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="ncdConfirmation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}CustomerOfProductGroupStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerOfProductGroup_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "ncdLevel",
    "ncdLevelDuration",
    "ncdLevelDate",
    "ncdConfirmation",
    "status"
})
public class CustomerOfProductGroupTO
    extends RoleInProductGroupTO
{

    protected BigDecimal ncdLevel;
    protected Duration ncdLevelDuration;
    protected XMLGregorianCalendar ncdLevelDate;
    protected Boolean ncdConfirmation;
    protected List<CustomerOfProductGroupStatusTO> status;

    /**
     * Gets the value of the ncdLevel property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNcdLevel() {
        return ncdLevel;
    }

    /**
     * Sets the value of the ncdLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNcdLevel(BigDecimal value) {
        this.ncdLevel = value;
    }

    /**
     * Gets the value of the ncdLevelDuration property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getNcdLevelDuration() {
        return ncdLevelDuration;
    }

    /**
     * Sets the value of the ncdLevelDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setNcdLevelDuration(Duration value) {
        this.ncdLevelDuration = value;
    }

    /**
     * Gets the value of the ncdLevelDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNcdLevelDate() {
        return ncdLevelDate;
    }

    /**
     * Sets the value of the ncdLevelDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNcdLevelDate(XMLGregorianCalendar value) {
        this.ncdLevelDate = value;
    }

    /**
     * Gets the value of the ncdConfirmation property.
     * This getter has been renamed from isNcdConfirmation() to getNcdConfirmation() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNcdConfirmation() {
        return ncdConfirmation;
    }

    /**
     * Sets the value of the ncdConfirmation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNcdConfirmation(Boolean value) {
        this.ncdConfirmation = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerOfProductGroupStatusTO }
     * 
     * 
     */
    public List<CustomerOfProductGroupStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<CustomerOfProductGroupStatusTO>();
        }
        return this.status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link CustomerOfProductGroupStatusTO }
     *     
     */
    public void setStatus(List<CustomerOfProductGroupStatusTO> status) {
        this.status = status;
    }

}
