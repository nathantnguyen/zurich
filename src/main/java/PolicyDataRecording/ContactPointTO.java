
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContactPoint_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactPoint_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}ContactPointStatus" minOccurs="0"/>
 *         &lt;element name="contactPointAsString" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="referredPlaces" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contactPreferencesReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="partyRegistrations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="isForeign" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactPoint_TO", propOrder = {
    "startDate",
    "endDate",
    "status",
    "contactPointAsString",
    "referredPlaces",
    "contactPreferencesReference",
    "partyRegistrations",
    "isForeign",
    "externalReference",
    "alternateReference"
})
@XmlSeeAlso({
    ElectronicAddressTO.class,
    PostalAddressTO.class,
    CareOfAddressTO.class,
    TelephoneNumberTO.class,
    UnstructuredContactPointTO.class
})
public class ContactPointTO
    extends BusinessModelObjectTO
{

    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected ContactPointStatus status;
    protected String contactPointAsString;
    protected List<PlaceTO> referredPlaces;
    protected List<ObjectReferenceTO> contactPreferencesReference;
    protected List<PartyRegistrationTO> partyRegistrations;
    protected Boolean isForeign;
    protected String externalReference;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ContactPointStatus }
     *     
     */
    public ContactPointStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPointStatus }
     *     
     */
    public void setStatus(ContactPointStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the contactPointAsString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPointAsString() {
        return contactPointAsString;
    }

    /**
     * Sets the value of the contactPointAsString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPointAsString(String value) {
        this.contactPointAsString = value;
    }

    /**
     * Gets the value of the referredPlaces property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referredPlaces property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferredPlaces().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlaceTO }
     * 
     * 
     */
    public List<PlaceTO> getReferredPlaces() {
        if (referredPlaces == null) {
            referredPlaces = new ArrayList<PlaceTO>();
        }
        return this.referredPlaces;
    }

    /**
     * Gets the value of the contactPreferencesReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactPreferencesReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactPreferencesReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getContactPreferencesReference() {
        if (contactPreferencesReference == null) {
            contactPreferencesReference = new ArrayList<ObjectReferenceTO>();
        }
        return this.contactPreferencesReference;
    }

    /**
     * Gets the value of the partyRegistrations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyRegistrations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyRegistrations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyRegistrationTO }
     * 
     * 
     */
    public List<PartyRegistrationTO> getPartyRegistrations() {
        if (partyRegistrations == null) {
            partyRegistrations = new ArrayList<PartyRegistrationTO>();
        }
        return this.partyRegistrations;
    }

    /**
     * Gets the value of the isForeign property.
     * This getter has been renamed from isIsForeign() to getIsForeign() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsForeign() {
        return isForeign;
    }

    /**
     * Sets the value of the isForeign property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsForeign(Boolean value) {
        this.isForeign = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the referredPlaces property.
     * 
     * @param referredPlaces
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setReferredPlaces(List<PlaceTO> referredPlaces) {
        this.referredPlaces = referredPlaces;
    }

    /**
     * Sets the value of the contactPreferencesReference property.
     * 
     * @param contactPreferencesReference
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setContactPreferencesReference(List<ObjectReferenceTO> contactPreferencesReference) {
        this.contactPreferencesReference = contactPreferencesReference;
    }

    /**
     * Sets the value of the partyRegistrations property.
     * 
     * @param partyRegistrations
     *     allowed object is
     *     {@link PartyRegistrationTO }
     *     
     */
    public void setPartyRegistrations(List<PartyRegistrationTO> partyRegistrations) {
        this.partyRegistrations = partyRegistrations;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
