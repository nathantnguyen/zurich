
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailElectronicType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmailElectronicType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Internet Email"/>
 *     &lt;enumeration value="Proprietary"/>
 *     &lt;enumeration value="Ftp Site"/>
 *     &lt;enumeration value="Http Site"/>
 *     &lt;enumeration value="Lotus Notes"/>
 *     &lt;enumeration value="X 400"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EmailElectronicType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum EmailElectronicType {

    @XmlEnumValue("Internet Email")
    INTERNET_EMAIL("Internet Email"),
    @XmlEnumValue("Proprietary")
    PROPRIETARY("Proprietary"),
    @XmlEnumValue("Ftp Site")
    FTP_SITE("Ftp Site"),
    @XmlEnumValue("Http Site")
    HTTP_SITE("Http Site"),
    @XmlEnumValue("Lotus Notes")
    LOTUS_NOTES("Lotus Notes"),
    @XmlEnumValue("X 400")
    X_400("X 400");
    private final String value;

    EmailElectronicType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmailElectronicType fromValue(String v) {
        for (EmailElectronicType c: EmailElectronicType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
