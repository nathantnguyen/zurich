
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectUsage_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectUsage_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInRolePlayer_TO">
 *       &lt;sequence>
 *         &lt;element name="usage" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}ObjectUsageUsage" minOccurs="0"/>
 *         &lt;element name="usedPhysicalObject" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectUsage_TO", propOrder = {
    "usage",
    "usedPhysicalObject"
})
public class ObjectUsageTO
    extends RoleInRolePlayerTO
{

    protected ObjectUsageUsage usage;
    protected PhysicalObjectTO usedPhysicalObject;

    /**
     * Gets the value of the usage property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectUsageUsage }
     *     
     */
    public ObjectUsageUsage getUsage() {
        return usage;
    }

    /**
     * Sets the value of the usage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectUsageUsage }
     *     
     */
    public void setUsage(ObjectUsageUsage value) {
        this.usage = value;
    }

    /**
     * Gets the value of the usedPhysicalObject property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public PhysicalObjectTO getUsedPhysicalObject() {
        return usedPhysicalObject;
    }

    /**
     * Sets the value of the usedPhysicalObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setUsedPhysicalObject(PhysicalObjectTO value) {
        this.usedPhysicalObject = value;
    }

}
