
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Policyholder_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Policyholder_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRole_TO">
 *       &lt;sequence>
 *         &lt;element name="policyholderRejectionReason" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}PolicyholderRejectionReason" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Policyholder_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "policyholderRejectionReason"
})
public class PolicyholderTO
    extends FinancialServicesRoleTO
{

    protected PolicyholderRejectionReason policyholderRejectionReason;

    /**
     * Gets the value of the policyholderRejectionReason property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyholderRejectionReason }
     *     
     */
    public PolicyholderRejectionReason getPolicyholderRejectionReason() {
        return policyholderRejectionReason;
    }

    /**
     * Sets the value of the policyholderRejectionReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyholderRejectionReason }
     *     
     */
    public void setPolicyholderRejectionReason(PolicyholderRejectionReason value) {
        this.policyholderRejectionReason = value;
    }

}
