
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Type_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Type_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Type_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "description"
})
@XmlSeeAlso({
    NoteTypeTO.class,
    DocumentTemplateTypeTO.class,
    MarketableProductTypeTO.class,
    ContractSpecificationTypeTO.class,
    RiskAgreementTypeTO.class,
    CorporateAgreementTypeTO.class,
    CorporateAgreementRequestTypeTO.class,
    TaxRegulatoryAgreementTypeTO.class,
    RiskTypeTO.class,
    IndexValueTypeTO.class,
    PublishedEventTypeTO.class,
    FinancialValuationTypeTO.class,
    ConditionTypeTO.class,
    ActivityOccurrenceTypeTO.class,
    AssessmentResultRegistrationTypeTO.class,
    AuditTypeTO.class,
    TransportationTypeTO.class,
    AuthorisationTypeTO.class,
    PlaceTypeTO.class,
    MedicalConditionTypeTO.class,
    ObjectTreatmentTypeTO.class,
    ScoreTypeTO.class,
    AssessmentActivityTypeTO.class,
    ModelSpecificationTypeTO.class,
    ManufacturedItemTypeTO.class,
    PhysicalObjectTypeTO.class,
    ObjectGroupTypeTO.class,
    ActuarialStatisticsTypeTO.class,
    SolvencyRiskParametersTypeTO.class,
    CommercialAgreementTypeTO.class,
    FinancialServicesRequestTypeTO.class,
    CoverageComponentTypeTO.class,
    DerivativeContractTypeTO.class,
    InsuredTypeTO.class,
    ServiceComponentTypeTO.class,
    PaymentFacilityTypeTO.class,
    FinancialMovementRequestTypeTO.class,
    TaskSpecificationTypeTO.class,
    TaskTypeTO.class,
    TaskPerformanceScoreTypeTO.class,
    LegalActionTypeTO.class,
    ReservedAmountTypeTO.class,
    MoneyProvisionElementPartTypeTO.class,
    MoneyProvisionElementTypeTO.class,
    FinancialStatementTypeTO.class,
    CapitalInstrumentTypeTO.class,
    MarketableObjectTypeTO.class,
    ContractInstrumentTypeTO.class,
    MonetaryAccountTypeTO.class,
    FundTypeTO.class,
    BondTypeTO.class,
    FinancialAssetTypeTO.class,
    AccountEntryTypeTO.class,
    FutureTypeTO.class,
    DebtInstrumentTypeTO.class,
    ReserveAccountTypeTO.class,
    CompositeInstrumentTypeTO.class,
    DerivativeTypeTO.class,
    SwapTypeTO.class,
    OptionTypeTO.class,
    GoalAndNeedTypeTO.class,
    NationalRegistrationTypeTO.class,
    PartyRegistrationTypeTO.class,
    PartyNameTypeTO.class,
    RolePlayerPlaceUsageTypeTO.class,
    OrganisationTypeTO.class,
    VirtualPartyTypeTO.class,
    GeneralActivityTypeTO.class,
    LifeEventTypeTO.class,
    RelationshipTypeTO.class,
    RoleTypeTO.class
})
public class TypeTO
    extends BaseTransferObject
{

    protected String description;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
