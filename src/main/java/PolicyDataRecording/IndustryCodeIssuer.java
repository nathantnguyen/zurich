
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IndustryCodeIssuer.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IndustryCodeIssuer">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Class Code"/>
 *     &lt;enumeration value="Naics"/>
 *     &lt;enumeration value="Sic"/>
 *     &lt;enumeration value="Multi Peril"/>
 *     &lt;enumeration value="Ncci"/>
 *     &lt;enumeration value="IBC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IndustryCodeIssuer", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum IndustryCodeIssuer {

    @XmlEnumValue("Class Code")
    CLASS_CODE("Class Code"),
    @XmlEnumValue("Naics")
    NAICS("Naics"),
    @XmlEnumValue("Sic")
    SIC("Sic"),
    @XmlEnumValue("Multi Peril")
    MULTI_PERIL("Multi Peril"),
    @XmlEnumValue("Ncci")
    NCCI("Ncci"),
    IBC("IBC");
    private final String value;

    IndustryCodeIssuer(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IndustryCodeIssuer fromValue(String v) {
        for (IndustryCodeIssuer c: IndustryCodeIssuer.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
