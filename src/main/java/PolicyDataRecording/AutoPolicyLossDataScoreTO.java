
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AutoPolicyLossDataScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutoPolicyLossDataScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="collisionClaimsCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="collisionEarnedVehicleCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="liabilityEarnedVehicleCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="liabilityClaimsCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="comprehensiveClaimsCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="comprehensiveEarnedVehicleCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoPolicyLossDataScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "collisionClaimsCount",
    "collisionEarnedVehicleCount",
    "liabilityEarnedVehicleCount",
    "liabilityClaimsCount",
    "comprehensiveClaimsCount",
    "comprehensiveEarnedVehicleCount"
})
public class AutoPolicyLossDataScoreTO
    extends ScoreTO
{

    protected BigDecimal collisionClaimsCount;
    protected BigDecimal collisionEarnedVehicleCount;
    protected BigDecimal liabilityEarnedVehicleCount;
    protected BigDecimal liabilityClaimsCount;
    protected BigDecimal comprehensiveClaimsCount;
    protected BigDecimal comprehensiveEarnedVehicleCount;

    /**
     * Gets the value of the collisionClaimsCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCollisionClaimsCount() {
        return collisionClaimsCount;
    }

    /**
     * Sets the value of the collisionClaimsCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCollisionClaimsCount(BigDecimal value) {
        this.collisionClaimsCount = value;
    }

    /**
     * Gets the value of the collisionEarnedVehicleCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCollisionEarnedVehicleCount() {
        return collisionEarnedVehicleCount;
    }

    /**
     * Sets the value of the collisionEarnedVehicleCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCollisionEarnedVehicleCount(BigDecimal value) {
        this.collisionEarnedVehicleCount = value;
    }

    /**
     * Gets the value of the liabilityEarnedVehicleCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLiabilityEarnedVehicleCount() {
        return liabilityEarnedVehicleCount;
    }

    /**
     * Sets the value of the liabilityEarnedVehicleCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLiabilityEarnedVehicleCount(BigDecimal value) {
        this.liabilityEarnedVehicleCount = value;
    }

    /**
     * Gets the value of the liabilityClaimsCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLiabilityClaimsCount() {
        return liabilityClaimsCount;
    }

    /**
     * Sets the value of the liabilityClaimsCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLiabilityClaimsCount(BigDecimal value) {
        this.liabilityClaimsCount = value;
    }

    /**
     * Gets the value of the comprehensiveClaimsCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComprehensiveClaimsCount() {
        return comprehensiveClaimsCount;
    }

    /**
     * Sets the value of the comprehensiveClaimsCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComprehensiveClaimsCount(BigDecimal value) {
        this.comprehensiveClaimsCount = value;
    }

    /**
     * Gets the value of the comprehensiveEarnedVehicleCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComprehensiveEarnedVehicleCount() {
        return comprehensiveEarnedVehicleCount;
    }

    /**
     * Sets the value of the comprehensiveEarnedVehicleCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComprehensiveEarnedVehicleCount(BigDecimal value) {
        this.comprehensiveEarnedVehicleCount = value;
    }

}
