
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityOccurrenceTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivityOccurrenceTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Social Media Activity"/>
 *     &lt;enumeration value="Project"/>
 *     &lt;enumeration value="Construction Activity"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActivityOccurrenceTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum ActivityOccurrenceTypeName {

    @XmlEnumValue("Social Media Activity")
    SOCIAL_MEDIA_ACTIVITY("Social Media Activity"),
    @XmlEnumValue("Project")
    PROJECT("Project"),
    @XmlEnumValue("Construction Activity")
    CONSTRUCTION_ACTIVITY("Construction Activity");
    private final String value;

    ActivityOccurrenceTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActivityOccurrenceTypeName fromValue(String v) {
        for (ActivityOccurrenceTypeName c: ActivityOccurrenceTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
