
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInStatisticsTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInStatisticsTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Index Risk Score"/>
 *     &lt;enumeration value="Source Of Statistics"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInStatisticsTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/")
@XmlEnum
public enum RoleInStatisticsTypeName {

    @XmlEnumValue("Index Risk Score")
    INDEX_RISK_SCORE("Index Risk Score"),
    @XmlEnumValue("Source Of Statistics")
    SOURCE_OF_STATISTICS("Source Of Statistics");
    private final String value;

    RoleInStatisticsTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInStatisticsTypeName fromValue(String v) {
        for (RoleInStatisticsTypeName c: RoleInStatisticsTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
