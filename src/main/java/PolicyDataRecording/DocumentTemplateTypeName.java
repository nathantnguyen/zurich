
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentTemplateTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DocumentTemplateTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Term And Condition"/>
 *     &lt;enumeration value="Audit Findings Report Template"/>
 *     &lt;enumeration value="Internal Control Report Template"/>
 *     &lt;enumeration value="Report Template"/>
 *     &lt;enumeration value="Risk Position Disclosure Form"/>
 *     &lt;enumeration value="Solvency Asset Report Template"/>
 *     &lt;enumeration value="Solvency Capital Management Report Template"/>
 *     &lt;enumeration value="Solvency Report Template"/>
 *     &lt;enumeration value="Solvency Risk Report Template"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DocumentTemplateTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum DocumentTemplateTypeName {

    @XmlEnumValue("Term And Condition")
    TERM_AND_CONDITION("Term And Condition"),
    @XmlEnumValue("Audit Findings Report Template")
    AUDIT_FINDINGS_REPORT_TEMPLATE("Audit Findings Report Template"),
    @XmlEnumValue("Internal Control Report Template")
    INTERNAL_CONTROL_REPORT_TEMPLATE("Internal Control Report Template"),
    @XmlEnumValue("Report Template")
    REPORT_TEMPLATE("Report Template"),
    @XmlEnumValue("Risk Position Disclosure Form")
    RISK_POSITION_DISCLOSURE_FORM("Risk Position Disclosure Form"),
    @XmlEnumValue("Solvency Asset Report Template")
    SOLVENCY_ASSET_REPORT_TEMPLATE("Solvency Asset Report Template"),
    @XmlEnumValue("Solvency Capital Management Report Template")
    SOLVENCY_CAPITAL_MANAGEMENT_REPORT_TEMPLATE("Solvency Capital Management Report Template"),
    @XmlEnumValue("Solvency Report Template")
    SOLVENCY_REPORT_TEMPLATE("Solvency Report Template"),
    @XmlEnumValue("Solvency Risk Report Template")
    SOLVENCY_RISK_REPORT_TEMPLATE("Solvency Risk Report Template");
    private final String value;

    DocumentTemplateTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DocumentTemplateTypeName fromValue(String v) {
        for (DocumentTemplateTypeName c: DocumentTemplateTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
