
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentFacilityTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentFacilityTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Cheque Facility"/>
 *     &lt;enumeration value="Credit Card Facility"/>
 *     &lt;enumeration value="Bank Transfer Facility"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentFacilityTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum PaymentFacilityTypeName {

    @XmlEnumValue("Cheque Facility")
    CHEQUE_FACILITY("Cheque Facility"),
    @XmlEnumValue("Credit Card Facility")
    CREDIT_CARD_FACILITY("Credit Card Facility"),
    @XmlEnumValue("Bank Transfer Facility")
    BANK_TRANSFER_FACILITY("Bank Transfer Facility");
    private final String value;

    PaymentFacilityTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentFacilityTypeName fromValue(String v) {
        for (PaymentFacilityTypeName c: PaymentFacilityTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
