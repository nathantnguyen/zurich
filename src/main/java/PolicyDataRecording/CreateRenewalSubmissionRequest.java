
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createRenewalSubmissionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createRenewalSubmissionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestHeader" type="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}RequestHeader"/>
 *         &lt;element name="nonRenewalIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean"/>
 *         &lt;element name="singleSubmissionIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean"/>
 *         &lt;element name="policies" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CommercialAgreement_TO" maxOccurs="unbounded"/>
 *         &lt;element name="nonRenewalReasonCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="newCarrier" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Company_TO" minOccurs="0"/>
 *         &lt;element name="nonRenewalCause" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createRenewalSubmissionRequest", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/", propOrder = {
    "requestHeader",
    "nonRenewalIndicator",
    "singleSubmissionIndicator",
    "policies",
    "nonRenewalReasonCode",
    "newCarrier",
    "nonRenewalCause"
})
public class CreateRenewalSubmissionRequest {

    @XmlElement(required = true)
    protected RequestHeader requestHeader;
    protected boolean nonRenewalIndicator;
    protected boolean singleSubmissionIndicator;
    @XmlElement(required = true)
    protected List<CommercialAgreementTO> policies;
    protected String nonRenewalReasonCode;
    protected CompanyTO newCarrier;
    protected String nonRenewalCause;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the nonRenewalIndicator property.
     * This getter has been renamed from isNonRenewalIndicator() to getNonRenewalIndicator() by cxf-xjc-boolean plugin.
     * 
     */
    public boolean getNonRenewalIndicator() {
        return nonRenewalIndicator;
    }

    /**
     * Sets the value of the nonRenewalIndicator property.
     * 
     */
    public void setNonRenewalIndicator(boolean value) {
        this.nonRenewalIndicator = value;
    }

    /**
     * Gets the value of the singleSubmissionIndicator property.
     * This getter has been renamed from isSingleSubmissionIndicator() to getSingleSubmissionIndicator() by cxf-xjc-boolean plugin.
     * 
     */
    public boolean getSingleSubmissionIndicator() {
        return singleSubmissionIndicator;
    }

    /**
     * Sets the value of the singleSubmissionIndicator property.
     * 
     */
    public void setSingleSubmissionIndicator(boolean value) {
        this.singleSubmissionIndicator = value;
    }

    /**
     * Gets the value of the policies property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policies property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicies().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommercialAgreementTO }
     * 
     * 
     */
    public List<CommercialAgreementTO> getPolicies() {
        if (policies == null) {
            policies = new ArrayList<CommercialAgreementTO>();
        }
        return this.policies;
    }

    /**
     * Gets the value of the nonRenewalReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonRenewalReasonCode() {
        return nonRenewalReasonCode;
    }

    /**
     * Sets the value of the nonRenewalReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonRenewalReasonCode(String value) {
        this.nonRenewalReasonCode = value;
    }

    /**
     * Gets the value of the newCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyTO }
     *     
     */
    public CompanyTO getNewCarrier() {
        return newCarrier;
    }

    /**
     * Sets the value of the newCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyTO }
     *     
     */
    public void setNewCarrier(CompanyTO value) {
        this.newCarrier = value;
    }

    /**
     * Gets the value of the nonRenewalCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonRenewalCause() {
        return nonRenewalCause;
    }

    /**
     * Sets the value of the nonRenewalCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonRenewalCause(String value) {
        this.nonRenewalCause = value;
    }

    /**
     * Sets the value of the policies property.
     * 
     * @param policies
     *     allowed object is
     *     {@link CommercialAgreementTO }
     *     
     */
    public void setPolicies(List<CommercialAgreementTO> policies) {
        this.policies = policies;
    }

}
