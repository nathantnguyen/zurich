
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AirbagsLocation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AirbagsLocation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All Seats"/>
 *     &lt;enumeration value="Driver"/>
 *     &lt;enumeration value="Driver And Front Passenger"/>
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Side Or Rear"/>
 *     &lt;enumeration value="Front And Rear"/>
 *     &lt;enumeration value="Front Only"/>
 *     &lt;enumeration value="Rear Seats Only"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AirbagsLocation", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum AirbagsLocation {

    @XmlEnumValue("All Seats")
    ALL_SEATS("All Seats"),
    @XmlEnumValue("Driver")
    DRIVER("Driver"),
    @XmlEnumValue("Driver And Front Passenger")
    DRIVER_AND_FRONT_PASSENGER("Driver And Front Passenger"),
    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Side Or Rear")
    SIDE_OR_REAR("Side Or Rear"),
    @XmlEnumValue("Front And Rear")
    FRONT_AND_REAR("Front And Rear"),
    @XmlEnumValue("Front Only")
    FRONT_ONLY("Front Only"),
    @XmlEnumValue("Rear Seats Only")
    REAR_SEATS_ONLY("Rear Seats Only");
    private final String value;

    AirbagsLocation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AirbagsLocation fromValue(String v) {
        for (AirbagsLocation c: AirbagsLocation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
