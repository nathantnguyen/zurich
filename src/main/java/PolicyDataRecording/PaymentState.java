
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Allocated"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Paid"/>
 *     &lt;enumeration value="Paid Back"/>
 *     &lt;enumeration value="Partially Allocated"/>
 *     &lt;enumeration value="Posted"/>
 *     &lt;enumeration value="Received"/>
 *     &lt;enumeration value="Rejected"/>
 *     &lt;enumeration value="Replaced"/>
 *     &lt;enumeration value="Authorised"/>
 *     &lt;enumeration value="Cheque Paid"/>
 *     &lt;enumeration value="Cheque Rejected"/>
 *     &lt;enumeration value="Cheque Valued"/>
 *     &lt;enumeration value="Cleared"/>
 *     &lt;enumeration value="Created"/>
 *     &lt;enumeration value="Deposited"/>
 *     &lt;enumeration value="Final For Cheque"/>
 *     &lt;enumeration value="Stopped"/>
 *     &lt;enumeration value="Unallocated"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum PaymentState {

    @XmlEnumValue("Allocated")
    ALLOCATED("Allocated"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Paid")
    PAID("Paid"),
    @XmlEnumValue("Paid Back")
    PAID_BACK("Paid Back"),
    @XmlEnumValue("Partially Allocated")
    PARTIALLY_ALLOCATED("Partially Allocated"),
    @XmlEnumValue("Posted")
    POSTED("Posted"),
    @XmlEnumValue("Received")
    RECEIVED("Received"),
    @XmlEnumValue("Rejected")
    REJECTED("Rejected"),
    @XmlEnumValue("Replaced")
    REPLACED("Replaced"),
    @XmlEnumValue("Authorised")
    AUTHORISED("Authorised"),
    @XmlEnumValue("Cheque Paid")
    CHEQUE_PAID("Cheque Paid"),
    @XmlEnumValue("Cheque Rejected")
    CHEQUE_REJECTED("Cheque Rejected"),
    @XmlEnumValue("Cheque Valued")
    CHEQUE_VALUED("Cheque Valued"),
    @XmlEnumValue("Cleared")
    CLEARED("Cleared"),
    @XmlEnumValue("Created")
    CREATED("Created"),
    @XmlEnumValue("Deposited")
    DEPOSITED("Deposited"),
    @XmlEnumValue("Final For Cheque")
    FINAL_FOR_CHEQUE("Final For Cheque"),
    @XmlEnumValue("Stopped")
    STOPPED("Stopped"),
    @XmlEnumValue("Unallocated")
    UNALLOCATED("Unallocated");
    private final String value;

    PaymentState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentState fromValue(String v) {
        for (PaymentState c: PaymentState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
