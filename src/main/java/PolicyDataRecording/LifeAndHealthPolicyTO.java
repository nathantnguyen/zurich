
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LifeAndHealthPolicy_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LifeAndHealthPolicy_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}IndividualAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="profitSharingDetails" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}ProfitSharingComponent_TO" minOccurs="0"/>
 *         &lt;element name="netSurrenderValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LifeAndHealthPolicy_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "profitSharingDetails",
    "netSurrenderValue"
})
@XmlSeeAlso({
    AnnuityTO.class,
    IndividualLifeInsurancePolicyTO.class
})
public class LifeAndHealthPolicyTO
    extends IndividualAgreementTO
{

    protected ProfitSharingComponentTO profitSharingDetails;
    protected BaseCurrencyAmount netSurrenderValue;

    /**
     * Gets the value of the profitSharingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ProfitSharingComponentTO }
     *     
     */
    public ProfitSharingComponentTO getProfitSharingDetails() {
        return profitSharingDetails;
    }

    /**
     * Sets the value of the profitSharingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfitSharingComponentTO }
     *     
     */
    public void setProfitSharingDetails(ProfitSharingComponentTO value) {
        this.profitSharingDetails = value;
    }

    /**
     * Gets the value of the netSurrenderValue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getNetSurrenderValue() {
        return netSurrenderValue;
    }

    /**
     * Sets the value of the netSurrenderValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setNetSurrenderValue(BaseCurrencyAmount value) {
        this.netSurrenderValue = value;
    }

}
