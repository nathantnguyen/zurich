
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DeathCertificate_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeathCertificate_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="registeredDeathDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="causesOfDeath" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}MedicalCondition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeathCertificate_TO", propOrder = {
    "registeredDeathDate",
    "causesOfDeath"
})
public class DeathCertificateTO
    extends PartyRegistrationTO
{

    protected XMLGregorianCalendar registeredDeathDate;
    protected List<MedicalConditionTO> causesOfDeath;

    /**
     * Gets the value of the registeredDeathDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegisteredDeathDate() {
        return registeredDeathDate;
    }

    /**
     * Sets the value of the registeredDeathDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegisteredDeathDate(XMLGregorianCalendar value) {
        this.registeredDeathDate = value;
    }

    /**
     * Gets the value of the causesOfDeath property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the causesOfDeath property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCausesOfDeath().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MedicalConditionTO }
     * 
     * 
     */
    public List<MedicalConditionTO> getCausesOfDeath() {
        if (causesOfDeath == null) {
            causesOfDeath = new ArrayList<MedicalConditionTO>();
        }
        return this.causesOfDeath;
    }

    /**
     * Sets the value of the causesOfDeath property.
     * 
     * @param causesOfDeath
     *     allowed object is
     *     {@link MedicalConditionTO }
     *     
     */
    public void setCausesOfDeath(List<MedicalConditionTO> causesOfDeath) {
        this.causesOfDeath = causesOfDeath;
    }

}
