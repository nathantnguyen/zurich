
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlacePositioning_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PlacePositioning_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Relationship_TO">
 *       &lt;sequence>
 *         &lt;element name="positioningDescription" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="distance" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="positionedPlace" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlacePositioning_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "positioningDescription",
    "distance",
    "positionedPlace"
})
public class PlacePositioningTO
    extends RelationshipTO
{

    protected String positioningDescription;
    protected Amount distance;
    protected PlaceTO positionedPlace;

    /**
     * Gets the value of the positioningDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPositioningDescription() {
        return positioningDescription;
    }

    /**
     * Sets the value of the positioningDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPositioningDescription(String value) {
        this.positioningDescription = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setDistance(Amount value) {
        this.distance = value;
    }

    /**
     * Gets the value of the positionedPlace property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getPositionedPlace() {
        return positionedPlace;
    }

    /**
     * Sets the value of the positionedPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setPositionedPlace(PlaceTO value) {
        this.positionedPlace = value;
    }

}
