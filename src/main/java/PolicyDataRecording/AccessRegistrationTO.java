
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AccessRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccessRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/}SecurityRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="priorityLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="passwordEncrypted" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Binary" minOccurs="0"/>
 *         &lt;element name="passwordEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="encryptionType" type="{http://www.zurich.com/zsoa/nac/schemas/UtilityComponents/Security/SecurityEnumerationsAndStates/}EncryptionType" minOccurs="0"/>
 *         &lt;element name="registeringAuthorityReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessRegistration_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/", propOrder = {
    "userId",
    "priorityLevel",
    "password",
    "passwordEncrypted",
    "passwordEndDate",
    "encryptionType",
    "registeringAuthorityReference"
})
public class AccessRegistrationTO
    extends SecurityRegistrationTO
{

    protected String userId;
    protected String priorityLevel;
    protected String password;
    protected Binary passwordEncrypted;
    protected XMLGregorianCalendar passwordEndDate;
    protected String encryptionType;
    protected ObjectReferenceTO registeringAuthorityReference;

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the priorityLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Sets the value of the priorityLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorityLevel(String value) {
        this.priorityLevel = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the passwordEncrypted property.
     * 
     * @return
     *     possible object is
     *     {@link Binary }
     *     
     */
    public Binary getPasswordEncrypted() {
        return passwordEncrypted;
    }

    /**
     * Sets the value of the passwordEncrypted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Binary }
     *     
     */
    public void setPasswordEncrypted(Binary value) {
        this.passwordEncrypted = value;
    }

    /**
     * Gets the value of the passwordEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPasswordEndDate() {
        return passwordEndDate;
    }

    /**
     * Sets the value of the passwordEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPasswordEndDate(XMLGregorianCalendar value) {
        this.passwordEndDate = value;
    }

    /**
     * Gets the value of the encryptionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncryptionType() {
        return encryptionType;
    }

    /**
     * Sets the value of the encryptionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncryptionType(String value) {
        this.encryptionType = value;
    }

    /**
     * Gets the value of the registeringAuthorityReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getRegisteringAuthorityReference() {
        return registeringAuthorityReference;
    }

    /**
     * Sets the value of the registeringAuthorityReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setRegisteringAuthorityReference(ObjectReferenceTO value) {
        this.registeringAuthorityReference = value;
    }

}
