
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BuildingWindClassification.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BuildingWindClassification">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Category 4 Hurricane"/>
 *     &lt;enumeration value="Wind Resistive"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BuildingWindClassification", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum BuildingWindClassification {

    @XmlEnumValue("Category 4 Hurricane")
    CATEGORY_4_HURRICANE("Category 4 Hurricane"),
    @XmlEnumValue("Wind Resistive")
    WIND_RESISTIVE("Wind Resistive");
    private final String value;

    BuildingWindClassification(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BuildingWindClassification fromValue(String v) {
        for (BuildingWindClassification c: BuildingWindClassification.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
