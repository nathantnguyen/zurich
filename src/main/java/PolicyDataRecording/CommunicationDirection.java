
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationDirection.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunicationDirection">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="External"/>
 *     &lt;enumeration value="Inbound"/>
 *     &lt;enumeration value="Internal"/>
 *     &lt;enumeration value="Outbound"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommunicationDirection", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum CommunicationDirection {

    @XmlEnumValue("External")
    EXTERNAL("External"),
    @XmlEnumValue("Inbound")
    INBOUND("Inbound"),
    @XmlEnumValue("Internal")
    INTERNAL("Internal"),
    @XmlEnumValue("Outbound")
    OUTBOUND("Outbound");
    private final String value;

    CommunicationDirection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationDirection fromValue(String v) {
        for (CommunicationDirection c: CommunicationDirection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
