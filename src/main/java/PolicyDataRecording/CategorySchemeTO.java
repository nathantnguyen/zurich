
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CategoryScheme_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CategoryScheme_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="overlapping" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="exhaustive" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="parentCategories" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subCategories" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategoryScheme_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "name",
    "description",
    "overlapping",
    "exhaustive",
    "parentCategories",
    "subCategories"
})
public class CategorySchemeTO
    extends BaseTransferObject
{

    protected String name;
    protected String description;
    protected Boolean overlapping;
    protected Boolean exhaustive;
    protected List<CategoryTO> parentCategories;
    protected List<CategoryTO> subCategories;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the overlapping property.
     * This getter has been renamed from isOverlapping() to getOverlapping() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOverlapping() {
        return overlapping;
    }

    /**
     * Sets the value of the overlapping property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverlapping(Boolean value) {
        this.overlapping = value;
    }

    /**
     * Gets the value of the exhaustive property.
     * This getter has been renamed from isExhaustive() to getExhaustive() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExhaustive() {
        return exhaustive;
    }

    /**
     * Sets the value of the exhaustive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExhaustive(Boolean value) {
        this.exhaustive = value;
    }

    /**
     * Gets the value of the parentCategories property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parentCategories property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParentCategories().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getParentCategories() {
        if (parentCategories == null) {
            parentCategories = new ArrayList<CategoryTO>();
        }
        return this.parentCategories;
    }

    /**
     * Gets the value of the subCategories property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subCategories property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubCategories().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getSubCategories() {
        if (subCategories == null) {
            subCategories = new ArrayList<CategoryTO>();
        }
        return this.subCategories;
    }

    /**
     * Sets the value of the parentCategories property.
     * 
     * @param parentCategories
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setParentCategories(List<CategoryTO> parentCategories) {
        this.parentCategories = parentCategories;
    }

    /**
     * Sets the value of the subCategories property.
     * 
     * @param subCategories
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setSubCategories(List<CategoryTO> subCategories) {
        this.subCategories = subCategories;
    }

}
