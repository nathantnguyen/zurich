
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeductiblePeriodicity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DeductiblePeriodicity">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Annual"/>
 *     &lt;enumeration value="Life Time"/>
 *     &lt;enumeration value="Per Claim"/>
 *     &lt;enumeration value="Per Damage"/>
 *     &lt;enumeration value="Per Loss Event"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DeductiblePeriodicity", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum DeductiblePeriodicity {

    @XmlEnumValue("Annual")
    ANNUAL("Annual"),
    @XmlEnumValue("Life Time")
    LIFE_TIME("Life Time"),
    @XmlEnumValue("Per Claim")
    PER_CLAIM("Per Claim"),
    @XmlEnumValue("Per Damage")
    PER_DAMAGE("Per Damage"),
    @XmlEnumValue("Per Loss Event")
    PER_LOSS_EVENT("Per Loss Event");
    private final String value;

    DeductiblePeriodicity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeductiblePeriodicity fromValue(String v) {
        for (DeductiblePeriodicity c: DeductiblePeriodicity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
