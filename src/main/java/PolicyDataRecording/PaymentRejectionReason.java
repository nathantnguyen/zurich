
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentRejectionReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentRejectionReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Bad Signature"/>
 *     &lt;enumeration value="No Provision"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentRejectionReason", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum PaymentRejectionReason {

    @XmlEnumValue("Bad Signature")
    BAD_SIGNATURE("Bad Signature"),
    @XmlEnumValue("No Provision")
    NO_PROVISION("No Provision");
    private final String value;

    PaymentRejectionReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentRejectionReason fromValue(String v) {
        for (PaymentRejectionReason c: PaymentRejectionReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
