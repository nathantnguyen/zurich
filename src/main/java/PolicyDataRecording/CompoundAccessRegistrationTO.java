
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompoundAccessRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompoundAccessRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/}SecurityRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="encryptionType" type="{http://www.zurich.com/zsoa/nac/schemas/UtilityComponents/Security/SecurityEnumerationsAndStates/}EncryptionType" minOccurs="0"/>
 *         &lt;element name="consistsOf" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/}SecurityKey_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompoundAccessRegistration_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/", propOrder = {
    "encryptionType",
    "consistsOf"
})
public class CompoundAccessRegistrationTO
    extends SecurityRegistrationTO
{

    protected String encryptionType;
    protected List<SecurityKeyTO> consistsOf;

    /**
     * Gets the value of the encryptionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncryptionType() {
        return encryptionType;
    }

    /**
     * Sets the value of the encryptionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncryptionType(String value) {
        this.encryptionType = value;
    }

    /**
     * Gets the value of the consistsOf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consistsOf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsistsOf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SecurityKeyTO }
     * 
     * 
     */
    public List<SecurityKeyTO> getConsistsOf() {
        if (consistsOf == null) {
            consistsOf = new ArrayList<SecurityKeyTO>();
        }
        return this.consistsOf;
    }

    /**
     * Sets the value of the consistsOf property.
     * 
     * @param consistsOf
     *     allowed object is
     *     {@link SecurityKeyTO }
     *     
     */
    public void setConsistsOf(List<SecurityKeyTO> consistsOf) {
        this.consistsOf = consistsOf;
    }

}
