
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BaseTransferObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseTransferObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseTransferObject", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/")
@XmlSeeAlso({
    SelectedanswerTO.class,
    TextspecificationcompositionTO.class,
    ContractRequestSpecificationInclusionTO.class,
    ContractspecificationcompositionTO.class,
    IndexableCurrencyAmountValueTO.class,
    PropertyTO.class,
    ObjectReferenceTO.class,
    CategorySchemeTO.class,
    IndexableCurrencyAmountTO.class,
    PlaceusageinactivityTO.class,
    AgreementSpecDefCriteriaTO.class,
    AvailableRequestTO.class,
    AgreementVersionTO.class,
    ExpressionResultTO.class,
    MultiplicityListTO.class,
    RuleResultTO.class,
    RoleInMoneyProvisionTypeTO.class,
    TypeTO.class,
    RelationshipTO.class,
    CategoryTO.class,
    CategorisableObjectTO.class,
    StatusTO.class
})
public class BaseTransferObject {


}
