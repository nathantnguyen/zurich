
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PropensityScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PropensityScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerScore_TO">
 *       &lt;sequence>
 *         &lt;element name="predictiveModel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}PredictiveModel" minOccurs="0"/>
 *         &lt;element name="propensityToBuy" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="propensityToCancel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="propensityToClaim" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="propensityToEnquire" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="propensityToLapse" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="propensityToRespond" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="propensityToUpgrade" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="instanceCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="propensityToCommitFraud" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="rating" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PropensityScore_TO", propOrder = {
    "predictiveModel",
    "propensityToBuy",
    "propensityToCancel",
    "propensityToClaim",
    "propensityToEnquire",
    "propensityToLapse",
    "propensityToRespond",
    "propensityToUpgrade",
    "instanceCount",
    "propensityToCommitFraud",
    "rating"
})
public class PropensityScoreTO
    extends RolePlayerScoreTO
{

    protected PredictiveModel predictiveModel;
    protected BigDecimal propensityToBuy;
    protected BigDecimal propensityToCancel;
    protected Frequency propensityToClaim;
    protected BigDecimal propensityToEnquire;
    protected BigDecimal propensityToLapse;
    protected BigDecimal propensityToRespond;
    protected Frequency propensityToUpgrade;
    protected BigInteger instanceCount;
    protected BigDecimal propensityToCommitFraud;
    protected String rating;

    /**
     * Gets the value of the predictiveModel property.
     * 
     * @return
     *     possible object is
     *     {@link PredictiveModel }
     *     
     */
    public PredictiveModel getPredictiveModel() {
        return predictiveModel;
    }

    /**
     * Sets the value of the predictiveModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link PredictiveModel }
     *     
     */
    public void setPredictiveModel(PredictiveModel value) {
        this.predictiveModel = value;
    }

    /**
     * Gets the value of the propensityToBuy property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPropensityToBuy() {
        return propensityToBuy;
    }

    /**
     * Sets the value of the propensityToBuy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPropensityToBuy(BigDecimal value) {
        this.propensityToBuy = value;
    }

    /**
     * Gets the value of the propensityToCancel property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPropensityToCancel() {
        return propensityToCancel;
    }

    /**
     * Sets the value of the propensityToCancel property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPropensityToCancel(BigDecimal value) {
        this.propensityToCancel = value;
    }

    /**
     * Gets the value of the propensityToClaim property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getPropensityToClaim() {
        return propensityToClaim;
    }

    /**
     * Sets the value of the propensityToClaim property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setPropensityToClaim(Frequency value) {
        this.propensityToClaim = value;
    }

    /**
     * Gets the value of the propensityToEnquire property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPropensityToEnquire() {
        return propensityToEnquire;
    }

    /**
     * Sets the value of the propensityToEnquire property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPropensityToEnquire(BigDecimal value) {
        this.propensityToEnquire = value;
    }

    /**
     * Gets the value of the propensityToLapse property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPropensityToLapse() {
        return propensityToLapse;
    }

    /**
     * Sets the value of the propensityToLapse property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPropensityToLapse(BigDecimal value) {
        this.propensityToLapse = value;
    }

    /**
     * Gets the value of the propensityToRespond property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPropensityToRespond() {
        return propensityToRespond;
    }

    /**
     * Sets the value of the propensityToRespond property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPropensityToRespond(BigDecimal value) {
        this.propensityToRespond = value;
    }

    /**
     * Gets the value of the propensityToUpgrade property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getPropensityToUpgrade() {
        return propensityToUpgrade;
    }

    /**
     * Sets the value of the propensityToUpgrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setPropensityToUpgrade(Frequency value) {
        this.propensityToUpgrade = value;
    }

    /**
     * Gets the value of the instanceCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInstanceCount() {
        return instanceCount;
    }

    /**
     * Sets the value of the instanceCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInstanceCount(BigInteger value) {
        this.instanceCount = value;
    }

    /**
     * Gets the value of the propensityToCommitFraud property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPropensityToCommitFraud() {
        return propensityToCommitFraud;
    }

    /**
     * Sets the value of the propensityToCommitFraud property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPropensityToCommitFraud(BigDecimal value) {
        this.propensityToCommitFraud = value;
    }

    /**
     * Gets the value of the rating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRating() {
        return rating;
    }

    /**
     * Sets the value of the rating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRating(String value) {
        this.rating = value;
    }

}
