
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SecurityKey_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SecurityKey_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/}SecurityRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="keyName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="keyValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="keyValueEncrypted" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="keyEncryptionType" type="{http://www.zurich.com/zsoa/nac/schemas/UtilityComponents/Security/SecurityEnumerationsAndStates/}EncryptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecurityKey_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Security/", propOrder = {
    "keyName",
    "keyValue",
    "keyValueEncrypted",
    "keyEncryptionType"
})
public class SecurityKeyTO
    extends SecurityRegistrationTO
{

    protected String keyName;
    protected String keyValue;
    protected String keyValueEncrypted;
    protected String keyEncryptionType;

    /**
     * Gets the value of the keyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyName() {
        return keyName;
    }

    /**
     * Sets the value of the keyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyName(String value) {
        this.keyName = value;
    }

    /**
     * Gets the value of the keyValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyValue() {
        return keyValue;
    }

    /**
     * Sets the value of the keyValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyValue(String value) {
        this.keyValue = value;
    }

    /**
     * Gets the value of the keyValueEncrypted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyValueEncrypted() {
        return keyValueEncrypted;
    }

    /**
     * Sets the value of the keyValueEncrypted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyValueEncrypted(String value) {
        this.keyValueEncrypted = value;
    }

    /**
     * Gets the value of the keyEncryptionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyEncryptionType() {
        return keyEncryptionType;
    }

    /**
     * Sets the value of the keyEncryptionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyEncryptionType(String value) {
        this.keyEncryptionType = value;
    }

}
