
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoveredVehicleSymbol.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CoveredVehicleSymbol">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Any Auto"/>
 *     &lt;enumeration value="Hired Autos Only"/>
 *     &lt;enumeration value="Nonowned Autos Only"/>
 *     &lt;enumeration value="Owned Autos Only"/>
 *     &lt;enumeration value="Owned Autos Other Than Private Passenger Autos Only"/>
 *     &lt;enumeration value="Owned Autos Subject To A Compulsory Uninsured Motorist Law"/>
 *     &lt;enumeration value="Owned Autos Subject To No-fault"/>
 *     &lt;enumeration value="Owned Private Passenger Autos Only"/>
 *     &lt;enumeration value="Specifically Described Autos"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CoveredVehicleSymbol", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum CoveredVehicleSymbol {

    @XmlEnumValue("Any Auto")
    ANY_AUTO("Any Auto"),
    @XmlEnumValue("Hired Autos Only")
    HIRED_AUTOS_ONLY("Hired Autos Only"),
    @XmlEnumValue("Nonowned Autos Only")
    NONOWNED_AUTOS_ONLY("Nonowned Autos Only"),
    @XmlEnumValue("Owned Autos Only")
    OWNED_AUTOS_ONLY("Owned Autos Only"),
    @XmlEnumValue("Owned Autos Other Than Private Passenger Autos Only")
    OWNED_AUTOS_OTHER_THAN_PRIVATE_PASSENGER_AUTOS_ONLY("Owned Autos Other Than Private Passenger Autos Only"),
    @XmlEnumValue("Owned Autos Subject To A Compulsory Uninsured Motorist Law")
    OWNED_AUTOS_SUBJECT_TO_A_COMPULSORY_UNINSURED_MOTORIST_LAW("Owned Autos Subject To A Compulsory Uninsured Motorist Law"),
    @XmlEnumValue("Owned Autos Subject To No-fault")
    OWNED_AUTOS_SUBJECT_TO_NO_FAULT("Owned Autos Subject To No-fault"),
    @XmlEnumValue("Owned Private Passenger Autos Only")
    OWNED_PRIVATE_PASSENGER_AUTOS_ONLY("Owned Private Passenger Autos Only"),
    @XmlEnumValue("Specifically Described Autos")
    SPECIFICALLY_DESCRIBED_AUTOS("Specifically Described Autos");
    private final String value;

    CoveredVehicleSymbol(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CoveredVehicleSymbol fromValue(String v) {
        for (CoveredVehicleSymbol c: CoveredVehicleSymbol.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
