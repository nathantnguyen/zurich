
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RolePlayerScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RolePlayerScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="assessedRolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="partyRelationship" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}InfluenceRating" minOccurs="0"/>
 *         &lt;element name="veracity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RolePlayerScore_TO", propOrder = {
    "assessedRolePlayer",
    "partyRelationship",
    "veracity"
})
@XmlSeeAlso({
    CreditRatingTO.class,
    ProfitabilityScoreTO.class,
    LoyaltyScoreTO.class,
    FinancialStressScoreTO.class,
    PropensityScoreTO.class,
    AttitudeTO.class,
    PartyScoreTO.class,
    RelationshipStageTO.class,
    SatisfactionScoreTO.class
})
public class RolePlayerScoreTO
    extends ScoreTO
{

    protected RolePlayerTO assessedRolePlayer;
    protected InfluenceRating partyRelationship;
    protected BigDecimal veracity;

    /**
     * Gets the value of the assessedRolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getAssessedRolePlayer() {
        return assessedRolePlayer;
    }

    /**
     * Sets the value of the assessedRolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setAssessedRolePlayer(RolePlayerTO value) {
        this.assessedRolePlayer = value;
    }

    /**
     * Gets the value of the partyRelationship property.
     * 
     * @return
     *     possible object is
     *     {@link InfluenceRating }
     *     
     */
    public InfluenceRating getPartyRelationship() {
        return partyRelationship;
    }

    /**
     * Sets the value of the partyRelationship property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfluenceRating }
     *     
     */
    public void setPartyRelationship(InfluenceRating value) {
        this.partyRelationship = value;
    }

    /**
     * Gets the value of the veracity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVeracity() {
        return veracity;
    }

    /**
     * Sets the value of the veracity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVeracity(BigDecimal value) {
        this.veracity = value;
    }

}
