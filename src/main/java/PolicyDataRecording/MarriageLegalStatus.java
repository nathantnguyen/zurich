
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarriageLegalStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MarriageLegalStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Common Law Marriage"/>
 *     &lt;enumeration value="Legal Marriage"/>
 *     &lt;enumeration value="Registered Domestic Partnership"/>
 *     &lt;enumeration value="Unregistered Cohabitation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MarriageLegalStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum MarriageLegalStatus {

    @XmlEnumValue("Common Law Marriage")
    COMMON_LAW_MARRIAGE("Common Law Marriage"),
    @XmlEnumValue("Legal Marriage")
    LEGAL_MARRIAGE("Legal Marriage"),
    @XmlEnumValue("Registered Domestic Partnership")
    REGISTERED_DOMESTIC_PARTNERSHIP("Registered Domestic Partnership"),
    @XmlEnumValue("Unregistered Cohabitation")
    UNREGISTERED_COHABITATION("Unregistered Cohabitation");
    private final String value;

    MarriageLegalStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MarriageLegalStatus fromValue(String v) {
        for (MarriageLegalStatus c: MarriageLegalStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
