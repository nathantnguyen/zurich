
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Limit_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Limit_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}MoneyProvisionDeterminer_TO">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexableCurrencyAmount_TO" minOccurs="0"/>
 *         &lt;element name="anniversaryDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="periodicity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/}LimitPeriodicity" minOccurs="0"/>
 *         &lt;element name="maximum" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Limit_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "amount",
    "anniversaryDate",
    "periodicity",
    "maximum"
})
@XmlSeeAlso({
    RiskToleranceLimitTO.class
})
public class LimitTO
    extends MoneyProvisionDeterminerTO
{

    protected IndexableCurrencyAmountTO amount;
    protected XMLGregorianCalendar anniversaryDate;
    protected LimitPeriodicity periodicity;
    protected Boolean maximum;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public IndexableCurrencyAmountTO getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexableCurrencyAmountTO }
     *     
     */
    public void setAmount(IndexableCurrencyAmountTO value) {
        this.amount = value;
    }

    /**
     * Gets the value of the anniversaryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAnniversaryDate() {
        return anniversaryDate;
    }

    /**
     * Sets the value of the anniversaryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAnniversaryDate(XMLGregorianCalendar value) {
        this.anniversaryDate = value;
    }

    /**
     * Gets the value of the periodicity property.
     * 
     * @return
     *     possible object is
     *     {@link LimitPeriodicity }
     *     
     */
    public LimitPeriodicity getPeriodicity() {
        return periodicity;
    }

    /**
     * Sets the value of the periodicity property.
     * 
     * @param value
     *     allowed object is
     *     {@link LimitPeriodicity }
     *     
     */
    public void setPeriodicity(LimitPeriodicity value) {
        this.periodicity = value;
    }

    /**
     * Gets the value of the maximum property.
     * This getter has been renamed from isMaximum() to getMaximum() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMaximum() {
        return maximum;
    }

    /**
     * Sets the value of the maximum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaximum(Boolean value) {
        this.maximum = value;
    }

}
