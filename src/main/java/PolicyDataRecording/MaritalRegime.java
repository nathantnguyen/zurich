
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaritalRegime.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MaritalRegime">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Separation Of Goods"/>
 *     &lt;enumeration value="Common Goods"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MaritalRegime", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum MaritalRegime {

    @XmlEnumValue("Separation Of Goods")
    SEPARATION_OF_GOODS("Separation Of Goods"),
    @XmlEnumValue("Common Goods")
    COMMON_GOODS("Common Goods");
    private final String value;

    MaritalRegime(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MaritalRegime fromValue(String v) {
        for (MaritalRegime c: MaritalRegime.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
