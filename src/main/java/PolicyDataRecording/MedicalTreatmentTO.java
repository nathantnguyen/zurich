
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MedicalTreatment_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MedicalTreatment_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ObjectTreatment_TO">
 *       &lt;sequence>
 *         &lt;element name="intensity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}MedicalTreatmentIntensity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedicalTreatment_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "intensity"
})
public class MedicalTreatmentTO
    extends ObjectTreatmentTO
{

    protected MedicalTreatmentIntensity intensity;

    /**
     * Gets the value of the intensity property.
     * 
     * @return
     *     possible object is
     *     {@link MedicalTreatmentIntensity }
     *     
     */
    public MedicalTreatmentIntensity getIntensity() {
        return intensity;
    }

    /**
     * Sets the value of the intensity property.
     * 
     * @param value
     *     allowed object is
     *     {@link MedicalTreatmentIntensity }
     *     
     */
    public void setIntensity(MedicalTreatmentIntensity value) {
        this.intensity = value;
    }

}
