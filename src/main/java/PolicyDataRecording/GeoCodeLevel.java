
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeoCodeLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GeoCodeLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="StreetAddress"/>
 *     &lt;enumeration value="PostalCentroid"/>
 *     &lt;enumeration value="GeographicCentroid"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GeoCodeLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum GeoCodeLevel {

    @XmlEnumValue("StreetAddress")
    STREET_ADDRESS("StreetAddress"),
    @XmlEnumValue("PostalCentroid")
    POSTAL_CENTROID("PostalCentroid"),
    @XmlEnumValue("GeographicCentroid")
    GEOGRAPHIC_CENTROID("GeographicCentroid");
    private final String value;

    GeoCodeLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GeoCodeLevel fromValue(String v) {
        for (GeoCodeLevel c: GeoCodeLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
