
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInFinancialServicesRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInFinancialServicesRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContractRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="producerAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}IntermediaryAgreement_TO" minOccurs="0"/>
 *         &lt;element name="roleInFinancialServicesRequestRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}RoleInFinancialServicesRequestType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInFinancialServicesRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "producerAgreement",
    "roleInFinancialServicesRequestRootType"
})
public class RoleInFinancialServicesRequestTO
    extends RoleInContractRequestTO
{

    protected IntermediaryAgreementTO producerAgreement;
    protected RoleInFinancialServicesRequestTypeTO roleInFinancialServicesRequestRootType;

    /**
     * Gets the value of the producerAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public IntermediaryAgreementTO getProducerAgreement() {
        return producerAgreement;
    }

    /**
     * Sets the value of the producerAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public void setProducerAgreement(IntermediaryAgreementTO value) {
        this.producerAgreement = value;
    }

    /**
     * Gets the value of the roleInFinancialServicesRequestRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInFinancialServicesRequestTypeTO }
     *     
     */
    public RoleInFinancialServicesRequestTypeTO getRoleInFinancialServicesRequestRootType() {
        return roleInFinancialServicesRequestRootType;
    }

    /**
     * Sets the value of the roleInFinancialServicesRequestRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInFinancialServicesRequestTypeTO }
     *     
     */
    public void setRoleInFinancialServicesRequestRootType(RoleInFinancialServicesRequestTypeTO value) {
        this.roleInFinancialServicesRequestRootType = value;
    }

}
