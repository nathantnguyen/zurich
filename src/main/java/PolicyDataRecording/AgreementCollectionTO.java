
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgreementCollection_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgreementCollection_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO">
 *       &lt;sequence>
 *         &lt;element name="allocatedReserves" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}ReserveAccount_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgreementCollection_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "allocatedReserves"
})
@XmlSeeAlso({
    EmploymentCategoryTO.class,
    LargeRiskAgreementsTO.class
})
public class AgreementCollectionTO
    extends CategoryTO
{

    protected List<ReserveAccountTO> allocatedReserves;

    /**
     * Gets the value of the allocatedReserves property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allocatedReserves property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocatedReserves().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReserveAccountTO }
     * 
     * 
     */
    public List<ReserveAccountTO> getAllocatedReserves() {
        if (allocatedReserves == null) {
            allocatedReserves = new ArrayList<ReserveAccountTO>();
        }
        return this.allocatedReserves;
    }

    /**
     * Sets the value of the allocatedReserves property.
     * 
     * @param allocatedReserves
     *     allowed object is
     *     {@link ReserveAccountTO }
     *     
     */
    public void setAllocatedReserves(List<ReserveAccountTO> allocatedReserves) {
        this.allocatedReserves = allocatedReserves;
    }

}
