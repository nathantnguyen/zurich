
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReasonForActivityEnding.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReasonForActivityEnding">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Change Of Employment"/>
 *     &lt;enumeration value="Injury"/>
 *     &lt;enumeration value="Loss Of Interest"/>
 *     &lt;enumeration value="Retired"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReasonForActivityEnding", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum ReasonForActivityEnding {

    @XmlEnumValue("Change Of Employment")
    CHANGE_OF_EMPLOYMENT("Change Of Employment"),
    @XmlEnumValue("Injury")
    INJURY("Injury"),
    @XmlEnumValue("Loss Of Interest")
    LOSS_OF_INTEREST("Loss Of Interest"),
    @XmlEnumValue("Retired")
    RETIRED("Retired");
    private final String value;

    ReasonForActivityEnding(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReasonForActivityEnding fromValue(String v) {
        for (ReasonForActivityEnding c: ReasonForActivityEnding.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
