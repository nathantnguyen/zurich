
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditRating_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditRating_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerScore_TO">
 *       &lt;sequence>
 *         &lt;element name="rating" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}SatisfactionRating" minOccurs="0"/>
 *         &lt;element name="creditHoldingsIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="litigationIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="paymentIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="riskWeight" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="freeTextPointsScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditRating_TO", propOrder = {
    "rating",
    "creditHoldingsIndicator",
    "litigationIndicator",
    "paymentIndicator",
    "riskWeight",
    "freeTextPointsScore"
})
public class CreditRatingTO
    extends RolePlayerScoreTO
{

    protected String rating;
    protected Boolean creditHoldingsIndicator;
    protected Boolean litigationIndicator;
    protected Boolean paymentIndicator;
    protected BigDecimal riskWeight;
    protected String freeTextPointsScore;

    /**
     * Gets the value of the rating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRating() {
        return rating;
    }

    /**
     * Sets the value of the rating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRating(String value) {
        this.rating = value;
    }

    /**
     * Gets the value of the creditHoldingsIndicator property.
     * This getter has been renamed from isCreditHoldingsIndicator() to getCreditHoldingsIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCreditHoldingsIndicator() {
        return creditHoldingsIndicator;
    }

    /**
     * Sets the value of the creditHoldingsIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreditHoldingsIndicator(Boolean value) {
        this.creditHoldingsIndicator = value;
    }

    /**
     * Gets the value of the litigationIndicator property.
     * This getter has been renamed from isLitigationIndicator() to getLitigationIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLitigationIndicator() {
        return litigationIndicator;
    }

    /**
     * Sets the value of the litigationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLitigationIndicator(Boolean value) {
        this.litigationIndicator = value;
    }

    /**
     * Gets the value of the paymentIndicator property.
     * This getter has been renamed from isPaymentIndicator() to getPaymentIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPaymentIndicator() {
        return paymentIndicator;
    }

    /**
     * Sets the value of the paymentIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPaymentIndicator(Boolean value) {
        this.paymentIndicator = value;
    }

    /**
     * Gets the value of the riskWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRiskWeight() {
        return riskWeight;
    }

    /**
     * Sets the value of the riskWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRiskWeight(BigDecimal value) {
        this.riskWeight = value;
    }

    /**
     * Gets the value of the freeTextPointsScore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeTextPointsScore() {
        return freeTextPointsScore;
    }

    /**
     * Sets the value of the freeTextPointsScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeTextPointsScore(String value) {
        this.freeTextPointsScore = value;
    }

}
