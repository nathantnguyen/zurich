
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInCorporateAgreementType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInCorporateAgreementType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleType_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}RoleInCorporateAgreementTypeName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInCorporateAgreementType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "name"
})
public class RoleInCorporateAgreementTypeTO
    extends RoleTypeTO
{

    protected RoleInCorporateAgreementTypeName name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInCorporateAgreementTypeName }
     *     
     */
    public RoleInCorporateAgreementTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInCorporateAgreementTypeName }
     *     
     */
    public void setName(RoleInCorporateAgreementTypeName value) {
        this.name = value;
    }

}
