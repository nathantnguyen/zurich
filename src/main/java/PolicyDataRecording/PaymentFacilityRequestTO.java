
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentFacilityRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentFacilityRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialMovementRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="requestedFinancialTransactions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}FinancialTransaction_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentFacilityRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "requestedFinancialTransactions"
})
@XmlSeeAlso({
    CreditCardTransactionRequestTO.class,
    SingleTransferRequestTO.class
})
public class PaymentFacilityRequestTO
    extends FinancialMovementRequestTO
{

    protected List<FinancialTransactionTO> requestedFinancialTransactions;

    /**
     * Gets the value of the requestedFinancialTransactions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestedFinancialTransactions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestedFinancialTransactions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialTransactionTO }
     * 
     * 
     */
    public List<FinancialTransactionTO> getRequestedFinancialTransactions() {
        if (requestedFinancialTransactions == null) {
            requestedFinancialTransactions = new ArrayList<FinancialTransactionTO>();
        }
        return this.requestedFinancialTransactions;
    }

    /**
     * Sets the value of the requestedFinancialTransactions property.
     * 
     * @param requestedFinancialTransactions
     *     allowed object is
     *     {@link FinancialTransactionTO }
     *     
     */
    public void setRequestedFinancialTransactions(List<FinancialTransactionTO> requestedFinancialTransactions) {
        this.requestedFinancialTransactions = requestedFinancialTransactions;
    }

}
