
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CalculationSpecificationKind.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CalculationSpecificationKind">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Adjustment Calculation"/>
 *     &lt;enumeration value="Amendment Premium Calculation"/>
 *     &lt;enumeration value="Benefit Calculation"/>
 *     &lt;enumeration value="Commission Calculation"/>
 *     &lt;enumeration value="Interest Calculation"/>
 *     &lt;enumeration value="Loan Balance Calculation"/>
 *     &lt;enumeration value="Premium Calculation"/>
 *     &lt;enumeration value="Premium Payment Calculation"/>
 *     &lt;enumeration value="Price Calculation"/>
 *     &lt;enumeration value="Surrender Calculation"/>
 *     &lt;enumeration value="Initialisation Calculation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CalculationSpecificationKind", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/")
@XmlEnum
public enum CalculationSpecificationKind {

    @XmlEnumValue("Adjustment Calculation")
    ADJUSTMENT_CALCULATION("Adjustment Calculation"),
    @XmlEnumValue("Amendment Premium Calculation")
    AMENDMENT_PREMIUM_CALCULATION("Amendment Premium Calculation"),
    @XmlEnumValue("Benefit Calculation")
    BENEFIT_CALCULATION("Benefit Calculation"),
    @XmlEnumValue("Commission Calculation")
    COMMISSION_CALCULATION("Commission Calculation"),
    @XmlEnumValue("Interest Calculation")
    INTEREST_CALCULATION("Interest Calculation"),
    @XmlEnumValue("Loan Balance Calculation")
    LOAN_BALANCE_CALCULATION("Loan Balance Calculation"),
    @XmlEnumValue("Premium Calculation")
    PREMIUM_CALCULATION("Premium Calculation"),
    @XmlEnumValue("Premium Payment Calculation")
    PREMIUM_PAYMENT_CALCULATION("Premium Payment Calculation"),
    @XmlEnumValue("Price Calculation")
    PRICE_CALCULATION("Price Calculation"),
    @XmlEnumValue("Surrender Calculation")
    SURRENDER_CALCULATION("Surrender Calculation"),
    @XmlEnumValue("Initialisation Calculation")
    INITIALISATION_CALCULATION("Initialisation Calculation");
    private final String value;

    CalculationSpecificationKind(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CalculationSpecificationKind fromValue(String v) {
        for (CalculationSpecificationKind c: CalculationSpecificationKind.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
