
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServicingChannelTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ServicingChannelTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Premium Collector"/>
 *     &lt;enumeration value="Paying Agent"/>
 *     &lt;enumeration value="Correspondent"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ServicingChannelTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum ServicingChannelTypeName {

    @XmlEnumValue("Premium Collector")
    PREMIUM_COLLECTOR("Premium Collector"),
    @XmlEnumValue("Paying Agent")
    PAYING_AGENT("Paying Agent"),
    @XmlEnumValue("Correspondent")
    CORRESPONDENT("Correspondent");
    private final String value;

    ServicingChannelTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServicingChannelTypeName fromValue(String v) {
        for (ServicingChannelTypeName c: ServicingChannelTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
