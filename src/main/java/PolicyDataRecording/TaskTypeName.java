
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaskTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TaskTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Litigation Activity"/>
 *     &lt;enumeration value="Business Exception Handling Activity"/>
 *     &lt;enumeration value="Regulatory Non Compliance Resolution Action"/>
 *     &lt;enumeration value="Reporting Activity"/>
 *     &lt;enumeration value="Enterprise Risk Disclosure Activity"/>
 *     &lt;enumeration value="Risk Management Activity"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TaskTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/UtilityComponents/TaskManager/TaskManagerEnumerationsAndStates/")
@XmlEnum
public enum TaskTypeName {

    @XmlEnumValue("Litigation Activity")
    LITIGATION_ACTIVITY("Litigation Activity"),
    @XmlEnumValue("Business Exception Handling Activity")
    BUSINESS_EXCEPTION_HANDLING_ACTIVITY("Business Exception Handling Activity"),
    @XmlEnumValue("Regulatory Non Compliance Resolution Action")
    REGULATORY_NON_COMPLIANCE_RESOLUTION_ACTION("Regulatory Non Compliance Resolution Action"),
    @XmlEnumValue("Reporting Activity")
    REPORTING_ACTIVITY("Reporting Activity"),
    @XmlEnumValue("Enterprise Risk Disclosure Activity")
    ENTERPRISE_RISK_DISCLOSURE_ACTIVITY("Enterprise Risk Disclosure Activity"),
    @XmlEnumValue("Risk Management Activity")
    RISK_MANAGEMENT_ACTIVITY("Risk Management Activity");
    private final String value;

    TaskTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TaskTypeName fromValue(String v) {
        for (TaskTypeName c: TaskTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
