
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ElementaryExpressionSpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ElementaryExpressionSpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ExpressionSpec_TO">
 *       &lt;sequence>
 *         &lt;element name="isFormal" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="businessDescription" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="formalDefinition" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Algorithm_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementaryExpressionSpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "isFormal",
    "businessDescription",
    "formalDefinition"
})
public class ElementaryExpressionSpecTO
    extends ExpressionSpecTO
{

    protected Boolean isFormal;
    protected String businessDescription;
    protected AlgorithmTO formalDefinition;

    /**
     * Gets the value of the isFormal property.
     * This getter has been renamed from isIsFormal() to getIsFormal() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsFormal() {
        return isFormal;
    }

    /**
     * Sets the value of the isFormal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsFormal(Boolean value) {
        this.isFormal = value;
    }

    /**
     * Gets the value of the businessDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessDescription() {
        return businessDescription;
    }

    /**
     * Sets the value of the businessDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessDescription(String value) {
        this.businessDescription = value;
    }

    /**
     * Gets the value of the formalDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link AlgorithmTO }
     *     
     */
    public AlgorithmTO getFormalDefinition() {
        return formalDefinition;
    }

    /**
     * Sets the value of the formalDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link AlgorithmTO }
     *     
     */
    public void setFormalDefinition(AlgorithmTO value) {
        this.formalDefinition = value;
    }

}
