
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentTemplate_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentTemplate_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CompositeTextBlock_TO">
 *       &lt;sequence>
 *         &lt;element name="contextWhereUsed" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/}ContextWhereUsed" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentTemplate_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "contextWhereUsed"
})
@XmlSeeAlso({
    QuestionnaireTO.class
})
public class DocumentTemplateTO
    extends CompositeTextBlockTO
{

    protected String contextWhereUsed;

    /**
     * Gets the value of the contextWhereUsed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContextWhereUsed() {
        return contextWhereUsed;
    }

    /**
     * Sets the value of the contextWhereUsed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContextWhereUsed(String value) {
        this.contextWhereUsed = value;
    }

}
