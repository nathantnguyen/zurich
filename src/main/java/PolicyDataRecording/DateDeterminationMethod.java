
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DateDeterminationMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DateDeterminationMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Expert Estimation"/>
 *     &lt;enumeration value="Role Player Estimation"/>
 *     &lt;enumeration value="Role Player Specified"/>
 *     &lt;enumeration value="Asap"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DateDeterminationMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum DateDeterminationMethod {

    @XmlEnumValue("Expert Estimation")
    EXPERT_ESTIMATION("Expert Estimation"),
    @XmlEnumValue("Role Player Estimation")
    ROLE_PLAYER_ESTIMATION("Role Player Estimation"),
    @XmlEnumValue("Role Player Specified")
    ROLE_PLAYER_SPECIFIED("Role Player Specified"),
    @XmlEnumValue("Asap")
    ASAP("Asap");
    private final String value;

    DateDeterminationMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DateDeterminationMethod fromValue(String v) {
        for (DateDeterminationMethod c: DateDeterminationMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
