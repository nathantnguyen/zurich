
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmploymentAgreementState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmploymentAgreementState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="In Force"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Suspended"/>
 *     &lt;enumeration value="Terminated"/>
 *     &lt;enumeration value="Under Negotiation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EmploymentAgreementState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum EmploymentAgreementState {

    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("In Force")
    IN_FORCE("In Force"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Suspended")
    SUSPENDED("Suspended"),
    @XmlEnumValue("Terminated")
    TERMINATED("Terminated"),
    @XmlEnumValue("Under Negotiation")
    UNDER_NEGOTIATION("Under Negotiation");
    private final String value;

    EmploymentAgreementState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmploymentAgreementState fromValue(String v) {
        for (EmploymentAgreementState c: EmploymentAgreementState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
