
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialServicesRequestTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FinancialServicesRequestTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Contractual Content Change Request"/>
 *     &lt;enumeration value="Financial Services Agreement Information Request"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FinancialServicesRequestTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum FinancialServicesRequestTypeName {

    @XmlEnumValue("Contractual Content Change Request")
    CONTRACTUAL_CONTENT_CHANGE_REQUEST("Contractual Content Change Request"),
    @XmlEnumValue("Financial Services Agreement Information Request")
    FINANCIAL_SERVICES_AGREEMENT_INFORMATION_REQUEST("Financial Services Agreement Information Request");
    private final String value;

    FinancialServicesRequestTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FinancialServicesRequestTypeName fromValue(String v) {
        for (FinancialServicesRequestTypeName c: FinancialServicesRequestTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
