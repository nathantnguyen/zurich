
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricingTier.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PricingTier">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Preferred"/>
 *     &lt;enumeration value="Standard"/>
 *     &lt;enumeration value="Super Preferred"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PricingTier", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum PricingTier {

    @XmlEnumValue("Preferred")
    PREFERRED("Preferred"),
    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    @XmlEnumValue("Super Preferred")
    SUPER_PREFERRED("Super Preferred");
    private final String value;

    PricingTier(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PricingTier fromValue(String v) {
        for (PricingTier c: PricingTier.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
