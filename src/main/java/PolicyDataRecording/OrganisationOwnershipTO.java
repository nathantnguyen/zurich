
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OrganisationOwnership_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrganisationOwnership_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerRelationship_TO">
 *       &lt;sequence>
 *         &lt;element name="acquisitionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="acquisitionMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}AcquisitionMethod" minOccurs="0"/>
 *         &lt;element name="ownershipForm" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}OrganisationOwnershipForm" minOccurs="0"/>
 *         &lt;element name="ownershipPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="ownedOrganisation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO" minOccurs="0"/>
 *         &lt;element name="participationCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="votingRightsPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="yearsOwned" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationOwnership_TO", propOrder = {
    "acquisitionDate",
    "acquisitionMethod",
    "ownershipForm",
    "ownershipPercentage",
    "ownedOrganisation",
    "participationCode",
    "votingRightsPercentage",
    "yearsOwned"
})
public class OrganisationOwnershipTO
    extends RolePlayerRelationshipTO
{

    protected XMLGregorianCalendar acquisitionDate;
    protected AcquisitionMethod acquisitionMethod;
    protected OrganisationOwnershipForm ownershipForm;
    protected BigDecimal ownershipPercentage;
    protected OrganisationTO ownedOrganisation;
    protected String participationCode;
    protected BigDecimal votingRightsPercentage;
    protected BigInteger yearsOwned;

    /**
     * Gets the value of the acquisitionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAcquisitionDate() {
        return acquisitionDate;
    }

    /**
     * Sets the value of the acquisitionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAcquisitionDate(XMLGregorianCalendar value) {
        this.acquisitionDate = value;
    }

    /**
     * Gets the value of the acquisitionMethod property.
     * 
     * @return
     *     possible object is
     *     {@link AcquisitionMethod }
     *     
     */
    public AcquisitionMethod getAcquisitionMethod() {
        return acquisitionMethod;
    }

    /**
     * Sets the value of the acquisitionMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcquisitionMethod }
     *     
     */
    public void setAcquisitionMethod(AcquisitionMethod value) {
        this.acquisitionMethod = value;
    }

    /**
     * Gets the value of the ownershipForm property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationOwnershipForm }
     *     
     */
    public OrganisationOwnershipForm getOwnershipForm() {
        return ownershipForm;
    }

    /**
     * Sets the value of the ownershipForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationOwnershipForm }
     *     
     */
    public void setOwnershipForm(OrganisationOwnershipForm value) {
        this.ownershipForm = value;
    }

    /**
     * Gets the value of the ownershipPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOwnershipPercentage() {
        return ownershipPercentage;
    }

    /**
     * Sets the value of the ownershipPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOwnershipPercentage(BigDecimal value) {
        this.ownershipPercentage = value;
    }

    /**
     * Gets the value of the ownedOrganisation property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationTO }
     *     
     */
    public OrganisationTO getOwnedOrganisation() {
        return ownedOrganisation;
    }

    /**
     * Sets the value of the ownedOrganisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationTO }
     *     
     */
    public void setOwnedOrganisation(OrganisationTO value) {
        this.ownedOrganisation = value;
    }

    /**
     * Gets the value of the participationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipationCode() {
        return participationCode;
    }

    /**
     * Sets the value of the participationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipationCode(String value) {
        this.participationCode = value;
    }

    /**
     * Gets the value of the votingRightsPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVotingRightsPercentage() {
        return votingRightsPercentage;
    }

    /**
     * Sets the value of the votingRightsPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVotingRightsPercentage(BigDecimal value) {
        this.votingRightsPercentage = value;
    }

    /**
     * Gets the value of the yearsOwned property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getYearsOwned() {
        return yearsOwned;
    }

    /**
     * Sets the value of the yearsOwned property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setYearsOwned(BigInteger value) {
        this.yearsOwned = value;
    }

}
