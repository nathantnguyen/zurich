
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MedicalConditionType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MedicalConditionType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}MedicalConditionTypeName" minOccurs="0"/>
 *         &lt;element name="formalName" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}FormalMedicalConditionName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedicalConditionType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "name",
    "formalName"
})
public class MedicalConditionTypeTO
    extends TypeTO
{

    protected MedicalConditionTypeName name;
    protected FormalMedicalConditionName formalName;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link MedicalConditionTypeName }
     *     
     */
    public MedicalConditionTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link MedicalConditionTypeName }
     *     
     */
    public void setName(MedicalConditionTypeName value) {
        this.name = value;
    }

    /**
     * Gets the value of the formalName property.
     * 
     * @return
     *     possible object is
     *     {@link FormalMedicalConditionName }
     *     
     */
    public FormalMedicalConditionName getFormalName() {
        return formalName;
    }

    /**
     * Sets the value of the formalName property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormalMedicalConditionName }
     *     
     */
    public void setFormalName(FormalMedicalConditionName value) {
        this.formalName = value;
    }

}
