
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyProvisionRoleInContractTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MoneyProvisionRoleInContractTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Applicable Limits"/>
 *     &lt;enumeration value="Cash Provision"/>
 *     &lt;enumeration value="Coverage Benefit"/>
 *     &lt;enumeration value="Coverage Premium"/>
 *     &lt;enumeration value="Employment Salary"/>
 *     &lt;enumeration value="Intermediary Commission"/>
 *     &lt;enumeration value="Money Lent"/>
 *     &lt;enumeration value="Money Repayment"/>
 *     &lt;enumeration value="Offer Price"/>
 *     &lt;enumeration value="Profit Sharing Bonus"/>
 *     &lt;enumeration value="Provider Agreement Pricing"/>
 *     &lt;enumeration value="Risk Agreement Cost"/>
 *     &lt;enumeration value="Savings Installment"/>
 *     &lt;enumeration value="Savings Repayment"/>
 *     &lt;enumeration value="Service Component Price"/>
 *     &lt;enumeration value="Interest Earning"/>
 *     &lt;enumeration value="Loan Interest"/>
 *     &lt;enumeration value="Agreement Charge"/>
 *     &lt;enumeration value="Capital Requirement"/>
 *     &lt;enumeration value="Risk Capital Allocation"/>
 *     &lt;enumeration value="Applicable Rates"/>
 *     &lt;enumeration value="Inception Price"/>
 *     &lt;enumeration value="Maturity Price"/>
 *     &lt;enumeration value="Insurance Policy Premium"/>
 *     &lt;enumeration value="Structural Component Premium"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MoneyProvisionRoleInContractTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum MoneyProvisionRoleInContractTypeName {

    @XmlEnumValue("Applicable Limits")
    APPLICABLE_LIMITS("Applicable Limits"),
    @XmlEnumValue("Cash Provision")
    CASH_PROVISION("Cash Provision"),
    @XmlEnumValue("Coverage Benefit")
    COVERAGE_BENEFIT("Coverage Benefit"),
    @XmlEnumValue("Coverage Premium")
    COVERAGE_PREMIUM("Coverage Premium"),
    @XmlEnumValue("Employment Salary")
    EMPLOYMENT_SALARY("Employment Salary"),
    @XmlEnumValue("Intermediary Commission")
    INTERMEDIARY_COMMISSION("Intermediary Commission"),
    @XmlEnumValue("Money Lent")
    MONEY_LENT("Money Lent"),
    @XmlEnumValue("Money Repayment")
    MONEY_REPAYMENT("Money Repayment"),
    @XmlEnumValue("Offer Price")
    OFFER_PRICE("Offer Price"),
    @XmlEnumValue("Profit Sharing Bonus")
    PROFIT_SHARING_BONUS("Profit Sharing Bonus"),
    @XmlEnumValue("Provider Agreement Pricing")
    PROVIDER_AGREEMENT_PRICING("Provider Agreement Pricing"),
    @XmlEnumValue("Risk Agreement Cost")
    RISK_AGREEMENT_COST("Risk Agreement Cost"),
    @XmlEnumValue("Savings Installment")
    SAVINGS_INSTALLMENT("Savings Installment"),
    @XmlEnumValue("Savings Repayment")
    SAVINGS_REPAYMENT("Savings Repayment"),
    @XmlEnumValue("Service Component Price")
    SERVICE_COMPONENT_PRICE("Service Component Price"),
    @XmlEnumValue("Interest Earning")
    INTEREST_EARNING("Interest Earning"),
    @XmlEnumValue("Loan Interest")
    LOAN_INTEREST("Loan Interest"),
    @XmlEnumValue("Agreement Charge")
    AGREEMENT_CHARGE("Agreement Charge"),
    @XmlEnumValue("Capital Requirement")
    CAPITAL_REQUIREMENT("Capital Requirement"),
    @XmlEnumValue("Risk Capital Allocation")
    RISK_CAPITAL_ALLOCATION("Risk Capital Allocation"),
    @XmlEnumValue("Applicable Rates")
    APPLICABLE_RATES("Applicable Rates"),
    @XmlEnumValue("Inception Price")
    INCEPTION_PRICE("Inception Price"),
    @XmlEnumValue("Maturity Price")
    MATURITY_PRICE("Maturity Price"),
    @XmlEnumValue("Insurance Policy Premium")
    INSURANCE_POLICY_PREMIUM("Insurance Policy Premium"),
    @XmlEnumValue("Structural Component Premium")
    STRUCTURAL_COMPONENT_PREMIUM("Structural Component Premium");
    private final String value;

    MoneyProvisionRoleInContractTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MoneyProvisionRoleInContractTypeName fromValue(String v) {
        for (MoneyProvisionRoleInContractTypeName c: MoneyProvisionRoleInContractTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
