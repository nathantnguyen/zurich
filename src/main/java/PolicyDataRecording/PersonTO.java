
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Person_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Person_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Party_TO">
 *       &lt;sequence>
 *         &lt;element name="birthDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="deathDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="disposableIncome" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="educationLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}EducationLevel" minOccurs="0"/>
 *         &lt;element name="employmentStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}EmploymentStatus" minOccurs="0"/>
 *         &lt;element name="gender" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Gender" minOccurs="0"/>
 *         &lt;element name="grossIncome" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="maritalStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}MaritalStatus" minOccurs="0"/>
 *         &lt;element name="numberOfDependentChildren" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Byte" minOccurs="0"/>
 *         &lt;element name="numberOfDependentAdults" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Byte" minOccurs="0"/>
 *         &lt;element name="missing" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="missingNotificationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="ethnicity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}Ethnicity" minOccurs="0"/>
 *         &lt;element name="birthDateIsEstimate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="personalSaving" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="body" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}HumanBody_TO" minOccurs="0"/>
 *         &lt;element name="communicationPreference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CommunicationProfile_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="occupations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Occupation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="isSmoker" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isHealthy" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="currentEmployerName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="maritalStatusDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="overallCondition" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}OverallCondition" minOccurs="0"/>
 *         &lt;element name="previousEmployerName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="birthPlaceName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="bankingProfile" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}BankingProfile" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Person_TO", propOrder = {
    "birthDate",
    "deathDate",
    "disposableIncome",
    "educationLevel",
    "employmentStatus",
    "gender",
    "grossIncome",
    "maritalStatus",
    "numberOfDependentChildren",
    "numberOfDependentAdults",
    "missing",
    "missingNotificationDate",
    "ethnicity",
    "birthDateIsEstimate",
    "personalSaving",
    "body",
    "communicationPreference",
    "occupations",
    "isSmoker",
    "isHealthy",
    "currentEmployerName",
    "maritalStatusDate",
    "overallCondition",
    "previousEmployerName",
    "birthPlaceName",
    "bankingProfile"
})
public class PersonTO
    extends PartyTO
{

    protected XMLGregorianCalendar birthDate;
    protected XMLGregorianCalendar deathDate;
    protected BaseCurrencyAmount disposableIncome;
    protected EducationLevel educationLevel;
    protected EmploymentStatus employmentStatus;
    protected Gender gender;
    protected BaseCurrencyAmount grossIncome;
    protected MaritalStatus maritalStatus;
    protected Byte numberOfDependentChildren;
    protected Byte numberOfDependentAdults;
    protected Boolean missing;
    protected XMLGregorianCalendar missingNotificationDate;
    protected Ethnicity ethnicity;
    protected Boolean birthDateIsEstimate;
    protected BaseCurrencyAmount personalSaving;
    protected HumanBodyTO body;
    protected List<CommunicationProfileTO> communicationPreference;
    protected List<OccupationTO> occupations;
    protected Boolean isSmoker;
    protected Boolean isHealthy;
    protected String currentEmployerName;
    protected XMLGregorianCalendar maritalStatusDate;
    protected OverallCondition overallCondition;
    protected String previousEmployerName;
    protected String birthPlaceName;
    protected BankingProfile bankingProfile;

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the deathDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeathDate() {
        return deathDate;
    }

    /**
     * Sets the value of the deathDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeathDate(XMLGregorianCalendar value) {
        this.deathDate = value;
    }

    /**
     * Gets the value of the disposableIncome property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getDisposableIncome() {
        return disposableIncome;
    }

    /**
     * Sets the value of the disposableIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setDisposableIncome(BaseCurrencyAmount value) {
        this.disposableIncome = value;
    }

    /**
     * Gets the value of the educationLevel property.
     * 
     * @return
     *     possible object is
     *     {@link EducationLevel }
     *     
     */
    public EducationLevel getEducationLevel() {
        return educationLevel;
    }

    /**
     * Sets the value of the educationLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link EducationLevel }
     *     
     */
    public void setEducationLevel(EducationLevel value) {
        this.educationLevel = value;
    }

    /**
     * Gets the value of the employmentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link EmploymentStatus }
     *     
     */
    public EmploymentStatus getEmploymentStatus() {
        return employmentStatus;
    }

    /**
     * Sets the value of the employmentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmploymentStatus }
     *     
     */
    public void setEmploymentStatus(EmploymentStatus value) {
        this.employmentStatus = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link Gender }
     *     
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link Gender }
     *     
     */
    public void setGender(Gender value) {
        this.gender = value;
    }

    /**
     * Gets the value of the grossIncome property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getGrossIncome() {
        return grossIncome;
    }

    /**
     * Sets the value of the grossIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setGrossIncome(BaseCurrencyAmount value) {
        this.grossIncome = value;
    }

    /**
     * Gets the value of the maritalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link MaritalStatus }
     *     
     */
    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Sets the value of the maritalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaritalStatus }
     *     
     */
    public void setMaritalStatus(MaritalStatus value) {
        this.maritalStatus = value;
    }

    /**
     * Gets the value of the numberOfDependentChildren property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getNumberOfDependentChildren() {
        return numberOfDependentChildren;
    }

    /**
     * Sets the value of the numberOfDependentChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setNumberOfDependentChildren(Byte value) {
        this.numberOfDependentChildren = value;
    }

    /**
     * Gets the value of the numberOfDependentAdults property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getNumberOfDependentAdults() {
        return numberOfDependentAdults;
    }

    /**
     * Sets the value of the numberOfDependentAdults property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setNumberOfDependentAdults(Byte value) {
        this.numberOfDependentAdults = value;
    }

    /**
     * Gets the value of the missing property.
     * This getter has been renamed from isMissing() to getMissing() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMissing() {
        return missing;
    }

    /**
     * Sets the value of the missing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMissing(Boolean value) {
        this.missing = value;
    }

    /**
     * Gets the value of the missingNotificationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMissingNotificationDate() {
        return missingNotificationDate;
    }

    /**
     * Sets the value of the missingNotificationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMissingNotificationDate(XMLGregorianCalendar value) {
        this.missingNotificationDate = value;
    }

    /**
     * Gets the value of the ethnicity property.
     * 
     * @return
     *     possible object is
     *     {@link Ethnicity }
     *     
     */
    public Ethnicity getEthnicity() {
        return ethnicity;
    }

    /**
     * Sets the value of the ethnicity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ethnicity }
     *     
     */
    public void setEthnicity(Ethnicity value) {
        this.ethnicity = value;
    }

    /**
     * Gets the value of the birthDateIsEstimate property.
     * This getter has been renamed from isBirthDateIsEstimate() to getBirthDateIsEstimate() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBirthDateIsEstimate() {
        return birthDateIsEstimate;
    }

    /**
     * Sets the value of the birthDateIsEstimate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBirthDateIsEstimate(Boolean value) {
        this.birthDateIsEstimate = value;
    }

    /**
     * Gets the value of the personalSaving property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getPersonalSaving() {
        return personalSaving;
    }

    /**
     * Sets the value of the personalSaving property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setPersonalSaving(BaseCurrencyAmount value) {
        this.personalSaving = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link HumanBodyTO }
     *     
     */
    public HumanBodyTO getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link HumanBodyTO }
     *     
     */
    public void setBody(HumanBodyTO value) {
        this.body = value;
    }

    /**
     * Gets the value of the communicationPreference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the communicationPreference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommunicationPreference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationProfileTO }
     * 
     * 
     */
    public List<CommunicationProfileTO> getCommunicationPreference() {
        if (communicationPreference == null) {
            communicationPreference = new ArrayList<CommunicationProfileTO>();
        }
        return this.communicationPreference;
    }

    /**
     * Gets the value of the occupations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the occupations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOccupations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OccupationTO }
     * 
     * 
     */
    public List<OccupationTO> getOccupations() {
        if (occupations == null) {
            occupations = new ArrayList<OccupationTO>();
        }
        return this.occupations;
    }

    /**
     * Gets the value of the isSmoker property.
     * This getter has been renamed from isIsSmoker() to getIsSmoker() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsSmoker() {
        return isSmoker;
    }

    /**
     * Sets the value of the isSmoker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSmoker(Boolean value) {
        this.isSmoker = value;
    }

    /**
     * Gets the value of the isHealthy property.
     * This getter has been renamed from isIsHealthy() to getIsHealthy() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsHealthy() {
        return isHealthy;
    }

    /**
     * Sets the value of the isHealthy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsHealthy(Boolean value) {
        this.isHealthy = value;
    }

    /**
     * Gets the value of the currentEmployerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentEmployerName() {
        return currentEmployerName;
    }

    /**
     * Sets the value of the currentEmployerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentEmployerName(String value) {
        this.currentEmployerName = value;
    }

    /**
     * Gets the value of the maritalStatusDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMaritalStatusDate() {
        return maritalStatusDate;
    }

    /**
     * Sets the value of the maritalStatusDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMaritalStatusDate(XMLGregorianCalendar value) {
        this.maritalStatusDate = value;
    }

    /**
     * Gets the value of the overallCondition property.
     * 
     * @return
     *     possible object is
     *     {@link OverallCondition }
     *     
     */
    public OverallCondition getOverallCondition() {
        return overallCondition;
    }

    /**
     * Sets the value of the overallCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link OverallCondition }
     *     
     */
    public void setOverallCondition(OverallCondition value) {
        this.overallCondition = value;
    }

    /**
     * Gets the value of the previousEmployerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousEmployerName() {
        return previousEmployerName;
    }

    /**
     * Sets the value of the previousEmployerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousEmployerName(String value) {
        this.previousEmployerName = value;
    }

    /**
     * Gets the value of the birthPlaceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthPlaceName() {
        return birthPlaceName;
    }

    /**
     * Sets the value of the birthPlaceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthPlaceName(String value) {
        this.birthPlaceName = value;
    }

    /**
     * Gets the value of the bankingProfile property.
     * 
     * @return
     *     possible object is
     *     {@link BankingProfile }
     *     
     */
    public BankingProfile getBankingProfile() {
        return bankingProfile;
    }

    /**
     * Sets the value of the bankingProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankingProfile }
     *     
     */
    public void setBankingProfile(BankingProfile value) {
        this.bankingProfile = value;
    }

    /**
     * Sets the value of the communicationPreference property.
     * 
     * @param communicationPreference
     *     allowed object is
     *     {@link CommunicationProfileTO }
     *     
     */
    public void setCommunicationPreference(List<CommunicationProfileTO> communicationPreference) {
        this.communicationPreference = communicationPreference;
    }

    /**
     * Sets the value of the occupations property.
     * 
     * @param occupations
     *     allowed object is
     *     {@link OccupationTO }
     *     
     */
    public void setOccupations(List<OccupationTO> occupations) {
        this.occupations = occupations;
    }

}
