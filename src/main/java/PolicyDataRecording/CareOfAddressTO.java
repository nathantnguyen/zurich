
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CareOfAddress_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CareOfAddress_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPoint_TO">
 *       &lt;sequence>
 *         &lt;element name="careOfAddressee" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyName_TO" minOccurs="0"/>
 *         &lt;element name="careOfContactPoint" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ContactPoint_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CareOfAddress_TO", propOrder = {
    "careOfAddressee",
    "careOfContactPoint"
})
public class CareOfAddressTO
    extends ContactPointTO
{

    protected PartyNameTO careOfAddressee;
    protected ContactPointTO careOfContactPoint;

    /**
     * Gets the value of the careOfAddressee property.
     * 
     * @return
     *     possible object is
     *     {@link PartyNameTO }
     *     
     */
    public PartyNameTO getCareOfAddressee() {
        return careOfAddressee;
    }

    /**
     * Sets the value of the careOfAddressee property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyNameTO }
     *     
     */
    public void setCareOfAddressee(PartyNameTO value) {
        this.careOfAddressee = value;
    }

    /**
     * Gets the value of the careOfContactPoint property.
     * 
     * @return
     *     possible object is
     *     {@link ContactPointTO }
     *     
     */
    public ContactPointTO getCareOfContactPoint() {
        return careOfContactPoint;
    }

    /**
     * Sets the value of the careOfContactPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactPointTO }
     *     
     */
    public void setCareOfContactPoint(ContactPointTO value) {
        this.careOfContactPoint = value;
    }

}
