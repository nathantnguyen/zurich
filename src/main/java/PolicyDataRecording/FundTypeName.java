
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FundTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FundTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Index Linked Fund"/>
 *     &lt;enumeration value="Commodity Fund"/>
 *     &lt;enumeration value="Equity Fund"/>
 *     &lt;enumeration value="Real Estate Fund"/>
 *     &lt;enumeration value="Debt Fund"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FundTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum FundTypeName {

    @XmlEnumValue("Index Linked Fund")
    INDEX_LINKED_FUND("Index Linked Fund"),
    @XmlEnumValue("Commodity Fund")
    COMMODITY_FUND("Commodity Fund"),
    @XmlEnumValue("Equity Fund")
    EQUITY_FUND("Equity Fund"),
    @XmlEnumValue("Real Estate Fund")
    REAL_ESTATE_FUND("Real Estate Fund"),
    @XmlEnumValue("Debt Fund")
    DEBT_FUND("Debt Fund");
    private final String value;

    FundTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FundTypeName fromValue(String v) {
        for (FundTypeName c: FundTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
