
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInContextClass_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInContextClass_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Role_TO">
 *       &lt;sequence>
 *         &lt;element name="rolePlayerReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO" minOccurs="0"/>
 *         &lt;element name="rolePlayerNavigation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RolePlayerClassRole_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInContextClass_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "rolePlayerReference",
    "rolePlayerNavigation"
})
@XmlSeeAlso({
    RoleInCommunicationContentTO.class,
    RoleInProductGroupTO.class,
    RoleInContractSpecificationTO.class,
    RoleInRiskExposureTO.class,
    RoleInRiskAssessmentTO.class,
    RoleInEventTO.class,
    RoleInPhysicalObjectTO.class,
    RoleInStatisticsTO.class,
    RoleInContractRequestTO.class,
    RoleInContractTO.class,
    RoleInTaskTO.class,
    RoleInLegalActionTO.class,
    RoleInFinancialTransactionTO.class,
    RoleInMoneyProvisionTO.class,
    RoleInFinancialAssetTO.class,
    RoleInAccountTO.class,
    RoleInGoalAndNeedTO.class,
    RoleInGeneralActivityTO.class,
    RoleInRolePlayerTO.class
})
public class RoleInContextClassTO
    extends RoleTO
{

    protected BusinessModelObjectTO rolePlayerReference;
    protected RolePlayerClassRoleTO rolePlayerNavigation;

    /**
     * Gets the value of the rolePlayerReference property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessModelObjectTO }
     *     
     */
    public BusinessModelObjectTO getRolePlayerReference() {
        return rolePlayerReference;
    }

    /**
     * Sets the value of the rolePlayerReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessModelObjectTO }
     *     
     */
    public void setRolePlayerReference(BusinessModelObjectTO value) {
        this.rolePlayerReference = value;
    }

    /**
     * Gets the value of the rolePlayerNavigation property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerClassRoleTO }
     *     
     */
    public RolePlayerClassRoleTO getRolePlayerNavigation() {
        return rolePlayerNavigation;
    }

    /**
     * Sets the value of the rolePlayerNavigation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerClassRoleTO }
     *     
     */
    public void setRolePlayerNavigation(RolePlayerClassRoleTO value) {
        this.rolePlayerNavigation = value;
    }

}
