
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InsurerRejectionReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InsurerRejectionReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="High Risk"/>
 *     &lt;enumeration value="History Of Fraud"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InsurerRejectionReason", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum InsurerRejectionReason {

    @XmlEnumValue("High Risk")
    HIGH_RISK("High Risk"),
    @XmlEnumValue("History Of Fraud")
    HISTORY_OF_FRAUD("History Of Fraud");
    private final String value;

    InsurerRejectionReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InsurerRejectionReason fromValue(String v) {
        for (InsurerRejectionReason c: InsurerRejectionReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
