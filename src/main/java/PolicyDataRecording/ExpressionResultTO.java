
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExpressionResult_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExpressionResult_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="originalValue" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}TriStateLogic" minOccurs="0"/>
 *         &lt;element name="overriddenValue" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}TriStateLogic" minOccurs="0"/>
 *         &lt;element name="isOverridable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="compositeExpressionResult" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ExpressionResult_TO" minOccurs="0"/>
 *         &lt;element name="expressionResultComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}ExpressionResult_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExpressionResult_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "description",
    "originalValue",
    "overriddenValue",
    "isOverridable",
    "compositeExpressionResult",
    "expressionResultComponents"
})
public class ExpressionResultTO
    extends BaseTransferObject
{

    protected String description;
    protected TriStateLogic originalValue;
    protected TriStateLogic overriddenValue;
    protected Boolean isOverridable;
    protected ExpressionResultTO compositeExpressionResult;
    protected List<ExpressionResultTO> expressionResultComponents;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the originalValue property.
     * 
     * @return
     *     possible object is
     *     {@link TriStateLogic }
     *     
     */
    public TriStateLogic getOriginalValue() {
        return originalValue;
    }

    /**
     * Sets the value of the originalValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link TriStateLogic }
     *     
     */
    public void setOriginalValue(TriStateLogic value) {
        this.originalValue = value;
    }

    /**
     * Gets the value of the overriddenValue property.
     * 
     * @return
     *     possible object is
     *     {@link TriStateLogic }
     *     
     */
    public TriStateLogic getOverriddenValue() {
        return overriddenValue;
    }

    /**
     * Sets the value of the overriddenValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link TriStateLogic }
     *     
     */
    public void setOverriddenValue(TriStateLogic value) {
        this.overriddenValue = value;
    }

    /**
     * Gets the value of the isOverridable property.
     * This getter has been renamed from isIsOverridable() to getIsOverridable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOverridable() {
        return isOverridable;
    }

    /**
     * Sets the value of the isOverridable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOverridable(Boolean value) {
        this.isOverridable = value;
    }

    /**
     * Gets the value of the compositeExpressionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionResultTO }
     *     
     */
    public ExpressionResultTO getCompositeExpressionResult() {
        return compositeExpressionResult;
    }

    /**
     * Sets the value of the compositeExpressionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionResultTO }
     *     
     */
    public void setCompositeExpressionResult(ExpressionResultTO value) {
        this.compositeExpressionResult = value;
    }

    /**
     * Gets the value of the expressionResultComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expressionResultComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpressionResultComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExpressionResultTO }
     * 
     * 
     */
    public List<ExpressionResultTO> getExpressionResultComponents() {
        if (expressionResultComponents == null) {
            expressionResultComponents = new ArrayList<ExpressionResultTO>();
        }
        return this.expressionResultComponents;
    }

    /**
     * Sets the value of the expressionResultComponents property.
     * 
     * @param expressionResultComponents
     *     allowed object is
     *     {@link ExpressionResultTO }
     *     
     */
    public void setExpressionResultComponents(List<ExpressionResultTO> expressionResultComponents) {
        this.expressionResultComponents = expressionResultComponents;
    }

}
