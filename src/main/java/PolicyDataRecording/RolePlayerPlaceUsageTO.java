
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RolePlayerPlaceUsage_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RolePlayerPlaceUsage_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInRolePlayer_TO">
 *       &lt;sequence>
 *         &lt;element name="rolePlayerPlaceUsageRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerPlaceUsageType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RolePlayerPlaceUsage_TO", propOrder = {
    "rolePlayerPlaceUsageRootType"
})
@XmlSeeAlso({
    ResidenceTO.class
})
public class RolePlayerPlaceUsageTO
    extends RoleInRolePlayerTO
{

    protected RolePlayerPlaceUsageTypeTO rolePlayerPlaceUsageRootType;

    /**
     * Gets the value of the rolePlayerPlaceUsageRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerPlaceUsageTypeTO }
     *     
     */
    public RolePlayerPlaceUsageTypeTO getRolePlayerPlaceUsageRootType() {
        return rolePlayerPlaceUsageRootType;
    }

    /**
     * Sets the value of the rolePlayerPlaceUsageRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerPlaceUsageTypeTO }
     *     
     */
    public void setRolePlayerPlaceUsageRootType(RolePlayerPlaceUsageTypeTO value) {
        this.rolePlayerPlaceUsageRootType = value;
    }

}
