
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DelinquencyAging.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DelinquencyAging">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Over 121"/>
 *     &lt;enumeration value="Delinquency 0 30"/>
 *     &lt;enumeration value="Delinquency 31 60"/>
 *     &lt;enumeration value="Delinquency 61 90"/>
 *     &lt;enumeration value="Delinquency 91 120"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DelinquencyAging", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum DelinquencyAging {

    @XmlEnumValue("Over 121")
    OVER_121("Over 121"),
    @XmlEnumValue("Delinquency 0 30")
    DELINQUENCY_0_30("Delinquency 0 30"),
    @XmlEnumValue("Delinquency 31 60")
    DELINQUENCY_31_60("Delinquency 31 60"),
    @XmlEnumValue("Delinquency 61 90")
    DELINQUENCY_61_90("Delinquency 61 90"),
    @XmlEnumValue("Delinquency 91 120")
    DELINQUENCY_91_120("Delinquency 91 120");
    private final String value;

    DelinquencyAging(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DelinquencyAging fromValue(String v) {
        for (DelinquencyAging c: DelinquencyAging.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
