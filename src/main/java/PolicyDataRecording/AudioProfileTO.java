
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AudioProfile_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AudioProfile_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CommunicationProfile_TO">
 *       &lt;sequence>
 *         &lt;element name="visualAlert" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="teleType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="speechReplacement" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="soundAmplification" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}SoundAmplification" minOccurs="0"/>
 *         &lt;element name="signLanguage" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}SignLanguage" minOccurs="0"/>
 *         &lt;element name="authorisedRelayService" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}RelayService_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="usesRelayService" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AudioProfile_TO", propOrder = {
    "visualAlert",
    "teleType",
    "speechReplacement",
    "soundAmplification",
    "signLanguage",
    "authorisedRelayService",
    "usesRelayService"
})
public class AudioProfileTO
    extends CommunicationProfileTO
{

    protected Boolean visualAlert;
    protected Boolean teleType;
    protected Boolean speechReplacement;
    protected SoundAmplification soundAmplification;
    protected SignLanguage signLanguage;
    protected List<RelayServiceTO> authorisedRelayService;
    protected Boolean usesRelayService;

    /**
     * Gets the value of the visualAlert property.
     * This getter has been renamed from isVisualAlert() to getVisualAlert() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getVisualAlert() {
        return visualAlert;
    }

    /**
     * Sets the value of the visualAlert property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisualAlert(Boolean value) {
        this.visualAlert = value;
    }

    /**
     * Gets the value of the teleType property.
     * This getter has been renamed from isTeleType() to getTeleType() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getTeleType() {
        return teleType;
    }

    /**
     * Sets the value of the teleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTeleType(Boolean value) {
        this.teleType = value;
    }

    /**
     * Gets the value of the speechReplacement property.
     * This getter has been renamed from isSpeechReplacement() to getSpeechReplacement() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSpeechReplacement() {
        return speechReplacement;
    }

    /**
     * Sets the value of the speechReplacement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSpeechReplacement(Boolean value) {
        this.speechReplacement = value;
    }

    /**
     * Gets the value of the soundAmplification property.
     * 
     * @return
     *     possible object is
     *     {@link SoundAmplification }
     *     
     */
    public SoundAmplification getSoundAmplification() {
        return soundAmplification;
    }

    /**
     * Sets the value of the soundAmplification property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoundAmplification }
     *     
     */
    public void setSoundAmplification(SoundAmplification value) {
        this.soundAmplification = value;
    }

    /**
     * Gets the value of the signLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link SignLanguage }
     *     
     */
    public SignLanguage getSignLanguage() {
        return signLanguage;
    }

    /**
     * Sets the value of the signLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link SignLanguage }
     *     
     */
    public void setSignLanguage(SignLanguage value) {
        this.signLanguage = value;
    }

    /**
     * Gets the value of the authorisedRelayService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the authorisedRelayService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAuthorisedRelayService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelayServiceTO }
     * 
     * 
     */
    public List<RelayServiceTO> getAuthorisedRelayService() {
        if (authorisedRelayService == null) {
            authorisedRelayService = new ArrayList<RelayServiceTO>();
        }
        return this.authorisedRelayService;
    }

    /**
     * Gets the value of the usesRelayService property.
     * This getter has been renamed from isUsesRelayService() to getUsesRelayService() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUsesRelayService() {
        return usesRelayService;
    }

    /**
     * Sets the value of the usesRelayService property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsesRelayService(Boolean value) {
        this.usesRelayService = value;
    }

    /**
     * Sets the value of the authorisedRelayService property.
     * 
     * @param authorisedRelayService
     *     allowed object is
     *     {@link RelayServiceTO }
     *     
     */
    public void setAuthorisedRelayService(List<RelayServiceTO> authorisedRelayService) {
        this.authorisedRelayService = authorisedRelayService;
    }

}
