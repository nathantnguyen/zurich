
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BenefitDistributionCalculation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BenefitDistributionCalculation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Balance"/>
 *     &lt;enumeration value="Equal Parts"/>
 *     &lt;enumeration value="Flat Amount"/>
 *     &lt;enumeration value="Percentage Of Balance"/>
 *     &lt;enumeration value="Per Capita"/>
 *     &lt;enumeration value="Per Stirpes"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BenefitDistributionCalculation", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum BenefitDistributionCalculation {

    @XmlEnumValue("Balance")
    BALANCE("Balance"),
    @XmlEnumValue("Equal Parts")
    EQUAL_PARTS("Equal Parts"),
    @XmlEnumValue("Flat Amount")
    FLAT_AMOUNT("Flat Amount"),
    @XmlEnumValue("Percentage Of Balance")
    PERCENTAGE_OF_BALANCE("Percentage Of Balance"),
    @XmlEnumValue("Per Capita")
    PER_CAPITA("Per Capita"),
    @XmlEnumValue("Per Stirpes")
    PER_STIRPES("Per Stirpes");
    private final String value;

    BenefitDistributionCalculation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BenefitDistributionCalculation fromValue(String v) {
        for (BenefitDistributionCalculation c: BenefitDistributionCalculation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
