
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrganisationOwnershipForm.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganisationOwnershipForm">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Shareholder"/>
 *     &lt;enumeration value="Dormant Partner"/>
 *     &lt;enumeration value="Majority Shareholder"/>
 *     &lt;enumeration value="Sole Owner"/>
 *     &lt;enumeration value="Equity Shareholder"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrganisationOwnershipForm", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum OrganisationOwnershipForm {

    @XmlEnumValue("Shareholder")
    SHAREHOLDER("Shareholder"),
    @XmlEnumValue("Dormant Partner")
    DORMANT_PARTNER("Dormant Partner"),
    @XmlEnumValue("Majority Shareholder")
    MAJORITY_SHAREHOLDER("Majority Shareholder"),
    @XmlEnumValue("Sole Owner")
    SOLE_OWNER("Sole Owner"),
    @XmlEnumValue("Equity Shareholder")
    EQUITY_SHAREHOLDER("Equity Shareholder");
    private final String value;

    OrganisationOwnershipForm(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrganisationOwnershipForm fromValue(String v) {
        for (OrganisationOwnershipForm c: OrganisationOwnershipForm.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
