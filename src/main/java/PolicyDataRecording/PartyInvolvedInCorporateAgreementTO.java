
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyInvolvedInCorporateAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyInvolvedInCorporateAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RoleInCorporateAgreement_TO">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyInvolvedInCorporateAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/")
public class PartyInvolvedInCorporateAgreementTO
    extends RoleInCorporateAgreementTO
{


}
