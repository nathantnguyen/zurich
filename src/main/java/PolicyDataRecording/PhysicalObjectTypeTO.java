
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PhysicalObjectType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PhysicalObjectType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}PhysicalObjectTypeName" minOccurs="0"/>
 *         &lt;element name="isMovable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhysicalObjectType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "name",
    "isMovable"
})
public class PhysicalObjectTypeTO
    extends TypeTO
{

    protected PhysicalObjectTypeName name;
    protected Boolean isMovable;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalObjectTypeName }
     *     
     */
    public PhysicalObjectTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalObjectTypeName }
     *     
     */
    public void setName(PhysicalObjectTypeName value) {
        this.name = value;
    }

    /**
     * Gets the value of the isMovable property.
     * This getter has been renamed from isIsMovable() to getIsMovable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsMovable() {
        return isMovable;
    }

    /**
     * Sets the value of the isMovable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsMovable(Boolean value) {
        this.isMovable = value;
    }

}
