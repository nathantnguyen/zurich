
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Specification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Specification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="canBeRoot" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="requestSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RequestSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="calculationSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Calculation_TO" minOccurs="0"/>
 *         &lt;element name="roleSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RelationshipSpec_TO" minOccurs="0"/>
 *         &lt;element name="specificationComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Specification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="compositeSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Specification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ruleSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RuleSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="attributeSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AttributeSpec_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Specification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "description",
    "canBeRoot",
    "name",
    "kind",
    "requestSpecifications",
    "calculationSpecifications",
    "roleSpecifications",
    "specificationComponents",
    "compositeSpecifications",
    "ruleSpecifications",
    "attributeSpecifications"
})
@XmlSeeAlso({
    AgreementSpecTO.class,
    RequestSpecTO.class
})
public class SpecificationTO
    extends BusinessModelObjectTO
{

    protected String description;
    protected Boolean canBeRoot;
    protected String name;
    protected String kind;
    protected List<RequestSpecTO> requestSpecifications;
    protected CalculationTO calculationSpecifications;
    protected RelationshipSpecTO roleSpecifications;
    protected List<SpecificationTO> specificationComponents;
    protected List<SpecificationTO> compositeSpecifications;
    protected List<RuleSpecTO> ruleSpecifications;
    protected AttributeSpecTO attributeSpecifications;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the canBeRoot property.
     * This getter has been renamed from isCanBeRoot() to getCanBeRoot() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCanBeRoot() {
        return canBeRoot;
    }

    /**
     * Sets the value of the canBeRoot property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanBeRoot(Boolean value) {
        this.canBeRoot = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

    /**
     * Gets the value of the requestSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestSpecTO }
     * 
     * 
     */
    public List<RequestSpecTO> getRequestSpecifications() {
        if (requestSpecifications == null) {
            requestSpecifications = new ArrayList<RequestSpecTO>();
        }
        return this.requestSpecifications;
    }

    /**
     * Gets the value of the calculationSpecifications property.
     * 
     * @return
     *     possible object is
     *     {@link CalculationTO }
     *     
     */
    public CalculationTO getCalculationSpecifications() {
        return calculationSpecifications;
    }

    /**
     * Sets the value of the calculationSpecifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalculationTO }
     *     
     */
    public void setCalculationSpecifications(CalculationTO value) {
        this.calculationSpecifications = value;
    }

    /**
     * Gets the value of the roleSpecifications property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipSpecTO }
     *     
     */
    public RelationshipSpecTO getRoleSpecifications() {
        return roleSpecifications;
    }

    /**
     * Sets the value of the roleSpecifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipSpecTO }
     *     
     */
    public void setRoleSpecifications(RelationshipSpecTO value) {
        this.roleSpecifications = value;
    }

    /**
     * Gets the value of the specificationComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specificationComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecificationComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecificationTO }
     * 
     * 
     */
    public List<SpecificationTO> getSpecificationComponents() {
        if (specificationComponents == null) {
            specificationComponents = new ArrayList<SpecificationTO>();
        }
        return this.specificationComponents;
    }

    /**
     * Gets the value of the compositeSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the compositeSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCompositeSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecificationTO }
     * 
     * 
     */
    public List<SpecificationTO> getCompositeSpecifications() {
        if (compositeSpecifications == null) {
            compositeSpecifications = new ArrayList<SpecificationTO>();
        }
        return this.compositeSpecifications;
    }

    /**
     * Gets the value of the ruleSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ruleSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRuleSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RuleSpecTO }
     * 
     * 
     */
    public List<RuleSpecTO> getRuleSpecifications() {
        if (ruleSpecifications == null) {
            ruleSpecifications = new ArrayList<RuleSpecTO>();
        }
        return this.ruleSpecifications;
    }

    /**
     * Gets the value of the attributeSpecifications property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeSpecTO }
     *     
     */
    public AttributeSpecTO getAttributeSpecifications() {
        return attributeSpecifications;
    }

    /**
     * Sets the value of the attributeSpecifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeSpecTO }
     *     
     */
    public void setAttributeSpecifications(AttributeSpecTO value) {
        this.attributeSpecifications = value;
    }

    /**
     * Sets the value of the requestSpecifications property.
     * 
     * @param requestSpecifications
     *     allowed object is
     *     {@link RequestSpecTO }
     *     
     */
    public void setRequestSpecifications(List<RequestSpecTO> requestSpecifications) {
        this.requestSpecifications = requestSpecifications;
    }

    /**
     * Sets the value of the specificationComponents property.
     * 
     * @param specificationComponents
     *     allowed object is
     *     {@link SpecificationTO }
     *     
     */
    public void setSpecificationComponents(List<SpecificationTO> specificationComponents) {
        this.specificationComponents = specificationComponents;
    }

    /**
     * Sets the value of the compositeSpecifications property.
     * 
     * @param compositeSpecifications
     *     allowed object is
     *     {@link SpecificationTO }
     *     
     */
    public void setCompositeSpecifications(List<SpecificationTO> compositeSpecifications) {
        this.compositeSpecifications = compositeSpecifications;
    }

    /**
     * Sets the value of the ruleSpecifications property.
     * 
     * @param ruleSpecifications
     *     allowed object is
     *     {@link RuleSpecTO }
     *     
     */
    public void setRuleSpecifications(List<RuleSpecTO> ruleSpecifications) {
        this.ruleSpecifications = ruleSpecifications;
    }

}
