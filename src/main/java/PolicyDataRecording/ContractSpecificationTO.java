
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractSpecification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractSpecification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="version" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="productRegistration" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractSpecificationRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractSpecificationStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="kind" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="allowedRequests" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecificationAllowance_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ruleSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RuleSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="calculations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Calculation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="attributeSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AttributeSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="requestSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="relationshipSpecs" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RelationshipSpec_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="standardTextSpecificationRole" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}StandardTextSpecificationInvolvedInContractSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="originalContractSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractSpecification_TO" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="relatedFromContractSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractSpecificationRelationship_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractSpecification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "name",
    "description",
    "version",
    "productRegistration",
    "status",
    "externalReference",
    "kind",
    "allowedRequests",
    "ruleSpecs",
    "calculations",
    "attributeSpecs",
    "requestSpecifications",
    "relationshipSpecs",
    "standardTextSpecificationRole",
    "originalContractSpecification",
    "alternateReference",
    "relatedFromContractSpecification"
})
@XmlSeeAlso({
    FinancialServicesProductTO.class,
    AccountingSpecificationTO.class,
    CorporateAgreementSpecificationTO.class,
    IntermediaryAgreementSpecificationTO.class,
    EmploymentAgreementSpecificationTO.class
})
public class ContractSpecificationTO
    extends BusinessModelObjectTO
{

    protected String name;
    protected String description;
    protected String version;
    protected List<ContractSpecificationRegistrationTO> productRegistration;
    protected List<ContractSpecificationStatusTO> status;
    protected String externalReference;
    protected String kind;
    protected List<ContractRequestSpecificationAllowanceTO> allowedRequests;
    protected List<RuleSpecTO> ruleSpecs;
    protected List<CalculationTO> calculations;
    protected List<AttributeSpecTO> attributeSpecs;
    protected List<ContractRequestSpecificationTO> requestSpecifications;
    protected List<RelationshipSpecTO> relationshipSpecs;
    protected List<StandardTextSpecificationInvolvedInContractSpecificationTO> standardTextSpecificationRole;
    protected ContractSpecificationTO originalContractSpecification;
    protected List<AlternateIdTO> alternateReference;
    protected List<ContractSpecificationRelationshipTO> relatedFromContractSpecification;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the productRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractSpecificationRegistrationTO }
     * 
     * 
     */
    public List<ContractSpecificationRegistrationTO> getProductRegistration() {
        if (productRegistration == null) {
            productRegistration = new ArrayList<ContractSpecificationRegistrationTO>();
        }
        return this.productRegistration;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractSpecificationStatusTO }
     * 
     * 
     */
    public List<ContractSpecificationStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<ContractSpecificationStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the kind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKind() {
        return kind;
    }

    /**
     * Sets the value of the kind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKind(String value) {
        this.kind = value;
    }

    /**
     * Gets the value of the allowedRequests property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allowedRequests property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllowedRequests().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRequestSpecificationAllowanceTO }
     * 
     * 
     */
    public List<ContractRequestSpecificationAllowanceTO> getAllowedRequests() {
        if (allowedRequests == null) {
            allowedRequests = new ArrayList<ContractRequestSpecificationAllowanceTO>();
        }
        return this.allowedRequests;
    }

    /**
     * Gets the value of the ruleSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ruleSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRuleSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RuleSpecTO }
     * 
     * 
     */
    public List<RuleSpecTO> getRuleSpecs() {
        if (ruleSpecs == null) {
            ruleSpecs = new ArrayList<RuleSpecTO>();
        }
        return this.ruleSpecs;
    }

    /**
     * Gets the value of the calculations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the calculations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCalculations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalculationTO }
     * 
     * 
     */
    public List<CalculationTO> getCalculations() {
        if (calculations == null) {
            calculations = new ArrayList<CalculationTO>();
        }
        return this.calculations;
    }

    /**
     * Gets the value of the attributeSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeSpecTO }
     * 
     * 
     */
    public List<AttributeSpecTO> getAttributeSpecs() {
        if (attributeSpecs == null) {
            attributeSpecs = new ArrayList<AttributeSpecTO>();
        }
        return this.attributeSpecs;
    }

    /**
     * Gets the value of the requestSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRequestSpecificationTO }
     * 
     * 
     */
    public List<ContractRequestSpecificationTO> getRequestSpecifications() {
        if (requestSpecifications == null) {
            requestSpecifications = new ArrayList<ContractRequestSpecificationTO>();
        }
        return this.requestSpecifications;
    }

    /**
     * Gets the value of the relationshipSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relationshipSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelationshipSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelationshipSpecTO }
     * 
     * 
     */
    public List<RelationshipSpecTO> getRelationshipSpecs() {
        if (relationshipSpecs == null) {
            relationshipSpecs = new ArrayList<RelationshipSpecTO>();
        }
        return this.relationshipSpecs;
    }

    /**
     * Gets the value of the standardTextSpecificationRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the standardTextSpecificationRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStandardTextSpecificationRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StandardTextSpecificationInvolvedInContractSpecificationTO }
     * 
     * 
     */
    public List<StandardTextSpecificationInvolvedInContractSpecificationTO> getStandardTextSpecificationRole() {
        if (standardTextSpecificationRole == null) {
            standardTextSpecificationRole = new ArrayList<StandardTextSpecificationInvolvedInContractSpecificationTO>();
        }
        return this.standardTextSpecificationRole;
    }

    /**
     * Gets the value of the originalContractSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link ContractSpecificationTO }
     *     
     */
    public ContractSpecificationTO getOriginalContractSpecification() {
        return originalContractSpecification;
    }

    /**
     * Sets the value of the originalContractSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractSpecificationTO }
     *     
     */
    public void setOriginalContractSpecification(ContractSpecificationTO value) {
        this.originalContractSpecification = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Gets the value of the relatedFromContractSpecification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedFromContractSpecification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedFromContractSpecification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractSpecificationRelationshipTO }
     * 
     * 
     */
    public List<ContractSpecificationRelationshipTO> getRelatedFromContractSpecification() {
        if (relatedFromContractSpecification == null) {
            relatedFromContractSpecification = new ArrayList<ContractSpecificationRelationshipTO>();
        }
        return this.relatedFromContractSpecification;
    }

    /**
     * Sets the value of the productRegistration property.
     * 
     * @param productRegistration
     *     allowed object is
     *     {@link ContractSpecificationRegistrationTO }
     *     
     */
    public void setProductRegistration(List<ContractSpecificationRegistrationTO> productRegistration) {
        this.productRegistration = productRegistration;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link ContractSpecificationStatusTO }
     *     
     */
    public void setStatus(List<ContractSpecificationStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the allowedRequests property.
     * 
     * @param allowedRequests
     *     allowed object is
     *     {@link ContractRequestSpecificationAllowanceTO }
     *     
     */
    public void setAllowedRequests(List<ContractRequestSpecificationAllowanceTO> allowedRequests) {
        this.allowedRequests = allowedRequests;
    }

    /**
     * Sets the value of the ruleSpecs property.
     * 
     * @param ruleSpecs
     *     allowed object is
     *     {@link RuleSpecTO }
     *     
     */
    public void setRuleSpecs(List<RuleSpecTO> ruleSpecs) {
        this.ruleSpecs = ruleSpecs;
    }

    /**
     * Sets the value of the calculations property.
     * 
     * @param calculations
     *     allowed object is
     *     {@link CalculationTO }
     *     
     */
    public void setCalculations(List<CalculationTO> calculations) {
        this.calculations = calculations;
    }

    /**
     * Sets the value of the attributeSpecs property.
     * 
     * @param attributeSpecs
     *     allowed object is
     *     {@link AttributeSpecTO }
     *     
     */
    public void setAttributeSpecs(List<AttributeSpecTO> attributeSpecs) {
        this.attributeSpecs = attributeSpecs;
    }

    /**
     * Sets the value of the requestSpecifications property.
     * 
     * @param requestSpecifications
     *     allowed object is
     *     {@link ContractRequestSpecificationTO }
     *     
     */
    public void setRequestSpecifications(List<ContractRequestSpecificationTO> requestSpecifications) {
        this.requestSpecifications = requestSpecifications;
    }

    /**
     * Sets the value of the relationshipSpecs property.
     * 
     * @param relationshipSpecs
     *     allowed object is
     *     {@link RelationshipSpecTO }
     *     
     */
    public void setRelationshipSpecs(List<RelationshipSpecTO> relationshipSpecs) {
        this.relationshipSpecs = relationshipSpecs;
    }

    /**
     * Sets the value of the standardTextSpecificationRole property.
     * 
     * @param standardTextSpecificationRole
     *     allowed object is
     *     {@link StandardTextSpecificationInvolvedInContractSpecificationTO }
     *     
     */
    public void setStandardTextSpecificationRole(List<StandardTextSpecificationInvolvedInContractSpecificationTO> standardTextSpecificationRole) {
        this.standardTextSpecificationRole = standardTextSpecificationRole;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

    /**
     * Sets the value of the relatedFromContractSpecification property.
     * 
     * @param relatedFromContractSpecification
     *     allowed object is
     *     {@link ContractSpecificationRelationshipTO }
     *     
     */
    public void setRelatedFromContractSpecification(List<ContractSpecificationRelationshipTO> relatedFromContractSpecification) {
        this.relatedFromContractSpecification = relatedFromContractSpecification;
    }

}
