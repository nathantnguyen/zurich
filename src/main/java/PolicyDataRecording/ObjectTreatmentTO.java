
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectTreatment_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectTreatment_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO">
 *       &lt;sequence>
 *         &lt;element name="subjectPhysicalObjectReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="objectTreatmentRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ObjectTreatmentType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectTreatment_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "subjectPhysicalObjectReference",
    "objectTreatmentRootType"
})
@XmlSeeAlso({
    TransportationTO.class,
    MedicalTreatmentTO.class
})
public class ObjectTreatmentTO
    extends ActivityOccurrenceTO
{

    protected List<ObjectReferenceTO> subjectPhysicalObjectReference;
    protected ObjectTreatmentTypeTO objectTreatmentRootType;

    /**
     * Gets the value of the subjectPhysicalObjectReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subjectPhysicalObjectReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubjectPhysicalObjectReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getSubjectPhysicalObjectReference() {
        if (subjectPhysicalObjectReference == null) {
            subjectPhysicalObjectReference = new ArrayList<ObjectReferenceTO>();
        }
        return this.subjectPhysicalObjectReference;
    }

    /**
     * Gets the value of the objectTreatmentRootType property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectTreatmentTypeTO }
     *     
     */
    public ObjectTreatmentTypeTO getObjectTreatmentRootType() {
        return objectTreatmentRootType;
    }

    /**
     * Sets the value of the objectTreatmentRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectTreatmentTypeTO }
     *     
     */
    public void setObjectTreatmentRootType(ObjectTreatmentTypeTO value) {
        this.objectTreatmentRootType = value;
    }

    /**
     * Sets the value of the subjectPhysicalObjectReference property.
     * 
     * @param subjectPhysicalObjectReference
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSubjectPhysicalObjectReference(List<ObjectReferenceTO> subjectPhysicalObjectReference) {
        this.subjectPhysicalObjectReference = subjectPhysicalObjectReference;
    }

}
