
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}TopLevelFinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="allowMultiCurrencyBalances" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="allowMultiCurrencyTransactions" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="allowCollateral" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}AccountAgreementStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="interestPostingFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}InterestPostingFrequency" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "allowMultiCurrencyBalances",
    "allowMultiCurrencyTransactions",
    "allowCollateral",
    "status",
    "interestPostingFrequency"
})
@XmlSeeAlso({
    InvestmentPortfolioAgreementTO.class,
    LoanAccountAgreementTO.class,
    CreditCardAgreementTO.class,
    DepositAccountAgreementTO.class
})
public class AccountAgreementTO
    extends TopLevelFinancialServicesAgreementTO
{

    protected Boolean allowMultiCurrencyBalances;
    protected Boolean allowMultiCurrencyTransactions;
    protected Boolean allowCollateral;
    protected List<AccountAgreementStatusTO> status;
    protected InterestPostingFrequency interestPostingFrequency;

    /**
     * Gets the value of the allowMultiCurrencyBalances property.
     * This getter has been renamed from isAllowMultiCurrencyBalances() to getAllowMultiCurrencyBalances() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAllowMultiCurrencyBalances() {
        return allowMultiCurrencyBalances;
    }

    /**
     * Sets the value of the allowMultiCurrencyBalances property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowMultiCurrencyBalances(Boolean value) {
        this.allowMultiCurrencyBalances = value;
    }

    /**
     * Gets the value of the allowMultiCurrencyTransactions property.
     * This getter has been renamed from isAllowMultiCurrencyTransactions() to getAllowMultiCurrencyTransactions() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAllowMultiCurrencyTransactions() {
        return allowMultiCurrencyTransactions;
    }

    /**
     * Sets the value of the allowMultiCurrencyTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowMultiCurrencyTransactions(Boolean value) {
        this.allowMultiCurrencyTransactions = value;
    }

    /**
     * Gets the value of the allowCollateral property.
     * This getter has been renamed from isAllowCollateral() to getAllowCollateral() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAllowCollateral() {
        return allowCollateral;
    }

    /**
     * Sets the value of the allowCollateral property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowCollateral(Boolean value) {
        this.allowCollateral = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountAgreementStatusTO }
     * 
     * 
     */
    public List<AccountAgreementStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<AccountAgreementStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the interestPostingFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link InterestPostingFrequency }
     *     
     */
    public InterestPostingFrequency getInterestPostingFrequency() {
        return interestPostingFrequency;
    }

    /**
     * Sets the value of the interestPostingFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestPostingFrequency }
     *     
     */
    public void setInterestPostingFrequency(InterestPostingFrequency value) {
        this.interestPostingFrequency = value;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link AccountAgreementStatusTO }
     *     
     */
    public void setStatus(List<AccountAgreementStatusTO> status) {
        this.status = status;
    }

}
