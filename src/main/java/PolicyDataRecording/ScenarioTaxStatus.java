
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScenarioTaxStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ScenarioTaxStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Gross"/>
 *     &lt;enumeration value="Net"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ScenarioTaxStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum ScenarioTaxStatus {

    @XmlEnumValue("Gross")
    GROSS("Gross"),
    @XmlEnumValue("Net")
    NET("Net");
    private final String value;

    ScenarioTaxStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ScenarioTaxStatus fromValue(String v) {
        for (ScenarioTaxStatus c: ScenarioTaxStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
