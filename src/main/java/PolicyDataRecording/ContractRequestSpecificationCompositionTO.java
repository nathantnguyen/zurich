
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractRequestSpecificationComposition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractRequestSpecificationComposition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecificationInclusion_TO">
 *       &lt;sequence>
 *         &lt;element name="minimumNumberOfRequests" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractRequestSpecificationComposition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "minimumNumberOfRequests"
})
public class ContractRequestSpecificationCompositionTO
    extends ContractRequestSpecificationInclusionTO
{

    protected BigInteger minimumNumberOfRequests;

    /**
     * Gets the value of the minimumNumberOfRequests property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumNumberOfRequests() {
        return minimumNumberOfRequests;
    }

    /**
     * Sets the value of the minimumNumberOfRequests property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumNumberOfRequests(BigInteger value) {
        this.minimumNumberOfRequests = value;
    }

}
