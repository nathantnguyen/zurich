
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvestmentPortfolioAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvestmentPortfolioAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}AccountAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="clearingHouseIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="taxQualification" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}TaxQualification" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvestmentPortfolioAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "clearingHouseIndicator",
    "taxQualification"
})
public class InvestmentPortfolioAgreementTO
    extends AccountAgreementTO
{

    protected Boolean clearingHouseIndicator;
    protected String taxQualification;

    /**
     * Gets the value of the clearingHouseIndicator property.
     * This getter has been renamed from isClearingHouseIndicator() to getClearingHouseIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getClearingHouseIndicator() {
        return clearingHouseIndicator;
    }

    /**
     * Sets the value of the clearingHouseIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClearingHouseIndicator(Boolean value) {
        this.clearingHouseIndicator = value;
    }

    /**
     * Gets the value of the taxQualification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxQualification() {
        return taxQualification;
    }

    /**
     * Sets the value of the taxQualification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxQualification(String value) {
        this.taxQualification = value;
    }

}
