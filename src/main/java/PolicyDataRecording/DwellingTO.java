
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Dwelling_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Dwelling_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}Structure_TO">
 *       &lt;sequence>
 *         &lt;element name="numberOfBathrooms" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="numberOfBedrooms" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="neighbourhoodWatch" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="dwellingDesignStyle" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}DwellingDesignStyle" minOccurs="0"/>
 *         &lt;element name="isOwnerOccupied" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="occupationPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Dwelling_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "numberOfBathrooms",
    "numberOfBedrooms",
    "neighbourhoodWatch",
    "dwellingDesignStyle",
    "isOwnerOccupied",
    "occupationPeriod"
})
public class DwellingTO
    extends StructureTO
{

    protected BigDecimal numberOfBathrooms;
    protected BigDecimal numberOfBedrooms;
    protected Boolean neighbourhoodWatch;
    protected DwellingDesignStyle dwellingDesignStyle;
    protected Boolean isOwnerOccupied;
    protected String occupationPeriod;

    /**
     * Gets the value of the numberOfBathrooms property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumberOfBathrooms() {
        return numberOfBathrooms;
    }

    /**
     * Sets the value of the numberOfBathrooms property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumberOfBathrooms(BigDecimal value) {
        this.numberOfBathrooms = value;
    }

    /**
     * Gets the value of the numberOfBedrooms property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumberOfBedrooms() {
        return numberOfBedrooms;
    }

    /**
     * Sets the value of the numberOfBedrooms property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumberOfBedrooms(BigDecimal value) {
        this.numberOfBedrooms = value;
    }

    /**
     * Gets the value of the neighbourhoodWatch property.
     * This getter has been renamed from isNeighbourhoodWatch() to getNeighbourhoodWatch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getNeighbourhoodWatch() {
        return neighbourhoodWatch;
    }

    /**
     * Sets the value of the neighbourhoodWatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNeighbourhoodWatch(Boolean value) {
        this.neighbourhoodWatch = value;
    }

    /**
     * Gets the value of the dwellingDesignStyle property.
     * 
     * @return
     *     possible object is
     *     {@link DwellingDesignStyle }
     *     
     */
    public DwellingDesignStyle getDwellingDesignStyle() {
        return dwellingDesignStyle;
    }

    /**
     * Sets the value of the dwellingDesignStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link DwellingDesignStyle }
     *     
     */
    public void setDwellingDesignStyle(DwellingDesignStyle value) {
        this.dwellingDesignStyle = value;
    }

    /**
     * Gets the value of the isOwnerOccupied property.
     * This getter has been renamed from isIsOwnerOccupied() to getIsOwnerOccupied() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOwnerOccupied() {
        return isOwnerOccupied;
    }

    /**
     * Sets the value of the isOwnerOccupied property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOwnerOccupied(Boolean value) {
        this.isOwnerOccupied = value;
    }

    /**
     * Gets the value of the occupationPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationPeriod() {
        return occupationPeriod;
    }

    /**
     * Sets the value of the occupationPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationPeriod(String value) {
        this.occupationPeriod = value;
    }

}
