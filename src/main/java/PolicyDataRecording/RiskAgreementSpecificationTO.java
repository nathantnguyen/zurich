
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskAgreementSpecification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskAgreementSpecification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}CorporateAgreementSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="riskPositionCalculation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Calculation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="riskReportTemplates" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}DocumentTemplate_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskAgreementSpecification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "riskPositionCalculation",
    "riskReportTemplates"
})
public class RiskAgreementSpecificationTO
    extends CorporateAgreementSpecificationTO
{

    protected List<CalculationTO> riskPositionCalculation;
    protected List<DocumentTemplateTO> riskReportTemplates;

    /**
     * Gets the value of the riskPositionCalculation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riskPositionCalculation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRiskPositionCalculation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalculationTO }
     * 
     * 
     */
    public List<CalculationTO> getRiskPositionCalculation() {
        if (riskPositionCalculation == null) {
            riskPositionCalculation = new ArrayList<CalculationTO>();
        }
        return this.riskPositionCalculation;
    }

    /**
     * Gets the value of the riskReportTemplates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riskReportTemplates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRiskReportTemplates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentTemplateTO }
     * 
     * 
     */
    public List<DocumentTemplateTO> getRiskReportTemplates() {
        if (riskReportTemplates == null) {
            riskReportTemplates = new ArrayList<DocumentTemplateTO>();
        }
        return this.riskReportTemplates;
    }

    /**
     * Sets the value of the riskPositionCalculation property.
     * 
     * @param riskPositionCalculation
     *     allowed object is
     *     {@link CalculationTO }
     *     
     */
    public void setRiskPositionCalculation(List<CalculationTO> riskPositionCalculation) {
        this.riskPositionCalculation = riskPositionCalculation;
    }

    /**
     * Sets the value of the riskReportTemplates property.
     * 
     * @param riskReportTemplates
     *     allowed object is
     *     {@link DocumentTemplateTO }
     *     
     */
    public void setRiskReportTemplates(List<DocumentTemplateTO> riskReportTemplates) {
        this.riskReportTemplates = riskReportTemplates;
    }

}
