
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ActivityOccurrence_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityOccurrence_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Activity_TO">
 *       &lt;sequence>
 *         &lt;element name="plannedStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="plannedEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrenceStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="budget" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="objective" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="actualCost" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="conditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subjectConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="followOnActivities" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subjectedAssessmentResults" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="allowingAuthorizationsReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="followedOnActivityReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="requiringCondition" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" minOccurs="0"/>
 *         &lt;element name="activityOccurrenceRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrenceType_TO" minOccurs="0"/>
 *         &lt;element name="plannedEndDateTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityOccurrence_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "plannedStartDate",
    "plannedEndDate",
    "frequency",
    "externalReference",
    "status",
    "budget",
    "objective",
    "actualCost",
    "conditions",
    "subjectConditions",
    "followOnActivities",
    "subjectedAssessmentResults",
    "allowingAuthorizationsReferences",
    "followedOnActivityReferences",
    "requiringCondition",
    "activityOccurrenceRootType",
    "plannedEndDateTime",
    "alternateReference"
})
@XmlSeeAlso({
    TrainingTO.class,
    SocialActivityTO.class,
    RelayServiceTO.class,
    AssessmentActivityTO.class,
    ObjectTreatmentTO.class,
    DrivingActionTO.class,
    TaskTO.class
})
public class ActivityOccurrenceTO
    extends ActivityTO
{

    protected XMLGregorianCalendar plannedStartDate;
    protected XMLGregorianCalendar plannedEndDate;
    protected Frequency frequency;
    protected String externalReference;
    protected List<ActivityOccurrenceStatusTO> status;
    protected BaseCurrencyAmount budget;
    protected String objective;
    protected BaseCurrencyAmount actualCost;
    protected List<ConditionTO> conditions;
    protected List<ConditionTO> subjectConditions;
    protected List<ActivityOccurrenceTO> followOnActivities;
    protected List<AssessmentResultTO> subjectedAssessmentResults;
    protected List<ObjectReferenceTO> allowingAuthorizationsReferences;
    protected ObjectReferenceTO followedOnActivityReferences;
    protected ConditionTO requiringCondition;
    protected ActivityOccurrenceTypeTO activityOccurrenceRootType;
    protected XMLGregorianCalendar plannedEndDateTime;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the plannedStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlannedStartDate() {
        return plannedStartDate;
    }

    /**
     * Sets the value of the plannedStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlannedStartDate(XMLGregorianCalendar value) {
        this.plannedStartDate = value;
    }

    /**
     * Gets the value of the plannedEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlannedEndDate() {
        return plannedEndDate;
    }

    /**
     * Sets the value of the plannedEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlannedEndDate(XMLGregorianCalendar value) {
        this.plannedEndDate = value;
    }

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceStatusTO }
     * 
     * 
     */
    public List<ActivityOccurrenceStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<ActivityOccurrenceStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the budget property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getBudget() {
        return budget;
    }

    /**
     * Sets the value of the budget property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setBudget(BaseCurrencyAmount value) {
        this.budget = value;
    }

    /**
     * Gets the value of the objective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjective() {
        return objective;
    }

    /**
     * Sets the value of the objective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjective(String value) {
        this.objective = value;
    }

    /**
     * Gets the value of the actualCost property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getActualCost() {
        return actualCost;
    }

    /**
     * Sets the value of the actualCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setActualCost(BaseCurrencyAmount value) {
        this.actualCost = value;
    }

    /**
     * Gets the value of the conditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionTO }
     * 
     * 
     */
    public List<ConditionTO> getConditions() {
        if (conditions == null) {
            conditions = new ArrayList<ConditionTO>();
        }
        return this.conditions;
    }

    /**
     * Gets the value of the subjectConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subjectConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubjectConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionTO }
     * 
     * 
     */
    public List<ConditionTO> getSubjectConditions() {
        if (subjectConditions == null) {
            subjectConditions = new ArrayList<ConditionTO>();
        }
        return this.subjectConditions;
    }

    /**
     * Gets the value of the followOnActivities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the followOnActivities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFollowOnActivities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceTO }
     * 
     * 
     */
    public List<ActivityOccurrenceTO> getFollowOnActivities() {
        if (followOnActivities == null) {
            followOnActivities = new ArrayList<ActivityOccurrenceTO>();
        }
        return this.followOnActivities;
    }

    /**
     * Gets the value of the subjectedAssessmentResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subjectedAssessmentResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubjectedAssessmentResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssessmentResultTO }
     * 
     * 
     */
    public List<AssessmentResultTO> getSubjectedAssessmentResults() {
        if (subjectedAssessmentResults == null) {
            subjectedAssessmentResults = new ArrayList<AssessmentResultTO>();
        }
        return this.subjectedAssessmentResults;
    }

    /**
     * Gets the value of the allowingAuthorizationsReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allowingAuthorizationsReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllowingAuthorizationsReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getAllowingAuthorizationsReferences() {
        if (allowingAuthorizationsReferences == null) {
            allowingAuthorizationsReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.allowingAuthorizationsReferences;
    }

    /**
     * Gets the value of the followedOnActivityReferences property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getFollowedOnActivityReferences() {
        return followedOnActivityReferences;
    }

    /**
     * Sets the value of the followedOnActivityReferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setFollowedOnActivityReferences(ObjectReferenceTO value) {
        this.followedOnActivityReferences = value;
    }

    /**
     * Gets the value of the requiringCondition property.
     * 
     * @return
     *     possible object is
     *     {@link ConditionTO }
     *     
     */
    public ConditionTO getRequiringCondition() {
        return requiringCondition;
    }

    /**
     * Sets the value of the requiringCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setRequiringCondition(ConditionTO value) {
        this.requiringCondition = value;
    }

    /**
     * Gets the value of the activityOccurrenceRootType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityOccurrenceTypeTO }
     *     
     */
    public ActivityOccurrenceTypeTO getActivityOccurrenceRootType() {
        return activityOccurrenceRootType;
    }

    /**
     * Sets the value of the activityOccurrenceRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityOccurrenceTypeTO }
     *     
     */
    public void setActivityOccurrenceRootType(ActivityOccurrenceTypeTO value) {
        this.activityOccurrenceRootType = value;
    }

    /**
     * Gets the value of the plannedEndDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlannedEndDateTime() {
        return plannedEndDateTime;
    }

    /**
     * Sets the value of the plannedEndDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlannedEndDateTime(XMLGregorianCalendar value) {
        this.plannedEndDateTime = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link ActivityOccurrenceStatusTO }
     *     
     */
    public void setStatus(List<ActivityOccurrenceStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the conditions property.
     * 
     * @param conditions
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setConditions(List<ConditionTO> conditions) {
        this.conditions = conditions;
    }

    /**
     * Sets the value of the subjectConditions property.
     * 
     * @param subjectConditions
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setSubjectConditions(List<ConditionTO> subjectConditions) {
        this.subjectConditions = subjectConditions;
    }

    /**
     * Sets the value of the followOnActivities property.
     * 
     * @param followOnActivities
     *     allowed object is
     *     {@link ActivityOccurrenceTO }
     *     
     */
    public void setFollowOnActivities(List<ActivityOccurrenceTO> followOnActivities) {
        this.followOnActivities = followOnActivities;
    }

    /**
     * Sets the value of the subjectedAssessmentResults property.
     * 
     * @param subjectedAssessmentResults
     *     allowed object is
     *     {@link AssessmentResultTO }
     *     
     */
    public void setSubjectedAssessmentResults(List<AssessmentResultTO> subjectedAssessmentResults) {
        this.subjectedAssessmentResults = subjectedAssessmentResults;
    }

    /**
     * Sets the value of the allowingAuthorizationsReferences property.
     * 
     * @param allowingAuthorizationsReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setAllowingAuthorizationsReferences(List<ObjectReferenceTO> allowingAuthorizationsReferences) {
        this.allowingAuthorizationsReferences = allowingAuthorizationsReferences;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
