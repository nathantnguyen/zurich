
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FundSwitchRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FundSwitchRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialMarketRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="quantityToSell" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="quantityToBuy" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="percentageToSell" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="sourceFund" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" minOccurs="0"/>
 *         &lt;element name="targetFund" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FundSwitchRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "quantityToSell",
    "quantityToBuy",
    "percentageToSell",
    "sourceFund",
    "targetFund"
})
public class FundSwitchRequestTO
    extends FinancialMarketRequestTO
{

    protected BigInteger quantityToSell;
    protected BigInteger quantityToBuy;
    protected BigDecimal percentageToSell;
    protected FinancialAssetTO sourceFund;
    protected FinancialAssetTO targetFund;

    /**
     * Gets the value of the quantityToSell property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityToSell() {
        return quantityToSell;
    }

    /**
     * Sets the value of the quantityToSell property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityToSell(BigInteger value) {
        this.quantityToSell = value;
    }

    /**
     * Gets the value of the quantityToBuy property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityToBuy() {
        return quantityToBuy;
    }

    /**
     * Sets the value of the quantityToBuy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityToBuy(BigInteger value) {
        this.quantityToBuy = value;
    }

    /**
     * Gets the value of the percentageToSell property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentageToSell() {
        return percentageToSell;
    }

    /**
     * Sets the value of the percentageToSell property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentageToSell(BigDecimal value) {
        this.percentageToSell = value;
    }

    /**
     * Gets the value of the sourceFund property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAssetTO }
     *     
     */
    public FinancialAssetTO getSourceFund() {
        return sourceFund;
    }

    /**
     * Sets the value of the sourceFund property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setSourceFund(FinancialAssetTO value) {
        this.sourceFund = value;
    }

    /**
     * Gets the value of the targetFund property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAssetTO }
     *     
     */
    public FinancialAssetTO getTargetFund() {
        return targetFund;
    }

    /**
     * Sets the value of the targetFund property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setTargetFund(FinancialAssetTO value) {
        this.targetFund = value;
    }

}
