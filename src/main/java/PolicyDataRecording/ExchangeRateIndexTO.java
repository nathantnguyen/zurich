
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExchangeRateIndex_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExchangeRateIndex_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Index_TO">
 *       &lt;sequence>
 *         &lt;element name="targetCurrency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="exchangeBasis" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}ExchangeBasis" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangeRateIndex_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/", propOrder = {
    "targetCurrency",
    "exchangeBasis"
})
public class ExchangeRateIndexTO
    extends IndexTO
{

    protected CurrencyCode targetCurrency;
    protected ExchangeBasis exchangeBasis;

    /**
     * Gets the value of the targetCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCode }
     *     
     */
    public CurrencyCode getTargetCurrency() {
        return targetCurrency;
    }

    /**
     * Sets the value of the targetCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCode }
     *     
     */
    public void setTargetCurrency(CurrencyCode value) {
        this.targetCurrency = value;
    }

    /**
     * Gets the value of the exchangeBasis property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeBasis }
     *     
     */
    public ExchangeBasis getExchangeBasis() {
        return exchangeBasis;
    }

    /**
     * Sets the value of the exchangeBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeBasis }
     *     
     */
    public void setExchangeBasis(ExchangeBasis value) {
        this.exchangeBasis = value;
    }

}
