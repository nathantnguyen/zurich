
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createSubmissionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createSubmissionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseHeader" type="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}ResponseHeader"/>
 *         &lt;element name="submissions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRequest_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="questionnaire" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}Questionnaire_TO" minOccurs="0"/>
 *         &lt;element name="customer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Customer_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createSubmissionResponse", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/", propOrder = {
    "responseHeader",
    "submissions",
    "questionnaire",
    "customer"
})
public class CreateSubmissionResponse {

    @XmlElement(required = true)
    protected ResponseHeader responseHeader;
    protected List<FinancialServicesRequestTO> submissions;
    protected QuestionnaireTO questionnaire;
    protected CustomerTO customer;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the submissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the submissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubmissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialServicesRequestTO }
     * 
     * 
     */
    public List<FinancialServicesRequestTO> getSubmissions() {
        if (submissions == null) {
            submissions = new ArrayList<FinancialServicesRequestTO>();
        }
        return this.submissions;
    }

    /**
     * Gets the value of the questionnaire property.
     * 
     * @return
     *     possible object is
     *     {@link QuestionnaireTO }
     *     
     */
    public QuestionnaireTO getQuestionnaire() {
        return questionnaire;
    }

    /**
     * Sets the value of the questionnaire property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuestionnaireTO }
     *     
     */
    public void setQuestionnaire(QuestionnaireTO value) {
        this.questionnaire = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerTO }
     *     
     */
    public CustomerTO getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerTO }
     *     
     */
    public void setCustomer(CustomerTO value) {
        this.customer = value;
    }

    /**
     * Sets the value of the submissions property.
     * 
     * @param submissions
     *     allowed object is
     *     {@link FinancialServicesRequestTO }
     *     
     */
    public void setSubmissions(List<FinancialServicesRequestTO> submissions) {
        this.submissions = submissions;
    }

}
