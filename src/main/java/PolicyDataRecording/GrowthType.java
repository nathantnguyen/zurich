
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GrowthType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GrowthType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Arithmetic Series"/>
 *     &lt;enumeration value="Geometric Series"/>
 *     &lt;enumeration value="Level"/>
 *     &lt;enumeration value="Stepped"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GrowthType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum GrowthType {

    @XmlEnumValue("Arithmetic Series")
    ARITHMETIC_SERIES("Arithmetic Series"),
    @XmlEnumValue("Geometric Series")
    GEOMETRIC_SERIES("Geometric Series"),
    @XmlEnumValue("Level")
    LEVEL("Level"),
    @XmlEnumValue("Stepped")
    STEPPED("Stepped"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    GrowthType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GrowthType fromValue(String v) {
        for (GrowthType c: GrowthType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
