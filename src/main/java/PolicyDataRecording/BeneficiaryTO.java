
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Beneficiary_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Beneficiary_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRole_TO">
 *       &lt;sequence>
 *         &lt;element name="accepted" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="partExemptFromTaxes" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="legalWording" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="beneficiaryDesignation" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}BeneficiaryDesignation" minOccurs="0"/>
 *         &lt;element name="benefitDistributionCalculation" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}BenefitDistributionCalculation" minOccurs="0"/>
 *         &lt;element name="irrevocable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Beneficiary_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "accepted",
    "partExemptFromTaxes",
    "legalWording",
    "beneficiaryDesignation",
    "benefitDistributionCalculation",
    "irrevocable"
})
public class BeneficiaryTO
    extends FinancialServicesRoleTO
{

    protected Boolean accepted;
    protected BigDecimal partExemptFromTaxes;
    protected String legalWording;
    protected BeneficiaryDesignation beneficiaryDesignation;
    protected BenefitDistributionCalculation benefitDistributionCalculation;
    protected Boolean irrevocable;

    /**
     * Gets the value of the accepted property.
     * This getter has been renamed from isAccepted() to getAccepted() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAccepted() {
        return accepted;
    }

    /**
     * Sets the value of the accepted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccepted(Boolean value) {
        this.accepted = value;
    }

    /**
     * Gets the value of the partExemptFromTaxes property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPartExemptFromTaxes() {
        return partExemptFromTaxes;
    }

    /**
     * Sets the value of the partExemptFromTaxes property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPartExemptFromTaxes(BigDecimal value) {
        this.partExemptFromTaxes = value;
    }

    /**
     * Gets the value of the legalWording property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegalWording() {
        return legalWording;
    }

    /**
     * Sets the value of the legalWording property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegalWording(String value) {
        this.legalWording = value;
    }

    /**
     * Gets the value of the beneficiaryDesignation property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiaryDesignation }
     *     
     */
    public BeneficiaryDesignation getBeneficiaryDesignation() {
        return beneficiaryDesignation;
    }

    /**
     * Sets the value of the beneficiaryDesignation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiaryDesignation }
     *     
     */
    public void setBeneficiaryDesignation(BeneficiaryDesignation value) {
        this.beneficiaryDesignation = value;
    }

    /**
     * Gets the value of the benefitDistributionCalculation property.
     * 
     * @return
     *     possible object is
     *     {@link BenefitDistributionCalculation }
     *     
     */
    public BenefitDistributionCalculation getBenefitDistributionCalculation() {
        return benefitDistributionCalculation;
    }

    /**
     * Sets the value of the benefitDistributionCalculation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BenefitDistributionCalculation }
     *     
     */
    public void setBenefitDistributionCalculation(BenefitDistributionCalculation value) {
        this.benefitDistributionCalculation = value;
    }

    /**
     * Gets the value of the irrevocable property.
     * This getter has been renamed from isIrrevocable() to getIrrevocable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIrrevocable() {
        return irrevocable;
    }

    /**
     * Sets the value of the irrevocable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIrrevocable(Boolean value) {
        this.irrevocable = value;
    }

}
