
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditCardEndorsement.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CreditCardEndorsement">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Non Endorsed"/>
 *     &lt;enumeration value="Private"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CreditCardEndorsement", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum CreditCardEndorsement {

    @XmlEnumValue("Non Endorsed")
    NON_ENDORSED("Non Endorsed"),
    @XmlEnumValue("Private")
    PRIVATE("Private");
    private final String value;

    CreditCardEndorsement(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CreditCardEndorsement fromValue(String v) {
        for (CreditCardEndorsement c: CreditCardEndorsement.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
