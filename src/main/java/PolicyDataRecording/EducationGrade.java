
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EducationGrade.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EducationGrade">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Black Belt"/>
 *     &lt;enumeration value="Brown Belt"/>
 *     &lt;enumeration value="Honours"/>
 *     &lt;enumeration value="Magna Cum Laude"/>
 *     &lt;enumeration value="Pass"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EducationGrade", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum EducationGrade {

    @XmlEnumValue("Black Belt")
    BLACK_BELT("Black Belt"),
    @XmlEnumValue("Brown Belt")
    BROWN_BELT("Brown Belt"),
    @XmlEnumValue("Honours")
    HONOURS("Honours"),
    @XmlEnumValue("Magna Cum Laude")
    MAGNA_CUM_LAUDE("Magna Cum Laude"),
    @XmlEnumValue("Pass")
    PASS("Pass");
    private final String value;

    EducationGrade(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EducationGrade fromValue(String v) {
        for (EducationGrade c: EducationGrade.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
