
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TelephoneElectronicType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TelephoneElectronicType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Fax"/>
 *     &lt;enumeration value="Voice"/>
 *     &lt;enumeration value="Mobile"/>
 *     &lt;enumeration value="Pager"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TelephoneElectronicType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum TelephoneElectronicType {

    @XmlEnumValue("Fax")
    FAX("Fax"),
    @XmlEnumValue("Voice")
    VOICE("Voice"),
    @XmlEnumValue("Mobile")
    MOBILE("Mobile"),
    @XmlEnumValue("Pager")
    PAGER("Pager");
    private final String value;

    TelephoneElectronicType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TelephoneElectronicType fromValue(String v) {
        for (TelephoneElectronicType c: TelephoneElectronicType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
