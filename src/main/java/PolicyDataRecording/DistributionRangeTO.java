
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DistributionRange_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DistributionRange_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}MoneyProvisionDeterminer_TO">
 *       &lt;sequence>
 *         &lt;element name="binFrom" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="binTo" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="distributionInterval" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="frequencyInterval" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DistributionRange_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/", propOrder = {
    "binFrom",
    "binTo",
    "distributionInterval",
    "frequencyInterval"
})
public class DistributionRangeTO
    extends MoneyProvisionDeterminerTO
{

    protected BigDecimal binFrom;
    protected BigDecimal binTo;
    protected BigDecimal distributionInterval;
    protected String frequencyInterval;

    /**
     * Gets the value of the binFrom property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBinFrom() {
        return binFrom;
    }

    /**
     * Sets the value of the binFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBinFrom(BigDecimal value) {
        this.binFrom = value;
    }

    /**
     * Gets the value of the binTo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBinTo() {
        return binTo;
    }

    /**
     * Sets the value of the binTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBinTo(BigDecimal value) {
        this.binTo = value;
    }

    /**
     * Gets the value of the distributionInterval property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDistributionInterval() {
        return distributionInterval;
    }

    /**
     * Sets the value of the distributionInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDistributionInterval(BigDecimal value) {
        this.distributionInterval = value;
    }

    /**
     * Gets the value of the frequencyInterval property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrequencyInterval() {
        return frequencyInterval;
    }

    /**
     * Sets the value of the frequencyInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrequencyInterval(String value) {
        this.frequencyInterval = value;
    }

}
