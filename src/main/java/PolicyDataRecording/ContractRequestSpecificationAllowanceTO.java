
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractRequestSpecificationAllowance_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractRequestSpecificationAllowance_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequestSpecificationInclusion_TO">
 *       &lt;sequence>
 *         &lt;element name="fee" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="periodicity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}RequestPeriodicity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractRequestSpecificationAllowance_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "fee",
    "periodicity"
})
public class ContractRequestSpecificationAllowanceTO
    extends ContractRequestSpecificationInclusionTO
{

    protected BaseCurrencyAmount fee;
    protected RequestPeriodicity periodicity;

    /**
     * Gets the value of the fee property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getFee() {
        return fee;
    }

    /**
     * Sets the value of the fee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setFee(BaseCurrencyAmount value) {
        this.fee = value;
    }

    /**
     * Gets the value of the periodicity property.
     * 
     * @return
     *     possible object is
     *     {@link RequestPeriodicity }
     *     
     */
    public RequestPeriodicity getPeriodicity() {
        return periodicity;
    }

    /**
     * Sets the value of the periodicity property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestPeriodicity }
     *     
     */
    public void setPeriodicity(RequestPeriodicity value) {
        this.periodicity = value;
    }

}
