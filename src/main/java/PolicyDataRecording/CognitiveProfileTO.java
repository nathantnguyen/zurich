
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CognitiveProfile_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CognitiveProfile_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CommunicationProfile_TO">
 *       &lt;sequence>
 *         &lt;element name="dialogueLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}DialogueLevel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CognitiveProfile_TO", propOrder = {
    "dialogueLevel"
})
public class CognitiveProfileTO
    extends CommunicationProfileTO
{

    protected DialogueLevel dialogueLevel;

    /**
     * Gets the value of the dialogueLevel property.
     * 
     * @return
     *     possible object is
     *     {@link DialogueLevel }
     *     
     */
    public DialogueLevel getDialogueLevel() {
        return dialogueLevel;
    }

    /**
     * Sets the value of the dialogueLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link DialogueLevel }
     *     
     */
    public void setDialogueLevel(DialogueLevel value) {
        this.dialogueLevel = value;
    }

}
