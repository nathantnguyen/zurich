
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CurrencyCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CurrencyCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Gbp"/>
 *     &lt;enumeration value="Euro"/>
 *     &lt;enumeration value="Us Dollar"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CurrencyCode", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/")
@XmlEnum
public enum CurrencyCode {

    @XmlEnumValue("Gbp")
    GBP("Gbp"),
    @XmlEnumValue("Euro")
    EURO("Euro"),
    @XmlEnumValue("Us Dollar")
    US_DOLLAR("Us Dollar");
    private final String value;

    CurrencyCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CurrencyCode fromValue(String v) {
        for (CurrencyCode c: CurrencyCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
