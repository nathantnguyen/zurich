
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Employee_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Employee_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}EmploymentRole_TO">
 *       &lt;sequence>
 *         &lt;element name="jobTitle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="jobDescription" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="annualSalary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="baseSalary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="salaryGrade" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}SalaryGrade" minOccurs="0"/>
 *         &lt;element name="annualTaxableBenefit" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="bonusSalary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="skillLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}SkillLevel" minOccurs="0"/>
 *         &lt;element name="isPensionable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="employeeNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="employmentStatus" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}EmploymentStatus_TO" minOccurs="0"/>
 *         &lt;element name="workLocation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Employee_TO", propOrder = {
    "jobTitle",
    "jobDescription",
    "externalReference",
    "annualSalary",
    "baseSalary",
    "salaryGrade",
    "annualTaxableBenefit",
    "bonusSalary",
    "skillLevel",
    "isPensionable",
    "employeeNumber",
    "employmentStatus",
    "workLocation",
    "alternateReference"
})
public class EmployeeTO
    extends EmploymentRoleTO
{

    protected String jobTitle;
    protected String jobDescription;
    protected String externalReference;
    protected BaseCurrencyAmount annualSalary;
    protected BaseCurrencyAmount baseSalary;
    protected SalaryGrade salaryGrade;
    protected BaseCurrencyAmount annualTaxableBenefit;
    protected BaseCurrencyAmount bonusSalary;
    protected SkillLevel skillLevel;
    protected Boolean isPensionable;
    protected String employeeNumber;
    protected EmploymentStatusTO employmentStatus;
    protected PlaceTO workLocation;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the jobTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Sets the value of the jobTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

    /**
     * Gets the value of the jobDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobDescription() {
        return jobDescription;
    }

    /**
     * Sets the value of the jobDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobDescription(String value) {
        this.jobDescription = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the annualSalary property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAnnualSalary() {
        return annualSalary;
    }

    /**
     * Sets the value of the annualSalary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAnnualSalary(BaseCurrencyAmount value) {
        this.annualSalary = value;
    }

    /**
     * Gets the value of the baseSalary property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getBaseSalary() {
        return baseSalary;
    }

    /**
     * Sets the value of the baseSalary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setBaseSalary(BaseCurrencyAmount value) {
        this.baseSalary = value;
    }

    /**
     * Gets the value of the salaryGrade property.
     * 
     * @return
     *     possible object is
     *     {@link SalaryGrade }
     *     
     */
    public SalaryGrade getSalaryGrade() {
        return salaryGrade;
    }

    /**
     * Sets the value of the salaryGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalaryGrade }
     *     
     */
    public void setSalaryGrade(SalaryGrade value) {
        this.salaryGrade = value;
    }

    /**
     * Gets the value of the annualTaxableBenefit property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAnnualTaxableBenefit() {
        return annualTaxableBenefit;
    }

    /**
     * Sets the value of the annualTaxableBenefit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAnnualTaxableBenefit(BaseCurrencyAmount value) {
        this.annualTaxableBenefit = value;
    }

    /**
     * Gets the value of the bonusSalary property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getBonusSalary() {
        return bonusSalary;
    }

    /**
     * Sets the value of the bonusSalary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setBonusSalary(BaseCurrencyAmount value) {
        this.bonusSalary = value;
    }

    /**
     * Gets the value of the skillLevel property.
     * 
     * @return
     *     possible object is
     *     {@link SkillLevel }
     *     
     */
    public SkillLevel getSkillLevel() {
        return skillLevel;
    }

    /**
     * Sets the value of the skillLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkillLevel }
     *     
     */
    public void setSkillLevel(SkillLevel value) {
        this.skillLevel = value;
    }

    /**
     * Gets the value of the isPensionable property.
     * This getter has been renamed from isIsPensionable() to getIsPensionable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsPensionable() {
        return isPensionable;
    }

    /**
     * Sets the value of the isPensionable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPensionable(Boolean value) {
        this.isPensionable = value;
    }

    /**
     * Gets the value of the employeeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeNumber() {
        return employeeNumber;
    }

    /**
     * Sets the value of the employeeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeNumber(String value) {
        this.employeeNumber = value;
    }

    /**
     * Gets the value of the employmentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link EmploymentStatusTO }
     *     
     */
    public EmploymentStatusTO getEmploymentStatus() {
        return employmentStatus;
    }

    /**
     * Sets the value of the employmentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmploymentStatusTO }
     *     
     */
    public void setEmploymentStatus(EmploymentStatusTO value) {
        this.employmentStatus = value;
    }

    /**
     * Gets the value of the workLocation property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getWorkLocation() {
        return workLocation;
    }

    /**
     * Sets the value of the workLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setWorkLocation(PlaceTO value) {
        this.workLocation = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
