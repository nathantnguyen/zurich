
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrganisationPurpose.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganisationPurpose">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Travel Assistance"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrganisationPurpose", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum OrganisationPurpose {

    @XmlEnumValue("Travel Assistance")
    TRAVEL_ASSISTANCE("Travel Assistance");
    private final String value;

    OrganisationPurpose(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrganisationPurpose fromValue(String v) {
        for (OrganisationPurpose c: OrganisationPurpose.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
