
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SentimentRating.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SentimentRating">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Negative"/>
 *     &lt;enumeration value="Neutral"/>
 *     &lt;enumeration value="Positive"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SentimentRating", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum SentimentRating {

    @XmlEnumValue("Negative")
    NEGATIVE("Negative"),
    @XmlEnumValue("Neutral")
    NEUTRAL("Neutral"),
    @XmlEnumValue("Positive")
    POSITIVE("Positive");
    private final String value;

    SentimentRating(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SentimentRating fromValue(String v) {
        for (SentimentRating c: SentimentRating.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
