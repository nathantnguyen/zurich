
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ButtonAndIconSize.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ButtonAndIconSize">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="Large"/>
 *     &lt;enumeration value="Larger"/>
 *     &lt;enumeration value="Largest"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ButtonAndIconSize", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum ButtonAndIconSize {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Large")
    LARGE("Large"),
    @XmlEnumValue("Larger")
    LARGER("Larger"),
    @XmlEnumValue("Largest")
    LARGEST("Largest");
    private final String value;

    ButtonAndIconSize(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ButtonAndIconSize fromValue(String v) {
        for (ButtonAndIconSize c: ButtonAndIconSize.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
