
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InterestRateType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InterestRateType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Adjustable Interest Rate"/>
 *     &lt;enumeration value="Fixed Interest Rate"/>
 *     &lt;enumeration value="Indexed Interest Rate"/>
 *     &lt;enumeration value="Variable Interest Rate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InterestRateType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/")
@XmlEnum
public enum InterestRateType {

    @XmlEnumValue("Adjustable Interest Rate")
    ADJUSTABLE_INTEREST_RATE("Adjustable Interest Rate"),
    @XmlEnumValue("Fixed Interest Rate")
    FIXED_INTEREST_RATE("Fixed Interest Rate"),
    @XmlEnumValue("Indexed Interest Rate")
    INDEXED_INTEREST_RATE("Indexed Interest Rate"),
    @XmlEnumValue("Variable Interest Rate")
    VARIABLE_INTEREST_RATE("Variable Interest Rate");
    private final String value;

    InterestRateType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InterestRateType fromValue(String v) {
        for (InterestRateType c: InterestRateType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
