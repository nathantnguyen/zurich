
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeathCoverage_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeathCoverage_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CoverageComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="deathBenefitDetermination" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}DeathBenefitDetermination" minOccurs="0"/>
 *         &lt;element name="deathBenefitPaymentType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}DeathBenefitPaymentType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeathCoverage_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "deathBenefitDetermination",
    "deathBenefitPaymentType"
})
public class DeathCoverageTO
    extends CoverageComponentTO
{

    protected DeathBenefitDetermination deathBenefitDetermination;
    protected DeathBenefitPaymentType deathBenefitPaymentType;

    /**
     * Gets the value of the deathBenefitDetermination property.
     * 
     * @return
     *     possible object is
     *     {@link DeathBenefitDetermination }
     *     
     */
    public DeathBenefitDetermination getDeathBenefitDetermination() {
        return deathBenefitDetermination;
    }

    /**
     * Sets the value of the deathBenefitDetermination property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeathBenefitDetermination }
     *     
     */
    public void setDeathBenefitDetermination(DeathBenefitDetermination value) {
        this.deathBenefitDetermination = value;
    }

    /**
     * Gets the value of the deathBenefitPaymentType property.
     * 
     * @return
     *     possible object is
     *     {@link DeathBenefitPaymentType }
     *     
     */
    public DeathBenefitPaymentType getDeathBenefitPaymentType() {
        return deathBenefitPaymentType;
    }

    /**
     * Sets the value of the deathBenefitPaymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeathBenefitPaymentType }
     *     
     */
    public void setDeathBenefitPaymentType(DeathBenefitPaymentType value) {
        this.deathBenefitPaymentType = value;
    }

}
