
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FormatOfTraining.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FormatOfTraining">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Conference"/>
 *     &lt;enumeration value="E Learning"/>
 *     &lt;enumeration value="Workshop"/>
 *     &lt;enumeration value="Seminar"/>
 *     &lt;enumeration value="On The Job"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FormatOfTraining", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum FormatOfTraining {

    @XmlEnumValue("Conference")
    CONFERENCE("Conference"),
    @XmlEnumValue("E Learning")
    E_LEARNING("E Learning"),
    @XmlEnumValue("Workshop")
    WORKSHOP("Workshop"),
    @XmlEnumValue("Seminar")
    SEMINAR("Seminar"),
    @XmlEnumValue("On The Job")
    ON_THE_JOB("On The Job");
    private final String value;

    FormatOfTraining(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FormatOfTraining fromValue(String v) {
        for (FormatOfTraining c: FormatOfTraining.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
