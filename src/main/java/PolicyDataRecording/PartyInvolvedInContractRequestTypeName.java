
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyInvolvedInContractRequestTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PartyInvolvedInContractRequestTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Requester"/>
 *     &lt;enumeration value="Underwriter"/>
 *     &lt;enumeration value="Signer"/>
 *     &lt;enumeration value="Underwriting Unit"/>
 *     &lt;enumeration value="Underwriting Assistant"/>
 *     &lt;enumeration value="Business Development Leader"/>
 *     &lt;enumeration value="Prior Insurer"/>
 *     &lt;enumeration value="Candidate Driver"/>
 *     &lt;enumeration value="Request Selling Channel"/>
 *     &lt;enumeration value="Request Administrator"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PartyInvolvedInContractRequestTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum PartyInvolvedInContractRequestTypeName {

    @XmlEnumValue("Requester")
    REQUESTER("Requester"),
    @XmlEnumValue("Underwriter")
    UNDERWRITER("Underwriter"),
    @XmlEnumValue("Signer")
    SIGNER("Signer"),
    @XmlEnumValue("Underwriting Unit")
    UNDERWRITING_UNIT("Underwriting Unit"),
    @XmlEnumValue("Underwriting Assistant")
    UNDERWRITING_ASSISTANT("Underwriting Assistant"),
    @XmlEnumValue("Business Development Leader")
    BUSINESS_DEVELOPMENT_LEADER("Business Development Leader"),
    @XmlEnumValue("Prior Insurer")
    PRIOR_INSURER("Prior Insurer"),
    @XmlEnumValue("Candidate Driver")
    CANDIDATE_DRIVER("Candidate Driver"),
    @XmlEnumValue("Request Selling Channel")
    REQUEST_SELLING_CHANNEL("Request Selling Channel"),
    @XmlEnumValue("Request Administrator")
    REQUEST_ADMINISTRATOR("Request Administrator");
    private final String value;

    PartyInvolvedInContractRequestTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartyInvolvedInContractRequestTypeName fromValue(String v) {
        for (PartyInvolvedInContractRequestTypeName c: PartyInvolvedInContractRequestTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
