
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TimeOut.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TimeOut">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="Long"/>
 *     &lt;enumeration value="Longer"/>
 *     &lt;enumeration value="Longest"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TimeOut", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum TimeOut {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Long")
    LONG("Long"),
    @XmlEnumValue("Longer")
    LONGER("Longer"),
    @XmlEnumValue("Longest")
    LONGEST("Longest");
    private final String value;

    TimeOut(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TimeOut fromValue(String v) {
        for (TimeOut c: TimeOut.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
