
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StructureScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StructureScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="buildingWindClassification" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}BuildingWindClassification" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "buildingWindClassification"
})
@XmlSeeAlso({
    EarthquakeBuildingScoreTO.class,
    FloodingRiskScoreTO.class
})
public class StructureScoreTO
    extends ScoreTO
{

    protected BuildingWindClassification buildingWindClassification;

    /**
     * Gets the value of the buildingWindClassification property.
     * 
     * @return
     *     possible object is
     *     {@link BuildingWindClassification }
     *     
     */
    public BuildingWindClassification getBuildingWindClassification() {
        return buildingWindClassification;
    }

    /**
     * Sets the value of the buildingWindClassification property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildingWindClassification }
     *     
     */
    public void setBuildingWindClassification(BuildingWindClassification value) {
        this.buildingWindClassification = value;
    }

}
