
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bindPolicyRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bindPolicyRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestHeader" type="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}RequestHeader"/>
 *         &lt;element name="masterPolicy" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CommercialAgreement_TO" minOccurs="0"/>
 *         &lt;element name="submission" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRequest_TO"/>
 *         &lt;element name="commercialAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CommercialAgreement_TO" minOccurs="0"/>
 *         &lt;element name="policyNumberAutoGenerateIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean"/>
 *         &lt;element name="policyNumberAutoGenerateCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bindPolicyRequest", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/", propOrder = {
    "requestHeader",
    "masterPolicy",
    "submission",
    "commercialAgreement",
    "policyNumberAutoGenerateIndicator",
    "policyNumberAutoGenerateCount"
})
public class BindPolicyRequest {

    @XmlElement(required = true)
    protected RequestHeader requestHeader;
    protected CommercialAgreementTO masterPolicy;
    @XmlElement(required = true)
    protected FinancialServicesRequestTO submission;
    protected CommercialAgreementTO commercialAgreement;
    protected boolean policyNumberAutoGenerateIndicator;
    @XmlElement(required = true)
    protected BigInteger policyNumberAutoGenerateCount;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the masterPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialAgreementTO }
     *     
     */
    public CommercialAgreementTO getMasterPolicy() {
        return masterPolicy;
    }

    /**
     * Sets the value of the masterPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialAgreementTO }
     *     
     */
    public void setMasterPolicy(CommercialAgreementTO value) {
        this.masterPolicy = value;
    }

    /**
     * Gets the value of the submission property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesRequestTO }
     *     
     */
    public FinancialServicesRequestTO getSubmission() {
        return submission;
    }

    /**
     * Sets the value of the submission property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesRequestTO }
     *     
     */
    public void setSubmission(FinancialServicesRequestTO value) {
        this.submission = value;
    }

    /**
     * Gets the value of the commercialAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialAgreementTO }
     *     
     */
    public CommercialAgreementTO getCommercialAgreement() {
        return commercialAgreement;
    }

    /**
     * Sets the value of the commercialAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialAgreementTO }
     *     
     */
    public void setCommercialAgreement(CommercialAgreementTO value) {
        this.commercialAgreement = value;
    }

    /**
     * Gets the value of the policyNumberAutoGenerateIndicator property.
     * This getter has been renamed from isPolicyNumberAutoGenerateIndicator() to getPolicyNumberAutoGenerateIndicator() by cxf-xjc-boolean plugin.
     * 
     */
    public boolean getPolicyNumberAutoGenerateIndicator() {
        return policyNumberAutoGenerateIndicator;
    }

    /**
     * Sets the value of the policyNumberAutoGenerateIndicator property.
     * 
     */
    public void setPolicyNumberAutoGenerateIndicator(boolean value) {
        this.policyNumberAutoGenerateIndicator = value;
    }

    /**
     * Gets the value of the policyNumberAutoGenerateCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPolicyNumberAutoGenerateCount() {
        return policyNumberAutoGenerateCount;
    }

    /**
     * Sets the value of the policyNumberAutoGenerateCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPolicyNumberAutoGenerateCount(BigInteger value) {
        this.policyNumberAutoGenerateCount = value;
    }

}
