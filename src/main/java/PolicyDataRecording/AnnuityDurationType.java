
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AnnuityDurationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AnnuityDurationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Greater Of Period Certain And Life"/>
 *     &lt;enumeration value="Lesses Of Period Certain Or Life"/>
 *     &lt;enumeration value="Life Only"/>
 *     &lt;enumeration value="Life Plus Lump Sum"/>
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="Period Certain Only"/>
 *     &lt;enumeration value="Unknown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AnnuityDurationType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum AnnuityDurationType {

    @XmlEnumValue("Greater Of Period Certain And Life")
    GREATER_OF_PERIOD_CERTAIN_AND_LIFE("Greater Of Period Certain And Life"),
    @XmlEnumValue("Lesses Of Period Certain Or Life")
    LESSES_OF_PERIOD_CERTAIN_OR_LIFE("Lesses Of Period Certain Or Life"),
    @XmlEnumValue("Life Only")
    LIFE_ONLY("Life Only"),
    @XmlEnumValue("Life Plus Lump Sum")
    LIFE_PLUS_LUMP_SUM("Life Plus Lump Sum"),
    @XmlEnumValue("Other")
    OTHER("Other"),
    @XmlEnumValue("Period Certain Only")
    PERIOD_CERTAIN_ONLY("Period Certain Only"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    AnnuityDurationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AnnuityDurationType fromValue(String v) {
        for (AnnuityDurationType c: AnnuityDurationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
