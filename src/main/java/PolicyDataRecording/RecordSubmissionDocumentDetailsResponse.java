
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for recordSubmissionDocumentDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="recordSubmissionDocumentDetailsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseHeader" type="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}ResponseHeader"/>
 *         &lt;element name="electronicDocument" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}ElectronicDocument_TO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "recordSubmissionDocumentDetailsResponse", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/", propOrder = {
    "responseHeader",
    "electronicDocument"
})
public class RecordSubmissionDocumentDetailsResponse {

    @XmlElement(required = true)
    protected ResponseHeader responseHeader;
    @XmlElement(required = true)
    protected ElectronicDocumentTO electronicDocument;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the electronicDocument property.
     * 
     * @return
     *     possible object is
     *     {@link ElectronicDocumentTO }
     *     
     */
    public ElectronicDocumentTO getElectronicDocument() {
        return electronicDocument;
    }

    /**
     * Sets the value of the electronicDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElectronicDocumentTO }
     *     
     */
    public void setElectronicDocument(ElectronicDocumentTO value) {
        this.electronicDocument = value;
    }

}
