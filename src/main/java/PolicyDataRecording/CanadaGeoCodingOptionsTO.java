
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CanadaGeoCodingOptions_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CanadaGeoCodingOptions_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeoCodingOptions_TO">
 *       &lt;sequence>
 *         &lt;element name="geoCodeLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}GeoCodeLevel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CanadaGeoCodingOptions_TO", propOrder = {
    "geoCodeLevel"
})
public class CanadaGeoCodingOptionsTO
    extends GeoCodingOptionsTO
{

    protected GeoCodeLevel geoCodeLevel;

    /**
     * Gets the value of the geoCodeLevel property.
     * 
     * @return
     *     possible object is
     *     {@link GeoCodeLevel }
     *     
     */
    public GeoCodeLevel getGeoCodeLevel() {
        return geoCodeLevel;
    }

    /**
     * Sets the value of the geoCodeLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeoCodeLevel }
     *     
     */
    public void setGeoCodeLevel(GeoCodeLevel value) {
        this.geoCodeLevel = value;
    }

}
