
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardTextSpecificationLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StandardTextSpecificationLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Fixed Text"/>
 *     &lt;enumeration value="Variable Text"/>
 *     &lt;enumeration value="Document"/>
 *     &lt;enumeration value="Chapter"/>
 *     &lt;enumeration value="Paragraph"/>
 *     &lt;enumeration value="Section"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StandardTextSpecificationLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum StandardTextSpecificationLevel {

    @XmlEnumValue("Fixed Text")
    FIXED_TEXT("Fixed Text"),
    @XmlEnumValue("Variable Text")
    VARIABLE_TEXT("Variable Text"),
    @XmlEnumValue("Document")
    DOCUMENT("Document"),
    @XmlEnumValue("Chapter")
    CHAPTER("Chapter"),
    @XmlEnumValue("Paragraph")
    PARAGRAPH("Paragraph"),
    @XmlEnumValue("Section")
    SECTION("Section");
    private final String value;

    StandardTextSpecificationLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StandardTextSpecificationLevel fromValue(String v) {
        for (StandardTextSpecificationLevel c: StandardTextSpecificationLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
