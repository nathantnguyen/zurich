
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoverageComponentTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CoverageComponentTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Liability Coverage"/>
 *     &lt;enumeration value="Unemployment Coverage"/>
 *     &lt;enumeration value="Damage Coverage"/>
 *     &lt;enumeration value="Coverage Option"/>
 *     &lt;enumeration value="Legal Defence Coverage"/>
 *     &lt;enumeration value="Health Coverage"/>
 *     &lt;enumeration value="Loss Of Use Coverage"/>
 *     &lt;enumeration value="Natural Loss Coverage"/>
 *     &lt;enumeration value="Deficiency Coverage"/>
 *     &lt;enumeration value="Demolition Coverage"/>
 *     &lt;enumeration value="Foreign Travel Health Coverage"/>
 *     &lt;enumeration value="Life Annuity Coverage"/>
 *     &lt;enumeration value="Hospital Indemnity Coverage"/>
 *     &lt;enumeration value="Additional Living Expense Coverage"/>
 *     &lt;enumeration value="Glass Coverage"/>
 *     &lt;enumeration value="Assistance Coverage"/>
 *     &lt;enumeration value="Installment Refund Death Coverage"/>
 *     &lt;enumeration value="Sick Day Coverage"/>
 *     &lt;enumeration value="Employer Liability Coverage"/>
 *     &lt;enumeration value="Joint Liability Coverage"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CoverageComponentTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum CoverageComponentTypeName {

    @XmlEnumValue("Liability Coverage")
    LIABILITY_COVERAGE("Liability Coverage"),
    @XmlEnumValue("Unemployment Coverage")
    UNEMPLOYMENT_COVERAGE("Unemployment Coverage"),
    @XmlEnumValue("Damage Coverage")
    DAMAGE_COVERAGE("Damage Coverage"),
    @XmlEnumValue("Coverage Option")
    COVERAGE_OPTION("Coverage Option"),
    @XmlEnumValue("Legal Defence Coverage")
    LEGAL_DEFENCE_COVERAGE("Legal Defence Coverage"),
    @XmlEnumValue("Health Coverage")
    HEALTH_COVERAGE("Health Coverage"),
    @XmlEnumValue("Loss Of Use Coverage")
    LOSS_OF_USE_COVERAGE("Loss Of Use Coverage"),
    @XmlEnumValue("Natural Loss Coverage")
    NATURAL_LOSS_COVERAGE("Natural Loss Coverage"),
    @XmlEnumValue("Deficiency Coverage")
    DEFICIENCY_COVERAGE("Deficiency Coverage"),
    @XmlEnumValue("Demolition Coverage")
    DEMOLITION_COVERAGE("Demolition Coverage"),
    @XmlEnumValue("Foreign Travel Health Coverage")
    FOREIGN_TRAVEL_HEALTH_COVERAGE("Foreign Travel Health Coverage"),
    @XmlEnumValue("Life Annuity Coverage")
    LIFE_ANNUITY_COVERAGE("Life Annuity Coverage"),
    @XmlEnumValue("Hospital Indemnity Coverage")
    HOSPITAL_INDEMNITY_COVERAGE("Hospital Indemnity Coverage"),
    @XmlEnumValue("Additional Living Expense Coverage")
    ADDITIONAL_LIVING_EXPENSE_COVERAGE("Additional Living Expense Coverage"),
    @XmlEnumValue("Glass Coverage")
    GLASS_COVERAGE("Glass Coverage"),
    @XmlEnumValue("Assistance Coverage")
    ASSISTANCE_COVERAGE("Assistance Coverage"),
    @XmlEnumValue("Installment Refund Death Coverage")
    INSTALLMENT_REFUND_DEATH_COVERAGE("Installment Refund Death Coverage"),
    @XmlEnumValue("Sick Day Coverage")
    SICK_DAY_COVERAGE("Sick Day Coverage"),
    @XmlEnumValue("Employer Liability Coverage")
    EMPLOYER_LIABILITY_COVERAGE("Employer Liability Coverage"),
    @XmlEnumValue("Joint Liability Coverage")
    JOINT_LIABILITY_COVERAGE("Joint Liability Coverage");
    private final String value;

    CoverageComponentTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CoverageComponentTypeName fromValue(String v) {
        for (CoverageComponentTypeName c: CoverageComponentTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
