
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnitisedStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UnitisedStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Non-unitised Component"/>
 *     &lt;enumeration value="Unitised With-profit Component (uwf)"/>
 *     &lt;enumeration value="Unit-linked Component"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UnitisedStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum UnitisedStatus {

    @XmlEnumValue("Non-unitised Component")
    NON_UNITISED_COMPONENT("Non-unitised Component"),
    @XmlEnumValue("Unitised With-profit Component (uwf)")
    UNITISED_WITH_PROFIT_COMPONENT_UWF("Unitised With-profit Component (uwf)"),
    @XmlEnumValue("Unit-linked Component")
    UNIT_LINKED_COMPONENT("Unit-linked Component");
    private final String value;

    UnitisedStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnitisedStatus fromValue(String v) {
        for (UnitisedStatus c: UnitisedStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
