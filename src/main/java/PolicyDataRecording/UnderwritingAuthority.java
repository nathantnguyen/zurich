
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnderwritingAuthority.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UnderwritingAuthority">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Limited Authority"/>
 *     &lt;enumeration value="No Restrictions"/>
 *     &lt;enumeration value="Preferred Underwriter"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UnderwritingAuthority", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/")
@XmlEnum
public enum UnderwritingAuthority {

    @XmlEnumValue("Limited Authority")
    LIMITED_AUTHORITY("Limited Authority"),
    @XmlEnumValue("No Restrictions")
    NO_RESTRICTIONS("No Restrictions"),
    @XmlEnumValue("Preferred Underwriter")
    PREFERRED_UNDERWRITER("Preferred Underwriter");
    private final String value;

    UnderwritingAuthority(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnderwritingAuthority fromValue(String v) {
        for (UnderwritingAuthority c: UnderwritingAuthority.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
