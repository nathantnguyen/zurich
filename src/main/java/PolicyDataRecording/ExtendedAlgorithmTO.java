
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExtendedAlgorithm_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtendedAlgorithm_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Algorithm_TO">
 *       &lt;sequence>
 *         &lt;element name="implementationPoint" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtendedAlgorithm_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "implementationPoint"
})
public class ExtendedAlgorithmTO
    extends AlgorithmTO
{

    protected String implementationPoint;

    /**
     * Gets the value of the implementationPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImplementationPoint() {
        return implementationPoint;
    }

    /**
     * Sets the value of the implementationPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImplementationPoint(String value) {
        this.implementationPoint = value;
    }

}
