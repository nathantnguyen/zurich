
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompanyRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompanyRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="corporateLinkageLevel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="parentCompanyReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="regionalCompanyReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="topLevelCompanyReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompanyRegistration_TO", propOrder = {
    "corporateLinkageLevel",
    "parentCompanyReference",
    "regionalCompanyReference",
    "topLevelCompanyReference"
})
@XmlSeeAlso({
    FinancialServicesCompanyRegistrationTO.class
})
public class CompanyRegistrationTO
    extends PartyRegistrationTO
{

    protected String corporateLinkageLevel;
    protected String parentCompanyReference;
    protected String regionalCompanyReference;
    protected String topLevelCompanyReference;

    /**
     * Gets the value of the corporateLinkageLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporateLinkageLevel() {
        return corporateLinkageLevel;
    }

    /**
     * Sets the value of the corporateLinkageLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporateLinkageLevel(String value) {
        this.corporateLinkageLevel = value;
    }

    /**
     * Gets the value of the parentCompanyReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentCompanyReference() {
        return parentCompanyReference;
    }

    /**
     * Sets the value of the parentCompanyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentCompanyReference(String value) {
        this.parentCompanyReference = value;
    }

    /**
     * Gets the value of the regionalCompanyReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionalCompanyReference() {
        return regionalCompanyReference;
    }

    /**
     * Sets the value of the regionalCompanyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionalCompanyReference(String value) {
        this.regionalCompanyReference = value;
    }

    /**
     * Gets the value of the topLevelCompanyReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTopLevelCompanyReference() {
        return topLevelCompanyReference;
    }

    /**
     * Sets the value of the topLevelCompanyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTopLevelCompanyReference(String value) {
        this.topLevelCompanyReference = value;
    }

}
