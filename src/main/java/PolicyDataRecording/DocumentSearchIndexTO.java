
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentSearchIndex_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentSearchIndex_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="conformanceType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="isOptional" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentSearchIndex_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "conformanceType",
    "name",
    "value",
    "isOptional"
})
public class DocumentSearchIndexTO
    extends DependentObjectTO
{

    protected String conformanceType;
    protected String name;
    protected Object value;
    protected Boolean isOptional;

    /**
     * Gets the value of the conformanceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConformanceType() {
        return conformanceType;
    }

    /**
     * Sets the value of the conformanceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConformanceType(String value) {
        this.conformanceType = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Gets the value of the isOptional property.
     * This getter has been renamed from isIsOptional() to getIsOptional() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOptional() {
        return isOptional;
    }

    /**
     * Sets the value of the isOptional property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOptional(Boolean value) {
        this.isOptional = value;
    }

}
