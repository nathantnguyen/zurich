
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PartyRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Registration_TO">
 *       &lt;sequence>
 *         &lt;element name="registeredPartyNames" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyName_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="registeringAuthority" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="lastUsedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="lastVerifiedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="countryOfIssue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="partyRegistrationRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistrationType_TO" minOccurs="0"/>
 *         &lt;element name="score" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyRegistration_TO", propOrder = {
    "registeredPartyNames",
    "registeringAuthority",
    "lastUsedDate",
    "lastVerifiedDate",
    "countryOfIssue",
    "partyRegistrationRootType",
    "score"
})
@XmlSeeAlso({
    AgentLicenseTO.class,
    DrivingLicenseTO.class,
    CompanyRegistrationTO.class,
    DeathCertificateTO.class,
    BirthCertificateTO.class,
    HealthCareProviderRegistrationTO.class,
    NationalRegistrationTO.class,
    MarriageRegistrationTO.class
})
public class PartyRegistrationTO
    extends RegistrationTO
{

    protected List<PartyNameTO> registeredPartyNames;
    protected RolePlayerTO registeringAuthority;
    protected XMLGregorianCalendar lastUsedDate;
    protected XMLGregorianCalendar lastVerifiedDate;
    protected String countryOfIssue;
    protected PartyRegistrationTypeTO partyRegistrationRootType;
    protected ScoreTO score;

    /**
     * Gets the value of the registeredPartyNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the registeredPartyNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegisteredPartyNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyNameTO }
     * 
     * 
     */
    public List<PartyNameTO> getRegisteredPartyNames() {
        if (registeredPartyNames == null) {
            registeredPartyNames = new ArrayList<PartyNameTO>();
        }
        return this.registeredPartyNames;
    }

    /**
     * Gets the value of the registeringAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getRegisteringAuthority() {
        return registeringAuthority;
    }

    /**
     * Sets the value of the registeringAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setRegisteringAuthority(RolePlayerTO value) {
        this.registeringAuthority = value;
    }

    /**
     * Gets the value of the lastUsedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUsedDate() {
        return lastUsedDate;
    }

    /**
     * Sets the value of the lastUsedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUsedDate(XMLGregorianCalendar value) {
        this.lastUsedDate = value;
    }

    /**
     * Gets the value of the lastVerifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastVerifiedDate() {
        return lastVerifiedDate;
    }

    /**
     * Sets the value of the lastVerifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastVerifiedDate(XMLGregorianCalendar value) {
        this.lastVerifiedDate = value;
    }

    /**
     * Gets the value of the countryOfIssue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfIssue() {
        return countryOfIssue;
    }

    /**
     * Sets the value of the countryOfIssue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfIssue(String value) {
        this.countryOfIssue = value;
    }

    /**
     * Gets the value of the partyRegistrationRootType property.
     * 
     * @return
     *     possible object is
     *     {@link PartyRegistrationTypeTO }
     *     
     */
    public PartyRegistrationTypeTO getPartyRegistrationRootType() {
        return partyRegistrationRootType;
    }

    /**
     * Sets the value of the partyRegistrationRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyRegistrationTypeTO }
     *     
     */
    public void setPartyRegistrationRootType(PartyRegistrationTypeTO value) {
        this.partyRegistrationRootType = value;
    }

    /**
     * Gets the value of the score property.
     * 
     * @return
     *     possible object is
     *     {@link ScoreTO }
     *     
     */
    public ScoreTO getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScoreTO }
     *     
     */
    public void setScore(ScoreTO value) {
        this.score = value;
    }

    /**
     * Sets the value of the registeredPartyNames property.
     * 
     * @param registeredPartyNames
     *     allowed object is
     *     {@link PartyNameTO }
     *     
     */
    public void setRegisteredPartyNames(List<PartyNameTO> registeredPartyNames) {
        this.registeredPartyNames = registeredPartyNames;
    }

}
