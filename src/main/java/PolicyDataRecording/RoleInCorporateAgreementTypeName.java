
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInCorporateAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInCorporateAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Task Conditions"/>
 *     &lt;enumeration value="Product Scope"/>
 *     &lt;enumeration value="Subject Scope"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInCorporateAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum RoleInCorporateAgreementTypeName {

    @XmlEnumValue("Task Conditions")
    TASK_CONDITIONS("Task Conditions"),
    @XmlEnumValue("Product Scope")
    PRODUCT_SCOPE("Product Scope"),
    @XmlEnumValue("Subject Scope")
    SUBJECT_SCOPE("Subject Scope");
    private final String value;

    RoleInCorporateAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInCorporateAgreementTypeName fromValue(String v) {
        for (RoleInCorporateAgreementTypeName c: RoleInCorporateAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
