
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Organisation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Organisation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Party_TO">
 *       &lt;sequence>
 *         &lt;element name="numberOfMembers" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="dissolutionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="foundationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="numberOfEmployees" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="numberOfOwners" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="accountingPeriodStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="accountingPeriodEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="subOrganisation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="parentOrganisation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}OrganisationStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="legalEntity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="industries" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Industry_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="brand" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="organisationLevel" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}OrganisationLevel_TO" minOccurs="0"/>
 *         &lt;element name="organisationHierarchyPath" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="numberOfLocalEmployees" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="numberOfEmployeesAsString" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Organisation_TO", propOrder = {
    "numberOfMembers",
    "dissolutionDate",
    "foundationDate",
    "numberOfEmployees",
    "numberOfOwners",
    "accountingPeriodStartDate",
    "accountingPeriodEndDate",
    "subOrganisation",
    "parentOrganisation",
    "status",
    "legalEntity",
    "industries",
    "brand",
    "organisationLevel",
    "organisationHierarchyPath",
    "numberOfLocalEmployees",
    "numberOfEmployeesAsString"
})
@XmlSeeAlso({
    PublicSafetyTO.class,
    OrganisationUnitTO.class,
    HouseholdTO.class,
    AssociationTO.class,
    CompanyTO.class,
    MarketTO.class,
    AffinityGroupTO.class
})
public class OrganisationTO
    extends PartyTO
{

    protected BigInteger numberOfMembers;
    protected XMLGregorianCalendar dissolutionDate;
    protected XMLGregorianCalendar foundationDate;
    protected BigInteger numberOfEmployees;
    protected BigInteger numberOfOwners;
    protected XMLGregorianCalendar accountingPeriodStartDate;
    protected XMLGregorianCalendar accountingPeriodEndDate;
    protected List<OrganisationTO> subOrganisation;
    protected List<OrganisationTO> parentOrganisation;
    protected List<OrganisationStatusTO> status;
    protected Boolean legalEntity;
    protected List<IndustryTO> industries;
    protected String brand;
    protected OrganisationLevelTO organisationLevel;
    protected String organisationHierarchyPath;
    protected BigInteger numberOfLocalEmployees;
    protected String numberOfEmployeesAsString;

    /**
     * Gets the value of the numberOfMembers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfMembers() {
        return numberOfMembers;
    }

    /**
     * Sets the value of the numberOfMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfMembers(BigInteger value) {
        this.numberOfMembers = value;
    }

    /**
     * Gets the value of the dissolutionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDissolutionDate() {
        return dissolutionDate;
    }

    /**
     * Sets the value of the dissolutionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDissolutionDate(XMLGregorianCalendar value) {
        this.dissolutionDate = value;
    }

    /**
     * Gets the value of the foundationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFoundationDate() {
        return foundationDate;
    }

    /**
     * Sets the value of the foundationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFoundationDate(XMLGregorianCalendar value) {
        this.foundationDate = value;
    }

    /**
     * Gets the value of the numberOfEmployees property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfEmployees() {
        return numberOfEmployees;
    }

    /**
     * Sets the value of the numberOfEmployees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfEmployees(BigInteger value) {
        this.numberOfEmployees = value;
    }

    /**
     * Gets the value of the numberOfOwners property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfOwners() {
        return numberOfOwners;
    }

    /**
     * Sets the value of the numberOfOwners property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfOwners(BigInteger value) {
        this.numberOfOwners = value;
    }

    /**
     * Gets the value of the accountingPeriodStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAccountingPeriodStartDate() {
        return accountingPeriodStartDate;
    }

    /**
     * Sets the value of the accountingPeriodStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAccountingPeriodStartDate(XMLGregorianCalendar value) {
        this.accountingPeriodStartDate = value;
    }

    /**
     * Gets the value of the accountingPeriodEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAccountingPeriodEndDate() {
        return accountingPeriodEndDate;
    }

    /**
     * Sets the value of the accountingPeriodEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAccountingPeriodEndDate(XMLGregorianCalendar value) {
        this.accountingPeriodEndDate = value;
    }

    /**
     * Gets the value of the subOrganisation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subOrganisation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubOrganisation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrganisationTO }
     * 
     * 
     */
    public List<OrganisationTO> getSubOrganisation() {
        if (subOrganisation == null) {
            subOrganisation = new ArrayList<OrganisationTO>();
        }
        return this.subOrganisation;
    }

    /**
     * Gets the value of the parentOrganisation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parentOrganisation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParentOrganisation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrganisationTO }
     * 
     * 
     */
    public List<OrganisationTO> getParentOrganisation() {
        if (parentOrganisation == null) {
            parentOrganisation = new ArrayList<OrganisationTO>();
        }
        return this.parentOrganisation;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrganisationStatusTO }
     * 
     * 
     */
    public List<OrganisationStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<OrganisationStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the legalEntity property.
     * This getter has been renamed from isLegalEntity() to getLegalEntity() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLegalEntity() {
        return legalEntity;
    }

    /**
     * Sets the value of the legalEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLegalEntity(Boolean value) {
        this.legalEntity = value;
    }

    /**
     * Gets the value of the industries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the industries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIndustries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IndustryTO }
     * 
     * 
     */
    public List<IndustryTO> getIndustries() {
        if (industries == null) {
            industries = new ArrayList<IndustryTO>();
        }
        return this.industries;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the organisationLevel property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationLevelTO }
     *     
     */
    public OrganisationLevelTO getOrganisationLevel() {
        return organisationLevel;
    }

    /**
     * Sets the value of the organisationLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationLevelTO }
     *     
     */
    public void setOrganisationLevel(OrganisationLevelTO value) {
        this.organisationLevel = value;
    }

    /**
     * Gets the value of the organisationHierarchyPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganisationHierarchyPath() {
        return organisationHierarchyPath;
    }

    /**
     * Sets the value of the organisationHierarchyPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganisationHierarchyPath(String value) {
        this.organisationHierarchyPath = value;
    }

    /**
     * Gets the value of the numberOfLocalEmployees property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfLocalEmployees() {
        return numberOfLocalEmployees;
    }

    /**
     * Sets the value of the numberOfLocalEmployees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfLocalEmployees(BigInteger value) {
        this.numberOfLocalEmployees = value;
    }

    /**
     * Gets the value of the numberOfEmployeesAsString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfEmployeesAsString() {
        return numberOfEmployeesAsString;
    }

    /**
     * Sets the value of the numberOfEmployeesAsString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfEmployeesAsString(String value) {
        this.numberOfEmployeesAsString = value;
    }

    /**
     * Sets the value of the subOrganisation property.
     * 
     * @param subOrganisation
     *     allowed object is
     *     {@link OrganisationTO }
     *     
     */
    public void setSubOrganisation(List<OrganisationTO> subOrganisation) {
        this.subOrganisation = subOrganisation;
    }

    /**
     * Sets the value of the parentOrganisation property.
     * 
     * @param parentOrganisation
     *     allowed object is
     *     {@link OrganisationTO }
     *     
     */
    public void setParentOrganisation(List<OrganisationTO> parentOrganisation) {
        this.parentOrganisation = parentOrganisation;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link OrganisationStatusTO }
     *     
     */
    public void setStatus(List<OrganisationStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the industries property.
     * 
     * @param industries
     *     allowed object is
     *     {@link IndustryTO }
     *     
     */
    public void setIndustries(List<IndustryTO> industries) {
        this.industries = industries;
    }

}
