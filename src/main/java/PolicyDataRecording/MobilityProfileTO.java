
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for MobilityProfile_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MobilityProfile_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CommunicationProfile_TO">
 *       &lt;sequence>
 *         &lt;element name="buttonAndIconSize" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}ButtonAndIconSize" minOccurs="0"/>
 *         &lt;element name="houseVisit" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="keyboardInput" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="timeOut" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}TimeOut" minOccurs="0"/>
 *         &lt;element name="timeOutValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MobilityProfile_TO", propOrder = {
    "buttonAndIconSize",
    "houseVisit",
    "keyboardInput",
    "timeOut",
    "timeOutValue"
})
public class MobilityProfileTO
    extends CommunicationProfileTO
{

    protected ButtonAndIconSize buttonAndIconSize;
    protected Boolean houseVisit;
    protected Boolean keyboardInput;
    protected TimeOut timeOut;
    protected Duration timeOutValue;

    /**
     * Gets the value of the buttonAndIconSize property.
     * 
     * @return
     *     possible object is
     *     {@link ButtonAndIconSize }
     *     
     */
    public ButtonAndIconSize getButtonAndIconSize() {
        return buttonAndIconSize;
    }

    /**
     * Sets the value of the buttonAndIconSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link ButtonAndIconSize }
     *     
     */
    public void setButtonAndIconSize(ButtonAndIconSize value) {
        this.buttonAndIconSize = value;
    }

    /**
     * Gets the value of the houseVisit property.
     * This getter has been renamed from isHouseVisit() to getHouseVisit() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHouseVisit() {
        return houseVisit;
    }

    /**
     * Sets the value of the houseVisit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHouseVisit(Boolean value) {
        this.houseVisit = value;
    }

    /**
     * Gets the value of the keyboardInput property.
     * This getter has been renamed from isKeyboardInput() to getKeyboardInput() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getKeyboardInput() {
        return keyboardInput;
    }

    /**
     * Sets the value of the keyboardInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKeyboardInput(Boolean value) {
        this.keyboardInput = value;
    }

    /**
     * Gets the value of the timeOut property.
     * 
     * @return
     *     possible object is
     *     {@link TimeOut }
     *     
     */
    public TimeOut getTimeOut() {
        return timeOut;
    }

    /**
     * Sets the value of the timeOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeOut }
     *     
     */
    public void setTimeOut(TimeOut value) {
        this.timeOut = value;
    }

    /**
     * Gets the value of the timeOutValue property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getTimeOutValue() {
        return timeOutValue;
    }

    /**
     * Sets the value of the timeOutValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setTimeOutValue(Duration value) {
        this.timeOutValue = value;
    }

}
