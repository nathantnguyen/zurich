
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConditionTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ConditionTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Place Condition"/>
 *     &lt;enumeration value="Social Activity Condition"/>
 *     &lt;enumeration value="Claim Fraud Condition"/>
 *     &lt;enumeration value="Vehicle Damage"/>
 *     &lt;enumeration value="Housekeeping Condition"/>
 *     &lt;enumeration value="Lifestyle Activity Violation"/>
 *     &lt;enumeration value="Permanent Partial Disability"/>
 *     &lt;enumeration value="Permanent Total Disability"/>
 *     &lt;enumeration value="Temporary Total Disability"/>
 *     &lt;enumeration value="Temporary Partial Disability"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ConditionTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum ConditionTypeName {

    @XmlEnumValue("Place Condition")
    PLACE_CONDITION("Place Condition"),
    @XmlEnumValue("Social Activity Condition")
    SOCIAL_ACTIVITY_CONDITION("Social Activity Condition"),
    @XmlEnumValue("Claim Fraud Condition")
    CLAIM_FRAUD_CONDITION("Claim Fraud Condition"),
    @XmlEnumValue("Vehicle Damage")
    VEHICLE_DAMAGE("Vehicle Damage"),
    @XmlEnumValue("Housekeeping Condition")
    HOUSEKEEPING_CONDITION("Housekeeping Condition"),
    @XmlEnumValue("Lifestyle Activity Violation")
    LIFESTYLE_ACTIVITY_VIOLATION("Lifestyle Activity Violation"),
    @XmlEnumValue("Permanent Partial Disability")
    PERMANENT_PARTIAL_DISABILITY("Permanent Partial Disability"),
    @XmlEnumValue("Permanent Total Disability")
    PERMANENT_TOTAL_DISABILITY("Permanent Total Disability"),
    @XmlEnumValue("Temporary Total Disability")
    TEMPORARY_TOTAL_DISABILITY("Temporary Total Disability"),
    @XmlEnumValue("Temporary Partial Disability")
    TEMPORARY_PARTIAL_DISABILITY("Temporary Partial Disability");
    private final String value;

    ConditionTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ConditionTypeName fromValue(String v) {
        for (ConditionTypeName c: ConditionTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
