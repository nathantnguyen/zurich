
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaskSpecificationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TaskSpecificationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Procedure"/>
 *     &lt;enumeration value="Internal Audit Procedure"/>
 *     &lt;enumeration value="Internal Control Procedure"/>
 *     &lt;enumeration value="System"/>
 *     &lt;enumeration value="Internal Control System"/>
 *     &lt;enumeration value="System Of Governance"/>
 *     &lt;enumeration value="Business Activity Specification"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TaskSpecificationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/UtilityComponents/TaskManager/TaskManagerEnumerationsAndStates/")
@XmlEnum
public enum TaskSpecificationTypeName {

    @XmlEnumValue("Procedure")
    PROCEDURE("Procedure"),
    @XmlEnumValue("Internal Audit Procedure")
    INTERNAL_AUDIT_PROCEDURE("Internal Audit Procedure"),
    @XmlEnumValue("Internal Control Procedure")
    INTERNAL_CONTROL_PROCEDURE("Internal Control Procedure"),
    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Internal Control System")
    INTERNAL_CONTROL_SYSTEM("Internal Control System"),
    @XmlEnumValue("System Of Governance")
    SYSTEM_OF_GOVERNANCE("System Of Governance"),
    @XmlEnumValue("Business Activity Specification")
    BUSINESS_ACTIVITY_SPECIFICATION("Business Activity Specification");
    private final String value;

    TaskSpecificationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TaskSpecificationTypeName fromValue(String v) {
        for (TaskSpecificationTypeName c: TaskSpecificationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
