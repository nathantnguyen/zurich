
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactPreferencePurpose.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContactPreferencePurpose">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Billing"/>
 *     &lt;enumeration value="Advertising"/>
 *     &lt;enumeration value="Gifts"/>
 *     &lt;enumeration value="Mailing"/>
 *     &lt;enumeration value="Marketing"/>
 *     &lt;enumeration value="Delivery"/>
 *     &lt;enumeration value="General"/>
 *     &lt;enumeration value="Claims"/>
 *     &lt;enumeration value="Contract"/>
 *     &lt;enumeration value="Communication"/>
 *     &lt;enumeration value="Contact"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContactPreferencePurpose", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum ContactPreferencePurpose {

    @XmlEnumValue("Billing")
    BILLING("Billing"),
    @XmlEnumValue("Advertising")
    ADVERTISING("Advertising"),
    @XmlEnumValue("Gifts")
    GIFTS("Gifts"),
    @XmlEnumValue("Mailing")
    MAILING("Mailing"),
    @XmlEnumValue("Marketing")
    MARKETING("Marketing"),
    @XmlEnumValue("Delivery")
    DELIVERY("Delivery"),
    @XmlEnumValue("General")
    GENERAL("General"),
    @XmlEnumValue("Claims")
    CLAIMS("Claims"),
    @XmlEnumValue("Contract")
    CONTRACT("Contract"),
    @XmlEnumValue("Communication")
    COMMUNICATION("Communication"),
    @XmlEnumValue("Contact")
    CONTACT("Contact");
    private final String value;

    ContactPreferencePurpose(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContactPreferencePurpose fromValue(String v) {
        for (ContactPreferencePurpose c: ContactPreferencePurpose.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
