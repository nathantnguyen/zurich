
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuthorityLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AuthorityLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Selling"/>
 *     &lt;enumeration value="Servicing"/>
 *     &lt;enumeration value="Marketing"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AuthorityLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/")
@XmlEnum
public enum AuthorityLevel {

    @XmlEnumValue("Selling")
    SELLING("Selling"),
    @XmlEnumValue("Servicing")
    SERVICING("Servicing"),
    @XmlEnumValue("Marketing")
    MARKETING("Marketing");
    private final String value;

    AuthorityLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AuthorityLevel fromValue(String v) {
        for (AuthorityLevel c: AuthorityLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
