
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ParkingPlace.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ParkingPlace">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Driveway"/>
 *     &lt;enumeration value="Garage"/>
 *     &lt;enumeration value="Street"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ParkingPlace", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum ParkingPlace {

    @XmlEnumValue("Driveway")
    DRIVEWAY("Driveway"),
    @XmlEnumValue("Garage")
    GARAGE("Garage"),
    @XmlEnumValue("Street")
    STREET("Street");
    private final String value;

    ParkingPlace(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ParkingPlace fromValue(String v) {
        for (ParkingPlace c: ParkingPlace.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
