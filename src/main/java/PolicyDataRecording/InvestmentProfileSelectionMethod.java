
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvestmentProfileSelectionMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InvestmentProfileSelectionMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Automatic Profile Selection"/>
 *     &lt;enumeration value="Manual Selection"/>
 *     &lt;enumeration value="Manual Allocation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InvestmentProfileSelectionMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum InvestmentProfileSelectionMethod {

    @XmlEnumValue("Automatic Profile Selection")
    AUTOMATIC_PROFILE_SELECTION("Automatic Profile Selection"),
    @XmlEnumValue("Manual Selection")
    MANUAL_SELECTION("Manual Selection"),
    @XmlEnumValue("Manual Allocation")
    MANUAL_ALLOCATION("Manual Allocation");
    private final String value;

    InvestmentProfileSelectionMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InvestmentProfileSelectionMethod fromValue(String v) {
        for (InvestmentProfileSelectionMethod c: InvestmentProfileSelectionMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
