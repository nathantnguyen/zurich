
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Condition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Condition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO">
 *       &lt;sequence>
 *         &lt;element name="establishedEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="establishedStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="statedEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="statedStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="causingConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="causedConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="compositeCondition" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" minOccurs="0"/>
 *         &lt;element name="conditionComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="responsibleActivityReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subjectActivityReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="requiredActivityReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Condition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "establishedEndDate",
    "establishedStartDate",
    "statedEndDate",
    "statedStartDate",
    "causingConditions",
    "causedConditions",
    "compositeCondition",
    "conditionComponents",
    "responsibleActivityReferences",
    "subjectActivityReferences",
    "requiredActivityReferences"
})
@XmlSeeAlso({
    PhysicalConditionTO.class
})
public class ConditionTO
    extends AssessmentResultTO
{

    protected XMLGregorianCalendar establishedEndDate;
    protected XMLGregorianCalendar establishedStartDate;
    protected XMLGregorianCalendar statedEndDate;
    protected XMLGregorianCalendar statedStartDate;
    protected List<ConditionTO> causingConditions;
    protected List<ConditionTO> causedConditions;
    protected ConditionTO compositeCondition;
    protected List<ConditionTO> conditionComponents;
    protected List<ObjectReferenceTO> responsibleActivityReferences;
    protected List<ObjectReferenceTO> subjectActivityReferences;
    protected List<ObjectReferenceTO> requiredActivityReferences;

    /**
     * Gets the value of the establishedEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEstablishedEndDate() {
        return establishedEndDate;
    }

    /**
     * Sets the value of the establishedEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEstablishedEndDate(XMLGregorianCalendar value) {
        this.establishedEndDate = value;
    }

    /**
     * Gets the value of the establishedStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEstablishedStartDate() {
        return establishedStartDate;
    }

    /**
     * Sets the value of the establishedStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEstablishedStartDate(XMLGregorianCalendar value) {
        this.establishedStartDate = value;
    }

    /**
     * Gets the value of the statedEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatedEndDate() {
        return statedEndDate;
    }

    /**
     * Sets the value of the statedEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatedEndDate(XMLGregorianCalendar value) {
        this.statedEndDate = value;
    }

    /**
     * Gets the value of the statedStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatedStartDate() {
        return statedStartDate;
    }

    /**
     * Sets the value of the statedStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatedStartDate(XMLGregorianCalendar value) {
        this.statedStartDate = value;
    }

    /**
     * Gets the value of the causingConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the causingConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCausingConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionTO }
     * 
     * 
     */
    public List<ConditionTO> getCausingConditions() {
        if (causingConditions == null) {
            causingConditions = new ArrayList<ConditionTO>();
        }
        return this.causingConditions;
    }

    /**
     * Gets the value of the causedConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the causedConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCausedConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionTO }
     * 
     * 
     */
    public List<ConditionTO> getCausedConditions() {
        if (causedConditions == null) {
            causedConditions = new ArrayList<ConditionTO>();
        }
        return this.causedConditions;
    }

    /**
     * Gets the value of the compositeCondition property.
     * 
     * @return
     *     possible object is
     *     {@link ConditionTO }
     *     
     */
    public ConditionTO getCompositeCondition() {
        return compositeCondition;
    }

    /**
     * Sets the value of the compositeCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setCompositeCondition(ConditionTO value) {
        this.compositeCondition = value;
    }

    /**
     * Gets the value of the conditionComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditionComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditionComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionTO }
     * 
     * 
     */
    public List<ConditionTO> getConditionComponents() {
        if (conditionComponents == null) {
            conditionComponents = new ArrayList<ConditionTO>();
        }
        return this.conditionComponents;
    }

    /**
     * Gets the value of the responsibleActivityReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the responsibleActivityReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponsibleActivityReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getResponsibleActivityReferences() {
        if (responsibleActivityReferences == null) {
            responsibleActivityReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.responsibleActivityReferences;
    }

    /**
     * Gets the value of the subjectActivityReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subjectActivityReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubjectActivityReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getSubjectActivityReferences() {
        if (subjectActivityReferences == null) {
            subjectActivityReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.subjectActivityReferences;
    }

    /**
     * Gets the value of the requiredActivityReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requiredActivityReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequiredActivityReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getRequiredActivityReferences() {
        if (requiredActivityReferences == null) {
            requiredActivityReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.requiredActivityReferences;
    }

    /**
     * Sets the value of the causingConditions property.
     * 
     * @param causingConditions
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setCausingConditions(List<ConditionTO> causingConditions) {
        this.causingConditions = causingConditions;
    }

    /**
     * Sets the value of the causedConditions property.
     * 
     * @param causedConditions
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setCausedConditions(List<ConditionTO> causedConditions) {
        this.causedConditions = causedConditions;
    }

    /**
     * Sets the value of the conditionComponents property.
     * 
     * @param conditionComponents
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setConditionComponents(List<ConditionTO> conditionComponents) {
        this.conditionComponents = conditionComponents;
    }

    /**
     * Sets the value of the responsibleActivityReferences property.
     * 
     * @param responsibleActivityReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setResponsibleActivityReferences(List<ObjectReferenceTO> responsibleActivityReferences) {
        this.responsibleActivityReferences = responsibleActivityReferences;
    }

    /**
     * Sets the value of the subjectActivityReferences property.
     * 
     * @param subjectActivityReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSubjectActivityReferences(List<ObjectReferenceTO> subjectActivityReferences) {
        this.subjectActivityReferences = subjectActivityReferences;
    }

    /**
     * Sets the value of the requiredActivityReferences property.
     * 
     * @param requiredActivityReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setRequiredActivityReferences(List<ObjectReferenceTO> requiredActivityReferences) {
        this.requiredActivityReferences = requiredActivityReferences;
    }

}
