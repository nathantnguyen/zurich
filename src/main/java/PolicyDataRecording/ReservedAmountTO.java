
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReservedAmount_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReservedAmount_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElementPart_TO">
 *       &lt;sequence>
 *         &lt;element name="claimCostCode" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}ClaimCostCode_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReservedAmount_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "claimCostCode"
})
public class ReservedAmountTO
    extends MoneyProvisionElementPartTO
{

    protected ClaimCostCodeTO claimCostCode;

    /**
     * Gets the value of the claimCostCode property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimCostCodeTO }
     *     
     */
    public ClaimCostCodeTO getClaimCostCode() {
        return claimCostCode;
    }

    /**
     * Sets the value of the claimCostCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimCostCodeTO }
     *     
     */
    public void setClaimCostCode(ClaimCostCodeTO value) {
        this.claimCostCode = value;
    }

}
