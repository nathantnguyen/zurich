
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Procedure_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Procedure_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}TaskSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="procedureSteps" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}TaskSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Procedure_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/", propOrder = {
    "procedureSteps"
})
public class ProcedureTO
    extends TaskSpecificationTO
{

    protected List<TaskSpecificationTO> procedureSteps;

    /**
     * Gets the value of the procedureSteps property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the procedureSteps property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcedureSteps().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaskSpecificationTO }
     * 
     * 
     */
    public List<TaskSpecificationTO> getProcedureSteps() {
        if (procedureSteps == null) {
            procedureSteps = new ArrayList<TaskSpecificationTO>();
        }
        return this.procedureSteps;
    }

    /**
     * Sets the value of the procedureSteps property.
     * 
     * @param procedureSteps
     *     allowed object is
     *     {@link TaskSpecificationTO }
     *     
     */
    public void setProcedureSteps(List<TaskSpecificationTO> procedureSteps) {
        this.procedureSteps = procedureSteps;
    }

}
