
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationEffectivenessRating.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunicationEffectivenessRating">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Error In Response"/>
 *     &lt;enumeration value="No Error In Response"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommunicationEffectivenessRating", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum CommunicationEffectivenessRating {

    @XmlEnumValue("Error In Response")
    ERROR_IN_RESPONSE("Error In Response"),
    @XmlEnumValue("No Error In Response")
    NO_ERROR_IN_RESPONSE("No Error In Response");
    private final String value;

    CommunicationEffectivenessRating(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationEffectivenessRating fromValue(String v) {
        for (CommunicationEffectivenessRating c: CommunicationEffectivenessRating.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
