
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProfitAndLossAttribution_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfitAndLossAttribution_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="profitOrLossAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="profitOrLossCause" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="profitOrLossIndication" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfitAndLossAttribution_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "profitOrLossAmount",
    "profitOrLossCause",
    "profitOrLossIndication"
})
public class ProfitAndLossAttributionTO
    extends ScoreTO
{

    protected BaseCurrencyAmount profitOrLossAmount;
    protected String profitOrLossCause;
    protected Boolean profitOrLossIndication;

    /**
     * Gets the value of the profitOrLossAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getProfitOrLossAmount() {
        return profitOrLossAmount;
    }

    /**
     * Sets the value of the profitOrLossAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setProfitOrLossAmount(BaseCurrencyAmount value) {
        this.profitOrLossAmount = value;
    }

    /**
     * Gets the value of the profitOrLossCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitOrLossCause() {
        return profitOrLossCause;
    }

    /**
     * Sets the value of the profitOrLossCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitOrLossCause(String value) {
        this.profitOrLossCause = value;
    }

    /**
     * Gets the value of the profitOrLossIndication property.
     * This getter has been renamed from isProfitOrLossIndication() to getProfitOrLossIndication() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getProfitOrLossIndication() {
        return profitOrLossIndication;
    }

    /**
     * Sets the value of the profitOrLossIndication property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProfitOrLossIndication(Boolean value) {
        this.profitOrLossIndication = value;
    }

}
