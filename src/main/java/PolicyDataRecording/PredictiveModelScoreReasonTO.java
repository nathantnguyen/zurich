
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PredictiveModelScoreReason_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PredictiveModelScoreReason_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ScoreReason_TO">
 *       &lt;sequence>
 *         &lt;element name="predictiveModelProperties" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PredictiveModelPart_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="predictiveModelScoreReasonValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PredictiveModelScoreReason_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "predictiveModelProperties",
    "predictiveModelScoreReasonValue"
})
public class PredictiveModelScoreReasonTO
    extends ScoreReasonTO
{

    protected List<PredictiveModelPartTO> predictiveModelProperties;
    protected List<BigDecimal> predictiveModelScoreReasonValue;

    /**
     * Gets the value of the predictiveModelProperties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the predictiveModelProperties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPredictiveModelProperties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PredictiveModelPartTO }
     * 
     * 
     */
    public List<PredictiveModelPartTO> getPredictiveModelProperties() {
        if (predictiveModelProperties == null) {
            predictiveModelProperties = new ArrayList<PredictiveModelPartTO>();
        }
        return this.predictiveModelProperties;
    }

    /**
     * Gets the value of the predictiveModelScoreReasonValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the predictiveModelScoreReasonValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPredictiveModelScoreReasonValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BigDecimal }
     * 
     * 
     */
    public List<BigDecimal> getPredictiveModelScoreReasonValue() {
        if (predictiveModelScoreReasonValue == null) {
            predictiveModelScoreReasonValue = new ArrayList<BigDecimal>();
        }
        return this.predictiveModelScoreReasonValue;
    }

    /**
     * Sets the value of the predictiveModelProperties property.
     * 
     * @param predictiveModelProperties
     *     allowed object is
     *     {@link PredictiveModelPartTO }
     *     
     */
    public void setPredictiveModelProperties(List<PredictiveModelPartTO> predictiveModelProperties) {
        this.predictiveModelProperties = predictiveModelProperties;
    }

    /**
     * Sets the value of the predictiveModelScoreReasonValue property.
     * 
     * @param predictiveModelScoreReasonValue
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPredictiveModelScoreReasonValue(List<BigDecimal> predictiveModelScoreReasonValue) {
        this.predictiveModelScoreReasonValue = predictiveModelScoreReasonValue;
    }

}
