
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CommunicationContent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommunicationContent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="creationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0"/>
 *         &lt;element name="referencedObject" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="externalCommunication" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="communicationCorrections" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="includedCommunicationContents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="correctsContents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="includedContents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInCommunicationContent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}RoleInCommunicationContent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="documents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}Document_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationContent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "description",
    "name",
    "creationDate",
    "referencedObject",
    "externalCommunication",
    "communicationCorrections",
    "includedCommunicationContents",
    "correctsContents",
    "includedContents",
    "rolesInCommunicationContent",
    "documents"
})
@XmlSeeAlso({
    UnspecifiedContentTO.class,
    SpecifiedContentTO.class
})
public class CommunicationContentTO
    extends BusinessModelObjectTO
{

    protected String description;
    protected String name;
    protected XMLGregorianCalendar creationDate;
    protected ObjectReferenceTO referencedObject;
    protected Boolean externalCommunication;
    protected List<CommunicationContentTO> communicationCorrections;
    protected List<CommunicationContentTO> includedCommunicationContents;
    protected List<CommunicationContentTO> correctsContents;
    protected List<CommunicationContentTO> includedContents;
    protected List<RoleInCommunicationContentTO> rolesInCommunicationContent;
    protected List<DocumentTO> documents;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the referencedObject property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getReferencedObject() {
        return referencedObject;
    }

    /**
     * Sets the value of the referencedObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setReferencedObject(ObjectReferenceTO value) {
        this.referencedObject = value;
    }

    /**
     * Gets the value of the externalCommunication property.
     * This getter has been renamed from isExternalCommunication() to getExternalCommunication() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExternalCommunication() {
        return externalCommunication;
    }

    /**
     * Sets the value of the externalCommunication property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExternalCommunication(Boolean value) {
        this.externalCommunication = value;
    }

    /**
     * Gets the value of the communicationCorrections property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the communicationCorrections property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommunicationCorrections().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationContentTO }
     * 
     * 
     */
    public List<CommunicationContentTO> getCommunicationCorrections() {
        if (communicationCorrections == null) {
            communicationCorrections = new ArrayList<CommunicationContentTO>();
        }
        return this.communicationCorrections;
    }

    /**
     * Gets the value of the includedCommunicationContents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedCommunicationContents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludedCommunicationContents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationContentTO }
     * 
     * 
     */
    public List<CommunicationContentTO> getIncludedCommunicationContents() {
        if (includedCommunicationContents == null) {
            includedCommunicationContents = new ArrayList<CommunicationContentTO>();
        }
        return this.includedCommunicationContents;
    }

    /**
     * Gets the value of the correctsContents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the correctsContents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorrectsContents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationContentTO }
     * 
     * 
     */
    public List<CommunicationContentTO> getCorrectsContents() {
        if (correctsContents == null) {
            correctsContents = new ArrayList<CommunicationContentTO>();
        }
        return this.correctsContents;
    }

    /**
     * Gets the value of the includedContents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedContents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludedContents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationContentTO }
     * 
     * 
     */
    public List<CommunicationContentTO> getIncludedContents() {
        if (includedContents == null) {
            includedContents = new ArrayList<CommunicationContentTO>();
        }
        return this.includedContents;
    }

    /**
     * Gets the value of the rolesInCommunicationContent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInCommunicationContent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInCommunicationContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInCommunicationContentTO }
     * 
     * 
     */
    public List<RoleInCommunicationContentTO> getRolesInCommunicationContent() {
        if (rolesInCommunicationContent == null) {
            rolesInCommunicationContent = new ArrayList<RoleInCommunicationContentTO>();
        }
        return this.rolesInCommunicationContent;
    }

    /**
     * Gets the value of the documents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentTO }
     * 
     * 
     */
    public List<DocumentTO> getDocuments() {
        if (documents == null) {
            documents = new ArrayList<DocumentTO>();
        }
        return this.documents;
    }

    /**
     * Sets the value of the communicationCorrections property.
     * 
     * @param communicationCorrections
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setCommunicationCorrections(List<CommunicationContentTO> communicationCorrections) {
        this.communicationCorrections = communicationCorrections;
    }

    /**
     * Sets the value of the includedCommunicationContents property.
     * 
     * @param includedCommunicationContents
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setIncludedCommunicationContents(List<CommunicationContentTO> includedCommunicationContents) {
        this.includedCommunicationContents = includedCommunicationContents;
    }

    /**
     * Sets the value of the correctsContents property.
     * 
     * @param correctsContents
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setCorrectsContents(List<CommunicationContentTO> correctsContents) {
        this.correctsContents = correctsContents;
    }

    /**
     * Sets the value of the includedContents property.
     * 
     * @param includedContents
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setIncludedContents(List<CommunicationContentTO> includedContents) {
        this.includedContents = includedContents;
    }

    /**
     * Sets the value of the rolesInCommunicationContent property.
     * 
     * @param rolesInCommunicationContent
     *     allowed object is
     *     {@link RoleInCommunicationContentTO }
     *     
     */
    public void setRolesInCommunicationContent(List<RoleInCommunicationContentTO> rolesInCommunicationContent) {
        this.rolesInCommunicationContent = rolesInCommunicationContent;
    }

    /**
     * Sets the value of the documents property.
     * 
     * @param documents
     *     allowed object is
     *     {@link DocumentTO }
     *     
     */
    public void setDocuments(List<DocumentTO> documents) {
        this.documents = documents;
    }

}
