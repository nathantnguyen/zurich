
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PredictiveModelPart_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PredictiveModelPart_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="contributionName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="contributionRawValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="defaultUsed" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="factorCalculationValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="isVariableIn" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PredictiveModelScoreElement_TO" minOccurs="0"/>
 *         &lt;element name="maximumContributionValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="minimumContributionValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="contributionIndicationText" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="contributionValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PredictiveModelPart_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "contributionName",
    "contributionRawValue",
    "defaultUsed",
    "factorCalculationValue",
    "isVariableIn",
    "maximumContributionValue",
    "minimumContributionValue",
    "contributionIndicationText",
    "contributionValue"
})
public class PredictiveModelPartTO
    extends DependentObjectTO
{

    protected String contributionName;
    protected String contributionRawValue;
    protected Boolean defaultUsed;
    protected String factorCalculationValue;
    protected PredictiveModelScoreElementTO isVariableIn;
    protected BigDecimal maximumContributionValue;
    protected BigDecimal minimumContributionValue;
    protected String contributionIndicationText;
    protected BigDecimal contributionValue;

    /**
     * Gets the value of the contributionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContributionName() {
        return contributionName;
    }

    /**
     * Sets the value of the contributionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContributionName(String value) {
        this.contributionName = value;
    }

    /**
     * Gets the value of the contributionRawValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContributionRawValue() {
        return contributionRawValue;
    }

    /**
     * Sets the value of the contributionRawValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContributionRawValue(String value) {
        this.contributionRawValue = value;
    }

    /**
     * Gets the value of the defaultUsed property.
     * This getter has been renamed from isDefaultUsed() to getDefaultUsed() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDefaultUsed() {
        return defaultUsed;
    }

    /**
     * Sets the value of the defaultUsed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultUsed(Boolean value) {
        this.defaultUsed = value;
    }

    /**
     * Gets the value of the factorCalculationValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFactorCalculationValue() {
        return factorCalculationValue;
    }

    /**
     * Sets the value of the factorCalculationValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFactorCalculationValue(String value) {
        this.factorCalculationValue = value;
    }

    /**
     * Gets the value of the isVariableIn property.
     * 
     * @return
     *     possible object is
     *     {@link PredictiveModelScoreElementTO }
     *     
     */
    public PredictiveModelScoreElementTO getIsVariableIn() {
        return isVariableIn;
    }

    /**
     * Sets the value of the isVariableIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link PredictiveModelScoreElementTO }
     *     
     */
    public void setIsVariableIn(PredictiveModelScoreElementTO value) {
        this.isVariableIn = value;
    }

    /**
     * Gets the value of the maximumContributionValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximumContributionValue() {
        return maximumContributionValue;
    }

    /**
     * Sets the value of the maximumContributionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximumContributionValue(BigDecimal value) {
        this.maximumContributionValue = value;
    }

    /**
     * Gets the value of the minimumContributionValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinimumContributionValue() {
        return minimumContributionValue;
    }

    /**
     * Sets the value of the minimumContributionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinimumContributionValue(BigDecimal value) {
        this.minimumContributionValue = value;
    }

    /**
     * Gets the value of the contributionIndicationText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContributionIndicationText() {
        return contributionIndicationText;
    }

    /**
     * Sets the value of the contributionIndicationText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContributionIndicationText(String value) {
        this.contributionIndicationText = value;
    }

    /**
     * Gets the value of the contributionValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getContributionValue() {
        return contributionValue;
    }

    /**
     * Sets the value of the contributionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setContributionValue(BigDecimal value) {
        this.contributionValue = value;
    }

}
