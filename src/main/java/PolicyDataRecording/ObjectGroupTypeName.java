
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectGroupTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ObjectGroupTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Fleet"/>
 *     &lt;enumeration value="General Cargo"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ObjectGroupTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum ObjectGroupTypeName {

    @XmlEnumValue("Fleet")
    FLEET("Fleet"),
    @XmlEnumValue("General Cargo")
    GENERAL_CARGO("General Cargo");
    private final String value;

    ObjectGroupTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ObjectGroupTypeName fromValue(String v) {
        for (ObjectGroupTypeName c: ObjectGroupTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
