
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountFacilityComponent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountFacilityComponent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}ServiceComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="disclosureAcceptanceRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountFacilityComponent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "disclosureAcceptanceRequired"
})
@XmlSeeAlso({
    PaymentFacilityTO.class,
    ReportingFacilityTO.class,
    TransferFacilityTO.class,
    AccessFacilityTO.class
})
public class AccountFacilityComponentTO
    extends ServiceComponentTO
{

    protected Boolean disclosureAcceptanceRequired;

    /**
     * Gets the value of the disclosureAcceptanceRequired property.
     * This getter has been renamed from isDisclosureAcceptanceRequired() to getDisclosureAcceptanceRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDisclosureAcceptanceRequired() {
        return disclosureAcceptanceRequired;
    }

    /**
     * Sets the value of the disclosureAcceptanceRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisclosureAcceptanceRequired(Boolean value) {
        this.disclosureAcceptanceRequired = value;
    }

}
