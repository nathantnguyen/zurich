
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorporateAgreementRequestTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CorporateAgreementRequestTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Audit Risk Procedure Request"/>
 *     &lt;enumeration value="Corporate Agreement Change Request"/>
 *     &lt;enumeration value="Corporate Agreement Information Request"/>
 *     &lt;enumeration value="Internal Risk Control Request"/>
 *     &lt;enumeration value="Internal Risk Model Validation Request"/>
 *     &lt;enumeration value="Risk Agreement Change By Supervisor Request"/>
 *     &lt;enumeration value="Risk Portfolio Transfer Approval Request"/>
 *     &lt;enumeration value="Solvency Capital Add On Request"/>
 *     &lt;enumeration value="Solvency Capital Allocation Change Request"/>
 *     &lt;enumeration value="Corporate Agreement Approval Request"/>
 *     &lt;enumeration value="Solvency Report Disclosure Request"/>
 *     &lt;enumeration value="Solvency Report Request"/>
 *     &lt;enumeration value="Solvency Report To Supervisor Request"/>
 *     &lt;enumeration value="Solvency Supervisor Approval Request"/>
 *     &lt;enumeration value="Supervisor Approval Request"/>
 *     &lt;enumeration value="Supervisor Review Request"/>
 *     &lt;enumeration value="Corporate Agreement Transaction Request"/>
 *     &lt;enumeration value="Risk Event Simulation Request"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CorporateAgreementRequestTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum CorporateAgreementRequestTypeName {

    @XmlEnumValue("Audit Risk Procedure Request")
    AUDIT_RISK_PROCEDURE_REQUEST("Audit Risk Procedure Request"),
    @XmlEnumValue("Corporate Agreement Change Request")
    CORPORATE_AGREEMENT_CHANGE_REQUEST("Corporate Agreement Change Request"),
    @XmlEnumValue("Corporate Agreement Information Request")
    CORPORATE_AGREEMENT_INFORMATION_REQUEST("Corporate Agreement Information Request"),
    @XmlEnumValue("Internal Risk Control Request")
    INTERNAL_RISK_CONTROL_REQUEST("Internal Risk Control Request"),
    @XmlEnumValue("Internal Risk Model Validation Request")
    INTERNAL_RISK_MODEL_VALIDATION_REQUEST("Internal Risk Model Validation Request"),
    @XmlEnumValue("Risk Agreement Change By Supervisor Request")
    RISK_AGREEMENT_CHANGE_BY_SUPERVISOR_REQUEST("Risk Agreement Change By Supervisor Request"),
    @XmlEnumValue("Risk Portfolio Transfer Approval Request")
    RISK_PORTFOLIO_TRANSFER_APPROVAL_REQUEST("Risk Portfolio Transfer Approval Request"),
    @XmlEnumValue("Solvency Capital Add On Request")
    SOLVENCY_CAPITAL_ADD_ON_REQUEST("Solvency Capital Add On Request"),
    @XmlEnumValue("Solvency Capital Allocation Change Request")
    SOLVENCY_CAPITAL_ALLOCATION_CHANGE_REQUEST("Solvency Capital Allocation Change Request"),
    @XmlEnumValue("Corporate Agreement Approval Request")
    CORPORATE_AGREEMENT_APPROVAL_REQUEST("Corporate Agreement Approval Request"),
    @XmlEnumValue("Solvency Report Disclosure Request")
    SOLVENCY_REPORT_DISCLOSURE_REQUEST("Solvency Report Disclosure Request"),
    @XmlEnumValue("Solvency Report Request")
    SOLVENCY_REPORT_REQUEST("Solvency Report Request"),
    @XmlEnumValue("Solvency Report To Supervisor Request")
    SOLVENCY_REPORT_TO_SUPERVISOR_REQUEST("Solvency Report To Supervisor Request"),
    @XmlEnumValue("Solvency Supervisor Approval Request")
    SOLVENCY_SUPERVISOR_APPROVAL_REQUEST("Solvency Supervisor Approval Request"),
    @XmlEnumValue("Supervisor Approval Request")
    SUPERVISOR_APPROVAL_REQUEST("Supervisor Approval Request"),
    @XmlEnumValue("Supervisor Review Request")
    SUPERVISOR_REVIEW_REQUEST("Supervisor Review Request"),
    @XmlEnumValue("Corporate Agreement Transaction Request")
    CORPORATE_AGREEMENT_TRANSACTION_REQUEST("Corporate Agreement Transaction Request"),
    @XmlEnumValue("Risk Event Simulation Request")
    RISK_EVENT_SIMULATION_REQUEST("Risk Event Simulation Request");
    private final String value;

    CorporateAgreementRequestTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CorporateAgreementRequestTypeName fromValue(String v) {
        for (CorporateAgreementRequestTypeName c: CorporateAgreementRequestTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
