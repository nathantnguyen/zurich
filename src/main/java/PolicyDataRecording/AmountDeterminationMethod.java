
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AmountDeterminationMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AmountDeterminationMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Expert Estimation"/>
 *     &lt;enumeration value="Role Player Estimation"/>
 *     &lt;enumeration value="Role Player Specified"/>
 *     &lt;enumeration value="Highest Possible Loss"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AmountDeterminationMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum AmountDeterminationMethod {

    @XmlEnumValue("Expert Estimation")
    EXPERT_ESTIMATION("Expert Estimation"),
    @XmlEnumValue("Role Player Estimation")
    ROLE_PLAYER_ESTIMATION("Role Player Estimation"),
    @XmlEnumValue("Role Player Specified")
    ROLE_PLAYER_SPECIFIED("Role Player Specified"),
    @XmlEnumValue("Highest Possible Loss")
    HIGHEST_POSSIBLE_LOSS("Highest Possible Loss");
    private final String value;

    AmountDeterminationMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AmountDeterminationMethod fromValue(String v) {
        for (AmountDeterminationMethod c: AmountDeterminationMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
