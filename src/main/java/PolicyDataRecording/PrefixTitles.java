
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PrefixTitles.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PrefixTitles">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Mr"/>
 *     &lt;enumeration value="Mrs"/>
 *     &lt;enumeration value="Dr"/>
 *     &lt;enumeration value="Herr"/>
 *     &lt;enumeration value="Ms"/>
 *     &lt;enumeration value="Prof"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PrefixTitles", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum PrefixTitles {

    @XmlEnumValue("Mr")
    MR("Mr"),
    @XmlEnumValue("Mrs")
    MRS("Mrs"),
    @XmlEnumValue("Dr")
    DR("Dr"),
    @XmlEnumValue("Herr")
    HERR("Herr"),
    @XmlEnumValue("Ms")
    MS("Ms"),
    @XmlEnumValue("Prof")
    PROF("Prof");
    private final String value;

    PrefixTitles(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PrefixTitles fromValue(String v) {
        for (PrefixTitles c: PrefixTitles.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
