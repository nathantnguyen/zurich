
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvestmentScope.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InvestmentScope">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Bond"/>
 *     &lt;enumeration value="Bond And Share And Cash"/>
 *     &lt;enumeration value="Cash"/>
 *     &lt;enumeration value="Cash And Bond"/>
 *     &lt;enumeration value="Mixed"/>
 *     &lt;enumeration value="Mutual Fund"/>
 *     &lt;enumeration value="Property"/>
 *     &lt;enumeration value="Share"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InvestmentScope", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum InvestmentScope {

    @XmlEnumValue("Bond")
    BOND("Bond"),
    @XmlEnumValue("Bond And Share And Cash")
    BOND_AND_SHARE_AND_CASH("Bond And Share And Cash"),
    @XmlEnumValue("Cash")
    CASH("Cash"),
    @XmlEnumValue("Cash And Bond")
    CASH_AND_BOND("Cash And Bond"),
    @XmlEnumValue("Mixed")
    MIXED("Mixed"),
    @XmlEnumValue("Mutual Fund")
    MUTUAL_FUND("Mutual Fund"),
    @XmlEnumValue("Property")
    PROPERTY("Property"),
    @XmlEnumValue("Share")
    SHARE("Share");
    private final String value;

    InvestmentScope(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InvestmentScope fromValue(String v) {
        for (InvestmentScope c: InvestmentScope.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
