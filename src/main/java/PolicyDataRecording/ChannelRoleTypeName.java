
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChannelRoleTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ChannelRoleTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Agent"/>
 *     &lt;enumeration value="Broker"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChannelRoleTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/")
@XmlEnum
public enum ChannelRoleTypeName {

    @XmlEnumValue("Agent")
    AGENT("Agent"),
    @XmlEnumValue("Broker")
    BROKER("Broker");
    private final String value;

    ChannelRoleTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChannelRoleTypeName fromValue(String v) {
        for (ChannelRoleTypeName c: ChannelRoleTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
