
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TextSize.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TextSize">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Larger"/>
 *     &lt;enumeration value="Largest"/>
 *     &lt;enumeration value="Medium"/>
 *     &lt;enumeration value="Smaller"/>
 *     &lt;enumeration value="Smallest"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TextSize", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum TextSize {

    @XmlEnumValue("Larger")
    LARGER("Larger"),
    @XmlEnumValue("Largest")
    LARGEST("Largest"),
    @XmlEnumValue("Medium")
    MEDIUM("Medium"),
    @XmlEnumValue("Smaller")
    SMALLER("Smaller"),
    @XmlEnumValue("Smallest")
    SMALLEST("Smallest");
    private final String value;

    TextSize(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TextSize fromValue(String v) {
        for (TextSize c: TextSize.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
