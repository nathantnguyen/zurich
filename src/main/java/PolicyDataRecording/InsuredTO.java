
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Insured_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Insured_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRole_TO">
 *       &lt;sequence>
 *         &lt;element name="age" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="insuredRootType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Insured_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "age",
    "insuredRootType"
})
@XmlSeeAlso({
    NamedDriverTO.class,
    AdditionalInsuredTO.class
})
public class InsuredTO
    extends FinancialServicesRoleTO
{

    protected BigInteger age;
    protected String insuredRootType;

    /**
     * Gets the value of the age property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAge(BigInteger value) {
        this.age = value;
    }

    /**
     * Gets the value of the insuredRootType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredRootType() {
        return insuredRootType;
    }

    /**
     * Sets the value of the insuredRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredRootType(String value) {
        this.insuredRootType = value;
    }

}
