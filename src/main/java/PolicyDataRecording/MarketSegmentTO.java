
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketSegment_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketSegment_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO">
 *       &lt;sequence>
 *         &lt;element name="goalsAndNeeds" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GoalAndNeed_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketSegment_TO", propOrder = {
    "goalsAndNeeds"
})
public class MarketSegmentTO
    extends CategoryTO
{

    protected List<GoalAndNeedTO> goalsAndNeeds;

    /**
     * Gets the value of the goalsAndNeeds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the goalsAndNeeds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGoalsAndNeeds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GoalAndNeedTO }
     * 
     * 
     */
    public List<GoalAndNeedTO> getGoalsAndNeeds() {
        if (goalsAndNeeds == null) {
            goalsAndNeeds = new ArrayList<GoalAndNeedTO>();
        }
        return this.goalsAndNeeds;
    }

    /**
     * Sets the value of the goalsAndNeeds property.
     * 
     * @param goalsAndNeeds
     *     allowed object is
     *     {@link GoalAndNeedTO }
     *     
     */
    public void setGoalsAndNeeds(List<GoalAndNeedTO> goalsAndNeeds) {
        this.goalsAndNeeds = goalsAndNeeds;
    }

}
