
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssessmentScenarioSet.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssessmentScenarioSet">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Variance Percent"/>
 *     &lt;enumeration value="Actual"/>
 *     &lt;enumeration value="Forecast"/>
 *     &lt;enumeration value="Stress Testing"/>
 *     &lt;enumeration value="Budget"/>
 *     &lt;enumeration value="Alternative Variance Period"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssessmentScenarioSet", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum AssessmentScenarioSet {

    @XmlEnumValue("Variance Percent")
    VARIANCE_PERCENT("Variance Percent"),
    @XmlEnumValue("Actual")
    ACTUAL("Actual"),
    @XmlEnumValue("Forecast")
    FORECAST("Forecast"),
    @XmlEnumValue("Stress Testing")
    STRESS_TESTING("Stress Testing"),
    @XmlEnumValue("Budget")
    BUDGET("Budget"),
    @XmlEnumValue("Alternative Variance Period")
    ALTERNATIVE_VARIANCE_PERIOD("Alternative Variance Period");
    private final String value;

    AssessmentScenarioSet(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssessmentScenarioSet fromValue(String v) {
        for (AssessmentScenarioSet c: AssessmentScenarioSet.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
