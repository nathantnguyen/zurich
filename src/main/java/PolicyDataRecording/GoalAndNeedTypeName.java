
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GoalAndNeedTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GoalAndNeedTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Dependent Protection"/>
 *     &lt;enumeration value="Education Funding"/>
 *     &lt;enumeration value="Natural Phenomenon Protection"/>
 *     &lt;enumeration value="Retirement Funding"/>
 *     &lt;enumeration value="Financial Planning"/>
 *     &lt;enumeration value="Purchase Of Durable Goods"/>
 *     &lt;enumeration value="Risk Minimisation"/>
 *     &lt;enumeration value="Tax Reduction"/>
 *     &lt;enumeration value="Liability Protection"/>
 *     &lt;enumeration value="Asset Protection"/>
 *     &lt;enumeration value="Risk Strategy"/>
 *     &lt;enumeration value="Market Strategy"/>
 *     &lt;enumeration value="Operational Risk Strategy"/>
 *     &lt;enumeration value="Own Solvency Target"/>
 *     &lt;enumeration value="Regulatory Solvency Requirement"/>
 *     &lt;enumeration value="Reinsurance Strategy"/>
 *     &lt;enumeration value="Risk Mitigation Strategy"/>
 *     &lt;enumeration value="Solvency Strategy"/>
 *     &lt;enumeration value="Tax Regulatory Requirement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GoalAndNeedTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum GoalAndNeedTypeName {

    @XmlEnumValue("Dependent Protection")
    DEPENDENT_PROTECTION("Dependent Protection"),
    @XmlEnumValue("Education Funding")
    EDUCATION_FUNDING("Education Funding"),
    @XmlEnumValue("Natural Phenomenon Protection")
    NATURAL_PHENOMENON_PROTECTION("Natural Phenomenon Protection"),
    @XmlEnumValue("Retirement Funding")
    RETIREMENT_FUNDING("Retirement Funding"),
    @XmlEnumValue("Financial Planning")
    FINANCIAL_PLANNING("Financial Planning"),
    @XmlEnumValue("Purchase Of Durable Goods")
    PURCHASE_OF_DURABLE_GOODS("Purchase Of Durable Goods"),
    @XmlEnumValue("Risk Minimisation")
    RISK_MINIMISATION("Risk Minimisation"),
    @XmlEnumValue("Tax Reduction")
    TAX_REDUCTION("Tax Reduction"),
    @XmlEnumValue("Liability Protection")
    LIABILITY_PROTECTION("Liability Protection"),
    @XmlEnumValue("Asset Protection")
    ASSET_PROTECTION("Asset Protection"),
    @XmlEnumValue("Risk Strategy")
    RISK_STRATEGY("Risk Strategy"),
    @XmlEnumValue("Market Strategy")
    MARKET_STRATEGY("Market Strategy"),
    @XmlEnumValue("Operational Risk Strategy")
    OPERATIONAL_RISK_STRATEGY("Operational Risk Strategy"),
    @XmlEnumValue("Own Solvency Target")
    OWN_SOLVENCY_TARGET("Own Solvency Target"),
    @XmlEnumValue("Regulatory Solvency Requirement")
    REGULATORY_SOLVENCY_REQUIREMENT("Regulatory Solvency Requirement"),
    @XmlEnumValue("Reinsurance Strategy")
    REINSURANCE_STRATEGY("Reinsurance Strategy"),
    @XmlEnumValue("Risk Mitigation Strategy")
    RISK_MITIGATION_STRATEGY("Risk Mitigation Strategy"),
    @XmlEnumValue("Solvency Strategy")
    SOLVENCY_STRATEGY("Solvency Strategy"),
    @XmlEnumValue("Tax Regulatory Requirement")
    TAX_REGULATORY_REQUIREMENT("Tax Regulatory Requirement");
    private final String value;

    GoalAndNeedTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GoalAndNeedTypeName fromValue(String v) {
        for (GoalAndNeedTypeName c: GoalAndNeedTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
