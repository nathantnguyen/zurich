
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyProvisionElementPartTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MoneyProvisionElementPartTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Pure Premium"/>
 *     &lt;enumeration value="Commission Loading"/>
 *     &lt;enumeration value="Charge"/>
 *     &lt;enumeration value="Principal"/>
 *     &lt;enumeration value="Dividend"/>
 *     &lt;enumeration value="Tax"/>
 *     &lt;enumeration value="Commission"/>
 *     &lt;enumeration value="Salary"/>
 *     &lt;enumeration value="Bonus"/>
 *     &lt;enumeration value="Interest"/>
 *     &lt;enumeration value="Business Capital"/>
 *     &lt;enumeration value="Enterprise Risk Capital"/>
 *     &lt;enumeration value="Enterprise Solvency Capital"/>
 *     &lt;enumeration value="Reserve"/>
 *     &lt;enumeration value="Reserve For Claim"/>
 *     &lt;enumeration value="Inforce Premium"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MoneyProvisionElementPartTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum MoneyProvisionElementPartTypeName {

    @XmlEnumValue("Pure Premium")
    PURE_PREMIUM("Pure Premium"),
    @XmlEnumValue("Commission Loading")
    COMMISSION_LOADING("Commission Loading"),
    @XmlEnumValue("Charge")
    CHARGE("Charge"),
    @XmlEnumValue("Principal")
    PRINCIPAL("Principal"),
    @XmlEnumValue("Dividend")
    DIVIDEND("Dividend"),
    @XmlEnumValue("Tax")
    TAX("Tax"),
    @XmlEnumValue("Commission")
    COMMISSION("Commission"),
    @XmlEnumValue("Salary")
    SALARY("Salary"),
    @XmlEnumValue("Bonus")
    BONUS("Bonus"),
    @XmlEnumValue("Interest")
    INTEREST("Interest"),
    @XmlEnumValue("Business Capital")
    BUSINESS_CAPITAL("Business Capital"),
    @XmlEnumValue("Enterprise Risk Capital")
    ENTERPRISE_RISK_CAPITAL("Enterprise Risk Capital"),
    @XmlEnumValue("Enterprise Solvency Capital")
    ENTERPRISE_SOLVENCY_CAPITAL("Enterprise Solvency Capital"),
    @XmlEnumValue("Reserve")
    RESERVE("Reserve"),
    @XmlEnumValue("Reserve For Claim")
    RESERVE_FOR_CLAIM("Reserve For Claim"),
    @XmlEnumValue("Inforce Premium")
    INFORCE_PREMIUM("Inforce Premium");
    private final String value;

    MoneyProvisionElementPartTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MoneyProvisionElementPartTypeName fromValue(String v) {
        for (MoneyProvisionElementPartTypeName c: MoneyProvisionElementPartTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
