
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompositeInstrument_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompositeInstrument_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="incomeDistributionFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="minimumAmountOfTransfer" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="unitOfTransfer" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompositeInstrument_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "incomeDistributionFrequency",
    "minimumAmountOfTransfer",
    "unitOfTransfer"
})
public class CompositeInstrumentTO
    extends FinancialAssetTO
{

    protected BigInteger incomeDistributionFrequency;
    protected BaseCurrencyAmount minimumAmountOfTransfer;
    protected BigInteger unitOfTransfer;

    /**
     * Gets the value of the incomeDistributionFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIncomeDistributionFrequency() {
        return incomeDistributionFrequency;
    }

    /**
     * Sets the value of the incomeDistributionFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIncomeDistributionFrequency(BigInteger value) {
        this.incomeDistributionFrequency = value;
    }

    /**
     * Gets the value of the minimumAmountOfTransfer property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMinimumAmountOfTransfer() {
        return minimumAmountOfTransfer;
    }

    /**
     * Sets the value of the minimumAmountOfTransfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMinimumAmountOfTransfer(BaseCurrencyAmount value) {
        this.minimumAmountOfTransfer = value;
    }

    /**
     * Gets the value of the unitOfTransfer property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnitOfTransfer() {
        return unitOfTransfer;
    }

    /**
     * Sets the value of the unitOfTransfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnitOfTransfer(BigInteger value) {
        this.unitOfTransfer = value;
    }

}
