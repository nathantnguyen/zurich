
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SelectionKind.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SelectionKind">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Aggregate Table"/>
 *     &lt;enumeration value="Select Table"/>
 *     &lt;enumeration value="Standard Table"/>
 *     &lt;enumeration value="Ultimate Table"/>
 *     &lt;enumeration value="Assumptions Table"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SelectionKind", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/")
@XmlEnum
public enum SelectionKind {

    @XmlEnumValue("Aggregate Table")
    AGGREGATE_TABLE("Aggregate Table"),
    @XmlEnumValue("Select Table")
    SELECT_TABLE("Select Table"),
    @XmlEnumValue("Standard Table")
    STANDARD_TABLE("Standard Table"),
    @XmlEnumValue("Ultimate Table")
    ULTIMATE_TABLE("Ultimate Table"),
    @XmlEnumValue("Assumptions Table")
    ASSUMPTIONS_TABLE("Assumptions Table");
    private final String value;

    SelectionKind(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SelectionKind fromValue(String v) {
        for (SelectionKind c: SelectionKind.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
