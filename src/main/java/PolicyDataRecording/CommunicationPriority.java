
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationPriority.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunicationPriority">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="High Priority"/>
 *     &lt;enumeration value="Low Priority"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommunicationPriority", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum CommunicationPriority {

    @XmlEnumValue("High Priority")
    HIGH_PRIORITY("High Priority"),
    @XmlEnumValue("Low Priority")
    LOW_PRIORITY("Low Priority");
    private final String value;

    CommunicationPriority(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationPriority fromValue(String v) {
        for (CommunicationPriority c: CommunicationPriority.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
