
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OptionTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OptionTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Cap Option"/>
 *     &lt;enumeration value="Bond Option"/>
 *     &lt;enumeration value="Collars Option"/>
 *     &lt;enumeration value="Commodity Option"/>
 *     &lt;enumeration value="Currency Option"/>
 *     &lt;enumeration value="Equity Option"/>
 *     &lt;enumeration value="Floor Option"/>
 *     &lt;enumeration value="Futures Option"/>
 *     &lt;enumeration value="Index Option"/>
 *     &lt;enumeration value="Interest Rate Option"/>
 *     &lt;enumeration value="Swaption"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OptionTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum OptionTypeName {

    @XmlEnumValue("Cap Option")
    CAP_OPTION("Cap Option"),
    @XmlEnumValue("Bond Option")
    BOND_OPTION("Bond Option"),
    @XmlEnumValue("Collars Option")
    COLLARS_OPTION("Collars Option"),
    @XmlEnumValue("Commodity Option")
    COMMODITY_OPTION("Commodity Option"),
    @XmlEnumValue("Currency Option")
    CURRENCY_OPTION("Currency Option"),
    @XmlEnumValue("Equity Option")
    EQUITY_OPTION("Equity Option"),
    @XmlEnumValue("Floor Option")
    FLOOR_OPTION("Floor Option"),
    @XmlEnumValue("Futures Option")
    FUTURES_OPTION("Futures Option"),
    @XmlEnumValue("Index Option")
    INDEX_OPTION("Index Option"),
    @XmlEnumValue("Interest Rate Option")
    INTEREST_RATE_OPTION("Interest Rate Option"),
    @XmlEnumValue("Swaption")
    SWAPTION("Swaption");
    private final String value;

    OptionTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OptionTypeName fromValue(String v) {
        for (OptionTypeName c: OptionTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
