
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MarketableProduct_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketableProduct_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}FinancialServicesProduct_TO">
 *       &lt;sequence>
 *         &lt;element name="launchDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="withdrawalDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="suspensionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="terminationDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="targetMarketSegments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}MarketSegment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="canBeConvertedFrom" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="canBeConvertedTo" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="marketableProductRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}MarketableProductType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketableProduct_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "launchDate",
    "withdrawalDate",
    "suspensionDate",
    "terminationDate",
    "targetMarketSegments",
    "canBeConvertedFrom",
    "canBeConvertedTo",
    "marketableProductRootType"
})
public class MarketableProductTO
    extends FinancialServicesProductTO
{

    protected XMLGregorianCalendar launchDate;
    protected XMLGregorianCalendar withdrawalDate;
    protected XMLGregorianCalendar suspensionDate;
    protected XMLGregorianCalendar terminationDate;
    protected List<MarketSegmentTO> targetMarketSegments;
    protected ObjectReferenceTO canBeConvertedFrom;
    protected ObjectReferenceTO canBeConvertedTo;
    protected MarketableProductTypeTO marketableProductRootType;

    /**
     * Gets the value of the launchDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLaunchDate() {
        return launchDate;
    }

    /**
     * Sets the value of the launchDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLaunchDate(XMLGregorianCalendar value) {
        this.launchDate = value;
    }

    /**
     * Gets the value of the withdrawalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWithdrawalDate() {
        return withdrawalDate;
    }

    /**
     * Sets the value of the withdrawalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWithdrawalDate(XMLGregorianCalendar value) {
        this.withdrawalDate = value;
    }

    /**
     * Gets the value of the suspensionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSuspensionDate() {
        return suspensionDate;
    }

    /**
     * Sets the value of the suspensionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSuspensionDate(XMLGregorianCalendar value) {
        this.suspensionDate = value;
    }

    /**
     * Gets the value of the terminationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTerminationDate() {
        return terminationDate;
    }

    /**
     * Sets the value of the terminationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTerminationDate(XMLGregorianCalendar value) {
        this.terminationDate = value;
    }

    /**
     * Gets the value of the targetMarketSegments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the targetMarketSegments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTargetMarketSegments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MarketSegmentTO }
     * 
     * 
     */
    public List<MarketSegmentTO> getTargetMarketSegments() {
        if (targetMarketSegments == null) {
            targetMarketSegments = new ArrayList<MarketSegmentTO>();
        }
        return this.targetMarketSegments;
    }

    /**
     * Gets the value of the canBeConvertedFrom property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getCanBeConvertedFrom() {
        return canBeConvertedFrom;
    }

    /**
     * Sets the value of the canBeConvertedFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setCanBeConvertedFrom(ObjectReferenceTO value) {
        this.canBeConvertedFrom = value;
    }

    /**
     * Gets the value of the canBeConvertedTo property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getCanBeConvertedTo() {
        return canBeConvertedTo;
    }

    /**
     * Sets the value of the canBeConvertedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setCanBeConvertedTo(ObjectReferenceTO value) {
        this.canBeConvertedTo = value;
    }

    /**
     * Gets the value of the marketableProductRootType property.
     * 
     * @return
     *     possible object is
     *     {@link MarketableProductTypeTO }
     *     
     */
    public MarketableProductTypeTO getMarketableProductRootType() {
        return marketableProductRootType;
    }

    /**
     * Sets the value of the marketableProductRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketableProductTypeTO }
     *     
     */
    public void setMarketableProductRootType(MarketableProductTypeTO value) {
        this.marketableProductRootType = value;
    }

    /**
     * Sets the value of the targetMarketSegments property.
     * 
     * @param targetMarketSegments
     *     allowed object is
     *     {@link MarketSegmentTO }
     *     
     */
    public void setTargetMarketSegments(List<MarketSegmentTO> targetMarketSegments) {
        this.targetMarketSegments = targetMarketSegments;
    }

}
