
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgreementSpecState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AgreementSpecState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Defined"/>
 *     &lt;enumeration value="Draft"/>
 *     &lt;enumeration value="Effective"/>
 *     &lt;enumeration value="Suspended"/>
 *     &lt;enumeration value="Withdrawn"/>
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="Initial"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AgreementSpecState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/")
@XmlEnum
public enum AgreementSpecState {

    @XmlEnumValue("Defined")
    DEFINED("Defined"),
    @XmlEnumValue("Draft")
    DRAFT("Draft"),
    @XmlEnumValue("Effective")
    EFFECTIVE("Effective"),
    @XmlEnumValue("Suspended")
    SUSPENDED("Suspended"),
    @XmlEnumValue("Withdrawn")
    WITHDRAWN("Withdrawn"),
    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("Initial")
    INITIAL("Initial");
    private final String value;

    AgreementSpecState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AgreementSpecState fromValue(String v) {
        for (AgreementSpecState c: AgreementSpecState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
