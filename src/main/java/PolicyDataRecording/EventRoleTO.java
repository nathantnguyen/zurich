
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EventRole_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventRole_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RolePlayerClassRole_TO">
 *       &lt;sequence>
 *         &lt;element name="contextExternalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventRole_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "contextExternalReference"
})
public class EventRoleTO
    extends RolePlayerClassRoleTO
{

    protected String contextExternalReference;

    /**
     * Gets the value of the contextExternalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContextExternalReference() {
        return contextExternalReference;
    }

    /**
     * Sets the value of the contextExternalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContextExternalReference(String value) {
        this.contextExternalReference = value;
    }

}
