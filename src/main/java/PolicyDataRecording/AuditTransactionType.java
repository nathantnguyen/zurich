
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuditTransactionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AuditTransactionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="Interim"/>
 *     &lt;enumeration value="Revised Final"/>
 *     &lt;enumeration value="Revised Interim"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AuditTransactionType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum AuditTransactionType {

    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("Interim")
    INTERIM("Interim"),
    @XmlEnumValue("Revised Final")
    REVISED_FINAL("Revised Final"),
    @XmlEnumValue("Revised Interim")
    REVISED_INTERIM("Revised Interim");
    private final String value;

    AuditTransactionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AuditTransactionType fromValue(String v) {
        for (AuditTransactionType c: AuditTransactionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
