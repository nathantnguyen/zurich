
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ParticularMoneyProvision_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ParticularMoneyProvision_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO">
 *       &lt;sequence>
 *         &lt;element name="isEstimate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="moneyProvisionElements" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="paymentConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessRule_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="calculationMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/}CalculationMethod" minOccurs="0"/>
 *         &lt;element name="genericMoneyProvisio" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}GenericMoneyProvision_TO" minOccurs="0"/>
 *         &lt;element name="basedParticularMoneyProvisions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}ParticularMoneyProvision_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticularMoneyProvision_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "isEstimate",
    "moneyProvisionElements",
    "paymentConditions",
    "calculationMethod",
    "genericMoneyProvisio",
    "basedParticularMoneyProvisions"
})
public class ParticularMoneyProvisionTO
    extends MoneyProvisionTO
{

    protected Boolean isEstimate;
    protected List<MoneyProvisionElementTO> moneyProvisionElements;
    protected List<BusinessRuleTO> paymentConditions;
    protected CalculationMethod calculationMethod;
    protected GenericMoneyProvisionTO genericMoneyProvisio;
    protected List<ParticularMoneyProvisionTO> basedParticularMoneyProvisions;

    /**
     * Gets the value of the isEstimate property.
     * This getter has been renamed from isIsEstimate() to getIsEstimate() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsEstimate() {
        return isEstimate;
    }

    /**
     * Sets the value of the isEstimate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsEstimate(Boolean value) {
        this.isEstimate = value;
    }

    /**
     * Gets the value of the moneyProvisionElements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moneyProvisionElements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoneyProvisionElements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionElementTO }
     * 
     * 
     */
    public List<MoneyProvisionElementTO> getMoneyProvisionElements() {
        if (moneyProvisionElements == null) {
            moneyProvisionElements = new ArrayList<MoneyProvisionElementTO>();
        }
        return this.moneyProvisionElements;
    }

    /**
     * Gets the value of the paymentConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessRuleTO }
     * 
     * 
     */
    public List<BusinessRuleTO> getPaymentConditions() {
        if (paymentConditions == null) {
            paymentConditions = new ArrayList<BusinessRuleTO>();
        }
        return this.paymentConditions;
    }

    /**
     * Gets the value of the calculationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link CalculationMethod }
     *     
     */
    public CalculationMethod getCalculationMethod() {
        return calculationMethod;
    }

    /**
     * Sets the value of the calculationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link CalculationMethod }
     *     
     */
    public void setCalculationMethod(CalculationMethod value) {
        this.calculationMethod = value;
    }

    /**
     * Gets the value of the genericMoneyProvisio property.
     * 
     * @return
     *     possible object is
     *     {@link GenericMoneyProvisionTO }
     *     
     */
    public GenericMoneyProvisionTO getGenericMoneyProvisio() {
        return genericMoneyProvisio;
    }

    /**
     * Sets the value of the genericMoneyProvisio property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericMoneyProvisionTO }
     *     
     */
    public void setGenericMoneyProvisio(GenericMoneyProvisionTO value) {
        this.genericMoneyProvisio = value;
    }

    /**
     * Gets the value of the basedParticularMoneyProvisions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the basedParticularMoneyProvisions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBasedParticularMoneyProvisions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParticularMoneyProvisionTO }
     * 
     * 
     */
    public List<ParticularMoneyProvisionTO> getBasedParticularMoneyProvisions() {
        if (basedParticularMoneyProvisions == null) {
            basedParticularMoneyProvisions = new ArrayList<ParticularMoneyProvisionTO>();
        }
        return this.basedParticularMoneyProvisions;
    }

    /**
     * Sets the value of the moneyProvisionElements property.
     * 
     * @param moneyProvisionElements
     *     allowed object is
     *     {@link MoneyProvisionElementTO }
     *     
     */
    public void setMoneyProvisionElements(List<MoneyProvisionElementTO> moneyProvisionElements) {
        this.moneyProvisionElements = moneyProvisionElements;
    }

    /**
     * Sets the value of the paymentConditions property.
     * 
     * @param paymentConditions
     *     allowed object is
     *     {@link BusinessRuleTO }
     *     
     */
    public void setPaymentConditions(List<BusinessRuleTO> paymentConditions) {
        this.paymentConditions = paymentConditions;
    }

    /**
     * Sets the value of the basedParticularMoneyProvisions property.
     * 
     * @param basedParticularMoneyProvisions
     *     allowed object is
     *     {@link ParticularMoneyProvisionTO }
     *     
     */
    public void setBasedParticularMoneyProvisions(List<ParticularMoneyProvisionTO> basedParticularMoneyProvisions) {
        this.basedParticularMoneyProvisions = basedParticularMoneyProvisions;
    }

}
