
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyProvisionInvolvedInContract_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyProvisionInvolvedInContract_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContract_TO">
 *       &lt;sequence>
 *         &lt;element name="moneyProvision" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO" minOccurs="0"/>
 *         &lt;element name="moneyProvisionInvolvedInContractRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}MoneyProvisionRoleInContractType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyProvisionInvolvedInContract_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "moneyProvision",
    "moneyProvisionInvolvedInContractRootType"
})
public class MoneyProvisionInvolvedInContractTO
    extends RoleInContractTO
{

    protected MoneyProvisionTO moneyProvision;
    protected MoneyProvisionRoleInContractTypeTO moneyProvisionInvolvedInContractRootType;

    /**
     * Gets the value of the moneyProvision property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public MoneyProvisionTO getMoneyProvision() {
        return moneyProvision;
    }

    /**
     * Sets the value of the moneyProvision property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public void setMoneyProvision(MoneyProvisionTO value) {
        this.moneyProvision = value;
    }

    /**
     * Gets the value of the moneyProvisionInvolvedInContractRootType property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionRoleInContractTypeTO }
     *     
     */
    public MoneyProvisionRoleInContractTypeTO getMoneyProvisionInvolvedInContractRootType() {
        return moneyProvisionInvolvedInContractRootType;
    }

    /**
     * Sets the value of the moneyProvisionInvolvedInContractRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionRoleInContractTypeTO }
     *     
     */
    public void setMoneyProvisionInvolvedInContractRootType(MoneyProvisionRoleInContractTypeTO value) {
        this.moneyProvisionInvolvedInContractRootType = value;
    }

}
