
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FinancialAsset_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialAsset_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="assetFee" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AssetFee_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInFinancialAsset" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}RoleInFinancialAsset_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fungibleFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="hybridInstrumentFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="incrementalDenomination" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="instrumentVolatility" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="minimumDenomination" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="nextCallDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="otcIndicatorFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="publicTradedInstrumentFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="systematicInternalizerFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="assetMarketInformation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AssetMarketInformation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="assetPriceQuotation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AssetPriceQuotation_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="assetPrices" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AssetPrice_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialAsset_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "name",
    "description",
    "endDate",
    "startDate",
    "assetFee",
    "rolesInFinancialAsset",
    "fungibleFlag",
    "hybridInstrumentFlag",
    "incrementalDenomination",
    "instrumentVolatility",
    "minimumDenomination",
    "nextCallDate",
    "otcIndicatorFlag",
    "publicTradedInstrumentFlag",
    "systematicInternalizerFlag",
    "assetMarketInformation",
    "assetPriceQuotation",
    "assetPrices"
})
@XmlSeeAlso({
    DebtInstrumentTO.class,
    MarketableObjectTO.class,
    StructuredProductTO.class,
    CapitalInstrumentTO.class,
    ContractInstrumentTO.class,
    FundTO.class,
    BondTO.class,
    DerivativeTO.class,
    ShareTO.class,
    CompositeInstrumentTO.class
})
public class FinancialAssetTO
    extends BusinessModelObjectTO
{

    protected String name;
    protected String description;
    protected XMLGregorianCalendar endDate;
    protected XMLGregorianCalendar startDate;
    protected List<AssetFeeTO> assetFee;
    protected List<RoleInFinancialAssetTO> rolesInFinancialAsset;
    protected Boolean fungibleFlag;
    protected Boolean hybridInstrumentFlag;
    protected BaseCurrencyAmount incrementalDenomination;
    protected BigInteger instrumentVolatility;
    protected BaseCurrencyAmount minimumDenomination;
    protected XMLGregorianCalendar nextCallDate;
    protected Boolean otcIndicatorFlag;
    protected Boolean publicTradedInstrumentFlag;
    protected Boolean systematicInternalizerFlag;
    protected List<AssetMarketInformationTO> assetMarketInformation;
    protected List<AssetPriceQuotationTO> assetPriceQuotation;
    protected List<AssetPriceTO> assetPrices;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the assetFee property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assetFee property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssetFee().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssetFeeTO }
     * 
     * 
     */
    public List<AssetFeeTO> getAssetFee() {
        if (assetFee == null) {
            assetFee = new ArrayList<AssetFeeTO>();
        }
        return this.assetFee;
    }

    /**
     * Gets the value of the rolesInFinancialAsset property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInFinancialAsset property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInFinancialAsset().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInFinancialAssetTO }
     * 
     * 
     */
    public List<RoleInFinancialAssetTO> getRolesInFinancialAsset() {
        if (rolesInFinancialAsset == null) {
            rolesInFinancialAsset = new ArrayList<RoleInFinancialAssetTO>();
        }
        return this.rolesInFinancialAsset;
    }

    /**
     * Gets the value of the fungibleFlag property.
     * This getter has been renamed from isFungibleFlag() to getFungibleFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFungibleFlag() {
        return fungibleFlag;
    }

    /**
     * Sets the value of the fungibleFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFungibleFlag(Boolean value) {
        this.fungibleFlag = value;
    }

    /**
     * Gets the value of the hybridInstrumentFlag property.
     * This getter has been renamed from isHybridInstrumentFlag() to getHybridInstrumentFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHybridInstrumentFlag() {
        return hybridInstrumentFlag;
    }

    /**
     * Sets the value of the hybridInstrumentFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHybridInstrumentFlag(Boolean value) {
        this.hybridInstrumentFlag = value;
    }

    /**
     * Gets the value of the incrementalDenomination property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getIncrementalDenomination() {
        return incrementalDenomination;
    }

    /**
     * Sets the value of the incrementalDenomination property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setIncrementalDenomination(BaseCurrencyAmount value) {
        this.incrementalDenomination = value;
    }

    /**
     * Gets the value of the instrumentVolatility property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInstrumentVolatility() {
        return instrumentVolatility;
    }

    /**
     * Sets the value of the instrumentVolatility property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInstrumentVolatility(BigInteger value) {
        this.instrumentVolatility = value;
    }

    /**
     * Gets the value of the minimumDenomination property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMinimumDenomination() {
        return minimumDenomination;
    }

    /**
     * Sets the value of the minimumDenomination property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMinimumDenomination(BaseCurrencyAmount value) {
        this.minimumDenomination = value;
    }

    /**
     * Gets the value of the nextCallDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextCallDate() {
        return nextCallDate;
    }

    /**
     * Sets the value of the nextCallDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextCallDate(XMLGregorianCalendar value) {
        this.nextCallDate = value;
    }

    /**
     * Gets the value of the otcIndicatorFlag property.
     * This getter has been renamed from isOtcIndicatorFlag() to getOtcIndicatorFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOtcIndicatorFlag() {
        return otcIndicatorFlag;
    }

    /**
     * Sets the value of the otcIndicatorFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOtcIndicatorFlag(Boolean value) {
        this.otcIndicatorFlag = value;
    }

    /**
     * Gets the value of the publicTradedInstrumentFlag property.
     * This getter has been renamed from isPublicTradedInstrumentFlag() to getPublicTradedInstrumentFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPublicTradedInstrumentFlag() {
        return publicTradedInstrumentFlag;
    }

    /**
     * Sets the value of the publicTradedInstrumentFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPublicTradedInstrumentFlag(Boolean value) {
        this.publicTradedInstrumentFlag = value;
    }

    /**
     * Gets the value of the systematicInternalizerFlag property.
     * This getter has been renamed from isSystematicInternalizerFlag() to getSystematicInternalizerFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSystematicInternalizerFlag() {
        return systematicInternalizerFlag;
    }

    /**
     * Sets the value of the systematicInternalizerFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSystematicInternalizerFlag(Boolean value) {
        this.systematicInternalizerFlag = value;
    }

    /**
     * Gets the value of the assetMarketInformation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assetMarketInformation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssetMarketInformation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssetMarketInformationTO }
     * 
     * 
     */
    public List<AssetMarketInformationTO> getAssetMarketInformation() {
        if (assetMarketInformation == null) {
            assetMarketInformation = new ArrayList<AssetMarketInformationTO>();
        }
        return this.assetMarketInformation;
    }

    /**
     * Gets the value of the assetPriceQuotation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assetPriceQuotation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssetPriceQuotation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssetPriceQuotationTO }
     * 
     * 
     */
    public List<AssetPriceQuotationTO> getAssetPriceQuotation() {
        if (assetPriceQuotation == null) {
            assetPriceQuotation = new ArrayList<AssetPriceQuotationTO>();
        }
        return this.assetPriceQuotation;
    }

    /**
     * Gets the value of the assetPrices property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assetPrices property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssetPrices().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssetPriceTO }
     * 
     * 
     */
    public List<AssetPriceTO> getAssetPrices() {
        if (assetPrices == null) {
            assetPrices = new ArrayList<AssetPriceTO>();
        }
        return this.assetPrices;
    }

    /**
     * Sets the value of the assetFee property.
     * 
     * @param assetFee
     *     allowed object is
     *     {@link AssetFeeTO }
     *     
     */
    public void setAssetFee(List<AssetFeeTO> assetFee) {
        this.assetFee = assetFee;
    }

    /**
     * Sets the value of the rolesInFinancialAsset property.
     * 
     * @param rolesInFinancialAsset
     *     allowed object is
     *     {@link RoleInFinancialAssetTO }
     *     
     */
    public void setRolesInFinancialAsset(List<RoleInFinancialAssetTO> rolesInFinancialAsset) {
        this.rolesInFinancialAsset = rolesInFinancialAsset;
    }

    /**
     * Sets the value of the assetMarketInformation property.
     * 
     * @param assetMarketInformation
     *     allowed object is
     *     {@link AssetMarketInformationTO }
     *     
     */
    public void setAssetMarketInformation(List<AssetMarketInformationTO> assetMarketInformation) {
        this.assetMarketInformation = assetMarketInformation;
    }

    /**
     * Sets the value of the assetPriceQuotation property.
     * 
     * @param assetPriceQuotation
     *     allowed object is
     *     {@link AssetPriceQuotationTO }
     *     
     */
    public void setAssetPriceQuotation(List<AssetPriceQuotationTO> assetPriceQuotation) {
        this.assetPriceQuotation = assetPriceQuotation;
    }

    /**
     * Sets the value of the assetPrices property.
     * 
     * @param assetPrices
     *     allowed object is
     *     {@link AssetPriceTO }
     *     
     */
    public void setAssetPrices(List<AssetPriceTO> assetPrices) {
        this.assetPrices = assetPrices;
    }

}
