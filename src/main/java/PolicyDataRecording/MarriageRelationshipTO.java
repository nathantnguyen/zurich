
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarriageRelationship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarriageRelationship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerRelationship_TO">
 *       &lt;sequence>
 *         &lt;element name="legalStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}MarriageLegalStatus" minOccurs="0"/>
 *         &lt;element name="terminationReason" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}MarriageTerminationReason" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarriageRelationship_TO", propOrder = {
    "legalStatus",
    "terminationReason"
})
public class MarriageRelationshipTO
    extends RolePlayerRelationshipTO
{

    protected MarriageLegalStatus legalStatus;
    protected MarriageTerminationReason terminationReason;

    /**
     * Gets the value of the legalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link MarriageLegalStatus }
     *     
     */
    public MarriageLegalStatus getLegalStatus() {
        return legalStatus;
    }

    /**
     * Sets the value of the legalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarriageLegalStatus }
     *     
     */
    public void setLegalStatus(MarriageLegalStatus value) {
        this.legalStatus = value;
    }

    /**
     * Gets the value of the terminationReason property.
     * 
     * @return
     *     possible object is
     *     {@link MarriageTerminationReason }
     *     
     */
    public MarriageTerminationReason getTerminationReason() {
        return terminationReason;
    }

    /**
     * Sets the value of the terminationReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarriageTerminationReason }
     *     
     */
    public void setTerminationReason(MarriageTerminationReason value) {
        this.terminationReason = value;
    }

}
