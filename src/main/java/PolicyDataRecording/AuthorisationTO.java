
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Authorisation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Authorisation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO">
 *       &lt;sequence>
 *         &lt;element name="allowedActivityOccurrences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="authorisedPhyscalObjectReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="acceptedFinancialStatementObjectReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="approvedCommunicationContentObjectReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Authorisation_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "allowedActivityOccurrences",
    "authorisedPhyscalObjectReferences",
    "acceptedFinancialStatementObjectReference",
    "approvedCommunicationContentObjectReference"
})
@XmlSeeAlso({
    PreAuthorisationTO.class
})
public class AuthorisationTO
    extends AssessmentResultTO
{

    protected List<ActivityOccurrenceTO> allowedActivityOccurrences;
    protected List<ObjectReferenceTO> authorisedPhyscalObjectReferences;
    protected ObjectReferenceTO acceptedFinancialStatementObjectReference;
    protected ObjectReferenceTO approvedCommunicationContentObjectReference;

    /**
     * Gets the value of the allowedActivityOccurrences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allowedActivityOccurrences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllowedActivityOccurrences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceTO }
     * 
     * 
     */
    public List<ActivityOccurrenceTO> getAllowedActivityOccurrences() {
        if (allowedActivityOccurrences == null) {
            allowedActivityOccurrences = new ArrayList<ActivityOccurrenceTO>();
        }
        return this.allowedActivityOccurrences;
    }

    /**
     * Gets the value of the authorisedPhyscalObjectReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the authorisedPhyscalObjectReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAuthorisedPhyscalObjectReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getAuthorisedPhyscalObjectReferences() {
        if (authorisedPhyscalObjectReferences == null) {
            authorisedPhyscalObjectReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.authorisedPhyscalObjectReferences;
    }

    /**
     * Gets the value of the acceptedFinancialStatementObjectReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getAcceptedFinancialStatementObjectReference() {
        return acceptedFinancialStatementObjectReference;
    }

    /**
     * Sets the value of the acceptedFinancialStatementObjectReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setAcceptedFinancialStatementObjectReference(ObjectReferenceTO value) {
        this.acceptedFinancialStatementObjectReference = value;
    }

    /**
     * Gets the value of the approvedCommunicationContentObjectReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getApprovedCommunicationContentObjectReference() {
        return approvedCommunicationContentObjectReference;
    }

    /**
     * Sets the value of the approvedCommunicationContentObjectReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setApprovedCommunicationContentObjectReference(ObjectReferenceTO value) {
        this.approvedCommunicationContentObjectReference = value;
    }

    /**
     * Sets the value of the allowedActivityOccurrences property.
     * 
     * @param allowedActivityOccurrences
     *     allowed object is
     *     {@link ActivityOccurrenceTO }
     *     
     */
    public void setAllowedActivityOccurrences(List<ActivityOccurrenceTO> allowedActivityOccurrences) {
        this.allowedActivityOccurrences = allowedActivityOccurrences;
    }

    /**
     * Sets the value of the authorisedPhyscalObjectReferences property.
     * 
     * @param authorisedPhyscalObjectReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setAuthorisedPhyscalObjectReferences(List<ObjectReferenceTO> authorisedPhyscalObjectReferences) {
        this.authorisedPhyscalObjectReferences = authorisedPhyscalObjectReferences;
    }

}
