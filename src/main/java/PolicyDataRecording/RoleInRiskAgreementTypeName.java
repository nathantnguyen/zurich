
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInRiskAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInRiskAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Applicable Activities"/>
 *     &lt;enumeration value="Risk Agreement Reports"/>
 *     &lt;enumeration value="Risk Assessment Conditions"/>
 *     &lt;enumeration value="Statistics In Risk Agreement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInRiskAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum RoleInRiskAgreementTypeName {

    @XmlEnumValue("Applicable Activities")
    APPLICABLE_ACTIVITIES("Applicable Activities"),
    @XmlEnumValue("Risk Agreement Reports")
    RISK_AGREEMENT_REPORTS("Risk Agreement Reports"),
    @XmlEnumValue("Risk Assessment Conditions")
    RISK_ASSESSMENT_CONDITIONS("Risk Assessment Conditions"),
    @XmlEnumValue("Statistics In Risk Agreement")
    STATISTICS_IN_RISK_AGREEMENT("Statistics In Risk Agreement");
    private final String value;

    RoleInRiskAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInRiskAgreementTypeName fromValue(String v) {
        for (RoleInRiskAgreementTypeName c: RoleInRiskAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
