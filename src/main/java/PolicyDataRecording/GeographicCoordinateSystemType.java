
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeographicCoordinateSystemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GeographicCoordinateSystemType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EPSG:4326"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GeographicCoordinateSystemType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum GeographicCoordinateSystemType {

    @XmlEnumValue("EPSG:4326")
    EPSG_4326("EPSG:4326");
    private final String value;

    GeographicCoordinateSystemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GeographicCoordinateSystemType fromValue(String v) {
        for (GeographicCoordinateSystemType c: GeographicCoordinateSystemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
