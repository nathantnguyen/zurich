
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationMedium.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunicationMedium">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Email"/>
 *     &lt;enumeration value="Face To Face"/>
 *     &lt;enumeration value="Fax"/>
 *     &lt;enumeration value="Mail"/>
 *     &lt;enumeration value="Telephone"/>
 *     &lt;enumeration value="Claim Form"/>
 *     &lt;enumeration value="Internet"/>
 *     &lt;enumeration value="Telephone Direct"/>
 *     &lt;enumeration value="Sms"/>
 *     &lt;enumeration value="Wap"/>
 *     &lt;enumeration value="Mobile Phone"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommunicationMedium", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum CommunicationMedium {

    @XmlEnumValue("Email")
    EMAIL("Email"),
    @XmlEnumValue("Face To Face")
    FACE_TO_FACE("Face To Face"),
    @XmlEnumValue("Fax")
    FAX("Fax"),
    @XmlEnumValue("Mail")
    MAIL("Mail"),
    @XmlEnumValue("Telephone")
    TELEPHONE("Telephone"),
    @XmlEnumValue("Claim Form")
    CLAIM_FORM("Claim Form"),
    @XmlEnumValue("Internet")
    INTERNET("Internet"),
    @XmlEnumValue("Telephone Direct")
    TELEPHONE_DIRECT("Telephone Direct"),
    @XmlEnumValue("Sms")
    SMS("Sms"),
    @XmlEnumValue("Wap")
    WAP("Wap"),
    @XmlEnumValue("Mobile Phone")
    MOBILE_PHONE("Mobile Phone");
    private final String value;

    CommunicationMedium(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationMedium fromValue(String v) {
        for (CommunicationMedium c: CommunicationMedium.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
