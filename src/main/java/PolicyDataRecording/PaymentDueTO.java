
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PaymentDue_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentDue_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}FinancialTransaction_TO">
 *       &lt;sequence>
 *         &lt;element name="handlingMoneyOutScheduler" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyOutScheduler_TO" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="paymentDuePeriodStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="paymentDuePeriodEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="writtenOffDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="minimumPaymentDueAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="voucherNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="issueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="statementElements" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}StatementElement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}PaymentDueStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="settlingPayments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Payment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentDue_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "handlingMoneyOutScheduler",
    "dueDate",
    "paymentDuePeriodStartDate",
    "paymentDuePeriodEndDate",
    "writtenOffDate",
    "minimumPaymentDueAmount",
    "voucherNumber",
    "issueDate",
    "statementElements",
    "status",
    "settlingPayments"
})
public class PaymentDueTO
    extends FinancialTransactionTO
{

    protected MoneyOutSchedulerTO handlingMoneyOutScheduler;
    protected XMLGregorianCalendar dueDate;
    protected XMLGregorianCalendar paymentDuePeriodStartDate;
    protected XMLGregorianCalendar paymentDuePeriodEndDate;
    protected XMLGregorianCalendar writtenOffDate;
    protected BaseCurrencyAmount minimumPaymentDueAmount;
    protected String voucherNumber;
    protected XMLGregorianCalendar issueDate;
    protected List<StatementElementTO> statementElements;
    protected List<PaymentDueStatusTO> status;
    protected List<PaymentTO> settlingPayments;

    /**
     * Gets the value of the handlingMoneyOutScheduler property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyOutSchedulerTO }
     *     
     */
    public MoneyOutSchedulerTO getHandlingMoneyOutScheduler() {
        return handlingMoneyOutScheduler;
    }

    /**
     * Sets the value of the handlingMoneyOutScheduler property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyOutSchedulerTO }
     *     
     */
    public void setHandlingMoneyOutScheduler(MoneyOutSchedulerTO value) {
        this.handlingMoneyOutScheduler = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the paymentDuePeriodStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDuePeriodStartDate() {
        return paymentDuePeriodStartDate;
    }

    /**
     * Sets the value of the paymentDuePeriodStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDuePeriodStartDate(XMLGregorianCalendar value) {
        this.paymentDuePeriodStartDate = value;
    }

    /**
     * Gets the value of the paymentDuePeriodEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDuePeriodEndDate() {
        return paymentDuePeriodEndDate;
    }

    /**
     * Sets the value of the paymentDuePeriodEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDuePeriodEndDate(XMLGregorianCalendar value) {
        this.paymentDuePeriodEndDate = value;
    }

    /**
     * Gets the value of the writtenOffDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWrittenOffDate() {
        return writtenOffDate;
    }

    /**
     * Sets the value of the writtenOffDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWrittenOffDate(XMLGregorianCalendar value) {
        this.writtenOffDate = value;
    }

    /**
     * Gets the value of the minimumPaymentDueAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMinimumPaymentDueAmount() {
        return minimumPaymentDueAmount;
    }

    /**
     * Sets the value of the minimumPaymentDueAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMinimumPaymentDueAmount(BaseCurrencyAmount value) {
        this.minimumPaymentDueAmount = value;
    }

    /**
     * Gets the value of the voucherNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherNumber() {
        return voucherNumber;
    }

    /**
     * Sets the value of the voucherNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherNumber(String value) {
        this.voucherNumber = value;
    }

    /**
     * Gets the value of the issueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIssueDate() {
        return issueDate;
    }

    /**
     * Sets the value of the issueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIssueDate(XMLGregorianCalendar value) {
        this.issueDate = value;
    }

    /**
     * Gets the value of the statementElements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the statementElements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatementElements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatementElementTO }
     * 
     * 
     */
    public List<StatementElementTO> getStatementElements() {
        if (statementElements == null) {
            statementElements = new ArrayList<StatementElementTO>();
        }
        return this.statementElements;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentDueStatusTO }
     * 
     * 
     */
    public List<PaymentDueStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<PaymentDueStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the settlingPayments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the settlingPayments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSettlingPayments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentTO }
     * 
     * 
     */
    public List<PaymentTO> getSettlingPayments() {
        if (settlingPayments == null) {
            settlingPayments = new ArrayList<PaymentTO>();
        }
        return this.settlingPayments;
    }

    /**
     * Sets the value of the statementElements property.
     * 
     * @param statementElements
     *     allowed object is
     *     {@link StatementElementTO }
     *     
     */
    public void setStatementElements(List<StatementElementTO> statementElements) {
        this.statementElements = statementElements;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link PaymentDueStatusTO }
     *     
     */
    public void setStatus(List<PaymentDueStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the settlingPayments property.
     * 
     * @param settlingPayments
     *     allowed object is
     *     {@link PaymentTO }
     *     
     */
    public void setSettlingPayments(List<PaymentTO> settlingPayments) {
        this.settlingPayments = settlingPayments;
    }

}
