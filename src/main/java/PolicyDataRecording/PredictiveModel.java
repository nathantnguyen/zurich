
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PredictiveModel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PredictiveModel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Attrition"/>
 *     &lt;enumeration value="Cross Sell"/>
 *     &lt;enumeration value="Customer Retention"/>
 *     &lt;enumeration value="Up Sell"/>
 *     &lt;enumeration value="Winback"/>
 *     &lt;enumeration value="Cross Sell Life To Health"/>
 *     &lt;enumeration value="Cross Sell Life To Non Life"/>
 *     &lt;enumeration value="Cross Sell Non Life To Health"/>
 *     &lt;enumeration value="Up Sell Fire House Cover To Full House Cover"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PredictiveModel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum PredictiveModel {

    @XmlEnumValue("Attrition")
    ATTRITION("Attrition"),
    @XmlEnumValue("Cross Sell")
    CROSS_SELL("Cross Sell"),
    @XmlEnumValue("Customer Retention")
    CUSTOMER_RETENTION("Customer Retention"),
    @XmlEnumValue("Up Sell")
    UP_SELL("Up Sell"),
    @XmlEnumValue("Winback")
    WINBACK("Winback"),
    @XmlEnumValue("Cross Sell Life To Health")
    CROSS_SELL_LIFE_TO_HEALTH("Cross Sell Life To Health"),
    @XmlEnumValue("Cross Sell Life To Non Life")
    CROSS_SELL_LIFE_TO_NON_LIFE("Cross Sell Life To Non Life"),
    @XmlEnumValue("Cross Sell Non Life To Health")
    CROSS_SELL_NON_LIFE_TO_HEALTH("Cross Sell Non Life To Health"),
    @XmlEnumValue("Up Sell Fire House Cover To Full House Cover")
    UP_SELL_FIRE_HOUSE_COVER_TO_FULL_HOUSE_COVER("Up Sell Fire House Cover To Full House Cover");
    private final String value;

    PredictiveModel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PredictiveModel fromValue(String v) {
        for (PredictiveModel c: PredictiveModel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
