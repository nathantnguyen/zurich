
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HeatingUnitProtectiveMaterial.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HeatingUnitProtectiveMaterial">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Brick"/>
 *     &lt;enumeration value="Ceramic Tile"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HeatingUnitProtectiveMaterial", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum HeatingUnitProtectiveMaterial {

    @XmlEnumValue("Brick")
    BRICK("Brick"),
    @XmlEnumValue("Ceramic Tile")
    CERAMIC_TILE("Ceramic Tile");
    private final String value;

    HeatingUnitProtectiveMaterial(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HeatingUnitProtectiveMaterial fromValue(String v) {
        for (HeatingUnitProtectiveMaterial c: HeatingUnitProtectiveMaterial.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
