
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApplicationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApplicationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Batch"/>
 *     &lt;enumeration value="Conversational"/>
 *     &lt;enumeration value="Olap"/>
 *     &lt;enumeration value="Oltp"/>
 *     &lt;enumeration value="Web"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ApplicationType", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/")
@XmlEnum
public enum ApplicationType {

    @XmlEnumValue("Batch")
    BATCH("Batch"),
    @XmlEnumValue("Conversational")
    CONVERSATIONAL("Conversational"),
    @XmlEnumValue("Olap")
    OLAP("Olap"),
    @XmlEnumValue("Oltp")
    OLTP("Oltp"),
    @XmlEnumValue("Web")
    WEB("Web");
    private final String value;

    ApplicationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApplicationType fromValue(String v) {
        for (ApplicationType c: ApplicationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
