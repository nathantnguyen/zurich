
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Share_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Share_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="totalNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Share_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "totalNumber"
})
public class ShareTO
    extends FinancialAssetTO
{

    protected BigInteger totalNumber;

    /**
     * Gets the value of the totalNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalNumber() {
        return totalNumber;
    }

    /**
     * Sets the value of the totalNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalNumber(BigInteger value) {
        this.totalNumber = value;
    }

}
