
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Open"/>
 *     &lt;enumeration value="Draft"/>
 *     &lt;enumeration value="Pending Close"/>
 *     &lt;enumeration value="Closed"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Final"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AccountState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum AccountState {

    @XmlEnumValue("Open")
    OPEN("Open"),
    @XmlEnumValue("Draft")
    DRAFT("Draft"),
    @XmlEnumValue("Pending Close")
    PENDING_CLOSE("Pending Close"),
    @XmlEnumValue("Closed")
    CLOSED("Closed"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Final")
    FINAL("Final");
    private final String value;

    AccountState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountState fromValue(String v) {
        for (AccountState c: AccountState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
