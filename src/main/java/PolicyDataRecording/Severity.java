
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Severity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Severity">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Permanent"/>
 *     &lt;enumeration value="Above Healthy Level"/>
 *     &lt;enumeration value="Below Healthy Level"/>
 *     &lt;enumeration value="Far Above Healthy Level"/>
 *     &lt;enumeration value="Far Below Healthy Level"/>
 *     &lt;enumeration value="Healthy"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Severity", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum Severity {

    @XmlEnumValue("Permanent")
    PERMANENT("Permanent"),
    @XmlEnumValue("Above Healthy Level")
    ABOVE_HEALTHY_LEVEL("Above Healthy Level"),
    @XmlEnumValue("Below Healthy Level")
    BELOW_HEALTHY_LEVEL("Below Healthy Level"),
    @XmlEnumValue("Far Above Healthy Level")
    FAR_ABOVE_HEALTHY_LEVEL("Far Above Healthy Level"),
    @XmlEnumValue("Far Below Healthy Level")
    FAR_BELOW_HEALTHY_LEVEL("Far Below Healthy Level"),
    @XmlEnumValue("Healthy")
    HEALTHY("Healthy");
    private final String value;

    Severity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Severity fromValue(String v) {
        for (Severity c: Severity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
