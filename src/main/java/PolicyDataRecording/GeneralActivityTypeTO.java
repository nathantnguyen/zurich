
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeneralActivityType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeneralActivityType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}GeneralActivityTypeName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeneralActivityType_TO", propOrder = {
    "name"
})
public class GeneralActivityTypeTO
    extends TypeTO
{

    protected GeneralActivityTypeName name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link GeneralActivityTypeName }
     *     
     */
    public GeneralActivityTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralActivityTypeName }
     *     
     */
    public void setName(GeneralActivityTypeName value) {
        this.name = value;
    }

}
