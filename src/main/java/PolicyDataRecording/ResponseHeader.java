
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0" form="qualified"/>
 *         &lt;element name="systemName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0" form="qualified"/>
 *         &lt;element name="transactionNotification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}TransactionNotification_TO" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseHeader", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/", propOrder = {
    "messageReference",
    "systemName",
    "transactionNotification"
})
public class ResponseHeader {

    protected String messageReference;
    protected String systemName;
    protected TransactionNotificationTO transactionNotification;

    /**
     * Gets the value of the messageReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageReference() {
        return messageReference;
    }

    /**
     * Sets the value of the messageReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageReference(String value) {
        this.messageReference = value;
    }

    /**
     * Gets the value of the systemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemName() {
        return systemName;
    }

    /**
     * Sets the value of the systemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemName(String value) {
        this.systemName = value;
    }

    /**
     * Gets the value of the transactionNotification property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionNotificationTO }
     *     
     */
    public TransactionNotificationTO getTransactionNotification() {
        return transactionNotification;
    }

    /**
     * Sets the value of the transactionNotification property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionNotificationTO }
     *     
     */
    public void setTransactionNotification(TransactionNotificationTO value) {
        this.transactionNotification = value;
    }

}
