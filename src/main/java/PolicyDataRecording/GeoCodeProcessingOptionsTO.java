
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeoCodeProcessingOptions_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeoCodeProcessingOptions_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="matchingOptions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeoCodeMatchingOptions_TO" minOccurs="0"/>
 *         &lt;element name="geoCodingOptions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeoCodingOptions_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeoCodeProcessingOptions_TO", propOrder = {
    "matchingOptions",
    "geoCodingOptions"
})
public class GeoCodeProcessingOptionsTO
    extends DependentObjectTO
{

    protected GeoCodeMatchingOptionsTO matchingOptions;
    protected GeoCodingOptionsTO geoCodingOptions;

    /**
     * Gets the value of the matchingOptions property.
     * 
     * @return
     *     possible object is
     *     {@link GeoCodeMatchingOptionsTO }
     *     
     */
    public GeoCodeMatchingOptionsTO getMatchingOptions() {
        return matchingOptions;
    }

    /**
     * Sets the value of the matchingOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeoCodeMatchingOptionsTO }
     *     
     */
    public void setMatchingOptions(GeoCodeMatchingOptionsTO value) {
        this.matchingOptions = value;
    }

    /**
     * Gets the value of the geoCodingOptions property.
     * 
     * @return
     *     possible object is
     *     {@link GeoCodingOptionsTO }
     *     
     */
    public GeoCodingOptionsTO getGeoCodingOptions() {
        return geoCodingOptions;
    }

    /**
     * Sets the value of the geoCodingOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeoCodingOptionsTO }
     *     
     */
    public void setGeoCodingOptions(GeoCodingOptionsTO value) {
        this.geoCodingOptions = value;
    }

}
