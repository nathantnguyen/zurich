
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanInterestPaymentMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LoanInterestPaymentMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Add On Interest"/>
 *     &lt;enumeration value="Separate Interest Payment"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LoanInterestPaymentMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum LoanInterestPaymentMethod {

    @XmlEnumValue("Add On Interest")
    ADD_ON_INTEREST("Add On Interest"),
    @XmlEnumValue("Separate Interest Payment")
    SEPARATE_INTEREST_PAYMENT("Separate Interest Payment");
    private final String value;

    LoanInterestPaymentMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LoanInterestPaymentMethod fromValue(String v) {
        for (LoanInterestPaymentMethod c: LoanInterestPaymentMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
