
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CountryCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CountryCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Fj"/>
 *     &lt;enumeration value="Aus"/>
 *     &lt;enumeration value="Jor"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CountryCode", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum CountryCode {

    @XmlEnumValue("Fj")
    FJ("Fj"),
    @XmlEnumValue("Aus")
    AUS("Aus"),
    @XmlEnumValue("Jor")
    JOR("Jor");
    private final String value;

    CountryCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CountryCode fromValue(String v) {
        for (CountryCode c: CountryCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
