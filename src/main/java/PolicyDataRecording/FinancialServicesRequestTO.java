
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialServicesRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="exchangeRateIndex" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}ExchangeRateIndex_TO" minOccurs="0"/>
 *         &lt;element name="financialServiceRequestRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRequestType_TO" minOccurs="0"/>
 *         &lt;element name="requestedAssessmentResult" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="triggeringEvent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Event_TO" minOccurs="0"/>
 *         &lt;element name="productGroups" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ProductGroup_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="includedProduct" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}FinancialServicesProduct_TO" minOccurs="0"/>
 *         &lt;element name="intermediaryAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}IntermediaryAgreement_TO" minOccurs="0"/>
 *         &lt;element name="financialServicesRequestDetail" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRequestDetail_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "exchangeRateIndex",
    "financialServiceRequestRootType",
    "requestedAssessmentResult",
    "triggeringEvent",
    "productGroups",
    "includedProduct",
    "intermediaryAgreement",
    "financialServicesRequestDetail"
})
@XmlSeeAlso({
    StatusChangeRequestTO.class,
    AssessmentRequestTO.class,
    PaymentAuthorisationRequestTO.class,
    FinancialMovementRequestTO.class
})
public class FinancialServicesRequestTO
    extends ContractRequestTO
{

    protected ExchangeRateIndexTO exchangeRateIndex;
    protected FinancialServicesRequestTypeTO financialServiceRequestRootType;
    protected ObjectReferenceTO requestedAssessmentResult;
    protected EventTO triggeringEvent;
    protected List<ProductGroupTO> productGroups;
    protected FinancialServicesProductTO includedProduct;
    protected IntermediaryAgreementTO intermediaryAgreement;
    protected FinancialServicesRequestDetailTO financialServicesRequestDetail;

    /**
     * Gets the value of the exchangeRateIndex property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateIndexTO }
     *     
     */
    public ExchangeRateIndexTO getExchangeRateIndex() {
        return exchangeRateIndex;
    }

    /**
     * Sets the value of the exchangeRateIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateIndexTO }
     *     
     */
    public void setExchangeRateIndex(ExchangeRateIndexTO value) {
        this.exchangeRateIndex = value;
    }

    /**
     * Gets the value of the financialServiceRequestRootType property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesRequestTypeTO }
     *     
     */
    public FinancialServicesRequestTypeTO getFinancialServiceRequestRootType() {
        return financialServiceRequestRootType;
    }

    /**
     * Sets the value of the financialServiceRequestRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesRequestTypeTO }
     *     
     */
    public void setFinancialServiceRequestRootType(FinancialServicesRequestTypeTO value) {
        this.financialServiceRequestRootType = value;
    }

    /**
     * Gets the value of the requestedAssessmentResult property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getRequestedAssessmentResult() {
        return requestedAssessmentResult;
    }

    /**
     * Sets the value of the requestedAssessmentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setRequestedAssessmentResult(ObjectReferenceTO value) {
        this.requestedAssessmentResult = value;
    }

    /**
     * Gets the value of the triggeringEvent property.
     * 
     * @return
     *     possible object is
     *     {@link EventTO }
     *     
     */
    public EventTO getTriggeringEvent() {
        return triggeringEvent;
    }

    /**
     * Sets the value of the triggeringEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventTO }
     *     
     */
    public void setTriggeringEvent(EventTO value) {
        this.triggeringEvent = value;
    }

    /**
     * Gets the value of the productGroups property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productGroups property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductGroups().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductGroupTO }
     * 
     * 
     */
    public List<ProductGroupTO> getProductGroups() {
        if (productGroups == null) {
            productGroups = new ArrayList<ProductGroupTO>();
        }
        return this.productGroups;
    }

    /**
     * Gets the value of the includedProduct property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesProductTO }
     *     
     */
    public FinancialServicesProductTO getIncludedProduct() {
        return includedProduct;
    }

    /**
     * Sets the value of the includedProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesProductTO }
     *     
     */
    public void setIncludedProduct(FinancialServicesProductTO value) {
        this.includedProduct = value;
    }

    /**
     * Gets the value of the intermediaryAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public IntermediaryAgreementTO getIntermediaryAgreement() {
        return intermediaryAgreement;
    }

    /**
     * Sets the value of the intermediaryAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public void setIntermediaryAgreement(IntermediaryAgreementTO value) {
        this.intermediaryAgreement = value;
    }

    /**
     * Gets the value of the financialServicesRequestDetail property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesRequestDetailTO }
     *     
     */
    public FinancialServicesRequestDetailTO getFinancialServicesRequestDetail() {
        return financialServicesRequestDetail;
    }

    /**
     * Sets the value of the financialServicesRequestDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesRequestDetailTO }
     *     
     */
    public void setFinancialServicesRequestDetail(FinancialServicesRequestDetailTO value) {
        this.financialServicesRequestDetail = value;
    }

    /**
     * Sets the value of the productGroups property.
     * 
     * @param productGroups
     *     allowed object is
     *     {@link ProductGroupTO }
     *     
     */
    public void setProductGroups(List<ProductGroupTO> productGroups) {
        this.productGroups = productGroups;
    }

}
