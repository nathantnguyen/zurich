
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketableObjectTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MarketableObjectTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Agricultural Commodity"/>
 *     &lt;enumeration value="Fine Art Commodity"/>
 *     &lt;enumeration value="Mineral Commodity"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MarketableObjectTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum MarketableObjectTypeName {

    @XmlEnumValue("Agricultural Commodity")
    AGRICULTURAL_COMMODITY("Agricultural Commodity"),
    @XmlEnumValue("Fine Art Commodity")
    FINE_ART_COMMODITY("Fine Art Commodity"),
    @XmlEnumValue("Mineral Commodity")
    MINERAL_COMMODITY("Mineral Commodity");
    private final String value;

    MarketableObjectTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MarketableObjectTypeName fromValue(String v) {
        for (MarketableObjectTypeName c: MarketableObjectTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
