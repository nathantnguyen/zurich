
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialTransactionRelationship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialTransactionRelationship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Relationship_TO">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="replacedFinancialTransactionReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="settledFinancialTransactionReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="contraFinancialTransactionReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="basisFinancialTransactionReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="usedFinancialTransactionReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialTransactionRelationship_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "amount",
    "replacedFinancialTransactionReference",
    "settledFinancialTransactionReference",
    "contraFinancialTransactionReference",
    "basisFinancialTransactionReference",
    "usedFinancialTransactionReference"
})
public class FinancialTransactionRelationshipTO
    extends RelationshipTO
{

    protected BaseCurrencyAmount amount;
    protected ObjectReferenceTO replacedFinancialTransactionReference;
    protected ObjectReferenceTO settledFinancialTransactionReference;
    protected ObjectReferenceTO contraFinancialTransactionReference;
    protected ObjectReferenceTO basisFinancialTransactionReference;
    protected ObjectReferenceTO usedFinancialTransactionReference;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAmount(BaseCurrencyAmount value) {
        this.amount = value;
    }

    /**
     * Gets the value of the replacedFinancialTransactionReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getReplacedFinancialTransactionReference() {
        return replacedFinancialTransactionReference;
    }

    /**
     * Sets the value of the replacedFinancialTransactionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setReplacedFinancialTransactionReference(ObjectReferenceTO value) {
        this.replacedFinancialTransactionReference = value;
    }

    /**
     * Gets the value of the settledFinancialTransactionReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getSettledFinancialTransactionReference() {
        return settledFinancialTransactionReference;
    }

    /**
     * Sets the value of the settledFinancialTransactionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSettledFinancialTransactionReference(ObjectReferenceTO value) {
        this.settledFinancialTransactionReference = value;
    }

    /**
     * Gets the value of the contraFinancialTransactionReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getContraFinancialTransactionReference() {
        return contraFinancialTransactionReference;
    }

    /**
     * Sets the value of the contraFinancialTransactionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setContraFinancialTransactionReference(ObjectReferenceTO value) {
        this.contraFinancialTransactionReference = value;
    }

    /**
     * Gets the value of the basisFinancialTransactionReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getBasisFinancialTransactionReference() {
        return basisFinancialTransactionReference;
    }

    /**
     * Sets the value of the basisFinancialTransactionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setBasisFinancialTransactionReference(ObjectReferenceTO value) {
        this.basisFinancialTransactionReference = value;
    }

    /**
     * Gets the value of the usedFinancialTransactionReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getUsedFinancialTransactionReference() {
        return usedFinancialTransactionReference;
    }

    /**
     * Sets the value of the usedFinancialTransactionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setUsedFinancialTransactionReference(ObjectReferenceTO value) {
        this.usedFinancialTransactionReference = value;
    }

}
