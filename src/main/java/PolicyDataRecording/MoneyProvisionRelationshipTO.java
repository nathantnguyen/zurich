
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyProvisionRelationship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyProvisionRelationship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Relationship_TO">
 *       &lt;sequence>
 *         &lt;element name="sourceMoneyProvisionReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="targetMoneyProvisionReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyProvisionRelationship_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "sourceMoneyProvisionReference",
    "targetMoneyProvisionReference"
})
public class MoneyProvisionRelationshipTO
    extends RelationshipTO
{

    protected ObjectReferenceTO sourceMoneyProvisionReference;
    protected ObjectReferenceTO targetMoneyProvisionReference;

    /**
     * Gets the value of the sourceMoneyProvisionReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getSourceMoneyProvisionReference() {
        return sourceMoneyProvisionReference;
    }

    /**
     * Sets the value of the sourceMoneyProvisionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSourceMoneyProvisionReference(ObjectReferenceTO value) {
        this.sourceMoneyProvisionReference = value;
    }

    /**
     * Gets the value of the targetMoneyProvisionReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getTargetMoneyProvisionReference() {
        return targetMoneyProvisionReference;
    }

    /**
     * Sets the value of the targetMoneyProvisionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setTargetMoneyProvisionReference(ObjectReferenceTO value) {
        this.targetMoneyProvisionReference = value;
    }

}
