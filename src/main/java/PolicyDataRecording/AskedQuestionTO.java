
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AskedQuestion_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AskedQuestion_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}SpecifiedContent_TO">
 *       &lt;sequence>
 *         &lt;element name="selectedAnswers" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}selectedanswer_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="answer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}FreeTextAnswer_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AskedQuestion_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "selectedAnswers",
    "answer"
})
public class AskedQuestionTO
    extends SpecifiedContentTO
{

    protected List<SelectedanswerTO> selectedAnswers;
    protected FreeTextAnswerTO answer;

    /**
     * Gets the value of the selectedAnswers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectedAnswers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectedAnswers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SelectedanswerTO }
     * 
     * 
     */
    public List<SelectedanswerTO> getSelectedAnswers() {
        if (selectedAnswers == null) {
            selectedAnswers = new ArrayList<SelectedanswerTO>();
        }
        return this.selectedAnswers;
    }

    /**
     * Gets the value of the answer property.
     * 
     * @return
     *     possible object is
     *     {@link FreeTextAnswerTO }
     *     
     */
    public FreeTextAnswerTO getAnswer() {
        return answer;
    }

    /**
     * Sets the value of the answer property.
     * 
     * @param value
     *     allowed object is
     *     {@link FreeTextAnswerTO }
     *     
     */
    public void setAnswer(FreeTextAnswerTO value) {
        this.answer = value;
    }

    /**
     * Sets the value of the selectedAnswers property.
     * 
     * @param selectedAnswers
     *     allowed object is
     *     {@link SelectedanswerTO }
     *     
     */
    public void setSelectedAnswers(List<SelectedanswerTO> selectedAnswers) {
        this.selectedAnswers = selectedAnswers;
    }

}
