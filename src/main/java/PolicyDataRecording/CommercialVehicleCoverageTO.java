
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommercialVehicleCoverage_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialVehicleCoverage_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CoverageComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="coveredVehicleSymbol" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}CoveredVehicleSymbol" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialVehicleCoverage_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "coveredVehicleSymbol"
})
public class CommercialVehicleCoverageTO
    extends CoverageComponentTO
{

    protected CoveredVehicleSymbol coveredVehicleSymbol;

    /**
     * Gets the value of the coveredVehicleSymbol property.
     * 
     * @return
     *     possible object is
     *     {@link CoveredVehicleSymbol }
     *     
     */
    public CoveredVehicleSymbol getCoveredVehicleSymbol() {
        return coveredVehicleSymbol;
    }

    /**
     * Sets the value of the coveredVehicleSymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoveredVehicleSymbol }
     *     
     */
    public void setCoveredVehicleSymbol(CoveredVehicleSymbol value) {
        this.coveredVehicleSymbol = value;
    }

}
