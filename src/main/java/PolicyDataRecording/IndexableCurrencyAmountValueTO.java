
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for IndexableCurrencyAmountValue_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IndexableCurrencyAmountValue_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="valueStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="valueEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="valueDateAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndexableCurrencyAmountValue_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "valueStartDate",
    "valueEndDate",
    "valueDateAmount"
})
public class IndexableCurrencyAmountValueTO
    extends BaseTransferObject
{

    protected XMLGregorianCalendar valueStartDate;
    protected XMLGregorianCalendar valueEndDate;
    protected BaseCurrencyAmount valueDateAmount;

    /**
     * Gets the value of the valueStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValueStartDate() {
        return valueStartDate;
    }

    /**
     * Sets the value of the valueStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValueStartDate(XMLGregorianCalendar value) {
        this.valueStartDate = value;
    }

    /**
     * Gets the value of the valueEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValueEndDate() {
        return valueEndDate;
    }

    /**
     * Sets the value of the valueEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValueEndDate(XMLGregorianCalendar value) {
        this.valueEndDate = value;
    }

    /**
     * Gets the value of the valueDateAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getValueDateAmount() {
        return valueDateAmount;
    }

    /**
     * Sets the value of the valueDateAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setValueDateAmount(BaseCurrencyAmount value) {
        this.valueDateAmount = value;
    }

}
