
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for LegalAction_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegalAction_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="verdict" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="verdictDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="proceedingsStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="majorCitation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="drivingCitation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="speedingCitation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="causeOfDispute" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/DisputeResolution/DisputeResolutionEnumerationsAndStates/}CauseOfDispute" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="totalCost" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegalAction_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/DisputeResolution/", propOrder = {
    "description",
    "startDate",
    "endDate",
    "verdict",
    "verdictDate",
    "proceedingsStartDate",
    "majorCitation",
    "drivingCitation",
    "speedingCitation",
    "causeOfDispute",
    "externalReference",
    "totalCost",
    "alternateReference"
})
public class LegalActionTO
    extends BusinessModelObjectTO
{

    protected String description;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected String verdict;
    protected XMLGregorianCalendar verdictDate;
    protected XMLGregorianCalendar proceedingsStartDate;
    protected Boolean majorCitation;
    protected Boolean drivingCitation;
    protected Boolean speedingCitation;
    protected CauseOfDispute causeOfDispute;
    protected String externalReference;
    protected BaseCurrencyAmount totalCost;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the verdict property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerdict() {
        return verdict;
    }

    /**
     * Sets the value of the verdict property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerdict(String value) {
        this.verdict = value;
    }

    /**
     * Gets the value of the verdictDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVerdictDate() {
        return verdictDate;
    }

    /**
     * Sets the value of the verdictDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVerdictDate(XMLGregorianCalendar value) {
        this.verdictDate = value;
    }

    /**
     * Gets the value of the proceedingsStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProceedingsStartDate() {
        return proceedingsStartDate;
    }

    /**
     * Sets the value of the proceedingsStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProceedingsStartDate(XMLGregorianCalendar value) {
        this.proceedingsStartDate = value;
    }

    /**
     * Gets the value of the majorCitation property.
     * This getter has been renamed from isMajorCitation() to getMajorCitation() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMajorCitation() {
        return majorCitation;
    }

    /**
     * Sets the value of the majorCitation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMajorCitation(Boolean value) {
        this.majorCitation = value;
    }

    /**
     * Gets the value of the drivingCitation property.
     * This getter has been renamed from isDrivingCitation() to getDrivingCitation() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDrivingCitation() {
        return drivingCitation;
    }

    /**
     * Sets the value of the drivingCitation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDrivingCitation(Boolean value) {
        this.drivingCitation = value;
    }

    /**
     * Gets the value of the speedingCitation property.
     * This getter has been renamed from isSpeedingCitation() to getSpeedingCitation() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSpeedingCitation() {
        return speedingCitation;
    }

    /**
     * Sets the value of the speedingCitation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSpeedingCitation(Boolean value) {
        this.speedingCitation = value;
    }

    /**
     * Gets the value of the causeOfDispute property.
     * 
     * @return
     *     possible object is
     *     {@link CauseOfDispute }
     *     
     */
    public CauseOfDispute getCauseOfDispute() {
        return causeOfDispute;
    }

    /**
     * Sets the value of the causeOfDispute property.
     * 
     * @param value
     *     allowed object is
     *     {@link CauseOfDispute }
     *     
     */
    public void setCauseOfDispute(CauseOfDispute value) {
        this.causeOfDispute = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the totalCost property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTotalCost() {
        return totalCost;
    }

    /**
     * Sets the value of the totalCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTotalCost(BaseCurrencyAmount value) {
        this.totalCost = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
