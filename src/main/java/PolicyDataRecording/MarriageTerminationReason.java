
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarriageTerminationReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MarriageTerminationReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Widowed"/>
 *     &lt;enumeration value="Divorced"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MarriageTerminationReason", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum MarriageTerminationReason {

    @XmlEnumValue("Widowed")
    WIDOWED("Widowed"),
    @XmlEnumValue("Divorced")
    DIVORCED("Divorced");
    private final String value;

    MarriageTerminationReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MarriageTerminationReason fromValue(String v) {
        for (MarriageTerminationReason c: MarriageTerminationReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
