
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DiscountCategory.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DiscountCategory">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Single Car"/>
 *     &lt;enumeration value="Preferred"/>
 *     &lt;enumeration value="All Coverage"/>
 *     &lt;enumeration value="Multi Car"/>
 *     &lt;enumeration value="No Discount"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DiscountCategory", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum DiscountCategory {

    @XmlEnumValue("Single Car")
    SINGLE_CAR("Single Car"),
    @XmlEnumValue("Preferred")
    PREFERRED("Preferred"),
    @XmlEnumValue("All Coverage")
    ALL_COVERAGE("All Coverage"),
    @XmlEnumValue("Multi Car")
    MULTI_CAR("Multi Car"),
    @XmlEnumValue("No Discount")
    NO_DISCOUNT("No Discount");
    private final String value;

    DiscountCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DiscountCategory fromValue(String v) {
        for (DiscountCategory c: DiscountCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
