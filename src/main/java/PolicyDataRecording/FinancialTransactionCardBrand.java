
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialTransactionCardBrand.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FinancialTransactionCardBrand">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="American Express"/>
 *     &lt;enumeration value="Diners Club"/>
 *     &lt;enumeration value="Master Card"/>
 *     &lt;enumeration value="Visa"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FinancialTransactionCardBrand", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum FinancialTransactionCardBrand {

    @XmlEnumValue("American Express")
    AMERICAN_EXPRESS("American Express"),
    @XmlEnumValue("Diners Club")
    DINERS_CLUB("Diners Club"),
    @XmlEnumValue("Master Card")
    MASTER_CARD("Master Card"),
    @XmlEnumValue("Visa")
    VISA("Visa");
    private final String value;

    FinancialTransactionCardBrand(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FinancialTransactionCardBrand fromValue(String v) {
        for (FinancialTransactionCardBrand c: FinancialTransactionCardBrand.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
