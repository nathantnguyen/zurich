
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeometricShape_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeometricShape_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO">
 *       &lt;sequence>
 *         &lt;element name="centerPointElevation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="centerPointLatitude" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="centerPointLongitude" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="distance" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="layerReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="locationResolution" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="numberOfPoints" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="radius" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="version" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeometricShape_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "centerPointElevation",
    "centerPointLatitude",
    "centerPointLongitude",
    "distance",
    "layerReference",
    "locationResolution",
    "numberOfPoints",
    "radius",
    "version"
})
public class GeometricShapeTO
    extends PlaceTO
{

    protected String centerPointElevation;
    protected String centerPointLatitude;
    protected String centerPointLongitude;
    protected BigDecimal distance;
    protected String layerReference;
    protected String locationResolution;
    protected BigInteger numberOfPoints;
    protected BigDecimal radius;
    protected String version;

    /**
     * Gets the value of the centerPointElevation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterPointElevation() {
        return centerPointElevation;
    }

    /**
     * Sets the value of the centerPointElevation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterPointElevation(String value) {
        this.centerPointElevation = value;
    }

    /**
     * Gets the value of the centerPointLatitude property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterPointLatitude() {
        return centerPointLatitude;
    }

    /**
     * Sets the value of the centerPointLatitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterPointLatitude(String value) {
        this.centerPointLatitude = value;
    }

    /**
     * Gets the value of the centerPointLongitude property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCenterPointLongitude() {
        return centerPointLongitude;
    }

    /**
     * Sets the value of the centerPointLongitude property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCenterPointLongitude(String value) {
        this.centerPointLongitude = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDistance(BigDecimal value) {
        this.distance = value;
    }

    /**
     * Gets the value of the layerReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLayerReference() {
        return layerReference;
    }

    /**
     * Sets the value of the layerReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLayerReference(String value) {
        this.layerReference = value;
    }

    /**
     * Gets the value of the locationResolution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationResolution() {
        return locationResolution;
    }

    /**
     * Sets the value of the locationResolution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationResolution(String value) {
        this.locationResolution = value;
    }

    /**
     * Gets the value of the numberOfPoints property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfPoints() {
        return numberOfPoints;
    }

    /**
     * Sets the value of the numberOfPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfPoints(BigInteger value) {
        this.numberOfPoints = value;
    }

    /**
     * Gets the value of the radius property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRadius() {
        return radius;
    }

    /**
     * Sets the value of the radius property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRadius(BigDecimal value) {
        this.radius = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
