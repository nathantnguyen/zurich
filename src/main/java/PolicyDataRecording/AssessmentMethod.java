
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssessmentMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssessmentMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Survey"/>
 *     &lt;enumeration value="Certified Gemmologist Appraisal"/>
 *     &lt;enumeration value="Data Mining Algorithm"/>
 *     &lt;enumeration value="Purchased Demographic Data"/>
 *     &lt;enumeration value="Real Estate Appraisal"/>
 *     &lt;enumeration value="Class Rated Property"/>
 *     &lt;enumeration value="Global Look Through Investment Analysis"/>
 *     &lt;enumeration value="Mandate Look Through Investment Analysis"/>
 *     &lt;enumeration value="Solvency Balance Sheet Item Valuation - Mark To Market"/>
 *     &lt;enumeration value="Solvency Balance Sheet Item Valuation - Mark To Model"/>
 *     &lt;enumeration value="Solvency Balance Sheet Item Valuation - Other Method"/>
 *     &lt;enumeration value="Standard Look Through Investment Analysis"/>
 *     &lt;enumeration value="Valuation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssessmentMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum AssessmentMethod {

    @XmlEnumValue("Survey")
    SURVEY("Survey"),
    @XmlEnumValue("Certified Gemmologist Appraisal")
    CERTIFIED_GEMMOLOGIST_APPRAISAL("Certified Gemmologist Appraisal"),
    @XmlEnumValue("Data Mining Algorithm")
    DATA_MINING_ALGORITHM("Data Mining Algorithm"),
    @XmlEnumValue("Purchased Demographic Data")
    PURCHASED_DEMOGRAPHIC_DATA("Purchased Demographic Data"),
    @XmlEnumValue("Real Estate Appraisal")
    REAL_ESTATE_APPRAISAL("Real Estate Appraisal"),
    @XmlEnumValue("Class Rated Property")
    CLASS_RATED_PROPERTY("Class Rated Property"),
    @XmlEnumValue("Global Look Through Investment Analysis")
    GLOBAL_LOOK_THROUGH_INVESTMENT_ANALYSIS("Global Look Through Investment Analysis"),
    @XmlEnumValue("Mandate Look Through Investment Analysis")
    MANDATE_LOOK_THROUGH_INVESTMENT_ANALYSIS("Mandate Look Through Investment Analysis"),
    @XmlEnumValue("Solvency Balance Sheet Item Valuation - Mark To Market")
    SOLVENCY_BALANCE_SHEET_ITEM_VALUATION_MARK_TO_MARKET("Solvency Balance Sheet Item Valuation - Mark To Market"),
    @XmlEnumValue("Solvency Balance Sheet Item Valuation - Mark To Model")
    SOLVENCY_BALANCE_SHEET_ITEM_VALUATION_MARK_TO_MODEL("Solvency Balance Sheet Item Valuation - Mark To Model"),
    @XmlEnumValue("Solvency Balance Sheet Item Valuation - Other Method")
    SOLVENCY_BALANCE_SHEET_ITEM_VALUATION_OTHER_METHOD("Solvency Balance Sheet Item Valuation - Other Method"),
    @XmlEnumValue("Standard Look Through Investment Analysis")
    STANDARD_LOOK_THROUGH_INVESTMENT_ANALYSIS("Standard Look Through Investment Analysis"),
    @XmlEnumValue("Valuation")
    VALUATION("Valuation");
    private final String value;

    AssessmentMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssessmentMethod fromValue(String v) {
        for (AssessmentMethod c: AssessmentMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
