
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SuspectDuplicateAugmentation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SuspectDuplicateAugmentation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="matchCategory" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}MatchCategory" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SuspectDuplicateAugmentation_TO", propOrder = {
    "matchCategory"
})
@XmlSeeAlso({
    HumanAugmentationTO.class,
    AlgorithmicAugmentationTO.class
})
public class SuspectDuplicateAugmentationTO
    extends DependentObjectTO
{

    protected MatchCategory matchCategory;

    /**
     * Gets the value of the matchCategory property.
     * 
     * @return
     *     possible object is
     *     {@link MatchCategory }
     *     
     */
    public MatchCategory getMatchCategory() {
        return matchCategory;
    }

    /**
     * Sets the value of the matchCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchCategory }
     *     
     */
    public void setMatchCategory(MatchCategory value) {
        this.matchCategory = value;
    }

}
