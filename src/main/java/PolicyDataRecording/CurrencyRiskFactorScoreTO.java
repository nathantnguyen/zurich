
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CurrencyRiskFactorScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CurrencyRiskFactorScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskFactorScore_TO">
 *       &lt;sequence>
 *         &lt;element name="averageDailyVariance" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="highestExchangeRate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="lowestExchangeRate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="volatility" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="baseCurrency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="counterCurrency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}CurrencyCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CurrencyRiskFactorScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "averageDailyVariance",
    "highestExchangeRate",
    "lowestExchangeRate",
    "volatility",
    "baseCurrency",
    "counterCurrency"
})
public class CurrencyRiskFactorScoreTO
    extends RiskFactorScoreTO
{

    protected BigDecimal averageDailyVariance;
    protected BigDecimal highestExchangeRate;
    protected BigDecimal lowestExchangeRate;
    protected BigDecimal volatility;
    protected CurrencyCode baseCurrency;
    protected CurrencyCode counterCurrency;

    /**
     * Gets the value of the averageDailyVariance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAverageDailyVariance() {
        return averageDailyVariance;
    }

    /**
     * Sets the value of the averageDailyVariance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAverageDailyVariance(BigDecimal value) {
        this.averageDailyVariance = value;
    }

    /**
     * Gets the value of the highestExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHighestExchangeRate() {
        return highestExchangeRate;
    }

    /**
     * Sets the value of the highestExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHighestExchangeRate(BigDecimal value) {
        this.highestExchangeRate = value;
    }

    /**
     * Gets the value of the lowestExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLowestExchangeRate() {
        return lowestExchangeRate;
    }

    /**
     * Sets the value of the lowestExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLowestExchangeRate(BigDecimal value) {
        this.lowestExchangeRate = value;
    }

    /**
     * Gets the value of the volatility property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVolatility() {
        return volatility;
    }

    /**
     * Sets the value of the volatility property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVolatility(BigDecimal value) {
        this.volatility = value;
    }

    /**
     * Gets the value of the baseCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCode }
     *     
     */
    public CurrencyCode getBaseCurrency() {
        return baseCurrency;
    }

    /**
     * Sets the value of the baseCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCode }
     *     
     */
    public void setBaseCurrency(CurrencyCode value) {
        this.baseCurrency = value;
    }

    /**
     * Gets the value of the counterCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCode }
     *     
     */
    public CurrencyCode getCounterCurrency() {
        return counterCurrency;
    }

    /**
     * Sets the value of the counterCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCode }
     *     
     */
    public void setCounterCurrency(CurrencyCode value) {
        this.counterCurrency = value;
    }

}
