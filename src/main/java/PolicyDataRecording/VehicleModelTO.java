
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VehicleModel_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VehicleModel_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}ModelSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="bodyStyle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="grossWeight" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="engineCapacity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="horsePower" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="fuelType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="interiorSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="numberOfDrivingWheels" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="displacementSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="insuranceRatingGroup" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}InsuranceRatingGroup" minOccurs="0"/>
 *         &lt;element name="fuelInjection" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="ignition" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Ignition" minOccurs="0"/>
 *         &lt;element name="serialNumberEtching" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="passengerCapacity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="antiqueVehicle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="carryingWeight" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="engineType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}EngineType" minOccurs="0"/>
 *         &lt;element name="classicVehicle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="bodyType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="dutyType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="segmentation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="inCompleteVehicle" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="bodyStyleCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="fuelInjectionCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="fuelTypeCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="dutyTypeCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="numberOfDoors" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="segmentationCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="numberOfWheelEnds" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleModel_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "bodyStyle",
    "grossWeight",
    "engineCapacity",
    "horsePower",
    "fuelType",
    "interiorSize",
    "numberOfDrivingWheels",
    "displacementSize",
    "insuranceRatingGroup",
    "fuelInjection",
    "ignition",
    "serialNumberEtching",
    "passengerCapacity",
    "antiqueVehicle",
    "carryingWeight",
    "engineType",
    "classicVehicle",
    "bodyType",
    "dutyType",
    "segmentation",
    "inCompleteVehicle",
    "bodyStyleCode",
    "fuelInjectionCode",
    "fuelTypeCode",
    "dutyTypeCode",
    "numberOfDoors",
    "segmentationCode",
    "numberOfWheelEnds"
})
@XmlSeeAlso({
    ShipModelTO.class,
    AircraftModelTO.class,
    RoadVehicleModelTO.class
})
public class VehicleModelTO
    extends ModelSpecificationTO
{

    protected String bodyStyle;
    protected Amount grossWeight;
    protected Amount engineCapacity;
    protected Amount horsePower;
    protected String fuelType;
    protected Amount interiorSize;
    protected BigInteger numberOfDrivingWheels;
    protected Amount displacementSize;
    protected InsuranceRatingGroup insuranceRatingGroup;
    protected String fuelInjection;
    protected Ignition ignition;
    protected Boolean serialNumberEtching;
    protected BigInteger passengerCapacity;
    protected Boolean antiqueVehicle;
    protected Amount carryingWeight;
    protected EngineType engineType;
    protected Boolean classicVehicle;
    protected String bodyType;
    protected String dutyType;
    protected String segmentation;
    protected Boolean inCompleteVehicle;
    protected String bodyStyleCode;
    protected String fuelInjectionCode;
    protected String fuelTypeCode;
    protected String dutyTypeCode;
    protected BigInteger numberOfDoors;
    protected String segmentationCode;
    protected BigInteger numberOfWheelEnds;

    /**
     * Gets the value of the bodyStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBodyStyle() {
        return bodyStyle;
    }

    /**
     * Sets the value of the bodyStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBodyStyle(String value) {
        this.bodyStyle = value;
    }

    /**
     * Gets the value of the grossWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getGrossWeight() {
        return grossWeight;
    }

    /**
     * Sets the value of the grossWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setGrossWeight(Amount value) {
        this.grossWeight = value;
    }

    /**
     * Gets the value of the engineCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getEngineCapacity() {
        return engineCapacity;
    }

    /**
     * Sets the value of the engineCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setEngineCapacity(Amount value) {
        this.engineCapacity = value;
    }

    /**
     * Gets the value of the horsePower property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getHorsePower() {
        return horsePower;
    }

    /**
     * Sets the value of the horsePower property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setHorsePower(Amount value) {
        this.horsePower = value;
    }

    /**
     * Gets the value of the fuelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelType() {
        return fuelType;
    }

    /**
     * Sets the value of the fuelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelType(String value) {
        this.fuelType = value;
    }

    /**
     * Gets the value of the interiorSize property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getInteriorSize() {
        return interiorSize;
    }

    /**
     * Sets the value of the interiorSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setInteriorSize(Amount value) {
        this.interiorSize = value;
    }

    /**
     * Gets the value of the numberOfDrivingWheels property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfDrivingWheels() {
        return numberOfDrivingWheels;
    }

    /**
     * Sets the value of the numberOfDrivingWheels property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfDrivingWheels(BigInteger value) {
        this.numberOfDrivingWheels = value;
    }

    /**
     * Gets the value of the displacementSize property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getDisplacementSize() {
        return displacementSize;
    }

    /**
     * Sets the value of the displacementSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setDisplacementSize(Amount value) {
        this.displacementSize = value;
    }

    /**
     * Gets the value of the insuranceRatingGroup property.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceRatingGroup }
     *     
     */
    public InsuranceRatingGroup getInsuranceRatingGroup() {
        return insuranceRatingGroup;
    }

    /**
     * Sets the value of the insuranceRatingGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceRatingGroup }
     *     
     */
    public void setInsuranceRatingGroup(InsuranceRatingGroup value) {
        this.insuranceRatingGroup = value;
    }

    /**
     * Gets the value of the fuelInjection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelInjection() {
        return fuelInjection;
    }

    /**
     * Sets the value of the fuelInjection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelInjection(String value) {
        this.fuelInjection = value;
    }

    /**
     * Gets the value of the ignition property.
     * 
     * @return
     *     possible object is
     *     {@link Ignition }
     *     
     */
    public Ignition getIgnition() {
        return ignition;
    }

    /**
     * Sets the value of the ignition property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ignition }
     *     
     */
    public void setIgnition(Ignition value) {
        this.ignition = value;
    }

    /**
     * Gets the value of the serialNumberEtching property.
     * This getter has been renamed from isSerialNumberEtching() to getSerialNumberEtching() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSerialNumberEtching() {
        return serialNumberEtching;
    }

    /**
     * Sets the value of the serialNumberEtching property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSerialNumberEtching(Boolean value) {
        this.serialNumberEtching = value;
    }

    /**
     * Gets the value of the passengerCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPassengerCapacity() {
        return passengerCapacity;
    }

    /**
     * Sets the value of the passengerCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPassengerCapacity(BigInteger value) {
        this.passengerCapacity = value;
    }

    /**
     * Gets the value of the antiqueVehicle property.
     * This getter has been renamed from isAntiqueVehicle() to getAntiqueVehicle() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAntiqueVehicle() {
        return antiqueVehicle;
    }

    /**
     * Sets the value of the antiqueVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAntiqueVehicle(Boolean value) {
        this.antiqueVehicle = value;
    }

    /**
     * Gets the value of the carryingWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getCarryingWeight() {
        return carryingWeight;
    }

    /**
     * Sets the value of the carryingWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setCarryingWeight(Amount value) {
        this.carryingWeight = value;
    }

    /**
     * Gets the value of the engineType property.
     * 
     * @return
     *     possible object is
     *     {@link EngineType }
     *     
     */
    public EngineType getEngineType() {
        return engineType;
    }

    /**
     * Sets the value of the engineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link EngineType }
     *     
     */
    public void setEngineType(EngineType value) {
        this.engineType = value;
    }

    /**
     * Gets the value of the classicVehicle property.
     * This getter has been renamed from isClassicVehicle() to getClassicVehicle() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getClassicVehicle() {
        return classicVehicle;
    }

    /**
     * Sets the value of the classicVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClassicVehicle(Boolean value) {
        this.classicVehicle = value;
    }

    /**
     * Gets the value of the bodyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBodyType() {
        return bodyType;
    }

    /**
     * Sets the value of the bodyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBodyType(String value) {
        this.bodyType = value;
    }

    /**
     * Gets the value of the dutyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyType() {
        return dutyType;
    }

    /**
     * Sets the value of the dutyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyType(String value) {
        this.dutyType = value;
    }

    /**
     * Gets the value of the segmentation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentation() {
        return segmentation;
    }

    /**
     * Sets the value of the segmentation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentation(String value) {
        this.segmentation = value;
    }

    /**
     * Gets the value of the inCompleteVehicle property.
     * This getter has been renamed from isInCompleteVehicle() to getInCompleteVehicle() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInCompleteVehicle() {
        return inCompleteVehicle;
    }

    /**
     * Sets the value of the inCompleteVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInCompleteVehicle(Boolean value) {
        this.inCompleteVehicle = value;
    }

    /**
     * Gets the value of the bodyStyleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBodyStyleCode() {
        return bodyStyleCode;
    }

    /**
     * Sets the value of the bodyStyleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBodyStyleCode(String value) {
        this.bodyStyleCode = value;
    }

    /**
     * Gets the value of the fuelInjectionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelInjectionCode() {
        return fuelInjectionCode;
    }

    /**
     * Sets the value of the fuelInjectionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelInjectionCode(String value) {
        this.fuelInjectionCode = value;
    }

    /**
     * Gets the value of the fuelTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelTypeCode() {
        return fuelTypeCode;
    }

    /**
     * Sets the value of the fuelTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelTypeCode(String value) {
        this.fuelTypeCode = value;
    }

    /**
     * Gets the value of the dutyTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyTypeCode() {
        return dutyTypeCode;
    }

    /**
     * Sets the value of the dutyTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyTypeCode(String value) {
        this.dutyTypeCode = value;
    }

    /**
     * Gets the value of the numberOfDoors property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfDoors() {
        return numberOfDoors;
    }

    /**
     * Sets the value of the numberOfDoors property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfDoors(BigInteger value) {
        this.numberOfDoors = value;
    }

    /**
     * Gets the value of the segmentationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentationCode() {
        return segmentationCode;
    }

    /**
     * Sets the value of the segmentationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentationCode(String value) {
        this.segmentationCode = value;
    }

    /**
     * Gets the value of the numberOfWheelEnds property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfWheelEnds() {
        return numberOfWheelEnds;
    }

    /**
     * Sets the value of the numberOfWheelEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfWheelEnds(BigInteger value) {
        this.numberOfWheelEnds = value;
    }

}
