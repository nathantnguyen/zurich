
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SkillLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SkillLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Professional"/>
 *     &lt;enumeration value="Skilled"/>
 *     &lt;enumeration value="Semi Skilled"/>
 *     &lt;enumeration value="Un Skilled"/>
 *     &lt;enumeration value="Excellent"/>
 *     &lt;enumeration value="Good"/>
 *     &lt;enumeration value="Poor"/>
 *     &lt;enumeration value="Student Level"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SkillLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum SkillLevel {

    @XmlEnumValue("Professional")
    PROFESSIONAL("Professional"),
    @XmlEnumValue("Skilled")
    SKILLED("Skilled"),
    @XmlEnumValue("Semi Skilled")
    SEMI_SKILLED("Semi Skilled"),
    @XmlEnumValue("Un Skilled")
    UN_SKILLED("Un Skilled"),
    @XmlEnumValue("Excellent")
    EXCELLENT("Excellent"),
    @XmlEnumValue("Good")
    GOOD("Good"),
    @XmlEnumValue("Poor")
    POOR("Poor"),
    @XmlEnumValue("Student Level")
    STUDENT_LEVEL("Student Level");
    private final String value;

    SkillLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SkillLevel fromValue(String v) {
        for (SkillLevel c: SkillLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
