
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdjustmentReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AdjustmentReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Age Of Building"/>
 *     &lt;enumeration value="Alarm System"/>
 *     &lt;enumeration value="Amount Of Insurance"/>
 *     &lt;enumeration value="Automatic Increase"/>
 *     &lt;enumeration value="Class Of Business"/>
 *     &lt;enumeration value="Equipment Breakdown Exclusion"/>
 *     &lt;enumeration value="Mall"/>
 *     &lt;enumeration value="Sole Occupancy"/>
 *     &lt;enumeration value="Speciality Program"/>
 *     &lt;enumeration value="Sprinkler"/>
 *     &lt;enumeration value="Sprinkler Leakage Exclusion"/>
 *     &lt;enumeration value="Vandalism Exclusion"/>
 *     &lt;enumeration value="Windstorm Exclusion"/>
 *     &lt;enumeration value="Campaign Discount"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AdjustmentReason", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum AdjustmentReason {

    @XmlEnumValue("Age Of Building")
    AGE_OF_BUILDING("Age Of Building"),
    @XmlEnumValue("Alarm System")
    ALARM_SYSTEM("Alarm System"),
    @XmlEnumValue("Amount Of Insurance")
    AMOUNT_OF_INSURANCE("Amount Of Insurance"),
    @XmlEnumValue("Automatic Increase")
    AUTOMATIC_INCREASE("Automatic Increase"),
    @XmlEnumValue("Class Of Business")
    CLASS_OF_BUSINESS("Class Of Business"),
    @XmlEnumValue("Equipment Breakdown Exclusion")
    EQUIPMENT_BREAKDOWN_EXCLUSION("Equipment Breakdown Exclusion"),
    @XmlEnumValue("Mall")
    MALL("Mall"),
    @XmlEnumValue("Sole Occupancy")
    SOLE_OCCUPANCY("Sole Occupancy"),
    @XmlEnumValue("Speciality Program")
    SPECIALITY_PROGRAM("Speciality Program"),
    @XmlEnumValue("Sprinkler")
    SPRINKLER("Sprinkler"),
    @XmlEnumValue("Sprinkler Leakage Exclusion")
    SPRINKLER_LEAKAGE_EXCLUSION("Sprinkler Leakage Exclusion"),
    @XmlEnumValue("Vandalism Exclusion")
    VANDALISM_EXCLUSION("Vandalism Exclusion"),
    @XmlEnumValue("Windstorm Exclusion")
    WINDSTORM_EXCLUSION("Windstorm Exclusion"),
    @XmlEnumValue("Campaign Discount")
    CAMPAIGN_DISCOUNT("Campaign Discount");
    private final String value;

    AdjustmentReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdjustmentReason fromValue(String v) {
        for (AdjustmentReason c: AdjustmentReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
