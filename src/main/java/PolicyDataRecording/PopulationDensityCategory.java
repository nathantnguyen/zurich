
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PopulationDensityCategory.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PopulationDensityCategory">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="City Area"/>
 *     &lt;enumeration value="Not Populated"/>
 *     &lt;enumeration value="Rural Area"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PopulationDensityCategory", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum PopulationDensityCategory {

    @XmlEnumValue("City Area")
    CITY_AREA("City Area"),
    @XmlEnumValue("Not Populated")
    NOT_POPULATED("Not Populated"),
    @XmlEnumValue("Rural Area")
    RURAL_AREA("Rural Area");
    private final String value;

    PopulationDensityCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PopulationDensityCategory fromValue(String v) {
        for (PopulationDensityCategory c: PopulationDensityCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
