
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BodyElement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BodyElement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO">
 *       &lt;sequence>
 *         &lt;element name="bodyElementComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}BodyPart_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="medicalConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}MedicalCondition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BodyElement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "bodyElementComponents",
    "medicalConditions"
})
@XmlSeeAlso({
    BodyPartTO.class,
    LifeFormTO.class
})
public class BodyElementTO
    extends PhysicalObjectTO
{

    protected List<BodyPartTO> bodyElementComponents;
    protected List<MedicalConditionTO> medicalConditions;

    /**
     * Gets the value of the bodyElementComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bodyElementComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBodyElementComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BodyPartTO }
     * 
     * 
     */
    public List<BodyPartTO> getBodyElementComponents() {
        if (bodyElementComponents == null) {
            bodyElementComponents = new ArrayList<BodyPartTO>();
        }
        return this.bodyElementComponents;
    }

    /**
     * Gets the value of the medicalConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the medicalConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMedicalConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MedicalConditionTO }
     * 
     * 
     */
    public List<MedicalConditionTO> getMedicalConditions() {
        if (medicalConditions == null) {
            medicalConditions = new ArrayList<MedicalConditionTO>();
        }
        return this.medicalConditions;
    }

    /**
     * Sets the value of the bodyElementComponents property.
     * 
     * @param bodyElementComponents
     *     allowed object is
     *     {@link BodyPartTO }
     *     
     */
    public void setBodyElementComponents(List<BodyPartTO> bodyElementComponents) {
        this.bodyElementComponents = bodyElementComponents;
    }

    /**
     * Sets the value of the medicalConditions property.
     * 
     * @param medicalConditions
     *     allowed object is
     *     {@link MedicalConditionTO }
     *     
     */
    public void setMedicalConditions(List<MedicalConditionTO> medicalConditions) {
        this.medicalConditions = medicalConditions;
    }

}
