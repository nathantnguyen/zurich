
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LegalActionRole_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegalActionRole_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RolePlayerClassRole_TO">
 *       &lt;sequence>
 *         &lt;element name="litigationActivityReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegalActionRole_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/DisputeResolution/", propOrder = {
    "litigationActivityReference"
})
public class LegalActionRoleTO
    extends RolePlayerClassRoleTO
{

    protected ObjectReferenceTO litigationActivityReference;

    /**
     * Gets the value of the litigationActivityReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getLitigationActivityReference() {
        return litigationActivityReference;
    }

    /**
     * Sets the value of the litigationActivityReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setLitigationActivityReference(ObjectReferenceTO value) {
        this.litigationActivityReference = value;
    }

}
