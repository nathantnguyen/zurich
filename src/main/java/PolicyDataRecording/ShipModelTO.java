
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShipModel_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipModel_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}VehicleModel_TO">
 *       &lt;sequence>
 *         &lt;element name="baleCapacity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="beam" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="draft" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="hullType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="loadedDisplacementTonnage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="netTonnage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="shipBasedOnSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}Ship_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipModel_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "baleCapacity",
    "beam",
    "draft",
    "hullType",
    "loadedDisplacementTonnage",
    "netTonnage",
    "shipBasedOnSpecification"
})
public class ShipModelTO
    extends VehicleModelTO
{

    protected Amount baleCapacity;
    protected Amount beam;
    protected BigDecimal draft;
    protected String hullType;
    protected BigDecimal loadedDisplacementTonnage;
    protected BigDecimal netTonnage;
    protected List<ShipTO> shipBasedOnSpecification;

    /**
     * Gets the value of the baleCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getBaleCapacity() {
        return baleCapacity;
    }

    /**
     * Sets the value of the baleCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setBaleCapacity(Amount value) {
        this.baleCapacity = value;
    }

    /**
     * Gets the value of the beam property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getBeam() {
        return beam;
    }

    /**
     * Sets the value of the beam property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setBeam(Amount value) {
        this.beam = value;
    }

    /**
     * Gets the value of the draft property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDraft() {
        return draft;
    }

    /**
     * Sets the value of the draft property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDraft(BigDecimal value) {
        this.draft = value;
    }

    /**
     * Gets the value of the hullType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHullType() {
        return hullType;
    }

    /**
     * Sets the value of the hullType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHullType(String value) {
        this.hullType = value;
    }

    /**
     * Gets the value of the loadedDisplacementTonnage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLoadedDisplacementTonnage() {
        return loadedDisplacementTonnage;
    }

    /**
     * Sets the value of the loadedDisplacementTonnage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLoadedDisplacementTonnage(BigDecimal value) {
        this.loadedDisplacementTonnage = value;
    }

    /**
     * Gets the value of the netTonnage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNetTonnage() {
        return netTonnage;
    }

    /**
     * Sets the value of the netTonnage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNetTonnage(BigDecimal value) {
        this.netTonnage = value;
    }

    /**
     * Gets the value of the shipBasedOnSpecification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipBasedOnSpecification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipBasedOnSpecification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipTO }
     * 
     * 
     */
    public List<ShipTO> getShipBasedOnSpecification() {
        if (shipBasedOnSpecification == null) {
            shipBasedOnSpecification = new ArrayList<ShipTO>();
        }
        return this.shipBasedOnSpecification;
    }

    /**
     * Sets the value of the shipBasedOnSpecification property.
     * 
     * @param shipBasedOnSpecification
     *     allowed object is
     *     {@link ShipTO }
     *     
     */
    public void setShipBasedOnSpecification(List<ShipTO> shipBasedOnSpecification) {
        this.shipBasedOnSpecification = shipBasedOnSpecification;
    }

}
