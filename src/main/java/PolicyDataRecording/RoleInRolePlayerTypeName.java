
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInRolePlayerTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInRolePlayerTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Activity Occurrence Performance"/>
 *     &lt;enumeration value="Activity Occurrence Management"/>
 *     &lt;enumeration value="Activity Occurrence Participation"/>
 *     &lt;enumeration value="Activity Occurrence Authorization"/>
 *     &lt;enumeration value="Activity Occurrence Initiation"/>
 *     &lt;enumeration value="Role Player Subject Of Activity"/>
 *     &lt;enumeration value="Manufacturer"/>
 *     &lt;enumeration value="Model Supplier"/>
 *     &lt;enumeration value="Object Caretaking"/>
 *     &lt;enumeration value="Object Management"/>
 *     &lt;enumeration value="Object Provider"/>
 *     &lt;enumeration value="Object Reception"/>
 *     &lt;enumeration value="Subject Of Assessment"/>
 *     &lt;enumeration value="Registered Owner"/>
 *     &lt;enumeration value="Activity Occurrence Completion"/>
 *     &lt;enumeration value="Party Registration Limitation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInRolePlayerTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum RoleInRolePlayerTypeName {

    @XmlEnumValue("Activity Occurrence Performance")
    ACTIVITY_OCCURRENCE_PERFORMANCE("Activity Occurrence Performance"),
    @XmlEnumValue("Activity Occurrence Management")
    ACTIVITY_OCCURRENCE_MANAGEMENT("Activity Occurrence Management"),
    @XmlEnumValue("Activity Occurrence Participation")
    ACTIVITY_OCCURRENCE_PARTICIPATION("Activity Occurrence Participation"),
    @XmlEnumValue("Activity Occurrence Authorization")
    ACTIVITY_OCCURRENCE_AUTHORIZATION("Activity Occurrence Authorization"),
    @XmlEnumValue("Activity Occurrence Initiation")
    ACTIVITY_OCCURRENCE_INITIATION("Activity Occurrence Initiation"),
    @XmlEnumValue("Role Player Subject Of Activity")
    ROLE_PLAYER_SUBJECT_OF_ACTIVITY("Role Player Subject Of Activity"),
    @XmlEnumValue("Manufacturer")
    MANUFACTURER("Manufacturer"),
    @XmlEnumValue("Model Supplier")
    MODEL_SUPPLIER("Model Supplier"),
    @XmlEnumValue("Object Caretaking")
    OBJECT_CARETAKING("Object Caretaking"),
    @XmlEnumValue("Object Management")
    OBJECT_MANAGEMENT("Object Management"),
    @XmlEnumValue("Object Provider")
    OBJECT_PROVIDER("Object Provider"),
    @XmlEnumValue("Object Reception")
    OBJECT_RECEPTION("Object Reception"),
    @XmlEnumValue("Subject Of Assessment")
    SUBJECT_OF_ASSESSMENT("Subject Of Assessment"),
    @XmlEnumValue("Registered Owner")
    REGISTERED_OWNER("Registered Owner"),
    @XmlEnumValue("Activity Occurrence Completion")
    ACTIVITY_OCCURRENCE_COMPLETION("Activity Occurrence Completion"),
    @XmlEnumValue("Party Registration Limitation")
    PARTY_REGISTRATION_LIMITATION("Party Registration Limitation");
    private final String value;

    RoleInRolePlayerTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInRolePlayerTypeName fromValue(String v) {
        for (RoleInRolePlayerTypeName c: RoleInRolePlayerTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
