
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Score_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Score_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO">
 *       &lt;sequence>
 *         &lt;element name="freeTextScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="scoreRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ScoreType_TO" minOccurs="0"/>
 *         &lt;element name="usedFactor" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ScoreFactor_TO" minOccurs="0"/>
 *         &lt;element name="applicableRanges" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ScoreRange_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="matchingScoredGeographicCoordinates" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}GeographicCoordinates_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Score_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "freeTextScore",
    "scoreRootType",
    "usedFactor",
    "applicableRanges",
    "matchingScoredGeographicCoordinates"
})
@XmlSeeAlso({
    InformationScoreTO.class,
    CommunicationEffectivenessScoreTO.class,
    SupervisionTO.class,
    ProfitAndLossAttributionTO.class,
    RiskFactorScoreTO.class,
    AutoPolicyLossDataScoreTO.class,
    InsurerLossDataScoreTO.class,
    PredictiveModelScoreTO.class,
    DriverActivityScoreTO.class,
    DemographicScoreTO.class,
    StructureScoreTO.class,
    ShipScoreTO.class,
    VehicleScoreTO.class,
    TaskScoreTO.class,
    SuspectDuplicateTO.class,
    GeoCodeTO.class,
    RolePlayerScoreTO.class
})
public class ScoreTO
    extends AssessmentResultTO
{

    protected String freeTextScore;
    protected ScoreTypeTO scoreRootType;
    protected ScoreFactorTO usedFactor;
    protected List<ScoreRangeTO> applicableRanges;
    protected GeographicCoordinatesTO matchingScoredGeographicCoordinates;

    /**
     * Gets the value of the freeTextScore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeTextScore() {
        return freeTextScore;
    }

    /**
     * Sets the value of the freeTextScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeTextScore(String value) {
        this.freeTextScore = value;
    }

    /**
     * Gets the value of the scoreRootType property.
     * 
     * @return
     *     possible object is
     *     {@link ScoreTypeTO }
     *     
     */
    public ScoreTypeTO getScoreRootType() {
        return scoreRootType;
    }

    /**
     * Sets the value of the scoreRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScoreTypeTO }
     *     
     */
    public void setScoreRootType(ScoreTypeTO value) {
        this.scoreRootType = value;
    }

    /**
     * Gets the value of the usedFactor property.
     * 
     * @return
     *     possible object is
     *     {@link ScoreFactorTO }
     *     
     */
    public ScoreFactorTO getUsedFactor() {
        return usedFactor;
    }

    /**
     * Sets the value of the usedFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScoreFactorTO }
     *     
     */
    public void setUsedFactor(ScoreFactorTO value) {
        this.usedFactor = value;
    }

    /**
     * Gets the value of the applicableRanges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableRanges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicableRanges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScoreRangeTO }
     * 
     * 
     */
    public List<ScoreRangeTO> getApplicableRanges() {
        if (applicableRanges == null) {
            applicableRanges = new ArrayList<ScoreRangeTO>();
        }
        return this.applicableRanges;
    }

    /**
     * Gets the value of the matchingScoredGeographicCoordinates property.
     * 
     * @return
     *     possible object is
     *     {@link GeographicCoordinatesTO }
     *     
     */
    public GeographicCoordinatesTO getMatchingScoredGeographicCoordinates() {
        return matchingScoredGeographicCoordinates;
    }

    /**
     * Sets the value of the matchingScoredGeographicCoordinates property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeographicCoordinatesTO }
     *     
     */
    public void setMatchingScoredGeographicCoordinates(GeographicCoordinatesTO value) {
        this.matchingScoredGeographicCoordinates = value;
    }

    /**
     * Sets the value of the applicableRanges property.
     * 
     * @param applicableRanges
     *     allowed object is
     *     {@link ScoreRangeTO }
     *     
     */
    public void setApplicableRanges(List<ScoreRangeTO> applicableRanges) {
        this.applicableRanges = applicableRanges;
    }

}
