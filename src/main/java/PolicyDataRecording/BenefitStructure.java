
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BenefitStructure.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BenefitStructure">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Decreasing"/>
 *     &lt;enumeration value="Level"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BenefitStructure", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum BenefitStructure {

    @XmlEnumValue("Decreasing")
    DECREASING("Decreasing"),
    @XmlEnumValue("Level")
    LEVEL("Level");
    private final String value;

    BenefitStructure(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BenefitStructure fromValue(String v) {
        for (BenefitStructure c: BenefitStructure.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
