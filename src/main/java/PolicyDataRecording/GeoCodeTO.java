
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeoCode_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeoCode_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="geoCoderStatus" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeoCoderStatus_TO" minOccurs="0"/>
 *         &lt;element name="sourceDataSet" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="parsedAddressElements" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}ParsedPostalAddress_TO" minOccurs="0"/>
 *         &lt;element name="ignoredAddressData" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PostalAddress_TO" minOccurs="0"/>
 *         &lt;element name="matchCategory" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="closeMatch" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="locationPrecision" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="returnedAddresses" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PostalAddress_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="numberOfMatchingAddresses" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="numberOfAddressRanges" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="numberOfUnitRanges" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeoCode_TO", propOrder = {
    "geoCoderStatus",
    "sourceDataSet",
    "parsedAddressElements",
    "ignoredAddressData",
    "matchCategory",
    "closeMatch",
    "locationPrecision",
    "returnedAddresses",
    "numberOfMatchingAddresses",
    "numberOfAddressRanges",
    "numberOfUnitRanges"
})
public class GeoCodeTO
    extends ScoreTO
{

    protected GeoCoderStatusTO geoCoderStatus;
    protected String sourceDataSet;
    protected ParsedPostalAddressTO parsedAddressElements;
    protected PostalAddressTO ignoredAddressData;
    protected String matchCategory;
    protected Boolean closeMatch;
    protected String locationPrecision;
    protected List<PostalAddressTO> returnedAddresses;
    protected BigInteger numberOfMatchingAddresses;
    protected BigInteger numberOfAddressRanges;
    protected BigInteger numberOfUnitRanges;

    /**
     * Gets the value of the geoCoderStatus property.
     * 
     * @return
     *     possible object is
     *     {@link GeoCoderStatusTO }
     *     
     */
    public GeoCoderStatusTO getGeoCoderStatus() {
        return geoCoderStatus;
    }

    /**
     * Sets the value of the geoCoderStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeoCoderStatusTO }
     *     
     */
    public void setGeoCoderStatus(GeoCoderStatusTO value) {
        this.geoCoderStatus = value;
    }

    /**
     * Gets the value of the sourceDataSet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceDataSet() {
        return sourceDataSet;
    }

    /**
     * Sets the value of the sourceDataSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceDataSet(String value) {
        this.sourceDataSet = value;
    }

    /**
     * Gets the value of the parsedAddressElements property.
     * 
     * @return
     *     possible object is
     *     {@link ParsedPostalAddressTO }
     *     
     */
    public ParsedPostalAddressTO getParsedAddressElements() {
        return parsedAddressElements;
    }

    /**
     * Sets the value of the parsedAddressElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParsedPostalAddressTO }
     *     
     */
    public void setParsedAddressElements(ParsedPostalAddressTO value) {
        this.parsedAddressElements = value;
    }

    /**
     * Gets the value of the ignoredAddressData property.
     * 
     * @return
     *     possible object is
     *     {@link PostalAddressTO }
     *     
     */
    public PostalAddressTO getIgnoredAddressData() {
        return ignoredAddressData;
    }

    /**
     * Sets the value of the ignoredAddressData property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostalAddressTO }
     *     
     */
    public void setIgnoredAddressData(PostalAddressTO value) {
        this.ignoredAddressData = value;
    }

    /**
     * Gets the value of the matchCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchCategory() {
        return matchCategory;
    }

    /**
     * Sets the value of the matchCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchCategory(String value) {
        this.matchCategory = value;
    }

    /**
     * Gets the value of the closeMatch property.
     * This getter has been renamed from isCloseMatch() to getCloseMatch() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCloseMatch() {
        return closeMatch;
    }

    /**
     * Sets the value of the closeMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCloseMatch(Boolean value) {
        this.closeMatch = value;
    }

    /**
     * Gets the value of the locationPrecision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationPrecision() {
        return locationPrecision;
    }

    /**
     * Sets the value of the locationPrecision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationPrecision(String value) {
        this.locationPrecision = value;
    }

    /**
     * Gets the value of the returnedAddresses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the returnedAddresses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReturnedAddresses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PostalAddressTO }
     * 
     * 
     */
    public List<PostalAddressTO> getReturnedAddresses() {
        if (returnedAddresses == null) {
            returnedAddresses = new ArrayList<PostalAddressTO>();
        }
        return this.returnedAddresses;
    }

    /**
     * Gets the value of the numberOfMatchingAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfMatchingAddresses() {
        return numberOfMatchingAddresses;
    }

    /**
     * Sets the value of the numberOfMatchingAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfMatchingAddresses(BigInteger value) {
        this.numberOfMatchingAddresses = value;
    }

    /**
     * Gets the value of the numberOfAddressRanges property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfAddressRanges() {
        return numberOfAddressRanges;
    }

    /**
     * Sets the value of the numberOfAddressRanges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfAddressRanges(BigInteger value) {
        this.numberOfAddressRanges = value;
    }

    /**
     * Gets the value of the numberOfUnitRanges property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfUnitRanges() {
        return numberOfUnitRanges;
    }

    /**
     * Sets the value of the numberOfUnitRanges property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfUnitRanges(BigInteger value) {
        this.numberOfUnitRanges = value;
    }

    /**
     * Sets the value of the returnedAddresses property.
     * 
     * @param returnedAddresses
     *     allowed object is
     *     {@link PostalAddressTO }
     *     
     */
    public void setReturnedAddresses(List<PostalAddressTO> returnedAddresses) {
        this.returnedAddresses = returnedAddresses;
    }

}
