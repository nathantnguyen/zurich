
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeneralActivityTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GeneralActivityTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Domain Of Interest"/>
 *     &lt;enumeration value="Daily Living"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GeneralActivityTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum GeneralActivityTypeName {

    @XmlEnumValue("Domain Of Interest")
    DOMAIN_OF_INTEREST("Domain Of Interest"),
    @XmlEnumValue("Daily Living")
    DAILY_LIVING("Daily Living");
    private final String value;

    GeneralActivityTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GeneralActivityTypeName fromValue(String v) {
        for (GeneralActivityTypeName c: GeneralActivityTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
