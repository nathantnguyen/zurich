
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommercialAutoInsurancePolicy_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialAutoInsurancePolicy_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CommercialAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="autoPolicyLossDataScore" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AutoPolicyLossDataScore_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialAutoInsurancePolicy_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "autoPolicyLossDataScore"
})
public class CommercialAutoInsurancePolicyTO
    extends CommercialAgreementTO
{

    protected List<AutoPolicyLossDataScoreTO> autoPolicyLossDataScore;

    /**
     * Gets the value of the autoPolicyLossDataScore property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the autoPolicyLossDataScore property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutoPolicyLossDataScore().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutoPolicyLossDataScoreTO }
     * 
     * 
     */
    public List<AutoPolicyLossDataScoreTO> getAutoPolicyLossDataScore() {
        if (autoPolicyLossDataScore == null) {
            autoPolicyLossDataScore = new ArrayList<AutoPolicyLossDataScoreTO>();
        }
        return this.autoPolicyLossDataScore;
    }

    /**
     * Sets the value of the autoPolicyLossDataScore property.
     * 
     * @param autoPolicyLossDataScore
     *     allowed object is
     *     {@link AutoPolicyLossDataScoreTO }
     *     
     */
    public void setAutoPolicyLossDataScore(List<AutoPolicyLossDataScoreTO> autoPolicyLossDataScore) {
        this.autoPolicyLossDataScore = autoPolicyLossDataScore;
    }

}
