
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReportingChain_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReportingChain_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerRelationship_TO">
 *       &lt;sequence>
 *         &lt;element name="reportingType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingChain_TO", propOrder = {
    "reportingType"
})
public class ReportingChainTO
    extends RolePlayerRelationshipTO
{

    @XmlElement(required = true)
    protected String reportingType;

    /**
     * Gets the value of the reportingType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingType() {
        return reportingType;
    }

    /**
     * Sets the value of the reportingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingType(String value) {
        this.reportingType = value;
    }

}
