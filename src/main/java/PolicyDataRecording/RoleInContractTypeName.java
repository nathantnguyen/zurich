
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInContractTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInContractTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Contract Scheduling"/>
 *     &lt;enumeration value="Place Limitation"/>
 *     &lt;enumeration value="Contract Valuation"/>
 *     &lt;enumeration value="Supervision Requirement"/>
 *     &lt;enumeration value="Content Representation"/>
 *     &lt;enumeration value="Physical Object Reference"/>
 *     &lt;enumeration value="Physical Object Provision"/>
 *     &lt;enumeration value="Contract Account"/>
 *     &lt;enumeration value="Contract Reported By Account Statement"/>
 *     &lt;enumeration value="Holding Purpose"/>
 *     &lt;enumeration value="Contract Context Of Statement"/>
 *     &lt;enumeration value="Risk Capital Reserve"/>
 *     &lt;enumeration value="Need Fulfilment"/>
 *     &lt;enumeration value="Strategy Implementation"/>
 *     &lt;enumeration value="Organisation"/>
 *     &lt;enumeration value="Predominant State"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInContractTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum RoleInContractTypeName {

    @XmlEnumValue("Contract Scheduling")
    CONTRACT_SCHEDULING("Contract Scheduling"),
    @XmlEnumValue("Place Limitation")
    PLACE_LIMITATION("Place Limitation"),
    @XmlEnumValue("Contract Valuation")
    CONTRACT_VALUATION("Contract Valuation"),
    @XmlEnumValue("Supervision Requirement")
    SUPERVISION_REQUIREMENT("Supervision Requirement"),
    @XmlEnumValue("Content Representation")
    CONTENT_REPRESENTATION("Content Representation"),
    @XmlEnumValue("Physical Object Reference")
    PHYSICAL_OBJECT_REFERENCE("Physical Object Reference"),
    @XmlEnumValue("Physical Object Provision")
    PHYSICAL_OBJECT_PROVISION("Physical Object Provision"),
    @XmlEnumValue("Contract Account")
    CONTRACT_ACCOUNT("Contract Account"),
    @XmlEnumValue("Contract Reported By Account Statement")
    CONTRACT_REPORTED_BY_ACCOUNT_STATEMENT("Contract Reported By Account Statement"),
    @XmlEnumValue("Holding Purpose")
    HOLDING_PURPOSE("Holding Purpose"),
    @XmlEnumValue("Contract Context Of Statement")
    CONTRACT_CONTEXT_OF_STATEMENT("Contract Context Of Statement"),
    @XmlEnumValue("Risk Capital Reserve")
    RISK_CAPITAL_RESERVE("Risk Capital Reserve"),
    @XmlEnumValue("Need Fulfilment")
    NEED_FULFILMENT("Need Fulfilment"),
    @XmlEnumValue("Strategy Implementation")
    STRATEGY_IMPLEMENTATION("Strategy Implementation"),
    @XmlEnumValue("Organisation")
    ORGANISATION("Organisation"),
    @XmlEnumValue("Predominant State")
    PREDOMINANT_STATE("Predominant State");
    private final String value;

    RoleInContractTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInContractTypeName fromValue(String v) {
        for (RoleInContractTypeName c: RoleInContractTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
