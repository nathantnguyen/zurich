
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialServicesProduct_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesProduct_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="includedInCoverage" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="shortName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="excludedFromCoverage" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="productGroup" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ProductGroup_TO" minOccurs="0"/>
 *         &lt;element name="availableFinancialAssets" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="lineOfBusiness" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="financialServicesProductComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}financialservicesproductcomposition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="partiesInvolved" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}PartyInvolvedInFinancialServicesProduct_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="claimCostCodes" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}ClaimCostCode_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="descriptions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="usedStatistics" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="brand" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="replicable" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="sourceExternalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="generalActivities" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeneralActivity_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesProduct_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "includedInCoverage",
    "shortName",
    "excludedFromCoverage",
    "productGroup",
    "availableFinancialAssets",
    "lineOfBusiness",
    "financialServicesProductComponents",
    "partiesInvolved",
    "claimCostCodes",
    "descriptions",
    "usedStatistics",
    "brand",
    "replicable",
    "sourceExternalReference",
    "generalActivities"
})
@XmlSeeAlso({
    FinancialServicesProductComponentTO.class,
    MarketableProductTO.class
})
public class FinancialServicesProductTO
    extends ContractSpecificationTO
{

    protected List<CategoryTO> includedInCoverage;
    protected String shortName;
    protected List<CategoryTO> excludedFromCoverage;
    protected ProductGroupTO productGroup;
    protected List<FinancialAssetTO> availableFinancialAssets;
    protected String lineOfBusiness;
    protected List<FinancialservicesproductcompositionTO> financialServicesProductComponents;
    protected List<PartyInvolvedInFinancialServicesProductTO> partiesInvolved;
    protected List<ClaimCostCodeTO> claimCostCodes;
    protected List<CommunicationContentTO> descriptions;
    protected List<ObjectReferenceTO> usedStatistics;
    protected String brand;
    protected Boolean replicable;
    protected String sourceExternalReference;
    protected List<GeneralActivityTO> generalActivities;

    /**
     * Gets the value of the includedInCoverage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the includedInCoverage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludedInCoverage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getIncludedInCoverage() {
        if (includedInCoverage == null) {
            includedInCoverage = new ArrayList<CategoryTO>();
        }
        return this.includedInCoverage;
    }

    /**
     * Gets the value of the shortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Sets the value of the shortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortName(String value) {
        this.shortName = value;
    }

    /**
     * Gets the value of the excludedFromCoverage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the excludedFromCoverage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExcludedFromCoverage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getExcludedFromCoverage() {
        if (excludedFromCoverage == null) {
            excludedFromCoverage = new ArrayList<CategoryTO>();
        }
        return this.excludedFromCoverage;
    }

    /**
     * Gets the value of the productGroup property.
     * 
     * @return
     *     possible object is
     *     {@link ProductGroupTO }
     *     
     */
    public ProductGroupTO getProductGroup() {
        return productGroup;
    }

    /**
     * Sets the value of the productGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductGroupTO }
     *     
     */
    public void setProductGroup(ProductGroupTO value) {
        this.productGroup = value;
    }

    /**
     * Gets the value of the availableFinancialAssets property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the availableFinancialAssets property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAvailableFinancialAssets().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialAssetTO }
     * 
     * 
     */
    public List<FinancialAssetTO> getAvailableFinancialAssets() {
        if (availableFinancialAssets == null) {
            availableFinancialAssets = new ArrayList<FinancialAssetTO>();
        }
        return this.availableFinancialAssets;
    }

    /**
     * Gets the value of the lineOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    /**
     * Sets the value of the lineOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineOfBusiness(String value) {
        this.lineOfBusiness = value;
    }

    /**
     * Gets the value of the financialServicesProductComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the financialServicesProductComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFinancialServicesProductComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialservicesproductcompositionTO }
     * 
     * 
     */
    public List<FinancialservicesproductcompositionTO> getFinancialServicesProductComponents() {
        if (financialServicesProductComponents == null) {
            financialServicesProductComponents = new ArrayList<FinancialservicesproductcompositionTO>();
        }
        return this.financialServicesProductComponents;
    }

    /**
     * Gets the value of the partiesInvolved property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partiesInvolved property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartiesInvolved().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyInvolvedInFinancialServicesProductTO }
     * 
     * 
     */
    public List<PartyInvolvedInFinancialServicesProductTO> getPartiesInvolved() {
        if (partiesInvolved == null) {
            partiesInvolved = new ArrayList<PartyInvolvedInFinancialServicesProductTO>();
        }
        return this.partiesInvolved;
    }

    /**
     * Gets the value of the claimCostCodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimCostCodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimCostCodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimCostCodeTO }
     * 
     * 
     */
    public List<ClaimCostCodeTO> getClaimCostCodes() {
        if (claimCostCodes == null) {
            claimCostCodes = new ArrayList<ClaimCostCodeTO>();
        }
        return this.claimCostCodes;
    }

    /**
     * Gets the value of the descriptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the descriptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescriptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationContentTO }
     * 
     * 
     */
    public List<CommunicationContentTO> getDescriptions() {
        if (descriptions == null) {
            descriptions = new ArrayList<CommunicationContentTO>();
        }
        return this.descriptions;
    }

    /**
     * Gets the value of the usedStatistics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usedStatistics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsedStatistics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getUsedStatistics() {
        if (usedStatistics == null) {
            usedStatistics = new ArrayList<ObjectReferenceTO>();
        }
        return this.usedStatistics;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the replicable property.
     * This getter has been renamed from isReplicable() to getReplicable() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getReplicable() {
        return replicable;
    }

    /**
     * Sets the value of the replicable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReplicable(Boolean value) {
        this.replicable = value;
    }

    /**
     * Gets the value of the sourceExternalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceExternalReference() {
        return sourceExternalReference;
    }

    /**
     * Sets the value of the sourceExternalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceExternalReference(String value) {
        this.sourceExternalReference = value;
    }

    /**
     * Gets the value of the generalActivities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the generalActivities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeneralActivities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeneralActivityTO }
     * 
     * 
     */
    public List<GeneralActivityTO> getGeneralActivities() {
        if (generalActivities == null) {
            generalActivities = new ArrayList<GeneralActivityTO>();
        }
        return this.generalActivities;
    }

    /**
     * Sets the value of the includedInCoverage property.
     * 
     * @param includedInCoverage
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setIncludedInCoverage(List<CategoryTO> includedInCoverage) {
        this.includedInCoverage = includedInCoverage;
    }

    /**
     * Sets the value of the excludedFromCoverage property.
     * 
     * @param excludedFromCoverage
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setExcludedFromCoverage(List<CategoryTO> excludedFromCoverage) {
        this.excludedFromCoverage = excludedFromCoverage;
    }

    /**
     * Sets the value of the availableFinancialAssets property.
     * 
     * @param availableFinancialAssets
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setAvailableFinancialAssets(List<FinancialAssetTO> availableFinancialAssets) {
        this.availableFinancialAssets = availableFinancialAssets;
    }

    /**
     * Sets the value of the financialServicesProductComponents property.
     * 
     * @param financialServicesProductComponents
     *     allowed object is
     *     {@link FinancialservicesproductcompositionTO }
     *     
     */
    public void setFinancialServicesProductComponents(List<FinancialservicesproductcompositionTO> financialServicesProductComponents) {
        this.financialServicesProductComponents = financialServicesProductComponents;
    }

    /**
     * Sets the value of the partiesInvolved property.
     * 
     * @param partiesInvolved
     *     allowed object is
     *     {@link PartyInvolvedInFinancialServicesProductTO }
     *     
     */
    public void setPartiesInvolved(List<PartyInvolvedInFinancialServicesProductTO> partiesInvolved) {
        this.partiesInvolved = partiesInvolved;
    }

    /**
     * Sets the value of the claimCostCodes property.
     * 
     * @param claimCostCodes
     *     allowed object is
     *     {@link ClaimCostCodeTO }
     *     
     */
    public void setClaimCostCodes(List<ClaimCostCodeTO> claimCostCodes) {
        this.claimCostCodes = claimCostCodes;
    }

    /**
     * Sets the value of the descriptions property.
     * 
     * @param descriptions
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setDescriptions(List<CommunicationContentTO> descriptions) {
        this.descriptions = descriptions;
    }

    /**
     * Sets the value of the usedStatistics property.
     * 
     * @param usedStatistics
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setUsedStatistics(List<ObjectReferenceTO> usedStatistics) {
        this.usedStatistics = usedStatistics;
    }

    /**
     * Sets the value of the generalActivities property.
     * 
     * @param generalActivities
     *     allowed object is
     *     {@link GeneralActivityTO }
     *     
     */
    public void setGeneralActivities(List<GeneralActivityTO> generalActivities) {
        this.generalActivities = generalActivities;
    }

}
