
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CauseOfDispute.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CauseOfDispute">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Benefit Amount"/>
 *     &lt;enumeration value="Coverage Rejection"/>
 *     &lt;enumeration value="Loss Estimate"/>
 *     &lt;enumeration value="Responsibility In Loss Event"/>
 *     &lt;enumeration value="Circumstances Of Caused Event"/>
 *     &lt;enumeration value="Severity Of Damage"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CauseOfDispute", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/DisputeResolution/DisputeResolutionEnumerationsAndStates/")
@XmlEnum
public enum CauseOfDispute {

    @XmlEnumValue("Benefit Amount")
    BENEFIT_AMOUNT("Benefit Amount"),
    @XmlEnumValue("Coverage Rejection")
    COVERAGE_REJECTION("Coverage Rejection"),
    @XmlEnumValue("Loss Estimate")
    LOSS_ESTIMATE("Loss Estimate"),
    @XmlEnumValue("Responsibility In Loss Event")
    RESPONSIBILITY_IN_LOSS_EVENT("Responsibility In Loss Event"),
    @XmlEnumValue("Circumstances Of Caused Event")
    CIRCUMSTANCES_OF_CAUSED_EVENT("Circumstances Of Caused Event"),
    @XmlEnumValue("Severity Of Damage")
    SEVERITY_OF_DAMAGE("Severity Of Damage");
    private final String value;

    CauseOfDispute(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CauseOfDispute fromValue(String v) {
        for (CauseOfDispute c: CauseOfDispute.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
