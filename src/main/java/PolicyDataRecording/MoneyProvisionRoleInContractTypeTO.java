
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyProvisionRoleInContractType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyProvisionRoleInContractType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleType_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}MoneyProvisionRoleInContractTypeName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyProvisionRoleInContractType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "name"
})
public class MoneyProvisionRoleInContractTypeTO
    extends RoleTypeTO
{

    protected MoneyProvisionRoleInContractTypeName name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionRoleInContractTypeName }
     *     
     */
    public MoneyProvisionRoleInContractTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionRoleInContractTypeName }
     *     
     */
    public void setName(MoneyProvisionRoleInContractTypeName value) {
        this.name = value;
    }

}
