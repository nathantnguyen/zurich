
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardTextSpecificationInvolvedInContractSpecification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandardTextSpecificationInvolvedInContractSpecification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContractSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="documentTemplates" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}DocumentTemplate_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardTextSpecificationInvolvedInContractSpecification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "documentTemplates"
})
public class StandardTextSpecificationInvolvedInContractSpecificationTO
    extends RoleInContractSpecificationTO
{

    protected DocumentTemplateTO documentTemplates;

    /**
     * Gets the value of the documentTemplates property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentTemplateTO }
     *     
     */
    public DocumentTemplateTO getDocumentTemplates() {
        return documentTemplates;
    }

    /**
     * Sets the value of the documentTemplates property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentTemplateTO }
     *     
     */
    public void setDocumentTemplates(DocumentTemplateTO value) {
        this.documentTemplates = value;
    }

}
