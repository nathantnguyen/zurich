
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Subtotal_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Subtotal_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}StatementElement_TO">
 *       &lt;sequence>
 *         &lt;element name="subtotalComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}StatementLine_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Subtotal_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "subtotalComponents"
})
public class SubtotalTO
    extends StatementElementTO
{

    protected List<StatementLineTO> subtotalComponents;

    /**
     * Gets the value of the subtotalComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subtotalComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubtotalComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatementLineTO }
     * 
     * 
     */
    public List<StatementLineTO> getSubtotalComponents() {
        if (subtotalComponents == null) {
            subtotalComponents = new ArrayList<StatementLineTO>();
        }
        return this.subtotalComponents;
    }

    /**
     * Sets the value of the subtotalComponents property.
     * 
     * @param subtotalComponents
     *     allowed object is
     *     {@link StatementLineTO }
     *     
     */
    public void setSubtotalComponents(List<StatementLineTO> subtotalComponents) {
        this.subtotalComponents = subtotalComponents;
    }

}
