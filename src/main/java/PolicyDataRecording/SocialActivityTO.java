
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SocialActivity_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SocialActivity_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO">
 *       &lt;sequence>
 *         &lt;element name="socialActivityConditions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SocialActivity_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "socialActivityConditions"
})
public class SocialActivityTO
    extends ActivityOccurrenceTO
{

    protected ConditionTO socialActivityConditions;

    /**
     * Gets the value of the socialActivityConditions property.
     * 
     * @return
     *     possible object is
     *     {@link ConditionTO }
     *     
     */
    public ConditionTO getSocialActivityConditions() {
        return socialActivityConditions;
    }

    /**
     * Sets the value of the socialActivityConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionTO }
     *     
     */
    public void setSocialActivityConditions(ConditionTO value) {
        this.socialActivityConditions = value;
    }

}
