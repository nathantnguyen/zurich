
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BankingProfile.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BankingProfile">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Several Banks"/>
 *     &lt;enumeration value="Single Bank"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BankingProfile", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum BankingProfile {

    @XmlEnumValue("Several Banks")
    SEVERAL_BANKS("Several Banks"),
    @XmlEnumValue("Single Bank")
    SINGLE_BANK("Single Bank");
    private final String value;

    BankingProfile(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BankingProfile fromValue(String v) {
        for (BankingProfile c: BankingProfile.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
