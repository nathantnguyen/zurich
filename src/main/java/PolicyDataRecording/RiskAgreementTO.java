
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RiskAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}CorporateAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="riskAssessment" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskAssessment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="riskModelSourceType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}RiskModelSourceType" minOccurs="0"/>
 *         &lt;element name="riskPeriodEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="riskPeriodStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="estimatedRiskExposures" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskExposure_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cost" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO" minOccurs="0"/>
 *         &lt;element name="applicableActivities" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="applicableLimits" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="estimatedRiskFactorScore" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskFactorScore_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="partiesInvolvedInRiskAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}PartyInvolvedInRiskAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="riskAgreementRegistrations" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="riskAgreementSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="riskAgreementReports" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="managedPerilTypes" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "riskAssessment",
    "riskModelSourceType",
    "riskPeriodEndDate",
    "riskPeriodStartDate",
    "estimatedRiskExposures",
    "cost",
    "applicableActivities",
    "applicableLimits",
    "estimatedRiskFactorScore",
    "partiesInvolvedInRiskAgreement",
    "riskAgreementRegistrations",
    "riskAgreementSpecification",
    "riskAgreementReports",
    "managedPerilTypes"
})
@XmlSeeAlso({
    RiskAssessmentScenarioTO.class,
    RiskManagementAgreementTO.class
})
public class RiskAgreementTO
    extends CorporateAgreementTO
{

    protected List<RiskAssessmentTO> riskAssessment;
    protected RiskModelSourceType riskModelSourceType;
    protected XMLGregorianCalendar riskPeriodEndDate;
    protected XMLGregorianCalendar riskPeriodStartDate;
    protected List<RiskExposureTO> estimatedRiskExposures;
    protected MoneyProvisionTO cost;
    protected List<ActivityOccurrenceTO> applicableActivities;
    protected List<MoneyProvisionTO> applicableLimits;
    protected List<RiskFactorScoreTO> estimatedRiskFactorScore;
    protected List<PartyInvolvedInRiskAgreementTO> partiesInvolvedInRiskAgreement;
    protected List<ObjectReferenceTO> riskAgreementRegistrations;
    protected ObjectReferenceTO riskAgreementSpecification;
    protected List<ObjectReferenceTO> riskAgreementReports;
    protected List<TypeTO> managedPerilTypes;

    /**
     * Gets the value of the riskAssessment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riskAssessment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRiskAssessment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskAssessmentTO }
     * 
     * 
     */
    public List<RiskAssessmentTO> getRiskAssessment() {
        if (riskAssessment == null) {
            riskAssessment = new ArrayList<RiskAssessmentTO>();
        }
        return this.riskAssessment;
    }

    /**
     * Gets the value of the riskModelSourceType property.
     * 
     * @return
     *     possible object is
     *     {@link RiskModelSourceType }
     *     
     */
    public RiskModelSourceType getRiskModelSourceType() {
        return riskModelSourceType;
    }

    /**
     * Sets the value of the riskModelSourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskModelSourceType }
     *     
     */
    public void setRiskModelSourceType(RiskModelSourceType value) {
        this.riskModelSourceType = value;
    }

    /**
     * Gets the value of the riskPeriodEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRiskPeriodEndDate() {
        return riskPeriodEndDate;
    }

    /**
     * Sets the value of the riskPeriodEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRiskPeriodEndDate(XMLGregorianCalendar value) {
        this.riskPeriodEndDate = value;
    }

    /**
     * Gets the value of the riskPeriodStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRiskPeriodStartDate() {
        return riskPeriodStartDate;
    }

    /**
     * Sets the value of the riskPeriodStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRiskPeriodStartDate(XMLGregorianCalendar value) {
        this.riskPeriodStartDate = value;
    }

    /**
     * Gets the value of the estimatedRiskExposures property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the estimatedRiskExposures property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEstimatedRiskExposures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskExposureTO }
     * 
     * 
     */
    public List<RiskExposureTO> getEstimatedRiskExposures() {
        if (estimatedRiskExposures == null) {
            estimatedRiskExposures = new ArrayList<RiskExposureTO>();
        }
        return this.estimatedRiskExposures;
    }

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public MoneyProvisionTO getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public void setCost(MoneyProvisionTO value) {
        this.cost = value;
    }

    /**
     * Gets the value of the applicableActivities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableActivities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicableActivities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceTO }
     * 
     * 
     */
    public List<ActivityOccurrenceTO> getApplicableActivities() {
        if (applicableActivities == null) {
            applicableActivities = new ArrayList<ActivityOccurrenceTO>();
        }
        return this.applicableActivities;
    }

    /**
     * Gets the value of the applicableLimits property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableLimits property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicableLimits().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionTO }
     * 
     * 
     */
    public List<MoneyProvisionTO> getApplicableLimits() {
        if (applicableLimits == null) {
            applicableLimits = new ArrayList<MoneyProvisionTO>();
        }
        return this.applicableLimits;
    }

    /**
     * Gets the value of the estimatedRiskFactorScore property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the estimatedRiskFactorScore property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEstimatedRiskFactorScore().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RiskFactorScoreTO }
     * 
     * 
     */
    public List<RiskFactorScoreTO> getEstimatedRiskFactorScore() {
        if (estimatedRiskFactorScore == null) {
            estimatedRiskFactorScore = new ArrayList<RiskFactorScoreTO>();
        }
        return this.estimatedRiskFactorScore;
    }

    /**
     * Gets the value of the partiesInvolvedInRiskAgreement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partiesInvolvedInRiskAgreement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartiesInvolvedInRiskAgreement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyInvolvedInRiskAgreementTO }
     * 
     * 
     */
    public List<PartyInvolvedInRiskAgreementTO> getPartiesInvolvedInRiskAgreement() {
        if (partiesInvolvedInRiskAgreement == null) {
            partiesInvolvedInRiskAgreement = new ArrayList<PartyInvolvedInRiskAgreementTO>();
        }
        return this.partiesInvolvedInRiskAgreement;
    }

    /**
     * Gets the value of the riskAgreementRegistrations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riskAgreementRegistrations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRiskAgreementRegistrations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getRiskAgreementRegistrations() {
        if (riskAgreementRegistrations == null) {
            riskAgreementRegistrations = new ArrayList<ObjectReferenceTO>();
        }
        return this.riskAgreementRegistrations;
    }

    /**
     * Gets the value of the riskAgreementSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getRiskAgreementSpecification() {
        return riskAgreementSpecification;
    }

    /**
     * Sets the value of the riskAgreementSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setRiskAgreementSpecification(ObjectReferenceTO value) {
        this.riskAgreementSpecification = value;
    }

    /**
     * Gets the value of the riskAgreementReports property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riskAgreementReports property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRiskAgreementReports().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getRiskAgreementReports() {
        if (riskAgreementReports == null) {
            riskAgreementReports = new ArrayList<ObjectReferenceTO>();
        }
        return this.riskAgreementReports;
    }

    /**
     * Gets the value of the managedPerilTypes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the managedPerilTypes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getManagedPerilTypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeTO }
     * 
     * 
     */
    public List<TypeTO> getManagedPerilTypes() {
        if (managedPerilTypes == null) {
            managedPerilTypes = new ArrayList<TypeTO>();
        }
        return this.managedPerilTypes;
    }

    /**
     * Sets the value of the riskAssessment property.
     * 
     * @param riskAssessment
     *     allowed object is
     *     {@link RiskAssessmentTO }
     *     
     */
    public void setRiskAssessment(List<RiskAssessmentTO> riskAssessment) {
        this.riskAssessment = riskAssessment;
    }

    /**
     * Sets the value of the estimatedRiskExposures property.
     * 
     * @param estimatedRiskExposures
     *     allowed object is
     *     {@link RiskExposureTO }
     *     
     */
    public void setEstimatedRiskExposures(List<RiskExposureTO> estimatedRiskExposures) {
        this.estimatedRiskExposures = estimatedRiskExposures;
    }

    /**
     * Sets the value of the applicableActivities property.
     * 
     * @param applicableActivities
     *     allowed object is
     *     {@link ActivityOccurrenceTO }
     *     
     */
    public void setApplicableActivities(List<ActivityOccurrenceTO> applicableActivities) {
        this.applicableActivities = applicableActivities;
    }

    /**
     * Sets the value of the applicableLimits property.
     * 
     * @param applicableLimits
     *     allowed object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public void setApplicableLimits(List<MoneyProvisionTO> applicableLimits) {
        this.applicableLimits = applicableLimits;
    }

    /**
     * Sets the value of the estimatedRiskFactorScore property.
     * 
     * @param estimatedRiskFactorScore
     *     allowed object is
     *     {@link RiskFactorScoreTO }
     *     
     */
    public void setEstimatedRiskFactorScore(List<RiskFactorScoreTO> estimatedRiskFactorScore) {
        this.estimatedRiskFactorScore = estimatedRiskFactorScore;
    }

    /**
     * Sets the value of the partiesInvolvedInRiskAgreement property.
     * 
     * @param partiesInvolvedInRiskAgreement
     *     allowed object is
     *     {@link PartyInvolvedInRiskAgreementTO }
     *     
     */
    public void setPartiesInvolvedInRiskAgreement(List<PartyInvolvedInRiskAgreementTO> partiesInvolvedInRiskAgreement) {
        this.partiesInvolvedInRiskAgreement = partiesInvolvedInRiskAgreement;
    }

    /**
     * Sets the value of the riskAgreementRegistrations property.
     * 
     * @param riskAgreementRegistrations
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setRiskAgreementRegistrations(List<ObjectReferenceTO> riskAgreementRegistrations) {
        this.riskAgreementRegistrations = riskAgreementRegistrations;
    }

    /**
     * Sets the value of the riskAgreementReports property.
     * 
     * @param riskAgreementReports
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setRiskAgreementReports(List<ObjectReferenceTO> riskAgreementReports) {
        this.riskAgreementReports = riskAgreementReports;
    }

    /**
     * Sets the value of the managedPerilTypes property.
     * 
     * @param managedPerilTypes
     *     allowed object is
     *     {@link TypeTO }
     *     
     */
    public void setManagedPerilTypes(List<TypeTO> managedPerilTypes) {
        this.managedPerilTypes = managedPerilTypes;
    }

}
