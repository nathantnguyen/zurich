
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SolvencyRiskParameters_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SolvencyRiskParameters_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Statistics_TO">
 *       &lt;sequence>
 *         &lt;element name="riskModelType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/}SelectionKind" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolvencyRiskParameters_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/", propOrder = {
    "riskModelType"
})
public class SolvencyRiskParametersTO
    extends StatisticsTO
{

    protected SelectionKind riskModelType;

    /**
     * Gets the value of the riskModelType property.
     * 
     * @return
     *     possible object is
     *     {@link SelectionKind }
     *     
     */
    public SelectionKind getRiskModelType() {
        return riskModelType;
    }

    /**
     * Sets the value of the riskModelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectionKind }
     *     
     */
    public void setRiskModelType(SelectionKind value) {
        this.riskModelType = value;
    }

}
