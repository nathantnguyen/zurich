
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInFinancialServicesAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInFinancialServicesAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Activity Coverage"/>
 *     &lt;enumeration value="Surrender Value"/>
 *     &lt;enumeration value="Preexisting Condition"/>
 *     &lt;enumeration value="Group Scheme Offer"/>
 *     &lt;enumeration value="Holding Fund"/>
 *     &lt;enumeration value="Customer Account Funding"/>
 *     &lt;enumeration value="Underlying Asset"/>
 *     &lt;enumeration value="Underlying Derivative"/>
 *     &lt;enumeration value="Covered Prescription Drug"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInFinancialServicesAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum RoleInFinancialServicesAgreementTypeName {

    @XmlEnumValue("Activity Coverage")
    ACTIVITY_COVERAGE("Activity Coverage"),
    @XmlEnumValue("Surrender Value")
    SURRENDER_VALUE("Surrender Value"),
    @XmlEnumValue("Preexisting Condition")
    PREEXISTING_CONDITION("Preexisting Condition"),
    @XmlEnumValue("Group Scheme Offer")
    GROUP_SCHEME_OFFER("Group Scheme Offer"),
    @XmlEnumValue("Holding Fund")
    HOLDING_FUND("Holding Fund"),
    @XmlEnumValue("Customer Account Funding")
    CUSTOMER_ACCOUNT_FUNDING("Customer Account Funding"),
    @XmlEnumValue("Underlying Asset")
    UNDERLYING_ASSET("Underlying Asset"),
    @XmlEnumValue("Underlying Derivative")
    UNDERLYING_DERIVATIVE("Underlying Derivative"),
    @XmlEnumValue("Covered Prescription Drug")
    COVERED_PRESCRIPTION_DRUG("Covered Prescription Drug");
    private final String value;

    RoleInFinancialServicesAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInFinancialServicesAgreementTypeName fromValue(String v) {
        for (RoleInFinancialServicesAgreementTypeName c: RoleInFinancialServicesAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
