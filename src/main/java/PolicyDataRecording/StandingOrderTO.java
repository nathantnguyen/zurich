
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for StandingOrder_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandingOrder_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}TransferFacility_TO">
 *       &lt;sequence>
 *         &lt;element name="fixedAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="executionDateIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}ExecutionDateIndicator" minOccurs="0"/>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="firstPaymentDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="initialAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="finalAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="targetAccountAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}AccountAgreement_TO" minOccurs="0"/>
 *         &lt;element name="totalNumberOfInstances" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="skipNextNumberOfTransfers" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandingOrder_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "fixedAmount",
    "executionDateIndicator",
    "frequency",
    "firstPaymentDate",
    "initialAmount",
    "finalAmount",
    "targetAccountAgreement",
    "totalNumberOfInstances",
    "skipNextNumberOfTransfers"
})
public class StandingOrderTO
    extends TransferFacilityTO
{

    protected BaseCurrencyAmount fixedAmount;
    protected ExecutionDateIndicator executionDateIndicator;
    protected Frequency frequency;
    protected XMLGregorianCalendar firstPaymentDate;
    protected BaseCurrencyAmount initialAmount;
    protected BaseCurrencyAmount finalAmount;
    protected AccountAgreementTO targetAccountAgreement;
    protected BigInteger totalNumberOfInstances;
    protected BigInteger skipNextNumberOfTransfers;

    /**
     * Gets the value of the fixedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getFixedAmount() {
        return fixedAmount;
    }

    /**
     * Sets the value of the fixedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setFixedAmount(BaseCurrencyAmount value) {
        this.fixedAmount = value;
    }

    /**
     * Gets the value of the executionDateIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link ExecutionDateIndicator }
     *     
     */
    public ExecutionDateIndicator getExecutionDateIndicator() {
        return executionDateIndicator;
    }

    /**
     * Sets the value of the executionDateIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutionDateIndicator }
     *     
     */
    public void setExecutionDateIndicator(ExecutionDateIndicator value) {
        this.executionDateIndicator = value;
    }

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the firstPaymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstPaymentDate() {
        return firstPaymentDate;
    }

    /**
     * Sets the value of the firstPaymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstPaymentDate(XMLGregorianCalendar value) {
        this.firstPaymentDate = value;
    }

    /**
     * Gets the value of the initialAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getInitialAmount() {
        return initialAmount;
    }

    /**
     * Sets the value of the initialAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setInitialAmount(BaseCurrencyAmount value) {
        this.initialAmount = value;
    }

    /**
     * Gets the value of the finalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getFinalAmount() {
        return finalAmount;
    }

    /**
     * Sets the value of the finalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setFinalAmount(BaseCurrencyAmount value) {
        this.finalAmount = value;
    }

    /**
     * Gets the value of the targetAccountAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link AccountAgreementTO }
     *     
     */
    public AccountAgreementTO getTargetAccountAgreement() {
        return targetAccountAgreement;
    }

    /**
     * Sets the value of the targetAccountAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountAgreementTO }
     *     
     */
    public void setTargetAccountAgreement(AccountAgreementTO value) {
        this.targetAccountAgreement = value;
    }

    /**
     * Gets the value of the totalNumberOfInstances property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalNumberOfInstances() {
        return totalNumberOfInstances;
    }

    /**
     * Sets the value of the totalNumberOfInstances property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalNumberOfInstances(BigInteger value) {
        this.totalNumberOfInstances = value;
    }

    /**
     * Gets the value of the skipNextNumberOfTransfers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSkipNextNumberOfTransfers() {
        return skipNextNumberOfTransfers;
    }

    /**
     * Sets the value of the skipNextNumberOfTransfers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSkipNextNumberOfTransfers(BigInteger value) {
        this.skipNextNumberOfTransfers = value;
    }

}
