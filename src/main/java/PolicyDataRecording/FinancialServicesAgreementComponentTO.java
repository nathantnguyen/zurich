
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialServicesAgreementComponent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesAgreementComponent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="parentFinancialServicesAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreement_TO" minOccurs="0"/>
 *         &lt;element name="renewal" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Status_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rateClass" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesAgreementComponent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "parentFinancialServicesAgreement",
    "renewal",
    "status",
    "rateClass"
})
@XmlSeeAlso({
    StructuralComponentTO.class,
    SavingsComponentTO.class,
    ReinsuranceAgreementComponentTO.class,
    ProfitSharingComponentTO.class,
    ObligationComponentTO.class,
    GuaranteeTO.class,
    ServiceComponentTO.class,
    CoverageComponentTO.class,
    LoanComponentTO.class
})
public class FinancialServicesAgreementComponentTO
    extends FinancialServicesAgreementTO
{

    protected FinancialServicesAgreementTO parentFinancialServicesAgreement;
    protected Boolean renewal;
    protected List<StatusTO> status;
    protected String rateClass;

    /**
     * Gets the value of the parentFinancialServicesAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesAgreementTO }
     *     
     */
    public FinancialServicesAgreementTO getParentFinancialServicesAgreement() {
        return parentFinancialServicesAgreement;
    }

    /**
     * Sets the value of the parentFinancialServicesAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesAgreementTO }
     *     
     */
    public void setParentFinancialServicesAgreement(FinancialServicesAgreementTO value) {
        this.parentFinancialServicesAgreement = value;
    }

    /**
     * Gets the value of the renewal property.
     * This getter has been renamed from isRenewal() to getRenewal() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRenewal() {
        return renewal;
    }

    /**
     * Sets the value of the renewal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRenewal(Boolean value) {
        this.renewal = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusTO }
     * 
     * 
     */
    public List<StatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the rateClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateClass() {
        return rateClass;
    }

    /**
     * Sets the value of the rateClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateClass(String value) {
        this.rateClass = value;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link StatusTO }
     *     
     */
    public void setStatus(List<StatusTO> status) {
        this.status = status;
    }

}
