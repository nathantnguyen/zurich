
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityOfDailyLivingCapabilityLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivityOfDailyLivingCapabilityLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Hands On Assistance"/>
 *     &lt;enumeration value="No Assistance Required"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActivityOfDailyLivingCapabilityLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum ActivityOfDailyLivingCapabilityLevel {

    @XmlEnumValue("Hands On Assistance")
    HANDS_ON_ASSISTANCE("Hands On Assistance"),
    @XmlEnumValue("No Assistance Required")
    NO_ASSISTANCE_REQUIRED("No Assistance Required");
    private final String value;

    ActivityOfDailyLivingCapabilityLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActivityOfDailyLivingCapabilityLevel fromValue(String v) {
        for (ActivityOfDailyLivingCapabilityLevel c: ActivityOfDailyLivingCapabilityLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
