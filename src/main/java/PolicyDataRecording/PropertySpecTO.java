
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PropertySpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PropertySpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AttributeSpec_TO">
 *       &lt;sequence>
 *         &lt;element name="defaultValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Value" minOccurs="0"/>
 *         &lt;element name="allowedValues" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Value" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="isCalculated" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isOptional" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="canBeOverwritten" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PropertySpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "defaultValue",
    "allowedValues",
    "isCalculated",
    "isOptional",
    "canBeOverwritten"
})
public class PropertySpecTO
    extends AttributeSpecTO
{

    protected Value defaultValue;
    protected List<Value> allowedValues;
    protected Boolean isCalculated;
    protected Boolean isOptional;
    protected Boolean canBeOverwritten;

    /**
     * Gets the value of the defaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link Value }
     *     
     */
    public Value getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the value of the defaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Value }
     *     
     */
    public void setDefaultValue(Value value) {
        this.defaultValue = value;
    }

    /**
     * Gets the value of the allowedValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allowedValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllowedValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Value }
     * 
     * 
     */
    public List<Value> getAllowedValues() {
        if (allowedValues == null) {
            allowedValues = new ArrayList<Value>();
        }
        return this.allowedValues;
    }

    /**
     * Gets the value of the isCalculated property.
     * This getter has been renamed from isIsCalculated() to getIsCalculated() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsCalculated() {
        return isCalculated;
    }

    /**
     * Sets the value of the isCalculated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCalculated(Boolean value) {
        this.isCalculated = value;
    }

    /**
     * Gets the value of the isOptional property.
     * This getter has been renamed from isIsOptional() to getIsOptional() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOptional() {
        return isOptional;
    }

    /**
     * Sets the value of the isOptional property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOptional(Boolean value) {
        this.isOptional = value;
    }

    /**
     * Gets the value of the canBeOverwritten property.
     * This getter has been renamed from isCanBeOverwritten() to getCanBeOverwritten() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCanBeOverwritten() {
        return canBeOverwritten;
    }

    /**
     * Sets the value of the canBeOverwritten property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCanBeOverwritten(Boolean value) {
        this.canBeOverwritten = value;
    }

    /**
     * Sets the value of the allowedValues property.
     * 
     * @param allowedValues
     *     allowed object is
     *     {@link Value }
     *     
     */
    public void setAllowedValues(List<Value> allowedValues) {
        this.allowedValues = allowedValues;
    }

}
