
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialServicesRoleType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesRoleType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleType_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}FinancialServicesRoleTypeName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesRoleType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "name"
})
public class FinancialServicesRoleTypeTO
    extends RoleTypeTO
{

    protected FinancialServicesRoleTypeName name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesRoleTypeName }
     *     
     */
    public FinancialServicesRoleTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesRoleTypeName }
     *     
     */
    public void setName(FinancialServicesRoleTypeName value) {
        this.name = value;
    }

}
