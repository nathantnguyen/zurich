
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InsuredDesignation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InsuredDesignation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="By Name"/>
 *     &lt;enumeration value="All Subsidiaries Of Main Insured"/>
 *     &lt;enumeration value="All Subcontractors Of Main Insured"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InsuredDesignation", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum InsuredDesignation {

    @XmlEnumValue("By Name")
    BY_NAME("By Name"),
    @XmlEnumValue("All Subsidiaries Of Main Insured")
    ALL_SUBSIDIARIES_OF_MAIN_INSURED("All Subsidiaries Of Main Insured"),
    @XmlEnumValue("All Subcontractors Of Main Insured")
    ALL_SUBCONTRACTORS_OF_MAIN_INSURED("All Subcontractors Of Main Insured");
    private final String value;

    InsuredDesignation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InsuredDesignation fromValue(String v) {
        for (InsuredDesignation c: InsuredDesignation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
