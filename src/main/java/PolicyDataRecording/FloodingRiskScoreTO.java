
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FloodingRiskScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FloodingRiskScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}StructureScore_TO">
 *       &lt;sequence>
 *         &lt;element name="baseFloodElevation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="lowestAdjacentGrade" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FloodingRiskScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "baseFloodElevation",
    "lowestAdjacentGrade"
})
public class FloodingRiskScoreTO
    extends StructureScoreTO
{

    protected Amount baseFloodElevation;
    protected Amount lowestAdjacentGrade;

    /**
     * Gets the value of the baseFloodElevation property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getBaseFloodElevation() {
        return baseFloodElevation;
    }

    /**
     * Sets the value of the baseFloodElevation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setBaseFloodElevation(Amount value) {
        this.baseFloodElevation = value;
    }

    /**
     * Gets the value of the lowestAdjacentGrade property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getLowestAdjacentGrade() {
        return lowestAdjacentGrade;
    }

    /**
     * Sets the value of the lowestAdjacentGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setLowestAdjacentGrade(Amount value) {
        this.lowestAdjacentGrade = value;
    }

}
