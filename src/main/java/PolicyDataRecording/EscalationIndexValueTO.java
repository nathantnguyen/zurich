
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for EscalationIndexValue_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EscalationIndexValue_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexValue_TO">
 *       &lt;sequence>
 *         &lt;element name="relativeStartTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="relativeEndTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EscalationIndexValue_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/", propOrder = {
    "relativeStartTime",
    "relativeEndTime"
})
public class EscalationIndexValueTO
    extends IndexValueTO
{

    protected Duration relativeStartTime;
    protected Duration relativeEndTime;

    /**
     * Gets the value of the relativeStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getRelativeStartTime() {
        return relativeStartTime;
    }

    /**
     * Sets the value of the relativeStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setRelativeStartTime(Duration value) {
        this.relativeStartTime = value;
    }

    /**
     * Gets the value of the relativeEndTime property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getRelativeEndTime() {
        return relativeEndTime;
    }

    /**
     * Sets the value of the relativeEndTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setRelativeEndTime(Duration value) {
        this.relativeEndTime = value;
    }

}
