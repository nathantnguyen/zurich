
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EducationCertificateLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EducationCertificateLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Professional Certification"/>
 *     &lt;enumeration value="Mba"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EducationCertificateLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum EducationCertificateLevel {

    @XmlEnumValue("Professional Certification")
    PROFESSIONAL_CERTIFICATION("Professional Certification"),
    @XmlEnumValue("Mba")
    MBA("Mba");
    private final String value;

    EducationCertificateLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EducationCertificateLevel fromValue(String v) {
        for (EducationCertificateLevel c: EducationCertificateLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
