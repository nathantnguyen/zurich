
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GenericMoneyProvision_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenericMoneyProvision_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO">
 *       &lt;sequence>
 *         &lt;element name="usedRates" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Rate_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="investmentProfile" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}InvestmentProfile_TO" minOccurs="0"/>
 *         &lt;element name="allocationKey" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AllocationKey_TO" minOccurs="0"/>
 *         &lt;element name="deductibles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Deductible_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="limits" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Limit_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="applicableRates" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Rate_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericMoneyProvision_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "usedRates",
    "investmentProfile",
    "allocationKey",
    "deductibles",
    "limits",
    "applicableRates"
})
@XmlSeeAlso({
    CommissionScheduleTO.class
})
public class GenericMoneyProvisionTO
    extends MoneyProvisionTO
{

    protected List<RateTO> usedRates;
    protected InvestmentProfileTO investmentProfile;
    protected AllocationKeyTO allocationKey;
    protected List<DeductibleTO> deductibles;
    protected List<LimitTO> limits;
    protected List<RateTO> applicableRates;

    /**
     * Gets the value of the usedRates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usedRates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsedRates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RateTO }
     * 
     * 
     */
    public List<RateTO> getUsedRates() {
        if (usedRates == null) {
            usedRates = new ArrayList<RateTO>();
        }
        return this.usedRates;
    }

    /**
     * Gets the value of the investmentProfile property.
     * 
     * @return
     *     possible object is
     *     {@link InvestmentProfileTO }
     *     
     */
    public InvestmentProfileTO getInvestmentProfile() {
        return investmentProfile;
    }

    /**
     * Sets the value of the investmentProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvestmentProfileTO }
     *     
     */
    public void setInvestmentProfile(InvestmentProfileTO value) {
        this.investmentProfile = value;
    }

    /**
     * Gets the value of the allocationKey property.
     * 
     * @return
     *     possible object is
     *     {@link AllocationKeyTO }
     *     
     */
    public AllocationKeyTO getAllocationKey() {
        return allocationKey;
    }

    /**
     * Sets the value of the allocationKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllocationKeyTO }
     *     
     */
    public void setAllocationKey(AllocationKeyTO value) {
        this.allocationKey = value;
    }

    /**
     * Gets the value of the deductibles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deductibles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeductibles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeductibleTO }
     * 
     * 
     */
    public List<DeductibleTO> getDeductibles() {
        if (deductibles == null) {
            deductibles = new ArrayList<DeductibleTO>();
        }
        return this.deductibles;
    }

    /**
     * Gets the value of the limits property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the limits property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLimits().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LimitTO }
     * 
     * 
     */
    public List<LimitTO> getLimits() {
        if (limits == null) {
            limits = new ArrayList<LimitTO>();
        }
        return this.limits;
    }

    /**
     * Gets the value of the applicableRates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableRates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicableRates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RateTO }
     * 
     * 
     */
    public List<RateTO> getApplicableRates() {
        if (applicableRates == null) {
            applicableRates = new ArrayList<RateTO>();
        }
        return this.applicableRates;
    }

    /**
     * Sets the value of the usedRates property.
     * 
     * @param usedRates
     *     allowed object is
     *     {@link RateTO }
     *     
     */
    public void setUsedRates(List<RateTO> usedRates) {
        this.usedRates = usedRates;
    }

    /**
     * Sets the value of the deductibles property.
     * 
     * @param deductibles
     *     allowed object is
     *     {@link DeductibleTO }
     *     
     */
    public void setDeductibles(List<DeductibleTO> deductibles) {
        this.deductibles = deductibles;
    }

    /**
     * Sets the value of the limits property.
     * 
     * @param limits
     *     allowed object is
     *     {@link LimitTO }
     *     
     */
    public void setLimits(List<LimitTO> limits) {
        this.limits = limits;
    }

    /**
     * Sets the value of the applicableRates property.
     * 
     * @param applicableRates
     *     allowed object is
     *     {@link RateTO }
     *     
     */
    public void setApplicableRates(List<RateTO> applicableRates) {
        this.applicableRates = applicableRates;
    }

}
