
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BodyPart_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BodyPart_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}BodyElement_TO">
 *       &lt;sequence>
 *         &lt;element name="positioningDescription" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BodyPart_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "positioningDescription"
})
public class BodyPartTO
    extends BodyElementTO
{

    protected String positioningDescription;

    /**
     * Gets the value of the positioningDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPositioningDescription() {
        return positioningDescription;
    }

    /**
     * Sets the value of the positioningDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPositioningDescription(String value) {
        this.positioningDescription = value;
    }

}
