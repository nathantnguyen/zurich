
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PhysicalDocument_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PhysicalDocument_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}Document_TO">
 *       &lt;sequence>
 *         &lt;element name="onFile" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="electronicalVersion" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}ElectronicDocument_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhysicalDocument_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "onFile",
    "electronicalVersion"
})
public class PhysicalDocumentTO
    extends DocumentTO
{

    protected Boolean onFile;
    protected List<ElectronicDocumentTO> electronicalVersion;

    /**
     * Gets the value of the onFile property.
     * This getter has been renamed from isOnFile() to getOnFile() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOnFile() {
        return onFile;
    }

    /**
     * Sets the value of the onFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOnFile(Boolean value) {
        this.onFile = value;
    }

    /**
     * Gets the value of the electronicalVersion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the electronicalVersion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElectronicalVersion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ElectronicDocumentTO }
     * 
     * 
     */
    public List<ElectronicDocumentTO> getElectronicalVersion() {
        if (electronicalVersion == null) {
            electronicalVersion = new ArrayList<ElectronicDocumentTO>();
        }
        return this.electronicalVersion;
    }

    /**
     * Sets the value of the electronicalVersion property.
     * 
     * @param electronicalVersion
     *     allowed object is
     *     {@link ElectronicDocumentTO }
     *     
     */
    public void setElectronicalVersion(List<ElectronicDocumentTO> electronicalVersion) {
        this.electronicalVersion = electronicalVersion;
    }

}
