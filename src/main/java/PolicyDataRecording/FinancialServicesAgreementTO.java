
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FinancialServicesAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO">
 *       &lt;sequence>
 *         &lt;element name="boundDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="intermediaryAgreements" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/}IntermediaryAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="taxStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Tax/TaxEnumerationsAndStates/}TaxStatus" minOccurs="0"/>
 *         &lt;element name="newBusinessRequest" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}NewBusinessRequest_TO" minOccurs="0"/>
 *         &lt;element name="generalActivities" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeneralActivity_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="coveredActivities" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="financialServicesAgreementComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreementComponent_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="productGroups" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ProductGroup_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="createdByFinancialServicesRequest" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRequest_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "boundDate",
    "intermediaryAgreements",
    "taxStatus",
    "newBusinessRequest",
    "generalActivities",
    "coveredActivities",
    "financialServicesAgreementComponents",
    "productGroups",
    "createdByFinancialServicesRequest"
})
@XmlSeeAlso({
    TopLevelFinancialServicesAgreementTO.class,
    FinancialServicesAgreementComponentTO.class
})
public class FinancialServicesAgreementTO
    extends ContractTO
{

    protected XMLGregorianCalendar boundDate;
    protected List<IntermediaryAgreementTO> intermediaryAgreements;
    protected TaxStatus taxStatus;
    protected NewBusinessRequestTO newBusinessRequest;
    protected List<GeneralActivityTO> generalActivities;
    protected List<ActivityOccurrenceTO> coveredActivities;
    protected List<FinancialServicesAgreementComponentTO> financialServicesAgreementComponents;
    protected List<ProductGroupTO> productGroups;
    protected FinancialServicesRequestTO createdByFinancialServicesRequest;

    /**
     * Gets the value of the boundDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBoundDate() {
        return boundDate;
    }

    /**
     * Sets the value of the boundDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBoundDate(XMLGregorianCalendar value) {
        this.boundDate = value;
    }

    /**
     * Gets the value of the intermediaryAgreements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intermediaryAgreements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntermediaryAgreements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntermediaryAgreementTO }
     * 
     * 
     */
    public List<IntermediaryAgreementTO> getIntermediaryAgreements() {
        if (intermediaryAgreements == null) {
            intermediaryAgreements = new ArrayList<IntermediaryAgreementTO>();
        }
        return this.intermediaryAgreements;
    }

    /**
     * Gets the value of the taxStatus property.
     * 
     * @return
     *     possible object is
     *     {@link TaxStatus }
     *     
     */
    public TaxStatus getTaxStatus() {
        return taxStatus;
    }

    /**
     * Sets the value of the taxStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxStatus }
     *     
     */
    public void setTaxStatus(TaxStatus value) {
        this.taxStatus = value;
    }

    /**
     * Gets the value of the newBusinessRequest property.
     * 
     * @return
     *     possible object is
     *     {@link NewBusinessRequestTO }
     *     
     */
    public NewBusinessRequestTO getNewBusinessRequest() {
        return newBusinessRequest;
    }

    /**
     * Sets the value of the newBusinessRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link NewBusinessRequestTO }
     *     
     */
    public void setNewBusinessRequest(NewBusinessRequestTO value) {
        this.newBusinessRequest = value;
    }

    /**
     * Gets the value of the generalActivities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the generalActivities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeneralActivities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeneralActivityTO }
     * 
     * 
     */
    public List<GeneralActivityTO> getGeneralActivities() {
        if (generalActivities == null) {
            generalActivities = new ArrayList<GeneralActivityTO>();
        }
        return this.generalActivities;
    }

    /**
     * Gets the value of the coveredActivities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coveredActivities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoveredActivities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityOccurrenceTO }
     * 
     * 
     */
    public List<ActivityOccurrenceTO> getCoveredActivities() {
        if (coveredActivities == null) {
            coveredActivities = new ArrayList<ActivityOccurrenceTO>();
        }
        return this.coveredActivities;
    }

    /**
     * Gets the value of the financialServicesAgreementComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the financialServicesAgreementComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFinancialServicesAgreementComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialServicesAgreementComponentTO }
     * 
     * 
     */
    public List<FinancialServicesAgreementComponentTO> getFinancialServicesAgreementComponents() {
        if (financialServicesAgreementComponents == null) {
            financialServicesAgreementComponents = new ArrayList<FinancialServicesAgreementComponentTO>();
        }
        return this.financialServicesAgreementComponents;
    }

    /**
     * Gets the value of the productGroups property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productGroups property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductGroups().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductGroupTO }
     * 
     * 
     */
    public List<ProductGroupTO> getProductGroups() {
        if (productGroups == null) {
            productGroups = new ArrayList<ProductGroupTO>();
        }
        return this.productGroups;
    }

    /**
     * Gets the value of the createdByFinancialServicesRequest property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesRequestTO }
     *     
     */
    public FinancialServicesRequestTO getCreatedByFinancialServicesRequest() {
        return createdByFinancialServicesRequest;
    }

    /**
     * Sets the value of the createdByFinancialServicesRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesRequestTO }
     *     
     */
    public void setCreatedByFinancialServicesRequest(FinancialServicesRequestTO value) {
        this.createdByFinancialServicesRequest = value;
    }

    /**
     * Sets the value of the intermediaryAgreements property.
     * 
     * @param intermediaryAgreements
     *     allowed object is
     *     {@link IntermediaryAgreementTO }
     *     
     */
    public void setIntermediaryAgreements(List<IntermediaryAgreementTO> intermediaryAgreements) {
        this.intermediaryAgreements = intermediaryAgreements;
    }

    /**
     * Sets the value of the generalActivities property.
     * 
     * @param generalActivities
     *     allowed object is
     *     {@link GeneralActivityTO }
     *     
     */
    public void setGeneralActivities(List<GeneralActivityTO> generalActivities) {
        this.generalActivities = generalActivities;
    }

    /**
     * Sets the value of the coveredActivities property.
     * 
     * @param coveredActivities
     *     allowed object is
     *     {@link ActivityOccurrenceTO }
     *     
     */
    public void setCoveredActivities(List<ActivityOccurrenceTO> coveredActivities) {
        this.coveredActivities = coveredActivities;
    }

    /**
     * Sets the value of the financialServicesAgreementComponents property.
     * 
     * @param financialServicesAgreementComponents
     *     allowed object is
     *     {@link FinancialServicesAgreementComponentTO }
     *     
     */
    public void setFinancialServicesAgreementComponents(List<FinancialServicesAgreementComponentTO> financialServicesAgreementComponents) {
        this.financialServicesAgreementComponents = financialServicesAgreementComponents;
    }

    /**
     * Sets the value of the productGroups property.
     * 
     * @param productGroups
     *     allowed object is
     *     {@link ProductGroupTO }
     *     
     */
    public void setProductGroups(List<ProductGroupTO> productGroups) {
        this.productGroups = productGroups;
    }

}
