
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HeatingUnit_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HeatingUnit_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}StructureBuiltin_TO">
 *       &lt;sequence>
 *         &lt;element name="heatingUnitProtectiveMaterial" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}HeatingUnitProtectiveMaterial" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeatingUnit_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "heatingUnitProtectiveMaterial"
})
public class HeatingUnitTO
    extends StructureBuiltinTO
{

    protected HeatingUnitProtectiveMaterial heatingUnitProtectiveMaterial;

    /**
     * Gets the value of the heatingUnitProtectiveMaterial property.
     * 
     * @return
     *     possible object is
     *     {@link HeatingUnitProtectiveMaterial }
     *     
     */
    public HeatingUnitProtectiveMaterial getHeatingUnitProtectiveMaterial() {
        return heatingUnitProtectiveMaterial;
    }

    /**
     * Sets the value of the heatingUnitProtectiveMaterial property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeatingUnitProtectiveMaterial }
     *     
     */
    public void setHeatingUnitProtectiveMaterial(HeatingUnitProtectiveMaterial value) {
        this.heatingUnitProtectiveMaterial = value;
    }

}
