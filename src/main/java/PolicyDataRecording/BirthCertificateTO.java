
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BirthCertificate_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BirthCertificate_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="registeredBirthDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="registeredBirthPlace" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BirthCertificate_TO", propOrder = {
    "registeredBirthDate",
    "registeredBirthPlace"
})
public class BirthCertificateTO
    extends PartyRegistrationTO
{

    protected XMLGregorianCalendar registeredBirthDate;
    protected PlaceTO registeredBirthPlace;

    /**
     * Gets the value of the registeredBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegisteredBirthDate() {
        return registeredBirthDate;
    }

    /**
     * Sets the value of the registeredBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegisteredBirthDate(XMLGregorianCalendar value) {
        this.registeredBirthDate = value;
    }

    /**
     * Gets the value of the registeredBirthPlace property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getRegisteredBirthPlace() {
        return registeredBirthPlace;
    }

    /**
     * Sets the value of the registeredBirthPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setRegisteredBirthPlace(PlaceTO value) {
        this.registeredBirthPlace = value;
    }

}
