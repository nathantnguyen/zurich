
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GoalAndNeed_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GoalAndNeed_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="firstDueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="beforeTax" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="amountDeterminationMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}AmountDeterminationMethod" minOccurs="0"/>
 *         &lt;element name="startDateDeterminationMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}DateDeterminationMethod" minOccurs="0"/>
 *         &lt;element name="endDateDeterminationMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}DateDeterminationMethod" minOccurs="0"/>
 *         &lt;element name="illustratingScenarios" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Scenario_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="selfInsured" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="selfInsuredAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="selfInsuredPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="rolesInGoalAndNeed" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInGoalAndNeed_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="marketSegments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}MarketSegment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GoalAndNeed_TO", propOrder = {
    "amount",
    "description",
    "endDate",
    "firstDueDate",
    "frequency",
    "beforeTax",
    "amountDeterminationMethod",
    "startDateDeterminationMethod",
    "endDateDeterminationMethod",
    "illustratingScenarios",
    "selfInsured",
    "selfInsuredAmount",
    "selfInsuredPercentage",
    "startDate",
    "rolesInGoalAndNeed",
    "rolePlayer",
    "marketSegments"
})
public class GoalAndNeedTO
    extends DependentObjectTO
{

    protected BaseCurrencyAmount amount;
    protected String description;
    protected XMLGregorianCalendar endDate;
    protected XMLGregorianCalendar firstDueDate;
    protected Frequency frequency;
    protected Boolean beforeTax;
    protected AmountDeterminationMethod amountDeterminationMethod;
    protected DateDeterminationMethod startDateDeterminationMethod;
    protected DateDeterminationMethod endDateDeterminationMethod;
    protected List<ScenarioTO> illustratingScenarios;
    protected Boolean selfInsured;
    protected BaseCurrencyAmount selfInsuredAmount;
    protected BigDecimal selfInsuredPercentage;
    protected XMLGregorianCalendar startDate;
    protected List<RoleInGoalAndNeedTO> rolesInGoalAndNeed;
    protected RolePlayerTO rolePlayer;
    protected List<MarketSegmentTO> marketSegments;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAmount(BaseCurrencyAmount value) {
        this.amount = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the firstDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstDueDate() {
        return firstDueDate;
    }

    /**
     * Sets the value of the firstDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstDueDate(XMLGregorianCalendar value) {
        this.firstDueDate = value;
    }

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the beforeTax property.
     * This getter has been renamed from isBeforeTax() to getBeforeTax() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getBeforeTax() {
        return beforeTax;
    }

    /**
     * Sets the value of the beforeTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBeforeTax(Boolean value) {
        this.beforeTax = value;
    }

    /**
     * Gets the value of the amountDeterminationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link AmountDeterminationMethod }
     *     
     */
    public AmountDeterminationMethod getAmountDeterminationMethod() {
        return amountDeterminationMethod;
    }

    /**
     * Sets the value of the amountDeterminationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountDeterminationMethod }
     *     
     */
    public void setAmountDeterminationMethod(AmountDeterminationMethod value) {
        this.amountDeterminationMethod = value;
    }

    /**
     * Gets the value of the startDateDeterminationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link DateDeterminationMethod }
     *     
     */
    public DateDeterminationMethod getStartDateDeterminationMethod() {
        return startDateDeterminationMethod;
    }

    /**
     * Sets the value of the startDateDeterminationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateDeterminationMethod }
     *     
     */
    public void setStartDateDeterminationMethod(DateDeterminationMethod value) {
        this.startDateDeterminationMethod = value;
    }

    /**
     * Gets the value of the endDateDeterminationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link DateDeterminationMethod }
     *     
     */
    public DateDeterminationMethod getEndDateDeterminationMethod() {
        return endDateDeterminationMethod;
    }

    /**
     * Sets the value of the endDateDeterminationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateDeterminationMethod }
     *     
     */
    public void setEndDateDeterminationMethod(DateDeterminationMethod value) {
        this.endDateDeterminationMethod = value;
    }

    /**
     * Gets the value of the illustratingScenarios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the illustratingScenarios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIllustratingScenarios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScenarioTO }
     * 
     * 
     */
    public List<ScenarioTO> getIllustratingScenarios() {
        if (illustratingScenarios == null) {
            illustratingScenarios = new ArrayList<ScenarioTO>();
        }
        return this.illustratingScenarios;
    }

    /**
     * Gets the value of the selfInsured property.
     * This getter has been renamed from isSelfInsured() to getSelfInsured() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSelfInsured() {
        return selfInsured;
    }

    /**
     * Sets the value of the selfInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelfInsured(Boolean value) {
        this.selfInsured = value;
    }

    /**
     * Gets the value of the selfInsuredAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getSelfInsuredAmount() {
        return selfInsuredAmount;
    }

    /**
     * Sets the value of the selfInsuredAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setSelfInsuredAmount(BaseCurrencyAmount value) {
        this.selfInsuredAmount = value;
    }

    /**
     * Gets the value of the selfInsuredPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSelfInsuredPercentage() {
        return selfInsuredPercentage;
    }

    /**
     * Sets the value of the selfInsuredPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSelfInsuredPercentage(BigDecimal value) {
        this.selfInsuredPercentage = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the rolesInGoalAndNeed property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInGoalAndNeed property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInGoalAndNeed().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInGoalAndNeedTO }
     * 
     * 
     */
    public List<RoleInGoalAndNeedTO> getRolesInGoalAndNeed() {
        if (rolesInGoalAndNeed == null) {
            rolesInGoalAndNeed = new ArrayList<RoleInGoalAndNeedTO>();
        }
        return this.rolesInGoalAndNeed;
    }

    /**
     * Gets the value of the rolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getRolePlayer() {
        return rolePlayer;
    }

    /**
     * Sets the value of the rolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setRolePlayer(RolePlayerTO value) {
        this.rolePlayer = value;
    }

    /**
     * Gets the value of the marketSegments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the marketSegments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMarketSegments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MarketSegmentTO }
     * 
     * 
     */
    public List<MarketSegmentTO> getMarketSegments() {
        if (marketSegments == null) {
            marketSegments = new ArrayList<MarketSegmentTO>();
        }
        return this.marketSegments;
    }

    /**
     * Sets the value of the illustratingScenarios property.
     * 
     * @param illustratingScenarios
     *     allowed object is
     *     {@link ScenarioTO }
     *     
     */
    public void setIllustratingScenarios(List<ScenarioTO> illustratingScenarios) {
        this.illustratingScenarios = illustratingScenarios;
    }

    /**
     * Sets the value of the rolesInGoalAndNeed property.
     * 
     * @param rolesInGoalAndNeed
     *     allowed object is
     *     {@link RoleInGoalAndNeedTO }
     *     
     */
    public void setRolesInGoalAndNeed(List<RoleInGoalAndNeedTO> rolesInGoalAndNeed) {
        this.rolesInGoalAndNeed = rolesInGoalAndNeed;
    }

    /**
     * Sets the value of the marketSegments property.
     * 
     * @param marketSegments
     *     allowed object is
     *     {@link MarketSegmentTO }
     *     
     */
    public void setMarketSegments(List<MarketSegmentTO> marketSegments) {
        this.marketSegments = marketSegments;
    }

}
