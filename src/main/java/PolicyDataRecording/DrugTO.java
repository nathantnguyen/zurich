
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Drug_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Drug_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}ManufacturedItem_TO">
 *       &lt;sequence>
 *         &lt;element name="legend" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="drugSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}DrugSpecification_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Drug_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "legend",
    "drugSpecification"
})
public class DrugTO
    extends ManufacturedItemTO
{

    protected Boolean legend;
    protected DrugSpecificationTO drugSpecification;

    /**
     * Gets the value of the legend property.
     * This getter has been renamed from isLegend() to getLegend() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLegend() {
        return legend;
    }

    /**
     * Sets the value of the legend property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLegend(Boolean value) {
        this.legend = value;
    }

    /**
     * Gets the value of the drugSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link DrugSpecificationTO }
     *     
     */
    public DrugSpecificationTO getDrugSpecification() {
        return drugSpecification;
    }

    /**
     * Sets the value of the drugSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link DrugSpecificationTO }
     *     
     */
    public void setDrugSpecification(DrugSpecificationTO value) {
        this.drugSpecification = value;
    }

}
