
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NetWorth_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NetWorth_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}FinancialValuation_TO">
 *       &lt;sequence>
 *         &lt;element name="liquidity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}Liquidity" minOccurs="0"/>
 *         &lt;element name="assessedRoleplayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NetWorth_TO", propOrder = {
    "liquidity",
    "assessedRoleplayer"
})
public class NetWorthTO
    extends FinancialValuationTO
{

    protected Liquidity liquidity;
    protected RolePlayerTO assessedRoleplayer;

    /**
     * Gets the value of the liquidity property.
     * 
     * @return
     *     possible object is
     *     {@link Liquidity }
     *     
     */
    public Liquidity getLiquidity() {
        return liquidity;
    }

    /**
     * Sets the value of the liquidity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Liquidity }
     *     
     */
    public void setLiquidity(Liquidity value) {
        this.liquidity = value;
    }

    /**
     * Gets the value of the assessedRoleplayer property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getAssessedRoleplayer() {
        return assessedRoleplayer;
    }

    /**
     * Sets the value of the assessedRoleplayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setAssessedRoleplayer(RolePlayerTO value) {
        this.assessedRoleplayer = value;
    }

}
