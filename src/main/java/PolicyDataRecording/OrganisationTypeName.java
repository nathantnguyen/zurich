
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrganisationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganisationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Trust"/>
 *     &lt;enumeration value="Profession Group"/>
 *     &lt;enumeration value="Department"/>
 *     &lt;enumeration value="Branch"/>
 *     &lt;enumeration value="Regional Unit"/>
 *     &lt;enumeration value="Government Body"/>
 *     &lt;enumeration value="Team"/>
 *     &lt;enumeration value="Bank"/>
 *     &lt;enumeration value="Banking Market"/>
 *     &lt;enumeration value="Captive Insurance Undertaking"/>
 *     &lt;enumeration value="Captive Producer Market"/>
 *     &lt;enumeration value="Captive Reinsurance Undertaking"/>
 *     &lt;enumeration value="Financial Services Market"/>
 *     &lt;enumeration value="Financial Undertaking"/>
 *     &lt;enumeration value="Insurance Brokerage Market"/>
 *     &lt;enumeration value="Insurance Intermediation Market"/>
 *     &lt;enumeration value="Insurance Market"/>
 *     &lt;enumeration value="Insurance Regulator"/>
 *     &lt;enumeration value="Insurance Solvency Supervisor"/>
 *     &lt;enumeration value="Insurance Special Purpose Vehicle"/>
 *     &lt;enumeration value="Insurance Undertaking"/>
 *     &lt;enumeration value="Investment Special Purpose Vehicle"/>
 *     &lt;enumeration value="National Insurance Guaranty Fund"/>
 *     &lt;enumeration value="National Insurers Bureau"/>
 *     &lt;enumeration value="Reinsurance Undertaking"/>
 *     &lt;enumeration value="Retail Bank Market"/>
 *     &lt;enumeration value="Special Purpose Entity"/>
 *     &lt;enumeration value="Tax Office"/>
 *     &lt;enumeration value="Fraud Network"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrganisationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum OrganisationTypeName {

    @XmlEnumValue("Trust")
    TRUST("Trust"),
    @XmlEnumValue("Profession Group")
    PROFESSION_GROUP("Profession Group"),
    @XmlEnumValue("Department")
    DEPARTMENT("Department"),
    @XmlEnumValue("Branch")
    BRANCH("Branch"),
    @XmlEnumValue("Regional Unit")
    REGIONAL_UNIT("Regional Unit"),
    @XmlEnumValue("Government Body")
    GOVERNMENT_BODY("Government Body"),
    @XmlEnumValue("Team")
    TEAM("Team"),
    @XmlEnumValue("Bank")
    BANK("Bank"),
    @XmlEnumValue("Banking Market")
    BANKING_MARKET("Banking Market"),
    @XmlEnumValue("Captive Insurance Undertaking")
    CAPTIVE_INSURANCE_UNDERTAKING("Captive Insurance Undertaking"),
    @XmlEnumValue("Captive Producer Market")
    CAPTIVE_PRODUCER_MARKET("Captive Producer Market"),
    @XmlEnumValue("Captive Reinsurance Undertaking")
    CAPTIVE_REINSURANCE_UNDERTAKING("Captive Reinsurance Undertaking"),
    @XmlEnumValue("Financial Services Market")
    FINANCIAL_SERVICES_MARKET("Financial Services Market"),
    @XmlEnumValue("Financial Undertaking")
    FINANCIAL_UNDERTAKING("Financial Undertaking"),
    @XmlEnumValue("Insurance Brokerage Market")
    INSURANCE_BROKERAGE_MARKET("Insurance Brokerage Market"),
    @XmlEnumValue("Insurance Intermediation Market")
    INSURANCE_INTERMEDIATION_MARKET("Insurance Intermediation Market"),
    @XmlEnumValue("Insurance Market")
    INSURANCE_MARKET("Insurance Market"),
    @XmlEnumValue("Insurance Regulator")
    INSURANCE_REGULATOR("Insurance Regulator"),
    @XmlEnumValue("Insurance Solvency Supervisor")
    INSURANCE_SOLVENCY_SUPERVISOR("Insurance Solvency Supervisor"),
    @XmlEnumValue("Insurance Special Purpose Vehicle")
    INSURANCE_SPECIAL_PURPOSE_VEHICLE("Insurance Special Purpose Vehicle"),
    @XmlEnumValue("Insurance Undertaking")
    INSURANCE_UNDERTAKING("Insurance Undertaking"),
    @XmlEnumValue("Investment Special Purpose Vehicle")
    INVESTMENT_SPECIAL_PURPOSE_VEHICLE("Investment Special Purpose Vehicle"),
    @XmlEnumValue("National Insurance Guaranty Fund")
    NATIONAL_INSURANCE_GUARANTY_FUND("National Insurance Guaranty Fund"),
    @XmlEnumValue("National Insurers Bureau")
    NATIONAL_INSURERS_BUREAU("National Insurers Bureau"),
    @XmlEnumValue("Reinsurance Undertaking")
    REINSURANCE_UNDERTAKING("Reinsurance Undertaking"),
    @XmlEnumValue("Retail Bank Market")
    RETAIL_BANK_MARKET("Retail Bank Market"),
    @XmlEnumValue("Special Purpose Entity")
    SPECIAL_PURPOSE_ENTITY("Special Purpose Entity"),
    @XmlEnumValue("Tax Office")
    TAX_OFFICE("Tax Office"),
    @XmlEnumValue("Fraud Network")
    FRAUD_NETWORK("Fraud Network");
    private final String value;

    OrganisationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrganisationTypeName fromValue(String v) {
        for (OrganisationTypeName c: OrganisationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
