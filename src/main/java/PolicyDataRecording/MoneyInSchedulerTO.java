
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for MoneyInScheduler_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyInScheduler_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyScheduler_TO">
 *       &lt;sequence>
 *         &lt;element name="gracePeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="receivedPayments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Payment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="premiumHoliday" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="offsetByMoneyOutSchedulers" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyOutScheduler_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyInScheduler_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "gracePeriod",
    "receivedPayments",
    "premiumHoliday",
    "offsetByMoneyOutSchedulers"
})
public class MoneyInSchedulerTO
    extends MoneySchedulerTO
{

    protected Duration gracePeriod;
    protected List<PaymentTO> receivedPayments;
    protected Boolean premiumHoliday;
    protected List<MoneyOutSchedulerTO> offsetByMoneyOutSchedulers;

    /**
     * Gets the value of the gracePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getGracePeriod() {
        return gracePeriod;
    }

    /**
     * Sets the value of the gracePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setGracePeriod(Duration value) {
        this.gracePeriod = value;
    }

    /**
     * Gets the value of the receivedPayments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receivedPayments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceivedPayments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentTO }
     * 
     * 
     */
    public List<PaymentTO> getReceivedPayments() {
        if (receivedPayments == null) {
            receivedPayments = new ArrayList<PaymentTO>();
        }
        return this.receivedPayments;
    }

    /**
     * Gets the value of the premiumHoliday property.
     * This getter has been renamed from isPremiumHoliday() to getPremiumHoliday() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPremiumHoliday() {
        return premiumHoliday;
    }

    /**
     * Sets the value of the premiumHoliday property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPremiumHoliday(Boolean value) {
        this.premiumHoliday = value;
    }

    /**
     * Gets the value of the offsetByMoneyOutSchedulers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offsetByMoneyOutSchedulers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOffsetByMoneyOutSchedulers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyOutSchedulerTO }
     * 
     * 
     */
    public List<MoneyOutSchedulerTO> getOffsetByMoneyOutSchedulers() {
        if (offsetByMoneyOutSchedulers == null) {
            offsetByMoneyOutSchedulers = new ArrayList<MoneyOutSchedulerTO>();
        }
        return this.offsetByMoneyOutSchedulers;
    }

    /**
     * Sets the value of the receivedPayments property.
     * 
     * @param receivedPayments
     *     allowed object is
     *     {@link PaymentTO }
     *     
     */
    public void setReceivedPayments(List<PaymentTO> receivedPayments) {
        this.receivedPayments = receivedPayments;
    }

    /**
     * Sets the value of the offsetByMoneyOutSchedulers property.
     * 
     * @param offsetByMoneyOutSchedulers
     *     allowed object is
     *     {@link MoneyOutSchedulerTO }
     *     
     */
    public void setOffsetByMoneyOutSchedulers(List<MoneyOutSchedulerTO> offsetByMoneyOutSchedulers) {
        this.offsetByMoneyOutSchedulers = offsetByMoneyOutSchedulers;
    }

}
