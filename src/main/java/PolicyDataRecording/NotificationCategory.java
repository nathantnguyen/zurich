
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NotificationCategory.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NotificationCategory">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Change To Notification"/>
 *     &lt;enumeration value="Initial Notification"/>
 *     &lt;enumeration value="Error"/>
 *     &lt;enumeration value="Info"/>
 *     &lt;enumeration value="Warning"/>
 *     &lt;enumeration value="Abort"/>
 *     &lt;enumeration value="Success"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NotificationCategory", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/")
@XmlEnum
public enum NotificationCategory {

    @XmlEnumValue("Change To Notification")
    CHANGE_TO_NOTIFICATION("Change To Notification"),
    @XmlEnumValue("Initial Notification")
    INITIAL_NOTIFICATION("Initial Notification"),
    @XmlEnumValue("Error")
    ERROR("Error"),
    @XmlEnumValue("Info")
    INFO("Info"),
    @XmlEnumValue("Warning")
    WARNING("Warning"),
    @XmlEnumValue("Abort")
    ABORT("Abort"),
    @XmlEnumValue("Success")
    SUCCESS("Success");
    private final String value;

    NotificationCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NotificationCategory fromValue(String v) {
        for (NotificationCategory c: NotificationCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
