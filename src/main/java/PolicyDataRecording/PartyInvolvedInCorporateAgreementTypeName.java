
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyInvolvedInCorporateAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PartyInvolvedInCorporateAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Corporate Agreement Administrator"/>
 *     &lt;enumeration value="Corporate Policy Owner"/>
 *     &lt;enumeration value="Manager"/>
 *     &lt;enumeration value="Regulatory Risk Supervisor"/>
 *     &lt;enumeration value="Supervisor"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PartyInvolvedInCorporateAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum PartyInvolvedInCorporateAgreementTypeName {

    @XmlEnumValue("Corporate Agreement Administrator")
    CORPORATE_AGREEMENT_ADMINISTRATOR("Corporate Agreement Administrator"),
    @XmlEnumValue("Corporate Policy Owner")
    CORPORATE_POLICY_OWNER("Corporate Policy Owner"),
    @XmlEnumValue("Manager")
    MANAGER("Manager"),
    @XmlEnumValue("Regulatory Risk Supervisor")
    REGULATORY_RISK_SUPERVISOR("Regulatory Risk Supervisor"),
    @XmlEnumValue("Supervisor")
    SUPERVISOR("Supervisor");
    private final String value;

    PartyInvolvedInCorporateAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartyInvolvedInCorporateAgreementTypeName fromValue(String v) {
        for (PartyInvolvedInCorporateAgreementTypeName c: PartyInvolvedInCorporateAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
