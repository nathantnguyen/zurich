
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ProfitSharingComponent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfitSharingComponent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreementComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="destination" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}ProfitDestination" minOccurs="0"/>
 *         &lt;element name="endOfYearDividendAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="nextDividendAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="nextDividendDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="participatingLifeInsuranceDividendOption" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfitSharingComponent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "destination",
    "endOfYearDividendAmount",
    "nextDividendAmount",
    "nextDividendDate",
    "participatingLifeInsuranceDividendOption"
})
public class ProfitSharingComponentTO
    extends FinancialServicesAgreementComponentTO
{

    protected ProfitDestination destination;
    protected BaseCurrencyAmount endOfYearDividendAmount;
    protected BaseCurrencyAmount nextDividendAmount;
    protected XMLGregorianCalendar nextDividendDate;
    protected String participatingLifeInsuranceDividendOption;

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link ProfitDestination }
     *     
     */
    public ProfitDestination getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfitDestination }
     *     
     */
    public void setDestination(ProfitDestination value) {
        this.destination = value;
    }

    /**
     * Gets the value of the endOfYearDividendAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getEndOfYearDividendAmount() {
        return endOfYearDividendAmount;
    }

    /**
     * Sets the value of the endOfYearDividendAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setEndOfYearDividendAmount(BaseCurrencyAmount value) {
        this.endOfYearDividendAmount = value;
    }

    /**
     * Gets the value of the nextDividendAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getNextDividendAmount() {
        return nextDividendAmount;
    }

    /**
     * Sets the value of the nextDividendAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setNextDividendAmount(BaseCurrencyAmount value) {
        this.nextDividendAmount = value;
    }

    /**
     * Gets the value of the nextDividendDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextDividendDate() {
        return nextDividendDate;
    }

    /**
     * Sets the value of the nextDividendDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextDividendDate(XMLGregorianCalendar value) {
        this.nextDividendDate = value;
    }

    /**
     * Gets the value of the participatingLifeInsuranceDividendOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingLifeInsuranceDividendOption() {
        return participatingLifeInsuranceDividendOption;
    }

    /**
     * Sets the value of the participatingLifeInsuranceDividendOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingLifeInsuranceDividendOption(String value) {
        this.participatingLifeInsuranceDividendOption = value;
    }

}
