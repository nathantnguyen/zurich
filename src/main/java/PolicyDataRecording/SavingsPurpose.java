
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SavingsPurpose.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SavingsPurpose">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Capital Increase"/>
 *     &lt;enumeration value="Financial Security"/>
 *     &lt;enumeration value="Generate Revenue"/>
 *     &lt;enumeration value="Liquidity"/>
 *     &lt;enumeration value="Tax Incentive"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SavingsPurpose", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum SavingsPurpose {

    @XmlEnumValue("Capital Increase")
    CAPITAL_INCREASE("Capital Increase"),
    @XmlEnumValue("Financial Security")
    FINANCIAL_SECURITY("Financial Security"),
    @XmlEnumValue("Generate Revenue")
    GENERATE_REVENUE("Generate Revenue"),
    @XmlEnumValue("Liquidity")
    LIQUIDITY("Liquidity"),
    @XmlEnumValue("Tax Incentive")
    TAX_INCENTIVE("Tax Incentive");
    private final String value;

    SavingsPurpose(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SavingsPurpose fromValue(String v) {
        for (SavingsPurpose c: SavingsPurpose.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
