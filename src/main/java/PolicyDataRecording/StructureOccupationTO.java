
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StructureOccupation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StructureOccupation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInRolePlayer_TO">
 *       &lt;sequence>
 *         &lt;element name="structure" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}Structure_TO" minOccurs="0"/>
 *         &lt;element name="ownedIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureOccupation_TO", propOrder = {
    "structure",
    "ownedIndicator"
})
public class StructureOccupationTO
    extends RoleInRolePlayerTO
{

    protected StructureTO structure;
    protected Boolean ownedIndicator;

    /**
     * Gets the value of the structure property.
     * 
     * @return
     *     possible object is
     *     {@link StructureTO }
     *     
     */
    public StructureTO getStructure() {
        return structure;
    }

    /**
     * Sets the value of the structure property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureTO }
     *     
     */
    public void setStructure(StructureTO value) {
        this.structure = value;
    }

    /**
     * Gets the value of the ownedIndicator property.
     * This getter has been renamed from isOwnedIndicator() to getOwnedIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOwnedIndicator() {
        return ownedIndicator;
    }

    /**
     * Sets the value of the ownedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOwnedIndicator(Boolean value) {
        this.ownedIndicator = value;
    }

}
