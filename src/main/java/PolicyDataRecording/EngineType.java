
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EngineType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EngineType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Solar Energy"/>
 *     &lt;enumeration value="Electric"/>
 *     &lt;enumeration value="Steam"/>
 *     &lt;enumeration value="Gasoline"/>
 *     &lt;enumeration value="Diesel"/>
 *     &lt;enumeration value="Lpg"/>
 *     &lt;enumeration value="Wind"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EngineType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum EngineType {

    @XmlEnumValue("Solar Energy")
    SOLAR_ENERGY("Solar Energy"),
    @XmlEnumValue("Electric")
    ELECTRIC("Electric"),
    @XmlEnumValue("Steam")
    STEAM("Steam"),
    @XmlEnumValue("Gasoline")
    GASOLINE("Gasoline"),
    @XmlEnumValue("Diesel")
    DIESEL("Diesel"),
    @XmlEnumValue("Lpg")
    LPG("Lpg"),
    @XmlEnumValue("Wind")
    WIND("Wind");
    private final String value;

    EngineType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EngineType fromValue(String v) {
        for (EngineType c: EngineType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
