
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CapitalInstrumentTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CapitalInstrumentTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Preference Equity"/>
 *     &lt;enumeration value="Subordinated Liabilities"/>
 *     &lt;enumeration value="Subordinated Mutual Member Accounts"/>
 *     &lt;enumeration value="Common Equity"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CapitalInstrumentTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum CapitalInstrumentTypeName {

    @XmlEnumValue("Preference Equity")
    PREFERENCE_EQUITY("Preference Equity"),
    @XmlEnumValue("Subordinated Liabilities")
    SUBORDINATED_LIABILITIES("Subordinated Liabilities"),
    @XmlEnumValue("Subordinated Mutual Member Accounts")
    SUBORDINATED_MUTUAL_MEMBER_ACCOUNTS("Subordinated Mutual Member Accounts"),
    @XmlEnumValue("Common Equity")
    COMMON_EQUITY("Common Equity");
    private final String value;

    CapitalInstrumentTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CapitalInstrumentTypeName fromValue(String v) {
        for (CapitalInstrumentTypeName c: CapitalInstrumentTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
