
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AutomobileInsurancePolicy_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutomobileInsurancePolicy_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}IndividualAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="goodDriverDiscount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="antiLockBrakeDiscount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="antiTheftDiscount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="otherDiscount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutomobileInsurancePolicy_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "goodDriverDiscount",
    "antiLockBrakeDiscount",
    "antiTheftDiscount",
    "otherDiscount"
})
public class AutomobileInsurancePolicyTO
    extends IndividualAgreementTO
{

    protected Boolean goodDriverDiscount;
    protected Boolean antiLockBrakeDiscount;
    protected Boolean antiTheftDiscount;
    protected Boolean otherDiscount;

    /**
     * Gets the value of the goodDriverDiscount property.
     * This getter has been renamed from isGoodDriverDiscount() to getGoodDriverDiscount() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getGoodDriverDiscount() {
        return goodDriverDiscount;
    }

    /**
     * Sets the value of the goodDriverDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGoodDriverDiscount(Boolean value) {
        this.goodDriverDiscount = value;
    }

    /**
     * Gets the value of the antiLockBrakeDiscount property.
     * This getter has been renamed from isAntiLockBrakeDiscount() to getAntiLockBrakeDiscount() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAntiLockBrakeDiscount() {
        return antiLockBrakeDiscount;
    }

    /**
     * Sets the value of the antiLockBrakeDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAntiLockBrakeDiscount(Boolean value) {
        this.antiLockBrakeDiscount = value;
    }

    /**
     * Gets the value of the antiTheftDiscount property.
     * This getter has been renamed from isAntiTheftDiscount() to getAntiTheftDiscount() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAntiTheftDiscount() {
        return antiTheftDiscount;
    }

    /**
     * Sets the value of the antiTheftDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAntiTheftDiscount(Boolean value) {
        this.antiTheftDiscount = value;
    }

    /**
     * Gets the value of the otherDiscount property.
     * This getter has been renamed from isOtherDiscount() to getOtherDiscount() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOtherDiscount() {
        return otherDiscount;
    }

    /**
     * Sets the value of the otherDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOtherDiscount(Boolean value) {
        this.otherDiscount = value;
    }

}
