
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for EmploymentPositionAssignment_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmploymentPositionAssignment_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayerRelationship_TO">
 *       &lt;sequence>
 *         &lt;element name="assignmentPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="assignmentTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmploymentPositionAssignment_TO", propOrder = {
    "assignmentPercentage",
    "assignmentTime"
})
public class EmploymentPositionAssignmentTO
    extends RolePlayerRelationshipTO
{

    protected BigDecimal assignmentPercentage;
    protected Duration assignmentTime;

    /**
     * Gets the value of the assignmentPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAssignmentPercentage() {
        return assignmentPercentage;
    }

    /**
     * Sets the value of the assignmentPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAssignmentPercentage(BigDecimal value) {
        this.assignmentPercentage = value;
    }

    /**
     * Gets the value of the assignmentTime property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getAssignmentTime() {
        return assignmentTime;
    }

    /**
     * Sets the value of the assignmentTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setAssignmentTime(Duration value) {
        this.assignmentTime = value;
    }

}
