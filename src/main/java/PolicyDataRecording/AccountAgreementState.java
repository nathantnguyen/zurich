
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountAgreementState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountAgreementState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Cancelled"/>
 *     &lt;enumeration value="Draft"/>
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="In Force"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Matured"/>
 *     &lt;enumeration value="Rejected"/>
 *     &lt;enumeration value="Requested"/>
 *     &lt;enumeration value="Suspended"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AccountAgreementState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum AccountAgreementState {

    @XmlEnumValue("Cancelled")
    CANCELLED("Cancelled"),
    @XmlEnumValue("Draft")
    DRAFT("Draft"),
    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("In Force")
    IN_FORCE("In Force"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Matured")
    MATURED("Matured"),
    @XmlEnumValue("Rejected")
    REJECTED("Rejected"),
    @XmlEnumValue("Requested")
    REQUESTED("Requested"),
    @XmlEnumValue("Suspended")
    SUSPENDED("Suspended");
    private final String value;

    AccountAgreementState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountAgreementState fromValue(String v) {
        for (AccountAgreementState c: AccountAgreementState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
