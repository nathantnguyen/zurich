
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Contract_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Contract_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="currencySelection" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="renewalDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="reviewDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="version" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="inceptionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="plannedEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="plannedConversionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="raisedContractRequests" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRequest_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contractRegistration" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="inceptionTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Time" minOccurs="0"/>
 *         &lt;element name="componentOfPackage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="contractRelationships" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRelationship_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rolesInContract" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContract_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contractSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractSpecification_TO" minOccurs="0"/>
 *         &lt;element name="contractRoles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRole_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contractComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="plannedStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="scopedPlaces" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contentRepresentation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}CommunicationContent_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contract_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "currencySelection",
    "externalReference",
    "renewalDate",
    "reviewDate",
    "version",
    "inceptionDate",
    "plannedEndDate",
    "description",
    "endDate",
    "startDate",
    "plannedConversionDate",
    "raisedContractRequests",
    "contractRegistration",
    "inceptionTime",
    "componentOfPackage",
    "contractRelationships",
    "rolesInContract",
    "contractSpecification",
    "contractRoles",
    "contractComponents",
    "plannedStartDate",
    "alternateReference",
    "scopedPlaces",
    "contentRepresentation"
})
@XmlSeeAlso({
    ContractGroupTO.class,
    CorporateAgreementTO.class,
    IntermediaryAgreementTO.class,
    EmploymentAgreementTO.class,
    FinancialServicesAgreementTO.class
})
public class ContractTO
    extends BusinessModelObjectTO
{

    protected CurrencyCode currencySelection;
    protected String externalReference;
    protected XMLGregorianCalendar renewalDate;
    protected XMLGregorianCalendar reviewDate;
    protected String version;
    protected XMLGregorianCalendar inceptionDate;
    protected XMLGregorianCalendar plannedEndDate;
    protected String description;
    protected XMLGregorianCalendar endDate;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar plannedConversionDate;
    protected List<ContractRequestTO> raisedContractRequests;
    protected List<ContractRegistrationTO> contractRegistration;
    protected XMLGregorianCalendar inceptionTime;
    protected Boolean componentOfPackage;
    protected List<ContractRelationshipTO> contractRelationships;
    protected List<RoleInContractTO> rolesInContract;
    protected ContractSpecificationTO contractSpecification;
    protected List<ContractRoleTO> contractRoles;
    protected List<ContractTO> contractComponents;
    protected XMLGregorianCalendar plannedStartDate;
    protected List<AlternateIdTO> alternateReference;
    protected List<PlaceTO> scopedPlaces;
    protected CommunicationContentTO contentRepresentation;

    /**
     * Gets the value of the currencySelection property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCode }
     *     
     */
    public CurrencyCode getCurrencySelection() {
        return currencySelection;
    }

    /**
     * Sets the value of the currencySelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCode }
     *     
     */
    public void setCurrencySelection(CurrencyCode value) {
        this.currencySelection = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the renewalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRenewalDate() {
        return renewalDate;
    }

    /**
     * Sets the value of the renewalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRenewalDate(XMLGregorianCalendar value) {
        this.renewalDate = value;
    }

    /**
     * Gets the value of the reviewDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReviewDate() {
        return reviewDate;
    }

    /**
     * Sets the value of the reviewDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReviewDate(XMLGregorianCalendar value) {
        this.reviewDate = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the inceptionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInceptionDate() {
        return inceptionDate;
    }

    /**
     * Sets the value of the inceptionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInceptionDate(XMLGregorianCalendar value) {
        this.inceptionDate = value;
    }

    /**
     * Gets the value of the plannedEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlannedEndDate() {
        return plannedEndDate;
    }

    /**
     * Sets the value of the plannedEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlannedEndDate(XMLGregorianCalendar value) {
        this.plannedEndDate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the plannedConversionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlannedConversionDate() {
        return plannedConversionDate;
    }

    /**
     * Sets the value of the plannedConversionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlannedConversionDate(XMLGregorianCalendar value) {
        this.plannedConversionDate = value;
    }

    /**
     * Gets the value of the raisedContractRequests property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the raisedContractRequests property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRaisedContractRequests().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRequestTO }
     * 
     * 
     */
    public List<ContractRequestTO> getRaisedContractRequests() {
        if (raisedContractRequests == null) {
            raisedContractRequests = new ArrayList<ContractRequestTO>();
        }
        return this.raisedContractRequests;
    }

    /**
     * Gets the value of the contractRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRegistrationTO }
     * 
     * 
     */
    public List<ContractRegistrationTO> getContractRegistration() {
        if (contractRegistration == null) {
            contractRegistration = new ArrayList<ContractRegistrationTO>();
        }
        return this.contractRegistration;
    }

    /**
     * Gets the value of the inceptionTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInceptionTime() {
        return inceptionTime;
    }

    /**
     * Sets the value of the inceptionTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInceptionTime(XMLGregorianCalendar value) {
        this.inceptionTime = value;
    }

    /**
     * Gets the value of the componentOfPackage property.
     * This getter has been renamed from isComponentOfPackage() to getComponentOfPackage() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getComponentOfPackage() {
        return componentOfPackage;
    }

    /**
     * Sets the value of the componentOfPackage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setComponentOfPackage(Boolean value) {
        this.componentOfPackage = value;
    }

    /**
     * Gets the value of the contractRelationships property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractRelationships property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractRelationships().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRelationshipTO }
     * 
     * 
     */
    public List<ContractRelationshipTO> getContractRelationships() {
        if (contractRelationships == null) {
            contractRelationships = new ArrayList<ContractRelationshipTO>();
        }
        return this.contractRelationships;
    }

    /**
     * Gets the value of the rolesInContract property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInContract property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInContract().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInContractTO }
     * 
     * 
     */
    public List<RoleInContractTO> getRolesInContract() {
        if (rolesInContract == null) {
            rolesInContract = new ArrayList<RoleInContractTO>();
        }
        return this.rolesInContract;
    }

    /**
     * Gets the value of the contractSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link ContractSpecificationTO }
     *     
     */
    public ContractSpecificationTO getContractSpecification() {
        return contractSpecification;
    }

    /**
     * Sets the value of the contractSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractSpecificationTO }
     *     
     */
    public void setContractSpecification(ContractSpecificationTO value) {
        this.contractSpecification = value;
    }

    /**
     * Gets the value of the contractRoles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractRoles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractRoleTO }
     * 
     * 
     */
    public List<ContractRoleTO> getContractRoles() {
        if (contractRoles == null) {
            contractRoles = new ArrayList<ContractRoleTO>();
        }
        return this.contractRoles;
    }

    /**
     * Gets the value of the contractComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractTO }
     * 
     * 
     */
    public List<ContractTO> getContractComponents() {
        if (contractComponents == null) {
            contractComponents = new ArrayList<ContractTO>();
        }
        return this.contractComponents;
    }

    /**
     * Gets the value of the plannedStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlannedStartDate() {
        return plannedStartDate;
    }

    /**
     * Sets the value of the plannedStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlannedStartDate(XMLGregorianCalendar value) {
        this.plannedStartDate = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Gets the value of the scopedPlaces property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scopedPlaces property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScopedPlaces().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlaceTO }
     * 
     * 
     */
    public List<PlaceTO> getScopedPlaces() {
        if (scopedPlaces == null) {
            scopedPlaces = new ArrayList<PlaceTO>();
        }
        return this.scopedPlaces;
    }

    /**
     * Gets the value of the contentRepresentation property.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationContentTO }
     *     
     */
    public CommunicationContentTO getContentRepresentation() {
        return contentRepresentation;
    }

    /**
     * Sets the value of the contentRepresentation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationContentTO }
     *     
     */
    public void setContentRepresentation(CommunicationContentTO value) {
        this.contentRepresentation = value;
    }

    /**
     * Sets the value of the raisedContractRequests property.
     * 
     * @param raisedContractRequests
     *     allowed object is
     *     {@link ContractRequestTO }
     *     
     */
    public void setRaisedContractRequests(List<ContractRequestTO> raisedContractRequests) {
        this.raisedContractRequests = raisedContractRequests;
    }

    /**
     * Sets the value of the contractRegistration property.
     * 
     * @param contractRegistration
     *     allowed object is
     *     {@link ContractRegistrationTO }
     *     
     */
    public void setContractRegistration(List<ContractRegistrationTO> contractRegistration) {
        this.contractRegistration = contractRegistration;
    }

    /**
     * Sets the value of the contractRelationships property.
     * 
     * @param contractRelationships
     *     allowed object is
     *     {@link ContractRelationshipTO }
     *     
     */
    public void setContractRelationships(List<ContractRelationshipTO> contractRelationships) {
        this.contractRelationships = contractRelationships;
    }

    /**
     * Sets the value of the rolesInContract property.
     * 
     * @param rolesInContract
     *     allowed object is
     *     {@link RoleInContractTO }
     *     
     */
    public void setRolesInContract(List<RoleInContractTO> rolesInContract) {
        this.rolesInContract = rolesInContract;
    }

    /**
     * Sets the value of the contractRoles property.
     * 
     * @param contractRoles
     *     allowed object is
     *     {@link ContractRoleTO }
     *     
     */
    public void setContractRoles(List<ContractRoleTO> contractRoles) {
        this.contractRoles = contractRoles;
    }

    /**
     * Sets the value of the contractComponents property.
     * 
     * @param contractComponents
     *     allowed object is
     *     {@link ContractTO }
     *     
     */
    public void setContractComponents(List<ContractTO> contractComponents) {
        this.contractComponents = contractComponents;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

    /**
     * Sets the value of the scopedPlaces property.
     * 
     * @param scopedPlaces
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setScopedPlaces(List<PlaceTO> scopedPlaces) {
        this.scopedPlaces = scopedPlaces;
    }

}
