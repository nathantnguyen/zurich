
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleList_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleList_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}MultiplicityList_TO">
 *       &lt;sequence>
 *         &lt;element name="agreementRoles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}RoleInAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleList_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "agreementRoles"
})
public class RoleListTO
    extends MultiplicityListTO
{

    protected List<RoleInAgreementTO> agreementRoles;

    /**
     * Gets the value of the agreementRoles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agreementRoles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgreementRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInAgreementTO }
     * 
     * 
     */
    public List<RoleInAgreementTO> getAgreementRoles() {
        if (agreementRoles == null) {
            agreementRoles = new ArrayList<RoleInAgreementTO>();
        }
        return this.agreementRoles;
    }

    /**
     * Sets the value of the agreementRoles property.
     * 
     * @param agreementRoles
     *     allowed object is
     *     {@link RoleInAgreementTO }
     *     
     */
    public void setAgreementRoles(List<RoleInAgreementTO> agreementRoles) {
        this.agreementRoles = agreementRoles;
    }

}
