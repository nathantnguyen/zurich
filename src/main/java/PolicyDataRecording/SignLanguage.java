
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SignLanguage.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SignLanguage">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="American Sign Language"/>
 *     &lt;enumeration value="English Sign Language"/>
 *     &lt;enumeration value="French Sign Language"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SignLanguage", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum SignLanguage {

    @XmlEnumValue("American Sign Language")
    AMERICAN_SIGN_LANGUAGE("American Sign Language"),
    @XmlEnumValue("English Sign Language")
    ENGLISH_SIGN_LANGUAGE("English Sign Language"),
    @XmlEnumValue("French Sign Language")
    FRENCH_SIGN_LANGUAGE("French Sign Language");
    private final String value;

    SignLanguage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SignLanguage fromValue(String v) {
        for (SignLanguage c: SignLanguage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
