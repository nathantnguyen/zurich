
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConditionalRecurringTransfer_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConditionalRecurringTransfer_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}TransferFacility_TO">
 *       &lt;sequence>
 *         &lt;element name="triggerAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="minimumLevelToInvest" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="minimumBalanceToKeep" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="warningCommunicationMedium" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}WarningCommunicationMedium" minOccurs="0"/>
 *         &lt;element name="automaticTransfer" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConditionalRecurringTransfer_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "triggerAmount",
    "minimumLevelToInvest",
    "minimumBalanceToKeep",
    "warningCommunicationMedium",
    "automaticTransfer"
})
public class ConditionalRecurringTransferTO
    extends TransferFacilityTO
{

    protected BaseCurrencyAmount triggerAmount;
    protected BaseCurrencyAmount minimumLevelToInvest;
    protected BaseCurrencyAmount minimumBalanceToKeep;
    protected WarningCommunicationMedium warningCommunicationMedium;
    protected Boolean automaticTransfer;

    /**
     * Gets the value of the triggerAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getTriggerAmount() {
        return triggerAmount;
    }

    /**
     * Sets the value of the triggerAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setTriggerAmount(BaseCurrencyAmount value) {
        this.triggerAmount = value;
    }

    /**
     * Gets the value of the minimumLevelToInvest property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMinimumLevelToInvest() {
        return minimumLevelToInvest;
    }

    /**
     * Sets the value of the minimumLevelToInvest property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMinimumLevelToInvest(BaseCurrencyAmount value) {
        this.minimumLevelToInvest = value;
    }

    /**
     * Gets the value of the minimumBalanceToKeep property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMinimumBalanceToKeep() {
        return minimumBalanceToKeep;
    }

    /**
     * Sets the value of the minimumBalanceToKeep property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMinimumBalanceToKeep(BaseCurrencyAmount value) {
        this.minimumBalanceToKeep = value;
    }

    /**
     * Gets the value of the warningCommunicationMedium property.
     * 
     * @return
     *     possible object is
     *     {@link WarningCommunicationMedium }
     *     
     */
    public WarningCommunicationMedium getWarningCommunicationMedium() {
        return warningCommunicationMedium;
    }

    /**
     * Sets the value of the warningCommunicationMedium property.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningCommunicationMedium }
     *     
     */
    public void setWarningCommunicationMedium(WarningCommunicationMedium value) {
        this.warningCommunicationMedium = value;
    }

    /**
     * Gets the value of the automaticTransfer property.
     * This getter has been renamed from isAutomaticTransfer() to getAutomaticTransfer() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAutomaticTransfer() {
        return automaticTransfer;
    }

    /**
     * Sets the value of the automaticTransfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutomaticTransfer(Boolean value) {
        this.automaticTransfer = value;
    }

}
