
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReservedAmountTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReservedAmountTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Benefit"/>
 *     &lt;enumeration value="Price"/>
 *     &lt;enumeration value="Internal Claim Cost"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReservedAmountTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum ReservedAmountTypeName {

    @XmlEnumValue("Benefit")
    BENEFIT("Benefit"),
    @XmlEnumValue("Price")
    PRICE("Price"),
    @XmlEnumValue("Internal Claim Cost")
    INTERNAL_CLAIM_COST("Internal Claim Cost");
    private final String value;

    ReservedAmountTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReservedAmountTypeName fromValue(String v) {
        for (ReservedAmountTypeName c: ReservedAmountTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
