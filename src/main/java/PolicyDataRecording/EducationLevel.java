
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EducationLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EducationLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Degree"/>
 *     &lt;enumeration value="Diploma"/>
 *     &lt;enumeration value="Professional Certification"/>
 *     &lt;enumeration value="Second Level"/>
 *     &lt;enumeration value="University Degree"/>
 *     &lt;enumeration value="Bachelor"/>
 *     &lt;enumeration value="High School"/>
 *     &lt;enumeration value="Master"/>
 *     &lt;enumeration value="Post Graduate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EducationLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum EducationLevel {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Degree")
    DEGREE("Degree"),
    @XmlEnumValue("Diploma")
    DIPLOMA("Diploma"),
    @XmlEnumValue("Professional Certification")
    PROFESSIONAL_CERTIFICATION("Professional Certification"),
    @XmlEnumValue("Second Level")
    SECOND_LEVEL("Second Level"),
    @XmlEnumValue("University Degree")
    UNIVERSITY_DEGREE("University Degree"),
    @XmlEnumValue("Bachelor")
    BACHELOR("Bachelor"),
    @XmlEnumValue("High School")
    HIGH_SCHOOL("High School"),
    @XmlEnumValue("Master")
    MASTER("Master"),
    @XmlEnumValue("Post Graduate")
    POST_GRADUATE("Post Graduate");
    private final String value;

    EducationLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EducationLevel fromValue(String v) {
        for (EducationLevel c: EducationLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
