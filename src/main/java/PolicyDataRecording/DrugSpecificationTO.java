
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DrugSpecification_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DrugSpecification_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}ModelSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="drugBasedOnSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}Drug_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="drugRegistration" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}DrugRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="equivalent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}DrugSpecification_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DrugSpecification_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "drugBasedOnSpecification",
    "drugRegistration",
    "equivalent"
})
public class DrugSpecificationTO
    extends ModelSpecificationTO
{

    protected List<DrugTO> drugBasedOnSpecification;
    protected List<DrugRegistrationTO> drugRegistration;
    protected List<DrugSpecificationTO> equivalent;

    /**
     * Gets the value of the drugBasedOnSpecification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the drugBasedOnSpecification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDrugBasedOnSpecification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DrugTO }
     * 
     * 
     */
    public List<DrugTO> getDrugBasedOnSpecification() {
        if (drugBasedOnSpecification == null) {
            drugBasedOnSpecification = new ArrayList<DrugTO>();
        }
        return this.drugBasedOnSpecification;
    }

    /**
     * Gets the value of the drugRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the drugRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDrugRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DrugRegistrationTO }
     * 
     * 
     */
    public List<DrugRegistrationTO> getDrugRegistration() {
        if (drugRegistration == null) {
            drugRegistration = new ArrayList<DrugRegistrationTO>();
        }
        return this.drugRegistration;
    }

    /**
     * Gets the value of the equivalent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equivalent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquivalent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DrugSpecificationTO }
     * 
     * 
     */
    public List<DrugSpecificationTO> getEquivalent() {
        if (equivalent == null) {
            equivalent = new ArrayList<DrugSpecificationTO>();
        }
        return this.equivalent;
    }

    /**
     * Sets the value of the drugBasedOnSpecification property.
     * 
     * @param drugBasedOnSpecification
     *     allowed object is
     *     {@link DrugTO }
     *     
     */
    public void setDrugBasedOnSpecification(List<DrugTO> drugBasedOnSpecification) {
        this.drugBasedOnSpecification = drugBasedOnSpecification;
    }

    /**
     * Sets the value of the drugRegistration property.
     * 
     * @param drugRegistration
     *     allowed object is
     *     {@link DrugRegistrationTO }
     *     
     */
    public void setDrugRegistration(List<DrugRegistrationTO> drugRegistration) {
        this.drugRegistration = drugRegistration;
    }

    /**
     * Sets the value of the equivalent property.
     * 
     * @param equivalent
     *     allowed object is
     *     {@link DrugSpecificationTO }
     *     
     */
    public void setEquivalent(List<DrugSpecificationTO> equivalent) {
        this.equivalent = equivalent;
    }

}
