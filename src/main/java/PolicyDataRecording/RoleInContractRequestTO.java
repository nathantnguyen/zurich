
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInContractRequest_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInContractRequest_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="roleInContractRequestRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContractRequestType_TO" minOccurs="0"/>
 *         &lt;element name="triggeredActivity" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInContractRequest_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "roleInContractRequestRootType",
    "triggeredActivity"
})
@XmlSeeAlso({
    MoneyProvisionInvolvedInContractRequestTO.class,
    AssessmentResultInvolvedInContractRequestTO.class,
    RoleInCorporateAgreementRequestTO.class,
    RoleInFinancialServicesRequestTO.class,
    PartyInvolvedInContractRequestTO.class
})
public class RoleInContractRequestTO
    extends RoleInContextClassTO
{

    protected RoleInContractRequestTypeTO roleInContractRequestRootType;
    protected ActivityOccurrenceTO triggeredActivity;

    /**
     * Gets the value of the roleInContractRequestRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInContractRequestTypeTO }
     *     
     */
    public RoleInContractRequestTypeTO getRoleInContractRequestRootType() {
        return roleInContractRequestRootType;
    }

    /**
     * Sets the value of the roleInContractRequestRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInContractRequestTypeTO }
     *     
     */
    public void setRoleInContractRequestRootType(RoleInContractRequestTypeTO value) {
        this.roleInContractRequestRootType = value;
    }

    /**
     * Gets the value of the triggeredActivity property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityOccurrenceTO }
     *     
     */
    public ActivityOccurrenceTO getTriggeredActivity() {
        return triggeredActivity;
    }

    /**
     * Sets the value of the triggeredActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityOccurrenceTO }
     *     
     */
    public void setTriggeredActivity(ActivityOccurrenceTO value) {
        this.triggeredActivity = value;
    }

}
