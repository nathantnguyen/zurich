
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FormalMedicalConditionName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FormalMedicalConditionName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Aids"/>
 *     &lt;enumeration value="Broken Tibia"/>
 *     &lt;enumeration value="Diabetes"/>
 *     &lt;enumeration value="Hepatitis B"/>
 *     &lt;enumeration value="General Health"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FormalMedicalConditionName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum FormalMedicalConditionName {

    @XmlEnumValue("Aids")
    AIDS("Aids"),
    @XmlEnumValue("Broken Tibia")
    BROKEN_TIBIA("Broken Tibia"),
    @XmlEnumValue("Diabetes")
    DIABETES("Diabetes"),
    @XmlEnumValue("Hepatitis B")
    HEPATITIS_B("Hepatitis B"),
    @XmlEnumValue("General Health")
    GENERAL_HEALTH("General Health");
    private final String value;

    FormalMedicalConditionName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FormalMedicalConditionName fromValue(String v) {
        for (FormalMedicalConditionName c: FormalMedicalConditionName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
