
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DialogueLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DialogueLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Simplified"/>
 *     &lt;enumeration value="Very Simplified"/>
 *     &lt;enumeration value="Default"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DialogueLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum DialogueLevel {

    @XmlEnumValue("Simplified")
    SIMPLIFIED("Simplified"),
    @XmlEnumValue("Very Simplified")
    VERY_SIMPLIFIED("Very Simplified"),
    @XmlEnumValue("Default")
    DEFAULT("Default");
    private final String value;

    DialogueLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DialogueLevel fromValue(String v) {
        for (DialogueLevel c: DialogueLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
