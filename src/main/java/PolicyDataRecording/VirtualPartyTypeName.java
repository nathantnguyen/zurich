
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VirtualPartyTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VirtualPartyTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Kiosk"/>
 *     &lt;enumeration value="Web Site"/>
 *     &lt;enumeration value="Voice Response Unit"/>
 *     &lt;enumeration value="Placing Exchange"/>
 *     &lt;enumeration value="Social Media Network"/>
 *     &lt;enumeration value="Software Administration System"/>
 *     &lt;enumeration value="Computer System"/>
 *     &lt;enumeration value="Work Basket"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VirtualPartyTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum VirtualPartyTypeName {

    @XmlEnumValue("Kiosk")
    KIOSK("Kiosk"),
    @XmlEnumValue("Web Site")
    WEB_SITE("Web Site"),
    @XmlEnumValue("Voice Response Unit")
    VOICE_RESPONSE_UNIT("Voice Response Unit"),
    @XmlEnumValue("Placing Exchange")
    PLACING_EXCHANGE("Placing Exchange"),
    @XmlEnumValue("Social Media Network")
    SOCIAL_MEDIA_NETWORK("Social Media Network"),
    @XmlEnumValue("Software Administration System")
    SOFTWARE_ADMINISTRATION_SYSTEM("Software Administration System"),
    @XmlEnumValue("Computer System")
    COMPUTER_SYSTEM("Computer System"),
    @XmlEnumValue("Work Basket")
    WORK_BASKET("Work Basket");
    private final String value;

    VirtualPartyTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VirtualPartyTypeName fromValue(String v) {
        for (VirtualPartyTypeName c: VirtualPartyTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
