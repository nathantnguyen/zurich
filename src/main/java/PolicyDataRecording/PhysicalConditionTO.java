
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PhysicalCondition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PhysicalCondition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Condition_TO">
 *       &lt;sequence>
 *         &lt;element name="lossRate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="lossRateReason" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}LossRateReason" minOccurs="0"/>
 *         &lt;element name="repairability" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Repairable" minOccurs="0"/>
 *         &lt;element name="affectedPhysicalObject" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhysicalCondition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "lossRate",
    "lossRateReason",
    "repairability",
    "affectedPhysicalObject"
})
@XmlSeeAlso({
    TelematicConditionTO.class,
    MedicalConditionTO.class
})
public class PhysicalConditionTO
    extends ConditionTO
{

    protected BigDecimal lossRate;
    protected LossRateReason lossRateReason;
    protected Repairable repairability;
    protected PhysicalObjectTO affectedPhysicalObject;

    /**
     * Gets the value of the lossRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLossRate() {
        return lossRate;
    }

    /**
     * Sets the value of the lossRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLossRate(BigDecimal value) {
        this.lossRate = value;
    }

    /**
     * Gets the value of the lossRateReason property.
     * 
     * @return
     *     possible object is
     *     {@link LossRateReason }
     *     
     */
    public LossRateReason getLossRateReason() {
        return lossRateReason;
    }

    /**
     * Sets the value of the lossRateReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link LossRateReason }
     *     
     */
    public void setLossRateReason(LossRateReason value) {
        this.lossRateReason = value;
    }

    /**
     * Gets the value of the repairability property.
     * 
     * @return
     *     possible object is
     *     {@link Repairable }
     *     
     */
    public Repairable getRepairability() {
        return repairability;
    }

    /**
     * Sets the value of the repairability property.
     * 
     * @param value
     *     allowed object is
     *     {@link Repairable }
     *     
     */
    public void setRepairability(Repairable value) {
        this.repairability = value;
    }

    /**
     * Gets the value of the affectedPhysicalObject property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public PhysicalObjectTO getAffectedPhysicalObject() {
        return affectedPhysicalObject;
    }

    /**
     * Sets the value of the affectedPhysicalObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setAffectedPhysicalObject(PhysicalObjectTO value) {
        this.affectedPhysicalObject = value;
    }

}
