
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentDueState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentDueState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Due"/>
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="Fully Settled"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Partially Settled"/>
 *     &lt;enumeration value="Replaced"/>
 *     &lt;enumeration value="Written Off"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentDueState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum PaymentDueState {

    @XmlEnumValue("Due")
    DUE("Due"),
    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("Fully Settled")
    FULLY_SETTLED("Fully Settled"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Partially Settled")
    PARTIALLY_SETTLED("Partially Settled"),
    @XmlEnumValue("Replaced")
    REPLACED("Replaced"),
    @XmlEnumValue("Written Off")
    WRITTEN_OFF("Written Off");
    private final String value;

    PaymentDueState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentDueState fromValue(String v) {
        for (PaymentDueState c: PaymentDueState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
