
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialAssetTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FinancialAssetTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Forward Currency"/>
 *     &lt;enumeration value="Spot Currency"/>
 *     &lt;enumeration value="Currency Instrument"/>
 *     &lt;enumeration value="Deposit Instrument"/>
 *     &lt;enumeration value="Foreign Exchange Forward Currency"/>
 *     &lt;enumeration value="Foreign Exchange Spot Currency"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FinancialAssetTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum FinancialAssetTypeName {

    @XmlEnumValue("Forward Currency")
    FORWARD_CURRENCY("Forward Currency"),
    @XmlEnumValue("Spot Currency")
    SPOT_CURRENCY("Spot Currency"),
    @XmlEnumValue("Currency Instrument")
    CURRENCY_INSTRUMENT("Currency Instrument"),
    @XmlEnumValue("Deposit Instrument")
    DEPOSIT_INSTRUMENT("Deposit Instrument"),
    @XmlEnumValue("Foreign Exchange Forward Currency")
    FOREIGN_EXCHANGE_FORWARD_CURRENCY("Foreign Exchange Forward Currency"),
    @XmlEnumValue("Foreign Exchange Spot Currency")
    FOREIGN_EXCHANGE_SPOT_CURRENCY("Foreign Exchange Spot Currency");
    private final String value;

    FinancialAssetTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FinancialAssetTypeName fromValue(String v) {
        for (FinancialAssetTypeName c: FinancialAssetTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
