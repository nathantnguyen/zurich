
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Colour.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Colour">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Red"/>
 *     &lt;enumeration value="Green"/>
 *     &lt;enumeration value="Yellow"/>
 *     &lt;enumeration value="Blue"/>
 *     &lt;enumeration value="Black"/>
 *     &lt;enumeration value="White"/>
 *     &lt;enumeration value="Brown"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Colour", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum Colour {

    @XmlEnumValue("Red")
    RED("Red"),
    @XmlEnumValue("Green")
    GREEN("Green"),
    @XmlEnumValue("Yellow")
    YELLOW("Yellow"),
    @XmlEnumValue("Blue")
    BLUE("Blue"),
    @XmlEnumValue("Black")
    BLACK("Black"),
    @XmlEnumValue("White")
    WHITE("White"),
    @XmlEnumValue("Brown")
    BROWN("Brown");
    private final String value;

    Colour(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Colour fromValue(String v) {
        for (Colour c: Colour.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
