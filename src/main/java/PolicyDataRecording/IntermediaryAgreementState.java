
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntermediaryAgreementState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IntermediaryAgreementState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="In Force"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Suspended"/>
 *     &lt;enumeration value="Terminated"/>
 *     &lt;enumeration value="Under Negotiation"/>
 *     &lt;enumeration value="Transferred"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IntermediaryAgreementState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/")
@XmlEnum
public enum IntermediaryAgreementState {

    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("In Force")
    IN_FORCE("In Force"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Suspended")
    SUSPENDED("Suspended"),
    @XmlEnumValue("Terminated")
    TERMINATED("Terminated"),
    @XmlEnumValue("Under Negotiation")
    UNDER_NEGOTIATION("Under Negotiation"),
    @XmlEnumValue("Transferred")
    TRANSFERRED("Transferred");
    private final String value;

    IntermediaryAgreementState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IntermediaryAgreementState fromValue(String v) {
        for (IntermediaryAgreementState c: IntermediaryAgreementState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
