
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScenarioSolvencyMarginStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ScenarioSolvencyMarginStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Gross"/>
 *     &lt;enumeration value="Net"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ScenarioSolvencyMarginStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum ScenarioSolvencyMarginStatus {

    @XmlEnumValue("Gross")
    GROSS("Gross"),
    @XmlEnumValue("Net")
    NET("Net");
    private final String value;

    ScenarioSolvencyMarginStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ScenarioSolvencyMarginStatus fromValue(String v) {
        for (ScenarioSolvencyMarginStatus c: ScenarioSolvencyMarginStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
