
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInRiskExposure_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInRiskExposure_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="objectAtRisk" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="taskAtRisk" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/TaskManager/}Task_TO" minOccurs="0"/>
 *         &lt;element name="roleTypeName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="contextRiskExposure" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskExposure_TO" minOccurs="0"/>
 *         &lt;element name="roleInRiskExposureRootType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}RoleInRiskExposureTypeName" minOccurs="0"/>
 *         &lt;element name="riskLocation" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInRiskExposure_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "objectAtRisk",
    "taskAtRisk",
    "roleTypeName",
    "contextRiskExposure",
    "roleInRiskExposureRootType",
    "riskLocation"
})
public class RoleInRiskExposureTO
    extends RoleInContextClassTO
{

    protected ObjectReferenceTO objectAtRisk;
    protected TaskTO taskAtRisk;
    protected String roleTypeName;
    protected RiskExposureTO contextRiskExposure;
    protected RoleInRiskExposureTypeName roleInRiskExposureRootType;
    protected PlaceTO riskLocation;

    /**
     * Gets the value of the objectAtRisk property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getObjectAtRisk() {
        return objectAtRisk;
    }

    /**
     * Sets the value of the objectAtRisk property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setObjectAtRisk(ObjectReferenceTO value) {
        this.objectAtRisk = value;
    }

    /**
     * Gets the value of the taskAtRisk property.
     * 
     * @return
     *     possible object is
     *     {@link TaskTO }
     *     
     */
    public TaskTO getTaskAtRisk() {
        return taskAtRisk;
    }

    /**
     * Sets the value of the taskAtRisk property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskTO }
     *     
     */
    public void setTaskAtRisk(TaskTO value) {
        this.taskAtRisk = value;
    }

    /**
     * Gets the value of the roleTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleTypeName() {
        return roleTypeName;
    }

    /**
     * Sets the value of the roleTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleTypeName(String value) {
        this.roleTypeName = value;
    }

    /**
     * Gets the value of the contextRiskExposure property.
     * 
     * @return
     *     possible object is
     *     {@link RiskExposureTO }
     *     
     */
    public RiskExposureTO getContextRiskExposure() {
        return contextRiskExposure;
    }

    /**
     * Sets the value of the contextRiskExposure property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskExposureTO }
     *     
     */
    public void setContextRiskExposure(RiskExposureTO value) {
        this.contextRiskExposure = value;
    }

    /**
     * Gets the value of the roleInRiskExposureRootType property.
     * 
     * @return
     *     possible object is
     *     {@link RoleInRiskExposureTypeName }
     *     
     */
    public RoleInRiskExposureTypeName getRoleInRiskExposureRootType() {
        return roleInRiskExposureRootType;
    }

    /**
     * Sets the value of the roleInRiskExposureRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoleInRiskExposureTypeName }
     *     
     */
    public void setRoleInRiskExposureRootType(RoleInRiskExposureTypeName value) {
        this.roleInRiskExposureRootType = value;
    }

    /**
     * Gets the value of the riskLocation property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getRiskLocation() {
        return riskLocation;
    }

    /**
     * Sets the value of the riskLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setRiskLocation(PlaceTO value) {
        this.riskLocation = value;
    }

}
