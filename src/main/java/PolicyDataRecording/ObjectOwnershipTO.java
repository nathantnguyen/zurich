
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ObjectOwnership_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectOwnership_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RoleInRolePlayer_TO">
 *       &lt;sequence>
 *         &lt;element name="ownershipForm" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}PhysicalObjectOwnershipForm" minOccurs="0"/>
 *         &lt;element name="acquisitionCost" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="firstOwner" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="ownershipPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="purchaseDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="ownedPhysicalObject" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOwnership_TO", propOrder = {
    "ownershipForm",
    "acquisitionCost",
    "firstOwner",
    "ownershipPercentage",
    "purchaseDate",
    "ownedPhysicalObject"
})
public class ObjectOwnershipTO
    extends RoleInRolePlayerTO
{

    protected PhysicalObjectOwnershipForm ownershipForm;
    protected BaseCurrencyAmount acquisitionCost;
    protected Boolean firstOwner;
    protected BigDecimal ownershipPercentage;
    protected XMLGregorianCalendar purchaseDate;
    protected PhysicalObjectTO ownedPhysicalObject;

    /**
     * Gets the value of the ownershipForm property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalObjectOwnershipForm }
     *     
     */
    public PhysicalObjectOwnershipForm getOwnershipForm() {
        return ownershipForm;
    }

    /**
     * Sets the value of the ownershipForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalObjectOwnershipForm }
     *     
     */
    public void setOwnershipForm(PhysicalObjectOwnershipForm value) {
        this.ownershipForm = value;
    }

    /**
     * Gets the value of the acquisitionCost property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAcquisitionCost() {
        return acquisitionCost;
    }

    /**
     * Sets the value of the acquisitionCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAcquisitionCost(BaseCurrencyAmount value) {
        this.acquisitionCost = value;
    }

    /**
     * Gets the value of the firstOwner property.
     * This getter has been renamed from isFirstOwner() to getFirstOwner() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFirstOwner() {
        return firstOwner;
    }

    /**
     * Sets the value of the firstOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFirstOwner(Boolean value) {
        this.firstOwner = value;
    }

    /**
     * Gets the value of the ownershipPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOwnershipPercentage() {
        return ownershipPercentage;
    }

    /**
     * Sets the value of the ownershipPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOwnershipPercentage(BigDecimal value) {
        this.ownershipPercentage = value;
    }

    /**
     * Gets the value of the purchaseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * Sets the value of the purchaseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPurchaseDate(XMLGregorianCalendar value) {
        this.purchaseDate = value;
    }

    /**
     * Gets the value of the ownedPhysicalObject property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public PhysicalObjectTO getOwnedPhysicalObject() {
        return ownedPhysicalObject;
    }

    /**
     * Sets the value of the ownedPhysicalObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setOwnedPhysicalObject(PhysicalObjectTO value) {
        this.ownedPhysicalObject = value;
    }

}
