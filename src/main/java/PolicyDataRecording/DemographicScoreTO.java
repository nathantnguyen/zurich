
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DemographicScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DemographicScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="purchasingPower" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}PurchasingPower" minOccurs="0"/>
 *         &lt;element name="buildingDegree" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}BuildingDegree" minOccurs="0"/>
 *         &lt;element name="averageAge" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="averageBuildingAge" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="averageFamilySize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="averageHouseholdDwellingSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="averageNumberInHousehold" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="percentDwellingOwnership" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="medianIncome" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="medianMonthlyRent" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="medianPropertyValue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="percentBlueCollar" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentCollegeNoDegree" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentDegreeOnly" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentFamilyHouseholds" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentHouseholdsLowIncome" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentHighSchoolDiplomaOnly" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentHouseholdsWithChildren" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="housingUnitSizeDistribution" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="percentHousingUnitsOwnerOccupied" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentHousingUnitsSingleUnitDetached" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentNoHighSchoolDiploma" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentProfessionalWhiteCollar" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentSuburban" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentUnmarriedHouseholds" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentUrban" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="percentWhiteCollar" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="populationDensity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="professionalEmploymentPercentage" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="sociologicalStructure" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="buildingDesignStyle" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}DesignStyle" minOccurs="0"/>
 *         &lt;element name="mainLanguage" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}Language" minOccurs="0"/>
 *         &lt;element name="buildingCodeEffectivenessGrade" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}BuildingCodeEffectivenessGrade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemographicScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "purchasingPower",
    "buildingDegree",
    "averageAge",
    "averageBuildingAge",
    "averageFamilySize",
    "averageHouseholdDwellingSize",
    "averageNumberInHousehold",
    "percentDwellingOwnership",
    "medianIncome",
    "medianMonthlyRent",
    "medianPropertyValue",
    "percentBlueCollar",
    "percentCollegeNoDegree",
    "percentDegreeOnly",
    "percentFamilyHouseholds",
    "percentHouseholdsLowIncome",
    "percentHighSchoolDiplomaOnly",
    "percentHouseholdsWithChildren",
    "housingUnitSizeDistribution",
    "percentHousingUnitsOwnerOccupied",
    "percentHousingUnitsSingleUnitDetached",
    "percentNoHighSchoolDiploma",
    "percentProfessionalWhiteCollar",
    "percentSuburban",
    "percentUnmarriedHouseholds",
    "percentUrban",
    "percentWhiteCollar",
    "populationDensity",
    "professionalEmploymentPercentage",
    "sociologicalStructure",
    "buildingDesignStyle",
    "mainLanguage",
    "buildingCodeEffectivenessGrade"
})
public class DemographicScoreTO
    extends ScoreTO
{

    protected PurchasingPower purchasingPower;
    protected BuildingDegree buildingDegree;
    protected BigInteger averageAge;
    protected BigInteger averageBuildingAge;
    protected BigInteger averageFamilySize;
    protected BigInteger averageHouseholdDwellingSize;
    protected BigInteger averageNumberInHousehold;
    protected BigDecimal percentDwellingOwnership;
    protected String medianIncome;
    protected String medianMonthlyRent;
    protected String medianPropertyValue;
    protected BigDecimal percentBlueCollar;
    protected BigDecimal percentCollegeNoDegree;
    protected BigDecimal percentDegreeOnly;
    protected BigDecimal percentFamilyHouseholds;
    protected BigDecimal percentHouseholdsLowIncome;
    protected BigDecimal percentHighSchoolDiplomaOnly;
    protected BigDecimal percentHouseholdsWithChildren;
    protected String housingUnitSizeDistribution;
    protected BigDecimal percentHousingUnitsOwnerOccupied;
    protected BigDecimal percentHousingUnitsSingleUnitDetached;
    protected BigDecimal percentNoHighSchoolDiploma;
    protected BigDecimal percentProfessionalWhiteCollar;
    protected BigDecimal percentSuburban;
    protected BigDecimal percentUnmarriedHouseholds;
    protected BigDecimal percentUrban;
    protected BigDecimal percentWhiteCollar;
    protected BigDecimal populationDensity;
    protected BigDecimal professionalEmploymentPercentage;
    protected String sociologicalStructure;
    protected DesignStyle buildingDesignStyle;
    protected Language mainLanguage;
    protected String buildingCodeEffectivenessGrade;

    /**
     * Gets the value of the purchasingPower property.
     * 
     * @return
     *     possible object is
     *     {@link PurchasingPower }
     *     
     */
    public PurchasingPower getPurchasingPower() {
        return purchasingPower;
    }

    /**
     * Sets the value of the purchasingPower property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchasingPower }
     *     
     */
    public void setPurchasingPower(PurchasingPower value) {
        this.purchasingPower = value;
    }

    /**
     * Gets the value of the buildingDegree property.
     * 
     * @return
     *     possible object is
     *     {@link BuildingDegree }
     *     
     */
    public BuildingDegree getBuildingDegree() {
        return buildingDegree;
    }

    /**
     * Sets the value of the buildingDegree property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildingDegree }
     *     
     */
    public void setBuildingDegree(BuildingDegree value) {
        this.buildingDegree = value;
    }

    /**
     * Gets the value of the averageAge property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageAge() {
        return averageAge;
    }

    /**
     * Sets the value of the averageAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageAge(BigInteger value) {
        this.averageAge = value;
    }

    /**
     * Gets the value of the averageBuildingAge property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageBuildingAge() {
        return averageBuildingAge;
    }

    /**
     * Sets the value of the averageBuildingAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageBuildingAge(BigInteger value) {
        this.averageBuildingAge = value;
    }

    /**
     * Gets the value of the averageFamilySize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageFamilySize() {
        return averageFamilySize;
    }

    /**
     * Sets the value of the averageFamilySize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageFamilySize(BigInteger value) {
        this.averageFamilySize = value;
    }

    /**
     * Gets the value of the averageHouseholdDwellingSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageHouseholdDwellingSize() {
        return averageHouseholdDwellingSize;
    }

    /**
     * Sets the value of the averageHouseholdDwellingSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageHouseholdDwellingSize(BigInteger value) {
        this.averageHouseholdDwellingSize = value;
    }

    /**
     * Gets the value of the averageNumberInHousehold property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAverageNumberInHousehold() {
        return averageNumberInHousehold;
    }

    /**
     * Sets the value of the averageNumberInHousehold property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAverageNumberInHousehold(BigInteger value) {
        this.averageNumberInHousehold = value;
    }

    /**
     * Gets the value of the percentDwellingOwnership property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentDwellingOwnership() {
        return percentDwellingOwnership;
    }

    /**
     * Sets the value of the percentDwellingOwnership property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentDwellingOwnership(BigDecimal value) {
        this.percentDwellingOwnership = value;
    }

    /**
     * Gets the value of the medianIncome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedianIncome() {
        return medianIncome;
    }

    /**
     * Sets the value of the medianIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedianIncome(String value) {
        this.medianIncome = value;
    }

    /**
     * Gets the value of the medianMonthlyRent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedianMonthlyRent() {
        return medianMonthlyRent;
    }

    /**
     * Sets the value of the medianMonthlyRent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedianMonthlyRent(String value) {
        this.medianMonthlyRent = value;
    }

    /**
     * Gets the value of the medianPropertyValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedianPropertyValue() {
        return medianPropertyValue;
    }

    /**
     * Sets the value of the medianPropertyValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedianPropertyValue(String value) {
        this.medianPropertyValue = value;
    }

    /**
     * Gets the value of the percentBlueCollar property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentBlueCollar() {
        return percentBlueCollar;
    }

    /**
     * Sets the value of the percentBlueCollar property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentBlueCollar(BigDecimal value) {
        this.percentBlueCollar = value;
    }

    /**
     * Gets the value of the percentCollegeNoDegree property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentCollegeNoDegree() {
        return percentCollegeNoDegree;
    }

    /**
     * Sets the value of the percentCollegeNoDegree property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentCollegeNoDegree(BigDecimal value) {
        this.percentCollegeNoDegree = value;
    }

    /**
     * Gets the value of the percentDegreeOnly property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentDegreeOnly() {
        return percentDegreeOnly;
    }

    /**
     * Sets the value of the percentDegreeOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentDegreeOnly(BigDecimal value) {
        this.percentDegreeOnly = value;
    }

    /**
     * Gets the value of the percentFamilyHouseholds property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentFamilyHouseholds() {
        return percentFamilyHouseholds;
    }

    /**
     * Sets the value of the percentFamilyHouseholds property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentFamilyHouseholds(BigDecimal value) {
        this.percentFamilyHouseholds = value;
    }

    /**
     * Gets the value of the percentHouseholdsLowIncome property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentHouseholdsLowIncome() {
        return percentHouseholdsLowIncome;
    }

    /**
     * Sets the value of the percentHouseholdsLowIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentHouseholdsLowIncome(BigDecimal value) {
        this.percentHouseholdsLowIncome = value;
    }

    /**
     * Gets the value of the percentHighSchoolDiplomaOnly property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentHighSchoolDiplomaOnly() {
        return percentHighSchoolDiplomaOnly;
    }

    /**
     * Sets the value of the percentHighSchoolDiplomaOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentHighSchoolDiplomaOnly(BigDecimal value) {
        this.percentHighSchoolDiplomaOnly = value;
    }

    /**
     * Gets the value of the percentHouseholdsWithChildren property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentHouseholdsWithChildren() {
        return percentHouseholdsWithChildren;
    }

    /**
     * Sets the value of the percentHouseholdsWithChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentHouseholdsWithChildren(BigDecimal value) {
        this.percentHouseholdsWithChildren = value;
    }

    /**
     * Gets the value of the housingUnitSizeDistribution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHousingUnitSizeDistribution() {
        return housingUnitSizeDistribution;
    }

    /**
     * Sets the value of the housingUnitSizeDistribution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHousingUnitSizeDistribution(String value) {
        this.housingUnitSizeDistribution = value;
    }

    /**
     * Gets the value of the percentHousingUnitsOwnerOccupied property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentHousingUnitsOwnerOccupied() {
        return percentHousingUnitsOwnerOccupied;
    }

    /**
     * Sets the value of the percentHousingUnitsOwnerOccupied property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentHousingUnitsOwnerOccupied(BigDecimal value) {
        this.percentHousingUnitsOwnerOccupied = value;
    }

    /**
     * Gets the value of the percentHousingUnitsSingleUnitDetached property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentHousingUnitsSingleUnitDetached() {
        return percentHousingUnitsSingleUnitDetached;
    }

    /**
     * Sets the value of the percentHousingUnitsSingleUnitDetached property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentHousingUnitsSingleUnitDetached(BigDecimal value) {
        this.percentHousingUnitsSingleUnitDetached = value;
    }

    /**
     * Gets the value of the percentNoHighSchoolDiploma property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentNoHighSchoolDiploma() {
        return percentNoHighSchoolDiploma;
    }

    /**
     * Sets the value of the percentNoHighSchoolDiploma property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentNoHighSchoolDiploma(BigDecimal value) {
        this.percentNoHighSchoolDiploma = value;
    }

    /**
     * Gets the value of the percentProfessionalWhiteCollar property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentProfessionalWhiteCollar() {
        return percentProfessionalWhiteCollar;
    }

    /**
     * Sets the value of the percentProfessionalWhiteCollar property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentProfessionalWhiteCollar(BigDecimal value) {
        this.percentProfessionalWhiteCollar = value;
    }

    /**
     * Gets the value of the percentSuburban property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentSuburban() {
        return percentSuburban;
    }

    /**
     * Sets the value of the percentSuburban property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentSuburban(BigDecimal value) {
        this.percentSuburban = value;
    }

    /**
     * Gets the value of the percentUnmarriedHouseholds property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentUnmarriedHouseholds() {
        return percentUnmarriedHouseholds;
    }

    /**
     * Sets the value of the percentUnmarriedHouseholds property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentUnmarriedHouseholds(BigDecimal value) {
        this.percentUnmarriedHouseholds = value;
    }

    /**
     * Gets the value of the percentUrban property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentUrban() {
        return percentUrban;
    }

    /**
     * Sets the value of the percentUrban property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentUrban(BigDecimal value) {
        this.percentUrban = value;
    }

    /**
     * Gets the value of the percentWhiteCollar property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentWhiteCollar() {
        return percentWhiteCollar;
    }

    /**
     * Sets the value of the percentWhiteCollar property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentWhiteCollar(BigDecimal value) {
        this.percentWhiteCollar = value;
    }

    /**
     * Gets the value of the populationDensity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPopulationDensity() {
        return populationDensity;
    }

    /**
     * Sets the value of the populationDensity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPopulationDensity(BigDecimal value) {
        this.populationDensity = value;
    }

    /**
     * Gets the value of the professionalEmploymentPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProfessionalEmploymentPercentage() {
        return professionalEmploymentPercentage;
    }

    /**
     * Sets the value of the professionalEmploymentPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProfessionalEmploymentPercentage(BigDecimal value) {
        this.professionalEmploymentPercentage = value;
    }

    /**
     * Gets the value of the sociologicalStructure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSociologicalStructure() {
        return sociologicalStructure;
    }

    /**
     * Sets the value of the sociologicalStructure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSociologicalStructure(String value) {
        this.sociologicalStructure = value;
    }

    /**
     * Gets the value of the buildingDesignStyle property.
     * 
     * @return
     *     possible object is
     *     {@link DesignStyle }
     *     
     */
    public DesignStyle getBuildingDesignStyle() {
        return buildingDesignStyle;
    }

    /**
     * Sets the value of the buildingDesignStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link DesignStyle }
     *     
     */
    public void setBuildingDesignStyle(DesignStyle value) {
        this.buildingDesignStyle = value;
    }

    /**
     * Gets the value of the mainLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link Language }
     *     
     */
    public Language getMainLanguage() {
        return mainLanguage;
    }

    /**
     * Sets the value of the mainLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Language }
     *     
     */
    public void setMainLanguage(Language value) {
        this.mainLanguage = value;
    }

    /**
     * Gets the value of the buildingCodeEffectivenessGrade property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuildingCodeEffectivenessGrade() {
        return buildingCodeEffectivenessGrade;
    }

    /**
     * Sets the value of the buildingCodeEffectivenessGrade property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuildingCodeEffectivenessGrade(String value) {
        this.buildingCodeEffectivenessGrade = value;
    }

}
