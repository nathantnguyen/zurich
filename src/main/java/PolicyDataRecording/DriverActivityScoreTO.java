
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DriverActivityScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverActivityScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="accelerationScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="brakeScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="corneringScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="journeyDayTimeScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="mileageScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="roadTypeExposureScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="speedScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="swerveScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverActivityScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "accelerationScore",
    "brakeScore",
    "corneringScore",
    "journeyDayTimeScore",
    "mileageScore",
    "roadTypeExposureScore",
    "speedScore",
    "swerveScore"
})
public class DriverActivityScoreTO
    extends ScoreTO
{

    protected BigInteger accelerationScore;
    protected BigInteger brakeScore;
    protected BigInteger corneringScore;
    protected BigInteger journeyDayTimeScore;
    protected BigInteger mileageScore;
    protected BigInteger roadTypeExposureScore;
    protected BigInteger speedScore;
    protected BigInteger swerveScore;

    /**
     * Gets the value of the accelerationScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAccelerationScore() {
        return accelerationScore;
    }

    /**
     * Sets the value of the accelerationScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAccelerationScore(BigInteger value) {
        this.accelerationScore = value;
    }

    /**
     * Gets the value of the brakeScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBrakeScore() {
        return brakeScore;
    }

    /**
     * Sets the value of the brakeScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBrakeScore(BigInteger value) {
        this.brakeScore = value;
    }

    /**
     * Gets the value of the corneringScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCorneringScore() {
        return corneringScore;
    }

    /**
     * Sets the value of the corneringScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCorneringScore(BigInteger value) {
        this.corneringScore = value;
    }

    /**
     * Gets the value of the journeyDayTimeScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getJourneyDayTimeScore() {
        return journeyDayTimeScore;
    }

    /**
     * Sets the value of the journeyDayTimeScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setJourneyDayTimeScore(BigInteger value) {
        this.journeyDayTimeScore = value;
    }

    /**
     * Gets the value of the mileageScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMileageScore() {
        return mileageScore;
    }

    /**
     * Sets the value of the mileageScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMileageScore(BigInteger value) {
        this.mileageScore = value;
    }

    /**
     * Gets the value of the roadTypeExposureScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRoadTypeExposureScore() {
        return roadTypeExposureScore;
    }

    /**
     * Sets the value of the roadTypeExposureScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRoadTypeExposureScore(BigInteger value) {
        this.roadTypeExposureScore = value;
    }

    /**
     * Gets the value of the speedScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSpeedScore() {
        return speedScore;
    }

    /**
     * Sets the value of the speedScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSpeedScore(BigInteger value) {
        this.speedScore = value;
    }

    /**
     * Gets the value of the swerveScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSwerveScore() {
        return swerveScore;
    }

    /**
     * Sets the value of the swerveScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSwerveScore(BigInteger value) {
        this.swerveScore = value;
    }

}
