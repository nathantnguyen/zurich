
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ElectronicDocument_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ElectronicDocument_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}Document_TO">
 *       &lt;sequence>
 *         &lt;element name="fileLocation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="searchIndexes" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}DocumentSearchIndex_TO" minOccurs="0"/>
 *         &lt;element name="physicalVersion" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}PhysicalDocument_TO" minOccurs="0"/>
 *         &lt;element name="scannedDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="fileContent" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Base64Binary" minOccurs="0"/>
 *         &lt;element name="documentClass" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="mimeType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="fileSize" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElectronicDocument_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "fileLocation",
    "searchIndexes",
    "physicalVersion",
    "scannedDate",
    "fileContent",
    "documentClass",
    "mimeType",
    "fileSize"
})
public class ElectronicDocumentTO
    extends DocumentTO
{

    protected String fileLocation;
    protected DocumentSearchIndexTO searchIndexes;
    protected PhysicalDocumentTO physicalVersion;
    protected XMLGregorianCalendar scannedDate;
    protected byte[] fileContent;
    protected String documentClass;
    protected String mimeType;
    protected String fileSize;

    /**
     * Gets the value of the fileLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileLocation() {
        return fileLocation;
    }

    /**
     * Sets the value of the fileLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileLocation(String value) {
        this.fileLocation = value;
    }

    /**
     * Gets the value of the searchIndexes property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentSearchIndexTO }
     *     
     */
    public DocumentSearchIndexTO getSearchIndexes() {
        return searchIndexes;
    }

    /**
     * Sets the value of the searchIndexes property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentSearchIndexTO }
     *     
     */
    public void setSearchIndexes(DocumentSearchIndexTO value) {
        this.searchIndexes = value;
    }

    /**
     * Gets the value of the physicalVersion property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalDocumentTO }
     *     
     */
    public PhysicalDocumentTO getPhysicalVersion() {
        return physicalVersion;
    }

    /**
     * Sets the value of the physicalVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalDocumentTO }
     *     
     */
    public void setPhysicalVersion(PhysicalDocumentTO value) {
        this.physicalVersion = value;
    }

    /**
     * Gets the value of the scannedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getScannedDate() {
        return scannedDate;
    }

    /**
     * Sets the value of the scannedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setScannedDate(XMLGregorianCalendar value) {
        this.scannedDate = value;
    }

    /**
     * Gets the value of the fileContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFileContent() {
        return fileContent;
    }

    /**
     * Sets the value of the fileContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFileContent(byte[] value) {
        this.fileContent = value;
    }

    /**
     * Gets the value of the documentClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentClass() {
        return documentClass;
    }

    /**
     * Sets the value of the documentClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentClass(String value) {
        this.documentClass = value;
    }

    /**
     * Gets the value of the mimeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Sets the value of the mimeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeType(String value) {
        this.mimeType = value;
    }

    /**
     * Gets the value of the fileSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileSize() {
        return fileSize;
    }

    /**
     * Sets the value of the fileSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileSize(String value) {
        this.fileSize = value;
    }

}
