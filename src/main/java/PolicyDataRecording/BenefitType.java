
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BenefitType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BenefitType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Changes As In Original Contract"/>
 *     &lt;enumeration value="Changes In Ration To Original Contract"/>
 *     &lt;enumeration value="Level Death Benefit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BenefitType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum BenefitType {

    @XmlEnumValue("Changes As In Original Contract")
    CHANGES_AS_IN_ORIGINAL_CONTRACT("Changes As In Original Contract"),
    @XmlEnumValue("Changes In Ration To Original Contract")
    CHANGES_IN_RATION_TO_ORIGINAL_CONTRACT("Changes In Ration To Original Contract"),
    @XmlEnumValue("Level Death Benefit")
    LEVEL_DEATH_BENEFIT("Level Death Benefit");
    private final String value;

    BenefitType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BenefitType fromValue(String v) {
        for (BenefitType c: BenefitType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
