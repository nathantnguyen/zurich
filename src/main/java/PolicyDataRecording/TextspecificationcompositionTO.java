
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for textspecificationcomposition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="textspecificationcomposition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="sequenceNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="standardTextSpecification" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}StandardTextSpecification_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "textspecificationcomposition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "sequenceNumber",
    "standardTextSpecification"
})
public class TextspecificationcompositionTO
    extends BaseTransferObject
{

    protected BigInteger sequenceNumber;
    protected StandardTextSpecificationTO standardTextSpecification;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNumber(BigInteger value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the standardTextSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link StandardTextSpecificationTO }
     *     
     */
    public StandardTextSpecificationTO getStandardTextSpecification() {
        return standardTextSpecification;
    }

    /**
     * Sets the value of the standardTextSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardTextSpecificationTO }
     *     
     */
    public void setStandardTextSpecification(StandardTextSpecificationTO value) {
        this.standardTextSpecification = value;
    }

}
