
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInFinancialTransactionTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInFinancialTransactionTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Financial Transaction Payer"/>
 *     &lt;enumeration value="Role Player Authorising Payment"/>
 *     &lt;enumeration value="Representation Of Bill"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInFinancialTransactionTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum RoleInFinancialTransactionTypeName {

    @XmlEnumValue("Financial Transaction Payer")
    FINANCIAL_TRANSACTION_PAYER("Financial Transaction Payer"),
    @XmlEnumValue("Role Player Authorising Payment")
    ROLE_PLAYER_AUTHORISING_PAYMENT("Role Player Authorising Payment"),
    @XmlEnumValue("Representation Of Bill")
    REPRESENTATION_OF_BILL("Representation Of Bill");
    private final String value;

    RoleInFinancialTransactionTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInFinancialTransactionTypeName fromValue(String v) {
        for (RoleInFinancialTransactionTypeName c: RoleInFinancialTransactionTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
