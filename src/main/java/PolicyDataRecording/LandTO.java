
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Land_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Land_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO">
 *       &lt;sequence>
 *         &lt;element name="surfaceArea" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Land_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "surfaceArea"
})
public class LandTO
    extends PhysicalObjectTO
{

    protected BigDecimal surfaceArea;

    /**
     * Gets the value of the surfaceArea property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSurfaceArea() {
        return surfaceArea;
    }

    /**
     * Sets the value of the surfaceArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSurfaceArea(BigDecimal value) {
        this.surfaceArea = value;
    }

}
