
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PostalAddressRange_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PostalAddressRange_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="unitNumberLow" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="unitNumberParity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}NumberParity" minOccurs="0"/>
 *         &lt;element name="postalCodeExtensionLow" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="unitNumberHigh" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="postalCodeExtensionHigh" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="houseNumberParity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}NumberParity" minOccurs="0"/>
 *         &lt;element name="houseNumberLow" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="houseNumberHigh" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostalAddressRange_TO", propOrder = {
    "unitNumberLow",
    "unitNumberParity",
    "postalCodeExtensionLow",
    "unitNumberHigh",
    "postalCodeExtensionHigh",
    "houseNumberParity",
    "houseNumberLow",
    "houseNumberHigh"
})
public class PostalAddressRangeTO
    extends DependentObjectTO
{

    protected String unitNumberLow;
    protected NumberParity unitNumberParity;
    protected String postalCodeExtensionLow;
    protected String unitNumberHigh;
    protected String postalCodeExtensionHigh;
    protected NumberParity houseNumberParity;
    protected String houseNumberLow;
    protected String houseNumberHigh;

    /**
     * Gets the value of the unitNumberLow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitNumberLow() {
        return unitNumberLow;
    }

    /**
     * Sets the value of the unitNumberLow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitNumberLow(String value) {
        this.unitNumberLow = value;
    }

    /**
     * Gets the value of the unitNumberParity property.
     * 
     * @return
     *     possible object is
     *     {@link NumberParity }
     *     
     */
    public NumberParity getUnitNumberParity() {
        return unitNumberParity;
    }

    /**
     * Sets the value of the unitNumberParity property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberParity }
     *     
     */
    public void setUnitNumberParity(NumberParity value) {
        this.unitNumberParity = value;
    }

    /**
     * Gets the value of the postalCodeExtensionLow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCodeExtensionLow() {
        return postalCodeExtensionLow;
    }

    /**
     * Sets the value of the postalCodeExtensionLow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCodeExtensionLow(String value) {
        this.postalCodeExtensionLow = value;
    }

    /**
     * Gets the value of the unitNumberHigh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitNumberHigh() {
        return unitNumberHigh;
    }

    /**
     * Sets the value of the unitNumberHigh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitNumberHigh(String value) {
        this.unitNumberHigh = value;
    }

    /**
     * Gets the value of the postalCodeExtensionHigh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCodeExtensionHigh() {
        return postalCodeExtensionHigh;
    }

    /**
     * Sets the value of the postalCodeExtensionHigh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCodeExtensionHigh(String value) {
        this.postalCodeExtensionHigh = value;
    }

    /**
     * Gets the value of the houseNumberParity property.
     * 
     * @return
     *     possible object is
     *     {@link NumberParity }
     *     
     */
    public NumberParity getHouseNumberParity() {
        return houseNumberParity;
    }

    /**
     * Sets the value of the houseNumberParity property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberParity }
     *     
     */
    public void setHouseNumberParity(NumberParity value) {
        this.houseNumberParity = value;
    }

    /**
     * Gets the value of the houseNumberLow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseNumberLow() {
        return houseNumberLow;
    }

    /**
     * Sets the value of the houseNumberLow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseNumberLow(String value) {
        this.houseNumberLow = value;
    }

    /**
     * Gets the value of the houseNumberHigh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseNumberHigh() {
        return houseNumberHigh;
    }

    /**
     * Sets the value of the houseNumberHigh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseNumberHigh(String value) {
        this.houseNumberHigh = value;
    }

}
