
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Audit_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Audit_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentActivity_TO">
 *       &lt;sequence>
 *         &lt;element name="auditMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}AuditMethod" minOccurs="0"/>
 *         &lt;element name="auditSourceType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}AuditSourceType" minOccurs="0"/>
 *         &lt;element name="auditTransactionType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}AuditTransactionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Audit_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "auditMethod",
    "auditSourceType",
    "auditTransactionType"
})
public class AuditTO
    extends AssessmentActivityTO
{

    protected AuditMethod auditMethod;
    protected AuditSourceType auditSourceType;
    protected AuditTransactionType auditTransactionType;

    /**
     * Gets the value of the auditMethod property.
     * 
     * @return
     *     possible object is
     *     {@link AuditMethod }
     *     
     */
    public AuditMethod getAuditMethod() {
        return auditMethod;
    }

    /**
     * Sets the value of the auditMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditMethod }
     *     
     */
    public void setAuditMethod(AuditMethod value) {
        this.auditMethod = value;
    }

    /**
     * Gets the value of the auditSourceType property.
     * 
     * @return
     *     possible object is
     *     {@link AuditSourceType }
     *     
     */
    public AuditSourceType getAuditSourceType() {
        return auditSourceType;
    }

    /**
     * Sets the value of the auditSourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditSourceType }
     *     
     */
    public void setAuditSourceType(AuditSourceType value) {
        this.auditSourceType = value;
    }

    /**
     * Gets the value of the auditTransactionType property.
     * 
     * @return
     *     possible object is
     *     {@link AuditTransactionType }
     *     
     */
    public AuditTransactionType getAuditTransactionType() {
        return auditTransactionType;
    }

    /**
     * Sets the value of the auditTransactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditTransactionType }
     *     
     */
    public void setAuditTransactionType(AuditTransactionType value) {
        this.auditTransactionType = value;
    }

}
