
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssessmentResultRole_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssessmentResultRole_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RolePlayerClassRole_TO">
 *       &lt;sequence>
 *         &lt;element name="rolePlayerAssessmentResult" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResult_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssessmentResultRole_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "rolePlayerAssessmentResult"
})
public class AssessmentResultRoleTO
    extends RolePlayerClassRoleTO
{

    protected AssessmentResultTO rolePlayerAssessmentResult;

    /**
     * Gets the value of the rolePlayerAssessmentResult property.
     * 
     * @return
     *     possible object is
     *     {@link AssessmentResultTO }
     *     
     */
    public AssessmentResultTO getRolePlayerAssessmentResult() {
        return rolePlayerAssessmentResult;
    }

    /**
     * Sets the value of the rolePlayerAssessmentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssessmentResultTO }
     *     
     */
    public void setRolePlayerAssessmentResult(AssessmentResultTO value) {
        this.rolePlayerAssessmentResult = value;
    }

}
