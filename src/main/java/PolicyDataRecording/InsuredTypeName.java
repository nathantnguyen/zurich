
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InsuredTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InsuredTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Main Insured"/>
 *     &lt;enumeration value="Excluded Driver"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InsuredTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum InsuredTypeName {

    @XmlEnumValue("Main Insured")
    MAIN_INSURED("Main Insured"),
    @XmlEnumValue("Excluded Driver")
    EXCLUDED_DRIVER("Excluded Driver");
    private final String value;

    InsuredTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InsuredTypeName fromValue(String v) {
        for (InsuredTypeName c: InsuredTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
