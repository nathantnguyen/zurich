
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InterestCompoundingFrequency.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InterestCompoundingFrequency">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Daily"/>
 *     &lt;enumeration value="Monthly"/>
 *     &lt;enumeration value="Never"/>
 *     &lt;enumeration value="Quarterly"/>
 *     &lt;enumeration value="Yearly"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InterestCompoundingFrequency", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/")
@XmlEnum
public enum InterestCompoundingFrequency {

    @XmlEnumValue("Daily")
    DAILY("Daily"),
    @XmlEnumValue("Monthly")
    MONTHLY("Monthly"),
    @XmlEnumValue("Never")
    NEVER("Never"),
    @XmlEnumValue("Quarterly")
    QUARTERLY("Quarterly"),
    @XmlEnumValue("Yearly")
    YEARLY("Yearly");
    private final String value;

    InterestCompoundingFrequency(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InterestCompoundingFrequency fromValue(String v) {
        for (InterestCompoundingFrequency c: InterestCompoundingFrequency.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
