
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInContractRequestTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInContractRequestTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Provider Request For Activity"/>
 *     &lt;enumeration value="Financial Services Request For Activity"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInContractRequestTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum RoleInContractRequestTypeName {

    @XmlEnumValue("Provider Request For Activity")
    PROVIDER_REQUEST_FOR_ACTIVITY("Provider Request For Activity"),
    @XmlEnumValue("Financial Services Request For Activity")
    FINANCIAL_SERVICES_REQUEST_FOR_ACTIVITY("Financial Services Request For Activity");
    private final String value;

    RoleInContractRequestTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInContractRequestTypeName fromValue(String v) {
        for (RoleInContractRequestTypeName c: RoleInContractRequestTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
