
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditRatingCategory_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditRatingCategory_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO">
 *       &lt;sequence>
 *         &lt;element name="creditRatingIssuer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO" minOccurs="0"/>
 *         &lt;element name="usedStatistic" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}StandardCreditLevel_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditRatingCategory_TO", propOrder = {
    "creditRatingIssuer",
    "usedStatistic"
})
public class CreditRatingCategoryTO
    extends CategoryTO
{

    protected OrganisationTO creditRatingIssuer;
    protected StandardCreditLevelTO usedStatistic;

    /**
     * Gets the value of the creditRatingIssuer property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationTO }
     *     
     */
    public OrganisationTO getCreditRatingIssuer() {
        return creditRatingIssuer;
    }

    /**
     * Sets the value of the creditRatingIssuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationTO }
     *     
     */
    public void setCreditRatingIssuer(OrganisationTO value) {
        this.creditRatingIssuer = value;
    }

    /**
     * Gets the value of the usedStatistic property.
     * 
     * @return
     *     possible object is
     *     {@link StandardCreditLevelTO }
     *     
     */
    public StandardCreditLevelTO getUsedStatistic() {
        return usedStatistic;
    }

    /**
     * Sets the value of the usedStatistic property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardCreditLevelTO }
     *     
     */
    public void setUsedStatistic(StandardCreditLevelTO value) {
        this.usedStatistic = value;
    }

}
