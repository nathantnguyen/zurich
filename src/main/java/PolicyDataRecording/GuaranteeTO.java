
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Guarantee_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Guarantee_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreementComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="ancilliaryOwnFunds" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="sameGroupEntity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Guarantee_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "ancilliaryOwnFunds",
    "sameGroupEntity"
})
public class GuaranteeTO
    extends FinancialServicesAgreementComponentTO
{

    protected Boolean ancilliaryOwnFunds;
    protected Boolean sameGroupEntity;

    /**
     * Gets the value of the ancilliaryOwnFunds property.
     * This getter has been renamed from isAncilliaryOwnFunds() to getAncilliaryOwnFunds() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAncilliaryOwnFunds() {
        return ancilliaryOwnFunds;
    }

    /**
     * Sets the value of the ancilliaryOwnFunds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAncilliaryOwnFunds(Boolean value) {
        this.ancilliaryOwnFunds = value;
    }

    /**
     * Gets the value of the sameGroupEntity property.
     * This getter has been renamed from isSameGroupEntity() to getSameGroupEntity() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSameGroupEntity() {
        return sameGroupEntity;
    }

    /**
     * Sets the value of the sameGroupEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSameGroupEntity(Boolean value) {
        this.sameGroupEntity = value;
    }

}
