
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateSubmissionAndPartsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateSubmissionAndPartsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseHeader" type="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}ResponseHeader"/>
 *         &lt;element name="customer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Customer_TO" minOccurs="0"/>
 *         &lt;element name="submission" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}StatusChangeRequest_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateSubmissionAndPartsResponse", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Transactional/ServiceInterfaces/PolicyAcquisition/ServiceParameters/IPolicyDataRecording/", propOrder = {
    "responseHeader",
    "customer",
    "submission"
})
public class UpdateSubmissionAndPartsResponse {

    @XmlElement(required = true)
    protected ResponseHeader responseHeader;
    protected CustomerTO customer;
    protected StatusChangeRequestTO submission;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeader }
     *     
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeader }
     *     
     */
    public void setResponseHeader(ResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerTO }
     *     
     */
    public CustomerTO getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerTO }
     *     
     */
    public void setCustomer(CustomerTO value) {
        this.customer = value;
    }

    /**
     * Gets the value of the submission property.
     * 
     * @return
     *     possible object is
     *     {@link StatusChangeRequestTO }
     *     
     */
    public StatusChangeRequestTO getSubmission() {
        return submission;
    }

    /**
     * Sets the value of the submission property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusChangeRequestTO }
     *     
     */
    public void setSubmission(StatusChangeRequestTO value) {
        this.submission = value;
    }

}
