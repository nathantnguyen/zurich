
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompositeTextBlock_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompositeTextBlock_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}StandardTextSpecification_TO">
 *       &lt;sequence>
 *         &lt;element name="subdivision" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/}TextBlockSubdivision" minOccurs="0"/>
 *         &lt;element name="standardTextSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}textspecificationcomposition_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompositeTextBlock_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "subdivision",
    "standardTextSpecifications"
})
@XmlSeeAlso({
    DocumentTemplateTO.class
})
public class CompositeTextBlockTO
    extends StandardTextSpecificationTO
{

    protected TextBlockSubdivision subdivision;
    protected List<TextspecificationcompositionTO> standardTextSpecifications;

    /**
     * Gets the value of the subdivision property.
     * 
     * @return
     *     possible object is
     *     {@link TextBlockSubdivision }
     *     
     */
    public TextBlockSubdivision getSubdivision() {
        return subdivision;
    }

    /**
     * Sets the value of the subdivision property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextBlockSubdivision }
     *     
     */
    public void setSubdivision(TextBlockSubdivision value) {
        this.subdivision = value;
    }

    /**
     * Gets the value of the standardTextSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the standardTextSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStandardTextSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextspecificationcompositionTO }
     * 
     * 
     */
    public List<TextspecificationcompositionTO> getStandardTextSpecifications() {
        if (standardTextSpecifications == null) {
            standardTextSpecifications = new ArrayList<TextspecificationcompositionTO>();
        }
        return this.standardTextSpecifications;
    }

    /**
     * Sets the value of the standardTextSpecifications property.
     * 
     * @param standardTextSpecifications
     *     allowed object is
     *     {@link TextspecificationcompositionTO }
     *     
     */
    public void setStandardTextSpecifications(List<TextspecificationcompositionTO> standardTextSpecifications) {
        this.standardTextSpecifications = standardTextSpecifications;
    }

}
