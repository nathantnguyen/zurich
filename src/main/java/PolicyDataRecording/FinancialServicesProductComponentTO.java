
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialServicesProductComponent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialServicesProductComponent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}FinancialServicesProduct_TO">
 *       &lt;sequence>
 *         &lt;element name="exposureBasis" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="marketValueAdjustmentIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="unitisedStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}UnitisedStatus" minOccurs="0"/>
 *         &lt;element name="withProfitIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}ProductComponentWithProfitIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialServicesProductComponent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "exposureBasis",
    "marketValueAdjustmentIndicator",
    "unitisedStatus",
    "withProfitIndicator"
})
public class FinancialServicesProductComponentTO
    extends FinancialServicesProductTO
{

    protected String exposureBasis;
    protected Boolean marketValueAdjustmentIndicator;
    protected UnitisedStatus unitisedStatus;
    protected ProductComponentWithProfitIndicator withProfitIndicator;

    /**
     * Gets the value of the exposureBasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExposureBasis() {
        return exposureBasis;
    }

    /**
     * Sets the value of the exposureBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExposureBasis(String value) {
        this.exposureBasis = value;
    }

    /**
     * Gets the value of the marketValueAdjustmentIndicator property.
     * This getter has been renamed from isMarketValueAdjustmentIndicator() to getMarketValueAdjustmentIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMarketValueAdjustmentIndicator() {
        return marketValueAdjustmentIndicator;
    }

    /**
     * Sets the value of the marketValueAdjustmentIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketValueAdjustmentIndicator(Boolean value) {
        this.marketValueAdjustmentIndicator = value;
    }

    /**
     * Gets the value of the unitisedStatus property.
     * 
     * @return
     *     possible object is
     *     {@link UnitisedStatus }
     *     
     */
    public UnitisedStatus getUnitisedStatus() {
        return unitisedStatus;
    }

    /**
     * Sets the value of the unitisedStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnitisedStatus }
     *     
     */
    public void setUnitisedStatus(UnitisedStatus value) {
        this.unitisedStatus = value;
    }

    /**
     * Gets the value of the withProfitIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link ProductComponentWithProfitIndicator }
     *     
     */
    public ProductComponentWithProfitIndicator getWithProfitIndicator() {
        return withProfitIndicator;
    }

    /**
     * Sets the value of the withProfitIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductComponentWithProfitIndicator }
     *     
     */
    public void setWithProfitIndicator(ProductComponentWithProfitIndicator value) {
        this.withProfitIndicator = value;
    }

}
