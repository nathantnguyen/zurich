
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SettlementMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SettlementMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Ach"/>
 *     &lt;enumeration value="Book Entry"/>
 *     &lt;enumeration value="Chaps"/>
 *     &lt;enumeration value="Chips"/>
 *     &lt;enumeration value="Concentrator"/>
 *     &lt;enumeration value="Draft"/>
 *     &lt;enumeration value="Epay"/>
 *     &lt;enumeration value="Fednet"/>
 *     &lt;enumeration value="Swift"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SettlementMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum SettlementMethod {

    @XmlEnumValue("Ach")
    ACH("Ach"),
    @XmlEnumValue("Book Entry")
    BOOK_ENTRY("Book Entry"),
    @XmlEnumValue("Chaps")
    CHAPS("Chaps"),
    @XmlEnumValue("Chips")
    CHIPS("Chips"),
    @XmlEnumValue("Concentrator")
    CONCENTRATOR("Concentrator"),
    @XmlEnumValue("Draft")
    DRAFT("Draft"),
    @XmlEnumValue("Epay")
    EPAY("Epay"),
    @XmlEnumValue("Fednet")
    FEDNET("Fednet"),
    @XmlEnumValue("Swift")
    SWIFT("Swift");
    private final String value;

    SettlementMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SettlementMethod fromValue(String v) {
        for (SettlementMethod c: SettlementMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
