
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Guarantor_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Guarantor_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesRole_TO">
 *       &lt;sequence>
 *         &lt;element name="guaranteeLimit" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Guarantor_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "guaranteeLimit"
})
public class GuarantorTO
    extends FinancialServicesRoleTO
{

    protected BaseCurrencyAmount guaranteeLimit;

    /**
     * Gets the value of the guaranteeLimit property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getGuaranteeLimit() {
        return guaranteeLimit;
    }

    /**
     * Sets the value of the guaranteeLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setGuaranteeLimit(BaseCurrencyAmount value) {
        this.guaranteeLimit = value;
    }

}
