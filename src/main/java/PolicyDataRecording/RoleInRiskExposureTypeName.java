
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInRiskExposureTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RoleInRiskExposureTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Human Body At Risk"/>
 *     &lt;enumeration value="Task At Risk"/>
 *     &lt;enumeration value="Activity Occurrence At Risk"/>
 *     &lt;enumeration value="Object At Risk"/>
 *     &lt;enumeration value="Physical Object At Risk"/>
 *     &lt;enumeration value="Risk Location"/>
 *     &lt;enumeration value="Account At Risk"/>
 *     &lt;enumeration value="Risk Of Insurance"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RoleInRiskExposureTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum RoleInRiskExposureTypeName {

    @XmlEnumValue("Human Body At Risk")
    HUMAN_BODY_AT_RISK("Human Body At Risk"),
    @XmlEnumValue("Task At Risk")
    TASK_AT_RISK("Task At Risk"),
    @XmlEnumValue("Activity Occurrence At Risk")
    ACTIVITY_OCCURRENCE_AT_RISK("Activity Occurrence At Risk"),
    @XmlEnumValue("Object At Risk")
    OBJECT_AT_RISK("Object At Risk"),
    @XmlEnumValue("Physical Object At Risk")
    PHYSICAL_OBJECT_AT_RISK("Physical Object At Risk"),
    @XmlEnumValue("Risk Location")
    RISK_LOCATION("Risk Location"),
    @XmlEnumValue("Account At Risk")
    ACCOUNT_AT_RISK("Account At Risk"),
    @XmlEnumValue("Risk Of Insurance")
    RISK_OF_INSURANCE("Risk Of Insurance");
    private final String value;

    RoleInRiskExposureTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoleInRiskExposureTypeName fromValue(String v) {
        for (RoleInRiskExposureTypeName c: RoleInRiskExposureTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
