
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TextBlockSubdivision.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TextBlockSubdivision">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Chapter"/>
 *     &lt;enumeration value="Paragraph"/>
 *     &lt;enumeration value="Sentence"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TextBlockSubdivision", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum TextBlockSubdivision {

    @XmlEnumValue("Chapter")
    CHAPTER("Chapter"),
    @XmlEnumValue("Paragraph")
    PARAGRAPH("Paragraph"),
    @XmlEnumValue("Sentence")
    SENTENCE("Sentence");
    private final String value;

    TextBlockSubdivision(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TextBlockSubdivision fromValue(String v) {
        for (TextBlockSubdivision c: TextBlockSubdivision.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
