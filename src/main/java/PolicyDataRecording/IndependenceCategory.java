
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IndependenceCategory.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IndependenceCategory">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Employed"/>
 *     &lt;enumeration value="Fully Independent"/>
 *     &lt;enumeration value="Limited Independence"/>
 *     &lt;enumeration value="Tied"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IndependenceCategory", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/")
@XmlEnum
public enum IndependenceCategory {

    @XmlEnumValue("Employed")
    EMPLOYED("Employed"),
    @XmlEnumValue("Fully Independent")
    FULLY_INDEPENDENT("Fully Independent"),
    @XmlEnumValue("Limited Independence")
    LIMITED_INDEPENDENCE("Limited Independence"),
    @XmlEnumValue("Tied")
    TIED("Tied");
    private final String value;

    IndependenceCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IndependenceCategory fromValue(String v) {
        for (IndependenceCategory c: IndependenceCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
