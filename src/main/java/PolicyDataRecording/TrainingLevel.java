
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TrainingLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TrainingLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Advanced"/>
 *     &lt;enumeration value="Black Belt"/>
 *     &lt;enumeration value="Brown Belt"/>
 *     &lt;enumeration value="Senior"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TrainingLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/")
@XmlEnum
public enum TrainingLevel {

    @XmlEnumValue("Advanced")
    ADVANCED("Advanced"),
    @XmlEnumValue("Black Belt")
    BLACK_BELT("Black Belt"),
    @XmlEnumValue("Brown Belt")
    BROWN_BELT("Brown Belt"),
    @XmlEnumValue("Senior")
    SENIOR("Senior");
    private final String value;

    TrainingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrainingLevel fromValue(String v) {
        for (TrainingLevel c: TrainingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
