
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for InformationScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InformationScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="contactPreferenceProvided" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="differenceCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="expectedFormat" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="inconsistenciesBetweenDocuments" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="influenceRating" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}InfluenceRating" minOccurs="0"/>
 *         &lt;element name="informationMatched" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="informationVeracityRating" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="instanceCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="original" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="predictiveModel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}PredictiveModel" minOccurs="0"/>
 *         &lt;element name="quality" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="responseTime" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="sentimentRating" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/}SentimentRating" minOccurs="0"/>
 *         &lt;element name="sequential" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="sourceValidated" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformationScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/", propOrder = {
    "contactPreferenceProvided",
    "differenceCount",
    "expectedFormat",
    "inconsistenciesBetweenDocuments",
    "influenceRating",
    "informationMatched",
    "informationVeracityRating",
    "instanceCount",
    "original",
    "predictiveModel",
    "quality",
    "responseTime",
    "sentimentRating",
    "sequential",
    "sourceValidated"
})
public class InformationScoreTO
    extends ScoreTO
{

    protected Boolean contactPreferenceProvided;
    protected BigInteger differenceCount;
    protected Boolean expectedFormat;
    protected BigInteger inconsistenciesBetweenDocuments;
    protected InfluenceRating influenceRating;
    protected Boolean informationMatched;
    protected BigDecimal informationVeracityRating;
    protected BigInteger instanceCount;
    protected Boolean original;
    protected PredictiveModel predictiveModel;
    protected String quality;
    protected Duration responseTime;
    protected SentimentRating sentimentRating;
    protected Boolean sequential;
    protected Boolean sourceValidated;

    /**
     * Gets the value of the contactPreferenceProvided property.
     * This getter has been renamed from isContactPreferenceProvided() to getContactPreferenceProvided() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getContactPreferenceProvided() {
        return contactPreferenceProvided;
    }

    /**
     * Sets the value of the contactPreferenceProvided property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactPreferenceProvided(Boolean value) {
        this.contactPreferenceProvided = value;
    }

    /**
     * Gets the value of the differenceCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDifferenceCount() {
        return differenceCount;
    }

    /**
     * Sets the value of the differenceCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDifferenceCount(BigInteger value) {
        this.differenceCount = value;
    }

    /**
     * Gets the value of the expectedFormat property.
     * This getter has been renamed from isExpectedFormat() to getExpectedFormat() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExpectedFormat() {
        return expectedFormat;
    }

    /**
     * Sets the value of the expectedFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpectedFormat(Boolean value) {
        this.expectedFormat = value;
    }

    /**
     * Gets the value of the inconsistenciesBetweenDocuments property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInconsistenciesBetweenDocuments() {
        return inconsistenciesBetweenDocuments;
    }

    /**
     * Sets the value of the inconsistenciesBetweenDocuments property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInconsistenciesBetweenDocuments(BigInteger value) {
        this.inconsistenciesBetweenDocuments = value;
    }

    /**
     * Gets the value of the influenceRating property.
     * 
     * @return
     *     possible object is
     *     {@link InfluenceRating }
     *     
     */
    public InfluenceRating getInfluenceRating() {
        return influenceRating;
    }

    /**
     * Sets the value of the influenceRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfluenceRating }
     *     
     */
    public void setInfluenceRating(InfluenceRating value) {
        this.influenceRating = value;
    }

    /**
     * Gets the value of the informationMatched property.
     * This getter has been renamed from isInformationMatched() to getInformationMatched() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInformationMatched() {
        return informationMatched;
    }

    /**
     * Sets the value of the informationMatched property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInformationMatched(Boolean value) {
        this.informationMatched = value;
    }

    /**
     * Gets the value of the informationVeracityRating property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInformationVeracityRating() {
        return informationVeracityRating;
    }

    /**
     * Sets the value of the informationVeracityRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInformationVeracityRating(BigDecimal value) {
        this.informationVeracityRating = value;
    }

    /**
     * Gets the value of the instanceCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInstanceCount() {
        return instanceCount;
    }

    /**
     * Sets the value of the instanceCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInstanceCount(BigInteger value) {
        this.instanceCount = value;
    }

    /**
     * Gets the value of the original property.
     * This getter has been renamed from isOriginal() to getOriginal() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getOriginal() {
        return original;
    }

    /**
     * Sets the value of the original property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOriginal(Boolean value) {
        this.original = value;
    }

    /**
     * Gets the value of the predictiveModel property.
     * 
     * @return
     *     possible object is
     *     {@link PredictiveModel }
     *     
     */
    public PredictiveModel getPredictiveModel() {
        return predictiveModel;
    }

    /**
     * Sets the value of the predictiveModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link PredictiveModel }
     *     
     */
    public void setPredictiveModel(PredictiveModel value) {
        this.predictiveModel = value;
    }

    /**
     * Gets the value of the quality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuality() {
        return quality;
    }

    /**
     * Sets the value of the quality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuality(String value) {
        this.quality = value;
    }

    /**
     * Gets the value of the responseTime property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getResponseTime() {
        return responseTime;
    }

    /**
     * Sets the value of the responseTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setResponseTime(Duration value) {
        this.responseTime = value;
    }

    /**
     * Gets the value of the sentimentRating property.
     * 
     * @return
     *     possible object is
     *     {@link SentimentRating }
     *     
     */
    public SentimentRating getSentimentRating() {
        return sentimentRating;
    }

    /**
     * Sets the value of the sentimentRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link SentimentRating }
     *     
     */
    public void setSentimentRating(SentimentRating value) {
        this.sentimentRating = value;
    }

    /**
     * Gets the value of the sequential property.
     * This getter has been renamed from isSequential() to getSequential() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSequential() {
        return sequential;
    }

    /**
     * Sets the value of the sequential property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSequential(Boolean value) {
        this.sequential = value;
    }

    /**
     * Gets the value of the sourceValidated property.
     * This getter has been renamed from isSourceValidated() to getSourceValidated() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSourceValidated() {
        return sourceValidated;
    }

    /**
     * Sets the value of the sourceValidated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSourceValidated(Boolean value) {
        this.sourceValidated = value;
    }

}
