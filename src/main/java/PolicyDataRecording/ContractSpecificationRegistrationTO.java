
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractSpecificationRegistration_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractSpecificationRegistration_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Registration_TO">
 *       &lt;sequence>
 *         &lt;element name="registeredDocumentSpecifications" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Communication/}DocumentTemplate_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractSpecificationRegistration_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "registeredDocumentSpecifications"
})
public class ContractSpecificationRegistrationTO
    extends RegistrationTO
{

    protected List<DocumentTemplateTO> registeredDocumentSpecifications;

    /**
     * Gets the value of the registeredDocumentSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the registeredDocumentSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegisteredDocumentSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentTemplateTO }
     * 
     * 
     */
    public List<DocumentTemplateTO> getRegisteredDocumentSpecifications() {
        if (registeredDocumentSpecifications == null) {
            registeredDocumentSpecifications = new ArrayList<DocumentTemplateTO>();
        }
        return this.registeredDocumentSpecifications;
    }

    /**
     * Sets the value of the registeredDocumentSpecifications property.
     * 
     * @param registeredDocumentSpecifications
     *     allowed object is
     *     {@link DocumentTemplateTO }
     *     
     */
    public void setRegisteredDocumentSpecifications(List<DocumentTemplateTO> registeredDocumentSpecifications) {
        this.registeredDocumentSpecifications = registeredDocumentSpecifications;
    }

}
