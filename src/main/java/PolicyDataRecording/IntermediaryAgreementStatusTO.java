
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntermediaryAgreementStatus_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntermediaryAgreementStatus_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}StatusWithCommonReason_TO">
 *       &lt;sequence>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Intermediary/IntermediaryEnumerationsAndStates/}IntermediaryAgreementState" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntermediaryAgreementStatus_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/", propOrder = {
    "state"
})
public class IntermediaryAgreementStatusTO
    extends StatusWithCommonReasonTO
{

    protected IntermediaryAgreementState state;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link IntermediaryAgreementState }
     *     
     */
    public IntermediaryAgreementState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntermediaryAgreementState }
     *     
     */
    public void setState(IntermediaryAgreementState value) {
        this.state = value;
    }

}
