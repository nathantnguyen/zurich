
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ColourCombinationAvoidance.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ColourCombinationAvoidance">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Blue And Yellow"/>
 *     &lt;enumeration value="Red And Green"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ColourCombinationAvoidance", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum ColourCombinationAvoidance {

    @XmlEnumValue("Blue And Yellow")
    BLUE_AND_YELLOW("Blue And Yellow"),
    @XmlEnumValue("Red And Green")
    RED_AND_GREEN("Red And Green");
    private final String value;

    ColourCombinationAvoidance(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ColourCombinationAvoidance fromValue(String v) {
        for (ColourCombinationAvoidance c: ColourCombinationAvoidance.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
