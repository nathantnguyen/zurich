
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RateRoleInFsaTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RateRoleInFsaTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Guaranteed Rate"/>
 *     &lt;enumeration value="Applicable Credit Interest Rate"/>
 *     &lt;enumeration value="Applicable Debit Interest Rate"/>
 *     &lt;enumeration value="Financial Services Agreement Rate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RateRoleInFsaTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum RateRoleInFsaTypeName {

    @XmlEnumValue("Guaranteed Rate")
    GUARANTEED_RATE("Guaranteed Rate"),
    @XmlEnumValue("Applicable Credit Interest Rate")
    APPLICABLE_CREDIT_INTEREST_RATE("Applicable Credit Interest Rate"),
    @XmlEnumValue("Applicable Debit Interest Rate")
    APPLICABLE_DEBIT_INTEREST_RATE("Applicable Debit Interest Rate"),
    @XmlEnumValue("Financial Services Agreement Rate")
    FINANCIAL_SERVICES_AGREEMENT_RATE("Financial Services Agreement Rate");
    private final String value;

    RateRoleInFsaTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RateRoleInFsaTypeName fromValue(String v) {
        for (RateRoleInFsaTypeName c: RateRoleInFsaTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
