
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for IndexableCurrencyAmount_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IndexableCurrencyAmount_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="baseAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="baseIndex" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexValue_TO" minOccurs="0"/>
 *         &lt;element name="baseAmountIndexValueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="amountValues" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexableCurrencyAmountValue_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndexableCurrencyAmount_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "baseAmount",
    "baseIndex",
    "baseAmountIndexValueDate",
    "amountValues"
})
public class IndexableCurrencyAmountTO
    extends BaseTransferObject
{

    protected BaseCurrencyAmount baseAmount;
    protected IndexValueTO baseIndex;
    protected XMLGregorianCalendar baseAmountIndexValueDate;
    protected List<IndexableCurrencyAmountValueTO> amountValues;

    /**
     * Gets the value of the baseAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getBaseAmount() {
        return baseAmount;
    }

    /**
     * Sets the value of the baseAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setBaseAmount(BaseCurrencyAmount value) {
        this.baseAmount = value;
    }

    /**
     * Gets the value of the baseIndex property.
     * 
     * @return
     *     possible object is
     *     {@link IndexValueTO }
     *     
     */
    public IndexValueTO getBaseIndex() {
        return baseIndex;
    }

    /**
     * Sets the value of the baseIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexValueTO }
     *     
     */
    public void setBaseIndex(IndexValueTO value) {
        this.baseIndex = value;
    }

    /**
     * Gets the value of the baseAmountIndexValueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBaseAmountIndexValueDate() {
        return baseAmountIndexValueDate;
    }

    /**
     * Sets the value of the baseAmountIndexValueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBaseAmountIndexValueDate(XMLGregorianCalendar value) {
        this.baseAmountIndexValueDate = value;
    }

    /**
     * Gets the value of the amountValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the amountValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAmountValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IndexableCurrencyAmountValueTO }
     * 
     * 
     */
    public List<IndexableCurrencyAmountValueTO> getAmountValues() {
        if (amountValues == null) {
            amountValues = new ArrayList<IndexableCurrencyAmountValueTO>();
        }
        return this.amountValues;
    }

    /**
     * Sets the value of the amountValues property.
     * 
     * @param amountValues
     *     allowed object is
     *     {@link IndexableCurrencyAmountValueTO }
     *     
     */
    public void setAmountValues(List<IndexableCurrencyAmountValueTO> amountValues) {
        this.amountValues = amountValues;
    }

}
