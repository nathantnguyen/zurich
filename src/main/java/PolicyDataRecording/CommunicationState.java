
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunicationState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Abandoned"/>
 *     &lt;enumeration value="Cancelled"/>
 *     &lt;enumeration value="Declined"/>
 *     &lt;enumeration value="Draft"/>
 *     &lt;enumeration value="End Incoming Communication"/>
 *     &lt;enumeration value="End Interactive Communication"/>
 *     &lt;enumeration value="End Outgoing Communication"/>
 *     &lt;enumeration value="Ended"/>
 *     &lt;enumeration value="Failed"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Invited"/>
 *     &lt;enumeration value="Missed"/>
 *     &lt;enumeration value="Ready"/>
 *     &lt;enumeration value="Receipt Confirmed"/>
 *     &lt;enumeration value="Received"/>
 *     &lt;enumeration value="Receiving"/>
 *     &lt;enumeration value="Receiving Failed"/>
 *     &lt;enumeration value="Scheduled"/>
 *     &lt;enumeration value="Sending"/>
 *     &lt;enumeration value="Sent"/>
 *     &lt;enumeration value="Started"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommunicationState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Communication/CommunicationEnumerationsAndStates/")
@XmlEnum
public enum CommunicationState {

    @XmlEnumValue("Abandoned")
    ABANDONED("Abandoned"),
    @XmlEnumValue("Cancelled")
    CANCELLED("Cancelled"),
    @XmlEnumValue("Declined")
    DECLINED("Declined"),
    @XmlEnumValue("Draft")
    DRAFT("Draft"),
    @XmlEnumValue("End Incoming Communication")
    END_INCOMING_COMMUNICATION("End Incoming Communication"),
    @XmlEnumValue("End Interactive Communication")
    END_INTERACTIVE_COMMUNICATION("End Interactive Communication"),
    @XmlEnumValue("End Outgoing Communication")
    END_OUTGOING_COMMUNICATION("End Outgoing Communication"),
    @XmlEnumValue("Ended")
    ENDED("Ended"),
    @XmlEnumValue("Failed")
    FAILED("Failed"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Invited")
    INVITED("Invited"),
    @XmlEnumValue("Missed")
    MISSED("Missed"),
    @XmlEnumValue("Ready")
    READY("Ready"),
    @XmlEnumValue("Receipt Confirmed")
    RECEIPT_CONFIRMED("Receipt Confirmed"),
    @XmlEnumValue("Received")
    RECEIVED("Received"),
    @XmlEnumValue("Receiving")
    RECEIVING("Receiving"),
    @XmlEnumValue("Receiving Failed")
    RECEIVING_FAILED("Receiving Failed"),
    @XmlEnumValue("Scheduled")
    SCHEDULED("Scheduled"),
    @XmlEnumValue("Sending")
    SENDING("Sending"),
    @XmlEnumValue("Sent")
    SENT("Sent"),
    @XmlEnumValue("Started")
    STARTED("Started");
    private final String value;

    CommunicationState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationState fromValue(String v) {
        for (CommunicationState c: CommunicationState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
