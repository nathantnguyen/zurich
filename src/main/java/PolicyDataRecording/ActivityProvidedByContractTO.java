
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityProvidedByContract_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityProvidedByContract_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInContract_TO">
 *       &lt;sequence>
 *         &lt;element name="insuredQuantity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityProvidedByContract_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "insuredQuantity"
})
public class ActivityProvidedByContractTO
    extends RoleInContractTO
{

    protected Amount insuredQuantity;

    /**
     * Gets the value of the insuredQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getInsuredQuantity() {
        return insuredQuantity;
    }

    /**
     * Sets the value of the insuredQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setInsuredQuantity(Amount value) {
        this.insuredQuantity = value;
    }

}
