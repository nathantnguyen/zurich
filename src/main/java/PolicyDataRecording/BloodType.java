
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BloodType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BloodType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Ab Plus"/>
 *     &lt;enumeration value="A Plus"/>
 *     &lt;enumeration value="Ab"/>
 *     &lt;enumeration value="B Minus"/>
 *     &lt;enumeration value="B Plus"/>
 *     &lt;enumeration value="O"/>
 *     &lt;enumeration value="O Plus"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BloodType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum BloodType {

    @XmlEnumValue("Ab Plus")
    AB_PLUS("Ab Plus"),
    @XmlEnumValue("A Plus")
    A_PLUS("A Plus"),
    @XmlEnumValue("Ab")
    AB("Ab"),
    @XmlEnumValue("B Minus")
    B_MINUS("B Minus"),
    @XmlEnumValue("B Plus")
    B_PLUS("B Plus"),
    O("O"),
    @XmlEnumValue("O Plus")
    O_PLUS("O Plus");
    private final String value;

    BloodType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BloodType fromValue(String v) {
        for (BloodType c: BloodType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
