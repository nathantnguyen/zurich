
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyInvolvedInEmploymentAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyInvolvedInEmploymentAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}RoleInEmploymentAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="employmentRole" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}EmploymentRole_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyInvolvedInEmploymentAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "employmentRole"
})
public class PartyInvolvedInEmploymentAgreementTO
    extends RoleInEmploymentAgreementTO
{

    protected EmploymentRoleTO employmentRole;

    /**
     * Gets the value of the employmentRole property.
     * 
     * @return
     *     possible object is
     *     {@link EmploymentRoleTO }
     *     
     */
    public EmploymentRoleTO getEmploymentRole() {
        return employmentRole;
    }

    /**
     * Sets the value of the employmentRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmploymentRoleTO }
     *     
     */
    public void setEmploymentRole(EmploymentRoleTO value) {
        this.employmentRole = value;
    }

}
