
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MainInsuredRelation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MainInsuredRelation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Branch"/>
 *     &lt;enumeration value="Fully Owned Subsidiary"/>
 *     &lt;enumeration value="Regional Unit"/>
 *     &lt;enumeration value="Subcontractor"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MainInsuredRelation", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum MainInsuredRelation {

    @XmlEnumValue("Branch")
    BRANCH("Branch"),
    @XmlEnumValue("Fully Owned Subsidiary")
    FULLY_OWNED_SUBSIDIARY("Fully Owned Subsidiary"),
    @XmlEnumValue("Regional Unit")
    REGIONAL_UNIT("Regional Unit"),
    @XmlEnumValue("Subcontractor")
    SUBCONTRACTOR("Subcontractor");
    private final String value;

    MainInsuredRelation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MainInsuredRelation fromValue(String v) {
        for (MainInsuredRelation c: MainInsuredRelation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
