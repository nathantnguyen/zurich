
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DerivativeType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DerivativeType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/}DerivativeTypeName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DerivativeType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "name"
})
public class DerivativeTypeTO
    extends TypeTO
{

    protected DerivativeTypeName name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link DerivativeTypeName }
     *     
     */
    public DerivativeTypeName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link DerivativeTypeName }
     *     
     */
    public void setName(DerivativeTypeName value) {
        this.name = value;
    }

}
