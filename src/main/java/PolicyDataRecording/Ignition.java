
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Ignition.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Ignition">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Electronically Controlled Spark Ignition"/>
 *     &lt;enumeration value="Heat Ignition"/>
 *     &lt;enumeration value="Mechanically Controlled Spark Ignition"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Ignition", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum Ignition {

    @XmlEnumValue("Electronically Controlled Spark Ignition")
    ELECTRONICALLY_CONTROLLED_SPARK_IGNITION("Electronically Controlled Spark Ignition"),
    @XmlEnumValue("Heat Ignition")
    HEAT_IGNITION("Heat Ignition"),
    @XmlEnumValue("Mechanically Controlled Spark Ignition")
    MECHANICALLY_CONTROLLED_SPARK_IGNITION("Mechanically Controlled Spark Ignition");
    private final String value;

    Ignition(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Ignition fromValue(String v) {
        for (Ignition c: Ignition.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
