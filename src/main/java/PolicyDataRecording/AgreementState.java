
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgreementState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AgreementState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Application"/>
 *     &lt;enumeration value="Issued"/>
 *     &lt;enumeration value="Issued Pending Payment"/>
 *     &lt;enumeration value="Not Taken Up"/>
 *     &lt;enumeration value="Offered"/>
 *     &lt;enumeration value="Proposed"/>
 *     &lt;enumeration value="Rejected"/>
 *     &lt;enumeration value="Suspended"/>
 *     &lt;enumeration value="Terminated"/>
 *     &lt;enumeration value="Cancelled"/>
 *     &lt;enumeration value="Effective"/>
 *     &lt;enumeration value="In Force"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AgreementState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/")
@XmlEnum
public enum AgreementState {

    @XmlEnumValue("Application")
    APPLICATION("Application"),
    @XmlEnumValue("Issued")
    ISSUED("Issued"),
    @XmlEnumValue("Issued Pending Payment")
    ISSUED_PENDING_PAYMENT("Issued Pending Payment"),
    @XmlEnumValue("Not Taken Up")
    NOT_TAKEN_UP("Not Taken Up"),
    @XmlEnumValue("Offered")
    OFFERED("Offered"),
    @XmlEnumValue("Proposed")
    PROPOSED("Proposed"),
    @XmlEnumValue("Rejected")
    REJECTED("Rejected"),
    @XmlEnumValue("Suspended")
    SUSPENDED("Suspended"),
    @XmlEnumValue("Terminated")
    TERMINATED("Terminated"),
    @XmlEnumValue("Cancelled")
    CANCELLED("Cancelled"),
    @XmlEnumValue("Effective")
    EFFECTIVE("Effective"),
    @XmlEnumValue("In Force")
    IN_FORCE("In Force");
    private final String value;

    AgreementState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AgreementState fromValue(String v) {
        for (AgreementState c: AgreementState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
