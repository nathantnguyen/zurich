
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Training_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Training_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ActivityOccurrence_TO">
 *       &lt;sequence>
 *         &lt;element name="courseLanguage" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}Language" minOccurs="0"/>
 *         &lt;element name="formatOfTraining" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}FormatOfTraining" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Training_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "courseLanguage",
    "formatOfTraining"
})
public class TrainingTO
    extends ActivityOccurrenceTO
{

    protected Language courseLanguage;
    protected FormatOfTraining formatOfTraining;

    /**
     * Gets the value of the courseLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link Language }
     *     
     */
    public Language getCourseLanguage() {
        return courseLanguage;
    }

    /**
     * Sets the value of the courseLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Language }
     *     
     */
    public void setCourseLanguage(Language value) {
        this.courseLanguage = value;
    }

    /**
     * Gets the value of the formatOfTraining property.
     * 
     * @return
     *     possible object is
     *     {@link FormatOfTraining }
     *     
     */
    public FormatOfTraining getFormatOfTraining() {
        return formatOfTraining;
    }

    /**
     * Sets the value of the formatOfTraining property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatOfTraining }
     *     
     */
    public void setFormatOfTraining(FormatOfTraining value) {
        this.formatOfTraining = value;
    }

}
