
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskModelSourceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RiskModelSourceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Internal Model"/>
 *     &lt;enumeration value="Regulatory"/>
 *     &lt;enumeration value="Third Party Model"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RiskModelSourceType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/")
@XmlEnum
public enum RiskModelSourceType {

    @XmlEnumValue("Internal Model")
    INTERNAL_MODEL("Internal Model"),
    @XmlEnumValue("Regulatory")
    REGULATORY("Regulatory"),
    @XmlEnumValue("Third Party Model")
    THIRD_PARTY_MODEL("Third Party Model");
    private final String value;

    RiskModelSourceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RiskModelSourceType fromValue(String v) {
        for (RiskModelSourceType c: RiskModelSourceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
