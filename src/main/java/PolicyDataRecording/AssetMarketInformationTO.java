
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssetMarketInformation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssetMarketInformation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}AssetPrice_TO">
 *       &lt;sequence>
 *         &lt;element name="assetVolatilityMeasure" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="highestPrice" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="knockStrikePriceHigh" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="knockStrikePriceLow" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="lowestPrice" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="expenseRatio" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="turnoverRatio" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="yield" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssetMarketInformation_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "assetVolatilityMeasure",
    "highestPrice",
    "knockStrikePriceHigh",
    "knockStrikePriceLow",
    "lowestPrice",
    "expenseRatio",
    "turnoverRatio",
    "yield"
})
public class AssetMarketInformationTO
    extends AssetPriceTO
{

    protected BigInteger assetVolatilityMeasure;
    protected BaseCurrencyAmount highestPrice;
    protected BigInteger knockStrikePriceHigh;
    protected BaseCurrencyAmount knockStrikePriceLow;
    protected BaseCurrencyAmount lowestPrice;
    protected BigDecimal expenseRatio;
    protected BigDecimal turnoverRatio;
    protected BigDecimal yield;

    /**
     * Gets the value of the assetVolatilityMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAssetVolatilityMeasure() {
        return assetVolatilityMeasure;
    }

    /**
     * Sets the value of the assetVolatilityMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAssetVolatilityMeasure(BigInteger value) {
        this.assetVolatilityMeasure = value;
    }

    /**
     * Gets the value of the highestPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getHighestPrice() {
        return highestPrice;
    }

    /**
     * Sets the value of the highestPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setHighestPrice(BaseCurrencyAmount value) {
        this.highestPrice = value;
    }

    /**
     * Gets the value of the knockStrikePriceHigh property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getKnockStrikePriceHigh() {
        return knockStrikePriceHigh;
    }

    /**
     * Sets the value of the knockStrikePriceHigh property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setKnockStrikePriceHigh(BigInteger value) {
        this.knockStrikePriceHigh = value;
    }

    /**
     * Gets the value of the knockStrikePriceLow property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getKnockStrikePriceLow() {
        return knockStrikePriceLow;
    }

    /**
     * Sets the value of the knockStrikePriceLow property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setKnockStrikePriceLow(BaseCurrencyAmount value) {
        this.knockStrikePriceLow = value;
    }

    /**
     * Gets the value of the lowestPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getLowestPrice() {
        return lowestPrice;
    }

    /**
     * Sets the value of the lowestPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setLowestPrice(BaseCurrencyAmount value) {
        this.lowestPrice = value;
    }

    /**
     * Gets the value of the expenseRatio property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExpenseRatio() {
        return expenseRatio;
    }

    /**
     * Sets the value of the expenseRatio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExpenseRatio(BigDecimal value) {
        this.expenseRatio = value;
    }

    /**
     * Gets the value of the turnoverRatio property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTurnoverRatio() {
        return turnoverRatio;
    }

    /**
     * Sets the value of the turnoverRatio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTurnoverRatio(BigDecimal value) {
        this.turnoverRatio = value;
    }

    /**
     * Gets the value of the yield property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getYield() {
        return yield;
    }

    /**
     * Sets the value of the yield property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setYield(BigDecimal value) {
        this.yield = value;
    }

}
