
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MoneyScheduler_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyScheduler_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="anniversaryDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="firstDueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="adjustments" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Adjustment_TO" minOccurs="0"/>
 *         &lt;element name="targetAccount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Account_TO" minOccurs="0"/>
 *         &lt;element name="sourceAccount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Account_TO" minOccurs="0"/>
 *         &lt;element name="paymentMethod" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}PaymentMethod_TO" minOccurs="0"/>
 *         &lt;element name="receiver" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="payer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="allocatedFinancialAssets" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nextDueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="moneySchedulerRoles" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneySchedulerRole_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyScheduler_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "frequency",
    "anniversaryDate",
    "firstDueDate",
    "description",
    "startDate",
    "endDate",
    "adjustments",
    "targetAccount",
    "sourceAccount",
    "paymentMethod",
    "receiver",
    "payer",
    "allocatedFinancialAssets",
    "nextDueDate",
    "moneySchedulerRoles"
})
@XmlSeeAlso({
    MoneyInSchedulerTO.class,
    MoneyOutSchedulerTO.class
})
public class MoneySchedulerTO
    extends BusinessModelObjectTO
{

    protected Frequency frequency;
    protected XMLGregorianCalendar anniversaryDate;
    protected XMLGregorianCalendar firstDueDate;
    protected String description;
    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected AdjustmentTO adjustments;
    protected AccountTO targetAccount;
    protected AccountTO sourceAccount;
    protected PaymentMethodTO paymentMethod;
    protected RolePlayerTO receiver;
    protected RolePlayerTO payer;
    protected List<FinancialAssetTO> allocatedFinancialAssets;
    protected XMLGregorianCalendar nextDueDate;
    protected List<MoneySchedulerRoleTO> moneySchedulerRoles;

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the anniversaryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAnniversaryDate() {
        return anniversaryDate;
    }

    /**
     * Sets the value of the anniversaryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAnniversaryDate(XMLGregorianCalendar value) {
        this.anniversaryDate = value;
    }

    /**
     * Gets the value of the firstDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstDueDate() {
        return firstDueDate;
    }

    /**
     * Sets the value of the firstDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstDueDate(XMLGregorianCalendar value) {
        this.firstDueDate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the adjustments property.
     * 
     * @return
     *     possible object is
     *     {@link AdjustmentTO }
     *     
     */
    public AdjustmentTO getAdjustments() {
        return adjustments;
    }

    /**
     * Sets the value of the adjustments property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdjustmentTO }
     *     
     */
    public void setAdjustments(AdjustmentTO value) {
        this.adjustments = value;
    }

    /**
     * Gets the value of the targetAccount property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTO }
     *     
     */
    public AccountTO getTargetAccount() {
        return targetAccount;
    }

    /**
     * Sets the value of the targetAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTO }
     *     
     */
    public void setTargetAccount(AccountTO value) {
        this.targetAccount = value;
    }

    /**
     * Gets the value of the sourceAccount property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTO }
     *     
     */
    public AccountTO getSourceAccount() {
        return sourceAccount;
    }

    /**
     * Sets the value of the sourceAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTO }
     *     
     */
    public void setSourceAccount(AccountTO value) {
        this.sourceAccount = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMethodTO }
     *     
     */
    public PaymentMethodTO getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMethodTO }
     *     
     */
    public void setPaymentMethod(PaymentMethodTO value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setReceiver(RolePlayerTO value) {
        this.receiver = value;
    }

    /**
     * Gets the value of the payer property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getPayer() {
        return payer;
    }

    /**
     * Sets the value of the payer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setPayer(RolePlayerTO value) {
        this.payer = value;
    }

    /**
     * Gets the value of the allocatedFinancialAssets property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allocatedFinancialAssets property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocatedFinancialAssets().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FinancialAssetTO }
     * 
     * 
     */
    public List<FinancialAssetTO> getAllocatedFinancialAssets() {
        if (allocatedFinancialAssets == null) {
            allocatedFinancialAssets = new ArrayList<FinancialAssetTO>();
        }
        return this.allocatedFinancialAssets;
    }

    /**
     * Gets the value of the nextDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextDueDate() {
        return nextDueDate;
    }

    /**
     * Sets the value of the nextDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextDueDate(XMLGregorianCalendar value) {
        this.nextDueDate = value;
    }

    /**
     * Gets the value of the moneySchedulerRoles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moneySchedulerRoles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoneySchedulerRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneySchedulerRoleTO }
     * 
     * 
     */
    public List<MoneySchedulerRoleTO> getMoneySchedulerRoles() {
        if (moneySchedulerRoles == null) {
            moneySchedulerRoles = new ArrayList<MoneySchedulerRoleTO>();
        }
        return this.moneySchedulerRoles;
    }

    /**
     * Sets the value of the allocatedFinancialAssets property.
     * 
     * @param allocatedFinancialAssets
     *     allowed object is
     *     {@link FinancialAssetTO }
     *     
     */
    public void setAllocatedFinancialAssets(List<FinancialAssetTO> allocatedFinancialAssets) {
        this.allocatedFinancialAssets = allocatedFinancialAssets;
    }

    /**
     * Sets the value of the moneySchedulerRoles property.
     * 
     * @param moneySchedulerRoles
     *     allowed object is
     *     {@link MoneySchedulerRoleTO }
     *     
     */
    public void setMoneySchedulerRoles(List<MoneySchedulerRoleTO> moneySchedulerRoles) {
        this.moneySchedulerRoles = moneySchedulerRoles;
    }

}
