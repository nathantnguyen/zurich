
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StructuredProduct_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StructuredProduct_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="attachmentPoint" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="capitalProtection" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="detachmentPoint" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="executionOption" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="prepaymentPossibleIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuredProduct_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "attachmentPoint",
    "capitalProtection",
    "detachmentPoint",
    "executionOption",
    "prepaymentPossibleIndicator"
})
public class StructuredProductTO
    extends FinancialAssetTO
{

    protected BigDecimal attachmentPoint;
    protected String capitalProtection;
    protected BigDecimal detachmentPoint;
    protected String executionOption;
    protected Boolean prepaymentPossibleIndicator;

    /**
     * Gets the value of the attachmentPoint property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAttachmentPoint() {
        return attachmentPoint;
    }

    /**
     * Sets the value of the attachmentPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAttachmentPoint(BigDecimal value) {
        this.attachmentPoint = value;
    }

    /**
     * Gets the value of the capitalProtection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapitalProtection() {
        return capitalProtection;
    }

    /**
     * Sets the value of the capitalProtection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapitalProtection(String value) {
        this.capitalProtection = value;
    }

    /**
     * Gets the value of the detachmentPoint property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDetachmentPoint() {
        return detachmentPoint;
    }

    /**
     * Sets the value of the detachmentPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDetachmentPoint(BigDecimal value) {
        this.detachmentPoint = value;
    }

    /**
     * Gets the value of the executionOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutionOption() {
        return executionOption;
    }

    /**
     * Sets the value of the executionOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutionOption(String value) {
        this.executionOption = value;
    }

    /**
     * Gets the value of the prepaymentPossibleIndicator property.
     * This getter has been renamed from isPrepaymentPossibleIndicator() to getPrepaymentPossibleIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPrepaymentPossibleIndicator() {
        return prepaymentPossibleIndicator;
    }

    /**
     * Sets the value of the prepaymentPossibleIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrepaymentPossibleIndicator(Boolean value) {
        this.prepaymentPossibleIndicator = value;
    }

}
