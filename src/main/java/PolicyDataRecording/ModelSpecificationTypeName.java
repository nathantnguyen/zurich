
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModelSpecificationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ModelSpecificationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Manufactured Part Model"/>
 *     &lt;enumeration value="Trim Level Option"/>
 *     &lt;enumeration value="Additional Option"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ModelSpecificationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum ModelSpecificationTypeName {

    @XmlEnumValue("Manufactured Part Model")
    MANUFACTURED_PART_MODEL("Manufactured Part Model"),
    @XmlEnumValue("Trim Level Option")
    TRIM_LEVEL_OPTION("Trim Level Option"),
    @XmlEnumValue("Additional Option")
    ADDITIONAL_OPTION("Additional Option");
    private final String value;

    ModelSpecificationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ModelSpecificationTypeName fromValue(String v) {
        for (ModelSpecificationTypeName c: ModelSpecificationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
