
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SuspectDuplicateStatus_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SuspectDuplicateStatus_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}StatusWithCommonReason_TO">
 *       &lt;sequence>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}SuspectDuplicateState" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SuspectDuplicateStatus_TO", propOrder = {
    "state"
})
public class SuspectDuplicateStatusTO
    extends StatusWithCommonReasonTO
{

    protected SuspectDuplicateState state;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link SuspectDuplicateState }
     *     
     */
    public SuspectDuplicateState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link SuspectDuplicateState }
     *     
     */
    public void setState(SuspectDuplicateState value) {
        this.state = value;
    }

}
