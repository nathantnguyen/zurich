
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InsurancePolicy_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsurancePolicy_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}TopLevelFinancialServicesAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="entryCode" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}EntryCode" minOccurs="0"/>
 *         &lt;element name="renewal" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="taxQualification" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}TaxQualification" minOccurs="0"/>
 *         &lt;element name="sectorType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}InsurancePolicyStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="premiumsPaidSinceLastAnniversary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="premiumsPaidInPreviousAnniversaryYear" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="maximumPolicyLoanAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="minimumPremium" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="cumulativePremiumAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="nextPaymentDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="lastPaymentDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="paidToDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="lastPayment" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Payment_TO" minOccurs="0"/>
 *         &lt;element name="sourceExternalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="sourceVersion" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="cedentProducingPlace" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsurancePolicy_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "entryCode",
    "renewal",
    "taxQualification",
    "sectorType",
    "status",
    "premiumsPaidSinceLastAnniversary",
    "premiumsPaidInPreviousAnniversaryYear",
    "maximumPolicyLoanAmount",
    "minimumPremium",
    "cumulativePremiumAmount",
    "nextPaymentDate",
    "lastPaymentDate",
    "paidToDate",
    "lastPayment",
    "sourceExternalReference",
    "sourceVersion",
    "cedentProducingPlace"
})
@XmlSeeAlso({
    CommercialAgreementTO.class,
    IndividualAgreementTO.class,
    GroupAgreementTO.class
})
public class InsurancePolicyTO
    extends TopLevelFinancialServicesAgreementTO
{

    protected EntryCode entryCode;
    protected Boolean renewal;
    protected String taxQualification;
    protected String sectorType;
    protected List<InsurancePolicyStatusTO> status;
    protected BaseCurrencyAmount premiumsPaidSinceLastAnniversary;
    protected BaseCurrencyAmount premiumsPaidInPreviousAnniversaryYear;
    protected BaseCurrencyAmount maximumPolicyLoanAmount;
    protected BaseCurrencyAmount minimumPremium;
    protected BaseCurrencyAmount cumulativePremiumAmount;
    protected XMLGregorianCalendar nextPaymentDate;
    protected XMLGregorianCalendar lastPaymentDate;
    protected XMLGregorianCalendar paidToDate;
    protected PaymentTO lastPayment;
    protected String sourceExternalReference;
    protected String sourceVersion;
    protected PlaceTO cedentProducingPlace;

    /**
     * Gets the value of the entryCode property.
     * 
     * @return
     *     possible object is
     *     {@link EntryCode }
     *     
     */
    public EntryCode getEntryCode() {
        return entryCode;
    }

    /**
     * Sets the value of the entryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntryCode }
     *     
     */
    public void setEntryCode(EntryCode value) {
        this.entryCode = value;
    }

    /**
     * Gets the value of the renewal property.
     * This getter has been renamed from isRenewal() to getRenewal() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRenewal() {
        return renewal;
    }

    /**
     * Sets the value of the renewal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRenewal(Boolean value) {
        this.renewal = value;
    }

    /**
     * Gets the value of the taxQualification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxQualification() {
        return taxQualification;
    }

    /**
     * Sets the value of the taxQualification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxQualification(String value) {
        this.taxQualification = value;
    }

    /**
     * Gets the value of the sectorType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSectorType() {
        return sectorType;
    }

    /**
     * Sets the value of the sectorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSectorType(String value) {
        this.sectorType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InsurancePolicyStatusTO }
     * 
     * 
     */
    public List<InsurancePolicyStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<InsurancePolicyStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the premiumsPaidSinceLastAnniversary property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getPremiumsPaidSinceLastAnniversary() {
        return premiumsPaidSinceLastAnniversary;
    }

    /**
     * Sets the value of the premiumsPaidSinceLastAnniversary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setPremiumsPaidSinceLastAnniversary(BaseCurrencyAmount value) {
        this.premiumsPaidSinceLastAnniversary = value;
    }

    /**
     * Gets the value of the premiumsPaidInPreviousAnniversaryYear property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getPremiumsPaidInPreviousAnniversaryYear() {
        return premiumsPaidInPreviousAnniversaryYear;
    }

    /**
     * Sets the value of the premiumsPaidInPreviousAnniversaryYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setPremiumsPaidInPreviousAnniversaryYear(BaseCurrencyAmount value) {
        this.premiumsPaidInPreviousAnniversaryYear = value;
    }

    /**
     * Gets the value of the maximumPolicyLoanAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMaximumPolicyLoanAmount() {
        return maximumPolicyLoanAmount;
    }

    /**
     * Sets the value of the maximumPolicyLoanAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMaximumPolicyLoanAmount(BaseCurrencyAmount value) {
        this.maximumPolicyLoanAmount = value;
    }

    /**
     * Gets the value of the minimumPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMinimumPremium() {
        return minimumPremium;
    }

    /**
     * Sets the value of the minimumPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMinimumPremium(BaseCurrencyAmount value) {
        this.minimumPremium = value;
    }

    /**
     * Gets the value of the cumulativePremiumAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCumulativePremiumAmount() {
        return cumulativePremiumAmount;
    }

    /**
     * Sets the value of the cumulativePremiumAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCumulativePremiumAmount(BaseCurrencyAmount value) {
        this.cumulativePremiumAmount = value;
    }

    /**
     * Gets the value of the nextPaymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextPaymentDate() {
        return nextPaymentDate;
    }

    /**
     * Sets the value of the nextPaymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextPaymentDate(XMLGregorianCalendar value) {
        this.nextPaymentDate = value;
    }

    /**
     * Gets the value of the lastPaymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastPaymentDate() {
        return lastPaymentDate;
    }

    /**
     * Sets the value of the lastPaymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastPaymentDate(XMLGregorianCalendar value) {
        this.lastPaymentDate = value;
    }

    /**
     * Gets the value of the paidToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaidToDate() {
        return paidToDate;
    }

    /**
     * Sets the value of the paidToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaidToDate(XMLGregorianCalendar value) {
        this.paidToDate = value;
    }

    /**
     * Gets the value of the lastPayment property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTO }
     *     
     */
    public PaymentTO getLastPayment() {
        return lastPayment;
    }

    /**
     * Sets the value of the lastPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTO }
     *     
     */
    public void setLastPayment(PaymentTO value) {
        this.lastPayment = value;
    }

    /**
     * Gets the value of the sourceExternalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceExternalReference() {
        return sourceExternalReference;
    }

    /**
     * Sets the value of the sourceExternalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceExternalReference(String value) {
        this.sourceExternalReference = value;
    }

    /**
     * Gets the value of the sourceVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceVersion() {
        return sourceVersion;
    }

    /**
     * Sets the value of the sourceVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceVersion(String value) {
        this.sourceVersion = value;
    }

    /**
     * Gets the value of the cedentProducingPlace property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getCedentProducingPlace() {
        return cedentProducingPlace;
    }

    /**
     * Sets the value of the cedentProducingPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setCedentProducingPlace(PlaceTO value) {
        this.cedentProducingPlace = value;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link InsurancePolicyStatusTO }
     *     
     */
    public void setStatus(List<InsurancePolicyStatusTO> status) {
        this.status = status;
    }

}
