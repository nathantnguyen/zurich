
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CensusStreetSegment_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CensusStreetSegment_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO">
 *       &lt;sequence>
 *         &lt;element name="houseNumberLow" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="segmentParity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}StreetSegmentParity" minOccurs="0"/>
 *         &lt;element name="segmentDirection" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}StreetSegmentDirection" minOccurs="0"/>
 *         &lt;element name="blockSuffix" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}BlockSuffix" minOccurs="0"/>
 *         &lt;element name="houseNumberHigh" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="streetSide" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}StreetSide" minOccurs="0"/>
 *         &lt;element name="roadClass" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}StreetRoadClass" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CensusStreetSegment_TO", propOrder = {
    "houseNumberLow",
    "segmentParity",
    "segmentDirection",
    "blockSuffix",
    "houseNumberHigh",
    "length",
    "streetSide",
    "roadClass"
})
public class CensusStreetSegmentTO
    extends PlaceTO
{

    protected String houseNumberLow;
    protected StreetSegmentParity segmentParity;
    protected StreetSegmentDirection segmentDirection;
    protected BlockSuffix blockSuffix;
    protected String houseNumberHigh;
    protected Amount length;
    protected StreetSide streetSide;
    protected StreetRoadClass roadClass;

    /**
     * Gets the value of the houseNumberLow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseNumberLow() {
        return houseNumberLow;
    }

    /**
     * Sets the value of the houseNumberLow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseNumberLow(String value) {
        this.houseNumberLow = value;
    }

    /**
     * Gets the value of the segmentParity property.
     * 
     * @return
     *     possible object is
     *     {@link StreetSegmentParity }
     *     
     */
    public StreetSegmentParity getSegmentParity() {
        return segmentParity;
    }

    /**
     * Sets the value of the segmentParity property.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetSegmentParity }
     *     
     */
    public void setSegmentParity(StreetSegmentParity value) {
        this.segmentParity = value;
    }

    /**
     * Gets the value of the segmentDirection property.
     * 
     * @return
     *     possible object is
     *     {@link StreetSegmentDirection }
     *     
     */
    public StreetSegmentDirection getSegmentDirection() {
        return segmentDirection;
    }

    /**
     * Sets the value of the segmentDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetSegmentDirection }
     *     
     */
    public void setSegmentDirection(StreetSegmentDirection value) {
        this.segmentDirection = value;
    }

    /**
     * Gets the value of the blockSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link BlockSuffix }
     *     
     */
    public BlockSuffix getBlockSuffix() {
        return blockSuffix;
    }

    /**
     * Sets the value of the blockSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockSuffix }
     *     
     */
    public void setBlockSuffix(BlockSuffix value) {
        this.blockSuffix = value;
    }

    /**
     * Gets the value of the houseNumberHigh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseNumberHigh() {
        return houseNumberHigh;
    }

    /**
     * Sets the value of the houseNumberHigh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseNumberHigh(String value) {
        this.houseNumberHigh = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setLength(Amount value) {
        this.length = value;
    }

    /**
     * Gets the value of the streetSide property.
     * 
     * @return
     *     possible object is
     *     {@link StreetSide }
     *     
     */
    public StreetSide getStreetSide() {
        return streetSide;
    }

    /**
     * Sets the value of the streetSide property.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetSide }
     *     
     */
    public void setStreetSide(StreetSide value) {
        this.streetSide = value;
    }

    /**
     * Gets the value of the roadClass property.
     * 
     * @return
     *     possible object is
     *     {@link StreetRoadClass }
     *     
     */
    public StreetRoadClass getRoadClass() {
        return roadClass;
    }

    /**
     * Sets the value of the roadClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetRoadClass }
     *     
     */
    public void setRoadClass(StreetRoadClass value) {
        this.roadClass = value;
    }

}
