
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActuarialStatisticsTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActuarialStatisticsTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Economic Assumptions For Valuations"/>
 *     &lt;enumeration value="Lapsation Propensity Assumptions"/>
 *     &lt;enumeration value="Life Expectancy"/>
 *     &lt;enumeration value="Life Expectancy Assumptions"/>
 *     &lt;enumeration value="Morbidity"/>
 *     &lt;enumeration value="Mortality"/>
 *     &lt;enumeration value="Mortality Assumptions"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActuarialStatisticsTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Rating/RatingEnumerationsAndStates/")
@XmlEnum
public enum ActuarialStatisticsTypeName {

    @XmlEnumValue("Economic Assumptions For Valuations")
    ECONOMIC_ASSUMPTIONS_FOR_VALUATIONS("Economic Assumptions For Valuations"),
    @XmlEnumValue("Lapsation Propensity Assumptions")
    LAPSATION_PROPENSITY_ASSUMPTIONS("Lapsation Propensity Assumptions"),
    @XmlEnumValue("Life Expectancy")
    LIFE_EXPECTANCY("Life Expectancy"),
    @XmlEnumValue("Life Expectancy Assumptions")
    LIFE_EXPECTANCY_ASSUMPTIONS("Life Expectancy Assumptions"),
    @XmlEnumValue("Morbidity")
    MORBIDITY("Morbidity"),
    @XmlEnumValue("Mortality")
    MORTALITY("Mortality"),
    @XmlEnumValue("Mortality Assumptions")
    MORTALITY_ASSUMPTIONS("Mortality Assumptions");
    private final String value;

    ActuarialStatisticsTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActuarialStatisticsTypeName fromValue(String v) {
        for (ActuarialStatisticsTypeName c: ActuarialStatisticsTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
