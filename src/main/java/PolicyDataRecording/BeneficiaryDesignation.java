
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BeneficiaryDesignation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BeneficiaryDesignation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="By Name"/>
 *     &lt;enumeration value="Children Of Insured"/>
 *     &lt;enumeration value="Domestic Partner"/>
 *     &lt;enumeration value="Insured"/>
 *     &lt;enumeration value="Spouse"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BeneficiaryDesignation", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum BeneficiaryDesignation {

    @XmlEnumValue("By Name")
    BY_NAME("By Name"),
    @XmlEnumValue("Children Of Insured")
    CHILDREN_OF_INSURED("Children Of Insured"),
    @XmlEnumValue("Domestic Partner")
    DOMESTIC_PARTNER("Domestic Partner"),
    @XmlEnumValue("Insured")
    INSURED("Insured"),
    @XmlEnumValue("Spouse")
    SPOUSE("Spouse");
    private final String value;

    BeneficiaryDesignation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BeneficiaryDesignation fromValue(String v) {
        for (BeneficiaryDesignation c: BeneficiaryDesignation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
