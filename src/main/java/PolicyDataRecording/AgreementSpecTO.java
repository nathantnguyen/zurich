
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgreementSpec_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgreementSpec_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}Specification_TO">
 *       &lt;sequence>
 *         &lt;element name="versions" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementSpecDefCriteria_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="specConceptComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementSpecConcept_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/}AgreementSpecStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgreementSpec_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/GenericAgreement/", propOrder = {
    "versions",
    "specConceptComponents",
    "status"
})
public class AgreementSpecTO
    extends SpecificationTO
{

    protected List<AgreementSpecDefCriteriaTO> versions;
    protected List<AgreementSpecConceptTO> specConceptComponents;
    protected List<AgreementSpecStatusTO> status;

    /**
     * Gets the value of the versions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the versions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVersions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgreementSpecDefCriteriaTO }
     * 
     * 
     */
    public List<AgreementSpecDefCriteriaTO> getVersions() {
        if (versions == null) {
            versions = new ArrayList<AgreementSpecDefCriteriaTO>();
        }
        return this.versions;
    }

    /**
     * Gets the value of the specConceptComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specConceptComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecConceptComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgreementSpecConceptTO }
     * 
     * 
     */
    public List<AgreementSpecConceptTO> getSpecConceptComponents() {
        if (specConceptComponents == null) {
            specConceptComponents = new ArrayList<AgreementSpecConceptTO>();
        }
        return this.specConceptComponents;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgreementSpecStatusTO }
     * 
     * 
     */
    public List<AgreementSpecStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<AgreementSpecStatusTO>();
        }
        return this.status;
    }

    /**
     * Sets the value of the versions property.
     * 
     * @param versions
     *     allowed object is
     *     {@link AgreementSpecDefCriteriaTO }
     *     
     */
    public void setVersions(List<AgreementSpecDefCriteriaTO> versions) {
        this.versions = versions;
    }

    /**
     * Sets the value of the specConceptComponents property.
     * 
     * @param specConceptComponents
     *     allowed object is
     *     {@link AgreementSpecConceptTO }
     *     
     */
    public void setSpecConceptComponents(List<AgreementSpecConceptTO> specConceptComponents) {
        this.specConceptComponents = specConceptComponents;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link AgreementSpecStatusTO }
     *     
     */
    public void setStatus(List<AgreementSpecStatusTO> status) {
        this.status = status;
    }

}
