
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MonetaryAccountTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MonetaryAccountTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Expense Account"/>
 *     &lt;enumeration value="Liability Account"/>
 *     &lt;enumeration value="Revenue Account"/>
 *     &lt;enumeration value="Asset Account"/>
 *     &lt;enumeration value="Current Liability Account"/>
 *     &lt;enumeration value="Long Term Liability Account"/>
 *     &lt;enumeration value="Life Expense Account"/>
 *     &lt;enumeration value="Current Assets Account"/>
 *     &lt;enumeration value="Increase In Provisions Due To Discount Account"/>
 *     &lt;enumeration value="Intermediate Agreement Account"/>
 *     &lt;enumeration value="Life Technical Account"/>
 *     &lt;enumeration value="Non Current Assets Account"/>
 *     &lt;enumeration value="Non Life Technical Account"/>
 *     &lt;enumeration value="Non Technical Account"/>
 *     &lt;enumeration value="Off Balance Sheet Items Account"/>
 *     &lt;enumeration value="Operating Account"/>
 *     &lt;enumeration value="Own Funds Account"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MonetaryAccountTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum MonetaryAccountTypeName {

    @XmlEnumValue("Expense Account")
    EXPENSE_ACCOUNT("Expense Account"),
    @XmlEnumValue("Liability Account")
    LIABILITY_ACCOUNT("Liability Account"),
    @XmlEnumValue("Revenue Account")
    REVENUE_ACCOUNT("Revenue Account"),
    @XmlEnumValue("Asset Account")
    ASSET_ACCOUNT("Asset Account"),
    @XmlEnumValue("Current Liability Account")
    CURRENT_LIABILITY_ACCOUNT("Current Liability Account"),
    @XmlEnumValue("Long Term Liability Account")
    LONG_TERM_LIABILITY_ACCOUNT("Long Term Liability Account"),
    @XmlEnumValue("Life Expense Account")
    LIFE_EXPENSE_ACCOUNT("Life Expense Account"),
    @XmlEnumValue("Current Assets Account")
    CURRENT_ASSETS_ACCOUNT("Current Assets Account"),
    @XmlEnumValue("Increase In Provisions Due To Discount Account")
    INCREASE_IN_PROVISIONS_DUE_TO_DISCOUNT_ACCOUNT("Increase In Provisions Due To Discount Account"),
    @XmlEnumValue("Intermediate Agreement Account")
    INTERMEDIATE_AGREEMENT_ACCOUNT("Intermediate Agreement Account"),
    @XmlEnumValue("Life Technical Account")
    LIFE_TECHNICAL_ACCOUNT("Life Technical Account"),
    @XmlEnumValue("Non Current Assets Account")
    NON_CURRENT_ASSETS_ACCOUNT("Non Current Assets Account"),
    @XmlEnumValue("Non Life Technical Account")
    NON_LIFE_TECHNICAL_ACCOUNT("Non Life Technical Account"),
    @XmlEnumValue("Non Technical Account")
    NON_TECHNICAL_ACCOUNT("Non Technical Account"),
    @XmlEnumValue("Off Balance Sheet Items Account")
    OFF_BALANCE_SHEET_ITEMS_ACCOUNT("Off Balance Sheet Items Account"),
    @XmlEnumValue("Operating Account")
    OPERATING_ACCOUNT("Operating Account"),
    @XmlEnumValue("Own Funds Account")
    OWN_FUNDS_ACCOUNT("Own Funds Account");
    private final String value;

    MonetaryAccountTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MonetaryAccountTypeName fromValue(String v) {
        for (MonetaryAccountTypeName c: MonetaryAccountTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
