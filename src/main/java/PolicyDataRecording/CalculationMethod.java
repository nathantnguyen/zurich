
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CalculationMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CalculationMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Earned Premium Flat"/>
 *     &lt;enumeration value="Earned Premium Fully Earned"/>
 *     &lt;enumeration value="Earned Premium Prorate"/>
 *     &lt;enumeration value="Earned Premium Short Rate"/>
 *     &lt;enumeration value="Free Look Return Provision Account Value"/>
 *     &lt;enumeration value="Free Look Return Provision Initial Deposit"/>
 *     &lt;enumeration value="Free Look Return Provision Net Premium"/>
 *     &lt;enumeration value="Free Look Return Provision Per Unit Basis"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CalculationMethod", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum CalculationMethod {

    @XmlEnumValue("Earned Premium Flat")
    EARNED_PREMIUM_FLAT("Earned Premium Flat"),
    @XmlEnumValue("Earned Premium Fully Earned")
    EARNED_PREMIUM_FULLY_EARNED("Earned Premium Fully Earned"),
    @XmlEnumValue("Earned Premium Prorate")
    EARNED_PREMIUM_PRORATE("Earned Premium Prorate"),
    @XmlEnumValue("Earned Premium Short Rate")
    EARNED_PREMIUM_SHORT_RATE("Earned Premium Short Rate"),
    @XmlEnumValue("Free Look Return Provision Account Value")
    FREE_LOOK_RETURN_PROVISION_ACCOUNT_VALUE("Free Look Return Provision Account Value"),
    @XmlEnumValue("Free Look Return Provision Initial Deposit")
    FREE_LOOK_RETURN_PROVISION_INITIAL_DEPOSIT("Free Look Return Provision Initial Deposit"),
    @XmlEnumValue("Free Look Return Provision Net Premium")
    FREE_LOOK_RETURN_PROVISION_NET_PREMIUM("Free Look Return Provision Net Premium"),
    @XmlEnumValue("Free Look Return Provision Per Unit Basis")
    FREE_LOOK_RETURN_PROVISION_PER_UNIT_BASIS("Free Look Return Provision Per Unit Basis");
    private final String value;

    CalculationMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CalculationMethod fromValue(String v) {
        for (CalculationMethod c: CalculationMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
