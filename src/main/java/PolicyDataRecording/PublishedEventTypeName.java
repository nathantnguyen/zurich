
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PublishedEventTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PublishedEventTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Contact Point Changed"/>
 *     &lt;enumeration value="Person Died"/>
 *     &lt;enumeration value="Company Gone Bankrupt"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PublishedEventTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/")
@XmlEnum
public enum PublishedEventTypeName {

    @XmlEnumValue("Contact Point Changed")
    CONTACT_POINT_CHANGED("Contact Point Changed"),
    @XmlEnumValue("Person Died")
    PERSON_DIED("Person Died"),
    @XmlEnumValue("Company Gone Bankrupt")
    COMPANY_GONE_BANKRUPT("Company Gone Bankrupt");
    private final String value;

    PublishedEventTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PublishedEventTypeName fromValue(String v) {
        for (PublishedEventTypeName c: PublishedEventTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
