
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommercialAgreementTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommercialAgreementTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Multi Peril Policy"/>
 *     &lt;enumeration value="Business Owners Policy"/>
 *     &lt;enumeration value="Corporate Owned Life Insurance"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommercialAgreementTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum CommercialAgreementTypeName {

    @XmlEnumValue("Multi Peril Policy")
    MULTI_PERIL_POLICY("Multi Peril Policy"),
    @XmlEnumValue("Business Owners Policy")
    BUSINESS_OWNERS_POLICY("Business Owners Policy"),
    @XmlEnumValue("Corporate Owned Life Insurance")
    CORPORATE_OWNED_LIFE_INSURANCE("Corporate Owned Life Insurance");
    private final String value;

    CommercialAgreementTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommercialAgreementTypeName fromValue(String v) {
        for (CommercialAgreementTypeName c: CommercialAgreementTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
