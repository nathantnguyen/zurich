
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyProvisionRelationshipTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MoneyProvisionRelationshipTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Money Provision Basis"/>
 *     &lt;enumeration value="Money Provision Definition"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MoneyProvisionRelationshipTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/")
@XmlEnum
public enum MoneyProvisionRelationshipTypeName {

    @XmlEnumValue("Money Provision Basis")
    MONEY_PROVISION_BASIS("Money Provision Basis"),
    @XmlEnumValue("Money Provision Definition")
    MONEY_PROVISION_DEFINITION("Money Provision Definition");
    private final String value;

    MoneyProvisionRelationshipTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MoneyProvisionRelationshipTypeName fromValue(String v) {
        for (MoneyProvisionRelationshipTypeName c: MoneyProvisionRelationshipTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
