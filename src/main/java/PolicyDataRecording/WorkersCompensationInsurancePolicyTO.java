
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkersCompensationInsurancePolicy_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WorkersCompensationInsurancePolicy_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CommercialAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="insurerLossDataScore" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}InsurerLossDataScore_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkersCompensationInsurancePolicy_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "insurerLossDataScore"
})
public class WorkersCompensationInsurancePolicyTO
    extends CommercialAgreementTO
{

    protected List<InsurerLossDataScoreTO> insurerLossDataScore;

    /**
     * Gets the value of the insurerLossDataScore property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the insurerLossDataScore property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInsurerLossDataScore().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InsurerLossDataScoreTO }
     * 
     * 
     */
    public List<InsurerLossDataScoreTO> getInsurerLossDataScore() {
        if (insurerLossDataScore == null) {
            insurerLossDataScore = new ArrayList<InsurerLossDataScoreTO>();
        }
        return this.insurerLossDataScore;
    }

    /**
     * Sets the value of the insurerLossDataScore property.
     * 
     * @param insurerLossDataScore
     *     allowed object is
     *     {@link InsurerLossDataScoreTO }
     *     
     */
    public void setInsurerLossDataScore(List<InsurerLossDataScoreTO> insurerLossDataScore) {
        this.insurerLossDataScore = insurerLossDataScore;
    }

}
