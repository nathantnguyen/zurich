
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RoleInAccount_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RoleInAccount_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RoleInContextClass_TO">
 *       &lt;sequence>
 *         &lt;element name="contextAccount" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Account_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleInAccount_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "contextAccount"
})
public class RoleInAccountTO
    extends RoleInContextClassTO
{

    protected AccountTO contextAccount;

    /**
     * Gets the value of the contextAccount property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTO }
     *     
     */
    public AccountTO getContextAccount() {
        return contextAccount;
    }

    /**
     * Sets the value of the contextAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTO }
     *     
     */
    public void setContextAccount(AccountTO value) {
        this.contextAccount = value;
    }

}
