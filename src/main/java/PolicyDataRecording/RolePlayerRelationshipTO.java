
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RolePlayerRelationship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RolePlayerRelationship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Relationship_TO">
 *       &lt;sequence>
 *         &lt;element name="relatedToRolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}RolePlayer_TO" minOccurs="0"/>
 *         &lt;element name="relatedFromRolePlayer" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRole_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RolePlayerRelationship_TO", propOrder = {
    "relatedToRolePlayer",
    "relatedFromRolePlayer"
})
@XmlSeeAlso({
    EmploymentPositionAssignmentTO.class,
    OrganisationMembershipTO.class,
    FamilyRelationshipTO.class,
    ReportingChainTO.class,
    LinkedPartyTO.class,
    OrganisationOwnershipTO.class,
    MarriageRelationshipTO.class
})
public class RolePlayerRelationshipTO
    extends RelationshipTO
{

    protected RolePlayerTO relatedToRolePlayer;
    protected PartyRoleTO relatedFromRolePlayer;

    /**
     * Gets the value of the relatedToRolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link RolePlayerTO }
     *     
     */
    public RolePlayerTO getRelatedToRolePlayer() {
        return relatedToRolePlayer;
    }

    /**
     * Sets the value of the relatedToRolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link RolePlayerTO }
     *     
     */
    public void setRelatedToRolePlayer(RolePlayerTO value) {
        this.relatedToRolePlayer = value;
    }

    /**
     * Gets the value of the relatedFromRolePlayer property.
     * 
     * @return
     *     possible object is
     *     {@link PartyRoleTO }
     *     
     */
    public PartyRoleTO getRelatedFromRolePlayer() {
        return relatedFromRolePlayer;
    }

    /**
     * Sets the value of the relatedFromRolePlayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyRoleTO }
     *     
     */
    public void setRelatedFromRolePlayer(PartyRoleTO value) {
        this.relatedFromRolePlayer = value;
    }

}
