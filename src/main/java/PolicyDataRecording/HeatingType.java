
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HeatingType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HeatingType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Centrally Supplied Gas"/>
 *     &lt;enumeration value="Electricity"/>
 *     &lt;enumeration value="Solar"/>
 *     &lt;enumeration value="Other Gas"/>
 *     &lt;enumeration value="Solid Fuel"/>
 *     &lt;enumeration value="Coal"/>
 *     &lt;enumeration value="Oil"/>
 *     &lt;enumeration value="Propane Gas"/>
 *     &lt;enumeration value="Wood"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HeatingType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum HeatingType {

    @XmlEnumValue("Centrally Supplied Gas")
    CENTRALLY_SUPPLIED_GAS("Centrally Supplied Gas"),
    @XmlEnumValue("Electricity")
    ELECTRICITY("Electricity"),
    @XmlEnumValue("Solar")
    SOLAR("Solar"),
    @XmlEnumValue("Other Gas")
    OTHER_GAS("Other Gas"),
    @XmlEnumValue("Solid Fuel")
    SOLID_FUEL("Solid Fuel"),
    @XmlEnumValue("Coal")
    COAL("Coal"),
    @XmlEnumValue("Oil")
    OIL("Oil"),
    @XmlEnumValue("Propane Gas")
    PROPANE_GAS("Propane Gas"),
    @XmlEnumValue("Wood")
    WOOD("Wood");
    private final String value;

    HeatingType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HeatingType fromValue(String v) {
        for (HeatingType c: HeatingType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
