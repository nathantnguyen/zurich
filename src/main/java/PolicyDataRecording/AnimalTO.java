
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Animal_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Animal_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}LifeForm_TO">
 *       &lt;sequence>
 *         &lt;element name="domestication" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Domestication" minOccurs="0"/>
 *         &lt;element name="isDangerous" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Animal_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "domestication",
    "isDangerous"
})
public class AnimalTO
    extends LifeFormTO
{

    protected Domestication domestication;
    protected Boolean isDangerous;

    /**
     * Gets the value of the domestication property.
     * 
     * @return
     *     possible object is
     *     {@link Domestication }
     *     
     */
    public Domestication getDomestication() {
        return domestication;
    }

    /**
     * Sets the value of the domestication property.
     * 
     * @param value
     *     allowed object is
     *     {@link Domestication }
     *     
     */
    public void setDomestication(Domestication value) {
        this.domestication = value;
    }

    /**
     * Gets the value of the isDangerous property.
     * This getter has been renamed from isIsDangerous() to getIsDangerous() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsDangerous() {
        return isDangerous;
    }

    /**
     * Sets the value of the isDangerous property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDangerous(Boolean value) {
        this.isDangerous = value;
    }

}
