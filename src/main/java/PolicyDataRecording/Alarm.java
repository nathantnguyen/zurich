
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Alarm.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Alarm">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Central Station Call Dialing"/>
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Stand Alone Audible"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Alarm", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum Alarm {

    @XmlEnumValue("Central Station Call Dialing")
    CENTRAL_STATION_CALL_DIALING("Central Station Call Dialing"),
    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Stand Alone Audible")
    STAND_ALONE_AUDIBLE("Stand Alone Audible");
    private final String value;

    Alarm(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Alarm fromValue(String v) {
        for (Alarm c: Alarm.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
