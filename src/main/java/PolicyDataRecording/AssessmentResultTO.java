
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AssessmentResult_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssessmentResult_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="periodStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="periodEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="reliability" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}Reliability" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="accepted" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="assessmentResultRegistration" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResultRegistration_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentResultStatus_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subjectPhysicalObjectReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="assessmentRequestReferences" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subjectrolePlayertReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="assessmentActivityReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="assessedCategory" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" minOccurs="0"/>
 *         &lt;element name="assessedPlace" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Place_TO" minOccurs="0"/>
 *         &lt;element name="assessedActivityOccurrenceReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="importanceRating" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssessmentResult_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "periodStartDate",
    "periodEndDate",
    "reliability",
    "description",
    "externalReference",
    "accepted",
    "assessmentResultRegistration",
    "status",
    "subjectPhysicalObjectReference",
    "assessmentRequestReferences",
    "subjectrolePlayertReference",
    "assessmentActivityReference",
    "assessedCategory",
    "assessedPlace",
    "assessedActivityOccurrenceReference",
    "importanceRating",
    "alternateReference"
})
@XmlSeeAlso({
    StatisticalCodeSummaryTO.class,
    AuthorisationTO.class,
    ConditionTO.class,
    FinancialValuationTO.class,
    ScoreTO.class
})
public class AssessmentResultTO
    extends BusinessModelObjectTO
{

    protected XMLGregorianCalendar periodStartDate;
    protected XMLGregorianCalendar periodEndDate;
    protected Reliability reliability;
    protected String description;
    protected String externalReference;
    protected Boolean accepted;
    protected List<AssessmentResultRegistrationTO> assessmentResultRegistration;
    protected List<AssessmentResultStatusTO> status;
    protected ObjectReferenceTO subjectPhysicalObjectReference;
    protected List<ObjectReferenceTO> assessmentRequestReferences;
    protected ObjectReferenceTO subjectrolePlayertReference;
    protected ObjectReferenceTO assessmentActivityReference;
    protected CategoryTO assessedCategory;
    protected PlaceTO assessedPlace;
    protected ObjectReferenceTO assessedActivityOccurrenceReference;
    protected String importanceRating;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the periodStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPeriodStartDate() {
        return periodStartDate;
    }

    /**
     * Sets the value of the periodStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPeriodStartDate(XMLGregorianCalendar value) {
        this.periodStartDate = value;
    }

    /**
     * Gets the value of the periodEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPeriodEndDate() {
        return periodEndDate;
    }

    /**
     * Sets the value of the periodEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPeriodEndDate(XMLGregorianCalendar value) {
        this.periodEndDate = value;
    }

    /**
     * Gets the value of the reliability property.
     * 
     * @return
     *     possible object is
     *     {@link Reliability }
     *     
     */
    public Reliability getReliability() {
        return reliability;
    }

    /**
     * Sets the value of the reliability property.
     * 
     * @param value
     *     allowed object is
     *     {@link Reliability }
     *     
     */
    public void setReliability(Reliability value) {
        this.reliability = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the accepted property.
     * This getter has been renamed from isAccepted() to getAccepted() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAccepted() {
        return accepted;
    }

    /**
     * Sets the value of the accepted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccepted(Boolean value) {
        this.accepted = value;
    }

    /**
     * Gets the value of the assessmentResultRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assessmentResultRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssessmentResultRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssessmentResultRegistrationTO }
     * 
     * 
     */
    public List<AssessmentResultRegistrationTO> getAssessmentResultRegistration() {
        if (assessmentResultRegistration == null) {
            assessmentResultRegistration = new ArrayList<AssessmentResultRegistrationTO>();
        }
        return this.assessmentResultRegistration;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssessmentResultStatusTO }
     * 
     * 
     */
    public List<AssessmentResultStatusTO> getStatus() {
        if (status == null) {
            status = new ArrayList<AssessmentResultStatusTO>();
        }
        return this.status;
    }

    /**
     * Gets the value of the subjectPhysicalObjectReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getSubjectPhysicalObjectReference() {
        return subjectPhysicalObjectReference;
    }

    /**
     * Sets the value of the subjectPhysicalObjectReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSubjectPhysicalObjectReference(ObjectReferenceTO value) {
        this.subjectPhysicalObjectReference = value;
    }

    /**
     * Gets the value of the assessmentRequestReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assessmentRequestReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssessmentRequestReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getAssessmentRequestReferences() {
        if (assessmentRequestReferences == null) {
            assessmentRequestReferences = new ArrayList<ObjectReferenceTO>();
        }
        return this.assessmentRequestReferences;
    }

    /**
     * Gets the value of the subjectrolePlayertReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getSubjectrolePlayertReference() {
        return subjectrolePlayertReference;
    }

    /**
     * Sets the value of the subjectrolePlayertReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setSubjectrolePlayertReference(ObjectReferenceTO value) {
        this.subjectrolePlayertReference = value;
    }

    /**
     * Gets the value of the assessmentActivityReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getAssessmentActivityReference() {
        return assessmentActivityReference;
    }

    /**
     * Sets the value of the assessmentActivityReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setAssessmentActivityReference(ObjectReferenceTO value) {
        this.assessmentActivityReference = value;
    }

    /**
     * Gets the value of the assessedCategory property.
     * 
     * @return
     *     possible object is
     *     {@link CategoryTO }
     *     
     */
    public CategoryTO getAssessedCategory() {
        return assessedCategory;
    }

    /**
     * Sets the value of the assessedCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setAssessedCategory(CategoryTO value) {
        this.assessedCategory = value;
    }

    /**
     * Gets the value of the assessedPlace property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTO }
     *     
     */
    public PlaceTO getAssessedPlace() {
        return assessedPlace;
    }

    /**
     * Sets the value of the assessedPlace property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTO }
     *     
     */
    public void setAssessedPlace(PlaceTO value) {
        this.assessedPlace = value;
    }

    /**
     * Gets the value of the assessedActivityOccurrenceReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getAssessedActivityOccurrenceReference() {
        return assessedActivityOccurrenceReference;
    }

    /**
     * Sets the value of the assessedActivityOccurrenceReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setAssessedActivityOccurrenceReference(ObjectReferenceTO value) {
        this.assessedActivityOccurrenceReference = value;
    }

    /**
     * Gets the value of the importanceRating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImportanceRating() {
        return importanceRating;
    }

    /**
     * Sets the value of the importanceRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImportanceRating(String value) {
        this.importanceRating = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the assessmentResultRegistration property.
     * 
     * @param assessmentResultRegistration
     *     allowed object is
     *     {@link AssessmentResultRegistrationTO }
     *     
     */
    public void setAssessmentResultRegistration(List<AssessmentResultRegistrationTO> assessmentResultRegistration) {
        this.assessmentResultRegistration = assessmentResultRegistration;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param status
     *     allowed object is
     *     {@link AssessmentResultStatusTO }
     *     
     */
    public void setStatus(List<AssessmentResultStatusTO> status) {
        this.status = status;
    }

    /**
     * Sets the value of the assessmentRequestReferences property.
     * 
     * @param assessmentRequestReferences
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setAssessmentRequestReferences(List<ObjectReferenceTO> assessmentRequestReferences) {
        this.assessmentRequestReferences = assessmentRequestReferences;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
