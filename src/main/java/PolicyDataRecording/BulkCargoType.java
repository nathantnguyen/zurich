
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BulkCargoType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BulkCargoType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Dry Bulk Cargo"/>
 *     &lt;enumeration value="Liquid Cargo"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BulkCargoType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum BulkCargoType {

    @XmlEnumValue("Dry Bulk Cargo")
    DRY_BULK_CARGO("Dry Bulk Cargo"),
    @XmlEnumValue("Liquid Cargo")
    LIQUID_CARGO("Liquid Cargo");
    private final String value;

    BulkCargoType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BulkCargoType fromValue(String v) {
        for (BulkCargoType c: BulkCargoType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
