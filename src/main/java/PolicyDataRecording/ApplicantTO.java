
package PolicyDataRecording;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for Applicant_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Applicant_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}PartyInvolvedInContractRequest_TO">
 *       &lt;sequence>
 *         &lt;element name="noClaimDiscountPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="declaredClaimsCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="declaredClaimsAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="hasExistingContracts" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="isNewToSellingChannel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="yearsKnownToSellingChannel" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="existingContracts" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Applicant_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "noClaimDiscountPeriod",
    "declaredClaimsCount",
    "declaredClaimsAmount",
    "hasExistingContracts",
    "isNewToSellingChannel",
    "yearsKnownToSellingChannel",
    "existingContracts"
})
public class ApplicantTO
    extends PartyInvolvedInContractRequestTO
{

    protected Duration noClaimDiscountPeriod;
    protected BigInteger declaredClaimsCount;
    protected BaseCurrencyAmount declaredClaimsAmount;
    protected Boolean hasExistingContracts;
    protected Boolean isNewToSellingChannel;
    protected BigInteger yearsKnownToSellingChannel;
    protected List<ObjectReferenceTO> existingContracts;

    /**
     * Gets the value of the noClaimDiscountPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getNoClaimDiscountPeriod() {
        return noClaimDiscountPeriod;
    }

    /**
     * Sets the value of the noClaimDiscountPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setNoClaimDiscountPeriod(Duration value) {
        this.noClaimDiscountPeriod = value;
    }

    /**
     * Gets the value of the declaredClaimsCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDeclaredClaimsCount() {
        return declaredClaimsCount;
    }

    /**
     * Sets the value of the declaredClaimsCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDeclaredClaimsCount(BigInteger value) {
        this.declaredClaimsCount = value;
    }

    /**
     * Gets the value of the declaredClaimsAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getDeclaredClaimsAmount() {
        return declaredClaimsAmount;
    }

    /**
     * Sets the value of the declaredClaimsAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setDeclaredClaimsAmount(BaseCurrencyAmount value) {
        this.declaredClaimsAmount = value;
    }

    /**
     * Gets the value of the hasExistingContracts property.
     * This getter has been renamed from isHasExistingContracts() to getHasExistingContracts() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHasExistingContracts() {
        return hasExistingContracts;
    }

    /**
     * Sets the value of the hasExistingContracts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasExistingContracts(Boolean value) {
        this.hasExistingContracts = value;
    }

    /**
     * Gets the value of the isNewToSellingChannel property.
     * This getter has been renamed from isIsNewToSellingChannel() to getIsNewToSellingChannel() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsNewToSellingChannel() {
        return isNewToSellingChannel;
    }

    /**
     * Sets the value of the isNewToSellingChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsNewToSellingChannel(Boolean value) {
        this.isNewToSellingChannel = value;
    }

    /**
     * Gets the value of the yearsKnownToSellingChannel property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getYearsKnownToSellingChannel() {
        return yearsKnownToSellingChannel;
    }

    /**
     * Sets the value of the yearsKnownToSellingChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setYearsKnownToSellingChannel(BigInteger value) {
        this.yearsKnownToSellingChannel = value;
    }

    /**
     * Gets the value of the existingContracts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the existingContracts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExistingContracts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getExistingContracts() {
        if (existingContracts == null) {
            existingContracts = new ArrayList<ObjectReferenceTO>();
        }
        return this.existingContracts;
    }

    /**
     * Sets the value of the existingContracts property.
     * 
     * @param existingContracts
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setExistingContracts(List<ObjectReferenceTO> existingContracts) {
        this.existingContracts = existingContracts;
    }

}
