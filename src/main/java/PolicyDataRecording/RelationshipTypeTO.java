
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RelationshipType_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationshipType_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Type_TO">
 *       &lt;sequence>
 *         &lt;element name="leftToRightName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="rightToLeftName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationshipType_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "leftToRightName",
    "rightToLeftName"
})
@XmlSeeAlso({
    ContractSpecificationRelationshipTypeTO.class,
    ContractRelationshipTypeTO.class,
    MoneyProvisionRelationshipTypeTO.class,
    AccountRelationshipTypeTO.class,
    RolePlayerRelationshipTypeTO.class
})
public class RelationshipTypeTO
    extends TypeTO
{

    protected String leftToRightName;
    protected String rightToLeftName;

    /**
     * Gets the value of the leftToRightName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeftToRightName() {
        return leftToRightName;
    }

    /**
     * Sets the value of the leftToRightName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeftToRightName(String value) {
        this.leftToRightName = value;
    }

    /**
     * Gets the value of the rightToLeftName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRightToLeftName() {
        return rightToLeftName;
    }

    /**
     * Sets the value of the rightToLeftName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRightToLeftName(String value) {
        this.rightToLeftName = value;
    }

}
