
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RuleKind.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RuleKind">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Claim Validity Rule"/>
 *     &lt;enumeration value="Data Capture Rule"/>
 *     &lt;enumeration value="Eligibility Rule"/>
 *     &lt;enumeration value="Product Composition Rule"/>
 *     &lt;enumeration value="Request Validity Rule"/>
 *     &lt;enumeration value="Trigger Condition"/>
 *     &lt;enumeration value="Underwriting Rule"/>
 *     &lt;enumeration value="Validity Rule"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RuleKind", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/GenericAgreement/GenericAgreementEnumerationsAndStates/")
@XmlEnum
public enum RuleKind {

    @XmlEnumValue("Claim Validity Rule")
    CLAIM_VALIDITY_RULE("Claim Validity Rule"),
    @XmlEnumValue("Data Capture Rule")
    DATA_CAPTURE_RULE("Data Capture Rule"),
    @XmlEnumValue("Eligibility Rule")
    ELIGIBILITY_RULE("Eligibility Rule"),
    @XmlEnumValue("Product Composition Rule")
    PRODUCT_COMPOSITION_RULE("Product Composition Rule"),
    @XmlEnumValue("Request Validity Rule")
    REQUEST_VALIDITY_RULE("Request Validity Rule"),
    @XmlEnumValue("Trigger Condition")
    TRIGGER_CONDITION("Trigger Condition"),
    @XmlEnumValue("Underwriting Rule")
    UNDERWRITING_RULE("Underwriting Rule"),
    @XmlEnumValue("Validity Rule")
    VALIDITY_RULE("Validity Rule");
    private final String value;

    RuleKind(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RuleKind fromValue(String v) {
        for (RuleKind c: RuleKind.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
