
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConstructionMaterial.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ConstructionMaterial">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Steel"/>
 *     &lt;enumeration value="Brick"/>
 *     &lt;enumeration value="Concrete"/>
 *     &lt;enumeration value="Marble"/>
 *     &lt;enumeration value="Wood"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ConstructionMaterial", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum ConstructionMaterial {

    @XmlEnumValue("Steel")
    STEEL("Steel"),
    @XmlEnumValue("Brick")
    BRICK("Brick"),
    @XmlEnumValue("Concrete")
    CONCRETE("Concrete"),
    @XmlEnumValue("Marble")
    MARBLE("Marble"),
    @XmlEnumValue("Wood")
    WOOD("Wood");
    private final String value;

    ConstructionMaterial(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ConstructionMaterial fromValue(String v) {
        for (ConstructionMaterial c: ConstructionMaterial.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
