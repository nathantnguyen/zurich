
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityOccurrenceStatus_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityOccurrenceStatus_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}StatusWithCommonReason_TO">
 *       &lt;sequence>
 *         &lt;element name="activityOccurrenceState" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityOccurrenceStatus_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "activityOccurrenceState"
})
public class ActivityOccurrenceStatusTO
    extends StatusWithCommonReasonTO
{

    protected String activityOccurrenceState;

    /**
     * Gets the value of the activityOccurrenceState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityOccurrenceState() {
        return activityOccurrenceState;
    }

    /**
     * Sets the value of the activityOccurrenceState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityOccurrenceState(String value) {
        this.activityOccurrenceState = value;
    }

}
