
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyInvolvedInFinancialServicesProductTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PartyInvolvedInFinancialServicesProductTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Underwriter"/>
 *     &lt;enumeration value="Producer"/>
 *     &lt;enumeration value="Marketeer"/>
 *     &lt;enumeration value="Administrator"/>
 *     &lt;enumeration value="Product Jurisdiction"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PartyInvolvedInFinancialServicesProductTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum PartyInvolvedInFinancialServicesProductTypeName {

    @XmlEnumValue("Underwriter")
    UNDERWRITER("Underwriter"),
    @XmlEnumValue("Producer")
    PRODUCER("Producer"),
    @XmlEnumValue("Marketeer")
    MARKETEER("Marketeer"),
    @XmlEnumValue("Administrator")
    ADMINISTRATOR("Administrator"),
    @XmlEnumValue("Product Jurisdiction")
    PRODUCT_JURISDICTION("Product Jurisdiction");
    private final String value;

    PartyInvolvedInFinancialServicesProductTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartyInvolvedInFinancialServicesProductTypeName fromValue(String v) {
        for (PartyInvolvedInFinancialServicesProductTypeName c: PartyInvolvedInFinancialServicesProductTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
