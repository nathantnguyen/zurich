
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestPeriodicity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RequestPeriodicity">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Irregular"/>
 *     &lt;enumeration value="Daily"/>
 *     &lt;enumeration value="Monthly"/>
 *     &lt;enumeration value="Quarterly"/>
 *     &lt;enumeration value="Yearly"/>
 *     &lt;enumeration value="One Time"/>
 *     &lt;enumeration value="Per Medical Treatment"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RequestPeriodicity", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum RequestPeriodicity {

    @XmlEnumValue("Irregular")
    IRREGULAR("Irregular"),
    @XmlEnumValue("Daily")
    DAILY("Daily"),
    @XmlEnumValue("Monthly")
    MONTHLY("Monthly"),
    @XmlEnumValue("Quarterly")
    QUARTERLY("Quarterly"),
    @XmlEnumValue("Yearly")
    YEARLY("Yearly"),
    @XmlEnumValue("One Time")
    ONE_TIME("One Time"),
    @XmlEnumValue("Per Medical Treatment")
    PER_MEDICAL_TREATMENT("Per Medical Treatment");
    private final String value;

    RequestPeriodicity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RequestPeriodicity fromValue(String v) {
        for (RequestPeriodicity c: RequestPeriodicity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
