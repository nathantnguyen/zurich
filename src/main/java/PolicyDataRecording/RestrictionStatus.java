
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RestrictionStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RestrictionStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Grade Two Georgian Buildings"/>
 *     &lt;enumeration value="Historical Registry"/>
 *     &lt;enumeration value="Office And Light Industrial"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RestrictionStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/")
@XmlEnum
public enum RestrictionStatus {

    @XmlEnumValue("Grade Two Georgian Buildings")
    GRADE_TWO_GEORGIAN_BUILDINGS("Grade Two Georgian Buildings"),
    @XmlEnumValue("Historical Registry")
    HISTORICAL_REGISTRY("Historical Registry"),
    @XmlEnumValue("Office And Light Industrial")
    OFFICE_AND_LIGHT_INDUSTRIAL("Office And Light Industrial");
    private final String value;

    RestrictionStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RestrictionStatus fromValue(String v) {
        for (RestrictionStatus c: RestrictionStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
