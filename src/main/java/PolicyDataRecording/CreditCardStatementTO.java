
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CreditCardStatement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardStatement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}AccountStatement_TO">
 *       &lt;sequence>
 *         &lt;element name="paymentDueDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="minimumPaymentDue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="nextClosingDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="delinquency" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Delinquency_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardStatement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "paymentDueDate",
    "minimumPaymentDue",
    "nextClosingDate",
    "delinquency"
})
public class CreditCardStatementTO
    extends AccountStatementTO
{

    protected XMLGregorianCalendar paymentDueDate;
    protected BaseCurrencyAmount minimumPaymentDue;
    protected XMLGregorianCalendar nextClosingDate;
    protected List<DelinquencyTO> delinquency;

    /**
     * Gets the value of the paymentDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDueDate() {
        return paymentDueDate;
    }

    /**
     * Sets the value of the paymentDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDueDate(XMLGregorianCalendar value) {
        this.paymentDueDate = value;
    }

    /**
     * Gets the value of the minimumPaymentDue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getMinimumPaymentDue() {
        return minimumPaymentDue;
    }

    /**
     * Sets the value of the minimumPaymentDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setMinimumPaymentDue(BaseCurrencyAmount value) {
        this.minimumPaymentDue = value;
    }

    /**
     * Gets the value of the nextClosingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextClosingDate() {
        return nextClosingDate;
    }

    /**
     * Sets the value of the nextClosingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextClosingDate(XMLGregorianCalendar value) {
        this.nextClosingDate = value;
    }

    /**
     * Gets the value of the delinquency property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the delinquency property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDelinquency().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DelinquencyTO }
     * 
     * 
     */
    public List<DelinquencyTO> getDelinquency() {
        if (delinquency == null) {
            delinquency = new ArrayList<DelinquencyTO>();
        }
        return this.delinquency;
    }

    /**
     * Sets the value of the delinquency property.
     * 
     * @param delinquency
     *     allowed object is
     *     {@link DelinquencyTO }
     *     
     */
    public void setDelinquency(List<DelinquencyTO> delinquency) {
        this.delinquency = delinquency;
    }

}
