
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssessmentResultInvolvedInContractRequestTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssessmentResultInvolvedInContractRequestTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PACE Eligibility Assessment"/>
 *     &lt;enumeration value="Submission Product Conflict Assessment"/>
 *     &lt;enumeration value="Submission Product Quality Assessment"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssessmentResultInvolvedInContractRequestTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum AssessmentResultInvolvedInContractRequestTypeName {

    @XmlEnumValue("PACE Eligibility Assessment")
    PACE_ELIGIBILITY_ASSESSMENT("PACE Eligibility Assessment"),
    @XmlEnumValue("Submission Product Conflict Assessment")
    SUBMISSION_PRODUCT_CONFLICT_ASSESSMENT("Submission Product Conflict Assessment"),
    @XmlEnumValue("Submission Product Quality Assessment")
    SUBMISSION_PRODUCT_QUALITY_ASSESSMENT("Submission Product Quality Assessment");
    private final String value;

    AssessmentResultInvolvedInContractRequestTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssessmentResultInvolvedInContractRequestTypeName fromValue(String v) {
        for (AssessmentResultInvolvedInContractRequestTypeName c: AssessmentResultInvolvedInContractRequestTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
