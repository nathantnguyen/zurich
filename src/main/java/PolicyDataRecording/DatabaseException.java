
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DatabaseException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DatabaseException">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}ResourceException">
 *       &lt;sequence>
 *         &lt;element name="sqlCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatabaseException", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/", propOrder = {
    "sqlCode"
})
public class DatabaseException
    extends ResourceException
{

    @XmlElement(required = true)
    protected String sqlCode;

    /**
     * Gets the value of the sqlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSqlCode() {
        return sqlCode;
    }

    /**
     * Sets the value of the sqlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSqlCode(String value) {
        this.sqlCode = value;
    }

}
