
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartyNameTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PartyNameTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Birth Name"/>
 *     &lt;enumeration value="Company Name"/>
 *     &lt;enumeration value="Married Name"/>
 *     &lt;enumeration value="Official Name"/>
 *     &lt;enumeration value="Pen Name"/>
 *     &lt;enumeration value="Stage Name"/>
 *     &lt;enumeration value="Trading Name"/>
 *     &lt;enumeration value="Business Name"/>
 *     &lt;enumeration value="Abbreviated Name"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PartyNameTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum PartyNameTypeName {

    @XmlEnumValue("Birth Name")
    BIRTH_NAME("Birth Name"),
    @XmlEnumValue("Company Name")
    COMPANY_NAME("Company Name"),
    @XmlEnumValue("Married Name")
    MARRIED_NAME("Married Name"),
    @XmlEnumValue("Official Name")
    OFFICIAL_NAME("Official Name"),
    @XmlEnumValue("Pen Name")
    PEN_NAME("Pen Name"),
    @XmlEnumValue("Stage Name")
    STAGE_NAME("Stage Name"),
    @XmlEnumValue("Trading Name")
    TRADING_NAME("Trading Name"),
    @XmlEnumValue("Business Name")
    BUSINESS_NAME("Business Name"),
    @XmlEnumValue("Abbreviated Name")
    ABBREVIATED_NAME("Abbreviated Name");
    private final String value;

    PartyNameTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartyNameTypeName fromValue(String v) {
        for (PartyNameTypeName c: PartyNameTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
