
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NatureOfProvision.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NatureOfProvision">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Provision For Employee Benefits"/>
 *     &lt;enumeration value="Provision For Income Taxes"/>
 *     &lt;enumeration value="Provision For Insurance Policy Liabilities"/>
 *     &lt;enumeration value="Provision For Restructuring"/>
 *     &lt;enumeration value="Provision For Settlement Of A Lawsuit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NatureOfProvision", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum NatureOfProvision {

    @XmlEnumValue("Provision For Employee Benefits")
    PROVISION_FOR_EMPLOYEE_BENEFITS("Provision For Employee Benefits"),
    @XmlEnumValue("Provision For Income Taxes")
    PROVISION_FOR_INCOME_TAXES("Provision For Income Taxes"),
    @XmlEnumValue("Provision For Insurance Policy Liabilities")
    PROVISION_FOR_INSURANCE_POLICY_LIABILITIES("Provision For Insurance Policy Liabilities"),
    @XmlEnumValue("Provision For Restructuring")
    PROVISION_FOR_RESTRUCTURING("Provision For Restructuring"),
    @XmlEnumValue("Provision For Settlement Of A Lawsuit")
    PROVISION_FOR_SETTLEMENT_OF_A_LAWSUIT("Provision For Settlement Of A Lawsuit");
    private final String value;

    NatureOfProvision(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NatureOfProvision fromValue(String v) {
        for (NatureOfProvision c: NatureOfProvision.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
