
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmploymentStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmploymentStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Full Time"/>
 *     &lt;enumeration value="Part Time"/>
 *     &lt;enumeration value="Retired"/>
 *     &lt;enumeration value="Contractual"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EmploymentStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum EmploymentStatus {

    @XmlEnumValue("Full Time")
    FULL_TIME("Full Time"),
    @XmlEnumValue("Part Time")
    PART_TIME("Part Time"),
    @XmlEnumValue("Retired")
    RETIRED("Retired"),
    @XmlEnumValue("Contractual")
    CONTRACTUAL("Contractual");
    private final String value;

    EmploymentStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmploymentStatus fromValue(String v) {
        for (EmploymentStatus c: EmploymentStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
