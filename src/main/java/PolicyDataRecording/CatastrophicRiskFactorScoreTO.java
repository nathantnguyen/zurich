
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for CatastrophicRiskFactorScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CatastrophicRiskFactorScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskFactorScore_TO">
 *       &lt;sequence>
 *         &lt;element name="catastropheReturnPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CatastrophicRiskFactorScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "catastropheReturnPeriod"
})
public class CatastrophicRiskFactorScoreTO
    extends RiskFactorScoreTO
{

    protected Duration catastropheReturnPeriod;

    /**
     * Gets the value of the catastropheReturnPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getCatastropheReturnPeriod() {
        return catastropheReturnPeriod;
    }

    /**
     * Sets the value of the catastropheReturnPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setCatastropheReturnPeriod(Duration value) {
        this.catastropheReturnPeriod = value;
    }

}
