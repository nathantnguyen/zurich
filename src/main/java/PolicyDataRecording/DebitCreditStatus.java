
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DebitCreditStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DebitCreditStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Creditor"/>
 *     &lt;enumeration value="Debtor"/>
 *     &lt;enumeration value="Neutral"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DebitCreditStatus", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum DebitCreditStatus {

    @XmlEnumValue("Creditor")
    CREDITOR("Creditor"),
    @XmlEnumValue("Debtor")
    DEBTOR("Debtor"),
    @XmlEnumValue("Neutral")
    NEUTRAL("Neutral");
    private final String value;

    DebitCreditStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DebitCreditStatus fromValue(String v) {
        for (DebitCreditStatus c: DebitCreditStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
