
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Relationship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Relationship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="relationshipTypeName" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RelationshipType_TO" minOccurs="0"/>
 *         &lt;element name="leftToRightName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="rightToLeftName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Relationship_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "startDate",
    "endDate",
    "description",
    "relationshipTypeName",
    "leftToRightName",
    "rightToLeftName"
})
@XmlSeeAlso({
    ContractRelationshipTO.class,
    ContractSpecificationRelationshipTO.class,
    PlacePositioningTO.class,
    FinancialTransactionRelationshipTO.class,
    MoneyProvisionRelationshipTO.class,
    AccountRelationshipTO.class,
    RolePlayerRelationshipTO.class
})
public class RelationshipTO
    extends BaseTransferObject
{

    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected String description;
    protected RelationshipTypeTO relationshipTypeName;
    protected String leftToRightName;
    protected String rightToLeftName;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the relationshipTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipTypeTO }
     *     
     */
    public RelationshipTypeTO getRelationshipTypeName() {
        return relationshipTypeName;
    }

    /**
     * Sets the value of the relationshipTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipTypeTO }
     *     
     */
    public void setRelationshipTypeName(RelationshipTypeTO value) {
        this.relationshipTypeName = value;
    }

    /**
     * Gets the value of the leftToRightName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeftToRightName() {
        return leftToRightName;
    }

    /**
     * Sets the value of the leftToRightName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeftToRightName(String value) {
        this.leftToRightName = value;
    }

    /**
     * Gets the value of the rightToLeftName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRightToLeftName() {
        return rightToLeftName;
    }

    /**
     * Sets the value of the rightToLeftName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRightToLeftName(String value) {
        this.rightToLeftName = value;
    }

}
