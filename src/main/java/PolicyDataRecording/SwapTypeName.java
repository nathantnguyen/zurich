
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SwapTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SwapTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Currency Swap"/>
 *     &lt;enumeration value="Equity Swap"/>
 *     &lt;enumeration value="Interest Rate Swap"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SwapTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/AccountAndFund/AccountAndFundEnumerationsAndStates/")
@XmlEnum
public enum SwapTypeName {

    @XmlEnumValue("Currency Swap")
    CURRENCY_SWAP("Currency Swap"),
    @XmlEnumValue("Equity Swap")
    EQUITY_SWAP("Equity Swap"),
    @XmlEnumValue("Interest Rate Swap")
    INTEREST_RATE_SWAP("Interest Rate Swap");
    private final String value;

    SwapTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SwapTypeName fromValue(String v) {
        for (SwapTypeName c: SwapTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
