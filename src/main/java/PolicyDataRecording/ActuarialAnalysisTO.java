
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActuarialAnalysis_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActuarialAnalysis_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentActivity_TO">
 *       &lt;sequence>
 *         &lt;element name="reinsuranceStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}ScenarioReinsuranceStatus" minOccurs="0"/>
 *         &lt;element name="solvencyMarginStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}ScenarioSolvencyMarginStatus" minOccurs="0"/>
 *         &lt;element name="taxStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}ScenarioTaxStatus" minOccurs="0"/>
 *         &lt;element name="valuationState" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}AssessmentScenarioSet" minOccurs="0"/>
 *         &lt;element name="scenarioValuationBasis" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}ScenarioValuationBasis" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActuarialAnalysis_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "reinsuranceStatus",
    "solvencyMarginStatus",
    "taxStatus",
    "valuationState",
    "scenarioValuationBasis"
})
public class ActuarialAnalysisTO
    extends AssessmentActivityTO
{

    protected ScenarioReinsuranceStatus reinsuranceStatus;
    protected ScenarioSolvencyMarginStatus solvencyMarginStatus;
    protected ScenarioTaxStatus taxStatus;
    protected AssessmentScenarioSet valuationState;
    protected ScenarioValuationBasis scenarioValuationBasis;

    /**
     * Gets the value of the reinsuranceStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ScenarioReinsuranceStatus }
     *     
     */
    public ScenarioReinsuranceStatus getReinsuranceStatus() {
        return reinsuranceStatus;
    }

    /**
     * Sets the value of the reinsuranceStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScenarioReinsuranceStatus }
     *     
     */
    public void setReinsuranceStatus(ScenarioReinsuranceStatus value) {
        this.reinsuranceStatus = value;
    }

    /**
     * Gets the value of the solvencyMarginStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ScenarioSolvencyMarginStatus }
     *     
     */
    public ScenarioSolvencyMarginStatus getSolvencyMarginStatus() {
        return solvencyMarginStatus;
    }

    /**
     * Sets the value of the solvencyMarginStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScenarioSolvencyMarginStatus }
     *     
     */
    public void setSolvencyMarginStatus(ScenarioSolvencyMarginStatus value) {
        this.solvencyMarginStatus = value;
    }

    /**
     * Gets the value of the taxStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ScenarioTaxStatus }
     *     
     */
    public ScenarioTaxStatus getTaxStatus() {
        return taxStatus;
    }

    /**
     * Sets the value of the taxStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScenarioTaxStatus }
     *     
     */
    public void setTaxStatus(ScenarioTaxStatus value) {
        this.taxStatus = value;
    }

    /**
     * Gets the value of the valuationState property.
     * 
     * @return
     *     possible object is
     *     {@link AssessmentScenarioSet }
     *     
     */
    public AssessmentScenarioSet getValuationState() {
        return valuationState;
    }

    /**
     * Sets the value of the valuationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssessmentScenarioSet }
     *     
     */
    public void setValuationState(AssessmentScenarioSet value) {
        this.valuationState = value;
    }

    /**
     * Gets the value of the scenarioValuationBasis property.
     * 
     * @return
     *     possible object is
     *     {@link ScenarioValuationBasis }
     *     
     */
    public ScenarioValuationBasis getScenarioValuationBasis() {
        return scenarioValuationBasis;
    }

    /**
     * Sets the value of the scenarioValuationBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScenarioValuationBasis }
     *     
     */
    public void setScenarioValuationBasis(ScenarioValuationBasis value) {
        this.scenarioValuationBasis = value;
    }

}
