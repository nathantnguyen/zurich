
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmploymentAgreementStatus_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmploymentAgreementStatus_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Status_TO">
 *       &lt;sequence>
 *         &lt;element name="state" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}EmploymentAgreementState" minOccurs="0"/>
 *         &lt;element name="reason" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}EmploymentAgreementStatusReason" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmploymentAgreementStatus_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "state",
    "reason"
})
public class EmploymentAgreementStatusTO
    extends StatusTO
{

    protected EmploymentAgreementState state;
    protected EmploymentAgreementStatusReason reason;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link EmploymentAgreementState }
     *     
     */
    public EmploymentAgreementState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmploymentAgreementState }
     *     
     */
    public void setState(EmploymentAgreementState value) {
        this.state = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link EmploymentAgreementStatusReason }
     *     
     */
    public EmploymentAgreementStatusReason getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmploymentAgreementStatusReason }
     *     
     */
    public void setReason(EmploymentAgreementStatusReason value) {
        this.reason = value;
    }

}
