
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Derivative_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Derivative_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}FinancialAsset_TO">
 *       &lt;sequence>
 *         &lt;element name="automaticExerciseFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="automaticExercisePrice" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="electionDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="exerciseEndDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="exerciseStartDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="lastDeliveryNoticeDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="minimumExercisableQuantity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="minimumIncrementalExercisableQuanity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="multiLegNumber" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="multipleExerciseFlag" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Derivative_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "automaticExerciseFlag",
    "automaticExercisePrice",
    "electionDate",
    "exerciseEndDate",
    "exerciseStartDate",
    "lastDeliveryNoticeDate",
    "minimumExercisableQuantity",
    "minimumIncrementalExercisableQuanity",
    "multiLegNumber",
    "multipleExerciseFlag"
})
@XmlSeeAlso({
    OptionTO.class,
    WarrantDerivativeTO.class,
    SwapTO.class,
    FutureTO.class
})
public class DerivativeTO
    extends FinancialAssetTO
{

    protected Boolean automaticExerciseFlag;
    protected BaseCurrencyAmount automaticExercisePrice;
    protected XMLGregorianCalendar electionDate;
    protected XMLGregorianCalendar exerciseEndDate;
    protected XMLGregorianCalendar exerciseStartDate;
    protected XMLGregorianCalendar lastDeliveryNoticeDate;
    protected BigInteger minimumExercisableQuantity;
    protected BigInteger minimumIncrementalExercisableQuanity;
    protected BigInteger multiLegNumber;
    protected Boolean multipleExerciseFlag;

    /**
     * Gets the value of the automaticExerciseFlag property.
     * This getter has been renamed from isAutomaticExerciseFlag() to getAutomaticExerciseFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getAutomaticExerciseFlag() {
        return automaticExerciseFlag;
    }

    /**
     * Sets the value of the automaticExerciseFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutomaticExerciseFlag(Boolean value) {
        this.automaticExerciseFlag = value;
    }

    /**
     * Gets the value of the automaticExercisePrice property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAutomaticExercisePrice() {
        return automaticExercisePrice;
    }

    /**
     * Sets the value of the automaticExercisePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAutomaticExercisePrice(BaseCurrencyAmount value) {
        this.automaticExercisePrice = value;
    }

    /**
     * Gets the value of the electionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getElectionDate() {
        return electionDate;
    }

    /**
     * Sets the value of the electionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setElectionDate(XMLGregorianCalendar value) {
        this.electionDate = value;
    }

    /**
     * Gets the value of the exerciseEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExerciseEndDate() {
        return exerciseEndDate;
    }

    /**
     * Sets the value of the exerciseEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExerciseEndDate(XMLGregorianCalendar value) {
        this.exerciseEndDate = value;
    }

    /**
     * Gets the value of the exerciseStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExerciseStartDate() {
        return exerciseStartDate;
    }

    /**
     * Sets the value of the exerciseStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExerciseStartDate(XMLGregorianCalendar value) {
        this.exerciseStartDate = value;
    }

    /**
     * Gets the value of the lastDeliveryNoticeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastDeliveryNoticeDate() {
        return lastDeliveryNoticeDate;
    }

    /**
     * Sets the value of the lastDeliveryNoticeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastDeliveryNoticeDate(XMLGregorianCalendar value) {
        this.lastDeliveryNoticeDate = value;
    }

    /**
     * Gets the value of the minimumExercisableQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumExercisableQuantity() {
        return minimumExercisableQuantity;
    }

    /**
     * Sets the value of the minimumExercisableQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumExercisableQuantity(BigInteger value) {
        this.minimumExercisableQuantity = value;
    }

    /**
     * Gets the value of the minimumIncrementalExercisableQuanity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumIncrementalExercisableQuanity() {
        return minimumIncrementalExercisableQuanity;
    }

    /**
     * Sets the value of the minimumIncrementalExercisableQuanity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumIncrementalExercisableQuanity(BigInteger value) {
        this.minimumIncrementalExercisableQuanity = value;
    }

    /**
     * Gets the value of the multiLegNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMultiLegNumber() {
        return multiLegNumber;
    }

    /**
     * Sets the value of the multiLegNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMultiLegNumber(BigInteger value) {
        this.multiLegNumber = value;
    }

    /**
     * Gets the value of the multipleExerciseFlag property.
     * This getter has been renamed from isMultipleExerciseFlag() to getMultipleExerciseFlag() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultipleExerciseFlag() {
        return multipleExerciseFlag;
    }

    /**
     * Sets the value of the multipleExerciseFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultipleExerciseFlag(Boolean value) {
        this.multipleExerciseFlag = value;
    }

}
