
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InsurerLossDataScore_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsurerLossDataScore_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO">
 *       &lt;sequence>
 *         &lt;element name="dataAsOfDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="claimsWithPaymentCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="expectedClaimCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="reportLagClaimCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="lostTimeClaimCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="totalClaimCount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="exposureAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="incurredLossAmount" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="exposureBasis" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsurerLossDataScore_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "dataAsOfDate",
    "claimsWithPaymentCount",
    "expectedClaimCount",
    "reportLagClaimCount",
    "lostTimeClaimCount",
    "totalClaimCount",
    "exposureAmount",
    "incurredLossAmount",
    "exposureBasis"
})
public class InsurerLossDataScoreTO
    extends ScoreTO
{

    protected XMLGregorianCalendar dataAsOfDate;
    protected BigDecimal claimsWithPaymentCount;
    protected BigDecimal expectedClaimCount;
    protected BigDecimal reportLagClaimCount;
    protected BigDecimal lostTimeClaimCount;
    protected BigDecimal totalClaimCount;
    protected BaseCurrencyAmount exposureAmount;
    protected BaseCurrencyAmount incurredLossAmount;
    protected String exposureBasis;

    /**
     * Gets the value of the dataAsOfDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAsOfDate() {
        return dataAsOfDate;
    }

    /**
     * Sets the value of the dataAsOfDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAsOfDate(XMLGregorianCalendar value) {
        this.dataAsOfDate = value;
    }

    /**
     * Gets the value of the claimsWithPaymentCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getClaimsWithPaymentCount() {
        return claimsWithPaymentCount;
    }

    /**
     * Sets the value of the claimsWithPaymentCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setClaimsWithPaymentCount(BigDecimal value) {
        this.claimsWithPaymentCount = value;
    }

    /**
     * Gets the value of the expectedClaimCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExpectedClaimCount() {
        return expectedClaimCount;
    }

    /**
     * Sets the value of the expectedClaimCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExpectedClaimCount(BigDecimal value) {
        this.expectedClaimCount = value;
    }

    /**
     * Gets the value of the reportLagClaimCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReportLagClaimCount() {
        return reportLagClaimCount;
    }

    /**
     * Sets the value of the reportLagClaimCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReportLagClaimCount(BigDecimal value) {
        this.reportLagClaimCount = value;
    }

    /**
     * Gets the value of the lostTimeClaimCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLostTimeClaimCount() {
        return lostTimeClaimCount;
    }

    /**
     * Sets the value of the lostTimeClaimCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLostTimeClaimCount(BigDecimal value) {
        this.lostTimeClaimCount = value;
    }

    /**
     * Gets the value of the totalClaimCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalClaimCount() {
        return totalClaimCount;
    }

    /**
     * Sets the value of the totalClaimCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalClaimCount(BigDecimal value) {
        this.totalClaimCount = value;
    }

    /**
     * Gets the value of the exposureAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getExposureAmount() {
        return exposureAmount;
    }

    /**
     * Sets the value of the exposureAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setExposureAmount(BaseCurrencyAmount value) {
        this.exposureAmount = value;
    }

    /**
     * Gets the value of the incurredLossAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getIncurredLossAmount() {
        return incurredLossAmount;
    }

    /**
     * Sets the value of the incurredLossAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setIncurredLossAmount(BaseCurrencyAmount value) {
        this.incurredLossAmount = value;
    }

    /**
     * Gets the value of the exposureBasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExposureBasis() {
        return exposureBasis;
    }

    /**
     * Sets the value of the exposureBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExposureBasis(String value) {
        this.exposureBasis = value;
    }

}
