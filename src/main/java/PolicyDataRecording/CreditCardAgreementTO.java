
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditCardAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}AccountAgreement_TO">
 *       &lt;sequence>
 *         &lt;element name="minimumMonthlyRepayment" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="endorsement" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}CreditCardEndorsement" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "minimumMonthlyRepayment",
    "endorsement"
})
public class CreditCardAgreementTO
    extends AccountAgreementTO
{

    protected BigDecimal minimumMonthlyRepayment;
    protected CreditCardEndorsement endorsement;

    /**
     * Gets the value of the minimumMonthlyRepayment property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinimumMonthlyRepayment() {
        return minimumMonthlyRepayment;
    }

    /**
     * Sets the value of the minimumMonthlyRepayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinimumMonthlyRepayment(BigDecimal value) {
        this.minimumMonthlyRepayment = value;
    }

    /**
     * Gets the value of the endorsement property.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardEndorsement }
     *     
     */
    public CreditCardEndorsement getEndorsement() {
        return endorsement;
    }

    /**
     * Sets the value of the endorsement property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardEndorsement }
     *     
     */
    public void setEndorsement(CreditCardEndorsement value) {
        this.endorsement = value;
    }

}
