
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Liquidity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Liquidity">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Fixed Assets"/>
 *     &lt;enumeration value="Liquid Assets"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Liquidity", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum Liquidity {

    @XmlEnumValue("Fixed Assets")
    FIXED_ASSETS("Fixed Assets"),
    @XmlEnumValue("Liquid Assets")
    LIQUID_ASSETS("Liquid Assets");
    private final String value;

    Liquidity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Liquidity fromValue(String v) {
        for (Liquidity c: Liquidity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
