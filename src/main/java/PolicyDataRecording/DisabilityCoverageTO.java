
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for DisabilityCoverage_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DisabilityCoverage_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}CoverageComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="waitingPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="ownRiskPeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *         &lt;element name="lifeTimeMaximumBenefit" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DisabilityCoverage_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "waitingPeriod",
    "ownRiskPeriod",
    "lifeTimeMaximumBenefit"
})
public class DisabilityCoverageTO
    extends CoverageComponentTO
{

    protected Duration waitingPeriod;
    protected Duration ownRiskPeriod;
    protected BaseCurrencyAmount lifeTimeMaximumBenefit;

    /**
     * Gets the value of the waitingPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getWaitingPeriod() {
        return waitingPeriod;
    }

    /**
     * Sets the value of the waitingPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setWaitingPeriod(Duration value) {
        this.waitingPeriod = value;
    }

    /**
     * Gets the value of the ownRiskPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getOwnRiskPeriod() {
        return ownRiskPeriod;
    }

    /**
     * Sets the value of the ownRiskPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setOwnRiskPeriod(Duration value) {
        this.ownRiskPeriod = value;
    }

    /**
     * Gets the value of the lifeTimeMaximumBenefit property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getLifeTimeMaximumBenefit() {
        return lifeTimeMaximumBenefit;
    }

    /**
     * Sets the value of the lifeTimeMaximumBenefit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setLifeTimeMaximumBenefit(BaseCurrencyAmount value) {
        this.lifeTimeMaximumBenefit = value;
    }

}
