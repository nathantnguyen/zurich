
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for financialservicesproductcomposition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="financialservicesproductcomposition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}contractspecificationcomposition_TO">
 *       &lt;sequence>
 *         &lt;element name="unbundlingIndicator" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="productComponent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}FinancialServicesProductComponent_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "financialservicesproductcomposition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "unbundlingIndicator",
    "productComponent"
})
public class FinancialservicesproductcompositionTO
    extends ContractspecificationcompositionTO
{

    protected Boolean unbundlingIndicator;
    protected FinancialServicesProductComponentTO productComponent;

    /**
     * Gets the value of the unbundlingIndicator property.
     * This getter has been renamed from isUnbundlingIndicator() to getUnbundlingIndicator() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getUnbundlingIndicator() {
        return unbundlingIndicator;
    }

    /**
     * Sets the value of the unbundlingIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnbundlingIndicator(Boolean value) {
        this.unbundlingIndicator = value;
    }

    /**
     * Gets the value of the productComponent property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesProductComponentTO }
     *     
     */
    public FinancialServicesProductComponentTO getProductComponent() {
        return productComponent;
    }

    /**
     * Sets the value of the productComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesProductComponentTO }
     *     
     */
    public void setProductComponent(FinancialServicesProductComponentTO value) {
        this.productComponent = value;
    }

}
