
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanInterestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LoanInterestType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Adjustable Loan Interest Rate"/>
 *     &lt;enumeration value="Fixed Loan Interest Rate"/>
 *     &lt;enumeration value="Indexed Loan Interest Rate"/>
 *     &lt;enumeration value="Variable Loan Interest Rate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LoanInterestType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum LoanInterestType {

    @XmlEnumValue("Adjustable Loan Interest Rate")
    ADJUSTABLE_LOAN_INTEREST_RATE("Adjustable Loan Interest Rate"),
    @XmlEnumValue("Fixed Loan Interest Rate")
    FIXED_LOAN_INTEREST_RATE("Fixed Loan Interest Rate"),
    @XmlEnumValue("Indexed Loan Interest Rate")
    INDEXED_LOAN_INTEREST_RATE("Indexed Loan Interest Rate"),
    @XmlEnumValue("Variable Loan Interest Rate")
    VARIABLE_LOAN_INTEREST_RATE("Variable Loan Interest Rate");
    private final String value;

    LoanInterestType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LoanInterestType fromValue(String v) {
        for (LoanInterestType c: LoanInterestType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
