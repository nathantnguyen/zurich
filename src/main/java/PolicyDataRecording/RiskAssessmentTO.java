
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskAssessment_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RiskAssessment_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}AssessmentActivity_TO">
 *       &lt;sequence>
 *         &lt;element name="riskScenario" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/EnterpriseRisk/EnterpriseRiskEnumerationsAndStates/}AssessmentScenarioSet" minOccurs="0"/>
 *         &lt;element name="variancePeriod" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="riskAgreement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RiskAgreement_TO" minOccurs="0"/>
 *         &lt;element name="rolesInRiskAssessment" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}RoleInRiskAssessment_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RiskAssessment_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "riskScenario",
    "variancePeriod",
    "riskAgreement",
    "rolesInRiskAssessment"
})
public class RiskAssessmentTO
    extends AssessmentActivityTO
{

    protected AssessmentScenarioSet riskScenario;
    protected Amount variancePeriod;
    protected RiskAgreementTO riskAgreement;
    protected List<RoleInRiskAssessmentTO> rolesInRiskAssessment;

    /**
     * Gets the value of the riskScenario property.
     * 
     * @return
     *     possible object is
     *     {@link AssessmentScenarioSet }
     *     
     */
    public AssessmentScenarioSet getRiskScenario() {
        return riskScenario;
    }

    /**
     * Sets the value of the riskScenario property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssessmentScenarioSet }
     *     
     */
    public void setRiskScenario(AssessmentScenarioSet value) {
        this.riskScenario = value;
    }

    /**
     * Gets the value of the variancePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getVariancePeriod() {
        return variancePeriod;
    }

    /**
     * Sets the value of the variancePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setVariancePeriod(Amount value) {
        this.variancePeriod = value;
    }

    /**
     * Gets the value of the riskAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link RiskAgreementTO }
     *     
     */
    public RiskAgreementTO getRiskAgreement() {
        return riskAgreement;
    }

    /**
     * Sets the value of the riskAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link RiskAgreementTO }
     *     
     */
    public void setRiskAgreement(RiskAgreementTO value) {
        this.riskAgreement = value;
    }

    /**
     * Gets the value of the rolesInRiskAssessment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rolesInRiskAssessment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRolesInRiskAssessment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleInRiskAssessmentTO }
     * 
     * 
     */
    public List<RoleInRiskAssessmentTO> getRolesInRiskAssessment() {
        if (rolesInRiskAssessment == null) {
            rolesInRiskAssessment = new ArrayList<RoleInRiskAssessmentTO>();
        }
        return this.rolesInRiskAssessment;
    }

    /**
     * Sets the value of the rolesInRiskAssessment property.
     * 
     * @param rolesInRiskAssessment
     *     allowed object is
     *     {@link RoleInRiskAssessmentTO }
     *     
     */
    public void setRolesInRiskAssessment(List<RoleInRiskAssessmentTO> rolesInRiskAssessment) {
        this.rolesInRiskAssessment = rolesInRiskAssessment;
    }

}
