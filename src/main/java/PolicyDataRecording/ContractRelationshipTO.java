
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractRelationship_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractRelationship_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Relationship_TO">
 *       &lt;sequence>
 *         &lt;element name="sourceContract" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO" minOccurs="0"/>
 *         &lt;element name="targetContract" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO" minOccurs="0"/>
 *         &lt;element name="contractRelationshipRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}ContractRelationshipType_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractRelationship_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "sourceContract",
    "targetContract",
    "contractRelationshipRootType"
})
public class ContractRelationshipTO
    extends RelationshipTO
{

    protected ContractTO sourceContract;
    protected ContractTO targetContract;
    protected ContractRelationshipTypeTO contractRelationshipRootType;

    /**
     * Gets the value of the sourceContract property.
     * 
     * @return
     *     possible object is
     *     {@link ContractTO }
     *     
     */
    public ContractTO getSourceContract() {
        return sourceContract;
    }

    /**
     * Sets the value of the sourceContract property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractTO }
     *     
     */
    public void setSourceContract(ContractTO value) {
        this.sourceContract = value;
    }

    /**
     * Gets the value of the targetContract property.
     * 
     * @return
     *     possible object is
     *     {@link ContractTO }
     *     
     */
    public ContractTO getTargetContract() {
        return targetContract;
    }

    /**
     * Sets the value of the targetContract property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractTO }
     *     
     */
    public void setTargetContract(ContractTO value) {
        this.targetContract = value;
    }

    /**
     * Gets the value of the contractRelationshipRootType property.
     * 
     * @return
     *     possible object is
     *     {@link ContractRelationshipTypeTO }
     *     
     */
    public ContractRelationshipTypeTO getContractRelationshipRootType() {
        return contractRelationshipRootType;
    }

    /**
     * Sets the value of the contractRelationshipRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractRelationshipTypeTO }
     *     
     */
    public void setContractRelationshipRootType(ContractRelationshipTypeTO value) {
        this.contractRelationshipRootType = value;
    }

}
