
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanComponent_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoanComponent_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}FinancialServicesAgreementComponent_TO">
 *       &lt;sequence>
 *         &lt;element name="repaymentType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/}RepaymentType" minOccurs="0"/>
 *         &lt;element name="purpose" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}LoanPurpose" minOccurs="0"/>
 *         &lt;element name="loanInterestMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}LoanInterestPaymentMethod" minOccurs="0"/>
 *         &lt;element name="currentInterestRate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="accumulatedLoanInterestPaid" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="moneyLent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="loanInterestType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/}LoanInterestType" minOccurs="0"/>
 *         &lt;element name="repayment" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvision_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="loanInterestSinceLastAnniversary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="lentLoanComponent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}LoanComponent_TO" minOccurs="0"/>
 *         &lt;element name="repaidLoanComponent" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}LoanComponent_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanComponent_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "repaymentType",
    "purpose",
    "loanInterestMethod",
    "currentInterestRate",
    "accumulatedLoanInterestPaid",
    "moneyLent",
    "loanInterestType",
    "repayment",
    "loanInterestSinceLastAnniversary",
    "lentLoanComponent",
    "repaidLoanComponent"
})
public class LoanComponentTO
    extends FinancialServicesAgreementComponentTO
{

    protected RepaymentType repaymentType;
    protected LoanPurpose purpose;
    protected LoanInterestPaymentMethod loanInterestMethod;
    protected BigDecimal currentInterestRate;
    protected BaseCurrencyAmount accumulatedLoanInterestPaid;
    protected List<MoneyProvisionTO> moneyLent;
    protected LoanInterestType loanInterestType;
    protected List<MoneyProvisionTO> repayment;
    protected BaseCurrencyAmount loanInterestSinceLastAnniversary;
    protected LoanComponentTO lentLoanComponent;
    protected LoanComponentTO repaidLoanComponent;

    /**
     * Gets the value of the repaymentType property.
     * 
     * @return
     *     possible object is
     *     {@link RepaymentType }
     *     
     */
    public RepaymentType getRepaymentType() {
        return repaymentType;
    }

    /**
     * Sets the value of the repaymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RepaymentType }
     *     
     */
    public void setRepaymentType(RepaymentType value) {
        this.repaymentType = value;
    }

    /**
     * Gets the value of the purpose property.
     * 
     * @return
     *     possible object is
     *     {@link LoanPurpose }
     *     
     */
    public LoanPurpose getPurpose() {
        return purpose;
    }

    /**
     * Sets the value of the purpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanPurpose }
     *     
     */
    public void setPurpose(LoanPurpose value) {
        this.purpose = value;
    }

    /**
     * Gets the value of the loanInterestMethod property.
     * 
     * @return
     *     possible object is
     *     {@link LoanInterestPaymentMethod }
     *     
     */
    public LoanInterestPaymentMethod getLoanInterestMethod() {
        return loanInterestMethod;
    }

    /**
     * Sets the value of the loanInterestMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanInterestPaymentMethod }
     *     
     */
    public void setLoanInterestMethod(LoanInterestPaymentMethod value) {
        this.loanInterestMethod = value;
    }

    /**
     * Gets the value of the currentInterestRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrentInterestRate() {
        return currentInterestRate;
    }

    /**
     * Sets the value of the currentInterestRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrentInterestRate(BigDecimal value) {
        this.currentInterestRate = value;
    }

    /**
     * Gets the value of the accumulatedLoanInterestPaid property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getAccumulatedLoanInterestPaid() {
        return accumulatedLoanInterestPaid;
    }

    /**
     * Sets the value of the accumulatedLoanInterestPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setAccumulatedLoanInterestPaid(BaseCurrencyAmount value) {
        this.accumulatedLoanInterestPaid = value;
    }

    /**
     * Gets the value of the moneyLent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moneyLent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoneyLent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionTO }
     * 
     * 
     */
    public List<MoneyProvisionTO> getMoneyLent() {
        if (moneyLent == null) {
            moneyLent = new ArrayList<MoneyProvisionTO>();
        }
        return this.moneyLent;
    }

    /**
     * Gets the value of the loanInterestType property.
     * 
     * @return
     *     possible object is
     *     {@link LoanInterestType }
     *     
     */
    public LoanInterestType getLoanInterestType() {
        return loanInterestType;
    }

    /**
     * Sets the value of the loanInterestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanInterestType }
     *     
     */
    public void setLoanInterestType(LoanInterestType value) {
        this.loanInterestType = value;
    }

    /**
     * Gets the value of the repayment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the repayment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRepayment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionTO }
     * 
     * 
     */
    public List<MoneyProvisionTO> getRepayment() {
        if (repayment == null) {
            repayment = new ArrayList<MoneyProvisionTO>();
        }
        return this.repayment;
    }

    /**
     * Gets the value of the loanInterestSinceLastAnniversary property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getLoanInterestSinceLastAnniversary() {
        return loanInterestSinceLastAnniversary;
    }

    /**
     * Sets the value of the loanInterestSinceLastAnniversary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setLoanInterestSinceLastAnniversary(BaseCurrencyAmount value) {
        this.loanInterestSinceLastAnniversary = value;
    }

    /**
     * Gets the value of the lentLoanComponent property.
     * 
     * @return
     *     possible object is
     *     {@link LoanComponentTO }
     *     
     */
    public LoanComponentTO getLentLoanComponent() {
        return lentLoanComponent;
    }

    /**
     * Sets the value of the lentLoanComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanComponentTO }
     *     
     */
    public void setLentLoanComponent(LoanComponentTO value) {
        this.lentLoanComponent = value;
    }

    /**
     * Gets the value of the repaidLoanComponent property.
     * 
     * @return
     *     possible object is
     *     {@link LoanComponentTO }
     *     
     */
    public LoanComponentTO getRepaidLoanComponent() {
        return repaidLoanComponent;
    }

    /**
     * Sets the value of the repaidLoanComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanComponentTO }
     *     
     */
    public void setRepaidLoanComponent(LoanComponentTO value) {
        this.repaidLoanComponent = value;
    }

    /**
     * Sets the value of the moneyLent property.
     * 
     * @param moneyLent
     *     allowed object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public void setMoneyLent(List<MoneyProvisionTO> moneyLent) {
        this.moneyLent = moneyLent;
    }

    /**
     * Sets the value of the repayment property.
     * 
     * @param repayment
     *     allowed object is
     *     {@link MoneyProvisionTO }
     *     
     */
    public void setRepayment(List<MoneyProvisionTO> repayment) {
        this.repayment = repayment;
    }

}
