
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeathBenefitPaymentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DeathBenefitPaymentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Guaranteed Income"/>
 *     &lt;enumeration value="Installment Refund"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DeathBenefitPaymentType", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum DeathBenefitPaymentType {

    @XmlEnumValue("Guaranteed Income")
    GUARANTEED_INCOME("Guaranteed Income"),
    @XmlEnumValue("Installment Refund")
    INSTALLMENT_REFUND("Installment Refund");
    private final String value;

    DeathBenefitPaymentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeathBenefitPaymentType fromValue(String v) {
        for (DeathBenefitPaymentType c: DeathBenefitPaymentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
