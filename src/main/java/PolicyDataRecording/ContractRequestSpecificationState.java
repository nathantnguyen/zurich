
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractRequestSpecificationState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContractRequestSpecificationState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Defined"/>
 *     &lt;enumeration value="Draft"/>
 *     &lt;enumeration value="Effective"/>
 *     &lt;enumeration value="Final"/>
 *     &lt;enumeration value="Initial"/>
 *     &lt;enumeration value="Terminated"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContractRequestSpecificationState", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ContractAndSpecification/ContractAndSpecificationEnumerationsAndStates/")
@XmlEnum
public enum ContractRequestSpecificationState {

    @XmlEnumValue("Defined")
    DEFINED("Defined"),
    @XmlEnumValue("Draft")
    DRAFT("Draft"),
    @XmlEnumValue("Effective")
    EFFECTIVE("Effective"),
    @XmlEnumValue("Final")
    FINAL("Final"),
    @XmlEnumValue("Initial")
    INITIAL("Initial"),
    @XmlEnumValue("Terminated")
    TERMINATED("Terminated");
    private final String value;

    ContractRequestSpecificationState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContractRequestSpecificationState fromValue(String v) {
        for (ContractRequestSpecificationState c: ContractRequestSpecificationState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
