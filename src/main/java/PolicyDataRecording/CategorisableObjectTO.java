
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CategorisableObject_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CategorisableObject_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/}BaseTransferObject">
 *       &lt;sequence>
 *         &lt;element name="objectReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" minOccurs="0"/>
 *         &lt;element name="objectCategories" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategorisableObject_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/", propOrder = {
    "objectReference",
    "objectCategories"
})
@XmlSeeAlso({
    RequestBehaviourSpecTO.class,
    NavigationPathTO.class,
    ExpressionSpecTO.class,
    AlgorithmTO.class,
    SpecificationBuildingBlockTO.class,
    BusinessModelObjectTO.class
})
public class CategorisableObjectTO
    extends BaseTransferObject
{

    protected ObjectReferenceTO objectReference;
    protected List<CategoryTO> objectCategories;

    /**
     * Gets the value of the objectReference property.
     * 
     * @return
     *     possible object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public ObjectReferenceTO getObjectReference() {
        return objectReference;
    }

    /**
     * Sets the value of the objectReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setObjectReference(ObjectReferenceTO value) {
        this.objectReference = value;
    }

    /**
     * Gets the value of the objectCategories property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectCategories property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectCategories().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getObjectCategories() {
        if (objectCategories == null) {
            objectCategories = new ArrayList<CategoryTO>();
        }
        return this.objectCategories;
    }

    /**
     * Sets the value of the objectCategories property.
     * 
     * @param objectCategories
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setObjectCategories(List<CategoryTO> objectCategories) {
        this.objectCategories = objectCategories;
    }

}
