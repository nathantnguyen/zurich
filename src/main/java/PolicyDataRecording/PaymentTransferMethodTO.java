
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentTransferMethod_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentTransferMethod_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="settlementMethod" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialTransaction/FinancialTransactionEnumerationsAndStates/}SettlementMethod" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="memo" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="format" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="organisationContact" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Person_TO" minOccurs="0"/>
 *         &lt;element name="country" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="fee" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}MoneyProvisionElementPart_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentTransferMethod_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/", propOrder = {
    "settlementMethod",
    "externalReference",
    "memo",
    "format",
    "organisationContact",
    "country",
    "fee",
    "alternateReference"
})
public class PaymentTransferMethodTO
    extends BusinessModelObjectTO
{

    protected SettlementMethod settlementMethod;
    protected String externalReference;
    protected String memo;
    protected String format;
    protected PersonTO organisationContact;
    protected String country;
    protected List<MoneyProvisionElementPartTO> fee;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the settlementMethod property.
     * 
     * @return
     *     possible object is
     *     {@link SettlementMethod }
     *     
     */
    public SettlementMethod getSettlementMethod() {
        return settlementMethod;
    }

    /**
     * Sets the value of the settlementMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link SettlementMethod }
     *     
     */
    public void setSettlementMethod(SettlementMethod value) {
        this.settlementMethod = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the memo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemo() {
        return memo;
    }

    /**
     * Sets the value of the memo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemo(String value) {
        this.memo = value;
    }

    /**
     * Gets the value of the format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the value of the format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormat(String value) {
        this.format = value;
    }

    /**
     * Gets the value of the organisationContact property.
     * 
     * @return
     *     possible object is
     *     {@link PersonTO }
     *     
     */
    public PersonTO getOrganisationContact() {
        return organisationContact;
    }

    /**
     * Sets the value of the organisationContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonTO }
     *     
     */
    public void setOrganisationContact(PersonTO value) {
        this.organisationContact = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the fee property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fee property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFee().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyProvisionElementPartTO }
     * 
     * 
     */
    public List<MoneyProvisionElementPartTO> getFee() {
        if (fee == null) {
            fee = new ArrayList<MoneyProvisionElementPartTO>();
        }
        return this.fee;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the fee property.
     * 
     * @param fee
     *     allowed object is
     *     {@link MoneyProvisionElementPartTO }
     *     
     */
    public void setFee(List<MoneyProvisionElementPartTO> fee) {
        this.fee = fee;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
