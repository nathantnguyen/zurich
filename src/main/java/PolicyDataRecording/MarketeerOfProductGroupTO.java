
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketeerOfProductGroup_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketeerOfProductGroup_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}RoleInProductGroup_TO">
 *       &lt;sequence>
 *         &lt;element name="primaryBusiness" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketeerOfProductGroup_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/", propOrder = {
    "primaryBusiness"
})
public class MarketeerOfProductGroupTO
    extends RoleInProductGroupTO
{

    protected Boolean primaryBusiness;

    /**
     * Gets the value of the primaryBusiness property.
     * This getter has been renamed from isPrimaryBusiness() to getPrimaryBusiness() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPrimaryBusiness() {
        return primaryBusiness;
    }

    /**
     * Sets the value of the primaryBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimaryBusiness(Boolean value) {
        this.primaryBusiness = value;
    }

}
