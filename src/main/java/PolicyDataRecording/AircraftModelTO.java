
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AircraftModel_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AircraftModel_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}VehicleModel_TO">
 *       &lt;sequence>
 *         &lt;element name="aircraftSeries" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="isRatedForInstrumentalFlight" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AircraftModel_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "aircraftSeries",
    "isRatedForInstrumentalFlight"
})
public class AircraftModelTO
    extends VehicleModelTO
{

    protected String aircraftSeries;
    protected Boolean isRatedForInstrumentalFlight;

    /**
     * Gets the value of the aircraftSeries property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAircraftSeries() {
        return aircraftSeries;
    }

    /**
     * Sets the value of the aircraftSeries property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAircraftSeries(String value) {
        this.aircraftSeries = value;
    }

    /**
     * Gets the value of the isRatedForInstrumentalFlight property.
     * This getter has been renamed from isIsRatedForInstrumentalFlight() to getIsRatedForInstrumentalFlight() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsRatedForInstrumentalFlight() {
        return isRatedForInstrumentalFlight;
    }

    /**
     * Sets the value of the isRatedForInstrumentalFlight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRatedForInstrumentalFlight(Boolean value) {
        this.isRatedForInstrumentalFlight = value;
    }

}
