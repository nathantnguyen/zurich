
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * <p>Java class for ContactLimitation_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactLimitation_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="maximumContacts" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Byte" minOccurs="0"/>
 *         &lt;element name="maximumActiveContacts" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Byte" minOccurs="0"/>
 *         &lt;element name="period" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}TimePeriod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactLimitation_TO", propOrder = {
    "maximumContacts",
    "maximumActiveContacts",
    "period"
})
public class ContactLimitationTO
    extends DependentObjectTO
{

    protected Byte maximumContacts;
    protected Byte maximumActiveContacts;
    protected Duration period;

    /**
     * Gets the value of the maximumContacts property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getMaximumContacts() {
        return maximumContacts;
    }

    /**
     * Sets the value of the maximumContacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setMaximumContacts(Byte value) {
        this.maximumContacts = value;
    }

    /**
     * Gets the value of the maximumActiveContacts property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getMaximumActiveContacts() {
        return maximumActiveContacts;
    }

    /**
     * Sets the value of the maximumActiveContacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setMaximumActiveContacts(Byte value) {
        this.maximumActiveContacts = value;
    }

    /**
     * Gets the value of the period property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getPeriod() {
        return period;
    }

    /**
     * Sets the value of the period property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setPeriod(Duration value) {
        this.period = value;
    }

}
