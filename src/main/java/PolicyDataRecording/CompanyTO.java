
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Company_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Company_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO">
 *       &lt;sequence>
 *         &lt;element name="turnover" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Percentage" minOccurs="0"/>
 *         &lt;element name="franchised" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="fiscalYearEnd" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="legalForm" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}LegalForm" minOccurs="0"/>
 *         &lt;element name="profitAfterTax" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="revenue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="numberOfCustomers" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="companyDetail" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}CompanyDetail_TO" minOccurs="0"/>
 *         &lt;element name="salesRevenue" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Company_TO", propOrder = {
    "turnover",
    "franchised",
    "fiscalYearEnd",
    "legalForm",
    "profitAfterTax",
    "revenue",
    "numberOfCustomers",
    "companyDetail",
    "salesRevenue"
})
public class CompanyTO
    extends OrganisationTO
{

    protected BigDecimal turnover;
    protected Boolean franchised;
    protected String fiscalYearEnd;
    protected LegalForm legalForm;
    protected BaseCurrencyAmount profitAfterTax;
    protected BaseCurrencyAmount revenue;
    protected BigInteger numberOfCustomers;
    protected CompanyDetailTO companyDetail;
    protected BaseCurrencyAmount salesRevenue;

    /**
     * Gets the value of the turnover property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTurnover() {
        return turnover;
    }

    /**
     * Sets the value of the turnover property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTurnover(BigDecimal value) {
        this.turnover = value;
    }

    /**
     * Gets the value of the franchised property.
     * This getter has been renamed from isFranchised() to getFranchised() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getFranchised() {
        return franchised;
    }

    /**
     * Sets the value of the franchised property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFranchised(Boolean value) {
        this.franchised = value;
    }

    /**
     * Gets the value of the fiscalYearEnd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiscalYearEnd() {
        return fiscalYearEnd;
    }

    /**
     * Sets the value of the fiscalYearEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiscalYearEnd(String value) {
        this.fiscalYearEnd = value;
    }

    /**
     * Gets the value of the legalForm property.
     * 
     * @return
     *     possible object is
     *     {@link LegalForm }
     *     
     */
    public LegalForm getLegalForm() {
        return legalForm;
    }

    /**
     * Sets the value of the legalForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link LegalForm }
     *     
     */
    public void setLegalForm(LegalForm value) {
        this.legalForm = value;
    }

    /**
     * Gets the value of the profitAfterTax property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getProfitAfterTax() {
        return profitAfterTax;
    }

    /**
     * Sets the value of the profitAfterTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setProfitAfterTax(BaseCurrencyAmount value) {
        this.profitAfterTax = value;
    }

    /**
     * Gets the value of the revenue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getRevenue() {
        return revenue;
    }

    /**
     * Sets the value of the revenue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setRevenue(BaseCurrencyAmount value) {
        this.revenue = value;
    }

    /**
     * Gets the value of the numberOfCustomers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCustomers() {
        return numberOfCustomers;
    }

    /**
     * Sets the value of the numberOfCustomers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCustomers(BigInteger value) {
        this.numberOfCustomers = value;
    }

    /**
     * Gets the value of the companyDetail property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyDetailTO }
     *     
     */
    public CompanyDetailTO getCompanyDetail() {
        return companyDetail;
    }

    /**
     * Sets the value of the companyDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyDetailTO }
     *     
     */
    public void setCompanyDetail(CompanyDetailTO value) {
        this.companyDetail = value;
    }

    /**
     * Gets the value of the salesRevenue property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getSalesRevenue() {
        return salesRevenue;
    }

    /**
     * Sets the value of the salesRevenue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setSalesRevenue(BaseCurrencyAmount value) {
        this.salesRevenue = value;
    }

}
