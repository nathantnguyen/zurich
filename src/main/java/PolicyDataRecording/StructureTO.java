
package PolicyDataRecording;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Structure_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Structure_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO">
 *       &lt;sequence>
 *         &lt;element name="constructionMaterial" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}ConstructionMaterial" minOccurs="0"/>
 *         &lt;element name="numberOfFloors" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="constructionType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="constructionStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}ConstructionStatus" minOccurs="0"/>
 *         &lt;element name="numberOfRooms" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="restrictionStatus" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}RestrictionStatus" minOccurs="0"/>
 *         &lt;element name="occupationType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}OccupationType" minOccurs="0"/>
 *         &lt;element name="designStyle" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}DesignStyle" minOccurs="0"/>
 *         &lt;element name="doorLocks" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="windowLocks" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="safe" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="roofType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}RoofType" minOccurs="0"/>
 *         &lt;element name="hydrantDistance" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="tenancy" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Tenancy" minOccurs="0"/>
 *         &lt;element name="basementArea" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="centralAirConditioning" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="constructionQuality" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}ConstructionQuality" minOccurs="0"/>
 *         &lt;element name="gradeFloorArea" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="numberOfElevators" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="numberOfHabitableUnits" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="occupiedArea" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="parkingLotArea" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="percentageOfBuildingSprinklered" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="fireProtectionClass" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}FireProtectionClass" minOccurs="0"/>
 *         &lt;element name="size" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="heatingType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}HeatingType" minOccurs="0"/>
 *         &lt;element name="plumbingMaterial" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}PlumbingMaterial" minOccurs="0"/>
 *         &lt;element name="structureType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="shelteredObjects" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="historicHomeDistrict" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="occupancy" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}BuildingOccupancy" minOccurs="0"/>
 *         &lt;element name="isVacant" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="date" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="hasBurglerAlarm" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Alarm" minOccurs="0"/>
 *         &lt;element name="hasFireAlarm" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Alarm" minOccurs="0"/>
 *         &lt;element name="numberOfSmokingOccupiers" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="yearHeatingUpgraded" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="yearRoofUpgraded" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="yearElectricUpgraded" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="yearAirConditioningUpgraded" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="yearPlumbingUpgraded" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="floodDefence" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="manufactoredObjects" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalObject_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Structure_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "constructionMaterial",
    "numberOfFloors",
    "constructionType",
    "constructionStatus",
    "numberOfRooms",
    "restrictionStatus",
    "occupationType",
    "designStyle",
    "doorLocks",
    "windowLocks",
    "safe",
    "roofType",
    "hydrantDistance",
    "tenancy",
    "basementArea",
    "centralAirConditioning",
    "constructionQuality",
    "gradeFloorArea",
    "numberOfElevators",
    "numberOfHabitableUnits",
    "occupiedArea",
    "parkingLotArea",
    "percentageOfBuildingSprinklered",
    "fireProtectionClass",
    "size",
    "heatingType",
    "plumbingMaterial",
    "structureType",
    "shelteredObjects",
    "historicHomeDistrict",
    "occupancy",
    "isVacant",
    "date",
    "hasBurglerAlarm",
    "hasFireAlarm",
    "numberOfSmokingOccupiers",
    "yearHeatingUpgraded",
    "yearRoofUpgraded",
    "yearElectricUpgraded",
    "yearAirConditioningUpgraded",
    "yearPlumbingUpgraded",
    "floodDefence",
    "manufactoredObjects"
})
@XmlSeeAlso({
    OfficeBlockTO.class,
    GarageTO.class,
    DwellingTO.class
})
public class StructureTO
    extends PhysicalObjectTO
{

    protected ConstructionMaterial constructionMaterial;
    protected BigInteger numberOfFloors;
    protected String constructionType;
    protected ConstructionStatus constructionStatus;
    protected BigDecimal numberOfRooms;
    protected RestrictionStatus restrictionStatus;
    protected OccupationType occupationType;
    protected DesignStyle designStyle;
    protected Boolean doorLocks;
    protected Boolean windowLocks;
    protected Boolean safe;
    protected RoofType roofType;
    protected BigDecimal hydrantDistance;
    protected Tenancy tenancy;
    protected Amount basementArea;
    protected Boolean centralAirConditioning;
    protected ConstructionQuality constructionQuality;
    protected Amount gradeFloorArea;
    protected String numberOfElevators;
    protected String numberOfHabitableUnits;
    protected Amount occupiedArea;
    protected Amount parkingLotArea;
    protected String percentageOfBuildingSprinklered;
    protected String fireProtectionClass;
    protected Amount size;
    protected HeatingType heatingType;
    protected PlumbingMaterial plumbingMaterial;
    protected String structureType;
    protected List<PhysicalObjectTO> shelteredObjects;
    protected Boolean historicHomeDistrict;
    protected BuildingOccupancy occupancy;
    protected Boolean isVacant;
    protected XMLGregorianCalendar date;
    protected Alarm hasBurglerAlarm;
    protected Alarm hasFireAlarm;
    protected BigInteger numberOfSmokingOccupiers;
    protected XMLGregorianCalendar yearHeatingUpgraded;
    protected XMLGregorianCalendar yearRoofUpgraded;
    protected XMLGregorianCalendar yearElectricUpgraded;
    protected XMLGregorianCalendar yearAirConditioningUpgraded;
    protected XMLGregorianCalendar yearPlumbingUpgraded;
    protected String floodDefence;
    protected List<PhysicalObjectTO> manufactoredObjects;

    /**
     * Gets the value of the constructionMaterial property.
     * 
     * @return
     *     possible object is
     *     {@link ConstructionMaterial }
     *     
     */
    public ConstructionMaterial getConstructionMaterial() {
        return constructionMaterial;
    }

    /**
     * Sets the value of the constructionMaterial property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstructionMaterial }
     *     
     */
    public void setConstructionMaterial(ConstructionMaterial value) {
        this.constructionMaterial = value;
    }

    /**
     * Gets the value of the numberOfFloors property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfFloors() {
        return numberOfFloors;
    }

    /**
     * Sets the value of the numberOfFloors property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfFloors(BigInteger value) {
        this.numberOfFloors = value;
    }

    /**
     * Gets the value of the constructionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstructionType() {
        return constructionType;
    }

    /**
     * Sets the value of the constructionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstructionType(String value) {
        this.constructionType = value;
    }

    /**
     * Gets the value of the constructionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ConstructionStatus }
     *     
     */
    public ConstructionStatus getConstructionStatus() {
        return constructionStatus;
    }

    /**
     * Sets the value of the constructionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstructionStatus }
     *     
     */
    public void setConstructionStatus(ConstructionStatus value) {
        this.constructionStatus = value;
    }

    /**
     * Gets the value of the numberOfRooms property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumberOfRooms() {
        return numberOfRooms;
    }

    /**
     * Sets the value of the numberOfRooms property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumberOfRooms(BigDecimal value) {
        this.numberOfRooms = value;
    }

    /**
     * Gets the value of the restrictionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictionStatus }
     *     
     */
    public RestrictionStatus getRestrictionStatus() {
        return restrictionStatus;
    }

    /**
     * Sets the value of the restrictionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictionStatus }
     *     
     */
    public void setRestrictionStatus(RestrictionStatus value) {
        this.restrictionStatus = value;
    }

    /**
     * Gets the value of the occupationType property.
     * 
     * @return
     *     possible object is
     *     {@link OccupationType }
     *     
     */
    public OccupationType getOccupationType() {
        return occupationType;
    }

    /**
     * Sets the value of the occupationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link OccupationType }
     *     
     */
    public void setOccupationType(OccupationType value) {
        this.occupationType = value;
    }

    /**
     * Gets the value of the designStyle property.
     * 
     * @return
     *     possible object is
     *     {@link DesignStyle }
     *     
     */
    public DesignStyle getDesignStyle() {
        return designStyle;
    }

    /**
     * Sets the value of the designStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link DesignStyle }
     *     
     */
    public void setDesignStyle(DesignStyle value) {
        this.designStyle = value;
    }

    /**
     * Gets the value of the doorLocks property.
     * This getter has been renamed from isDoorLocks() to getDoorLocks() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDoorLocks() {
        return doorLocks;
    }

    /**
     * Sets the value of the doorLocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoorLocks(Boolean value) {
        this.doorLocks = value;
    }

    /**
     * Gets the value of the windowLocks property.
     * This getter has been renamed from isWindowLocks() to getWindowLocks() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getWindowLocks() {
        return windowLocks;
    }

    /**
     * Sets the value of the windowLocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWindowLocks(Boolean value) {
        this.windowLocks = value;
    }

    /**
     * Gets the value of the safe property.
     * This getter has been renamed from isSafe() to getSafe() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSafe() {
        return safe;
    }

    /**
     * Sets the value of the safe property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSafe(Boolean value) {
        this.safe = value;
    }

    /**
     * Gets the value of the roofType property.
     * 
     * @return
     *     possible object is
     *     {@link RoofType }
     *     
     */
    public RoofType getRoofType() {
        return roofType;
    }

    /**
     * Sets the value of the roofType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoofType }
     *     
     */
    public void setRoofType(RoofType value) {
        this.roofType = value;
    }

    /**
     * Gets the value of the hydrantDistance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHydrantDistance() {
        return hydrantDistance;
    }

    /**
     * Sets the value of the hydrantDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHydrantDistance(BigDecimal value) {
        this.hydrantDistance = value;
    }

    /**
     * Gets the value of the tenancy property.
     * 
     * @return
     *     possible object is
     *     {@link Tenancy }
     *     
     */
    public Tenancy getTenancy() {
        return tenancy;
    }

    /**
     * Sets the value of the tenancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Tenancy }
     *     
     */
    public void setTenancy(Tenancy value) {
        this.tenancy = value;
    }

    /**
     * Gets the value of the basementArea property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getBasementArea() {
        return basementArea;
    }

    /**
     * Sets the value of the basementArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setBasementArea(Amount value) {
        this.basementArea = value;
    }

    /**
     * Gets the value of the centralAirConditioning property.
     * This getter has been renamed from isCentralAirConditioning() to getCentralAirConditioning() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCentralAirConditioning() {
        return centralAirConditioning;
    }

    /**
     * Sets the value of the centralAirConditioning property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCentralAirConditioning(Boolean value) {
        this.centralAirConditioning = value;
    }

    /**
     * Gets the value of the constructionQuality property.
     * 
     * @return
     *     possible object is
     *     {@link ConstructionQuality }
     *     
     */
    public ConstructionQuality getConstructionQuality() {
        return constructionQuality;
    }

    /**
     * Sets the value of the constructionQuality property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstructionQuality }
     *     
     */
    public void setConstructionQuality(ConstructionQuality value) {
        this.constructionQuality = value;
    }

    /**
     * Gets the value of the gradeFloorArea property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getGradeFloorArea() {
        return gradeFloorArea;
    }

    /**
     * Sets the value of the gradeFloorArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setGradeFloorArea(Amount value) {
        this.gradeFloorArea = value;
    }

    /**
     * Gets the value of the numberOfElevators property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfElevators() {
        return numberOfElevators;
    }

    /**
     * Sets the value of the numberOfElevators property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfElevators(String value) {
        this.numberOfElevators = value;
    }

    /**
     * Gets the value of the numberOfHabitableUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfHabitableUnits() {
        return numberOfHabitableUnits;
    }

    /**
     * Sets the value of the numberOfHabitableUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfHabitableUnits(String value) {
        this.numberOfHabitableUnits = value;
    }

    /**
     * Gets the value of the occupiedArea property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getOccupiedArea() {
        return occupiedArea;
    }

    /**
     * Sets the value of the occupiedArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setOccupiedArea(Amount value) {
        this.occupiedArea = value;
    }

    /**
     * Gets the value of the parkingLotArea property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getParkingLotArea() {
        return parkingLotArea;
    }

    /**
     * Sets the value of the parkingLotArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setParkingLotArea(Amount value) {
        this.parkingLotArea = value;
    }

    /**
     * Gets the value of the percentageOfBuildingSprinklered property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercentageOfBuildingSprinklered() {
        return percentageOfBuildingSprinklered;
    }

    /**
     * Sets the value of the percentageOfBuildingSprinklered property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercentageOfBuildingSprinklered(String value) {
        this.percentageOfBuildingSprinklered = value;
    }

    /**
     * Gets the value of the fireProtectionClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFireProtectionClass() {
        return fireProtectionClass;
    }

    /**
     * Sets the value of the fireProtectionClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFireProtectionClass(String value) {
        this.fireProtectionClass = value;
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setSize(Amount value) {
        this.size = value;
    }

    /**
     * Gets the value of the heatingType property.
     * 
     * @return
     *     possible object is
     *     {@link HeatingType }
     *     
     */
    public HeatingType getHeatingType() {
        return heatingType;
    }

    /**
     * Sets the value of the heatingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeatingType }
     *     
     */
    public void setHeatingType(HeatingType value) {
        this.heatingType = value;
    }

    /**
     * Gets the value of the plumbingMaterial property.
     * 
     * @return
     *     possible object is
     *     {@link PlumbingMaterial }
     *     
     */
    public PlumbingMaterial getPlumbingMaterial() {
        return plumbingMaterial;
    }

    /**
     * Sets the value of the plumbingMaterial property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlumbingMaterial }
     *     
     */
    public void setPlumbingMaterial(PlumbingMaterial value) {
        this.plumbingMaterial = value;
    }

    /**
     * Gets the value of the structureType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureType() {
        return structureType;
    }

    /**
     * Sets the value of the structureType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureType(String value) {
        this.structureType = value;
    }

    /**
     * Gets the value of the shelteredObjects property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shelteredObjects property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShelteredObjects().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhysicalObjectTO }
     * 
     * 
     */
    public List<PhysicalObjectTO> getShelteredObjects() {
        if (shelteredObjects == null) {
            shelteredObjects = new ArrayList<PhysicalObjectTO>();
        }
        return this.shelteredObjects;
    }

    /**
     * Gets the value of the historicHomeDistrict property.
     * This getter has been renamed from isHistoricHomeDistrict() to getHistoricHomeDistrict() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getHistoricHomeDistrict() {
        return historicHomeDistrict;
    }

    /**
     * Sets the value of the historicHomeDistrict property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHistoricHomeDistrict(Boolean value) {
        this.historicHomeDistrict = value;
    }

    /**
     * Gets the value of the occupancy property.
     * 
     * @return
     *     possible object is
     *     {@link BuildingOccupancy }
     *     
     */
    public BuildingOccupancy getOccupancy() {
        return occupancy;
    }

    /**
     * Sets the value of the occupancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BuildingOccupancy }
     *     
     */
    public void setOccupancy(BuildingOccupancy value) {
        this.occupancy = value;
    }

    /**
     * Gets the value of the isVacant property.
     * This getter has been renamed from isIsVacant() to getIsVacant() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsVacant() {
        return isVacant;
    }

    /**
     * Sets the value of the isVacant property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsVacant(Boolean value) {
        this.isVacant = value;
    }

    /**
     * Gets the value of the date property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

    /**
     * Gets the value of the hasBurglerAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Alarm }
     *     
     */
    public Alarm getHasBurglerAlarm() {
        return hasBurglerAlarm;
    }

    /**
     * Sets the value of the hasBurglerAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Alarm }
     *     
     */
    public void setHasBurglerAlarm(Alarm value) {
        this.hasBurglerAlarm = value;
    }

    /**
     * Gets the value of the hasFireAlarm property.
     * 
     * @return
     *     possible object is
     *     {@link Alarm }
     *     
     */
    public Alarm getHasFireAlarm() {
        return hasFireAlarm;
    }

    /**
     * Sets the value of the hasFireAlarm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Alarm }
     *     
     */
    public void setHasFireAlarm(Alarm value) {
        this.hasFireAlarm = value;
    }

    /**
     * Gets the value of the numberOfSmokingOccupiers property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfSmokingOccupiers() {
        return numberOfSmokingOccupiers;
    }

    /**
     * Sets the value of the numberOfSmokingOccupiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfSmokingOccupiers(BigInteger value) {
        this.numberOfSmokingOccupiers = value;
    }

    /**
     * Gets the value of the yearHeatingUpgraded property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getYearHeatingUpgraded() {
        return yearHeatingUpgraded;
    }

    /**
     * Sets the value of the yearHeatingUpgraded property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setYearHeatingUpgraded(XMLGregorianCalendar value) {
        this.yearHeatingUpgraded = value;
    }

    /**
     * Gets the value of the yearRoofUpgraded property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getYearRoofUpgraded() {
        return yearRoofUpgraded;
    }

    /**
     * Sets the value of the yearRoofUpgraded property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setYearRoofUpgraded(XMLGregorianCalendar value) {
        this.yearRoofUpgraded = value;
    }

    /**
     * Gets the value of the yearElectricUpgraded property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getYearElectricUpgraded() {
        return yearElectricUpgraded;
    }

    /**
     * Sets the value of the yearElectricUpgraded property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setYearElectricUpgraded(XMLGregorianCalendar value) {
        this.yearElectricUpgraded = value;
    }

    /**
     * Gets the value of the yearAirConditioningUpgraded property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getYearAirConditioningUpgraded() {
        return yearAirConditioningUpgraded;
    }

    /**
     * Sets the value of the yearAirConditioningUpgraded property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setYearAirConditioningUpgraded(XMLGregorianCalendar value) {
        this.yearAirConditioningUpgraded = value;
    }

    /**
     * Gets the value of the yearPlumbingUpgraded property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getYearPlumbingUpgraded() {
        return yearPlumbingUpgraded;
    }

    /**
     * Sets the value of the yearPlumbingUpgraded property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setYearPlumbingUpgraded(XMLGregorianCalendar value) {
        this.yearPlumbingUpgraded = value;
    }

    /**
     * Gets the value of the floodDefence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFloodDefence() {
        return floodDefence;
    }

    /**
     * Sets the value of the floodDefence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFloodDefence(String value) {
        this.floodDefence = value;
    }

    /**
     * Gets the value of the manufactoredObjects property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the manufactoredObjects property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getManufactoredObjects().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhysicalObjectTO }
     * 
     * 
     */
    public List<PhysicalObjectTO> getManufactoredObjects() {
        if (manufactoredObjects == null) {
            manufactoredObjects = new ArrayList<PhysicalObjectTO>();
        }
        return this.manufactoredObjects;
    }

    /**
     * Sets the value of the shelteredObjects property.
     * 
     * @param shelteredObjects
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setShelteredObjects(List<PhysicalObjectTO> shelteredObjects) {
        this.shelteredObjects = shelteredObjects;
    }

    /**
     * Sets the value of the manufactoredObjects property.
     * 
     * @param manufactoredObjects
     *     allowed object is
     *     {@link PhysicalObjectTO }
     *     
     */
    public void setManufactoredObjects(List<PhysicalObjectTO> manufactoredObjects) {
        this.manufactoredObjects = manufactoredObjects;
    }

}
