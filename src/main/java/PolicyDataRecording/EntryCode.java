
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntryCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EntryCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Agency Entered"/>
 *     &lt;enumeration value="Branch Entered"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EntryCode", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum EntryCode {

    @XmlEnumValue("Agency Entered")
    AGENCY_ENTERED("Agency Entered"),
    @XmlEnumValue("Branch Entered")
    BRANCH_ENTERED("Branch Entered");
    private final String value;

    EntryCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EntryCode fromValue(String v) {
        for (EntryCode c: EntryCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
