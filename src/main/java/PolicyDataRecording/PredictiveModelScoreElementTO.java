
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PredictiveModelScoreElement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PredictiveModelScoreElement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="modelScore" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="modelType" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="reasons" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PredictiveModelScoreReason_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="isOverModelThreshold" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="modelRank" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="proposedActionName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="applicableRanges" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}ScoreRange_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="modelTypeDescription" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PredictiveModelScoreElement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "modelScore",
    "modelType",
    "reasons",
    "isOverModelThreshold",
    "modelRank",
    "proposedActionName",
    "applicableRanges",
    "modelTypeDescription"
})
public class PredictiveModelScoreElementTO
    extends DependentObjectTO
{

    protected BigDecimal modelScore;
    protected String modelType;
    protected List<PredictiveModelScoreReasonTO> reasons;
    protected Boolean isOverModelThreshold;
    protected String modelRank;
    protected String proposedActionName;
    protected List<ScoreRangeTO> applicableRanges;
    protected String modelTypeDescription;

    /**
     * Gets the value of the modelScore property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getModelScore() {
        return modelScore;
    }

    /**
     * Sets the value of the modelScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setModelScore(BigDecimal value) {
        this.modelScore = value;
    }

    /**
     * Gets the value of the modelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelType() {
        return modelType;
    }

    /**
     * Sets the value of the modelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelType(String value) {
        this.modelType = value;
    }

    /**
     * Gets the value of the reasons property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reasons property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReasons().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PredictiveModelScoreReasonTO }
     * 
     * 
     */
    public List<PredictiveModelScoreReasonTO> getReasons() {
        if (reasons == null) {
            reasons = new ArrayList<PredictiveModelScoreReasonTO>();
        }
        return this.reasons;
    }

    /**
     * Gets the value of the isOverModelThreshold property.
     * This getter has been renamed from isIsOverModelThreshold() to getIsOverModelThreshold() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsOverModelThreshold() {
        return isOverModelThreshold;
    }

    /**
     * Sets the value of the isOverModelThreshold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOverModelThreshold(Boolean value) {
        this.isOverModelThreshold = value;
    }

    /**
     * Gets the value of the modelRank property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelRank() {
        return modelRank;
    }

    /**
     * Sets the value of the modelRank property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelRank(String value) {
        this.modelRank = value;
    }

    /**
     * Gets the value of the proposedActionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposedActionName() {
        return proposedActionName;
    }

    /**
     * Sets the value of the proposedActionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposedActionName(String value) {
        this.proposedActionName = value;
    }

    /**
     * Gets the value of the applicableRanges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableRanges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicableRanges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScoreRangeTO }
     * 
     * 
     */
    public List<ScoreRangeTO> getApplicableRanges() {
        if (applicableRanges == null) {
            applicableRanges = new ArrayList<ScoreRangeTO>();
        }
        return this.applicableRanges;
    }

    /**
     * Gets the value of the modelTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelTypeDescription() {
        return modelTypeDescription;
    }

    /**
     * Sets the value of the modelTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelTypeDescription(String value) {
        this.modelTypeDescription = value;
    }

    /**
     * Sets the value of the reasons property.
     * 
     * @param reasons
     *     allowed object is
     *     {@link PredictiveModelScoreReasonTO }
     *     
     */
    public void setReasons(List<PredictiveModelScoreReasonTO> reasons) {
        this.reasons = reasons;
    }

    /**
     * Sets the value of the applicableRanges property.
     * 
     * @param applicableRanges
     *     allowed object is
     *     {@link ScoreRangeTO }
     *     
     */
    public void setApplicableRanges(List<ScoreRangeTO> applicableRanges) {
        this.applicableRanges = applicableRanges;
    }

}
