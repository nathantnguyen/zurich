
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityRiskClass.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivityRiskClass">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Construction Activities"/>
 *     &lt;enumeration value="Pollution Clean Up Activities"/>
 *     &lt;enumeration value="Transportation Activities"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActivityRiskClass", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum ActivityRiskClass {

    @XmlEnumValue("Construction Activities")
    CONSTRUCTION_ACTIVITIES("Construction Activities"),
    @XmlEnumValue("Pollution Clean Up Activities")
    POLLUTION_CLEAN_UP_ACTIVITIES("Pollution Clean Up Activities"),
    @XmlEnumValue("Transportation Activities")
    TRANSPORTATION_ACTIVITIES("Transportation Activities");
    private final String value;

    ActivityRiskClass(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActivityRiskClass fromValue(String v) {
        for (ActivityRiskClass c: ActivityRiskClass.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
