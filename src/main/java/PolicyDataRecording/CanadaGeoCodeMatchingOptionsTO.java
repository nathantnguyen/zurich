
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CanadaGeoCodeMatchingOptions_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CanadaGeoCodeMatchingOptions_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}GeoCodeMatchingOptions_TO">
 *       &lt;sequence>
 *         &lt;element name="countyMatchRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="returnCandidates" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="multipleMatchesAccepted" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="localDeliveryUnitMatchRequired" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="maximalNumberOfCandidates" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CanadaGeoCodeMatchingOptions_TO", propOrder = {
    "countyMatchRequired",
    "returnCandidates",
    "multipleMatchesAccepted",
    "localDeliveryUnitMatchRequired",
    "maximalNumberOfCandidates"
})
public class CanadaGeoCodeMatchingOptionsTO
    extends GeoCodeMatchingOptionsTO
{

    protected Boolean countyMatchRequired;
    protected Boolean returnCandidates;
    protected Boolean multipleMatchesAccepted;
    protected Boolean localDeliveryUnitMatchRequired;
    protected BigInteger maximalNumberOfCandidates;

    /**
     * Gets the value of the countyMatchRequired property.
     * This getter has been renamed from isCountyMatchRequired() to getCountyMatchRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCountyMatchRequired() {
        return countyMatchRequired;
    }

    /**
     * Sets the value of the countyMatchRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountyMatchRequired(Boolean value) {
        this.countyMatchRequired = value;
    }

    /**
     * Gets the value of the returnCandidates property.
     * This getter has been renamed from isReturnCandidates() to getReturnCandidates() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getReturnCandidates() {
        return returnCandidates;
    }

    /**
     * Sets the value of the returnCandidates property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnCandidates(Boolean value) {
        this.returnCandidates = value;
    }

    /**
     * Gets the value of the multipleMatchesAccepted property.
     * This getter has been renamed from isMultipleMatchesAccepted() to getMultipleMatchesAccepted() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMultipleMatchesAccepted() {
        return multipleMatchesAccepted;
    }

    /**
     * Sets the value of the multipleMatchesAccepted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultipleMatchesAccepted(Boolean value) {
        this.multipleMatchesAccepted = value;
    }

    /**
     * Gets the value of the localDeliveryUnitMatchRequired property.
     * This getter has been renamed from isLocalDeliveryUnitMatchRequired() to getLocalDeliveryUnitMatchRequired() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getLocalDeliveryUnitMatchRequired() {
        return localDeliveryUnitMatchRequired;
    }

    /**
     * Sets the value of the localDeliveryUnitMatchRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLocalDeliveryUnitMatchRequired(Boolean value) {
        this.localDeliveryUnitMatchRequired = value;
    }

    /**
     * Gets the value of the maximalNumberOfCandidates property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaximalNumberOfCandidates() {
        return maximalNumberOfCandidates;
    }

    /**
     * Sets the value of the maximalNumberOfCandidates property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaximalNumberOfCandidates(BigInteger value) {
        this.maximalNumberOfCandidates = value;
    }

}
