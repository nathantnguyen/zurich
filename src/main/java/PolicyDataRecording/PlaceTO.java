
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Place_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Place_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="abbreviation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="capitalCity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Text" minOccurs="0"/>
 *         &lt;element name="surfaceArea" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="soilType" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}SoilType" minOccurs="0"/>
 *         &lt;element name="populationDensityCategory" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}PopulationDensityCategory" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="administrativeLevel" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}AdministrativeLevel" minOccurs="0"/>
 *         &lt;element name="externalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="activities" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Activity_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="placePositionings" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PlacePositioning_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="coordinates" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}GeographicCoordinates_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="placeRootType" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}PlaceType_TO" minOccurs="0"/>
 *         &lt;element name="alternateReference" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}AlternateId_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Place_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "startDate",
    "endDate",
    "abbreviation",
    "capitalCity",
    "description",
    "surfaceArea",
    "soilType",
    "populationDensityCategory",
    "name",
    "administrativeLevel",
    "externalReference",
    "activities",
    "placePositionings",
    "coordinates",
    "placeRootType",
    "alternateReference"
})
@XmlSeeAlso({
    CountryTO.class,
    GeometricShapeTO.class,
    CensusStreetSegmentTO.class
})
public class PlaceTO
    extends BusinessModelObjectTO
{

    protected XMLGregorianCalendar startDate;
    protected XMLGregorianCalendar endDate;
    protected String abbreviation;
    protected Boolean capitalCity;
    protected String description;
    protected String surfaceArea;
    protected SoilType soilType;
    protected PopulationDensityCategory populationDensityCategory;
    protected String name;
    protected AdministrativeLevel administrativeLevel;
    protected String externalReference;
    protected List<ActivityTO> activities;
    protected List<PlacePositioningTO> placePositionings;
    protected List<GeographicCoordinatesTO> coordinates;
    protected PlaceTypeTO placeRootType;
    protected List<AlternateIdTO> alternateReference;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the abbreviation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbbreviation() {
        return abbreviation;
    }

    /**
     * Sets the value of the abbreviation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbbreviation(String value) {
        this.abbreviation = value;
    }

    /**
     * Gets the value of the capitalCity property.
     * This getter has been renamed from isCapitalCity() to getCapitalCity() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCapitalCity() {
        return capitalCity;
    }

    /**
     * Sets the value of the capitalCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCapitalCity(Boolean value) {
        this.capitalCity = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the surfaceArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurfaceArea() {
        return surfaceArea;
    }

    /**
     * Sets the value of the surfaceArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurfaceArea(String value) {
        this.surfaceArea = value;
    }

    /**
     * Gets the value of the soilType property.
     * 
     * @return
     *     possible object is
     *     {@link SoilType }
     *     
     */
    public SoilType getSoilType() {
        return soilType;
    }

    /**
     * Sets the value of the soilType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoilType }
     *     
     */
    public void setSoilType(SoilType value) {
        this.soilType = value;
    }

    /**
     * Gets the value of the populationDensityCategory property.
     * 
     * @return
     *     possible object is
     *     {@link PopulationDensityCategory }
     *     
     */
    public PopulationDensityCategory getPopulationDensityCategory() {
        return populationDensityCategory;
    }

    /**
     * Sets the value of the populationDensityCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link PopulationDensityCategory }
     *     
     */
    public void setPopulationDensityCategory(PopulationDensityCategory value) {
        this.populationDensityCategory = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the administrativeLevel property.
     * 
     * @return
     *     possible object is
     *     {@link AdministrativeLevel }
     *     
     */
    public AdministrativeLevel getAdministrativeLevel() {
        return administrativeLevel;
    }

    /**
     * Sets the value of the administrativeLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdministrativeLevel }
     *     
     */
    public void setAdministrativeLevel(AdministrativeLevel value) {
        this.administrativeLevel = value;
    }

    /**
     * Gets the value of the externalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Sets the value of the externalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalReference(String value) {
        this.externalReference = value;
    }

    /**
     * Gets the value of the activities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityTO }
     * 
     * 
     */
    public List<ActivityTO> getActivities() {
        if (activities == null) {
            activities = new ArrayList<ActivityTO>();
        }
        return this.activities;
    }

    /**
     * Gets the value of the placePositionings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the placePositionings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlacePositionings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlacePositioningTO }
     * 
     * 
     */
    public List<PlacePositioningTO> getPlacePositionings() {
        if (placePositionings == null) {
            placePositionings = new ArrayList<PlacePositioningTO>();
        }
        return this.placePositionings;
    }

    /**
     * Gets the value of the coordinates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coordinates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoordinates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GeographicCoordinatesTO }
     * 
     * 
     */
    public List<GeographicCoordinatesTO> getCoordinates() {
        if (coordinates == null) {
            coordinates = new ArrayList<GeographicCoordinatesTO>();
        }
        return this.coordinates;
    }

    /**
     * Gets the value of the placeRootType property.
     * 
     * @return
     *     possible object is
     *     {@link PlaceTypeTO }
     *     
     */
    public PlaceTypeTO getPlaceRootType() {
        return placeRootType;
    }

    /**
     * Sets the value of the placeRootType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceTypeTO }
     *     
     */
    public void setPlaceRootType(PlaceTypeTO value) {
        this.placeRootType = value;
    }

    /**
     * Gets the value of the alternateReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternateReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateIdTO }
     * 
     * 
     */
    public List<AlternateIdTO> getAlternateReference() {
        if (alternateReference == null) {
            alternateReference = new ArrayList<AlternateIdTO>();
        }
        return this.alternateReference;
    }

    /**
     * Sets the value of the activities property.
     * 
     * @param activities
     *     allowed object is
     *     {@link ActivityTO }
     *     
     */
    public void setActivities(List<ActivityTO> activities) {
        this.activities = activities;
    }

    /**
     * Sets the value of the placePositionings property.
     * 
     * @param placePositionings
     *     allowed object is
     *     {@link PlacePositioningTO }
     *     
     */
    public void setPlacePositionings(List<PlacePositioningTO> placePositionings) {
        this.placePositionings = placePositionings;
    }

    /**
     * Sets the value of the coordinates property.
     * 
     * @param coordinates
     *     allowed object is
     *     {@link GeographicCoordinatesTO }
     *     
     */
    public void setCoordinates(List<GeographicCoordinatesTO> coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Sets the value of the alternateReference property.
     * 
     * @param alternateReference
     *     allowed object is
     *     {@link AlternateIdTO }
     *     
     */
    public void setAlternateReference(List<AlternateIdTO> alternateReference) {
        this.alternateReference = alternateReference;
    }

}
