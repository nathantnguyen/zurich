
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AffinityGroup_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AffinityGroup_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}Organisation_TO">
 *       &lt;sequence>
 *         &lt;element name="membershipFee" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="purpose" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}OrganisationPurpose" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AffinityGroup_TO", propOrder = {
    "membershipFee",
    "purpose"
})
public class AffinityGroupTO
    extends OrganisationTO
{

    protected String membershipFee;
    protected OrganisationPurpose purpose;

    /**
     * Gets the value of the membershipFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembershipFee() {
        return membershipFee;
    }

    /**
     * Sets the value of the membershipFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembershipFee(String value) {
        this.membershipFee = value;
    }

    /**
     * Gets the value of the purpose property.
     * 
     * @return
     *     possible object is
     *     {@link OrganisationPurpose }
     *     
     */
    public OrganisationPurpose getPurpose() {
        return purpose;
    }

    /**
     * Sets the value of the purpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganisationPurpose }
     *     
     */
    public void setPurpose(OrganisationPurpose value) {
        this.purpose = value;
    }

}
