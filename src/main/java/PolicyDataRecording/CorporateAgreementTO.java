
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CorporateAgreement_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorporateAgreement_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}Contract_TO">
 *       &lt;sequence>
 *         &lt;element name="approvalDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Date" minOccurs="0"/>
 *         &lt;element name="corporateAgreementComponents" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/}CorporateAgreement_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="subjectCategoriesInScope" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="productsInScope" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ContractAndSpecification/}FinancialServicesProduct_TO" minOccurs="0"/>
 *         &lt;element name="subjectCategories" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}Category_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporateAgreement_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/EnterpriseRisk/", propOrder = {
    "approvalDate",
    "corporateAgreementComponents",
    "subjectCategoriesInScope",
    "productsInScope",
    "subjectCategories"
})
@XmlSeeAlso({
    TaxRegulatoryAgreementTO.class,
    RiskAgreementTO.class
})
public class CorporateAgreementTO
    extends ContractTO
{

    protected XMLGregorianCalendar approvalDate;
    protected List<CorporateAgreementTO> corporateAgreementComponents;
    protected List<CategoryTO> subjectCategoriesInScope;
    protected FinancialServicesProductTO productsInScope;
    protected CategoryTO subjectCategories;

    /**
     * Gets the value of the approvalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getApprovalDate() {
        return approvalDate;
    }

    /**
     * Sets the value of the approvalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setApprovalDate(XMLGregorianCalendar value) {
        this.approvalDate = value;
    }

    /**
     * Gets the value of the corporateAgreementComponents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the corporateAgreementComponents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorporateAgreementComponents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorporateAgreementTO }
     * 
     * 
     */
    public List<CorporateAgreementTO> getCorporateAgreementComponents() {
        if (corporateAgreementComponents == null) {
            corporateAgreementComponents = new ArrayList<CorporateAgreementTO>();
        }
        return this.corporateAgreementComponents;
    }

    /**
     * Gets the value of the subjectCategoriesInScope property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subjectCategoriesInScope property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubjectCategoriesInScope().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTO }
     * 
     * 
     */
    public List<CategoryTO> getSubjectCategoriesInScope() {
        if (subjectCategoriesInScope == null) {
            subjectCategoriesInScope = new ArrayList<CategoryTO>();
        }
        return this.subjectCategoriesInScope;
    }

    /**
     * Gets the value of the productsInScope property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialServicesProductTO }
     *     
     */
    public FinancialServicesProductTO getProductsInScope() {
        return productsInScope;
    }

    /**
     * Sets the value of the productsInScope property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialServicesProductTO }
     *     
     */
    public void setProductsInScope(FinancialServicesProductTO value) {
        this.productsInScope = value;
    }

    /**
     * Gets the value of the subjectCategories property.
     * 
     * @return
     *     possible object is
     *     {@link CategoryTO }
     *     
     */
    public CategoryTO getSubjectCategories() {
        return subjectCategories;
    }

    /**
     * Sets the value of the subjectCategories property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setSubjectCategories(CategoryTO value) {
        this.subjectCategories = value;
    }

    /**
     * Sets the value of the corporateAgreementComponents property.
     * 
     * @param corporateAgreementComponents
     *     allowed object is
     *     {@link CorporateAgreementTO }
     *     
     */
    public void setCorporateAgreementComponents(List<CorporateAgreementTO> corporateAgreementComponents) {
        this.corporateAgreementComponents = corporateAgreementComponents;
    }

    /**
     * Sets the value of the subjectCategoriesInScope property.
     * 
     * @param subjectCategoriesInScope
     *     allowed object is
     *     {@link CategoryTO }
     *     
     */
    public void setSubjectCategoriesInScope(List<CategoryTO> subjectCategoriesInScope) {
        this.subjectCategoriesInScope = subjectCategoriesInScope;
    }

}
