
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Annuity_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Annuity_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/}LifeAndHealthPolicy_TO">
 *       &lt;sequence>
 *         &lt;element name="initialDepositSource" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="initialDeposit" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialTransaction/}Payment_TO" minOccurs="0"/>
 *         &lt;element name="surrenderChargeSinceLastAnniversary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="cumulativeWithdrawalsLastPolicyYear" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="cumulativeWithdrawalsSinceLastAnniversary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}BaseCurrencyAmount" minOccurs="0"/>
 *         &lt;element name="numberOfWithdrawalsSinceLastAnniversary" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Annuity_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/FinancialServicesAgreement/", propOrder = {
    "initialDepositSource",
    "initialDeposit",
    "surrenderChargeSinceLastAnniversary",
    "cumulativeWithdrawalsLastPolicyYear",
    "cumulativeWithdrawalsSinceLastAnniversary",
    "numberOfWithdrawalsSinceLastAnniversary"
})
public class AnnuityTO
    extends LifeAndHealthPolicyTO
{

    protected String initialDepositSource;
    protected PaymentTO initialDeposit;
    protected BaseCurrencyAmount surrenderChargeSinceLastAnniversary;
    protected BaseCurrencyAmount cumulativeWithdrawalsLastPolicyYear;
    protected BaseCurrencyAmount cumulativeWithdrawalsSinceLastAnniversary;
    protected BigInteger numberOfWithdrawalsSinceLastAnniversary;

    /**
     * Gets the value of the initialDepositSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitialDepositSource() {
        return initialDepositSource;
    }

    /**
     * Sets the value of the initialDepositSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitialDepositSource(String value) {
        this.initialDepositSource = value;
    }

    /**
     * Gets the value of the initialDeposit property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTO }
     *     
     */
    public PaymentTO getInitialDeposit() {
        return initialDeposit;
    }

    /**
     * Sets the value of the initialDeposit property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTO }
     *     
     */
    public void setInitialDeposit(PaymentTO value) {
        this.initialDeposit = value;
    }

    /**
     * Gets the value of the surrenderChargeSinceLastAnniversary property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getSurrenderChargeSinceLastAnniversary() {
        return surrenderChargeSinceLastAnniversary;
    }

    /**
     * Sets the value of the surrenderChargeSinceLastAnniversary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setSurrenderChargeSinceLastAnniversary(BaseCurrencyAmount value) {
        this.surrenderChargeSinceLastAnniversary = value;
    }

    /**
     * Gets the value of the cumulativeWithdrawalsLastPolicyYear property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCumulativeWithdrawalsLastPolicyYear() {
        return cumulativeWithdrawalsLastPolicyYear;
    }

    /**
     * Sets the value of the cumulativeWithdrawalsLastPolicyYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCumulativeWithdrawalsLastPolicyYear(BaseCurrencyAmount value) {
        this.cumulativeWithdrawalsLastPolicyYear = value;
    }

    /**
     * Gets the value of the cumulativeWithdrawalsSinceLastAnniversary property.
     * 
     * @return
     *     possible object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public BaseCurrencyAmount getCumulativeWithdrawalsSinceLastAnniversary() {
        return cumulativeWithdrawalsSinceLastAnniversary;
    }

    /**
     * Sets the value of the cumulativeWithdrawalsSinceLastAnniversary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseCurrencyAmount }
     *     
     */
    public void setCumulativeWithdrawalsSinceLastAnniversary(BaseCurrencyAmount value) {
        this.cumulativeWithdrawalsSinceLastAnniversary = value;
    }

    /**
     * Gets the value of the numberOfWithdrawalsSinceLastAnniversary property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfWithdrawalsSinceLastAnniversary() {
        return numberOfWithdrawalsSinceLastAnniversary;
    }

    /**
     * Sets the value of the numberOfWithdrawalsSinceLastAnniversary property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfWithdrawalsSinceLastAnniversary(BigInteger value) {
        this.numberOfWithdrawalsSinceLastAnniversary = value;
    }

}
