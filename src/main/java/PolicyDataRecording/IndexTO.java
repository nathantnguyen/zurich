
package PolicyDataRecording;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Index_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Index_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/}Statistics_TO">
 *       &lt;sequence>
 *         &lt;element name="publicationFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="baseCurrency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="indexValueAtDate" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="indexValues" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}IndexValue_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Index_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Rating/", propOrder = {
    "publicationFrequency",
    "baseCurrency",
    "indexValueAtDate",
    "indexValues"
})
@XmlSeeAlso({
    ExchangeRateIndexTO.class,
    InflationIndexTO.class
})
public class IndexTO
    extends StatisticsTO
{

    protected Frequency publicationFrequency;
    protected CurrencyCode baseCurrency;
    protected BigDecimal indexValueAtDate;
    protected List<IndexValueTO> indexValues;

    /**
     * Gets the value of the publicationFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getPublicationFrequency() {
        return publicationFrequency;
    }

    /**
     * Sets the value of the publicationFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setPublicationFrequency(Frequency value) {
        this.publicationFrequency = value;
    }

    /**
     * Gets the value of the baseCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCode }
     *     
     */
    public CurrencyCode getBaseCurrency() {
        return baseCurrency;
    }

    /**
     * Sets the value of the baseCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCode }
     *     
     */
    public void setBaseCurrency(CurrencyCode value) {
        this.baseCurrency = value;
    }

    /**
     * Gets the value of the indexValueAtDate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIndexValueAtDate() {
        return indexValueAtDate;
    }

    /**
     * Sets the value of the indexValueAtDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIndexValueAtDate(BigDecimal value) {
        this.indexValueAtDate = value;
    }

    /**
     * Gets the value of the indexValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the indexValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIndexValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IndexValueTO }
     * 
     * 
     */
    public List<IndexValueTO> getIndexValues() {
        if (indexValues == null) {
            indexValues = new ArrayList<IndexValueTO>();
        }
        return this.indexValues;
    }

    /**
     * Sets the value of the indexValues property.
     * 
     * @param indexValues
     *     allowed object is
     *     {@link IndexValueTO }
     *     
     */
    public void setIndexValues(List<IndexValueTO> indexValues) {
        this.indexValues = indexValues;
    }

}
