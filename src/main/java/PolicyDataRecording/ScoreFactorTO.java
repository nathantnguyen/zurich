
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScoreFactor_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScoreFactor_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="identifier" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="factor" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="isUsedIn" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Score_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScoreFactor_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "identifier",
    "factor",
    "isUsedIn"
})
public class ScoreFactorTO
    extends DependentObjectTO
{

    protected String identifier;
    protected BigDecimal factor;
    protected ScoreTO isUsedIn;

    /**
     * Gets the value of the identifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets the value of the identifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentifier(String value) {
        this.identifier = value;
    }

    /**
     * Gets the value of the factor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFactor() {
        return factor;
    }

    /**
     * Sets the value of the factor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFactor(BigDecimal value) {
        this.factor = value;
    }

    /**
     * Gets the value of the isUsedIn property.
     * 
     * @return
     *     possible object is
     *     {@link ScoreTO }
     *     
     */
    public ScoreTO getIsUsedIn() {
        return isUsedIn;
    }

    /**
     * Sets the value of the isUsedIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScoreTO }
     *     
     */
    public void setIsUsedIn(ScoreTO value) {
        this.isUsedIn = value;
    }

}
