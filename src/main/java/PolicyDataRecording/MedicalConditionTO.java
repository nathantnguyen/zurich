
package PolicyDataRecording;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MedicalCondition_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MedicalCondition_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}PhysicalCondition_TO">
 *       &lt;sequence>
 *         &lt;element name="severity" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Severity" minOccurs="0"/>
 *         &lt;element name="frequency" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}Frequency" minOccurs="0"/>
 *         &lt;element name="height" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="weight" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Amount" minOccurs="0"/>
 *         &lt;element name="bloodPressureSystolic" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="bloodPressureDiastolic" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="oxygenSaturationQuantity" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Decimal" minOccurs="0"/>
 *         &lt;element name="affectedBodyElement" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/}BodyElement_TO" minOccurs="0"/>
 *         &lt;element name="curability" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/PhysicalObject/PhysicalObjectEnumerationsAndStates/}Curability" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedicalCondition_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/PhysicalObject/", propOrder = {
    "severity",
    "frequency",
    "height",
    "weight",
    "bloodPressureSystolic",
    "bloodPressureDiastolic",
    "oxygenSaturationQuantity",
    "affectedBodyElement",
    "curability"
})
public class MedicalConditionTO
    extends PhysicalConditionTO
{

    protected Severity severity;
    protected Frequency frequency;
    protected BigDecimal height;
    protected Amount weight;
    protected BigDecimal bloodPressureSystolic;
    protected BigDecimal bloodPressureDiastolic;
    protected BigDecimal oxygenSaturationQuantity;
    protected BodyElementTO affectedBodyElement;
    protected Curability curability;

    /**
     * Gets the value of the severity property.
     * 
     * @return
     *     possible object is
     *     {@link Severity }
     *     
     */
    public Severity getSeverity() {
        return severity;
    }

    /**
     * Sets the value of the severity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Severity }
     *     
     */
    public void setSeverity(Severity value) {
        this.severity = value;
    }

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link Frequency }
     *     
     */
    public Frequency getFrequency() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link Frequency }
     *     
     */
    public void setFrequency(Frequency value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHeight(BigDecimal value) {
        this.height = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setWeight(Amount value) {
        this.weight = value;
    }

    /**
     * Gets the value of the bloodPressureSystolic property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBloodPressureSystolic() {
        return bloodPressureSystolic;
    }

    /**
     * Sets the value of the bloodPressureSystolic property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBloodPressureSystolic(BigDecimal value) {
        this.bloodPressureSystolic = value;
    }

    /**
     * Gets the value of the bloodPressureDiastolic property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBloodPressureDiastolic() {
        return bloodPressureDiastolic;
    }

    /**
     * Sets the value of the bloodPressureDiastolic property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBloodPressureDiastolic(BigDecimal value) {
        this.bloodPressureDiastolic = value;
    }

    /**
     * Gets the value of the oxygenSaturationQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOxygenSaturationQuantity() {
        return oxygenSaturationQuantity;
    }

    /**
     * Sets the value of the oxygenSaturationQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOxygenSaturationQuantity(BigDecimal value) {
        this.oxygenSaturationQuantity = value;
    }

    /**
     * Gets the value of the affectedBodyElement property.
     * 
     * @return
     *     possible object is
     *     {@link BodyElementTO }
     *     
     */
    public BodyElementTO getAffectedBodyElement() {
        return affectedBodyElement;
    }

    /**
     * Sets the value of the affectedBodyElement property.
     * 
     * @param value
     *     allowed object is
     *     {@link BodyElementTO }
     *     
     */
    public void setAffectedBodyElement(BodyElementTO value) {
        this.affectedBodyElement = value;
    }

    /**
     * Gets the value of the curability property.
     * 
     * @return
     *     possible object is
     *     {@link Curability }
     *     
     */
    public Curability getCurability() {
        return curability;
    }

    /**
     * Sets the value of the curability property.
     * 
     * @param value
     *     allowed object is
     *     {@link Curability }
     *     
     */
    public void setCurability(Curability value) {
        this.curability = value;
    }

}
