
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationProfile_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommunicationProfile_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}DependentObject_TO">
 *       &lt;sequence>
 *         &lt;element name="language" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/}Language" minOccurs="0"/>
 *         &lt;element name="medium" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/}CommunicationMedium" minOccurs="0"/>
 *         &lt;element name="priority" type="{http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Common/CommonEnumerationsAndStates/}PriorityLevel" minOccurs="0"/>
 *         &lt;element name="personsAssigned" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contactPreferencesAssigned" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}ObjectReference_TO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationProfile_TO", propOrder = {
    "language",
    "medium",
    "priority",
    "personsAssigned",
    "contactPreferencesAssigned"
})
@XmlSeeAlso({
    VisualProfileTO.class,
    AudioProfileTO.class,
    MobilityProfileTO.class,
    CognitiveProfileTO.class
})
public class CommunicationProfileTO
    extends DependentObjectTO
{

    protected Language language;
    protected CommunicationMedium medium;
    protected PriorityLevel priority;
    protected List<ObjectReferenceTO> personsAssigned;
    protected List<ObjectReferenceTO> contactPreferencesAssigned;

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link Language }
     *     
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link Language }
     *     
     */
    public void setLanguage(Language value) {
        this.language = value;
    }

    /**
     * Gets the value of the medium property.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationMedium }
     *     
     */
    public CommunicationMedium getMedium() {
        return medium;
    }

    /**
     * Sets the value of the medium property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationMedium }
     *     
     */
    public void setMedium(CommunicationMedium value) {
        this.medium = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link PriorityLevel }
     *     
     */
    public PriorityLevel getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriorityLevel }
     *     
     */
    public void setPriority(PriorityLevel value) {
        this.priority = value;
    }

    /**
     * Gets the value of the personsAssigned property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the personsAssigned property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonsAssigned().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getPersonsAssigned() {
        if (personsAssigned == null) {
            personsAssigned = new ArrayList<ObjectReferenceTO>();
        }
        return this.personsAssigned;
    }

    /**
     * Gets the value of the contactPreferencesAssigned property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactPreferencesAssigned property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactPreferencesAssigned().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ObjectReferenceTO }
     * 
     * 
     */
    public List<ObjectReferenceTO> getContactPreferencesAssigned() {
        if (contactPreferencesAssigned == null) {
            contactPreferencesAssigned = new ArrayList<ObjectReferenceTO>();
        }
        return this.contactPreferencesAssigned;
    }

    /**
     * Sets the value of the personsAssigned property.
     * 
     * @param personsAssigned
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setPersonsAssigned(List<ObjectReferenceTO> personsAssigned) {
        this.personsAssigned = personsAssigned;
    }

    /**
     * Sets the value of the contactPreferencesAssigned property.
     * 
     * @param contactPreferencesAssigned
     *     allowed object is
     *     {@link ObjectReferenceTO }
     *     
     */
    public void setContactPreferencesAssigned(List<ObjectReferenceTO> contactPreferencesAssigned) {
        this.contactPreferencesAssigned = contactPreferencesAssigned;
    }

}
