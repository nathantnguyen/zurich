
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxRegistrationTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TaxRegistrationTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Vat Registration"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TaxRegistrationTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Tax/TaxEnumerationsAndStates/")
@XmlEnum
public enum TaxRegistrationTypeName {

    @XmlEnumValue("Vat Registration")
    VAT_REGISTRATION("Vat Registration");
    private final String value;

    TaxRegistrationTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TaxRegistrationTypeName fromValue(String v) {
        for (TaxRegistrationTypeName c: TaxRegistrationTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
