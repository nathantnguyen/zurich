
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DerivativeContractTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DerivativeContractTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Futures Contract"/>
 *     &lt;enumeration value="Option Contract"/>
 *     &lt;enumeration value="Catastrophe Equity Put"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DerivativeContractTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/FinancialServicesAgreement/FinancialServicesAgreementEnumerationsAndStates/")
@XmlEnum
public enum DerivativeContractTypeName {

    @XmlEnumValue("Futures Contract")
    FUTURES_CONTRACT("Futures Contract"),
    @XmlEnumValue("Option Contract")
    OPTION_CONTRACT("Option Contract"),
    @XmlEnumValue("Catastrophe Equity Put")
    CATASTROPHE_EQUITY_PUT("Catastrophe Equity Put");
    private final String value;

    DerivativeContractTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DerivativeContractTypeName fromValue(String v) {
        for (DerivativeContractTypeName c: DerivativeContractTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
