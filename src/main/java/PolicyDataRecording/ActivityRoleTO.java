
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityRole_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityRole_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}RolePlayerClassRole_TO">
 *       &lt;sequence>
 *         &lt;element name="contextExternalReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0"/>
 *         &lt;element name="rolePlayerActivity" type="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/}Activity_TO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityRole_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/ActivityConditionPlace/", propOrder = {
    "contextExternalReference",
    "rolePlayerActivity"
})
public class ActivityRoleTO
    extends RolePlayerClassRoleTO
{

    protected String contextExternalReference;
    protected ActivityTO rolePlayerActivity;

    /**
     * Gets the value of the contextExternalReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContextExternalReference() {
        return contextExternalReference;
    }

    /**
     * Sets the value of the contextExternalReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContextExternalReference(String value) {
        this.contextExternalReference = value;
    }

    /**
     * Gets the value of the rolePlayerActivity property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityTO }
     *     
     */
    public ActivityTO getRolePlayerActivity() {
        return rolePlayerActivity;
    }

    /**
     * Sets the value of the rolePlayerActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityTO }
     *     
     */
    public void setRolePlayerActivity(ActivityTO value) {
        this.rolePlayerActivity = value;
    }

}
