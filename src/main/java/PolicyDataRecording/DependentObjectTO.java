
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DependentObject_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DependentObject_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/}BusinessModelObject_TO">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DependentObject_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Common/")
@XmlSeeAlso({
    DocumentSearchIndexTO.class,
    CommentTO.class,
    ScoreRangeTO.class,
    PredictiveModelScoreElementTO.class,
    ScoreFactorTO.class,
    PredictiveModelPartTO.class,
    ScoreReasonTO.class,
    IndexValueTO.class,
    StandardCreditLevelParameterTO.class,
    CoverageComponentDetailTO.class,
    ReservationStrategyTO.class,
    PaymentMethodTO.class,
    RemittanceTO.class,
    ClaimCostCodeTO.class,
    MoneyProvisionElementTO.class,
    StatementElementTO.class,
    MoneyProvisionElementPartTO.class,
    AccountEntryTO.class,
    AssetPriceTO.class,
    AssetFeeTO.class,
    ContactPreferenceTO.class,
    ContactLimitationTO.class,
    ScenarioTO.class,
    PostalAddressRangeTO.class,
    TimingPreferenceTO.class,
    PartyNameTO.class,
    CompanyDetailTO.class,
    GeoCodeMatchingOptionsTO.class,
    ParsedPostalAddressTO.class,
    GeoCodeProcessingOptionsTO.class,
    SuspectDuplicateAugmentationTO.class,
    GoalAndNeedTO.class,
    CommunicationProfileTO.class,
    RegistrationTO.class,
    GeoCodingOptionsTO.class,
    RoleTO.class
})
public class DependentObjectTO
    extends BusinessModelObjectTO
{


}
