
package PolicyDataRecording;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Swap_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Swap_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/}Derivative_TO">
 *       &lt;sequence>
 *         &lt;element name="interestPaymentFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *         &lt;element name="interestResetFrequency" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Number" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Swap_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/AccountAndFund/", propOrder = {
    "interestPaymentFrequency",
    "interestResetFrequency"
})
public class SwapTO
    extends DerivativeTO
{

    protected BigInteger interestPaymentFrequency;
    protected BigInteger interestResetFrequency;

    /**
     * Gets the value of the interestPaymentFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInterestPaymentFrequency() {
        return interestPaymentFrequency;
    }

    /**
     * Sets the value of the interestPaymentFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInterestPaymentFrequency(BigInteger value) {
        this.interestPaymentFrequency = value;
    }

    /**
     * Gets the value of the interestResetFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInterestResetFrequency() {
        return interestResetFrequency;
    }

    /**
     * Sets the value of the interestResetFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInterestResetFrequency(BigInteger value) {
        this.interestResetFrequency = value;
    }

}
