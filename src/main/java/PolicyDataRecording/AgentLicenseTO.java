
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgentLicense_TO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgentLicense_TO">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Party/}PartyRegistration_TO">
 *       &lt;sequence>
 *         &lt;element name="isResidentOfState" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}Boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgentLicense_TO", namespace = "http://www.zurich.com/zsoa/nac/schemas/TransferObjects/Intermediary/", propOrder = {
    "isResidentOfState"
})
public class AgentLicenseTO
    extends PartyRegistrationTO
{

    protected Boolean isResidentOfState;

    /**
     * Gets the value of the isResidentOfState property.
     * This getter has been renamed from isIsResidentOfState() to getIsResidentOfState() by cxf-xjc-boolean plugin.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getIsResidentOfState() {
        return isResidentOfState;
    }

    /**
     * Sets the value of the isResidentOfState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsResidentOfState(Boolean value) {
        this.isResidentOfState = value;
    }

}
