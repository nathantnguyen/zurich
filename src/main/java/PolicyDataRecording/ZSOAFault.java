
package PolicyDataRecording;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * <p>Java class for ZSOAFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ZSOAFault">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="message" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0" form="qualified"/>
 *         &lt;element name="serviceName" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}QName" minOccurs="0" form="qualified"/>
 *         &lt;element name="operation" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}NCName" minOccurs="0" form="qualified"/>
 *         &lt;element name="timeStamp" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}DateTime" minOccurs="0" form="qualified"/>
 *         &lt;element name="exceptions" type="{http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/}BaseException" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *         &lt;element name="errorCode" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0" form="qualified"/>
 *         &lt;element name="messageReference" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" minOccurs="0" form="qualified"/>
 *         &lt;choice>
 *           &lt;element name="serviceURI" type="{http://www.w3.org/2001/XMLSchema}anyURI" form="qualified"/>
 *           &lt;element name="serviceEPR" type="{http://www.zurich.com/zsoa/nac/schemas/PrimitiveDatatypes/}String" form="qualified"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZSOAFault", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/", propOrder = {
    "message",
    "serviceName",
    "operation",
    "timeStamp",
    "exceptions",
    "errorCode",
    "messageReference",
    "serviceURI",
    "serviceEPR"
})
public class ZSOAFault {

    protected String message;
    protected QName serviceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String operation;
    @XmlElementRef(name = "timeStamp", namespace = "http://www.zurich.com/zsoa/nac/webservices/Services/Definitions/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> timeStamp;
    protected List<BaseException> exceptions;
    protected String errorCode;
    protected String messageReference;
    @XmlSchemaType(name = "anyURI")
    protected String serviceURI;
    protected String serviceEPR;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the serviceName property.
     * 
     * @return
     *     possible object is
     *     {@link QName }
     *     
     */
    public QName getServiceName() {
        return serviceName;
    }

    /**
     * Sets the value of the serviceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link QName }
     *     
     */
    public void setServiceName(QName value) {
        this.serviceName = value;
    }

    /**
     * Gets the value of the operation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Sets the value of the operation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperation(String value) {
        this.operation = value;
    }

    /**
     * Gets the value of the timeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the value of the timeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTimeStamp(JAXBElement<XMLGregorianCalendar> value) {
        this.timeStamp = value;
    }

    /**
     * Gets the value of the exceptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exceptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExceptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BaseException }
     * 
     * 
     */
    public List<BaseException> getExceptions() {
        if (exceptions == null) {
            exceptions = new ArrayList<BaseException>();
        }
        return this.exceptions;
    }

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the messageReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageReference() {
        return messageReference;
    }

    /**
     * Sets the value of the messageReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageReference(String value) {
        this.messageReference = value;
    }

    /**
     * Gets the value of the serviceURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceURI() {
        return serviceURI;
    }

    /**
     * Sets the value of the serviceURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceURI(String value) {
        this.serviceURI = value;
    }

    /**
     * Gets the value of the serviceEPR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceEPR() {
        return serviceEPR;
    }

    /**
     * Sets the value of the serviceEPR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceEPR(String value) {
        this.serviceEPR = value;
    }

    /**
     * Sets the value of the exceptions property.
     * 
     * @param exceptions
     *     allowed object is
     *     {@link BaseException }
     *     
     */
    public void setExceptions(List<BaseException> exceptions) {
        this.exceptions = exceptions;
    }

}
