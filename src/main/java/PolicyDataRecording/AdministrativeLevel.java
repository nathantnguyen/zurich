
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdministrativeLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AdministrativeLevel">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="City"/>
 *     &lt;enumeration value="Country"/>
 *     &lt;enumeration value="Region"/>
 *     &lt;enumeration value="Subregion"/>
 *     &lt;enumeration value="Supranational"/>
 *     &lt;enumeration value="Parcel"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AdministrativeLevel", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/ActivityConditionPlace/ActivityConditionPlaceEnumerationsAndStates/")
@XmlEnum
public enum AdministrativeLevel {

    @XmlEnumValue("City")
    CITY("City"),
    @XmlEnumValue("Country")
    COUNTRY("Country"),
    @XmlEnumValue("Region")
    REGION("Region"),
    @XmlEnumValue("Subregion")
    SUBREGION("Subregion"),
    @XmlEnumValue("Supranational")
    SUPRANATIONAL("Supranational"),
    @XmlEnumValue("Parcel")
    PARCEL("Parcel");
    private final String value;

    AdministrativeLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdministrativeLevel fromValue(String v) {
        for (AdministrativeLevel c: AdministrativeLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
