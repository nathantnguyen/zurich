
package PolicyDataRecording;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LifeEventTypeName.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LifeEventTypeName">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Personal Event"/>
 *     &lt;enumeration value="Organisation Event"/>
 *     &lt;enumeration value="Social Media Event"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LifeEventTypeName", namespace = "http://www.zurich.com/zsoa/nac/schemas/StructuralComponents/Party/PartyEnumerationsAndStates/")
@XmlEnum
public enum LifeEventTypeName {

    @XmlEnumValue("Personal Event")
    PERSONAL_EVENT("Personal Event"),
    @XmlEnumValue("Organisation Event")
    ORGANISATION_EVENT("Organisation Event"),
    @XmlEnumValue("Social Media Event")
    SOCIAL_MEDIA_EVENT("Social Media Event");
    private final String value;

    LifeEventTypeName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LifeEventTypeName fromValue(String v) {
        for (LifeEventTypeName c: LifeEventTypeName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new java.lang.IllegalArgumentException(v);
    }

}
