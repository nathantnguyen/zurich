
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PISNC_RQST_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse", propOrder = {
    "pisncrqstid"
})
public class GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2
    extends ResponseBase
{

    @XmlElement(name = "PISNC_RQST_ID")
    protected String pisncrqstid;

    /**
     * Gets the value of the pisncrqstid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPISNCRQSTID() {
        return pisncrqstid;
    }

    /**
     * Sets the value of the pisncrqstid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPISNCRQSTID(String value) {
        this.pisncrqstid = value;
    }

}
