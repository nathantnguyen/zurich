
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QuickSearchResult" type="{http://workstation.znawebservices.zurichna.com}QuickSearchResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "quickSearchResult"
})
@XmlRootElement(name = "QuickSearchResponse")
public class QuickSearchResponse {

    @XmlElement(name = "QuickSearchResult")
    protected QuickSearchResponse2 quickSearchResult;

    /**
     * Gets the value of the quickSearchResult property.
     * 
     * @return
     *     possible object is
     *     {@link QuickSearchResponse2 }
     *     
     */
    public QuickSearchResponse2 getQuickSearchResult() {
        return quickSearchResult;
    }

    /**
     * Sets the value of the quickSearchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuickSearchResponse2 }
     *     
     */
    public void setQuickSearchResult(QuickSearchResponse2 value) {
        this.quickSearchResult = value;
    }

}
