
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactComment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactComment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PTY_ROL_CMNT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ROL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CMNT_TTL_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CMNT_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactComment", propOrder = {
    "ptyrolcmntid",
    "ptyid",
    "rolid",
    "cmntttltxt",
    "cmntdesc",
    "effdt",
    "expidt",
    "enttistmp",
    "changeState"
})
public class ContactComment {

    @XmlElement(name = "PTY_ROL_CMNT_ID")
    protected int ptyrolcmntid;
    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "ROL_ID")
    protected int rolid;
    @XmlElement(name = "CMNT_TTL_TXT")
    protected String cmntttltxt;
    @XmlElement(name = "CMNT_DESC")
    protected String cmntdesc;
    @XmlElement(name = "EFF_DT")
    protected String effdt;
    @XmlElement(name = "EXPI_DT")
    protected String expidt;
    @XmlElement(name = "ENT_TISTMP")
    protected String enttistmp;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the ptyrolcmntid property.
     * 
     */
    public int getPTYROLCMNTID() {
        return ptyrolcmntid;
    }

    /**
     * Sets the value of the ptyrolcmntid property.
     * 
     */
    public void setPTYROLCMNTID(int value) {
        this.ptyrolcmntid = value;
    }

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the rolid property.
     * 
     */
    public int getROLID() {
        return rolid;
    }

    /**
     * Sets the value of the rolid property.
     * 
     */
    public void setROLID(int value) {
        this.rolid = value;
    }

    /**
     * Gets the value of the cmntttltxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMNTTTLTXT() {
        return cmntttltxt;
    }

    /**
     * Sets the value of the cmntttltxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMNTTTLTXT(String value) {
        this.cmntttltxt = value;
    }

    /**
     * Gets the value of the cmntdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMNTDESC() {
        return cmntdesc;
    }

    /**
     * Sets the value of the cmntdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMNTDESC(String value) {
        this.cmntdesc = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEFFDT(String value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPIDT(String value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENTTISTMP(String value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
