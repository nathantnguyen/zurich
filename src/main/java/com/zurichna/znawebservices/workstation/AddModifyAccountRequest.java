
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddModifyAccountRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddModifyAccountRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AddCustomer" type="{http://workstation.znawebservices.zurichna.com}AddModifyAccount" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddModifyAccountRequest", propOrder = {
    "addCustomer"
})
public class AddModifyAccountRequest
    extends RequestBase
{

    @XmlElement(name = "AddCustomer")
    protected AddModifyAccount addCustomer;

    /**
     * Gets the value of the addCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link AddModifyAccount }
     *     
     */
    public AddModifyAccount getAddCustomer() {
        return addCustomer;
    }

    /**
     * Sets the value of the addCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddModifyAccount }
     *     
     */
    public void setAddCustomer(AddModifyAccount value) {
        this.addCustomer = value;
    }

}
