
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuickSerachCustomer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuickSerachCustomer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUST_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TRD_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEP_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuickSerachCustomer", propOrder = {
    "custptyid",
    "nm",
    "trdnm",
    "dunsnbr",
    "addr",
    "depaddr",
    "ctynm",
    "stabbr",
    "zipcd",
    "ctrycd",
    "ctrynm",
    "siccd",
    "sicdesc"
})
public class QuickSerachCustomer {

    @XmlElement(name = "CUST_PTY_ID")
    protected int custptyid;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "TRD_NM")
    protected String trdnm;
    @XmlElement(name = "DUNS_NBR")
    protected String dunsnbr;
    @XmlElement(name = "ADDR")
    protected String addr;
    @XmlElement(name = "DEP_ADDR")
    protected String depaddr;
    @XmlElement(name = "CTY_NM")
    protected String ctynm;
    @XmlElement(name = "ST_ABBR")
    protected String stabbr;
    @XmlElement(name = "ZIP_CD")
    protected String zipcd;
    @XmlElement(name = "CTRY_CD")
    protected String ctrycd;
    @XmlElement(name = "CTRY_NM")
    protected String ctrynm;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "SIC_DESC")
    protected String sicdesc;

    /**
     * Gets the value of the custptyid property.
     * 
     */
    public int getCUSTPTYID() {
        return custptyid;
    }

    /**
     * Sets the value of the custptyid property.
     * 
     */
    public void setCUSTPTYID(int value) {
        this.custptyid = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the trdnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRDNM() {
        return trdnm;
    }

    /**
     * Sets the value of the trdnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRDNM(String value) {
        this.trdnm = value;
    }

    /**
     * Gets the value of the dunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNSNBR() {
        return dunsnbr;
    }

    /**
     * Sets the value of the dunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNSNBR(String value) {
        this.dunsnbr = value;
    }

    /**
     * Gets the value of the addr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDR() {
        return addr;
    }

    /**
     * Sets the value of the addr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDR(String value) {
        this.addr = value;
    }

    /**
     * Gets the value of the depaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEPADDR() {
        return depaddr;
    }

    /**
     * Sets the value of the depaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEPADDR(String value) {
        this.depaddr = value;
    }

    /**
     * Gets the value of the ctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTYNM() {
        return ctynm;
    }

    /**
     * Sets the value of the ctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTYNM(String value) {
        this.ctynm = value;
    }

    /**
     * Gets the value of the stabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTABBR() {
        return stabbr;
    }

    /**
     * Sets the value of the stabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTABBR(String value) {
        this.stabbr = value;
    }

    /**
     * Gets the value of the zipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIPCD() {
        return zipcd;
    }

    /**
     * Sets the value of the zipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIPCD(String value) {
        this.zipcd = value;
    }

    /**
     * Gets the value of the ctrycd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYCD() {
        return ctrycd;
    }

    /**
     * Sets the value of the ctrycd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYCD(String value) {
        this.ctrycd = value;
    }

    /**
     * Gets the value of the ctrynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYNM() {
        return ctrynm;
    }

    /**
     * Sets the value of the ctrynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYNM(String value) {
        this.ctrynm = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the sicdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICDESC() {
        return sicdesc;
    }

    /**
     * Sets the value of the sicdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICDESC(String value) {
        this.sicdesc = value;
    }

}
