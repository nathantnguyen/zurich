
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PolicyProduct complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyProduct"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDR_PTY_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_PTY_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_PTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_STRC_NM_COMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_STRC_ABBR_COMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BU_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDT_CAT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDT_CAT_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BU_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CISPEC_AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SCISPEC_STS_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="SCISPEC_STS_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UNDR_EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASST_UNDR_EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyProduct", propOrder = {
    "custminsspecid",
    "agmtid",
    "undrptyid",
    "undrptylstnm",
    "undrptyfstnm",
    "prdrptyid",
    "prdrptynm",
    "prdrnbr",
    "orgptyid",
    "orgptynm",
    "orgptystrcnmcomb",
    "orgptystrcabbrcomb",
    "buabbr",
    "prdtcatnm",
    "prdtcatabbr",
    "bunm",
    "cispecagmtid",
    "enttistmp",
    "scispecstsid",
    "changeState",
    "scispecstsenttistmp",
    "undremailaddr",
    "asstundremailaddr"
})
public class PolicyProduct {

    @XmlElement(name = "CUSTM_INS_SPEC_ID")
    protected int custminsspecid;
    @XmlElement(name = "AGMT_ID")
    protected int agmtid;
    @XmlElement(name = "UNDR_PTY_ID")
    protected int undrptyid;
    @XmlElement(name = "UNDR_PTY_LST_NM")
    protected String undrptylstnm;
    @XmlElement(name = "UNDR_PTY_FST_NM")
    protected String undrptyfstnm;
    @XmlElement(name = "PRDR_PTY_ID")
    protected int prdrptyid;
    @XmlElement(name = "PRDR_PTY_NM")
    protected String prdrptynm;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_PTY_NM")
    protected String orgptynm;
    @XmlElement(name = "ORG_PTY_STRC_NM_COMB")
    protected String orgptystrcnmcomb;
    @XmlElement(name = "ORG_PTY_STRC_ABBR_COMB")
    protected String orgptystrcabbrcomb;
    @XmlElement(name = "BU_ABBR")
    protected String buabbr;
    @XmlElement(name = "PRDT_CAT_NM")
    protected String prdtcatnm;
    @XmlElement(name = "PRDT_CAT_ABBR")
    protected String prdtcatabbr;
    @XmlElement(name = "BU_NM")
    protected String bunm;
    @XmlElement(name = "CISPEC_AGMT_ID")
    protected int cispecagmtid;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "SCISPEC_STS_ID")
    protected int scispecstsid;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    @XmlElement(name = "SCISPEC_STS_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar scispecstsenttistmp;
    @XmlElement(name = "UNDR_EMAIL_ADDR")
    protected String undremailaddr;
    @XmlElement(name = "ASST_UNDR_EMAIL_ADDR")
    protected String asstundremailaddr;

    /**
     * Gets the value of the custminsspecid property.
     * 
     */
    public int getCUSTMINSSPECID() {
        return custminsspecid;
    }

    /**
     * Sets the value of the custminsspecid property.
     * 
     */
    public void setCUSTMINSSPECID(int value) {
        this.custminsspecid = value;
    }

    /**
     * Gets the value of the agmtid property.
     * 
     */
    public int getAGMTID() {
        return agmtid;
    }

    /**
     * Sets the value of the agmtid property.
     * 
     */
    public void setAGMTID(int value) {
        this.agmtid = value;
    }

    /**
     * Gets the value of the undrptyid property.
     * 
     */
    public int getUNDRPTYID() {
        return undrptyid;
    }

    /**
     * Sets the value of the undrptyid property.
     * 
     */
    public void setUNDRPTYID(int value) {
        this.undrptyid = value;
    }

    /**
     * Gets the value of the undrptylstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRPTYLSTNM() {
        return undrptylstnm;
    }

    /**
     * Sets the value of the undrptylstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRPTYLSTNM(String value) {
        this.undrptylstnm = value;
    }

    /**
     * Gets the value of the undrptyfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRPTYFSTNM() {
        return undrptyfstnm;
    }

    /**
     * Sets the value of the undrptyfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRPTYFSTNM(String value) {
        this.undrptyfstnm = value;
    }

    /**
     * Gets the value of the prdrptyid property.
     * 
     */
    public int getPRDRPTYID() {
        return prdrptyid;
    }

    /**
     * Sets the value of the prdrptyid property.
     * 
     */
    public void setPRDRPTYID(int value) {
        this.prdrptyid = value;
    }

    /**
     * Gets the value of the prdrptynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRPTYNM() {
        return prdrptynm;
    }

    /**
     * Sets the value of the prdrptynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRPTYNM(String value) {
        this.prdrptynm = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgptynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGPTYNM() {
        return orgptynm;
    }

    /**
     * Sets the value of the orgptynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGPTYNM(String value) {
        this.orgptynm = value;
    }

    /**
     * Gets the value of the orgptystrcnmcomb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGPTYSTRCNMCOMB() {
        return orgptystrcnmcomb;
    }

    /**
     * Sets the value of the orgptystrcnmcomb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGPTYSTRCNMCOMB(String value) {
        this.orgptystrcnmcomb = value;
    }

    /**
     * Gets the value of the orgptystrcabbrcomb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGPTYSTRCABBRCOMB() {
        return orgptystrcabbrcomb;
    }

    /**
     * Sets the value of the orgptystrcabbrcomb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGPTYSTRCABBRCOMB(String value) {
        this.orgptystrcabbrcomb = value;
    }

    /**
     * Gets the value of the buabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUABBR() {
        return buabbr;
    }

    /**
     * Sets the value of the buabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUABBR(String value) {
        this.buabbr = value;
    }

    /**
     * Gets the value of the prdtcatnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTCATNM() {
        return prdtcatnm;
    }

    /**
     * Sets the value of the prdtcatnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTCATNM(String value) {
        this.prdtcatnm = value;
    }

    /**
     * Gets the value of the prdtcatabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTCATABBR() {
        return prdtcatabbr;
    }

    /**
     * Sets the value of the prdtcatabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTCATABBR(String value) {
        this.prdtcatabbr = value;
    }

    /**
     * Gets the value of the bunm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUNM() {
        return bunm;
    }

    /**
     * Sets the value of the bunm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUNM(String value) {
        this.bunm = value;
    }

    /**
     * Gets the value of the cispecagmtid property.
     * 
     */
    public int getCISPECAGMTID() {
        return cispecagmtid;
    }

    /**
     * Sets the value of the cispecagmtid property.
     * 
     */
    public void setCISPECAGMTID(int value) {
        this.cispecagmtid = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the scispecstsid property.
     * 
     */
    public int getSCISPECSTSID() {
        return scispecstsid;
    }

    /**
     * Sets the value of the scispecstsid property.
     * 
     */
    public void setSCISPECSTSID(int value) {
        this.scispecstsid = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the scispecstsenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSCISPECSTSENTTISTMP() {
        return scispecstsenttistmp;
    }

    /**
     * Sets the value of the scispecstsenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSCISPECSTSENTTISTMP(XMLGregorianCalendar value) {
        this.scispecstsenttistmp = value;
    }

    /**
     * Gets the value of the undremailaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDREMAILADDR() {
        return undremailaddr;
    }

    /**
     * Sets the value of the undremailaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDREMAILADDR(String value) {
        this.undremailaddr = value;
    }

    /**
     * Gets the value of the asstundremailaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSTUNDREMAILADDR() {
        return asstundremailaddr;
    }

    /**
     * Sets the value of the asstundremailaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSTUNDREMAILADDR(String value) {
        this.asstundremailaddr = value;
    }

}
