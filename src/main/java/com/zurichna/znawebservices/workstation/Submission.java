
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Submission complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Submission"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRIO_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BRCHR_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LOS_HIST_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ANNL_RPT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RECD_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CMNT_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="QTE_DUE_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CAPTV_RQST_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SPL_AGMT_RQST_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OT_AGMT_RQST_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QTE_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="BKD_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="BOUND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SYS_SRC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUST_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_ROL_SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_ROL_SMSN_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CUST_PTY_ROL_SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUST_PTY_ROL_SMSN_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SYS_SRC_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_TIED_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="submissionProduct" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionProduct" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Submission", propOrder = {
    "smsnid",
    "priocd",
    "brchrind",
    "loshistind",
    "annlrptind",
    "recddt",
    "cmnttxt",
    "effdt",
    "expidt",
    "enttistmp",
    "qteduedt",
    "nm",
    "captvrqstind",
    "splagmtrqstind",
    "otagmtrqstind",
    "qtepremamt",
    "bkdpremamt",
    "boundpremamt",
    "newrenlcd",
    "syssrccd",
    "custptyid",
    "orgptyid",
    "orgptyrolsmsnid",
    "orgptyrolsmsnenttistmp",
    "custptyrolsmsnid",
    "custptyrolsmsnenttistmp",
    "syssrctxt",
    "poltiedind",
    "changeState",
    "submissionProduct"
})
public class Submission {

    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "PRIO_CD")
    protected String priocd;
    @XmlElement(name = "BRCHR_IND")
    protected String brchrind;
    @XmlElement(name = "LOS_HIST_IND")
    protected String loshistind;
    @XmlElement(name = "ANNL_RPT_IND")
    protected String annlrptind;
    @XmlElement(name = "RECD_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recddt;
    @XmlElement(name = "CMNT_TXT")
    protected String cmnttxt;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "QTE_DUE_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar qteduedt;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "CAPTV_RQST_IND")
    protected String captvrqstind;
    @XmlElement(name = "SPL_AGMT_RQST_IND")
    protected String splagmtrqstind;
    @XmlElement(name = "OT_AGMT_RQST_IND")
    protected String otagmtrqstind;
    @XmlElement(name = "QTE_PREM_AMT", required = true)
    protected BigDecimal qtepremamt;
    @XmlElement(name = "BKD_PREM_AMT", required = true)
    protected BigDecimal bkdpremamt;
    @XmlElement(name = "BOUND_PREM_AMT", required = true)
    protected BigDecimal boundpremamt;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "SYS_SRC_CD")
    protected String syssrccd;
    @XmlElement(name = "CUST_PTY_ID")
    protected int custptyid;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_PTY_ROL_SMSN_ID")
    protected int orgptyrolsmsnid;
    @XmlElement(name = "ORG_PTY_ROL_SMSN_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar orgptyrolsmsnenttistmp;
    @XmlElement(name = "CUST_PTY_ROL_SMSN_ID")
    protected int custptyrolsmsnid;
    @XmlElement(name = "CUST_PTY_ROL_SMSN_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar custptyrolsmsnenttistmp;
    @XmlElement(name = "SYS_SRC_TXT")
    protected String syssrctxt;
    @XmlElement(name = "POL_TIED_IND")
    protected boolean poltiedind;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfSubmissionProduct submissionProduct;

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the priocd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRIOCD() {
        return priocd;
    }

    /**
     * Sets the value of the priocd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRIOCD(String value) {
        this.priocd = value;
    }

    /**
     * Gets the value of the brchrind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRCHRIND() {
        return brchrind;
    }

    /**
     * Sets the value of the brchrind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRCHRIND(String value) {
        this.brchrind = value;
    }

    /**
     * Gets the value of the loshistind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOSHISTIND() {
        return loshistind;
    }

    /**
     * Sets the value of the loshistind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOSHISTIND(String value) {
        this.loshistind = value;
    }

    /**
     * Gets the value of the annlrptind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getANNLRPTIND() {
        return annlrptind;
    }

    /**
     * Sets the value of the annlrptind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setANNLRPTIND(String value) {
        this.annlrptind = value;
    }

    /**
     * Gets the value of the recddt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRECDDT() {
        return recddt;
    }

    /**
     * Sets the value of the recddt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRECDDT(XMLGregorianCalendar value) {
        this.recddt = value;
    }

    /**
     * Gets the value of the cmnttxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMNTTXT() {
        return cmnttxt;
    }

    /**
     * Sets the value of the cmnttxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMNTTXT(String value) {
        this.cmnttxt = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the qteduedt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQTEDUEDT() {
        return qteduedt;
    }

    /**
     * Sets the value of the qteduedt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQTEDUEDT(XMLGregorianCalendar value) {
        this.qteduedt = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the captvrqstind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPTVRQSTIND() {
        return captvrqstind;
    }

    /**
     * Sets the value of the captvrqstind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPTVRQSTIND(String value) {
        this.captvrqstind = value;
    }

    /**
     * Gets the value of the splagmtrqstind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPLAGMTRQSTIND() {
        return splagmtrqstind;
    }

    /**
     * Sets the value of the splagmtrqstind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPLAGMTRQSTIND(String value) {
        this.splagmtrqstind = value;
    }

    /**
     * Gets the value of the otagmtrqstind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOTAGMTRQSTIND() {
        return otagmtrqstind;
    }

    /**
     * Sets the value of the otagmtrqstind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOTAGMTRQSTIND(String value) {
        this.otagmtrqstind = value;
    }

    /**
     * Gets the value of the qtepremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTEPREMAMT() {
        return qtepremamt;
    }

    /**
     * Sets the value of the qtepremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTEPREMAMT(BigDecimal value) {
        this.qtepremamt = value;
    }

    /**
     * Gets the value of the bkdpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBKDPREMAMT() {
        return bkdpremamt;
    }

    /**
     * Sets the value of the bkdpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBKDPREMAMT(BigDecimal value) {
        this.bkdpremamt = value;
    }

    /**
     * Gets the value of the boundpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBOUNDPREMAMT() {
        return boundpremamt;
    }

    /**
     * Sets the value of the boundpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBOUNDPREMAMT(BigDecimal value) {
        this.boundpremamt = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the syssrccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSYSSRCCD() {
        return syssrccd;
    }

    /**
     * Sets the value of the syssrccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSYSSRCCD(String value) {
        this.syssrccd = value;
    }

    /**
     * Gets the value of the custptyid property.
     * 
     */
    public int getCUSTPTYID() {
        return custptyid;
    }

    /**
     * Sets the value of the custptyid property.
     * 
     */
    public void setCUSTPTYID(int value) {
        this.custptyid = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgptyrolsmsnid property.
     * 
     */
    public int getORGPTYROLSMSNID() {
        return orgptyrolsmsnid;
    }

    /**
     * Sets the value of the orgptyrolsmsnid property.
     * 
     */
    public void setORGPTYROLSMSNID(int value) {
        this.orgptyrolsmsnid = value;
    }

    /**
     * Gets the value of the orgptyrolsmsnenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getORGPTYROLSMSNENTTISTMP() {
        return orgptyrolsmsnenttistmp;
    }

    /**
     * Sets the value of the orgptyrolsmsnenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setORGPTYROLSMSNENTTISTMP(XMLGregorianCalendar value) {
        this.orgptyrolsmsnenttistmp = value;
    }

    /**
     * Gets the value of the custptyrolsmsnid property.
     * 
     */
    public int getCUSTPTYROLSMSNID() {
        return custptyrolsmsnid;
    }

    /**
     * Sets the value of the custptyrolsmsnid property.
     * 
     */
    public void setCUSTPTYROLSMSNID(int value) {
        this.custptyrolsmsnid = value;
    }

    /**
     * Gets the value of the custptyrolsmsnenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCUSTPTYROLSMSNENTTISTMP() {
        return custptyrolsmsnenttistmp;
    }

    /**
     * Sets the value of the custptyrolsmsnenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCUSTPTYROLSMSNENTTISTMP(XMLGregorianCalendar value) {
        this.custptyrolsmsnenttistmp = value;
    }

    /**
     * Gets the value of the syssrctxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSYSSRCTXT() {
        return syssrctxt;
    }

    /**
     * Sets the value of the syssrctxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSYSSRCTXT(String value) {
        this.syssrctxt = value;
    }

    /**
     * Gets the value of the poltiedind property.
     * 
     */
    public boolean isPOLTIEDIND() {
        return poltiedind;
    }

    /**
     * Sets the value of the poltiedind property.
     * 
     */
    public void setPOLTIEDIND(boolean value) {
        this.poltiedind = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the submissionProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionProduct }
     *     
     */
    public ArrayOfSubmissionProduct getSubmissionProduct() {
        return submissionProduct;
    }

    /**
     * Sets the value of the submissionProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionProduct }
     *     
     */
    public void setSubmissionProduct(ArrayOfSubmissionProduct value) {
        this.submissionProduct = value;
    }

}
