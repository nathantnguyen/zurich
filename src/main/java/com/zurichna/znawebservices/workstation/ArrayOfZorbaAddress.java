
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfZorbaAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfZorbaAddress"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZorbaAddress" type="{http://workstation.znawebservices.zurichna.com}ZorbaAddress" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfZorbaAddress", propOrder = {
    "zorbaAddress"
})
public class ArrayOfZorbaAddress {

    @XmlElement(name = "ZorbaAddress", nillable = true)
    protected List<ZorbaAddress> zorbaAddress;

    /**
     * Gets the value of the zorbaAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zorbaAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZorbaAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ZorbaAddress }
     * 
     * 
     */
    public List<ZorbaAddress> getZorbaAddress() {
        if (zorbaAddress == null) {
            zorbaAddress = new ArrayList<ZorbaAddress>();
        }
        return this.zorbaAddress;
    }

}
