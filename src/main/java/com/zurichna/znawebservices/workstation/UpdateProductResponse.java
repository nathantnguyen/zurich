
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateProductResult" type="{http://workstation.znawebservices.zurichna.com}UpdateProductResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateProductResult"
})
@XmlRootElement(name = "UpdateProductResponse")
public class UpdateProductResponse {

    @XmlElement(name = "UpdateProductResult")
    protected UpdateProductResponse2 updateProductResult;

    /**
     * Gets the value of the updateProductResult property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateProductResponse2 }
     *     
     */
    public UpdateProductResponse2 getUpdateProductResult() {
        return updateProductResult;
    }

    /**
     * Sets the value of the updateProductResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateProductResponse2 }
     *     
     */
    public void setUpdateProductResult(UpdateProductResponse2 value) {
        this.updateProductResult = value;
    }

}
