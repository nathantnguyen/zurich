
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Underwriters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Underwriters"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UNDR_NOTES_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Underwriters", propOrder = {
    "undrnotesid",
    "undrfstnm",
    "undrlstnm",
    "undremailaddr"
})
public class Underwriters {

    @XmlElement(name = "UNDR_NOTES_ID")
    protected String undrnotesid;
    @XmlElement(name = "UNDR_FST_NM")
    protected String undrfstnm;
    @XmlElement(name = "UNDR_LST_NM")
    protected String undrlstnm;
    @XmlElement(name = "UNDR_EMAIL_ADDR")
    protected String undremailaddr;

    /**
     * Gets the value of the undrnotesid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRNOTESID() {
        return undrnotesid;
    }

    /**
     * Sets the value of the undrnotesid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRNOTESID(String value) {
        this.undrnotesid = value;
    }

    /**
     * Gets the value of the undrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRFSTNM() {
        return undrfstnm;
    }

    /**
     * Sets the value of the undrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRFSTNM(String value) {
        this.undrfstnm = value;
    }

    /**
     * Gets the value of the undrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRLSTNM() {
        return undrlstnm;
    }

    /**
     * Sets the value of the undrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRLSTNM(String value) {
        this.undrlstnm = value;
    }

    /**
     * Gets the value of the undremailaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDREMAILADDR() {
        return undremailaddr;
    }

    /**
     * Sets the value of the undremailaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDREMAILADDR(String value) {
        this.undremailaddr = value;
    }

}
