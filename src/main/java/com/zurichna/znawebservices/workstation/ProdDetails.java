
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProdDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProdDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TMPL_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PROD_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PROD_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SUB_PROD_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SUB_PROD_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProdDetails", propOrder = {
    "tmplinsspecid",
    "prodabbr",
    "proddesc",
    "subprodabbr",
    "subproddesc"
})
public class ProdDetails {

    @XmlElement(name = "TMPL_INS_SPEC_ID")
    protected int tmplinsspecid;
    @XmlElement(name = "PROD_ABBR")
    protected String prodabbr;
    @XmlElement(name = "PROD_DESC")
    protected String proddesc;
    @XmlElement(name = "SUB_PROD_ABBR")
    protected String subprodabbr;
    @XmlElement(name = "SUB_PROD_DESC")
    protected String subproddesc;

    /**
     * Gets the value of the tmplinsspecid property.
     * 
     */
    public int getTMPLINSSPECID() {
        return tmplinsspecid;
    }

    /**
     * Sets the value of the tmplinsspecid property.
     * 
     */
    public void setTMPLINSSPECID(int value) {
        this.tmplinsspecid = value;
    }

    /**
     * Gets the value of the prodabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRODABBR() {
        return prodabbr;
    }

    /**
     * Sets the value of the prodabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRODABBR(String value) {
        this.prodabbr = value;
    }

    /**
     * Gets the value of the proddesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRODDESC() {
        return proddesc;
    }

    /**
     * Sets the value of the proddesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRODDESC(String value) {
        this.proddesc = value;
    }

    /**
     * Gets the value of the subprodabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBPRODABBR() {
        return subprodabbr;
    }

    /**
     * Sets the value of the subprodabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBPRODABBR(String value) {
        this.subprodabbr = value;
    }

    /**
     * Gets the value of the subproddesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBPRODDESC() {
        return subproddesc;
    }

    /**
     * Sets the value of the subproddesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBPRODDESC(String value) {
        this.subproddesc = value;
    }

}
