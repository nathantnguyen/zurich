
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetWCCATModellingResultsResult" type="{http://workstation.znawebservices.zurichna.com}GetWCCATModellingResultsResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getWCCATModellingResultsResult"
})
@XmlRootElement(name = "GetWCCATModellingResultsResponse")
public class GetWCCATModellingResultsResponse {

    @XmlElement(name = "GetWCCATModellingResultsResult")
    protected GetWCCATModellingResultsResponse2 getWCCATModellingResultsResult;

    /**
     * Gets the value of the getWCCATModellingResultsResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetWCCATModellingResultsResponse2 }
     *     
     */
    public GetWCCATModellingResultsResponse2 getGetWCCATModellingResultsResult() {
        return getWCCATModellingResultsResult;
    }

    /**
     * Sets the value of the getWCCATModellingResultsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetWCCATModellingResultsResponse2 }
     *     
     */
    public void setGetWCCATModellingResultsResult(GetWCCATModellingResultsResponse2 value) {
        this.getWCCATModellingResultsResult = value;
    }

}
