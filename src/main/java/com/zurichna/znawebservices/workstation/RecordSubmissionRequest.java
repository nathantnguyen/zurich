
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RecordSubmissionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecordSubmissionRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="submissionName" type="{http://workstation.znawebservices.zurichna.com}SubmissionName" minOccurs="0"/&gt;
 *         &lt;element name="submission" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionV2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecordSubmissionRequest", propOrder = {
    "submissionName",
    "submission"
})
public class RecordSubmissionRequest
    extends RequestBase
{

    protected SubmissionName submissionName;
    protected ArrayOfSubmissionV2 submission;

    /**
     * Gets the value of the submissionName property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionName }
     *     
     */
    public SubmissionName getSubmissionName() {
        return submissionName;
    }

    /**
     * Sets the value of the submissionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionName }
     *     
     */
    public void setSubmissionName(SubmissionName value) {
        this.submissionName = value;
    }

    /**
     * Gets the value of the submission property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionV2 }
     *     
     */
    public ArrayOfSubmissionV2 getSubmission() {
        return submission;
    }

    /**
     * Sets the value of the submission property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionV2 }
     *     
     */
    public void setSubmission(ArrayOfSubmissionV2 value) {
        this.submission = value;
    }

}
