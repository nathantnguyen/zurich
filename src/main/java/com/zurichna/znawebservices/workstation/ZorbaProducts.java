
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ZorbaProducts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ZorbaProducts"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDT_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RQST_COVG_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RQST_COVG_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ORG_BU_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="submissionProductStatus" type="{http://workstation.znawebservices.zurichna.com}ArrayOfZorbaProductStatus" minOccurs="0"/&gt;
 *         &lt;element name="productpolicy" type="{http://workstation.znawebservices.zurichna.com}ArrayOfProductPolicy" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZorbaProducts", propOrder = {
    "custminsspecid",
    "prdtnm",
    "prdtabbr",
    "smsnid",
    "rqstcovgeffdt",
    "rqstcovgexpidt",
    "orgbuabbr",
    "prdrnm",
    "undrfstnm",
    "undrlstnm",
    "changeState",
    "submissionProductStatus",
    "productpolicy"
})
public class ZorbaProducts {

    @XmlElement(name = "CUSTM_INS_SPEC_ID")
    protected int custminsspecid;
    @XmlElement(name = "PRDT_NM")
    protected String prdtnm;
    @XmlElement(name = "PRDT_ABBR")
    protected String prdtabbr;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "RQST_COVG_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgeffdt;
    @XmlElement(name = "RQST_COVG_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgexpidt;
    @XmlElement(name = "ORG_BU_ABBR")
    protected String orgbuabbr;
    @XmlElement(name = "PRDR_NM")
    protected String prdrnm;
    @XmlElement(name = "UNDR_FST_NM")
    protected String undrfstnm;
    @XmlElement(name = "UNDR_LST_NM")
    protected String undrlstnm;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfZorbaProductStatus submissionProductStatus;
    protected ArrayOfProductPolicy productpolicy;

    /**
     * Gets the value of the custminsspecid property.
     * 
     */
    public int getCUSTMINSSPECID() {
        return custminsspecid;
    }

    /**
     * Sets the value of the custminsspecid property.
     * 
     */
    public void setCUSTMINSSPECID(int value) {
        this.custminsspecid = value;
    }

    /**
     * Gets the value of the prdtnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTNM() {
        return prdtnm;
    }

    /**
     * Sets the value of the prdtnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTNM(String value) {
        this.prdtnm = value;
    }

    /**
     * Gets the value of the prdtabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTABBR() {
        return prdtabbr;
    }

    /**
     * Sets the value of the prdtabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTABBR(String value) {
        this.prdtabbr = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the rqstcovgeffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEFFDT() {
        return rqstcovgeffdt;
    }

    /**
     * Sets the value of the rqstcovgeffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEFFDT(XMLGregorianCalendar value) {
        this.rqstcovgeffdt = value;
    }

    /**
     * Gets the value of the rqstcovgexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEXPIDT() {
        return rqstcovgexpidt;
    }

    /**
     * Sets the value of the rqstcovgexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEXPIDT(XMLGregorianCalendar value) {
        this.rqstcovgexpidt = value;
    }

    /**
     * Gets the value of the orgbuabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGBUABBR() {
        return orgbuabbr;
    }

    /**
     * Sets the value of the orgbuabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGBUABBR(String value) {
        this.orgbuabbr = value;
    }

    /**
     * Gets the value of the prdrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNM() {
        return prdrnm;
    }

    /**
     * Sets the value of the prdrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNM(String value) {
        this.prdrnm = value;
    }

    /**
     * Gets the value of the undrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRFSTNM() {
        return undrfstnm;
    }

    /**
     * Sets the value of the undrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRFSTNM(String value) {
        this.undrfstnm = value;
    }

    /**
     * Gets the value of the undrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRLSTNM() {
        return undrlstnm;
    }

    /**
     * Sets the value of the undrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRLSTNM(String value) {
        this.undrlstnm = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the submissionProductStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfZorbaProductStatus }
     *     
     */
    public ArrayOfZorbaProductStatus getSubmissionProductStatus() {
        return submissionProductStatus;
    }

    /**
     * Sets the value of the submissionProductStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfZorbaProductStatus }
     *     
     */
    public void setSubmissionProductStatus(ArrayOfZorbaProductStatus value) {
        this.submissionProductStatus = value;
    }

    /**
     * Gets the value of the productpolicy property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProductPolicy }
     *     
     */
    public ArrayOfProductPolicy getProductpolicy() {
        return productpolicy;
    }

    /**
     * Sets the value of the productpolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProductPolicy }
     *     
     */
    public void setProductpolicy(ArrayOfProductPolicy value) {
        this.productpolicy = value;
    }

}
