
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetSubmissionResult" type="{http://workstation.znawebservices.zurichna.com}SubmissionResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSubmissionResult"
})
@XmlRootElement(name = "GetSubmissionResponse")
public class GetSubmissionResponse {

    @XmlElement(name = "GetSubmissionResult")
    protected SubmissionResponse getSubmissionResult;

    /**
     * Gets the value of the getSubmissionResult property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionResponse }
     *     
     */
    public SubmissionResponse getGetSubmissionResult() {
        return getSubmissionResult;
    }

    /**
     * Sets the value of the getSubmissionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionResponse }
     *     
     */
    public void setGetSubmissionResult(SubmissionResponse value) {
        this.getSubmissionResult = value;
    }

}
