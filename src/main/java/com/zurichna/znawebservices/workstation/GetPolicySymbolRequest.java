
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPolicySymbolRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPolicySymbolRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PRDT_ID" type="{http://workstation.znawebservices.zurichna.com}ArrayOfInt" minOccurs="0"/&gt;
 *         &lt;element name="INS_GRP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPolicySymbolRequest", propOrder = {
    "prdtid",
    "insgrpid"
})
public class GetPolicySymbolRequest
    extends RequestBase
{

    @XmlElement(name = "PRDT_ID")
    protected ArrayOfInt prdtid;
    @XmlElement(name = "INS_GRP_ID")
    protected int insgrpid;

    /**
     * Gets the value of the prdtid property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInt }
     *     
     */
    public ArrayOfInt getPRDTID() {
        return prdtid;
    }

    /**
     * Sets the value of the prdtid property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInt }
     *     
     */
    public void setPRDTID(ArrayOfInt value) {
        this.prdtid = value;
    }

    /**
     * Gets the value of the insgrpid property.
     * 
     */
    public int getINSGRPID() {
        return insgrpid;
    }

    /**
     * Sets the value of the insgrpid property.
     * 
     */
    public void setINSGRPID(int value) {
        this.insgrpid = value;
    }

}
