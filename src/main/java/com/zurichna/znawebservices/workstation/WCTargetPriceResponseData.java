
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WCTargetPriceResponseData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WCTargetPriceResponseData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="wcTargetPrice" type="{http://workstation.znawebservices.zurichna.com}WCTargetPrice" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WCTargetPriceResponseData", propOrder = {
    "wcTargetPrice"
})
public class WCTargetPriceResponseData
    extends ResponseBase
{

    protected WCTargetPrice2 wcTargetPrice;

    /**
     * Gets the value of the wcTargetPrice property.
     * 
     * @return
     *     possible object is
     *     {@link WCTargetPrice2 }
     *     
     */
    public WCTargetPrice2 getWcTargetPrice() {
        return wcTargetPrice;
    }

    /**
     * Sets the value of the wcTargetPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link WCTargetPrice2 }
     *     
     */
    public void setWcTargetPrice(WCTargetPrice2 value) {
        this.wcTargetPrice = value;
    }

}
