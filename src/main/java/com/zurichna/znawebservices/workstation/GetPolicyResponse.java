
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetPolicyResult" type="{http://workstation.znawebservices.zurichna.com}PolicyResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPolicyResult"
})
@XmlRootElement(name = "GetPolicyResponse")
public class GetPolicyResponse {

    @XmlElement(name = "GetPolicyResult")
    protected PolicyResponse getPolicyResult;

    /**
     * Gets the value of the getPolicyResult property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyResponse }
     *     
     */
    public PolicyResponse getGetPolicyResult() {
        return getPolicyResult;
    }

    /**
     * Sets the value of the getPolicyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyResponse }
     *     
     */
    public void setGetPolicyResult(PolicyResponse value) {
        this.getPolicyResult = value;
    }

}
