
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DelSMSNProductResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DelSMSNProductResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SCISPEC_STS_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CMNT_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RSN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STS_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DelSMSNProductResponse", propOrder = {
    "custminsspecid",
    "stscd",
    "effdt",
    "expidt",
    "scispecstsenttistmp",
    "cmnttxt",
    "rsncd",
    "ststistmp",
    "changeState"
})
public class DelSMSNProductResponse
    extends ResponseBase
{

    @XmlElement(name = "CUSTM_INS_SPEC_ID")
    protected int custminsspecid;
    @XmlElement(name = "STS_CD")
    protected String stscd;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "SCISPEC_STS_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar scispecstsenttistmp;
    @XmlElement(name = "CMNT_TXT")
    protected String cmnttxt;
    @XmlElement(name = "RSN_CD")
    protected String rsncd;
    @XmlElement(name = "STS_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ststistmp;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the custminsspecid property.
     * 
     */
    public int getCUSTMINSSPECID() {
        return custminsspecid;
    }

    /**
     * Sets the value of the custminsspecid property.
     * 
     */
    public void setCUSTMINSSPECID(int value) {
        this.custminsspecid = value;
    }

    /**
     * Gets the value of the stscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTSCD() {
        return stscd;
    }

    /**
     * Sets the value of the stscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTSCD(String value) {
        this.stscd = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the scispecstsenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSCISPECSTSENTTISTMP() {
        return scispecstsenttistmp;
    }

    /**
     * Sets the value of the scispecstsenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSCISPECSTSENTTISTMP(XMLGregorianCalendar value) {
        this.scispecstsenttistmp = value;
    }

    /**
     * Gets the value of the cmnttxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMNTTXT() {
        return cmnttxt;
    }

    /**
     * Sets the value of the cmnttxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMNTTXT(String value) {
        this.cmnttxt = value;
    }

    /**
     * Gets the value of the rsncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRSNCD() {
        return rsncd;
    }

    /**
     * Sets the value of the rsncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRSNCD(String value) {
        this.rsncd = value;
    }

    /**
     * Gets the value of the ststistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTSTISTMP() {
        return ststistmp;
    }

    /**
     * Sets the value of the ststistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTSTISTMP(XMLGregorianCalendar value) {
        this.ststistmp = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
