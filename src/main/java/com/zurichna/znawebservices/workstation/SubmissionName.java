
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmissionName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionName"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PHTC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PTY_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SMSN_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SITE_DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ENPRS_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TRD_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FED_TAX_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEC_SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEC_SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ADDR_CHG_FLG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NAME_CHG_FLG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INTERNET_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUST_REVNW" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LOC_WITH_TIV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UMB_OCCUR_LMT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PACE_INELGB_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="PACE_INELGB_RSN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="address" type="{http://workstation.znawebservices.zurichna.com}ArrayOfAddress" minOccurs="0"/&gt;
 *         &lt;element name="telephone" type="{http://workstation.znawebservices.zurichna.com}ArrayOfTelephone" minOccurs="0"/&gt;
 *         &lt;element name="submissionNameAdditionalInformation" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionNameAdditionalInformation" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionName", propOrder = {
    "ptyid",
    "effdt",
    "expidt",
    "enttistmp",
    "phtcnm",
    "ptytypid",
    "smsnnm",
    "dunsnbr",
    "sitedunsnbr",
    "enprsid",
    "trdnm",
    "fedtaxid",
    "abbr",
    "siccd",
    "sicnm",
    "secsiccd",
    "secsicnm",
    "addrchgflg",
    "namechgflg",
    "internetaddr",
    "custrevnw",
    "locwithtiv",
    "umboccurlmt",
    "paceinelgbind",
    "paceinelgbrsn",
    "changeState",
    "address",
    "telephone",
    "submissionNameAdditionalInformation"
})
public class SubmissionName {

    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "PHTC_NM")
    protected String phtcnm;
    @XmlElement(name = "PTY_TYP_ID")
    protected int ptytypid;
    @XmlElement(name = "SMSN_NM")
    protected String smsnnm;
    @XmlElement(name = "DUNS_NBR")
    protected String dunsnbr;
    @XmlElement(name = "SITE_DUNS_NBR")
    protected String sitedunsnbr;
    @XmlElement(name = "ENPRS_ID")
    protected String enprsid;
    @XmlElement(name = "TRD_NM")
    protected String trdnm;
    @XmlElement(name = "FED_TAX_ID")
    protected String fedtaxid;
    @XmlElement(name = "ABBR")
    protected String abbr;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "SIC_NM")
    protected String sicnm;
    @XmlElement(name = "SEC_SIC_CD")
    protected String secsiccd;
    @XmlElement(name = "SEC_SIC_NM")
    protected String secsicnm;
    @XmlElement(name = "ADDR_CHG_FLG")
    protected String addrchgflg;
    @XmlElement(name = "NAME_CHG_FLG")
    protected String namechgflg;
    @XmlElement(name = "INTERNET_ADDR")
    protected String internetaddr;
    @XmlElement(name = "CUST_REVNW")
    protected String custrevnw;
    @XmlElement(name = "LOC_WITH_TIV")
    protected String locwithtiv;
    @XmlElement(name = "UMB_OCCUR_LMT")
    protected String umboccurlmt;
    @XmlElement(name = "PACE_INELGB_IND")
    protected boolean paceinelgbind;
    @XmlElement(name = "PACE_INELGB_RSN")
    protected String paceinelgbrsn;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfAddress address;
    protected ArrayOfTelephone telephone;
    protected ArrayOfSubmissionNameAdditionalInformation submissionNameAdditionalInformation;

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the phtcnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPHTCNM() {
        return phtcnm;
    }

    /**
     * Sets the value of the phtcnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPHTCNM(String value) {
        this.phtcnm = value;
    }

    /**
     * Gets the value of the ptytypid property.
     * 
     */
    public int getPTYTYPID() {
        return ptytypid;
    }

    /**
     * Sets the value of the ptytypid property.
     * 
     */
    public void setPTYTYPID(int value) {
        this.ptytypid = value;
    }

    /**
     * Gets the value of the smsnnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSMSNNM() {
        return smsnnm;
    }

    /**
     * Sets the value of the smsnnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSMSNNM(String value) {
        this.smsnnm = value;
    }

    /**
     * Gets the value of the dunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNSNBR() {
        return dunsnbr;
    }

    /**
     * Sets the value of the dunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNSNBR(String value) {
        this.dunsnbr = value;
    }

    /**
     * Gets the value of the sitedunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSITEDUNSNBR() {
        return sitedunsnbr;
    }

    /**
     * Sets the value of the sitedunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSITEDUNSNBR(String value) {
        this.sitedunsnbr = value;
    }

    /**
     * Gets the value of the enprsid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENPRSID() {
        return enprsid;
    }

    /**
     * Sets the value of the enprsid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENPRSID(String value) {
        this.enprsid = value;
    }

    /**
     * Gets the value of the trdnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRDNM() {
        return trdnm;
    }

    /**
     * Sets the value of the trdnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRDNM(String value) {
        this.trdnm = value;
    }

    /**
     * Gets the value of the fedtaxid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFEDTAXID() {
        return fedtaxid;
    }

    /**
     * Sets the value of the fedtaxid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFEDTAXID(String value) {
        this.fedtaxid = value;
    }

    /**
     * Gets the value of the abbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getABBR() {
        return abbr;
    }

    /**
     * Sets the value of the abbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setABBR(String value) {
        this.abbr = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the sicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICNM() {
        return sicnm;
    }

    /**
     * Sets the value of the sicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICNM(String value) {
        this.sicnm = value;
    }

    /**
     * Gets the value of the secsiccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSECSICCD() {
        return secsiccd;
    }

    /**
     * Sets the value of the secsiccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSECSICCD(String value) {
        this.secsiccd = value;
    }

    /**
     * Gets the value of the secsicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSECSICNM() {
        return secsicnm;
    }

    /**
     * Sets the value of the secsicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSECSICNM(String value) {
        this.secsicnm = value;
    }

    /**
     * Gets the value of the addrchgflg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDRCHGFLG() {
        return addrchgflg;
    }

    /**
     * Sets the value of the addrchgflg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDRCHGFLG(String value) {
        this.addrchgflg = value;
    }

    /**
     * Gets the value of the namechgflg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAMECHGFLG() {
        return namechgflg;
    }

    /**
     * Sets the value of the namechgflg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAMECHGFLG(String value) {
        this.namechgflg = value;
    }

    /**
     * Gets the value of the internetaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTERNETADDR() {
        return internetaddr;
    }

    /**
     * Sets the value of the internetaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTERNETADDR(String value) {
        this.internetaddr = value;
    }

    /**
     * Gets the value of the custrevnw property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTREVNW() {
        return custrevnw;
    }

    /**
     * Sets the value of the custrevnw property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTREVNW(String value) {
        this.custrevnw = value;
    }

    /**
     * Gets the value of the locwithtiv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOCWITHTIV() {
        return locwithtiv;
    }

    /**
     * Sets the value of the locwithtiv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOCWITHTIV(String value) {
        this.locwithtiv = value;
    }

    /**
     * Gets the value of the umboccurlmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUMBOCCURLMT() {
        return umboccurlmt;
    }

    /**
     * Sets the value of the umboccurlmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUMBOCCURLMT(String value) {
        this.umboccurlmt = value;
    }

    /**
     * Gets the value of the paceinelgbind property.
     * 
     */
    public boolean isPACEINELGBIND() {
        return paceinelgbind;
    }

    /**
     * Sets the value of the paceinelgbind property.
     * 
     */
    public void setPACEINELGBIND(boolean value) {
        this.paceinelgbind = value;
    }

    /**
     * Gets the value of the paceinelgbrsn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPACEINELGBRSN() {
        return paceinelgbrsn;
    }

    /**
     * Sets the value of the paceinelgbrsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPACEINELGBRSN(String value) {
        this.paceinelgbrsn = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAddress }
     *     
     */
    public ArrayOfAddress getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAddress }
     *     
     */
    public void setAddress(ArrayOfAddress value) {
        this.address = value;
    }

    /**
     * Gets the value of the telephone property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTelephone }
     *     
     */
    public ArrayOfTelephone getTelephone() {
        return telephone;
    }

    /**
     * Sets the value of the telephone property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTelephone }
     *     
     */
    public void setTelephone(ArrayOfTelephone value) {
        this.telephone = value;
    }

    /**
     * Gets the value of the submissionNameAdditionalInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionNameAdditionalInformation }
     *     
     */
    public ArrayOfSubmissionNameAdditionalInformation getSubmissionNameAdditionalInformation() {
        return submissionNameAdditionalInformation;
    }

    /**
     * Sets the value of the submissionNameAdditionalInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionNameAdditionalInformation }
     *     
     */
    public void setSubmissionNameAdditionalInformation(ArrayOfSubmissionNameAdditionalInformation value) {
        this.submissionNameAdditionalInformation = value;
    }

}
