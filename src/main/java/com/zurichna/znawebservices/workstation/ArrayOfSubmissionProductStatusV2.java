
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSubmissionProductStatusV2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSubmissionProductStatusV2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SubmissionProductStatusV2" type="{http://workstation.znawebservices.zurichna.com}SubmissionProductStatusV2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSubmissionProductStatusV2", propOrder = {
    "submissionProductStatusV2"
})
public class ArrayOfSubmissionProductStatusV2 {

    @XmlElement(name = "SubmissionProductStatusV2", nillable = true)
    protected List<SubmissionProductStatusV2> submissionProductStatusV2;

    /**
     * Gets the value of the submissionProductStatusV2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the submissionProductStatusV2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubmissionProductStatusV2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubmissionProductStatusV2 }
     * 
     * 
     */
    public List<SubmissionProductStatusV2> getSubmissionProductStatusV2() {
        if (submissionProductStatusV2 == null) {
            submissionProductStatusV2 = new ArrayList<SubmissionProductStatusV2>();
        }
        return this.submissionProductStatusV2;
    }

}
