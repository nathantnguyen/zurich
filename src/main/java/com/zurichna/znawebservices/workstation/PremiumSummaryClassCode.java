
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PremiumSummaryClassCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PremiumSummaryClassCode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="expAmount" type="{http://workstation.znawebservices.zurichna.com}ExposureAmount" minOccurs="0"/&gt;
 *         &lt;element name="postalRegion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="premAmount" type="{http://workstation.znawebservices.zurichna.com}PremiumAmount" minOccurs="0"/&gt;
 *         &lt;element name="classCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PremiumSummaryClassCode", propOrder = {
    "expAmount",
    "postalRegion",
    "premAmount",
    "classCode"
})
public class PremiumSummaryClassCode {

    protected ExposureAmount expAmount;
    protected String postalRegion;
    protected PremiumAmount premAmount;
    protected String classCode;

    /**
     * Gets the value of the expAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ExposureAmount }
     *     
     */
    public ExposureAmount getExpAmount() {
        return expAmount;
    }

    /**
     * Sets the value of the expAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExposureAmount }
     *     
     */
    public void setExpAmount(ExposureAmount value) {
        this.expAmount = value;
    }

    /**
     * Gets the value of the postalRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalRegion() {
        return postalRegion;
    }

    /**
     * Sets the value of the postalRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalRegion(String value) {
        this.postalRegion = value;
    }

    /**
     * Gets the value of the premAmount property.
     * 
     * @return
     *     possible object is
     *     {@link PremiumAmount }
     *     
     */
    public PremiumAmount getPremAmount() {
        return premAmount;
    }

    /**
     * Sets the value of the premAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link PremiumAmount }
     *     
     */
    public void setPremAmount(PremiumAmount value) {
        this.premAmount = value;
    }

    /**
     * Gets the value of the classCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassCode() {
        return classCode;
    }

    /**
     * Sets the value of the classCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassCode(String value) {
        this.classCode = value;
    }

}
