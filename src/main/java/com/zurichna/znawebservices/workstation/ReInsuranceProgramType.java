
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReInsuranceProgramType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReInsuranceProgramType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="REINS_PGM_TYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReInsuranceProgramType", propOrder = {
    "reinspgmtyp"
})
public class ReInsuranceProgramType {

    @XmlElement(name = "REINS_PGM_TYP")
    protected String reinspgmtyp;

    /**
     * Gets the value of the reinspgmtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREINSPGMTYP() {
        return reinspgmtyp;
    }

    /**
     * Sets the value of the reinspgmtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREINSPGMTYP(String value) {
        this.reinspgmtyp = value;
    }

}
