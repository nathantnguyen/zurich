
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPolicyRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPolicyRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerPartyId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="policyAgreementId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="submissionId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPolicyRequest", propOrder = {
    "customerPartyId",
    "policyAgreementId",
    "submissionId"
})
public class GetPolicyRequest
    extends RequestBase
{

    protected int customerPartyId;
    protected int policyAgreementId;
    protected int submissionId;

    /**
     * Gets the value of the customerPartyId property.
     * 
     */
    public int getCustomerPartyId() {
        return customerPartyId;
    }

    /**
     * Sets the value of the customerPartyId property.
     * 
     */
    public void setCustomerPartyId(int value) {
        this.customerPartyId = value;
    }

    /**
     * Gets the value of the policyAgreementId property.
     * 
     */
    public int getPolicyAgreementId() {
        return policyAgreementId;
    }

    /**
     * Sets the value of the policyAgreementId property.
     * 
     */
    public void setPolicyAgreementId(int value) {
        this.policyAgreementId = value;
    }

    /**
     * Gets the value of the submissionId property.
     * 
     */
    public int getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     */
    public void setSubmissionId(int value) {
        this.submissionId = value;
    }

}
