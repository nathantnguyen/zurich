
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetAutoIndicationPricResult" type="{http://workstation.znawebservices.zurichna.com}AutoIndicationPrcResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAutoIndicationPricResult"
})
@XmlRootElement(name = "GetAutoIndicationPricResponse")
public class GetAutoIndicationPricResponse {

    @XmlElement(name = "GetAutoIndicationPricResult")
    protected AutoIndicationPrcResponse getAutoIndicationPricResult;

    /**
     * Gets the value of the getAutoIndicationPricResult property.
     * 
     * @return
     *     possible object is
     *     {@link AutoIndicationPrcResponse }
     *     
     */
    public AutoIndicationPrcResponse getGetAutoIndicationPricResult() {
        return getAutoIndicationPricResult;
    }

    /**
     * Sets the value of the getAutoIndicationPricResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoIndicationPrcResponse }
     *     
     */
    public void setGetAutoIndicationPricResult(AutoIndicationPrcResponse value) {
        this.getAutoIndicationPricResult = value;
    }

}
