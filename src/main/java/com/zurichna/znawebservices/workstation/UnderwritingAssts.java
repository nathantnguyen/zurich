
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnderwritingAssts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnderwritingAssts"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UA_NOTES_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UA_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UA_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UA_EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnderwritingAssts", propOrder = {
    "uanotesid",
    "uafstnm",
    "ualstnm",
    "uaemailaddr"
})
public class UnderwritingAssts {

    @XmlElement(name = "UA_NOTES_ID")
    protected String uanotesid;
    @XmlElement(name = "UA_FST_NM")
    protected String uafstnm;
    @XmlElement(name = "UA_LST_NM")
    protected String ualstnm;
    @XmlElement(name = "UA_EMAIL_ADDR")
    protected String uaemailaddr;

    /**
     * Gets the value of the uanotesid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUANOTESID() {
        return uanotesid;
    }

    /**
     * Sets the value of the uanotesid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUANOTESID(String value) {
        this.uanotesid = value;
    }

    /**
     * Gets the value of the uafstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUAFSTNM() {
        return uafstnm;
    }

    /**
     * Sets the value of the uafstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUAFSTNM(String value) {
        this.uafstnm = value;
    }

    /**
     * Gets the value of the ualstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUALSTNM() {
        return ualstnm;
    }

    /**
     * Sets the value of the ualstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUALSTNM(String value) {
        this.ualstnm = value;
    }

    /**
     * Gets the value of the uaemailaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUAEMAILADDR() {
        return uaemailaddr;
    }

    /**
     * Sets the value of the uaemailaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUAEMAILADDR(String value) {
        this.uaemailaddr = value;
    }

}
