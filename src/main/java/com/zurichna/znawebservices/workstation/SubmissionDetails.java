
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmissionDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SMSN_QTE_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="SMSN_BOUND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="productDetails" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionProductDetail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionDetails", propOrder = {
    "smsnid",
    "ptynm",
    "newrenlcd",
    "smsnqtepremamt",
    "smsnboundpremamt",
    "dunsnbr",
    "productDetails"
})
public class SubmissionDetails {

    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "PTY_NM")
    protected String ptynm;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "SMSN_QTE_PREM_AMT", required = true)
    protected BigDecimal smsnqtepremamt;
    @XmlElement(name = "SMSN_BOUND_PREM_AMT", required = true)
    protected BigDecimal smsnboundpremamt;
    @XmlElement(name = "DUNS_NBR")
    protected String dunsnbr;
    protected ArrayOfSubmissionProductDetail productDetails;

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the ptynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTYNM() {
        return ptynm;
    }

    /**
     * Sets the value of the ptynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTYNM(String value) {
        this.ptynm = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the smsnqtepremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSMSNQTEPREMAMT() {
        return smsnqtepremamt;
    }

    /**
     * Sets the value of the smsnqtepremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSMSNQTEPREMAMT(BigDecimal value) {
        this.smsnqtepremamt = value;
    }

    /**
     * Gets the value of the smsnboundpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSMSNBOUNDPREMAMT() {
        return smsnboundpremamt;
    }

    /**
     * Sets the value of the smsnboundpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSMSNBOUNDPREMAMT(BigDecimal value) {
        this.smsnboundpremamt = value;
    }

    /**
     * Gets the value of the dunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNSNBR() {
        return dunsnbr;
    }

    /**
     * Sets the value of the dunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNSNBR(String value) {
        this.dunsnbr = value;
    }

    /**
     * Gets the value of the productDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionProductDetail }
     *     
     */
    public ArrayOfSubmissionProductDetail getProductDetails() {
        return productDetails;
    }

    /**
     * Sets the value of the productDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionProductDetail }
     *     
     */
    public void setProductDetails(ArrayOfSubmissionProductDetail value) {
        this.productDetails = value;
    }

}
