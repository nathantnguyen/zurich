
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetWCCATModellingResultsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetWCCATModellingResultsRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SubmissionId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetWCCATModellingResultsRequest", propOrder = {
    "submissionId"
})
public class GetWCCATModellingResultsRequest
    extends RequestBase
{

    @XmlElement(name = "SubmissionId")
    protected int submissionId;

    /**
     * Gets the value of the submissionId property.
     * 
     */
    public int getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     */
    public void setSubmissionId(int value) {
        this.submissionId = value;
    }

}
