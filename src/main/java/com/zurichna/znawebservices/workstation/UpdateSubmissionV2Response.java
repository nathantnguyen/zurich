
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateSubmissionV2Result" type="{http://workstation.znawebservices.zurichna.com}UpdateSubmissionV2Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateSubmissionV2Result"
})
@XmlRootElement(name = "UpdateSubmissionV2Response")
public class UpdateSubmissionV2Response {

    @XmlElement(name = "UpdateSubmissionV2Result")
    protected UpdateSubmissionV2Response2 updateSubmissionV2Result;

    /**
     * Gets the value of the updateSubmissionV2Result property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateSubmissionV2Response2 }
     *     
     */
    public UpdateSubmissionV2Response2 getUpdateSubmissionV2Result() {
        return updateSubmissionV2Result;
    }

    /**
     * Sets the value of the updateSubmissionV2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateSubmissionV2Response2 }
     *     
     */
    public void setUpdateSubmissionV2Result(UpdateSubmissionV2Response2 value) {
        this.updateSubmissionV2Result = value;
    }

}
