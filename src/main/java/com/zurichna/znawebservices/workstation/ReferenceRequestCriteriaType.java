
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReferenceRequestCriteriaType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReferenceRequestCriteriaType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="GroupType"/&gt;
 *     &lt;enumeration value="LastRetrievedTimestamp"/&gt;
 *     &lt;enumeration value="OrganizationPtyId"/&gt;
 *     &lt;enumeration value="OrganizationId"/&gt;
 *     &lt;enumeration value="OrganizationSearchType"/&gt;
 *     &lt;enumeration value="ProducerName"/&gt;
 *     &lt;enumeration value="ProducerNumber"/&gt;
 *     &lt;enumeration value="StateAbbreviation"/&gt;
 *     &lt;enumeration value="EmployeeName"/&gt;
 *     &lt;enumeration value="EmployeePartyId"/&gt;
 *     &lt;enumeration value="EmployeeNumber"/&gt;
 *     &lt;enumeration value="EmployeePersonnelId"/&gt;
 *     &lt;enumeration value="EmployeeNotesId"/&gt;
 *     &lt;enumeration value="UnderwritingProgramNameOrCode"/&gt;
 *     &lt;enumeration value="ProductNameOrAbbreviation"/&gt;
 *     &lt;enumeration value="SICNameOrCode"/&gt;
 *     &lt;enumeration value="CarrierCode"/&gt;
 *     &lt;enumeration value="ReasonCode"/&gt;
 *     &lt;enumeration value="ReInsuranceProgramType"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ReferenceRequestCriteriaType")
@XmlEnum
public enum ReferenceRequestCriteriaType {

    @XmlEnumValue("GroupType")
    GROUP_TYPE("GroupType"),
    @XmlEnumValue("LastRetrievedTimestamp")
    LAST_RETRIEVED_TIMESTAMP("LastRetrievedTimestamp"),
    @XmlEnumValue("OrganizationPtyId")
    ORGANIZATION_PTY_ID("OrganizationPtyId"),
    @XmlEnumValue("OrganizationId")
    ORGANIZATION_ID("OrganizationId"),
    @XmlEnumValue("OrganizationSearchType")
    ORGANIZATION_SEARCH_TYPE("OrganizationSearchType"),
    @XmlEnumValue("ProducerName")
    PRODUCER_NAME("ProducerName"),
    @XmlEnumValue("ProducerNumber")
    PRODUCER_NUMBER("ProducerNumber"),
    @XmlEnumValue("StateAbbreviation")
    STATE_ABBREVIATION("StateAbbreviation"),
    @XmlEnumValue("EmployeeName")
    EMPLOYEE_NAME("EmployeeName"),
    @XmlEnumValue("EmployeePartyId")
    EMPLOYEE_PARTY_ID("EmployeePartyId"),
    @XmlEnumValue("EmployeeNumber")
    EMPLOYEE_NUMBER("EmployeeNumber"),
    @XmlEnumValue("EmployeePersonnelId")
    EMPLOYEE_PERSONNEL_ID("EmployeePersonnelId"),
    @XmlEnumValue("EmployeeNotesId")
    EMPLOYEE_NOTES_ID("EmployeeNotesId"),
    @XmlEnumValue("UnderwritingProgramNameOrCode")
    UNDERWRITING_PROGRAM_NAME_OR_CODE("UnderwritingProgramNameOrCode"),
    @XmlEnumValue("ProductNameOrAbbreviation")
    PRODUCT_NAME_OR_ABBREVIATION("ProductNameOrAbbreviation"),
    @XmlEnumValue("SICNameOrCode")
    SIC_NAME_OR_CODE("SICNameOrCode"),
    @XmlEnumValue("CarrierCode")
    CARRIER_CODE("CarrierCode"),
    @XmlEnumValue("ReasonCode")
    REASON_CODE("ReasonCode"),
    @XmlEnumValue("ReInsuranceProgramType")
    RE_INSURANCE_PROGRAM_TYPE("ReInsuranceProgramType");
    private final String value;

    ReferenceRequestCriteriaType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReferenceRequestCriteriaType fromValue(String v) {
        for (ReferenceRequestCriteriaType c: ReferenceRequestCriteriaType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
