
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WFMSubmissionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WFMSubmissionResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customer" type="{http://workstation.znawebservices.zurichna.com}WFMCustomer" minOccurs="0"/&gt;
 *         &lt;element name="submission" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmission" minOccurs="0"/&gt;
 *         &lt;element name="policy" type="{http://workstation.znawebservices.zurichna.com}ArrayOfWFMPolicy" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WFMSubmissionResponse", propOrder = {
    "customer",
    "submission",
    "policy"
})
public class WFMSubmissionResponse
    extends ResponseBase
{

    protected WFMCustomer customer;
    protected ArrayOfSubmission submission;
    protected ArrayOfWFMPolicy policy;

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link WFMCustomer }
     *     
     */
    public WFMCustomer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link WFMCustomer }
     *     
     */
    public void setCustomer(WFMCustomer value) {
        this.customer = value;
    }

    /**
     * Gets the value of the submission property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmission }
     *     
     */
    public ArrayOfSubmission getSubmission() {
        return submission;
    }

    /**
     * Sets the value of the submission property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmission }
     *     
     */
    public void setSubmission(ArrayOfSubmission value) {
        this.submission = value;
    }

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWFMPolicy }
     *     
     */
    public ArrayOfWFMPolicy getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWFMPolicy }
     *     
     */
    public void setPolicy(ArrayOfWFMPolicy value) {
        this.policy = value;
    }

}
