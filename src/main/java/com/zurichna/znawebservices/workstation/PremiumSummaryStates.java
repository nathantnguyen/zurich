
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PremiumSummaryStates complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PremiumSummaryStates"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="premsumclasscode" type="{http://workstation.znawebservices.zurichna.com}ArrayOfPremiumSummaryClassCode" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PremiumSummaryStates", propOrder = {
    "premsumclasscode"
})
public class PremiumSummaryStates {

    protected ArrayOfPremiumSummaryClassCode premsumclasscode;

    /**
     * Gets the value of the premsumclasscode property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPremiumSummaryClassCode }
     *     
     */
    public ArrayOfPremiumSummaryClassCode getPremsumclasscode() {
        return premsumclasscode;
    }

    /**
     * Sets the value of the premsumclasscode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPremiumSummaryClassCode }
     *     
     */
    public void setPremsumclasscode(ArrayOfPremiumSummaryClassCode value) {
        this.premsumclasscode = value;
    }

}
