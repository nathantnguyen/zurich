
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmissionProducts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionProducts"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ACQN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BKD_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CMPT_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="EXPO_LOC_CLS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INCL_EXCL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INS_SPEC_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PARNT_CISPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="QTE_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="RQST_COVG_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RQST_COVG_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CNDA_SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDG_PGM_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OPTN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="TMPL_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDG_PGM_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDT_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PERSONNEL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_STRT_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_DEP_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_CTRY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_AREA_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_XCH_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_TEL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_EXTN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORGL_PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_INS_GRP_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NTL_BRK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDR_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UNDR_EXP_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UNDR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_TYP_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_TYP_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IND_LICENSE_PRDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QLTY_SMSN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_CNTA_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_CNTA_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="submissionProductStatus" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionProductStatuses" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionProducts", propOrder = {
    "acqncd",
    "bkdpremamt",
    "cmptpremamt",
    "expolocclscd",
    "inclexclcd",
    "insspectypid",
    "ndpremamt",
    "newrenlcd",
    "parntcispecid",
    "qtepremamt",
    "rqstcovgeffdt",
    "rqstcovgexpidt",
    "cndasiccd",
    "smsnid",
    "undgpgmid",
    "optnid",
    "custminsspecid",
    "effdt",
    "custminsspecenttistmp",
    "tmplinsspecid",
    "undgpgmcd",
    "undgpgmnm",
    "prdtnm",
    "prdtabbr",
    "sicnm",
    "personnelid",
    "prdrptyid",
    "prdrenttistmp",
    "prdrnbr",
    "prdrnm",
    "prdrstrtaddr",
    "prdrdepaddr",
    "prdrctynm",
    "prdrstabbr",
    "prdrzipcd",
    "prdrctrynm",
    "prdrstscd",
    "prdrareacd",
    "prdrxchnbr",
    "prdrtelnbr",
    "prdrextn",
    "orglprdrnbr",
    "prdrinsgrpid",
    "ntlbrkid",
    "undrptyid",
    "undrid",
    "undreffdt",
    "undrexpdt",
    "undrnm",
    "undrlstnm",
    "undrfstnm",
    "orgid",
    "orgtypnm",
    "orgtypabbr",
    "orgptyid",
    "orgnm",
    "orgabbr",
    "indlicenseprdr",
    "qltysmsn",
    "prdrfstnm",
    "prdrlstnm",
    "prdremail",
    "insdfstnm",
    "insdlstnm",
    "insdemail",
    "prdrcntaind",
    "insdcntaind",
    "submissionProductStatus"
})
public class SubmissionProducts {

    @XmlElement(name = "ACQN_CD")
    protected String acqncd;
    @XmlElement(name = "BKD_PREM_AMT", required = true)
    protected BigDecimal bkdpremamt;
    @XmlElement(name = "CMPT_PREM_AMT", required = true)
    protected BigDecimal cmptpremamt;
    @XmlElement(name = "EXPO_LOC_CLS_CD")
    protected String expolocclscd;
    @XmlElement(name = "INCL_EXCL_CD")
    protected String inclexclcd;
    @XmlElement(name = "INS_SPEC_TYP_ID")
    protected int insspectypid;
    @XmlElement(name = "ND_PREM_AMT", required = true)
    protected BigDecimal ndpremamt;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "PARNT_CISPEC_ID")
    protected int parntcispecid;
    @XmlElement(name = "QTE_PREM_AMT", required = true)
    protected BigDecimal qtepremamt;
    @XmlElement(name = "RQST_COVG_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgeffdt;
    @XmlElement(name = "RQST_COVG_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgexpidt;
    @XmlElement(name = "CNDA_SIC_CD")
    protected String cndasiccd;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "UNDG_PGM_ID")
    protected int undgpgmid;
    @XmlElement(name = "OPTN_ID")
    protected int optnid;
    @XmlElement(name = "CUSTM_INS_SPEC_ID")
    protected int custminsspecid;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "CUSTM_INS_SPEC_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar custminsspecenttistmp;
    @XmlElement(name = "TMPL_INS_SPEC_ID")
    protected int tmplinsspecid;
    @XmlElement(name = "UNDG_PGM_CD")
    protected String undgpgmcd;
    @XmlElement(name = "UNDG_PGM_NM")
    protected String undgpgmnm;
    @XmlElement(name = "PRDT_NM")
    protected String prdtnm;
    @XmlElement(name = "PRDT_ABBR")
    protected String prdtabbr;
    @XmlElement(name = "SIC_NM")
    protected String sicnm;
    @XmlElement(name = "PERSONNEL_ID")
    protected int personnelid;
    @XmlElement(name = "PRDR_PTY_ID")
    protected int prdrptyid;
    @XmlElement(name = "PRDR_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar prdrenttistmp;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "PRDR_NM")
    protected String prdrnm;
    @XmlElement(name = "PRDR_STRT_ADDR")
    protected String prdrstrtaddr;
    @XmlElement(name = "PRDR_DEP_ADDR")
    protected String prdrdepaddr;
    @XmlElement(name = "PRDR_CTY_NM")
    protected String prdrctynm;
    @XmlElement(name = "PRDR_ST_ABBR")
    protected String prdrstabbr;
    @XmlElement(name = "PRDR_ZIP_CD")
    protected String prdrzipcd;
    @XmlElement(name = "PRDR_CTRY_NM")
    protected String prdrctrynm;
    @XmlElement(name = "PRDR_STS_CD")
    protected String prdrstscd;
    @XmlElement(name = "PRDR_AREA_CD")
    protected String prdrareacd;
    @XmlElement(name = "PRDR_XCH_NBR")
    protected String prdrxchnbr;
    @XmlElement(name = "PRDR_TEL_NBR")
    protected String prdrtelnbr;
    @XmlElement(name = "PRDR_EXTN")
    protected String prdrextn;
    @XmlElement(name = "ORGL_PRDR_NBR")
    protected String orglprdrnbr;
    @XmlElement(name = "PRDR_INS_GRP_ID")
    protected String prdrinsgrpid;
    @XmlElement(name = "NTL_BRK_ID")
    protected String ntlbrkid;
    @XmlElement(name = "UNDR_PTY_ID")
    protected int undrptyid;
    @XmlElement(name = "UNDR_ID")
    protected String undrid;
    @XmlElement(name = "UNDR_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar undreffdt;
    @XmlElement(name = "UNDR_EXP_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar undrexpdt;
    @XmlElement(name = "UNDR_NM")
    protected String undrnm;
    @XmlElement(name = "UNDR_LST_NM")
    protected String undrlstnm;
    @XmlElement(name = "UNDR_FST_NM")
    protected String undrfstnm;
    @XmlElement(name = "ORG_ID")
    protected int orgid;
    @XmlElement(name = "ORG_TYP_NM")
    protected String orgtypnm;
    @XmlElement(name = "ORG_TYP_ABBR")
    protected String orgtypabbr;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_NM")
    protected String orgnm;
    @XmlElement(name = "ORG_ABBR")
    protected String orgabbr;
    @XmlElement(name = "IND_LICENSE_PRDR")
    protected String indlicenseprdr;
    @XmlElement(name = "QLTY_SMSN")
    protected String qltysmsn;
    @XmlElement(name = "PRDR_FST_NM")
    protected String prdrfstnm;
    @XmlElement(name = "PRDR_LST_NM")
    protected String prdrlstnm;
    @XmlElement(name = "PRDR_EMAIL")
    protected String prdremail;
    @XmlElement(name = "INSD_FST_NM")
    protected String insdfstnm;
    @XmlElement(name = "INSD_LST_NM")
    protected String insdlstnm;
    @XmlElement(name = "INSD_EMAIL")
    protected String insdemail;
    @XmlElement(name = "PRDR_CNTA_IND")
    protected String prdrcntaind;
    @XmlElement(name = "INSD_CNTA_IND")
    protected String insdcntaind;
    protected ArrayOfSubmissionProductStatuses submissionProductStatus;

    /**
     * Gets the value of the acqncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACQNCD() {
        return acqncd;
    }

    /**
     * Sets the value of the acqncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACQNCD(String value) {
        this.acqncd = value;
    }

    /**
     * Gets the value of the bkdpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBKDPREMAMT() {
        return bkdpremamt;
    }

    /**
     * Sets the value of the bkdpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBKDPREMAMT(BigDecimal value) {
        this.bkdpremamt = value;
    }

    /**
     * Gets the value of the cmptpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCMPTPREMAMT() {
        return cmptpremamt;
    }

    /**
     * Sets the value of the cmptpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCMPTPREMAMT(BigDecimal value) {
        this.cmptpremamt = value;
    }

    /**
     * Gets the value of the expolocclscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPOLOCCLSCD() {
        return expolocclscd;
    }

    /**
     * Sets the value of the expolocclscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPOLOCCLSCD(String value) {
        this.expolocclscd = value;
    }

    /**
     * Gets the value of the inclexclcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINCLEXCLCD() {
        return inclexclcd;
    }

    /**
     * Sets the value of the inclexclcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINCLEXCLCD(String value) {
        this.inclexclcd = value;
    }

    /**
     * Gets the value of the insspectypid property.
     * 
     */
    public int getINSSPECTYPID() {
        return insspectypid;
    }

    /**
     * Sets the value of the insspectypid property.
     * 
     */
    public void setINSSPECTYPID(int value) {
        this.insspectypid = value;
    }

    /**
     * Gets the value of the ndpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNDPREMAMT() {
        return ndpremamt;
    }

    /**
     * Sets the value of the ndpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNDPREMAMT(BigDecimal value) {
        this.ndpremamt = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the parntcispecid property.
     * 
     */
    public int getPARNTCISPECID() {
        return parntcispecid;
    }

    /**
     * Sets the value of the parntcispecid property.
     * 
     */
    public void setPARNTCISPECID(int value) {
        this.parntcispecid = value;
    }

    /**
     * Gets the value of the qtepremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTEPREMAMT() {
        return qtepremamt;
    }

    /**
     * Sets the value of the qtepremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTEPREMAMT(BigDecimal value) {
        this.qtepremamt = value;
    }

    /**
     * Gets the value of the rqstcovgeffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEFFDT() {
        return rqstcovgeffdt;
    }

    /**
     * Sets the value of the rqstcovgeffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEFFDT(XMLGregorianCalendar value) {
        this.rqstcovgeffdt = value;
    }

    /**
     * Gets the value of the rqstcovgexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEXPIDT() {
        return rqstcovgexpidt;
    }

    /**
     * Sets the value of the rqstcovgexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEXPIDT(XMLGregorianCalendar value) {
        this.rqstcovgexpidt = value;
    }

    /**
     * Gets the value of the cndasiccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNDASICCD() {
        return cndasiccd;
    }

    /**
     * Sets the value of the cndasiccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNDASICCD(String value) {
        this.cndasiccd = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the undgpgmid property.
     * 
     */
    public int getUNDGPGMID() {
        return undgpgmid;
    }

    /**
     * Sets the value of the undgpgmid property.
     * 
     */
    public void setUNDGPGMID(int value) {
        this.undgpgmid = value;
    }

    /**
     * Gets the value of the optnid property.
     * 
     */
    public int getOPTNID() {
        return optnid;
    }

    /**
     * Sets the value of the optnid property.
     * 
     */
    public void setOPTNID(int value) {
        this.optnid = value;
    }

    /**
     * Gets the value of the custminsspecid property.
     * 
     */
    public int getCUSTMINSSPECID() {
        return custminsspecid;
    }

    /**
     * Sets the value of the custminsspecid property.
     * 
     */
    public void setCUSTMINSSPECID(int value) {
        this.custminsspecid = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the custminsspecenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCUSTMINSSPECENTTISTMP() {
        return custminsspecenttistmp;
    }

    /**
     * Sets the value of the custminsspecenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCUSTMINSSPECENTTISTMP(XMLGregorianCalendar value) {
        this.custminsspecenttistmp = value;
    }

    /**
     * Gets the value of the tmplinsspecid property.
     * 
     */
    public int getTMPLINSSPECID() {
        return tmplinsspecid;
    }

    /**
     * Sets the value of the tmplinsspecid property.
     * 
     */
    public void setTMPLINSSPECID(int value) {
        this.tmplinsspecid = value;
    }

    /**
     * Gets the value of the undgpgmcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMCD() {
        return undgpgmcd;
    }

    /**
     * Sets the value of the undgpgmcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMCD(String value) {
        this.undgpgmcd = value;
    }

    /**
     * Gets the value of the undgpgmnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMNM() {
        return undgpgmnm;
    }

    /**
     * Sets the value of the undgpgmnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMNM(String value) {
        this.undgpgmnm = value;
    }

    /**
     * Gets the value of the prdtnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTNM() {
        return prdtnm;
    }

    /**
     * Sets the value of the prdtnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTNM(String value) {
        this.prdtnm = value;
    }

    /**
     * Gets the value of the prdtabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTABBR() {
        return prdtabbr;
    }

    /**
     * Sets the value of the prdtabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTABBR(String value) {
        this.prdtabbr = value;
    }

    /**
     * Gets the value of the sicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICNM() {
        return sicnm;
    }

    /**
     * Sets the value of the sicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICNM(String value) {
        this.sicnm = value;
    }

    /**
     * Gets the value of the personnelid property.
     * 
     */
    public int getPERSONNELID() {
        return personnelid;
    }

    /**
     * Sets the value of the personnelid property.
     * 
     */
    public void setPERSONNELID(int value) {
        this.personnelid = value;
    }

    /**
     * Gets the value of the prdrptyid property.
     * 
     */
    public int getPRDRPTYID() {
        return prdrptyid;
    }

    /**
     * Sets the value of the prdrptyid property.
     * 
     */
    public void setPRDRPTYID(int value) {
        this.prdrptyid = value;
    }

    /**
     * Gets the value of the prdrenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPRDRENTTISTMP() {
        return prdrenttistmp;
    }

    /**
     * Sets the value of the prdrenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPRDRENTTISTMP(XMLGregorianCalendar value) {
        this.prdrenttistmp = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the prdrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNM() {
        return prdrnm;
    }

    /**
     * Sets the value of the prdrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNM(String value) {
        this.prdrnm = value;
    }

    /**
     * Gets the value of the prdrstrtaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTRTADDR() {
        return prdrstrtaddr;
    }

    /**
     * Sets the value of the prdrstrtaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTRTADDR(String value) {
        this.prdrstrtaddr = value;
    }

    /**
     * Gets the value of the prdrdepaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRDEPADDR() {
        return prdrdepaddr;
    }

    /**
     * Sets the value of the prdrdepaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRDEPADDR(String value) {
        this.prdrdepaddr = value;
    }

    /**
     * Gets the value of the prdrctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRCTYNM() {
        return prdrctynm;
    }

    /**
     * Sets the value of the prdrctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRCTYNM(String value) {
        this.prdrctynm = value;
    }

    /**
     * Gets the value of the prdrstabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTABBR() {
        return prdrstabbr;
    }

    /**
     * Sets the value of the prdrstabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTABBR(String value) {
        this.prdrstabbr = value;
    }

    /**
     * Gets the value of the prdrzipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRZIPCD() {
        return prdrzipcd;
    }

    /**
     * Sets the value of the prdrzipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRZIPCD(String value) {
        this.prdrzipcd = value;
    }

    /**
     * Gets the value of the prdrctrynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRCTRYNM() {
        return prdrctrynm;
    }

    /**
     * Sets the value of the prdrctrynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRCTRYNM(String value) {
        this.prdrctrynm = value;
    }

    /**
     * Gets the value of the prdrstscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTSCD() {
        return prdrstscd;
    }

    /**
     * Sets the value of the prdrstscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTSCD(String value) {
        this.prdrstscd = value;
    }

    /**
     * Gets the value of the prdrareacd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRAREACD() {
        return prdrareacd;
    }

    /**
     * Sets the value of the prdrareacd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRAREACD(String value) {
        this.prdrareacd = value;
    }

    /**
     * Gets the value of the prdrxchnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRXCHNBR() {
        return prdrxchnbr;
    }

    /**
     * Sets the value of the prdrxchnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRXCHNBR(String value) {
        this.prdrxchnbr = value;
    }

    /**
     * Gets the value of the prdrtelnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRTELNBR() {
        return prdrtelnbr;
    }

    /**
     * Sets the value of the prdrtelnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRTELNBR(String value) {
        this.prdrtelnbr = value;
    }

    /**
     * Gets the value of the prdrextn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDREXTN() {
        return prdrextn;
    }

    /**
     * Sets the value of the prdrextn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDREXTN(String value) {
        this.prdrextn = value;
    }

    /**
     * Gets the value of the orglprdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGLPRDRNBR() {
        return orglprdrnbr;
    }

    /**
     * Sets the value of the orglprdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGLPRDRNBR(String value) {
        this.orglprdrnbr = value;
    }

    /**
     * Gets the value of the prdrinsgrpid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRINSGRPID() {
        return prdrinsgrpid;
    }

    /**
     * Sets the value of the prdrinsgrpid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRINSGRPID(String value) {
        this.prdrinsgrpid = value;
    }

    /**
     * Gets the value of the ntlbrkid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNTLBRKID() {
        return ntlbrkid;
    }

    /**
     * Sets the value of the ntlbrkid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNTLBRKID(String value) {
        this.ntlbrkid = value;
    }

    /**
     * Gets the value of the undrptyid property.
     * 
     */
    public int getUNDRPTYID() {
        return undrptyid;
    }

    /**
     * Sets the value of the undrptyid property.
     * 
     */
    public void setUNDRPTYID(int value) {
        this.undrptyid = value;
    }

    /**
     * Gets the value of the undrid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRID() {
        return undrid;
    }

    /**
     * Sets the value of the undrid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRID(String value) {
        this.undrid = value;
    }

    /**
     * Gets the value of the undreffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUNDREFFDT() {
        return undreffdt;
    }

    /**
     * Sets the value of the undreffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUNDREFFDT(XMLGregorianCalendar value) {
        this.undreffdt = value;
    }

    /**
     * Gets the value of the undrexpdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUNDREXPDT() {
        return undrexpdt;
    }

    /**
     * Sets the value of the undrexpdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUNDREXPDT(XMLGregorianCalendar value) {
        this.undrexpdt = value;
    }

    /**
     * Gets the value of the undrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRNM() {
        return undrnm;
    }

    /**
     * Sets the value of the undrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRNM(String value) {
        this.undrnm = value;
    }

    /**
     * Gets the value of the undrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRLSTNM() {
        return undrlstnm;
    }

    /**
     * Sets the value of the undrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRLSTNM(String value) {
        this.undrlstnm = value;
    }

    /**
     * Gets the value of the undrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRFSTNM() {
        return undrfstnm;
    }

    /**
     * Sets the value of the undrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRFSTNM(String value) {
        this.undrfstnm = value;
    }

    /**
     * Gets the value of the orgid property.
     * 
     */
    public int getORGID() {
        return orgid;
    }

    /**
     * Sets the value of the orgid property.
     * 
     */
    public void setORGID(int value) {
        this.orgid = value;
    }

    /**
     * Gets the value of the orgtypnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGTYPNM() {
        return orgtypnm;
    }

    /**
     * Sets the value of the orgtypnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGTYPNM(String value) {
        this.orgtypnm = value;
    }

    /**
     * Gets the value of the orgtypabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGTYPABBR() {
        return orgtypabbr;
    }

    /**
     * Sets the value of the orgtypabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGTYPABBR(String value) {
        this.orgtypabbr = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGNM() {
        return orgnm;
    }

    /**
     * Sets the value of the orgnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGNM(String value) {
        this.orgnm = value;
    }

    /**
     * Gets the value of the orgabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGABBR() {
        return orgabbr;
    }

    /**
     * Sets the value of the orgabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGABBR(String value) {
        this.orgabbr = value;
    }

    /**
     * Gets the value of the indlicenseprdr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINDLICENSEPRDR() {
        return indlicenseprdr;
    }

    /**
     * Sets the value of the indlicenseprdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINDLICENSEPRDR(String value) {
        this.indlicenseprdr = value;
    }

    /**
     * Gets the value of the qltysmsn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQLTYSMSN() {
        return qltysmsn;
    }

    /**
     * Sets the value of the qltysmsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQLTYSMSN(String value) {
        this.qltysmsn = value;
    }

    /**
     * Gets the value of the prdrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRFSTNM() {
        return prdrfstnm;
    }

    /**
     * Sets the value of the prdrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRFSTNM(String value) {
        this.prdrfstnm = value;
    }

    /**
     * Gets the value of the prdrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRLSTNM() {
        return prdrlstnm;
    }

    /**
     * Sets the value of the prdrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRLSTNM(String value) {
        this.prdrlstnm = value;
    }

    /**
     * Gets the value of the prdremail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDREMAIL() {
        return prdremail;
    }

    /**
     * Sets the value of the prdremail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDREMAIL(String value) {
        this.prdremail = value;
    }

    /**
     * Gets the value of the insdfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDFSTNM() {
        return insdfstnm;
    }

    /**
     * Sets the value of the insdfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDFSTNM(String value) {
        this.insdfstnm = value;
    }

    /**
     * Gets the value of the insdlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDLSTNM() {
        return insdlstnm;
    }

    /**
     * Sets the value of the insdlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDLSTNM(String value) {
        this.insdlstnm = value;
    }

    /**
     * Gets the value of the insdemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDEMAIL() {
        return insdemail;
    }

    /**
     * Sets the value of the insdemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDEMAIL(String value) {
        this.insdemail = value;
    }

    /**
     * Gets the value of the prdrcntaind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRCNTAIND() {
        return prdrcntaind;
    }

    /**
     * Sets the value of the prdrcntaind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRCNTAIND(String value) {
        this.prdrcntaind = value;
    }

    /**
     * Gets the value of the insdcntaind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDCNTAIND() {
        return insdcntaind;
    }

    /**
     * Sets the value of the insdcntaind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDCNTAIND(String value) {
        this.insdcntaind = value;
    }

    /**
     * Gets the value of the submissionProductStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionProductStatuses }
     *     
     */
    public ArrayOfSubmissionProductStatuses getSubmissionProductStatus() {
        return submissionProductStatus;
    }

    /**
     * Sets the value of the submissionProductStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionProductStatuses }
     *     
     */
    public void setSubmissionProductStatus(ArrayOfSubmissionProductStatuses value) {
        this.submissionProductStatus = value;
    }

}
