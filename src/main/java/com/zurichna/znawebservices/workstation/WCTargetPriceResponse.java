
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WCTargetPriceResult" type="{http://workstation.znawebservices.zurichna.com}WCTargetPriceResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "wcTargetPriceResult"
})
@XmlRootElement(name = "WCTargetPriceResponse")
public class WCTargetPriceResponse {

    @XmlElement(name = "WCTargetPriceResult")
    protected WCTargetPriceResponse2 wcTargetPriceResult;

    /**
     * Gets the value of the wcTargetPriceResult property.
     * 
     * @return
     *     possible object is
     *     {@link WCTargetPriceResponse2 }
     *     
     */
    public WCTargetPriceResponse2 getWCTargetPriceResult() {
        return wcTargetPriceResult;
    }

    /**
     * Sets the value of the wcTargetPriceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WCTargetPriceResponse2 }
     *     
     */
    public void setWCTargetPriceResult(WCTargetPriceResponse2 value) {
        this.wcTargetPriceResult = value;
    }

}
