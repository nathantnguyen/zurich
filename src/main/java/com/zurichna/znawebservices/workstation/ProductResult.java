
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ProductResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TMPL_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RQST_COVG_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RQST_COVG_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="BU_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PROJ_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_NOTES_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductResult", propOrder = {
    "smsnid",
    "tmplinsspecid",
    "rqstcovgeffdt",
    "rqstcovgexpidt",
    "buabbr",
    "prdrnbr",
    "polsym",
    "projnm",
    "undrnotesid"
})
public class ProductResult {

    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "TMPL_INS_SPEC_ID")
    protected int tmplinsspecid;
    @XmlElement(name = "RQST_COVG_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgeffdt;
    @XmlElement(name = "RQST_COVG_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgexpidt;
    @XmlElement(name = "BU_ABBR")
    protected String buabbr;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "POL_SYM")
    protected String polsym;
    @XmlElement(name = "PROJ_NM")
    protected String projnm;
    @XmlElement(name = "UNDR_NOTES_ID")
    protected String undrnotesid;

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the tmplinsspecid property.
     * 
     */
    public int getTMPLINSSPECID() {
        return tmplinsspecid;
    }

    /**
     * Sets the value of the tmplinsspecid property.
     * 
     */
    public void setTMPLINSSPECID(int value) {
        this.tmplinsspecid = value;
    }

    /**
     * Gets the value of the rqstcovgeffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEFFDT() {
        return rqstcovgeffdt;
    }

    /**
     * Sets the value of the rqstcovgeffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEFFDT(XMLGregorianCalendar value) {
        this.rqstcovgeffdt = value;
    }

    /**
     * Gets the value of the rqstcovgexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEXPIDT() {
        return rqstcovgexpidt;
    }

    /**
     * Sets the value of the rqstcovgexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEXPIDT(XMLGregorianCalendar value) {
        this.rqstcovgexpidt = value;
    }

    /**
     * Gets the value of the buabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUABBR() {
        return buabbr;
    }

    /**
     * Sets the value of the buabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUABBR(String value) {
        this.buabbr = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the polsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLSYM() {
        return polsym;
    }

    /**
     * Sets the value of the polsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLSYM(String value) {
        this.polsym = value;
    }

    /**
     * Gets the value of the projnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROJNM() {
        return projnm;
    }

    /**
     * Sets the value of the projnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROJNM(String value) {
        this.projnm = value;
    }

    /**
     * Gets the value of the undrnotesid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRNOTESID() {
        return undrnotesid;
    }

    /**
     * Sets the value of the undrnotesid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRNOTESID(String value) {
        this.undrnotesid = value;
    }

}
