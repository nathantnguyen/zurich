
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Address complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Address"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GEOG_LOC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="GEOG_LOC_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRMS_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEP_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNTY_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_NM_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FRGN_POSTAL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FRGN_PRVN_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BLDG_CHAR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LNGD_NBR" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="LTTD_NBR" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="DEP_ADDR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CMNT_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LOC_NBR_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PTY_ROL_GLOC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_ROL_GLOC_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CTRY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = {
    "geoglocid",
    "ptyid",
    "effdt",
    "expidt",
    "enttistmp",
    "geogloctypid",
    "prmsnm",
    "addr",
    "depaddr",
    "ctynm",
    "stabbr",
    "zipcd",
    "cntycd",
    "ctrycd",
    "ctrynmabbr",
    "frgnpostalcd",
    "frgnprvnnm",
    "bldgcharid",
    "lngdnbr",
    "lttdnbr",
    "depaddr2",
    "cmnttxt",
    "locnbrtxt",
    "ptyrolglocid",
    "ptyrolglocenttistmp",
    "ctrynm",
    "changeState"
})
public class Address {

    @XmlElement(name = "GEOG_LOC_ID")
    protected int geoglocid;
    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "GEOG_LOC_TYP_ID")
    protected int geogloctypid;
    @XmlElement(name = "PRMS_NM")
    protected String prmsnm;
    @XmlElement(name = "ADDR")
    protected String addr;
    @XmlElement(name = "DEP_ADDR")
    protected String depaddr;
    @XmlElement(name = "CTY_NM")
    protected String ctynm;
    @XmlElement(name = "ST_ABBR")
    protected String stabbr;
    @XmlElement(name = "ZIP_CD")
    protected String zipcd;
    @XmlElement(name = "CNTY_CD")
    protected String cntycd;
    @XmlElement(name = "CTRY_CD")
    protected String ctrycd;
    @XmlElement(name = "CTRY_NM_ABBR")
    protected String ctrynmabbr;
    @XmlElement(name = "FRGN_POSTAL_CD")
    protected String frgnpostalcd;
    @XmlElement(name = "FRGN_PRVN_NM")
    protected String frgnprvnnm;
    @XmlElement(name = "BLDG_CHAR_ID")
    protected int bldgcharid;
    @XmlElement(name = "LNGD_NBR", required = true)
    protected BigDecimal lngdnbr;
    @XmlElement(name = "LTTD_NBR", required = true)
    protected BigDecimal lttdnbr;
    @XmlElement(name = "DEP_ADDR2")
    protected String depaddr2;
    @XmlElement(name = "CMNT_TXT")
    protected String cmnttxt;
    @XmlElement(name = "LOC_NBR_TXT")
    protected String locnbrtxt;
    @XmlElement(name = "PTY_ROL_GLOC_ID")
    protected int ptyrolglocid;
    @XmlElement(name = "PTY_ROL_GLOC_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ptyrolglocenttistmp;
    @XmlElement(name = "CTRY_NM")
    protected String ctrynm;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the geoglocid property.
     * 
     */
    public int getGEOGLOCID() {
        return geoglocid;
    }

    /**
     * Sets the value of the geoglocid property.
     * 
     */
    public void setGEOGLOCID(int value) {
        this.geoglocid = value;
    }

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the geogloctypid property.
     * 
     */
    public int getGEOGLOCTYPID() {
        return geogloctypid;
    }

    /**
     * Sets the value of the geogloctypid property.
     * 
     */
    public void setGEOGLOCTYPID(int value) {
        this.geogloctypid = value;
    }

    /**
     * Gets the value of the prmsnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRMSNM() {
        return prmsnm;
    }

    /**
     * Sets the value of the prmsnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRMSNM(String value) {
        this.prmsnm = value;
    }

    /**
     * Gets the value of the addr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDR() {
        return addr;
    }

    /**
     * Sets the value of the addr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDR(String value) {
        this.addr = value;
    }

    /**
     * Gets the value of the depaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEPADDR() {
        return depaddr;
    }

    /**
     * Sets the value of the depaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEPADDR(String value) {
        this.depaddr = value;
    }

    /**
     * Gets the value of the ctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTYNM() {
        return ctynm;
    }

    /**
     * Sets the value of the ctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTYNM(String value) {
        this.ctynm = value;
    }

    /**
     * Gets the value of the stabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTABBR() {
        return stabbr;
    }

    /**
     * Sets the value of the stabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTABBR(String value) {
        this.stabbr = value;
    }

    /**
     * Gets the value of the zipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIPCD() {
        return zipcd;
    }

    /**
     * Sets the value of the zipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIPCD(String value) {
        this.zipcd = value;
    }

    /**
     * Gets the value of the cntycd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNTYCD() {
        return cntycd;
    }

    /**
     * Sets the value of the cntycd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNTYCD(String value) {
        this.cntycd = value;
    }

    /**
     * Gets the value of the ctrycd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYCD() {
        return ctrycd;
    }

    /**
     * Sets the value of the ctrycd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYCD(String value) {
        this.ctrycd = value;
    }

    /**
     * Gets the value of the ctrynmabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYNMABBR() {
        return ctrynmabbr;
    }

    /**
     * Sets the value of the ctrynmabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYNMABBR(String value) {
        this.ctrynmabbr = value;
    }

    /**
     * Gets the value of the frgnpostalcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRGNPOSTALCD() {
        return frgnpostalcd;
    }

    /**
     * Sets the value of the frgnpostalcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRGNPOSTALCD(String value) {
        this.frgnpostalcd = value;
    }

    /**
     * Gets the value of the frgnprvnnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRGNPRVNNM() {
        return frgnprvnnm;
    }

    /**
     * Sets the value of the frgnprvnnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRGNPRVNNM(String value) {
        this.frgnprvnnm = value;
    }

    /**
     * Gets the value of the bldgcharid property.
     * 
     */
    public int getBLDGCHARID() {
        return bldgcharid;
    }

    /**
     * Sets the value of the bldgcharid property.
     * 
     */
    public void setBLDGCHARID(int value) {
        this.bldgcharid = value;
    }

    /**
     * Gets the value of the lngdnbr property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLNGDNBR() {
        return lngdnbr;
    }

    /**
     * Sets the value of the lngdnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLNGDNBR(BigDecimal value) {
        this.lngdnbr = value;
    }

    /**
     * Gets the value of the lttdnbr property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLTTDNBR() {
        return lttdnbr;
    }

    /**
     * Sets the value of the lttdnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLTTDNBR(BigDecimal value) {
        this.lttdnbr = value;
    }

    /**
     * Gets the value of the depaddr2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEPADDR2() {
        return depaddr2;
    }

    /**
     * Sets the value of the depaddr2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEPADDR2(String value) {
        this.depaddr2 = value;
    }

    /**
     * Gets the value of the cmnttxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMNTTXT() {
        return cmnttxt;
    }

    /**
     * Sets the value of the cmnttxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMNTTXT(String value) {
        this.cmnttxt = value;
    }

    /**
     * Gets the value of the locnbrtxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOCNBRTXT() {
        return locnbrtxt;
    }

    /**
     * Sets the value of the locnbrtxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOCNBRTXT(String value) {
        this.locnbrtxt = value;
    }

    /**
     * Gets the value of the ptyrolglocid property.
     * 
     */
    public int getPTYROLGLOCID() {
        return ptyrolglocid;
    }

    /**
     * Sets the value of the ptyrolglocid property.
     * 
     */
    public void setPTYROLGLOCID(int value) {
        this.ptyrolglocid = value;
    }

    /**
     * Gets the value of the ptyrolglocenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPTYROLGLOCENTTISTMP() {
        return ptyrolglocenttistmp;
    }

    /**
     * Sets the value of the ptyrolglocenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPTYROLGLOCENTTISTMP(XMLGregorianCalendar value) {
        this.ptyrolglocenttistmp = value;
    }

    /**
     * Gets the value of the ctrynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYNM() {
        return ctrynm;
    }

    /**
     * Sets the value of the ctrynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYNM(String value) {
        this.ctrynm = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
