
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfUnderwritingPrograms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfUnderwritingPrograms"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnderwritingPrograms" type="{http://workstation.znawebservices.zurichna.com}UnderwritingPrograms" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfUnderwritingPrograms", propOrder = {
    "underwritingPrograms"
})
public class ArrayOfUnderwritingPrograms {

    @XmlElement(name = "UnderwritingPrograms", nillable = true)
    protected List<UnderwritingPrograms> underwritingPrograms;

    /**
     * Gets the value of the underwritingPrograms property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the underwritingPrograms property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnderwritingPrograms().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UnderwritingPrograms }
     * 
     * 
     */
    public List<UnderwritingPrograms> getUnderwritingPrograms() {
        if (underwritingPrograms == null) {
            underwritingPrograms = new ArrayList<UnderwritingPrograms>();
        }
        return this.underwritingPrograms;
    }

}
