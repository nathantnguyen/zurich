
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult" type="{http://workstation.znawebservices.zurichna.com}GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult"
})
@XmlRootElement(name = "GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse")
public class GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse {

    @XmlElement(name = "GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult")
    protected GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2 getPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult;

    /**
     * Gets the value of the getPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2 }
     *     
     */
    public GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2 getGetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult() {
        return getPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult;
    }

    /**
     * Sets the value of the getPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2 }
     *     
     */
    public void setGetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult(GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2 value) {
        this.getPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResult = value;
    }

}
