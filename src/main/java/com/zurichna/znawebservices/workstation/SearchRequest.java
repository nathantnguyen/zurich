
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="reportType" type="{http://workstation.znawebservices.zurichna.com}ExtendedSearchReportType"/&gt;
 *         &lt;element name="customer" type="{http://workstation.znawebservices.zurichna.com}CustomerSearchCriteria" minOccurs="0"/&gt;
 *         &lt;element name="submission" type="{http://workstation.znawebservices.zurichna.com}SubmissionSearchCriteria" minOccurs="0"/&gt;
 *         &lt;element name="policy" type="{http://workstation.znawebservices.zurichna.com}PolicySearchCriteria" minOccurs="0"/&gt;
 *         &lt;element name="Insured" type="{http://workstation.znawebservices.zurichna.com}CustomerSearchCriteria" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchRequest", propOrder = {
    "reportType",
    "customer",
    "submission",
    "policy",
    "insured"
})
public class SearchRequest
    extends RequestBase
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ExtendedSearchReportType reportType;
    protected CustomerSearchCriteria customer;
    protected SubmissionSearchCriteria submission;
    protected PolicySearchCriteria policy;
    @XmlElement(name = "Insured")
    protected CustomerSearchCriteria insured;

    /**
     * Gets the value of the reportType property.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedSearchReportType }
     *     
     */
    public ExtendedSearchReportType getReportType() {
        return reportType;
    }

    /**
     * Sets the value of the reportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedSearchReportType }
     *     
     */
    public void setReportType(ExtendedSearchReportType value) {
        this.reportType = value;
    }

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerSearchCriteria }
     *     
     */
    public CustomerSearchCriteria getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerSearchCriteria }
     *     
     */
    public void setCustomer(CustomerSearchCriteria value) {
        this.customer = value;
    }

    /**
     * Gets the value of the submission property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionSearchCriteria }
     *     
     */
    public SubmissionSearchCriteria getSubmission() {
        return submission;
    }

    /**
     * Sets the value of the submission property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionSearchCriteria }
     *     
     */
    public void setSubmission(SubmissionSearchCriteria value) {
        this.submission = value;
    }

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicySearchCriteria }
     *     
     */
    public PolicySearchCriteria getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicySearchCriteria }
     *     
     */
    public void setPolicy(PolicySearchCriteria value) {
        this.policy = value;
    }

    /**
     * Gets the value of the insured property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerSearchCriteria }
     *     
     */
    public CustomerSearchCriteria getInsured() {
        return insured;
    }

    /**
     * Sets the value of the insured property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerSearchCriteria }
     *     
     */
    public void setInsured(CustomerSearchCriteria value) {
        this.insured = value;
    }

}
