
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmissionProductList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionProductList"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="productResult" type="{http://workstation.znawebservices.zurichna.com}ArrayOfProductResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionProductList", propOrder = {
    "smsnid",
    "productResult"
})
public class SubmissionProductList {

    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    protected ArrayOfProductResult productResult;

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the productResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProductResult }
     *     
     */
    public ArrayOfProductResult getProductResult() {
        return productResult;
    }

    /**
     * Sets the value of the productResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProductResult }
     *     
     */
    public void setProductResult(ArrayOfProductResult value) {
        this.productResult = value;
    }

}
