
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Policy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Policy"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="POL_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="POL_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="POL_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDG_PGM_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BKD_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MODU_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_CONTR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="POL_CONTR_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="POL_CONTRS_STS_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="POL_CONTRS_STS_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STS_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RSN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUST_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUST_PTY_ROL_AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUST_PTY_ROL_AGMT_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_ROL_AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_ROL_AGMT_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ORG_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="COMM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="BIL_PLN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BU_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_STRC_ABBR_COMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_STRC_NM_COMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="REN_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BU_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STS_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SMSN_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SYS_SRC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_PTY_ROL_AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_PTY_ROL_AGMT_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORGL_PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INS_GRP_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="INS_GRP_PTY_ROL_AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="INS_GRP_PTY_ROL_AGMT_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="INS_GRP_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AUTO_POL_NBR" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="AUTO_POL_CNT" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ACQN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IND_LICENSE_PRDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNTA_PRDR_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNTA_INSD_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MANG_CARE_BIL_REVW_FEE_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="policyProduct" type="{http://workstation.znawebservices.zurichna.com}ArrayOfPolicyProduct" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Policy", propOrder = {
    "agmtid",
    "poleffdt",
    "polexpidt",
    "polenttistmp",
    "siccd",
    "undgpgmid",
    "undgpgmcd",
    "bkdpremamt",
    "smsnid",
    "newrenlcd",
    "polnbr",
    "modunbr",
    "polsym",
    "polcontrid",
    "polcontrenttistmp",
    "polcontrsstsid",
    "polcontrsstsenttistmp",
    "stscd",
    "stsnm",
    "rsncd",
    "custptyid",
    "custptyrolagmtid",
    "custptyrolagmtenttistmp",
    "orgptyid",
    "orgptyrolagmtid",
    "orgptyrolagmtenttistmp",
    "orgnm",
    "custnm",
    "commamt",
    "bilpln",
    "buabbr",
    "orgptystrcabbrcomb",
    "orgptystrcnmcomb",
    "renind",
    "bndpremamt",
    "sicnm",
    "undgpgmnm",
    "bunm",
    "stseffdt",
    "smsnenttistmp",
    "syssrccd",
    "prdrptyid",
    "prdrptyrolagmtid",
    "prdrptyrolagmtenttistmp",
    "prdrnbr",
    "orglprdrnbr",
    "prdrnm",
    "insgrpptyid",
    "insgrpptyrolagmtid",
    "insgrpptyrolagmtenttistmp",
    "insgrpnm",
    "autopolnbr",
    "autopolcnt",
    "acqncd",
    "indlicenseprdr",
    "prdrfstnm",
    "prdrlstnm",
    "prdremail",
    "insdfstnm",
    "insdlstnm",
    "insdemail",
    "cntaprdrind",
    "cntainsdind",
    "mangcarebilrevwfeecd",
    "changeState",
    "policyProduct"
})
public class Policy {

    @XmlElement(name = "AGMT_ID")
    protected int agmtid;
    @XmlElement(name = "POL_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar poleffdt;
    @XmlElement(name = "POL_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar polexpidt;
    @XmlElement(name = "POL_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar polenttistmp;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "UNDG_PGM_ID")
    protected int undgpgmid;
    @XmlElement(name = "UNDG_PGM_CD")
    protected String undgpgmcd;
    @XmlElement(name = "BKD_PREM_AMT", required = true)
    protected BigDecimal bkdpremamt;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "POL_NBR")
    protected String polnbr;
    @XmlElement(name = "MODU_NBR")
    protected String modunbr;
    @XmlElement(name = "POL_SYM")
    protected String polsym;
    @XmlElement(name = "POL_CONTR_ID")
    protected int polcontrid;
    @XmlElement(name = "POL_CONTR_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar polcontrenttistmp;
    @XmlElement(name = "POL_CONTRS_STS_ID")
    protected int polcontrsstsid;
    @XmlElement(name = "POL_CONTRS_STS_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar polcontrsstsenttistmp;
    @XmlElement(name = "STS_CD")
    protected String stscd;
    @XmlElement(name = "STS_NM")
    protected String stsnm;
    @XmlElement(name = "RSN_CD")
    protected String rsncd;
    @XmlElement(name = "CUST_PTY_ID")
    protected int custptyid;
    @XmlElement(name = "CUST_PTY_ROL_AGMT_ID")
    protected int custptyrolagmtid;
    @XmlElement(name = "CUST_PTY_ROL_AGMT_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar custptyrolagmtenttistmp;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_PTY_ROL_AGMT_ID")
    protected int orgptyrolagmtid;
    @XmlElement(name = "ORG_PTY_ROL_AGMT_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar orgptyrolagmtenttistmp;
    @XmlElement(name = "ORG_NM")
    protected String orgnm;
    @XmlElement(name = "CUST_NM")
    protected String custnm;
    @XmlElement(name = "COMM_AMT", required = true)
    protected BigDecimal commamt;
    @XmlElement(name = "BIL_PLN")
    protected String bilpln;
    @XmlElement(name = "BU_ABBR")
    protected String buabbr;
    @XmlElement(name = "ORG_PTY_STRC_ABBR_COMB")
    protected String orgptystrcabbrcomb;
    @XmlElement(name = "ORG_PTY_STRC_NM_COMB")
    protected String orgptystrcnmcomb;
    @XmlElement(name = "REN_IND")
    protected String renind;
    @XmlElement(name = "BND_PREM_AMT", required = true)
    protected BigDecimal bndpremamt;
    @XmlElement(name = "SIC_NM")
    protected String sicnm;
    @XmlElement(name = "UNDG_PGM_NM")
    protected String undgpgmnm;
    @XmlElement(name = "BU_NM")
    protected String bunm;
    @XmlElement(name = "STS_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar stseffdt;
    @XmlElement(name = "SMSN_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar smsnenttistmp;
    @XmlElement(name = "SYS_SRC_CD")
    protected String syssrccd;
    @XmlElement(name = "PRDR_PTY_ID")
    protected int prdrptyid;
    @XmlElement(name = "PRDR_PTY_ROL_AGMT_ID")
    protected int prdrptyrolagmtid;
    @XmlElement(name = "PRDR_PTY_ROL_AGMT_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar prdrptyrolagmtenttistmp;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "ORGL_PRDR_NBR")
    protected String orglprdrnbr;
    @XmlElement(name = "PRDR_NM")
    protected String prdrnm;
    @XmlElement(name = "INS_GRP_PTY_ID")
    protected int insgrpptyid;
    @XmlElement(name = "INS_GRP_PTY_ROL_AGMT_ID")
    protected int insgrpptyrolagmtid;
    @XmlElement(name = "INS_GRP_PTY_ROL_AGMT_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar insgrpptyrolagmtenttistmp;
    @XmlElement(name = "INS_GRP_NM")
    protected String insgrpnm;
    @XmlElement(name = "AUTO_POL_NBR")
    protected boolean autopolnbr;
    @XmlElement(name = "AUTO_POL_CNT")
    protected int autopolcnt;
    @XmlElement(name = "ACQN_CD")
    protected String acqncd;
    @XmlElement(name = "IND_LICENSE_PRDR")
    protected String indlicenseprdr;
    @XmlElement(name = "PRDR_FST_NM")
    protected String prdrfstnm;
    @XmlElement(name = "PRDR_LST_NM")
    protected String prdrlstnm;
    @XmlElement(name = "PRDR_EMAIL")
    protected String prdremail;
    @XmlElement(name = "INSD_FST_NM")
    protected String insdfstnm;
    @XmlElement(name = "INSD_LST_NM")
    protected String insdlstnm;
    @XmlElement(name = "INSD_EMAIL")
    protected String insdemail;
    @XmlElement(name = "CNTA_PRDR_IND")
    protected String cntaprdrind;
    @XmlElement(name = "CNTA_INSD_IND")
    protected String cntainsdind;
    @XmlElement(name = "MANG_CARE_BIL_REVW_FEE_CD")
    protected String mangcarebilrevwfeecd;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfPolicyProduct policyProduct;

    /**
     * Gets the value of the agmtid property.
     * 
     */
    public int getAGMTID() {
        return agmtid;
    }

    /**
     * Sets the value of the agmtid property.
     * 
     */
    public void setAGMTID(int value) {
        this.agmtid = value;
    }

    /**
     * Gets the value of the poleffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLEFFDT() {
        return poleffdt;
    }

    /**
     * Sets the value of the poleffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLEFFDT(XMLGregorianCalendar value) {
        this.poleffdt = value;
    }

    /**
     * Gets the value of the polexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLEXPIDT() {
        return polexpidt;
    }

    /**
     * Sets the value of the polexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLEXPIDT(XMLGregorianCalendar value) {
        this.polexpidt = value;
    }

    /**
     * Gets the value of the polenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLENTTISTMP() {
        return polenttistmp;
    }

    /**
     * Sets the value of the polenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLENTTISTMP(XMLGregorianCalendar value) {
        this.polenttistmp = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the undgpgmid property.
     * 
     */
    public int getUNDGPGMID() {
        return undgpgmid;
    }

    /**
     * Sets the value of the undgpgmid property.
     * 
     */
    public void setUNDGPGMID(int value) {
        this.undgpgmid = value;
    }

    /**
     * Gets the value of the undgpgmcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMCD() {
        return undgpgmcd;
    }

    /**
     * Sets the value of the undgpgmcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMCD(String value) {
        this.undgpgmcd = value;
    }

    /**
     * Gets the value of the bkdpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBKDPREMAMT() {
        return bkdpremamt;
    }

    /**
     * Sets the value of the bkdpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBKDPREMAMT(BigDecimal value) {
        this.bkdpremamt = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the polnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLNBR() {
        return polnbr;
    }

    /**
     * Sets the value of the polnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLNBR(String value) {
        this.polnbr = value;
    }

    /**
     * Gets the value of the modunbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODUNBR() {
        return modunbr;
    }

    /**
     * Sets the value of the modunbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODUNBR(String value) {
        this.modunbr = value;
    }

    /**
     * Gets the value of the polsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLSYM() {
        return polsym;
    }

    /**
     * Sets the value of the polsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLSYM(String value) {
        this.polsym = value;
    }

    /**
     * Gets the value of the polcontrid property.
     * 
     */
    public int getPOLCONTRID() {
        return polcontrid;
    }

    /**
     * Sets the value of the polcontrid property.
     * 
     */
    public void setPOLCONTRID(int value) {
        this.polcontrid = value;
    }

    /**
     * Gets the value of the polcontrenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLCONTRENTTISTMP() {
        return polcontrenttistmp;
    }

    /**
     * Sets the value of the polcontrenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLCONTRENTTISTMP(XMLGregorianCalendar value) {
        this.polcontrenttistmp = value;
    }

    /**
     * Gets the value of the polcontrsstsid property.
     * 
     */
    public int getPOLCONTRSSTSID() {
        return polcontrsstsid;
    }

    /**
     * Sets the value of the polcontrsstsid property.
     * 
     */
    public void setPOLCONTRSSTSID(int value) {
        this.polcontrsstsid = value;
    }

    /**
     * Gets the value of the polcontrsstsenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLCONTRSSTSENTTISTMP() {
        return polcontrsstsenttistmp;
    }

    /**
     * Sets the value of the polcontrsstsenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLCONTRSSTSENTTISTMP(XMLGregorianCalendar value) {
        this.polcontrsstsenttistmp = value;
    }

    /**
     * Gets the value of the stscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTSCD() {
        return stscd;
    }

    /**
     * Sets the value of the stscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTSCD(String value) {
        this.stscd = value;
    }

    /**
     * Gets the value of the stsnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTSNM() {
        return stsnm;
    }

    /**
     * Sets the value of the stsnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTSNM(String value) {
        this.stsnm = value;
    }

    /**
     * Gets the value of the rsncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRSNCD() {
        return rsncd;
    }

    /**
     * Sets the value of the rsncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRSNCD(String value) {
        this.rsncd = value;
    }

    /**
     * Gets the value of the custptyid property.
     * 
     */
    public int getCUSTPTYID() {
        return custptyid;
    }

    /**
     * Sets the value of the custptyid property.
     * 
     */
    public void setCUSTPTYID(int value) {
        this.custptyid = value;
    }

    /**
     * Gets the value of the custptyrolagmtid property.
     * 
     */
    public int getCUSTPTYROLAGMTID() {
        return custptyrolagmtid;
    }

    /**
     * Sets the value of the custptyrolagmtid property.
     * 
     */
    public void setCUSTPTYROLAGMTID(int value) {
        this.custptyrolagmtid = value;
    }

    /**
     * Gets the value of the custptyrolagmtenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCUSTPTYROLAGMTENTTISTMP() {
        return custptyrolagmtenttistmp;
    }

    /**
     * Sets the value of the custptyrolagmtenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCUSTPTYROLAGMTENTTISTMP(XMLGregorianCalendar value) {
        this.custptyrolagmtenttistmp = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgptyrolagmtid property.
     * 
     */
    public int getORGPTYROLAGMTID() {
        return orgptyrolagmtid;
    }

    /**
     * Sets the value of the orgptyrolagmtid property.
     * 
     */
    public void setORGPTYROLAGMTID(int value) {
        this.orgptyrolagmtid = value;
    }

    /**
     * Gets the value of the orgptyrolagmtenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getORGPTYROLAGMTENTTISTMP() {
        return orgptyrolagmtenttistmp;
    }

    /**
     * Sets the value of the orgptyrolagmtenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setORGPTYROLAGMTENTTISTMP(XMLGregorianCalendar value) {
        this.orgptyrolagmtenttistmp = value;
    }

    /**
     * Gets the value of the orgnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGNM() {
        return orgnm;
    }

    /**
     * Sets the value of the orgnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGNM(String value) {
        this.orgnm = value;
    }

    /**
     * Gets the value of the custnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTNM() {
        return custnm;
    }

    /**
     * Sets the value of the custnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTNM(String value) {
        this.custnm = value;
    }

    /**
     * Gets the value of the commamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCOMMAMT() {
        return commamt;
    }

    /**
     * Sets the value of the commamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCOMMAMT(BigDecimal value) {
        this.commamt = value;
    }

    /**
     * Gets the value of the bilpln property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBILPLN() {
        return bilpln;
    }

    /**
     * Sets the value of the bilpln property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBILPLN(String value) {
        this.bilpln = value;
    }

    /**
     * Gets the value of the buabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUABBR() {
        return buabbr;
    }

    /**
     * Sets the value of the buabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUABBR(String value) {
        this.buabbr = value;
    }

    /**
     * Gets the value of the orgptystrcabbrcomb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGPTYSTRCABBRCOMB() {
        return orgptystrcabbrcomb;
    }

    /**
     * Sets the value of the orgptystrcabbrcomb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGPTYSTRCABBRCOMB(String value) {
        this.orgptystrcabbrcomb = value;
    }

    /**
     * Gets the value of the orgptystrcnmcomb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGPTYSTRCNMCOMB() {
        return orgptystrcnmcomb;
    }

    /**
     * Sets the value of the orgptystrcnmcomb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGPTYSTRCNMCOMB(String value) {
        this.orgptystrcnmcomb = value;
    }

    /**
     * Gets the value of the renind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRENIND() {
        return renind;
    }

    /**
     * Sets the value of the renind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRENIND(String value) {
        this.renind = value;
    }

    /**
     * Gets the value of the bndpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBNDPREMAMT() {
        return bndpremamt;
    }

    /**
     * Sets the value of the bndpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBNDPREMAMT(BigDecimal value) {
        this.bndpremamt = value;
    }

    /**
     * Gets the value of the sicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICNM() {
        return sicnm;
    }

    /**
     * Sets the value of the sicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICNM(String value) {
        this.sicnm = value;
    }

    /**
     * Gets the value of the undgpgmnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMNM() {
        return undgpgmnm;
    }

    /**
     * Sets the value of the undgpgmnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMNM(String value) {
        this.undgpgmnm = value;
    }

    /**
     * Gets the value of the bunm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUNM() {
        return bunm;
    }

    /**
     * Sets the value of the bunm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUNM(String value) {
        this.bunm = value;
    }

    /**
     * Gets the value of the stseffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTSEFFDT() {
        return stseffdt;
    }

    /**
     * Sets the value of the stseffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTSEFFDT(XMLGregorianCalendar value) {
        this.stseffdt = value;
    }

    /**
     * Gets the value of the smsnenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSMSNENTTISTMP() {
        return smsnenttistmp;
    }

    /**
     * Sets the value of the smsnenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSMSNENTTISTMP(XMLGregorianCalendar value) {
        this.smsnenttistmp = value;
    }

    /**
     * Gets the value of the syssrccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSYSSRCCD() {
        return syssrccd;
    }

    /**
     * Sets the value of the syssrccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSYSSRCCD(String value) {
        this.syssrccd = value;
    }

    /**
     * Gets the value of the prdrptyid property.
     * 
     */
    public int getPRDRPTYID() {
        return prdrptyid;
    }

    /**
     * Sets the value of the prdrptyid property.
     * 
     */
    public void setPRDRPTYID(int value) {
        this.prdrptyid = value;
    }

    /**
     * Gets the value of the prdrptyrolagmtid property.
     * 
     */
    public int getPRDRPTYROLAGMTID() {
        return prdrptyrolagmtid;
    }

    /**
     * Sets the value of the prdrptyrolagmtid property.
     * 
     */
    public void setPRDRPTYROLAGMTID(int value) {
        this.prdrptyrolagmtid = value;
    }

    /**
     * Gets the value of the prdrptyrolagmtenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPRDRPTYROLAGMTENTTISTMP() {
        return prdrptyrolagmtenttistmp;
    }

    /**
     * Sets the value of the prdrptyrolagmtenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPRDRPTYROLAGMTENTTISTMP(XMLGregorianCalendar value) {
        this.prdrptyrolagmtenttistmp = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the orglprdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGLPRDRNBR() {
        return orglprdrnbr;
    }

    /**
     * Sets the value of the orglprdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGLPRDRNBR(String value) {
        this.orglprdrnbr = value;
    }

    /**
     * Gets the value of the prdrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNM() {
        return prdrnm;
    }

    /**
     * Sets the value of the prdrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNM(String value) {
        this.prdrnm = value;
    }

    /**
     * Gets the value of the insgrpptyid property.
     * 
     */
    public int getINSGRPPTYID() {
        return insgrpptyid;
    }

    /**
     * Sets the value of the insgrpptyid property.
     * 
     */
    public void setINSGRPPTYID(int value) {
        this.insgrpptyid = value;
    }

    /**
     * Gets the value of the insgrpptyrolagmtid property.
     * 
     */
    public int getINSGRPPTYROLAGMTID() {
        return insgrpptyrolagmtid;
    }

    /**
     * Sets the value of the insgrpptyrolagmtid property.
     * 
     */
    public void setINSGRPPTYROLAGMTID(int value) {
        this.insgrpptyrolagmtid = value;
    }

    /**
     * Gets the value of the insgrpptyrolagmtenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getINSGRPPTYROLAGMTENTTISTMP() {
        return insgrpptyrolagmtenttistmp;
    }

    /**
     * Sets the value of the insgrpptyrolagmtenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setINSGRPPTYROLAGMTENTTISTMP(XMLGregorianCalendar value) {
        this.insgrpptyrolagmtenttistmp = value;
    }

    /**
     * Gets the value of the insgrpnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSGRPNM() {
        return insgrpnm;
    }

    /**
     * Sets the value of the insgrpnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSGRPNM(String value) {
        this.insgrpnm = value;
    }

    /**
     * Gets the value of the autopolnbr property.
     * 
     */
    public boolean isAUTOPOLNBR() {
        return autopolnbr;
    }

    /**
     * Sets the value of the autopolnbr property.
     * 
     */
    public void setAUTOPOLNBR(boolean value) {
        this.autopolnbr = value;
    }

    /**
     * Gets the value of the autopolcnt property.
     * 
     */
    public int getAUTOPOLCNT() {
        return autopolcnt;
    }

    /**
     * Sets the value of the autopolcnt property.
     * 
     */
    public void setAUTOPOLCNT(int value) {
        this.autopolcnt = value;
    }

    /**
     * Gets the value of the acqncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACQNCD() {
        return acqncd;
    }

    /**
     * Sets the value of the acqncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACQNCD(String value) {
        this.acqncd = value;
    }

    /**
     * Gets the value of the indlicenseprdr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINDLICENSEPRDR() {
        return indlicenseprdr;
    }

    /**
     * Sets the value of the indlicenseprdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINDLICENSEPRDR(String value) {
        this.indlicenseprdr = value;
    }

    /**
     * Gets the value of the prdrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRFSTNM() {
        return prdrfstnm;
    }

    /**
     * Sets the value of the prdrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRFSTNM(String value) {
        this.prdrfstnm = value;
    }

    /**
     * Gets the value of the prdrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRLSTNM() {
        return prdrlstnm;
    }

    /**
     * Sets the value of the prdrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRLSTNM(String value) {
        this.prdrlstnm = value;
    }

    /**
     * Gets the value of the prdremail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDREMAIL() {
        return prdremail;
    }

    /**
     * Sets the value of the prdremail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDREMAIL(String value) {
        this.prdremail = value;
    }

    /**
     * Gets the value of the insdfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDFSTNM() {
        return insdfstnm;
    }

    /**
     * Sets the value of the insdfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDFSTNM(String value) {
        this.insdfstnm = value;
    }

    /**
     * Gets the value of the insdlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDLSTNM() {
        return insdlstnm;
    }

    /**
     * Sets the value of the insdlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDLSTNM(String value) {
        this.insdlstnm = value;
    }

    /**
     * Gets the value of the insdemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDEMAIL() {
        return insdemail;
    }

    /**
     * Sets the value of the insdemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDEMAIL(String value) {
        this.insdemail = value;
    }

    /**
     * Gets the value of the cntaprdrind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNTAPRDRIND() {
        return cntaprdrind;
    }

    /**
     * Sets the value of the cntaprdrind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNTAPRDRIND(String value) {
        this.cntaprdrind = value;
    }

    /**
     * Gets the value of the cntainsdind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNTAINSDIND() {
        return cntainsdind;
    }

    /**
     * Sets the value of the cntainsdind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNTAINSDIND(String value) {
        this.cntainsdind = value;
    }

    /**
     * Gets the value of the mangcarebilrevwfeecd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMANGCAREBILREVWFEECD() {
        return mangcarebilrevwfeecd;
    }

    /**
     * Sets the value of the mangcarebilrevwfeecd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMANGCAREBILREVWFEECD(String value) {
        this.mangcarebilrevwfeecd = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the policyProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPolicyProduct }
     *     
     */
    public ArrayOfPolicyProduct getPolicyProduct() {
        return policyProduct;
    }

    /**
     * Sets the value of the policyProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPolicyProduct }
     *     
     */
    public void setPolicyProduct(ArrayOfPolicyProduct value) {
        this.policyProduct = value;
    }

}
