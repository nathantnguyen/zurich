
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Telephone complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Telephone"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TELC_NBR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TELC_NBR_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="GEOG_LOC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="AREA_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="XCH_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TE4_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TEL_EXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FRGN_TELC_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PROL_TELC_NBR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PROL_TELC_NBR_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Telephone", propOrder = {
    "telcnbrid",
    "ptyid",
    "telcnbrtypid",
    "geoglocid",
    "areacd",
    "xchnbr",
    "te4NBR",
    "telext",
    "frgntelcnbr",
    "effdt",
    "expidt",
    "enttistmp",
    "proltelcnbrid",
    "proltelcnbrenttistmp",
    "changeState"
})
public class Telephone {

    @XmlElement(name = "TELC_NBR_ID")
    protected int telcnbrid;
    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "TELC_NBR_TYP_ID")
    protected int telcnbrtypid;
    @XmlElement(name = "GEOG_LOC_ID")
    protected int geoglocid;
    @XmlElement(name = "AREA_CD")
    protected String areacd;
    @XmlElement(name = "XCH_NBR")
    protected String xchnbr;
    @XmlElement(name = "TE4_NBR")
    protected String te4NBR;
    @XmlElement(name = "TEL_EXT")
    protected String telext;
    @XmlElement(name = "FRGN_TELC_NBR")
    protected String frgntelcnbr;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "PROL_TELC_NBR_ID")
    protected int proltelcnbrid;
    @XmlElement(name = "PROL_TELC_NBR_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar proltelcnbrenttistmp;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the telcnbrid property.
     * 
     */
    public int getTELCNBRID() {
        return telcnbrid;
    }

    /**
     * Sets the value of the telcnbrid property.
     * 
     */
    public void setTELCNBRID(int value) {
        this.telcnbrid = value;
    }

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the telcnbrtypid property.
     * 
     */
    public int getTELCNBRTYPID() {
        return telcnbrtypid;
    }

    /**
     * Sets the value of the telcnbrtypid property.
     * 
     */
    public void setTELCNBRTYPID(int value) {
        this.telcnbrtypid = value;
    }

    /**
     * Gets the value of the geoglocid property.
     * 
     */
    public int getGEOGLOCID() {
        return geoglocid;
    }

    /**
     * Sets the value of the geoglocid property.
     * 
     */
    public void setGEOGLOCID(int value) {
        this.geoglocid = value;
    }

    /**
     * Gets the value of the areacd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAREACD() {
        return areacd;
    }

    /**
     * Sets the value of the areacd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAREACD(String value) {
        this.areacd = value;
    }

    /**
     * Gets the value of the xchnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXCHNBR() {
        return xchnbr;
    }

    /**
     * Sets the value of the xchnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXCHNBR(String value) {
        this.xchnbr = value;
    }

    /**
     * Gets the value of the te4NBR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTE4NBR() {
        return te4NBR;
    }

    /**
     * Sets the value of the te4NBR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTE4NBR(String value) {
        this.te4NBR = value;
    }

    /**
     * Gets the value of the telext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTELEXT() {
        return telext;
    }

    /**
     * Sets the value of the telext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTELEXT(String value) {
        this.telext = value;
    }

    /**
     * Gets the value of the frgntelcnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRGNTELCNBR() {
        return frgntelcnbr;
    }

    /**
     * Sets the value of the frgntelcnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRGNTELCNBR(String value) {
        this.frgntelcnbr = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the proltelcnbrid property.
     * 
     */
    public int getPROLTELCNBRID() {
        return proltelcnbrid;
    }

    /**
     * Sets the value of the proltelcnbrid property.
     * 
     */
    public void setPROLTELCNBRID(int value) {
        this.proltelcnbrid = value;
    }

    /**
     * Gets the value of the proltelcnbrenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPROLTELCNBRENTTISTMP() {
        return proltelcnbrenttistmp;
    }

    /**
     * Sets the value of the proltelcnbrenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPROLTELCNBRENTTISTMP(XMLGregorianCalendar value) {
        this.proltelcnbrenttistmp = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
