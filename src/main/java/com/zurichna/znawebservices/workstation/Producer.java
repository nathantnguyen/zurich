
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Producer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Producer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PRDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORGL_PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="COMB_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORGL_COMB_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Selected" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="STRT_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEP_STRT_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AREA_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="XCH_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TE4_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TEL_EXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FAX_TEL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_STS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Producer", propOrder = {
    "prdrptyid",
    "prdrnbr",
    "orglprdrnbr",
    "nm",
    "combnm",
    "orglcombnm",
    "selected",
    "strtaddr",
    "depstrtaddr",
    "ctynm",
    "stabbr",
    "zipcd",
    "ctrynm",
    "areacd",
    "xchnbr",
    "te4NBR",
    "telext",
    "faxtelnbr",
    "prdrsts"
})
public class Producer {

    @XmlElement(name = "PRDR_PTY_ID")
    protected int prdrptyid;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "ORGL_PRDR_NBR")
    protected String orglprdrnbr;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "COMB_NM")
    protected String combnm;
    @XmlElement(name = "ORGL_COMB_NM")
    protected String orglcombnm;
    @XmlElement(name = "Selected")
    protected boolean selected;
    @XmlElement(name = "STRT_ADDR")
    protected String strtaddr;
    @XmlElement(name = "DEP_STRT_ADDR")
    protected String depstrtaddr;
    @XmlElement(name = "CTY_NM")
    protected String ctynm;
    @XmlElement(name = "ST_ABBR")
    protected String stabbr;
    @XmlElement(name = "ZIP_CD")
    protected String zipcd;
    @XmlElement(name = "CTRY_NM")
    protected String ctrynm;
    @XmlElement(name = "AREA_CD")
    protected String areacd;
    @XmlElement(name = "XCH_NBR")
    protected String xchnbr;
    @XmlElement(name = "TE4_NBR")
    protected String te4NBR;
    @XmlElement(name = "TEL_EXT")
    protected String telext;
    @XmlElement(name = "FAX_TEL_NBR")
    protected String faxtelnbr;
    @XmlElement(name = "PRDR_STS")
    protected String prdrsts;

    /**
     * Gets the value of the prdrptyid property.
     * 
     */
    public int getPRDRPTYID() {
        return prdrptyid;
    }

    /**
     * Sets the value of the prdrptyid property.
     * 
     */
    public void setPRDRPTYID(int value) {
        this.prdrptyid = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the orglprdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGLPRDRNBR() {
        return orglprdrnbr;
    }

    /**
     * Sets the value of the orglprdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGLPRDRNBR(String value) {
        this.orglprdrnbr = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the combnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMBNM() {
        return combnm;
    }

    /**
     * Sets the value of the combnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMBNM(String value) {
        this.combnm = value;
    }

    /**
     * Gets the value of the orglcombnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGLCOMBNM() {
        return orglcombnm;
    }

    /**
     * Sets the value of the orglcombnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGLCOMBNM(String value) {
        this.orglcombnm = value;
    }

    /**
     * Gets the value of the selected property.
     * 
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets the value of the selected property.
     * 
     */
    public void setSelected(boolean value) {
        this.selected = value;
    }

    /**
     * Gets the value of the strtaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTRTADDR() {
        return strtaddr;
    }

    /**
     * Sets the value of the strtaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTRTADDR(String value) {
        this.strtaddr = value;
    }

    /**
     * Gets the value of the depstrtaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEPSTRTADDR() {
        return depstrtaddr;
    }

    /**
     * Sets the value of the depstrtaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEPSTRTADDR(String value) {
        this.depstrtaddr = value;
    }

    /**
     * Gets the value of the ctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTYNM() {
        return ctynm;
    }

    /**
     * Sets the value of the ctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTYNM(String value) {
        this.ctynm = value;
    }

    /**
     * Gets the value of the stabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTABBR() {
        return stabbr;
    }

    /**
     * Sets the value of the stabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTABBR(String value) {
        this.stabbr = value;
    }

    /**
     * Gets the value of the zipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIPCD() {
        return zipcd;
    }

    /**
     * Sets the value of the zipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIPCD(String value) {
        this.zipcd = value;
    }

    /**
     * Gets the value of the ctrynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYNM() {
        return ctrynm;
    }

    /**
     * Sets the value of the ctrynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYNM(String value) {
        this.ctrynm = value;
    }

    /**
     * Gets the value of the areacd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAREACD() {
        return areacd;
    }

    /**
     * Sets the value of the areacd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAREACD(String value) {
        this.areacd = value;
    }

    /**
     * Gets the value of the xchnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXCHNBR() {
        return xchnbr;
    }

    /**
     * Sets the value of the xchnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXCHNBR(String value) {
        this.xchnbr = value;
    }

    /**
     * Gets the value of the te4NBR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTE4NBR() {
        return te4NBR;
    }

    /**
     * Sets the value of the te4NBR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTE4NBR(String value) {
        this.te4NBR = value;
    }

    /**
     * Gets the value of the telext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTELEXT() {
        return telext;
    }

    /**
     * Sets the value of the telext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTELEXT(String value) {
        this.telext = value;
    }

    /**
     * Gets the value of the faxtelnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFAXTELNBR() {
        return faxtelnbr;
    }

    /**
     * Sets the value of the faxtelnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFAXTELNBR(String value) {
        this.faxtelnbr = value;
    }

    /**
     * Gets the value of the prdrsts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTS() {
        return prdrsts;
    }

    /**
     * Sets the value of the prdrsts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTS(String value) {
        this.prdrsts = value;
    }

}
