
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetRMSLoginGivenSubmissionProductIdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetRMSLoginGivenSubmissionProductIdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RMS_USR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RMS_PWD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RMS_LOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetRMSLoginGivenSubmissionProductIdResponse", propOrder = {
    "rmsusrnm",
    "rmspwd",
    "rmslob"
})
public class GetRMSLoginGivenSubmissionProductIdResponse2
    extends ResponseBase
{

    @XmlElement(name = "RMS_USR_NM")
    protected String rmsusrnm;
    @XmlElement(name = "RMS_PWD")
    protected String rmspwd;
    @XmlElement(name = "RMS_LOB")
    protected String rmslob;

    /**
     * Gets the value of the rmsusrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRMSUSRNM() {
        return rmsusrnm;
    }

    /**
     * Sets the value of the rmsusrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRMSUSRNM(String value) {
        this.rmsusrnm = value;
    }

    /**
     * Gets the value of the rmspwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRMSPWD() {
        return rmspwd;
    }

    /**
     * Sets the value of the rmspwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRMSPWD(String value) {
        this.rmspwd = value;
    }

    /**
     * Gets the value of the rmslob property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRMSLOB() {
        return rmslob;
    }

    /**
     * Sets the value of the rmslob property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRMSLOB(String value) {
        this.rmslob = value;
    }

}
