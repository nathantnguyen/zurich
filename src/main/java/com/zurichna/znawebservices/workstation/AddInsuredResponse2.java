
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddInsuredResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddInsuredResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addInsured" type="{http://workstation.znawebservices.zurichna.com}ArrayOfInsuredDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddInsuredResponse", propOrder = {
    "addInsured"
})
public class AddInsuredResponse2
    extends ResponseBase
{

    protected ArrayOfInsuredDetails addInsured;

    /**
     * Gets the value of the addInsured property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInsuredDetails }
     *     
     */
    public ArrayOfInsuredDetails getAddInsured() {
        return addInsured;
    }

    /**
     * Sets the value of the addInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInsuredDetails }
     *     
     */
    public void setAddInsured(ArrayOfInsuredDetails value) {
        this.addInsured = value;
    }

}
