
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPolicySymbolsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPolicySymbolsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PolicySymbol" type="{http://workstation.znawebservices.zurichna.com}ArrayOfPolicySymbol" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPolicySymbolsResponse", propOrder = {
    "policySymbol"
})
public class GetPolicySymbolsResponse
    extends ResponseBase
{

    @XmlElement(name = "PolicySymbol")
    protected ArrayOfPolicySymbol policySymbol;

    /**
     * Gets the value of the policySymbol property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPolicySymbol }
     *     
     */
    public ArrayOfPolicySymbol getPolicySymbol() {
        return policySymbol;
    }

    /**
     * Sets the value of the policySymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPolicySymbol }
     *     
     */
    public void setPolicySymbol(ArrayOfPolicySymbol value) {
        this.policySymbol = value;
    }

}
