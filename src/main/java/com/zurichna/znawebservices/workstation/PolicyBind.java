
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PolicyBind complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyBind"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="POL_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="POL_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AUTO_POL_NBR_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="AUTO_POL_CNT" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="MODU_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="INS_GRP_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BIL_TYP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASSM_FM_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INTL_DIR_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IND_LICENSE_PRDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNTA_PRDR_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNTA_INSD_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MANG_CARE_BIL_REVW_FEE_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="REINS_PGM_TYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CAPTV_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DOMCL_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CAPTV_MGR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LIM_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUST_TYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUST_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyBind", propOrder = {
    "poleffdt",
    "polexpidt",
    "polsym",
    "polnbr",
    "autopolnbrind",
    "autopolcnt",
    "modunbr",
    "undgpgmcd",
    "siccd",
    "prdrnbr",
    "orgptyid",
    "insgrpid",
    "biltypcd",
    "newrenlcd",
    "assmfmnm",
    "intldirind",
    "indlicenseprdr",
    "prdrfstnm",
    "prdrlstnm",
    "prdremail",
    "insdfstnm",
    "insdlstnm",
    "insdemail",
    "cntaprdrind",
    "cntainsdind",
    "mangcarebilrevwfeecd",
    "reinspgmtyp",
    "captvnm",
    "domclnm",
    "captvmgrnm",
    "limtxt",
    "custtyp",
    "custnbr",
    "changeState"
})
public class PolicyBind {

    @XmlElement(name = "POL_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar poleffdt;
    @XmlElement(name = "POL_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar polexpidt;
    @XmlElement(name = "POL_SYM")
    protected String polsym;
    @XmlElement(name = "POL_NBR")
    protected String polnbr;
    @XmlElement(name = "AUTO_POL_NBR_IND")
    protected boolean autopolnbrind;
    @XmlElement(name = "AUTO_POL_CNT")
    protected int autopolcnt;
    @XmlElement(name = "MODU_NBR")
    protected String modunbr;
    @XmlElement(name = "UNDG_PGM_CD")
    protected String undgpgmcd;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "INS_GRP_ID")
    protected String insgrpid;
    @XmlElement(name = "BIL_TYP_CD")
    protected String biltypcd;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "ASSM_FM_NM")
    protected String assmfmnm;
    @XmlElement(name = "INTL_DIR_IND")
    protected String intldirind;
    @XmlElement(name = "IND_LICENSE_PRDR")
    protected String indlicenseprdr;
    @XmlElement(name = "PRDR_FST_NM")
    protected String prdrfstnm;
    @XmlElement(name = "PRDR_LST_NM")
    protected String prdrlstnm;
    @XmlElement(name = "PRDR_EMAIL")
    protected String prdremail;
    @XmlElement(name = "INSD_FST_NM")
    protected String insdfstnm;
    @XmlElement(name = "INSD_LST_NM")
    protected String insdlstnm;
    @XmlElement(name = "INSD_EMAIL")
    protected String insdemail;
    @XmlElement(name = "CNTA_PRDR_IND")
    protected String cntaprdrind;
    @XmlElement(name = "CNTA_INSD_IND")
    protected String cntainsdind;
    @XmlElement(name = "MANG_CARE_BIL_REVW_FEE_CD")
    protected String mangcarebilrevwfeecd;
    @XmlElement(name = "REINS_PGM_TYP")
    protected String reinspgmtyp;
    @XmlElement(name = "CAPTV_NM")
    protected String captvnm;
    @XmlElement(name = "DOMCL_NM")
    protected String domclnm;
    @XmlElement(name = "CAPTV_MGR_NM")
    protected String captvmgrnm;
    @XmlElement(name = "LIM_TXT")
    protected String limtxt;
    @XmlElement(name = "CUST_TYP")
    protected String custtyp;
    @XmlElement(name = "CUST_NBR")
    protected String custnbr;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the poleffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLEFFDT() {
        return poleffdt;
    }

    /**
     * Sets the value of the poleffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLEFFDT(XMLGregorianCalendar value) {
        this.poleffdt = value;
    }

    /**
     * Gets the value of the polexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLEXPIDT() {
        return polexpidt;
    }

    /**
     * Sets the value of the polexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLEXPIDT(XMLGregorianCalendar value) {
        this.polexpidt = value;
    }

    /**
     * Gets the value of the polsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLSYM() {
        return polsym;
    }

    /**
     * Sets the value of the polsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLSYM(String value) {
        this.polsym = value;
    }

    /**
     * Gets the value of the polnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLNBR() {
        return polnbr;
    }

    /**
     * Sets the value of the polnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLNBR(String value) {
        this.polnbr = value;
    }

    /**
     * Gets the value of the autopolnbrind property.
     * 
     */
    public boolean isAUTOPOLNBRIND() {
        return autopolnbrind;
    }

    /**
     * Sets the value of the autopolnbrind property.
     * 
     */
    public void setAUTOPOLNBRIND(boolean value) {
        this.autopolnbrind = value;
    }

    /**
     * Gets the value of the autopolcnt property.
     * 
     */
    public int getAUTOPOLCNT() {
        return autopolcnt;
    }

    /**
     * Sets the value of the autopolcnt property.
     * 
     */
    public void setAUTOPOLCNT(int value) {
        this.autopolcnt = value;
    }

    /**
     * Gets the value of the modunbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODUNBR() {
        return modunbr;
    }

    /**
     * Sets the value of the modunbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODUNBR(String value) {
        this.modunbr = value;
    }

    /**
     * Gets the value of the undgpgmcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMCD() {
        return undgpgmcd;
    }

    /**
     * Sets the value of the undgpgmcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMCD(String value) {
        this.undgpgmcd = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the insgrpid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSGRPID() {
        return insgrpid;
    }

    /**
     * Sets the value of the insgrpid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSGRPID(String value) {
        this.insgrpid = value;
    }

    /**
     * Gets the value of the biltypcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBILTYPCD() {
        return biltypcd;
    }

    /**
     * Sets the value of the biltypcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBILTYPCD(String value) {
        this.biltypcd = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the assmfmnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSMFMNM() {
        return assmfmnm;
    }

    /**
     * Sets the value of the assmfmnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSMFMNM(String value) {
        this.assmfmnm = value;
    }

    /**
     * Gets the value of the intldirind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTLDIRIND() {
        return intldirind;
    }

    /**
     * Sets the value of the intldirind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTLDIRIND(String value) {
        this.intldirind = value;
    }

    /**
     * Gets the value of the indlicenseprdr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINDLICENSEPRDR() {
        return indlicenseprdr;
    }

    /**
     * Sets the value of the indlicenseprdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINDLICENSEPRDR(String value) {
        this.indlicenseprdr = value;
    }

    /**
     * Gets the value of the prdrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRFSTNM() {
        return prdrfstnm;
    }

    /**
     * Sets the value of the prdrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRFSTNM(String value) {
        this.prdrfstnm = value;
    }

    /**
     * Gets the value of the prdrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRLSTNM() {
        return prdrlstnm;
    }

    /**
     * Sets the value of the prdrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRLSTNM(String value) {
        this.prdrlstnm = value;
    }

    /**
     * Gets the value of the prdremail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDREMAIL() {
        return prdremail;
    }

    /**
     * Sets the value of the prdremail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDREMAIL(String value) {
        this.prdremail = value;
    }

    /**
     * Gets the value of the insdfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDFSTNM() {
        return insdfstnm;
    }

    /**
     * Sets the value of the insdfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDFSTNM(String value) {
        this.insdfstnm = value;
    }

    /**
     * Gets the value of the insdlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDLSTNM() {
        return insdlstnm;
    }

    /**
     * Sets the value of the insdlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDLSTNM(String value) {
        this.insdlstnm = value;
    }

    /**
     * Gets the value of the insdemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDEMAIL() {
        return insdemail;
    }

    /**
     * Sets the value of the insdemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDEMAIL(String value) {
        this.insdemail = value;
    }

    /**
     * Gets the value of the cntaprdrind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNTAPRDRIND() {
        return cntaprdrind;
    }

    /**
     * Sets the value of the cntaprdrind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNTAPRDRIND(String value) {
        this.cntaprdrind = value;
    }

    /**
     * Gets the value of the cntainsdind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNTAINSDIND() {
        return cntainsdind;
    }

    /**
     * Sets the value of the cntainsdind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNTAINSDIND(String value) {
        this.cntainsdind = value;
    }

    /**
     * Gets the value of the mangcarebilrevwfeecd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMANGCAREBILREVWFEECD() {
        return mangcarebilrevwfeecd;
    }

    /**
     * Sets the value of the mangcarebilrevwfeecd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMANGCAREBILREVWFEECD(String value) {
        this.mangcarebilrevwfeecd = value;
    }

    /**
     * Gets the value of the reinspgmtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREINSPGMTYP() {
        return reinspgmtyp;
    }

    /**
     * Sets the value of the reinspgmtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREINSPGMTYP(String value) {
        this.reinspgmtyp = value;
    }

    /**
     * Gets the value of the captvnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPTVNM() {
        return captvnm;
    }

    /**
     * Sets the value of the captvnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPTVNM(String value) {
        this.captvnm = value;
    }

    /**
     * Gets the value of the domclnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOMCLNM() {
        return domclnm;
    }

    /**
     * Sets the value of the domclnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOMCLNM(String value) {
        this.domclnm = value;
    }

    /**
     * Gets the value of the captvmgrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPTVMGRNM() {
        return captvmgrnm;
    }

    /**
     * Sets the value of the captvmgrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPTVMGRNM(String value) {
        this.captvmgrnm = value;
    }

    /**
     * Gets the value of the limtxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLIMTXT() {
        return limtxt;
    }

    /**
     * Sets the value of the limtxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLIMTXT(String value) {
        this.limtxt = value;
    }

    /**
     * Gets the value of the custtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTTYP() {
        return custtyp;
    }

    /**
     * Sets the value of the custtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTTYP(String value) {
        this.custtyp = value;
    }

    /**
     * Gets the value of the custnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTNBR() {
        return custnbr;
    }

    /**
     * Sets the value of the custnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTNBR(String value) {
        this.custnbr = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
