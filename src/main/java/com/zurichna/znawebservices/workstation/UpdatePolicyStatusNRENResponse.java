
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdatePolicyStatusNRENResult" type="{http://workstation.znawebservices.zurichna.com}UpdatePolicyStatusResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updatePolicyStatusNRENResult"
})
@XmlRootElement(name = "UpdatePolicyStatusNRENResponse")
public class UpdatePolicyStatusNRENResponse {

    @XmlElement(name = "UpdatePolicyStatusNRENResult")
    protected UpdatePolicyStatusResponse updatePolicyStatusNRENResult;

    /**
     * Gets the value of the updatePolicyStatusNRENResult property.
     * 
     * @return
     *     possible object is
     *     {@link UpdatePolicyStatusResponse }
     *     
     */
    public UpdatePolicyStatusResponse getUpdatePolicyStatusNRENResult() {
        return updatePolicyStatusNRENResult;
    }

    /**
     * Sets the value of the updatePolicyStatusNRENResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdatePolicyStatusResponse }
     *     
     */
    public void setUpdatePolicyStatusNRENResult(UpdatePolicyStatusResponse value) {
        this.updatePolicyStatusNRENResult = value;
    }

}
