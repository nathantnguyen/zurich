
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for QuickSearchrequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuickSearchrequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TRD_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RQST_COVG_EFF_DT_BEGIN" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RQST_COVG_EFF_DT_END" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RQST_COVG_EXPI_DT_BEGIN" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RQST_COVG_EXPI_DT_END" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuickSearchrequest", propOrder = {
    "ptyid",
    "nm",
    "trdnm",
    "dunsnbr",
    "addr",
    "ctynm",
    "stabbr",
    "zipcd",
    "ctrynm",
    "smsnid",
    "rqstcovgeffdtbegin",
    "rqstcovgeffdtend",
    "rqstcovgexpidtbegin",
    "rqstcovgexpidtend"
})
public class QuickSearchrequest
    extends RequestBase
{

    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "TRD_NM")
    protected String trdnm;
    @XmlElement(name = "DUNS_NBR")
    protected String dunsnbr;
    @XmlElement(name = "ADDR")
    protected String addr;
    @XmlElement(name = "CTY_NM")
    protected String ctynm;
    @XmlElement(name = "ST_ABBR")
    protected String stabbr;
    @XmlElement(name = "ZIP_CD")
    protected String zipcd;
    @XmlElement(name = "CTRY_NM")
    protected String ctrynm;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "RQST_COVG_EFF_DT_BEGIN", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgeffdtbegin;
    @XmlElement(name = "RQST_COVG_EFF_DT_END", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgeffdtend;
    @XmlElement(name = "RQST_COVG_EXPI_DT_BEGIN", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgexpidtbegin;
    @XmlElement(name = "RQST_COVG_EXPI_DT_END", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgexpidtend;

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the trdnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRDNM() {
        return trdnm;
    }

    /**
     * Sets the value of the trdnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRDNM(String value) {
        this.trdnm = value;
    }

    /**
     * Gets the value of the dunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNSNBR() {
        return dunsnbr;
    }

    /**
     * Sets the value of the dunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNSNBR(String value) {
        this.dunsnbr = value;
    }

    /**
     * Gets the value of the addr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDR() {
        return addr;
    }

    /**
     * Sets the value of the addr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDR(String value) {
        this.addr = value;
    }

    /**
     * Gets the value of the ctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTYNM() {
        return ctynm;
    }

    /**
     * Sets the value of the ctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTYNM(String value) {
        this.ctynm = value;
    }

    /**
     * Gets the value of the stabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTABBR() {
        return stabbr;
    }

    /**
     * Sets the value of the stabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTABBR(String value) {
        this.stabbr = value;
    }

    /**
     * Gets the value of the zipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIPCD() {
        return zipcd;
    }

    /**
     * Sets the value of the zipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIPCD(String value) {
        this.zipcd = value;
    }

    /**
     * Gets the value of the ctrynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYNM() {
        return ctrynm;
    }

    /**
     * Sets the value of the ctrynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYNM(String value) {
        this.ctrynm = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the rqstcovgeffdtbegin property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEFFDTBEGIN() {
        return rqstcovgeffdtbegin;
    }

    /**
     * Sets the value of the rqstcovgeffdtbegin property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEFFDTBEGIN(XMLGregorianCalendar value) {
        this.rqstcovgeffdtbegin = value;
    }

    /**
     * Gets the value of the rqstcovgeffdtend property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEFFDTEND() {
        return rqstcovgeffdtend;
    }

    /**
     * Sets the value of the rqstcovgeffdtend property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEFFDTEND(XMLGregorianCalendar value) {
        this.rqstcovgeffdtend = value;
    }

    /**
     * Gets the value of the rqstcovgexpidtbegin property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEXPIDTBEGIN() {
        return rqstcovgexpidtbegin;
    }

    /**
     * Sets the value of the rqstcovgexpidtbegin property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEXPIDTBEGIN(XMLGregorianCalendar value) {
        this.rqstcovgexpidtbegin = value;
    }

    /**
     * Gets the value of the rqstcovgexpidtend property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEXPIDTEND() {
        return rqstcovgexpidtend;
    }

    /**
     * Sets the value of the rqstcovgexpidtend property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEXPIDTEND(XMLGregorianCalendar value) {
        this.rqstcovgexpidtend = value;
    }

}
