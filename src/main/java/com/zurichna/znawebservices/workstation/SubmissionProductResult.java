
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmissionProductResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionProductResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TMPL_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="clearanceResult" type="{http://workstation.znawebservices.zurichna.com}ArrayOfClearanceResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionProductResult", propOrder = {
    "tmplinsspecid",
    "orgid",
    "clearanceResult"
})
public class SubmissionProductResult {

    @XmlElement(name = "TMPL_INS_SPEC_ID")
    protected int tmplinsspecid;
    @XmlElement(name = "ORG_ID")
    protected int orgid;
    protected ArrayOfClearanceResult clearanceResult;

    /**
     * Gets the value of the tmplinsspecid property.
     * 
     */
    public int getTMPLINSSPECID() {
        return tmplinsspecid;
    }

    /**
     * Sets the value of the tmplinsspecid property.
     * 
     */
    public void setTMPLINSSPECID(int value) {
        this.tmplinsspecid = value;
    }

    /**
     * Gets the value of the orgid property.
     * 
     */
    public int getORGID() {
        return orgid;
    }

    /**
     * Sets the value of the orgid property.
     * 
     */
    public void setORGID(int value) {
        this.orgid = value;
    }

    /**
     * Gets the value of the clearanceResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfClearanceResult }
     *     
     */
    public ArrayOfClearanceResult getClearanceResult() {
        return clearanceResult;
    }

    /**
     * Sets the value of the clearanceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfClearanceResult }
     *     
     */
    public void setClearanceResult(ArrayOfClearanceResult value) {
        this.clearanceResult = value;
    }

}
