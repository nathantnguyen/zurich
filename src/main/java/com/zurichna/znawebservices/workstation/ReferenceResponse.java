
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReferenceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReferenceResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="country" type="{http://workstation.znawebservices.zurichna.com}ArrayOfCountry" minOccurs="0"/&gt;
 *         &lt;element name="groupType" type="{http://workstation.znawebservices.zurichna.com}GroupType"/&gt;
 *         &lt;element name="organization" type="{http://workstation.znawebservices.zurichna.com}Organization" minOccurs="0"/&gt;
 *         &lt;element name="organizationSearchType" type="{http://workstation.znawebservices.zurichna.com}OrganizationSearchType"/&gt;
 *         &lt;element name="producer" type="{http://workstation.znawebservices.zurichna.com}ArrayOfProducer" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{http://workstation.znawebservices.zurichna.com}ArrayOfProduct" minOccurs="0"/&gt;
 *         &lt;element name="sic" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSIC" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{http://workstation.znawebservices.zurichna.com}ArrayOfState" minOccurs="0"/&gt;
 *         &lt;element name="employee" type="{http://workstation.znawebservices.zurichna.com}ArrayOfEmployee" minOccurs="0"/&gt;
 *         &lt;element name="underwritingProgram" type="{http://workstation.znawebservices.zurichna.com}ArrayOfUnderwritingProgram" minOccurs="0"/&gt;
 *         &lt;element name="geoLocation" type="{http://workstation.znawebservices.zurichna.com}ArrayOfGeoLocation" minOccurs="0"/&gt;
 *         &lt;element name="carrier" type="{http://workstation.znawebservices.zurichna.com}ArrayOfCarrier" minOccurs="0"/&gt;
 *         &lt;element name="reasonCode" type="{http://workstation.znawebservices.zurichna.com}ArrayOfReasonCode" minOccurs="0"/&gt;
 *         &lt;element name="ripType" type="{http://workstation.znawebservices.zurichna.com}ArrayOfReInsuranceProgramType" minOccurs="0"/&gt;
 *         &lt;element name="CompPrice" type="{http://workstation.znawebservices.zurichna.com}ArrayOfCompetitorPrice" minOccurs="0"/&gt;
 *         &lt;element name="BillType" type="{http://workstation.znawebservices.zurichna.com}ArrayOfBillType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceResponse", propOrder = {
    "country",
    "groupType",
    "organization",
    "organizationSearchType",
    "producer",
    "product",
    "sic",
    "state",
    "employee",
    "underwritingProgram",
    "geoLocation",
    "carrier",
    "reasonCode",
    "ripType",
    "compPrice",
    "billType"
})
public class ReferenceResponse
    extends ResponseBase
{

    protected ArrayOfCountry country;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected GroupType groupType;
    protected Organization organization;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected OrganizationSearchType organizationSearchType;
    protected ArrayOfProducer producer;
    protected ArrayOfProduct product;
    protected ArrayOfSIC sic;
    protected ArrayOfState state;
    protected ArrayOfEmployee employee;
    protected ArrayOfUnderwritingProgram underwritingProgram;
    protected ArrayOfGeoLocation geoLocation;
    protected ArrayOfCarrier carrier;
    protected ArrayOfReasonCode reasonCode;
    protected ArrayOfReInsuranceProgramType ripType;
    @XmlElement(name = "CompPrice")
    protected ArrayOfCompetitorPrice compPrice;
    @XmlElement(name = "BillType")
    protected ArrayOfBillType billType;

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCountry }
     *     
     */
    public ArrayOfCountry getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCountry }
     *     
     */
    public void setCountry(ArrayOfCountry value) {
        this.country = value;
    }

    /**
     * Gets the value of the groupType property.
     * 
     * @return
     *     possible object is
     *     {@link GroupType }
     *     
     */
    public GroupType getGroupType() {
        return groupType;
    }

    /**
     * Sets the value of the groupType property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupType }
     *     
     */
    public void setGroupType(GroupType value) {
        this.groupType = value;
    }

    /**
     * Gets the value of the organization property.
     * 
     * @return
     *     possible object is
     *     {@link Organization }
     *     
     */
    public Organization getOrganization() {
        return organization;
    }

    /**
     * Sets the value of the organization property.
     * 
     * @param value
     *     allowed object is
     *     {@link Organization }
     *     
     */
    public void setOrganization(Organization value) {
        this.organization = value;
    }

    /**
     * Gets the value of the organizationSearchType property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationSearchType }
     *     
     */
    public OrganizationSearchType getOrganizationSearchType() {
        return organizationSearchType;
    }

    /**
     * Sets the value of the organizationSearchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationSearchType }
     *     
     */
    public void setOrganizationSearchType(OrganizationSearchType value) {
        this.organizationSearchType = value;
    }

    /**
     * Gets the value of the producer property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProducer }
     *     
     */
    public ArrayOfProducer getProducer() {
        return producer;
    }

    /**
     * Sets the value of the producer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProducer }
     *     
     */
    public void setProducer(ArrayOfProducer value) {
        this.producer = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProduct }
     *     
     */
    public ArrayOfProduct getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProduct }
     *     
     */
    public void setProduct(ArrayOfProduct value) {
        this.product = value;
    }

    /**
     * Gets the value of the sic property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSIC }
     *     
     */
    public ArrayOfSIC getSic() {
        return sic;
    }

    /**
     * Sets the value of the sic property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSIC }
     *     
     */
    public void setSic(ArrayOfSIC value) {
        this.sic = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfState }
     *     
     */
    public ArrayOfState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfState }
     *     
     */
    public void setState(ArrayOfState value) {
        this.state = value;
    }

    /**
     * Gets the value of the employee property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEmployee }
     *     
     */
    public ArrayOfEmployee getEmployee() {
        return employee;
    }

    /**
     * Sets the value of the employee property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEmployee }
     *     
     */
    public void setEmployee(ArrayOfEmployee value) {
        this.employee = value;
    }

    /**
     * Gets the value of the underwritingProgram property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUnderwritingProgram }
     *     
     */
    public ArrayOfUnderwritingProgram getUnderwritingProgram() {
        return underwritingProgram;
    }

    /**
     * Sets the value of the underwritingProgram property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUnderwritingProgram }
     *     
     */
    public void setUnderwritingProgram(ArrayOfUnderwritingProgram value) {
        this.underwritingProgram = value;
    }

    /**
     * Gets the value of the geoLocation property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGeoLocation }
     *     
     */
    public ArrayOfGeoLocation getGeoLocation() {
        return geoLocation;
    }

    /**
     * Sets the value of the geoLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGeoLocation }
     *     
     */
    public void setGeoLocation(ArrayOfGeoLocation value) {
        this.geoLocation = value;
    }

    /**
     * Gets the value of the carrier property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCarrier }
     *     
     */
    public ArrayOfCarrier getCarrier() {
        return carrier;
    }

    /**
     * Sets the value of the carrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCarrier }
     *     
     */
    public void setCarrier(ArrayOfCarrier value) {
        this.carrier = value;
    }

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfReasonCode }
     *     
     */
    public ArrayOfReasonCode getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfReasonCode }
     *     
     */
    public void setReasonCode(ArrayOfReasonCode value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the ripType property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfReInsuranceProgramType }
     *     
     */
    public ArrayOfReInsuranceProgramType getRipType() {
        return ripType;
    }

    /**
     * Sets the value of the ripType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfReInsuranceProgramType }
     *     
     */
    public void setRipType(ArrayOfReInsuranceProgramType value) {
        this.ripType = value;
    }

    /**
     * Gets the value of the compPrice property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCompetitorPrice }
     *     
     */
    public ArrayOfCompetitorPrice getCompPrice() {
        return compPrice;
    }

    /**
     * Sets the value of the compPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCompetitorPrice }
     *     
     */
    public void setCompPrice(ArrayOfCompetitorPrice value) {
        this.compPrice = value;
    }

    /**
     * Gets the value of the billType property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBillType }
     *     
     */
    public ArrayOfBillType getBillType() {
        return billType;
    }

    /**
     * Sets the value of the billType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBillType }
     *     
     */
    public void setBillType(ArrayOfBillType value) {
        this.billType = value;
    }

}
