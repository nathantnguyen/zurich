
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmissionNameAdditionalInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionNameAdditionalInformation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ROL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="FAC_CAS_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SRC_SYS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CAS_PPTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="US_MERIT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CAS_ALT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FAC_PPTY_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INTL_STAT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CFDL_AGMT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BUSN_NTR_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NCCI_RSK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PSY_CD" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ACT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LRG_ACCT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IRDN_ACCT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CI_TGT_ACCT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NA_TGT_ACCT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CI_CUST_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNST_CUST_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NA_CUST_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZADP_CUST_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZAS_CUST_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZI_CUST_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FAC_BM_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EMPE_TOT_CNT" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EMPE_LOCL_CNT" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SLS_VOLM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="SAM_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DED_PROT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUST_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionNameAdditionalInformation", propOrder = {
    "ptyid",
    "rolid",
    "faccasid",
    "srcsyscd",
    "caspptynm",
    "usmeritid",
    "casaltnm",
    "facpptyid",
    "intlstatid",
    "enttistmp",
    "cfdlagmtind",
    "busnntrdesc",
    "nccirskid",
    "psycd",
    "actind",
    "lrgacctind",
    "irdnacctind",
    "citgtacctind",
    "natgtacctind",
    "cicustid",
    "cnstcustid",
    "nacustid",
    "zadpcustid",
    "zascustid",
    "zicustid",
    "facbmid",
    "empetotcnt",
    "empeloclcnt",
    "slsvolmamt",
    "samid",
    "dedprotid",
    "custid",
    "changeState"
})
public class SubmissionNameAdditionalInformation {

    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "ROL_ID")
    protected int rolid;
    @XmlElement(name = "FAC_CAS_ID")
    protected String faccasid;
    @XmlElement(name = "SRC_SYS_CD")
    protected String srcsyscd;
    @XmlElement(name = "CAS_PPTY_NM")
    protected String caspptynm;
    @XmlElement(name = "US_MERIT_ID")
    protected String usmeritid;
    @XmlElement(name = "CAS_ALT_NM")
    protected String casaltnm;
    @XmlElement(name = "FAC_PPTY_ID")
    protected String facpptyid;
    @XmlElement(name = "INTL_STAT_ID")
    protected String intlstatid;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "CFDL_AGMT_IND")
    protected String cfdlagmtind;
    @XmlElement(name = "BUSN_NTR_DESC")
    protected String busnntrdesc;
    @XmlElement(name = "NCCI_RSK_ID")
    protected String nccirskid;
    @XmlElement(name = "PSY_CD")
    protected int psycd;
    @XmlElement(name = "ACT_IND")
    protected String actind;
    @XmlElement(name = "LRG_ACCT_IND")
    protected String lrgacctind;
    @XmlElement(name = "IRDN_ACCT_IND")
    protected String irdnacctind;
    @XmlElement(name = "CI_TGT_ACCT_IND")
    protected String citgtacctind;
    @XmlElement(name = "NA_TGT_ACCT_IND")
    protected String natgtacctind;
    @XmlElement(name = "CI_CUST_ID")
    protected String cicustid;
    @XmlElement(name = "CNST_CUST_ID")
    protected String cnstcustid;
    @XmlElement(name = "NA_CUST_ID")
    protected String nacustid;
    @XmlElement(name = "ZADP_CUST_ID")
    protected String zadpcustid;
    @XmlElement(name = "ZAS_CUST_ID")
    protected String zascustid;
    @XmlElement(name = "ZI_CUST_ID")
    protected String zicustid;
    @XmlElement(name = "FAC_BM_ID")
    protected String facbmid;
    @XmlElement(name = "EMPE_TOT_CNT")
    protected int empetotcnt;
    @XmlElement(name = "EMPE_LOCL_CNT")
    protected int empeloclcnt;
    @XmlElement(name = "SLS_VOLM_AMT", required = true)
    protected BigDecimal slsvolmamt;
    @XmlElement(name = "SAM_ID")
    protected String samid;
    @XmlElement(name = "DED_PROT_ID")
    protected String dedprotid;
    @XmlElement(name = "CUST_ID")
    protected int custid;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the rolid property.
     * 
     */
    public int getROLID() {
        return rolid;
    }

    /**
     * Sets the value of the rolid property.
     * 
     */
    public void setROLID(int value) {
        this.rolid = value;
    }

    /**
     * Gets the value of the faccasid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFACCASID() {
        return faccasid;
    }

    /**
     * Sets the value of the faccasid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFACCASID(String value) {
        this.faccasid = value;
    }

    /**
     * Gets the value of the srcsyscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCSYSCD() {
        return srcsyscd;
    }

    /**
     * Sets the value of the srcsyscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCSYSCD(String value) {
        this.srcsyscd = value;
    }

    /**
     * Gets the value of the caspptynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCASPPTYNM() {
        return caspptynm;
    }

    /**
     * Sets the value of the caspptynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCASPPTYNM(String value) {
        this.caspptynm = value;
    }

    /**
     * Gets the value of the usmeritid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSMERITID() {
        return usmeritid;
    }

    /**
     * Sets the value of the usmeritid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSMERITID(String value) {
        this.usmeritid = value;
    }

    /**
     * Gets the value of the casaltnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCASALTNM() {
        return casaltnm;
    }

    /**
     * Sets the value of the casaltnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCASALTNM(String value) {
        this.casaltnm = value;
    }

    /**
     * Gets the value of the facpptyid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFACPPTYID() {
        return facpptyid;
    }

    /**
     * Sets the value of the facpptyid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFACPPTYID(String value) {
        this.facpptyid = value;
    }

    /**
     * Gets the value of the intlstatid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTLSTATID() {
        return intlstatid;
    }

    /**
     * Sets the value of the intlstatid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTLSTATID(String value) {
        this.intlstatid = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the cfdlagmtind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCFDLAGMTIND() {
        return cfdlagmtind;
    }

    /**
     * Sets the value of the cfdlagmtind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCFDLAGMTIND(String value) {
        this.cfdlagmtind = value;
    }

    /**
     * Gets the value of the busnntrdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNNTRDESC() {
        return busnntrdesc;
    }

    /**
     * Sets the value of the busnntrdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNNTRDESC(String value) {
        this.busnntrdesc = value;
    }

    /**
     * Gets the value of the nccirskid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNCCIRSKID() {
        return nccirskid;
    }

    /**
     * Sets the value of the nccirskid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNCCIRSKID(String value) {
        this.nccirskid = value;
    }

    /**
     * Gets the value of the psycd property.
     * 
     */
    public int getPSYCD() {
        return psycd;
    }

    /**
     * Sets the value of the psycd property.
     * 
     */
    public void setPSYCD(int value) {
        this.psycd = value;
    }

    /**
     * Gets the value of the actind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTIND() {
        return actind;
    }

    /**
     * Sets the value of the actind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTIND(String value) {
        this.actind = value;
    }

    /**
     * Gets the value of the lrgacctind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLRGACCTIND() {
        return lrgacctind;
    }

    /**
     * Sets the value of the lrgacctind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLRGACCTIND(String value) {
        this.lrgacctind = value;
    }

    /**
     * Gets the value of the irdnacctind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIRDNACCTIND() {
        return irdnacctind;
    }

    /**
     * Sets the value of the irdnacctind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIRDNACCTIND(String value) {
        this.irdnacctind = value;
    }

    /**
     * Gets the value of the citgtacctind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCITGTACCTIND() {
        return citgtacctind;
    }

    /**
     * Sets the value of the citgtacctind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCITGTACCTIND(String value) {
        this.citgtacctind = value;
    }

    /**
     * Gets the value of the natgtacctind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNATGTACCTIND() {
        return natgtacctind;
    }

    /**
     * Sets the value of the natgtacctind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNATGTACCTIND(String value) {
        this.natgtacctind = value;
    }

    /**
     * Gets the value of the cicustid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCICUSTID() {
        return cicustid;
    }

    /**
     * Sets the value of the cicustid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCICUSTID(String value) {
        this.cicustid = value;
    }

    /**
     * Gets the value of the cnstcustid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNSTCUSTID() {
        return cnstcustid;
    }

    /**
     * Sets the value of the cnstcustid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNSTCUSTID(String value) {
        this.cnstcustid = value;
    }

    /**
     * Gets the value of the nacustid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNACUSTID() {
        return nacustid;
    }

    /**
     * Sets the value of the nacustid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNACUSTID(String value) {
        this.nacustid = value;
    }

    /**
     * Gets the value of the zadpcustid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZADPCUSTID() {
        return zadpcustid;
    }

    /**
     * Sets the value of the zadpcustid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZADPCUSTID(String value) {
        this.zadpcustid = value;
    }

    /**
     * Gets the value of the zascustid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZASCUSTID() {
        return zascustid;
    }

    /**
     * Sets the value of the zascustid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZASCUSTID(String value) {
        this.zascustid = value;
    }

    /**
     * Gets the value of the zicustid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZICUSTID() {
        return zicustid;
    }

    /**
     * Sets the value of the zicustid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZICUSTID(String value) {
        this.zicustid = value;
    }

    /**
     * Gets the value of the facbmid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFACBMID() {
        return facbmid;
    }

    /**
     * Sets the value of the facbmid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFACBMID(String value) {
        this.facbmid = value;
    }

    /**
     * Gets the value of the empetotcnt property.
     * 
     */
    public int getEMPETOTCNT() {
        return empetotcnt;
    }

    /**
     * Sets the value of the empetotcnt property.
     * 
     */
    public void setEMPETOTCNT(int value) {
        this.empetotcnt = value;
    }

    /**
     * Gets the value of the empeloclcnt property.
     * 
     */
    public int getEMPELOCLCNT() {
        return empeloclcnt;
    }

    /**
     * Sets the value of the empeloclcnt property.
     * 
     */
    public void setEMPELOCLCNT(int value) {
        this.empeloclcnt = value;
    }

    /**
     * Gets the value of the slsvolmamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSLSVOLMAMT() {
        return slsvolmamt;
    }

    /**
     * Sets the value of the slsvolmamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSLSVOLMAMT(BigDecimal value) {
        this.slsvolmamt = value;
    }

    /**
     * Gets the value of the samid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSAMID() {
        return samid;
    }

    /**
     * Sets the value of the samid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSAMID(String value) {
        this.samid = value;
    }

    /**
     * Gets the value of the dedprotid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEDPROTID() {
        return dedprotid;
    }

    /**
     * Sets the value of the dedprotid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEDPROTID(String value) {
        this.dedprotid = value;
    }

    /**
     * Gets the value of the custid property.
     * 
     */
    public int getCUSTID() {
        return custid;
    }

    /**
     * Sets the value of the custid property.
     * 
     */
    public void setCUSTID(int value) {
        this.custid = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
