
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AutoLoss complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutoLoss"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PERD_NUM" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LOS_RN_DT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LIAB_CLM_CNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="COMP_CLM_CNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="COLL_CLM_CNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NUM_PWR_UNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoLoss", propOrder = {
    "perdnum",
    "losrndt",
    "liabclmcnt",
    "compclmcnt",
    "collclmcnt",
    "numpwrunt"
})
public class AutoLoss {

    @XmlElement(name = "PERD_NUM")
    protected int perdnum;
    @XmlElement(name = "LOS_RN_DT")
    protected String losrndt;
    @XmlElement(name = "LIAB_CLM_CNT")
    protected String liabclmcnt;
    @XmlElement(name = "COMP_CLM_CNT")
    protected String compclmcnt;
    @XmlElement(name = "COLL_CLM_CNT")
    protected String collclmcnt;
    @XmlElement(name = "NUM_PWR_UNT")
    protected String numpwrunt;

    /**
     * Gets the value of the perdnum property.
     * 
     */
    public int getPERDNUM() {
        return perdnum;
    }

    /**
     * Sets the value of the perdnum property.
     * 
     */
    public void setPERDNUM(int value) {
        this.perdnum = value;
    }

    /**
     * Gets the value of the losrndt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOSRNDT() {
        return losrndt;
    }

    /**
     * Sets the value of the losrndt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOSRNDT(String value) {
        this.losrndt = value;
    }

    /**
     * Gets the value of the liabclmcnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLIABCLMCNT() {
        return liabclmcnt;
    }

    /**
     * Sets the value of the liabclmcnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLIABCLMCNT(String value) {
        this.liabclmcnt = value;
    }

    /**
     * Gets the value of the compclmcnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMPCLMCNT() {
        return compclmcnt;
    }

    /**
     * Sets the value of the compclmcnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMPCLMCNT(String value) {
        this.compclmcnt = value;
    }

    /**
     * Gets the value of the collclmcnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOLLCLMCNT() {
        return collclmcnt;
    }

    /**
     * Sets the value of the collclmcnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOLLCLMCNT(String value) {
        this.collclmcnt = value;
    }

    /**
     * Gets the value of the numpwrunt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMPWRUNT() {
        return numpwrunt;
    }

    /**
     * Sets the value of the numpwrunt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMPWRUNT(String value) {
        this.numpwrunt = value;
    }

}
