
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfPremiumSummaryClassCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPremiumSummaryClassCode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PremiumSummaryClassCode" type="{http://workstation.znawebservices.zurichna.com}PremiumSummaryClassCode" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPremiumSummaryClassCode", propOrder = {
    "premiumSummaryClassCode"
})
public class ArrayOfPremiumSummaryClassCode {

    @XmlElement(name = "PremiumSummaryClassCode", nillable = true)
    protected List<PremiumSummaryClassCode> premiumSummaryClassCode;

    /**
     * Gets the value of the premiumSummaryClassCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the premiumSummaryClassCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPremiumSummaryClassCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PremiumSummaryClassCode }
     * 
     * 
     */
    public List<PremiumSummaryClassCode> getPremiumSummaryClassCode() {
        if (premiumSummaryClassCode == null) {
            premiumSummaryClassCode = new ArrayList<PremiumSummaryClassCode>();
        }
        return this.premiumSummaryClassCode;
    }

}
