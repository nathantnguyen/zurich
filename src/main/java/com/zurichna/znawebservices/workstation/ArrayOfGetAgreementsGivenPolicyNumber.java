
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetAgreementsGivenPolicyNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetAgreementsGivenPolicyNumber"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetAgreementsGivenPolicyNumber" type="{http://workstation.znawebservices.zurichna.com}GetAgreementsGivenPolicyNumber" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetAgreementsGivenPolicyNumber", propOrder = {
    "getAgreementsGivenPolicyNumber"
})
public class ArrayOfGetAgreementsGivenPolicyNumber {

    @XmlElement(name = "GetAgreementsGivenPolicyNumber", nillable = true)
    protected List<GetAgreementsGivenPolicyNumber2> getAgreementsGivenPolicyNumber;

    /**
     * Gets the value of the getAgreementsGivenPolicyNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getAgreementsGivenPolicyNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetAgreementsGivenPolicyNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetAgreementsGivenPolicyNumber2 }
     * 
     * 
     */
    public List<GetAgreementsGivenPolicyNumber2> getGetAgreementsGivenPolicyNumber() {
        if (getAgreementsGivenPolicyNumber == null) {
            getAgreementsGivenPolicyNumber = new ArrayList<GetAgreementsGivenPolicyNumber2>();
        }
        return this.getAgreementsGivenPolicyNumber;
    }

}
