
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactRequestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContactRequestType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SearchEmployee"/&gt;
 *     &lt;enumeration value="SearchProducer"/&gt;
 *     &lt;enumeration value="SearchCustomer"/&gt;
 *     &lt;enumeration value="ByAccountPartyId"/&gt;
 *     &lt;enumeration value="ByAccountAndContactPartyId"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ContactRequestType")
@XmlEnum
public enum ContactRequestType {

    @XmlEnumValue("SearchEmployee")
    SEARCH_EMPLOYEE("SearchEmployee"),
    @XmlEnumValue("SearchProducer")
    SEARCH_PRODUCER("SearchProducer"),
    @XmlEnumValue("SearchCustomer")
    SEARCH_CUSTOMER("SearchCustomer"),
    @XmlEnumValue("ByAccountPartyId")
    BY_ACCOUNT_PARTY_ID("ByAccountPartyId"),
    @XmlEnumValue("ByAccountAndContactPartyId")
    BY_ACCOUNT_AND_CONTACT_PARTY_ID("ByAccountAndContactPartyId");
    private final String value;

    ContactRequestType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContactRequestType fromValue(String v) {
        for (ContactRequestType c: ContactRequestType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
