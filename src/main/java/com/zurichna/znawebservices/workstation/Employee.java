
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Employee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Employee"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ROL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EMPE_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BDGT_CTR_DEPT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BDGT_CTR_BR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NOTES_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PERSONNEL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CMPR_ACCT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CMPR_USR_STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BU_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BU_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Employee", propOrder = {
    "ptyid",
    "fstnm",
    "lstnm",
    "rolid",
    "empenbr",
    "bdgtctrdept",
    "bdgtctrbr",
    "notesid",
    "emailaddr",
    "personnelid",
    "cmpracctid",
    "cmprusrstscd",
    "buid",
    "bunm"
})
public class Employee {

    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "FST_NM")
    protected String fstnm;
    @XmlElement(name = "LST_NM")
    protected String lstnm;
    @XmlElement(name = "ROL_ID")
    protected int rolid;
    @XmlElement(name = "EMPE_NBR")
    protected String empenbr;
    @XmlElement(name = "BDGT_CTR_DEPT")
    protected String bdgtctrdept;
    @XmlElement(name = "BDGT_CTR_BR")
    protected String bdgtctrbr;
    @XmlElement(name = "NOTES_ID")
    protected String notesid;
    @XmlElement(name = "EMAIL_ADDR")
    protected String emailaddr;
    @XmlElement(name = "PERSONNEL_ID")
    protected int personnelid;
    @XmlElement(name = "CMPR_ACCT_ID")
    protected String cmpracctid;
    @XmlElement(name = "CMPR_USR_STS_CD")
    protected String cmprusrstscd;
    @XmlElement(name = "BU_ID")
    protected String buid;
    @XmlElement(name = "BU_NM")
    protected String bunm;

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the fstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFSTNM() {
        return fstnm;
    }

    /**
     * Sets the value of the fstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFSTNM(String value) {
        this.fstnm = value;
    }

    /**
     * Gets the value of the lstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLSTNM() {
        return lstnm;
    }

    /**
     * Sets the value of the lstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLSTNM(String value) {
        this.lstnm = value;
    }

    /**
     * Gets the value of the rolid property.
     * 
     */
    public int getROLID() {
        return rolid;
    }

    /**
     * Sets the value of the rolid property.
     * 
     */
    public void setROLID(int value) {
        this.rolid = value;
    }

    /**
     * Gets the value of the empenbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMPENBR() {
        return empenbr;
    }

    /**
     * Sets the value of the empenbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMPENBR(String value) {
        this.empenbr = value;
    }

    /**
     * Gets the value of the bdgtctrdept property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBDGTCTRDEPT() {
        return bdgtctrdept;
    }

    /**
     * Sets the value of the bdgtctrdept property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBDGTCTRDEPT(String value) {
        this.bdgtctrdept = value;
    }

    /**
     * Gets the value of the bdgtctrbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBDGTCTRBR() {
        return bdgtctrbr;
    }

    /**
     * Sets the value of the bdgtctrbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBDGTCTRBR(String value) {
        this.bdgtctrbr = value;
    }

    /**
     * Gets the value of the notesid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOTESID() {
        return notesid;
    }

    /**
     * Sets the value of the notesid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOTESID(String value) {
        this.notesid = value;
    }

    /**
     * Gets the value of the emailaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMAILADDR() {
        return emailaddr;
    }

    /**
     * Sets the value of the emailaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMAILADDR(String value) {
        this.emailaddr = value;
    }

    /**
     * Gets the value of the personnelid property.
     * 
     */
    public int getPERSONNELID() {
        return personnelid;
    }

    /**
     * Sets the value of the personnelid property.
     * 
     */
    public void setPERSONNELID(int value) {
        this.personnelid = value;
    }

    /**
     * Gets the value of the cmpracctid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMPRACCTID() {
        return cmpracctid;
    }

    /**
     * Sets the value of the cmpracctid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMPRACCTID(String value) {
        this.cmpracctid = value;
    }

    /**
     * Gets the value of the cmprusrstscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMPRUSRSTSCD() {
        return cmprusrstscd;
    }

    /**
     * Sets the value of the cmprusrstscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMPRUSRSTSCD(String value) {
        this.cmprusrstscd = value;
    }

    /**
     * Gets the value of the buid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUID() {
        return buid;
    }

    /**
     * Sets the value of the buid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUID(String value) {
        this.buid = value;
    }

    /**
     * Gets the value of the bunm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUNM() {
        return bunm;
    }

    /**
     * Sets the value of the bunm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUNM(String value) {
        this.bunm = value;
    }

}
