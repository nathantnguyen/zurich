
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BUSN_TRAN_TYP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest", propOrder = {
    "agmtid",
    "busntrantypcd",
    "effdt"
})
public class GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest
    extends RequestBase
{

    @XmlElement(name = "AGMT_ID")
    protected int agmtid;
    @XmlElement(name = "BUSN_TRAN_TYP_CD")
    protected String busntrantypcd;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;

    /**
     * Gets the value of the agmtid property.
     * 
     */
    public int getAGMTID() {
        return agmtid;
    }

    /**
     * Sets the value of the agmtid property.
     * 
     */
    public void setAGMTID(int value) {
        this.agmtid = value;
    }

    /**
     * Gets the value of the busntrantypcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNTRANTYPCD() {
        return busntrantypcd;
    }

    /**
     * Sets the value of the busntrantypcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNTRANTYPCD(String value) {
        this.busntrantypcd = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

}
