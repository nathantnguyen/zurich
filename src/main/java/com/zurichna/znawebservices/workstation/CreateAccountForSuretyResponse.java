
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreateAccountForSuretyResult" type="{http://workstation.znawebservices.zurichna.com}CreateAccountResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createAccountForSuretyResult"
})
@XmlRootElement(name = "CreateAccountForSuretyResponse")
public class CreateAccountForSuretyResponse {

    @XmlElement(name = "CreateAccountForSuretyResult")
    protected CreateAccountResponse2 createAccountForSuretyResult;

    /**
     * Gets the value of the createAccountForSuretyResult property.
     * 
     * @return
     *     possible object is
     *     {@link CreateAccountResponse2 }
     *     
     */
    public CreateAccountResponse2 getCreateAccountForSuretyResult() {
        return createAccountForSuretyResult;
    }

    /**
     * Sets the value of the createAccountForSuretyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateAccountResponse2 }
     *     
     */
    public void setCreateAccountForSuretyResult(CreateAccountResponse2 value) {
        this.createAccountForSuretyResult = value;
    }

}
