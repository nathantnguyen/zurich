
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateAccountResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateAccountResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AddCustomer" type="{http://workstation.znawebservices.zurichna.com}ArrayOfCreateAccount" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateAccountResponse", propOrder = {
    "addCustomer"
})
public class CreateAccountResponse2
    extends ResponseBase
{

    @XmlElement(name = "AddCustomer")
    protected ArrayOfCreateAccount addCustomer;

    /**
     * Gets the value of the addCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCreateAccount }
     *     
     */
    public ArrayOfCreateAccount getAddCustomer() {
        return addCustomer;
    }

    /**
     * Sets the value of the addCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCreateAccount }
     *     
     */
    public void setAddCustomer(ArrayOfCreateAccount value) {
        this.addCustomer = value;
    }

}
