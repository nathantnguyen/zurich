
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdateContactResult" type="{http://workstation.znawebservices.zurichna.com}ContactResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateContactResult"
})
@XmlRootElement(name = "UpdateContactResponse")
public class UpdateContactResponse {

    @XmlElement(name = "UpdateContactResult")
    protected ContactResponse updateContactResult;

    /**
     * Gets the value of the updateContactResult property.
     * 
     * @return
     *     possible object is
     *     {@link ContactResponse }
     *     
     */
    public ContactResponse getUpdateContactResult() {
        return updateContactResult;
    }

    /**
     * Sets the value of the updateContactResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactResponse }
     *     
     */
    public void setUpdateContactResult(ContactResponse value) {
        this.updateContactResult = value;
    }

}
