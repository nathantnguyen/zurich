
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClearanceResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClearanceResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="submissionProductList" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionProductList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClearanceResult", propOrder = {
    "result",
    "submissionProductList"
})
public class ClearanceResult {

    @XmlElement(name = "Result")
    protected String result;
    protected ArrayOfSubmissionProductList submissionProductList;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

    /**
     * Gets the value of the submissionProductList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionProductList }
     *     
     */
    public ArrayOfSubmissionProductList getSubmissionProductList() {
        return submissionProductList;
    }

    /**
     * Sets the value of the submissionProductList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionProductList }
     *     
     */
    public void setSubmissionProductList(ArrayOfSubmissionProductList value) {
        this.submissionProductList = value;
    }

}
