
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requester" type="{http://workstation.znawebservices.zurichna.com}Requester" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestBase", propOrder = {
    "requester"
})
@XmlSeeAlso({
    QuickSearchrequest.class,
    SearchRequest.class,
    GetCustomerRequest.class,
    UpdateCustomerRequest.class,
    GetSubmissionRequest.class,
    GetMVRDataRequest.class,
    UpdateSubmissionRequest.class,
    ReferenceRequest.class,
    GetContactRequest.class,
    UpdateContactRequest.class,
    PolicyRequest.class,
    GetPolicyRequest.class,
    GetPtyRequest.class,
    UpdatePolicyRequest.class,
    GetAlertRequest.class,
    UpdateAlertRequest.class,
    UpdatePolicyStatusRequest.class,
    BindSubmissionRequest.class,
    RenewPolicyRequest.class,
    RenewMultiplePolicyRequest.class,
    AddInsuredRequest.class,
    ModifyInsuredRequest.class,
    AddSubmissionRequest.class,
    CreateAccountRequest.class,
    DelSMSNProductRequest.class,
    DelPolTermRequest.class,
    GetWCTechPriceDtls.class,
    GetAutoIndicationPrcRequest.class,
    GetPolicySymbolRequest.class,
    GetWCAHTargetPrcDtls.class,
    GetAgreementsGivenPolicyNumberRequest.class,
    GetRMSLoginGivenSubmissionProductIdRequest.class,
    ZSpireSMSNIdForRenewalQuoteRequest.class,
    GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest.class,
    GetWFMSubmissionRequest.class,
    GetSMSNDetailsForGivenProducerRequest.class,
    AddModifyAccountRequest.class,
    GetWCCATModellingResultsRequest.class,
    GetTargetPriceRateTrackDtlsRequest.class,
    PerformAutomatedClearanceRequest.class,
    RetrieveUserHistoryRequest.class,
    RecordSubmissionRequest.class,
    UpdateSubmissionV2Request.class,
    SubmissionProductStatusRequest.class,
    UpdateProductRequest.class
})
public abstract class RequestBase {

    protected Requester requester;

    /**
     * Gets the value of the requester property.
     * 
     * @return
     *     possible object is
     *     {@link Requester }
     *     
     */
    public Requester getRequester() {
        return requester;
    }

    /**
     * Sets the value of the requester property.
     * 
     * @param value
     *     allowed object is
     *     {@link Requester }
     *     
     */
    public void setRequester(Requester value) {
        this.requester = value;
    }

}
