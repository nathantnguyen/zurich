
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customer" type="{http://workstation.znawebservices.zurichna.com}ArrayOfCustomerSearchResults" minOccurs="0"/&gt;
 *         &lt;element name="submission" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionSearchResults" minOccurs="0"/&gt;
 *         &lt;element name="policy" type="{http://workstation.znawebservices.zurichna.com}ArrayOfPolicySearchResults" minOccurs="0"/&gt;
 *         &lt;element name="insured" type="{http://workstation.znawebservices.zurichna.com}ArrayOfInsuredSearchResults" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchResponse", propOrder = {
    "customer",
    "submission",
    "policy",
    "insured"
})
public class SearchResponse
    extends ResponseBase
{

    protected ArrayOfCustomerSearchResults customer;
    protected ArrayOfSubmissionSearchResults submission;
    protected ArrayOfPolicySearchResults policy;
    protected ArrayOfInsuredSearchResults insured;

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomerSearchResults }
     *     
     */
    public ArrayOfCustomerSearchResults getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomerSearchResults }
     *     
     */
    public void setCustomer(ArrayOfCustomerSearchResults value) {
        this.customer = value;
    }

    /**
     * Gets the value of the submission property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionSearchResults }
     *     
     */
    public ArrayOfSubmissionSearchResults getSubmission() {
        return submission;
    }

    /**
     * Sets the value of the submission property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionSearchResults }
     *     
     */
    public void setSubmission(ArrayOfSubmissionSearchResults value) {
        this.submission = value;
    }

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPolicySearchResults }
     *     
     */
    public ArrayOfPolicySearchResults getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPolicySearchResults }
     *     
     */
    public void setPolicy(ArrayOfPolicySearchResults value) {
        this.policy = value;
    }

    /**
     * Gets the value of the insured property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInsuredSearchResults }
     *     
     */
    public ArrayOfInsuredSearchResults getInsured() {
        return insured;
    }

    /**
     * Sets the value of the insured property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInsuredSearchResults }
     *     
     */
    public void setInsured(ArrayOfInsuredSearchResults value) {
        this.insured = value;
    }

}
