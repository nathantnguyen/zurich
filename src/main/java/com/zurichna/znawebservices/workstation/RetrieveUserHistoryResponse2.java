
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetrieveUserHistoryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveUserHistoryResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="underwritingAssts" type="{http://workstation.znawebservices.zurichna.com}ArrayOfUnderwritingAssts" minOccurs="0"/&gt;
 *         &lt;element name="underwriters" type="{http://workstation.znawebservices.zurichna.com}ArrayOfUnderwriters" minOccurs="0"/&gt;
 *         &lt;element name="prodDetails" type="{http://workstation.znawebservices.zurichna.com}ArrayOfProdDetails" minOccurs="0"/&gt;
 *         &lt;element name="orgs" type="{http://workstation.znawebservices.zurichna.com}ArrayOfOrgs" minOccurs="0"/&gt;
 *         &lt;element name="upcs" type="{http://workstation.znawebservices.zurichna.com}ArrayOfUnderwritingPrograms" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveUserHistoryResponse", propOrder = {
    "underwritingAssts",
    "underwriters",
    "prodDetails",
    "orgs",
    "upcs"
})
public class RetrieveUserHistoryResponse2
    extends ResponseBase
{

    protected ArrayOfUnderwritingAssts underwritingAssts;
    protected ArrayOfUnderwriters underwriters;
    protected ArrayOfProdDetails prodDetails;
    protected ArrayOfOrgs orgs;
    protected ArrayOfUnderwritingPrograms upcs;

    /**
     * Gets the value of the underwritingAssts property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUnderwritingAssts }
     *     
     */
    public ArrayOfUnderwritingAssts getUnderwritingAssts() {
        return underwritingAssts;
    }

    /**
     * Sets the value of the underwritingAssts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUnderwritingAssts }
     *     
     */
    public void setUnderwritingAssts(ArrayOfUnderwritingAssts value) {
        this.underwritingAssts = value;
    }

    /**
     * Gets the value of the underwriters property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUnderwriters }
     *     
     */
    public ArrayOfUnderwriters getUnderwriters() {
        return underwriters;
    }

    /**
     * Sets the value of the underwriters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUnderwriters }
     *     
     */
    public void setUnderwriters(ArrayOfUnderwriters value) {
        this.underwriters = value;
    }

    /**
     * Gets the value of the prodDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProdDetails }
     *     
     */
    public ArrayOfProdDetails getProdDetails() {
        return prodDetails;
    }

    /**
     * Sets the value of the prodDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProdDetails }
     *     
     */
    public void setProdDetails(ArrayOfProdDetails value) {
        this.prodDetails = value;
    }

    /**
     * Gets the value of the orgs property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOrgs }
     *     
     */
    public ArrayOfOrgs getOrgs() {
        return orgs;
    }

    /**
     * Sets the value of the orgs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOrgs }
     *     
     */
    public void setOrgs(ArrayOfOrgs value) {
        this.orgs = value;
    }

    /**
     * Gets the value of the upcs property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUnderwritingPrograms }
     *     
     */
    public ArrayOfUnderwritingPrograms getUpcs() {
        return upcs;
    }

    /**
     * Sets the value of the upcs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUnderwritingPrograms }
     *     
     */
    public void setUpcs(ArrayOfUnderwritingPrograms value) {
        this.upcs = value;
    }

}
