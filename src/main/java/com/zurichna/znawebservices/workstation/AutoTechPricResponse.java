
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AutoTechPricResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutoTechPricResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TOT_IND_PRC" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="LIAB_IND_PRC" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="PHY_DAM_IND_PRC" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="TECH_PRC_ERR_MSG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoTechPricResponse", propOrder = {
    "totindprc",
    "liabindprc",
    "phydamindprc",
    "techprcerrmsg"
})
public class AutoTechPricResponse {

    @XmlElement(name = "TOT_IND_PRC", required = true)
    protected BigDecimal totindprc;
    @XmlElement(name = "LIAB_IND_PRC", required = true)
    protected BigDecimal liabindprc;
    @XmlElement(name = "PHY_DAM_IND_PRC", required = true)
    protected BigDecimal phydamindprc;
    @XmlElement(name = "TECH_PRC_ERR_MSG")
    protected String techprcerrmsg;

    /**
     * Gets the value of the totindprc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTOTINDPRC() {
        return totindprc;
    }

    /**
     * Sets the value of the totindprc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTOTINDPRC(BigDecimal value) {
        this.totindprc = value;
    }

    /**
     * Gets the value of the liabindprc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLIABINDPRC() {
        return liabindprc;
    }

    /**
     * Sets the value of the liabindprc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLIABINDPRC(BigDecimal value) {
        this.liabindprc = value;
    }

    /**
     * Gets the value of the phydamindprc property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPHYDAMINDPRC() {
        return phydamindprc;
    }

    /**
     * Sets the value of the phydamindprc property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPHYDAMINDPRC(BigDecimal value) {
        this.phydamindprc = value;
    }

    /**
     * Gets the value of the techprcerrmsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTECHPRCERRMSG() {
        return techprcerrmsg;
    }

    /**
     * Sets the value of the techprcerrmsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTECHPRCERRMSG(String value) {
        this.techprcerrmsg = value;
    }

}
