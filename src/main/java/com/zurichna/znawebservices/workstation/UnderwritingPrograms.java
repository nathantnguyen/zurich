
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnderwritingPrograms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnderwritingPrograms"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UNDG_PGM_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnderwritingPrograms", propOrder = {
    "undgpgmcd",
    "undgpgmdesc"
})
public class UnderwritingPrograms {

    @XmlElement(name = "UNDG_PGM_CD")
    protected String undgpgmcd;
    @XmlElement(name = "UNDG_PGM_DESC")
    protected String undgpgmdesc;

    /**
     * Gets the value of the undgpgmcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMCD() {
        return undgpgmcd;
    }

    /**
     * Sets the value of the undgpgmcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMCD(String value) {
        this.undgpgmcd = value;
    }

    /**
     * Gets the value of the undgpgmdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMDESC() {
        return undgpgmdesc;
    }

    /**
     * Sets the value of the undgpgmdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMDESC(String value) {
        this.undgpgmdesc = value;
    }

}
