
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="objRequest" type="{http://workstation.znawebservices.zurichna.com}GetWCAHTargetPrcDtls" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "objRequest"
})
@XmlRootElement(name = "WCTargetPrice")
public class WCTargetPrice {

    protected GetWCAHTargetPrcDtls objRequest;

    /**
     * Gets the value of the objRequest property.
     * 
     * @return
     *     possible object is
     *     {@link GetWCAHTargetPrcDtls }
     *     
     */
    public GetWCAHTargetPrcDtls getObjRequest() {
        return objRequest;
    }

    /**
     * Sets the value of the objRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetWCAHTargetPrcDtls }
     *     
     */
    public void setObjRequest(GetWCAHTargetPrcDtls value) {
        this.objRequest = value;
    }

}
