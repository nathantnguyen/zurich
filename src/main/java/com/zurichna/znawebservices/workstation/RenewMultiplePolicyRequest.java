
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RenewMultiplePolicyRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RenewMultiplePolicyRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="policyAgreementId" type="{http://workstation.znawebservices.zurichna.com}ArrayOfAgreements" minOccurs="0"/&gt;
 *         &lt;element name="system" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nonRenewalReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nonRenewalIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="singleSMSNIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RenewMultiplePolicyRequest", propOrder = {
    "policyAgreementId",
    "system",
    "nonRenewalReasonCode",
    "nonRenewalIndicator",
    "singleSMSNIndicator",
    "changeState"
})
public class RenewMultiplePolicyRequest
    extends RequestBase
{

    protected ArrayOfAgreements policyAgreementId;
    protected String system;
    protected String nonRenewalReasonCode;
    protected boolean nonRenewalIndicator;
    protected boolean singleSMSNIndicator;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the policyAgreementId property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAgreements }
     *     
     */
    public ArrayOfAgreements getPolicyAgreementId() {
        return policyAgreementId;
    }

    /**
     * Sets the value of the policyAgreementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAgreements }
     *     
     */
    public void setPolicyAgreementId(ArrayOfAgreements value) {
        this.policyAgreementId = value;
    }

    /**
     * Gets the value of the system property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystem() {
        return system;
    }

    /**
     * Sets the value of the system property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystem(String value) {
        this.system = value;
    }

    /**
     * Gets the value of the nonRenewalReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonRenewalReasonCode() {
        return nonRenewalReasonCode;
    }

    /**
     * Sets the value of the nonRenewalReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonRenewalReasonCode(String value) {
        this.nonRenewalReasonCode = value;
    }

    /**
     * Gets the value of the nonRenewalIndicator property.
     * 
     */
    public boolean isNonRenewalIndicator() {
        return nonRenewalIndicator;
    }

    /**
     * Sets the value of the nonRenewalIndicator property.
     * 
     */
    public void setNonRenewalIndicator(boolean value) {
        this.nonRenewalIndicator = value;
    }

    /**
     * Gets the value of the singleSMSNIndicator property.
     * 
     */
    public boolean isSingleSMSNIndicator() {
        return singleSMSNIndicator;
    }

    /**
     * Sets the value of the singleSMSNIndicator property.
     * 
     */
    public void setSingleSMSNIndicator(boolean value) {
        this.singleSMSNIndicator = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
