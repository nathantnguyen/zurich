
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetPolicySymbolResult" type="{http://workstation.znawebservices.zurichna.com}GetPolicySymbolsResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPolicySymbolResult"
})
@XmlRootElement(name = "GetPolicySymbolResponse")
public class GetPolicySymbolResponse {

    @XmlElement(name = "GetPolicySymbolResult")
    protected GetPolicySymbolsResponse getPolicySymbolResult;

    /**
     * Gets the value of the getPolicySymbolResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetPolicySymbolsResponse }
     *     
     */
    public GetPolicySymbolsResponse getGetPolicySymbolResult() {
        return getPolicySymbolResult;
    }

    /**
     * Sets the value of the getPolicySymbolResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPolicySymbolsResponse }
     *     
     */
    public void setGetPolicySymbolResult(GetPolicySymbolsResponse value) {
        this.getPolicySymbolResult = value;
    }

}
