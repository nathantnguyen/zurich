
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmissionProductV2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionProductV2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PRDT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDT_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SUB_PRDT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SUB_PRDT_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INCL_EXCL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDG_PGM_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="INS_SPEC_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TMPL_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BUSN_TRAN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PARNT_CISPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BKD_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="QTE_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="BOUND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ACQN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CMPT_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="RQST_COVG_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RQST_COVG_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPO_LOC_CLS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="EST_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="POL_COVG_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RT_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EQPT_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RPT_FREQ_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="VALN_BAS_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RSK_TYP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRIR_CISPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRPS_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OPTN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_NM_COMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_ABBR_COMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_BU_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_BU_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORGL_PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_STS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_NOTES_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASST_UNDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ASST_UNDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASST_UNDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASST_UNDR_NOTES_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BUSN_DVL_LDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="BUSN_DVL_LDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BUSN_DVL_LDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BUSN_DVL_LDR_NOTES_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BND_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="BOOK_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ORG_PTY_ROL_CISPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_ROL_CISPEC_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PRDR_PTY_ROL_CISPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_PTY_ROL_CISPEC_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UNDR_PTY_ROL_CISPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDR_PTY_ROL_CISPEC_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RSP_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="BIND_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="SALES_PROF_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ISLOCATION" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CNDA_IBC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CNDA_BUSN_AREA_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ASSM_FM_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASSM_FM_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IBC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CLS_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SUB_CLS_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNDA_SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BUSN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BUSN_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DESC0" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASST_UNDR_EMAIL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CARR_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IN_OFC_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CMNT_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PGM_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="QLTY_SMSN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Z_SHR_PCT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CMPT_PRC_RNG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_CNTA_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_CNTA_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PROJ_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ICC_NBR" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="US_DOT_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNDA_BRK_TYP_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PACE_ELGB_IND" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="submissionProductStatusV2" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionProductStatusV2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionProductV2", propOrder = {
    "custminsspecid",
    "effdt",
    "expidt",
    "prdtnm",
    "prdtabbr",
    "subprdtnm",
    "subprdtabbr",
    "inclexclcd",
    "siccd",
    "sicnm",
    "undgpgmid",
    "undgpgmcd",
    "undgpgmnm",
    "smsnid",
    "insspectypid",
    "tmplinsspecid",
    "busntranid",
    "parntcispecid",
    "bkdpremamt",
    "qtepremamt",
    "boundpremamt",
    "newrenlcd",
    "acqncd",
    "cmptpremamt",
    "rqstcovgeffdt",
    "rqstcovgexpidt",
    "expolocclscd",
    "ndpremamt",
    "estpremamt",
    "polcovgind",
    "rttypid",
    "eqpttypid",
    "rptfreqtypid",
    "valnbastypid",
    "rsktypcd",
    "prircispecid",
    "rtind",
    "prpsid",
    "optnid",
    "orgptyid",
    "orgid",
    "orgnm",
    "orgnmcomb",
    "orgabbrcomb",
    "orgbunm",
    "orgbuabbr",
    "prdrptyid",
    "prdrnbr",
    "orglprdrnbr",
    "prdrnm",
    "prdrsts",
    "undrptyid",
    "undrfstnm",
    "undrlstnm",
    "undrnotesid",
    "asstundrptyid",
    "asstundrfstnm",
    "asstundrlstnm",
    "asstundrnotesid",
    "busndvlldrptyid",
    "busndvlldrfstnm",
    "busndvlldrlstnm",
    "busndvlldrnotesid",
    "bndind",
    "bookind",
    "enttistmp",
    "orgptyrolcispecid",
    "orgptyrolcispecenttistmp",
    "prdrptyrolcispecid",
    "prdrptyrolcispecenttistmp",
    "undrptyrolcispecid",
    "undrptyrolcispecenttistmp",
    "rspind",
    "bindind",
    "salesprofind",
    "islocation",
    "cndaibcid",
    "cndabusnareaid",
    "assmfmcd",
    "assmfmnm",
    "ibccd",
    "clsdesc",
    "subclsdesc",
    "cndasiccd",
    "busncd",
    "busnnm",
    "desc0",
    "undremailaddr",
    "asstundremailaddr",
    "carrcd",
    "inofcdt",
    "cmnttxt",
    "pgmtypid",
    "qltysmsn",
    "zshrpct",
    "cmptprcrng",
    "prdrfstnm",
    "prdrlstnm",
    "prdremail",
    "insdfstnm",
    "insdlstnm",
    "insdemail",
    "prdrcntaind",
    "insdcntaind",
    "projnm",
    "iccnbr",
    "usdotnbr",
    "cndabrktyptxt",
    "paceelgbind",
    "changeState",
    "submissionProductStatusV2"
})
public class SubmissionProductV2 {

    @XmlElement(name = "CUSTM_INS_SPEC_ID")
    protected int custminsspecid;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "PRDT_NM")
    protected String prdtnm;
    @XmlElement(name = "PRDT_ABBR")
    protected String prdtabbr;
    @XmlElement(name = "SUB_PRDT_NM")
    protected String subprdtnm;
    @XmlElement(name = "SUB_PRDT_ABBR")
    protected String subprdtabbr;
    @XmlElement(name = "INCL_EXCL_CD")
    protected String inclexclcd;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "SIC_NM")
    protected String sicnm;
    @XmlElement(name = "UNDG_PGM_ID")
    protected int undgpgmid;
    @XmlElement(name = "UNDG_PGM_CD")
    protected String undgpgmcd;
    @XmlElement(name = "UNDG_PGM_NM")
    protected String undgpgmnm;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "INS_SPEC_TYP_ID")
    protected int insspectypid;
    @XmlElement(name = "TMPL_INS_SPEC_ID")
    protected int tmplinsspecid;
    @XmlElement(name = "BUSN_TRAN_ID")
    protected int busntranid;
    @XmlElement(name = "PARNT_CISPEC_ID")
    protected int parntcispecid;
    @XmlElement(name = "BKD_PREM_AMT", required = true)
    protected BigDecimal bkdpremamt;
    @XmlElement(name = "QTE_PREM_AMT", required = true)
    protected BigDecimal qtepremamt;
    @XmlElement(name = "BOUND_PREM_AMT", required = true)
    protected BigDecimal boundpremamt;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "ACQN_CD")
    protected String acqncd;
    @XmlElement(name = "CMPT_PREM_AMT", required = true)
    protected BigDecimal cmptpremamt;
    @XmlElement(name = "RQST_COVG_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgeffdt;
    @XmlElement(name = "RQST_COVG_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgexpidt;
    @XmlElement(name = "EXPO_LOC_CLS_CD")
    protected String expolocclscd;
    @XmlElement(name = "ND_PREM_AMT", required = true)
    protected BigDecimal ndpremamt;
    @XmlElement(name = "EST_PREM_AMT", required = true)
    protected BigDecimal estpremamt;
    @XmlElement(name = "POL_COVG_IND")
    protected String polcovgind;
    @XmlElement(name = "RT_TYP_ID")
    protected int rttypid;
    @XmlElement(name = "EQPT_TYP_ID")
    protected int eqpttypid;
    @XmlElement(name = "RPT_FREQ_TYP_ID")
    protected int rptfreqtypid;
    @XmlElement(name = "VALN_BAS_TYP_ID")
    protected int valnbastypid;
    @XmlElement(name = "RSK_TYP_CD")
    protected String rsktypcd;
    @XmlElement(name = "PRIR_CISPEC_ID")
    protected int prircispecid;
    @XmlElement(name = "RT_IND")
    protected String rtind;
    @XmlElement(name = "PRPS_ID")
    protected int prpsid;
    @XmlElement(name = "OPTN_ID")
    protected int optnid;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_ID")
    protected int orgid;
    @XmlElement(name = "ORG_NM")
    protected String orgnm;
    @XmlElement(name = "ORG_NM_COMB")
    protected String orgnmcomb;
    @XmlElement(name = "ORG_ABBR_COMB")
    protected String orgabbrcomb;
    @XmlElement(name = "ORG_BU_NM")
    protected String orgbunm;
    @XmlElement(name = "ORG_BU_ABBR")
    protected String orgbuabbr;
    @XmlElement(name = "PRDR_PTY_ID")
    protected int prdrptyid;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "ORGL_PRDR_NBR")
    protected String orglprdrnbr;
    @XmlElement(name = "PRDR_NM")
    protected String prdrnm;
    @XmlElement(name = "PRDR_STS")
    protected String prdrsts;
    @XmlElement(name = "UNDR_PTY_ID")
    protected int undrptyid;
    @XmlElement(name = "UNDR_FST_NM")
    protected String undrfstnm;
    @XmlElement(name = "UNDR_LST_NM")
    protected String undrlstnm;
    @XmlElement(name = "UNDR_NOTES_ID")
    protected String undrnotesid;
    @XmlElement(name = "ASST_UNDR_PTY_ID")
    protected int asstundrptyid;
    @XmlElement(name = "ASST_UNDR_FST_NM")
    protected String asstundrfstnm;
    @XmlElement(name = "ASST_UNDR_LST_NM")
    protected String asstundrlstnm;
    @XmlElement(name = "ASST_UNDR_NOTES_ID")
    protected String asstundrnotesid;
    @XmlElement(name = "BUSN_DVL_LDR_PTY_ID")
    protected int busndvlldrptyid;
    @XmlElement(name = "BUSN_DVL_LDR_FST_NM")
    protected String busndvlldrfstnm;
    @XmlElement(name = "BUSN_DVL_LDR_LST_NM")
    protected String busndvlldrlstnm;
    @XmlElement(name = "BUSN_DVL_LDR_NOTES_ID")
    protected String busndvlldrnotesid;
    @XmlElement(name = "BND_IND")
    protected boolean bndind;
    @XmlElement(name = "BOOK_IND")
    protected boolean bookind;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "ORG_PTY_ROL_CISPEC_ID")
    protected int orgptyrolcispecid;
    @XmlElement(name = "ORG_PTY_ROL_CISPEC_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar orgptyrolcispecenttistmp;
    @XmlElement(name = "PRDR_PTY_ROL_CISPEC_ID")
    protected int prdrptyrolcispecid;
    @XmlElement(name = "PRDR_PTY_ROL_CISPEC_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar prdrptyrolcispecenttistmp;
    @XmlElement(name = "UNDR_PTY_ROL_CISPEC_ID")
    protected int undrptyrolcispecid;
    @XmlElement(name = "UNDR_PTY_ROL_CISPEC_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar undrptyrolcispecenttistmp;
    @XmlElement(name = "RSP_IND")
    protected boolean rspind;
    @XmlElement(name = "BIND_IND")
    protected boolean bindind;
    @XmlElement(name = "SALES_PROF_IND")
    protected String salesprofind;
    @XmlElement(name = "ISLOCATION")
    protected int islocation;
    @XmlElement(name = "CNDA_IBC_ID")
    protected int cndaibcid;
    @XmlElement(name = "CNDA_BUSN_AREA_ID")
    protected int cndabusnareaid;
    @XmlElement(name = "ASSM_FM_CD")
    protected String assmfmcd;
    @XmlElement(name = "ASSM_FM_NM")
    protected String assmfmnm;
    @XmlElement(name = "IBC_CD")
    protected String ibccd;
    @XmlElement(name = "CLS_DESC")
    protected String clsdesc;
    @XmlElement(name = "SUB_CLS_DESC")
    protected String subclsdesc;
    @XmlElement(name = "CNDA_SIC_CD")
    protected String cndasiccd;
    @XmlElement(name = "BUSN_CD")
    protected String busncd;
    @XmlElement(name = "BUSN_NM")
    protected String busnnm;
    @XmlElement(name = "DESC0")
    protected String desc0;
    @XmlElement(name = "UNDR_EMAIL_ADDR")
    protected String undremailaddr;
    @XmlElement(name = "ASST_UNDR_EMAIL_ADDR")
    protected String asstundremailaddr;
    @XmlElement(name = "CARR_CD")
    protected String carrcd;
    @XmlElement(name = "IN_OFC_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar inofcdt;
    @XmlElement(name = "CMNT_TXT")
    protected String cmnttxt;
    @XmlElement(name = "PGM_TYP_ID")
    protected int pgmtypid;
    @XmlElement(name = "QLTY_SMSN")
    protected String qltysmsn;
    @XmlElement(name = "Z_SHR_PCT", required = true)
    protected BigDecimal zshrpct;
    @XmlElement(name = "CMPT_PRC_RNG")
    protected String cmptprcrng;
    @XmlElement(name = "PRDR_FST_NM")
    protected String prdrfstnm;
    @XmlElement(name = "PRDR_LST_NM")
    protected String prdrlstnm;
    @XmlElement(name = "PRDR_EMAIL")
    protected String prdremail;
    @XmlElement(name = "INSD_FST_NM")
    protected String insdfstnm;
    @XmlElement(name = "INSD_LST_NM")
    protected String insdlstnm;
    @XmlElement(name = "INSD_EMAIL")
    protected String insdemail;
    @XmlElement(name = "PRDR_CNTA_IND")
    protected String prdrcntaind;
    @XmlElement(name = "INSD_CNTA_IND")
    protected String insdcntaind;
    @XmlElement(name = "PROJ_NM")
    protected String projnm;
    @XmlElement(name = "ICC_NBR")
    protected int iccnbr;
    @XmlElement(name = "US_DOT_NBR")
    protected String usdotnbr;
    @XmlElement(name = "CNDA_BRK_TYP_TXT")
    protected String cndabrktyptxt;
    @XmlElement(name = "PACE_ELGB_IND")
    protected int paceelgbind;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfSubmissionProductStatusV2 submissionProductStatusV2;

    /**
     * Gets the value of the custminsspecid property.
     * 
     */
    public int getCUSTMINSSPECID() {
        return custminsspecid;
    }

    /**
     * Sets the value of the custminsspecid property.
     * 
     */
    public void setCUSTMINSSPECID(int value) {
        this.custminsspecid = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the prdtnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTNM() {
        return prdtnm;
    }

    /**
     * Sets the value of the prdtnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTNM(String value) {
        this.prdtnm = value;
    }

    /**
     * Gets the value of the prdtabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTABBR() {
        return prdtabbr;
    }

    /**
     * Sets the value of the prdtabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTABBR(String value) {
        this.prdtabbr = value;
    }

    /**
     * Gets the value of the subprdtnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBPRDTNM() {
        return subprdtnm;
    }

    /**
     * Sets the value of the subprdtnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBPRDTNM(String value) {
        this.subprdtnm = value;
    }

    /**
     * Gets the value of the subprdtabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBPRDTABBR() {
        return subprdtabbr;
    }

    /**
     * Sets the value of the subprdtabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBPRDTABBR(String value) {
        this.subprdtabbr = value;
    }

    /**
     * Gets the value of the inclexclcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINCLEXCLCD() {
        return inclexclcd;
    }

    /**
     * Sets the value of the inclexclcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINCLEXCLCD(String value) {
        this.inclexclcd = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the sicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICNM() {
        return sicnm;
    }

    /**
     * Sets the value of the sicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICNM(String value) {
        this.sicnm = value;
    }

    /**
     * Gets the value of the undgpgmid property.
     * 
     */
    public int getUNDGPGMID() {
        return undgpgmid;
    }

    /**
     * Sets the value of the undgpgmid property.
     * 
     */
    public void setUNDGPGMID(int value) {
        this.undgpgmid = value;
    }

    /**
     * Gets the value of the undgpgmcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMCD() {
        return undgpgmcd;
    }

    /**
     * Sets the value of the undgpgmcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMCD(String value) {
        this.undgpgmcd = value;
    }

    /**
     * Gets the value of the undgpgmnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMNM() {
        return undgpgmnm;
    }

    /**
     * Sets the value of the undgpgmnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMNM(String value) {
        this.undgpgmnm = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the insspectypid property.
     * 
     */
    public int getINSSPECTYPID() {
        return insspectypid;
    }

    /**
     * Sets the value of the insspectypid property.
     * 
     */
    public void setINSSPECTYPID(int value) {
        this.insspectypid = value;
    }

    /**
     * Gets the value of the tmplinsspecid property.
     * 
     */
    public int getTMPLINSSPECID() {
        return tmplinsspecid;
    }

    /**
     * Sets the value of the tmplinsspecid property.
     * 
     */
    public void setTMPLINSSPECID(int value) {
        this.tmplinsspecid = value;
    }

    /**
     * Gets the value of the busntranid property.
     * 
     */
    public int getBUSNTRANID() {
        return busntranid;
    }

    /**
     * Sets the value of the busntranid property.
     * 
     */
    public void setBUSNTRANID(int value) {
        this.busntranid = value;
    }

    /**
     * Gets the value of the parntcispecid property.
     * 
     */
    public int getPARNTCISPECID() {
        return parntcispecid;
    }

    /**
     * Sets the value of the parntcispecid property.
     * 
     */
    public void setPARNTCISPECID(int value) {
        this.parntcispecid = value;
    }

    /**
     * Gets the value of the bkdpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBKDPREMAMT() {
        return bkdpremamt;
    }

    /**
     * Sets the value of the bkdpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBKDPREMAMT(BigDecimal value) {
        this.bkdpremamt = value;
    }

    /**
     * Gets the value of the qtepremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTEPREMAMT() {
        return qtepremamt;
    }

    /**
     * Sets the value of the qtepremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTEPREMAMT(BigDecimal value) {
        this.qtepremamt = value;
    }

    /**
     * Gets the value of the boundpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBOUNDPREMAMT() {
        return boundpremamt;
    }

    /**
     * Sets the value of the boundpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBOUNDPREMAMT(BigDecimal value) {
        this.boundpremamt = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the acqncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACQNCD() {
        return acqncd;
    }

    /**
     * Sets the value of the acqncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACQNCD(String value) {
        this.acqncd = value;
    }

    /**
     * Gets the value of the cmptpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCMPTPREMAMT() {
        return cmptpremamt;
    }

    /**
     * Sets the value of the cmptpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCMPTPREMAMT(BigDecimal value) {
        this.cmptpremamt = value;
    }

    /**
     * Gets the value of the rqstcovgeffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEFFDT() {
        return rqstcovgeffdt;
    }

    /**
     * Sets the value of the rqstcovgeffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEFFDT(XMLGregorianCalendar value) {
        this.rqstcovgeffdt = value;
    }

    /**
     * Gets the value of the rqstcovgexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEXPIDT() {
        return rqstcovgexpidt;
    }

    /**
     * Sets the value of the rqstcovgexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEXPIDT(XMLGregorianCalendar value) {
        this.rqstcovgexpidt = value;
    }

    /**
     * Gets the value of the expolocclscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPOLOCCLSCD() {
        return expolocclscd;
    }

    /**
     * Sets the value of the expolocclscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPOLOCCLSCD(String value) {
        this.expolocclscd = value;
    }

    /**
     * Gets the value of the ndpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNDPREMAMT() {
        return ndpremamt;
    }

    /**
     * Sets the value of the ndpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNDPREMAMT(BigDecimal value) {
        this.ndpremamt = value;
    }

    /**
     * Gets the value of the estpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getESTPREMAMT() {
        return estpremamt;
    }

    /**
     * Sets the value of the estpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setESTPREMAMT(BigDecimal value) {
        this.estpremamt = value;
    }

    /**
     * Gets the value of the polcovgind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLCOVGIND() {
        return polcovgind;
    }

    /**
     * Sets the value of the polcovgind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLCOVGIND(String value) {
        this.polcovgind = value;
    }

    /**
     * Gets the value of the rttypid property.
     * 
     */
    public int getRTTYPID() {
        return rttypid;
    }

    /**
     * Sets the value of the rttypid property.
     * 
     */
    public void setRTTYPID(int value) {
        this.rttypid = value;
    }

    /**
     * Gets the value of the eqpttypid property.
     * 
     */
    public int getEQPTTYPID() {
        return eqpttypid;
    }

    /**
     * Sets the value of the eqpttypid property.
     * 
     */
    public void setEQPTTYPID(int value) {
        this.eqpttypid = value;
    }

    /**
     * Gets the value of the rptfreqtypid property.
     * 
     */
    public int getRPTFREQTYPID() {
        return rptfreqtypid;
    }

    /**
     * Sets the value of the rptfreqtypid property.
     * 
     */
    public void setRPTFREQTYPID(int value) {
        this.rptfreqtypid = value;
    }

    /**
     * Gets the value of the valnbastypid property.
     * 
     */
    public int getVALNBASTYPID() {
        return valnbastypid;
    }

    /**
     * Sets the value of the valnbastypid property.
     * 
     */
    public void setVALNBASTYPID(int value) {
        this.valnbastypid = value;
    }

    /**
     * Gets the value of the rsktypcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRSKTYPCD() {
        return rsktypcd;
    }

    /**
     * Sets the value of the rsktypcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRSKTYPCD(String value) {
        this.rsktypcd = value;
    }

    /**
     * Gets the value of the prircispecid property.
     * 
     */
    public int getPRIRCISPECID() {
        return prircispecid;
    }

    /**
     * Sets the value of the prircispecid property.
     * 
     */
    public void setPRIRCISPECID(int value) {
        this.prircispecid = value;
    }

    /**
     * Gets the value of the rtind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRTIND() {
        return rtind;
    }

    /**
     * Sets the value of the rtind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRTIND(String value) {
        this.rtind = value;
    }

    /**
     * Gets the value of the prpsid property.
     * 
     */
    public int getPRPSID() {
        return prpsid;
    }

    /**
     * Sets the value of the prpsid property.
     * 
     */
    public void setPRPSID(int value) {
        this.prpsid = value;
    }

    /**
     * Gets the value of the optnid property.
     * 
     */
    public int getOPTNID() {
        return optnid;
    }

    /**
     * Sets the value of the optnid property.
     * 
     */
    public void setOPTNID(int value) {
        this.optnid = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgid property.
     * 
     */
    public int getORGID() {
        return orgid;
    }

    /**
     * Sets the value of the orgid property.
     * 
     */
    public void setORGID(int value) {
        this.orgid = value;
    }

    /**
     * Gets the value of the orgnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGNM() {
        return orgnm;
    }

    /**
     * Sets the value of the orgnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGNM(String value) {
        this.orgnm = value;
    }

    /**
     * Gets the value of the orgnmcomb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGNMCOMB() {
        return orgnmcomb;
    }

    /**
     * Sets the value of the orgnmcomb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGNMCOMB(String value) {
        this.orgnmcomb = value;
    }

    /**
     * Gets the value of the orgabbrcomb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGABBRCOMB() {
        return orgabbrcomb;
    }

    /**
     * Sets the value of the orgabbrcomb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGABBRCOMB(String value) {
        this.orgabbrcomb = value;
    }

    /**
     * Gets the value of the orgbunm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGBUNM() {
        return orgbunm;
    }

    /**
     * Sets the value of the orgbunm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGBUNM(String value) {
        this.orgbunm = value;
    }

    /**
     * Gets the value of the orgbuabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGBUABBR() {
        return orgbuabbr;
    }

    /**
     * Sets the value of the orgbuabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGBUABBR(String value) {
        this.orgbuabbr = value;
    }

    /**
     * Gets the value of the prdrptyid property.
     * 
     */
    public int getPRDRPTYID() {
        return prdrptyid;
    }

    /**
     * Sets the value of the prdrptyid property.
     * 
     */
    public void setPRDRPTYID(int value) {
        this.prdrptyid = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the orglprdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGLPRDRNBR() {
        return orglprdrnbr;
    }

    /**
     * Sets the value of the orglprdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGLPRDRNBR(String value) {
        this.orglprdrnbr = value;
    }

    /**
     * Gets the value of the prdrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNM() {
        return prdrnm;
    }

    /**
     * Sets the value of the prdrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNM(String value) {
        this.prdrnm = value;
    }

    /**
     * Gets the value of the prdrsts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTS() {
        return prdrsts;
    }

    /**
     * Sets the value of the prdrsts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTS(String value) {
        this.prdrsts = value;
    }

    /**
     * Gets the value of the undrptyid property.
     * 
     */
    public int getUNDRPTYID() {
        return undrptyid;
    }

    /**
     * Sets the value of the undrptyid property.
     * 
     */
    public void setUNDRPTYID(int value) {
        this.undrptyid = value;
    }

    /**
     * Gets the value of the undrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRFSTNM() {
        return undrfstnm;
    }

    /**
     * Sets the value of the undrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRFSTNM(String value) {
        this.undrfstnm = value;
    }

    /**
     * Gets the value of the undrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRLSTNM() {
        return undrlstnm;
    }

    /**
     * Sets the value of the undrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRLSTNM(String value) {
        this.undrlstnm = value;
    }

    /**
     * Gets the value of the undrnotesid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRNOTESID() {
        return undrnotesid;
    }

    /**
     * Sets the value of the undrnotesid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRNOTESID(String value) {
        this.undrnotesid = value;
    }

    /**
     * Gets the value of the asstundrptyid property.
     * 
     */
    public int getASSTUNDRPTYID() {
        return asstundrptyid;
    }

    /**
     * Sets the value of the asstundrptyid property.
     * 
     */
    public void setASSTUNDRPTYID(int value) {
        this.asstundrptyid = value;
    }

    /**
     * Gets the value of the asstundrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSTUNDRFSTNM() {
        return asstundrfstnm;
    }

    /**
     * Sets the value of the asstundrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSTUNDRFSTNM(String value) {
        this.asstundrfstnm = value;
    }

    /**
     * Gets the value of the asstundrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSTUNDRLSTNM() {
        return asstundrlstnm;
    }

    /**
     * Sets the value of the asstundrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSTUNDRLSTNM(String value) {
        this.asstundrlstnm = value;
    }

    /**
     * Gets the value of the asstundrnotesid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSTUNDRNOTESID() {
        return asstundrnotesid;
    }

    /**
     * Sets the value of the asstundrnotesid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSTUNDRNOTESID(String value) {
        this.asstundrnotesid = value;
    }

    /**
     * Gets the value of the busndvlldrptyid property.
     * 
     */
    public int getBUSNDVLLDRPTYID() {
        return busndvlldrptyid;
    }

    /**
     * Sets the value of the busndvlldrptyid property.
     * 
     */
    public void setBUSNDVLLDRPTYID(int value) {
        this.busndvlldrptyid = value;
    }

    /**
     * Gets the value of the busndvlldrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNDVLLDRFSTNM() {
        return busndvlldrfstnm;
    }

    /**
     * Sets the value of the busndvlldrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNDVLLDRFSTNM(String value) {
        this.busndvlldrfstnm = value;
    }

    /**
     * Gets the value of the busndvlldrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNDVLLDRLSTNM() {
        return busndvlldrlstnm;
    }

    /**
     * Sets the value of the busndvlldrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNDVLLDRLSTNM(String value) {
        this.busndvlldrlstnm = value;
    }

    /**
     * Gets the value of the busndvlldrnotesid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNDVLLDRNOTESID() {
        return busndvlldrnotesid;
    }

    /**
     * Sets the value of the busndvlldrnotesid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNDVLLDRNOTESID(String value) {
        this.busndvlldrnotesid = value;
    }

    /**
     * Gets the value of the bndind property.
     * 
     */
    public boolean isBNDIND() {
        return bndind;
    }

    /**
     * Sets the value of the bndind property.
     * 
     */
    public void setBNDIND(boolean value) {
        this.bndind = value;
    }

    /**
     * Gets the value of the bookind property.
     * 
     */
    public boolean isBOOKIND() {
        return bookind;
    }

    /**
     * Sets the value of the bookind property.
     * 
     */
    public void setBOOKIND(boolean value) {
        this.bookind = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the orgptyrolcispecid property.
     * 
     */
    public int getORGPTYROLCISPECID() {
        return orgptyrolcispecid;
    }

    /**
     * Sets the value of the orgptyrolcispecid property.
     * 
     */
    public void setORGPTYROLCISPECID(int value) {
        this.orgptyrolcispecid = value;
    }

    /**
     * Gets the value of the orgptyrolcispecenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getORGPTYROLCISPECENTTISTMP() {
        return orgptyrolcispecenttistmp;
    }

    /**
     * Sets the value of the orgptyrolcispecenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setORGPTYROLCISPECENTTISTMP(XMLGregorianCalendar value) {
        this.orgptyrolcispecenttistmp = value;
    }

    /**
     * Gets the value of the prdrptyrolcispecid property.
     * 
     */
    public int getPRDRPTYROLCISPECID() {
        return prdrptyrolcispecid;
    }

    /**
     * Sets the value of the prdrptyrolcispecid property.
     * 
     */
    public void setPRDRPTYROLCISPECID(int value) {
        this.prdrptyrolcispecid = value;
    }

    /**
     * Gets the value of the prdrptyrolcispecenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPRDRPTYROLCISPECENTTISTMP() {
        return prdrptyrolcispecenttistmp;
    }

    /**
     * Sets the value of the prdrptyrolcispecenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPRDRPTYROLCISPECENTTISTMP(XMLGregorianCalendar value) {
        this.prdrptyrolcispecenttistmp = value;
    }

    /**
     * Gets the value of the undrptyrolcispecid property.
     * 
     */
    public int getUNDRPTYROLCISPECID() {
        return undrptyrolcispecid;
    }

    /**
     * Sets the value of the undrptyrolcispecid property.
     * 
     */
    public void setUNDRPTYROLCISPECID(int value) {
        this.undrptyrolcispecid = value;
    }

    /**
     * Gets the value of the undrptyrolcispecenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUNDRPTYROLCISPECENTTISTMP() {
        return undrptyrolcispecenttistmp;
    }

    /**
     * Sets the value of the undrptyrolcispecenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUNDRPTYROLCISPECENTTISTMP(XMLGregorianCalendar value) {
        this.undrptyrolcispecenttistmp = value;
    }

    /**
     * Gets the value of the rspind property.
     * 
     */
    public boolean isRSPIND() {
        return rspind;
    }

    /**
     * Sets the value of the rspind property.
     * 
     */
    public void setRSPIND(boolean value) {
        this.rspind = value;
    }

    /**
     * Gets the value of the bindind property.
     * 
     */
    public boolean isBINDIND() {
        return bindind;
    }

    /**
     * Sets the value of the bindind property.
     * 
     */
    public void setBINDIND(boolean value) {
        this.bindind = value;
    }

    /**
     * Gets the value of the salesprofind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSALESPROFIND() {
        return salesprofind;
    }

    /**
     * Sets the value of the salesprofind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSALESPROFIND(String value) {
        this.salesprofind = value;
    }

    /**
     * Gets the value of the islocation property.
     * 
     */
    public int getISLOCATION() {
        return islocation;
    }

    /**
     * Sets the value of the islocation property.
     * 
     */
    public void setISLOCATION(int value) {
        this.islocation = value;
    }

    /**
     * Gets the value of the cndaibcid property.
     * 
     */
    public int getCNDAIBCID() {
        return cndaibcid;
    }

    /**
     * Sets the value of the cndaibcid property.
     * 
     */
    public void setCNDAIBCID(int value) {
        this.cndaibcid = value;
    }

    /**
     * Gets the value of the cndabusnareaid property.
     * 
     */
    public int getCNDABUSNAREAID() {
        return cndabusnareaid;
    }

    /**
     * Sets the value of the cndabusnareaid property.
     * 
     */
    public void setCNDABUSNAREAID(int value) {
        this.cndabusnareaid = value;
    }

    /**
     * Gets the value of the assmfmcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSMFMCD() {
        return assmfmcd;
    }

    /**
     * Sets the value of the assmfmcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSMFMCD(String value) {
        this.assmfmcd = value;
    }

    /**
     * Gets the value of the assmfmnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSMFMNM() {
        return assmfmnm;
    }

    /**
     * Sets the value of the assmfmnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSMFMNM(String value) {
        this.assmfmnm = value;
    }

    /**
     * Gets the value of the ibccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBCCD() {
        return ibccd;
    }

    /**
     * Sets the value of the ibccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBCCD(String value) {
        this.ibccd = value;
    }

    /**
     * Gets the value of the clsdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLSDESC() {
        return clsdesc;
    }

    /**
     * Sets the value of the clsdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLSDESC(String value) {
        this.clsdesc = value;
    }

    /**
     * Gets the value of the subclsdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBCLSDESC() {
        return subclsdesc;
    }

    /**
     * Sets the value of the subclsdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBCLSDESC(String value) {
        this.subclsdesc = value;
    }

    /**
     * Gets the value of the cndasiccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNDASICCD() {
        return cndasiccd;
    }

    /**
     * Sets the value of the cndasiccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNDASICCD(String value) {
        this.cndasiccd = value;
    }

    /**
     * Gets the value of the busncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNCD() {
        return busncd;
    }

    /**
     * Sets the value of the busncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNCD(String value) {
        this.busncd = value;
    }

    /**
     * Gets the value of the busnnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNNM() {
        return busnnm;
    }

    /**
     * Sets the value of the busnnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNNM(String value) {
        this.busnnm = value;
    }

    /**
     * Gets the value of the desc0 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESC0() {
        return desc0;
    }

    /**
     * Sets the value of the desc0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESC0(String value) {
        this.desc0 = value;
    }

    /**
     * Gets the value of the undremailaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDREMAILADDR() {
        return undremailaddr;
    }

    /**
     * Sets the value of the undremailaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDREMAILADDR(String value) {
        this.undremailaddr = value;
    }

    /**
     * Gets the value of the asstundremailaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSTUNDREMAILADDR() {
        return asstundremailaddr;
    }

    /**
     * Sets the value of the asstundremailaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSTUNDREMAILADDR(String value) {
        this.asstundremailaddr = value;
    }

    /**
     * Gets the value of the carrcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCARRCD() {
        return carrcd;
    }

    /**
     * Sets the value of the carrcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCARRCD(String value) {
        this.carrcd = value;
    }

    /**
     * Gets the value of the inofcdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getINOFCDT() {
        return inofcdt;
    }

    /**
     * Sets the value of the inofcdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setINOFCDT(XMLGregorianCalendar value) {
        this.inofcdt = value;
    }

    /**
     * Gets the value of the cmnttxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMNTTXT() {
        return cmnttxt;
    }

    /**
     * Sets the value of the cmnttxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMNTTXT(String value) {
        this.cmnttxt = value;
    }

    /**
     * Gets the value of the pgmtypid property.
     * 
     */
    public int getPGMTYPID() {
        return pgmtypid;
    }

    /**
     * Sets the value of the pgmtypid property.
     * 
     */
    public void setPGMTYPID(int value) {
        this.pgmtypid = value;
    }

    /**
     * Gets the value of the qltysmsn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQLTYSMSN() {
        return qltysmsn;
    }

    /**
     * Sets the value of the qltysmsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQLTYSMSN(String value) {
        this.qltysmsn = value;
    }

    /**
     * Gets the value of the zshrpct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getZSHRPCT() {
        return zshrpct;
    }

    /**
     * Sets the value of the zshrpct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setZSHRPCT(BigDecimal value) {
        this.zshrpct = value;
    }

    /**
     * Gets the value of the cmptprcrng property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMPTPRCRNG() {
        return cmptprcrng;
    }

    /**
     * Sets the value of the cmptprcrng property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMPTPRCRNG(String value) {
        this.cmptprcrng = value;
    }

    /**
     * Gets the value of the prdrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRFSTNM() {
        return prdrfstnm;
    }

    /**
     * Sets the value of the prdrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRFSTNM(String value) {
        this.prdrfstnm = value;
    }

    /**
     * Gets the value of the prdrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRLSTNM() {
        return prdrlstnm;
    }

    /**
     * Sets the value of the prdrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRLSTNM(String value) {
        this.prdrlstnm = value;
    }

    /**
     * Gets the value of the prdremail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDREMAIL() {
        return prdremail;
    }

    /**
     * Sets the value of the prdremail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDREMAIL(String value) {
        this.prdremail = value;
    }

    /**
     * Gets the value of the insdfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDFSTNM() {
        return insdfstnm;
    }

    /**
     * Sets the value of the insdfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDFSTNM(String value) {
        this.insdfstnm = value;
    }

    /**
     * Gets the value of the insdlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDLSTNM() {
        return insdlstnm;
    }

    /**
     * Sets the value of the insdlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDLSTNM(String value) {
        this.insdlstnm = value;
    }

    /**
     * Gets the value of the insdemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDEMAIL() {
        return insdemail;
    }

    /**
     * Sets the value of the insdemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDEMAIL(String value) {
        this.insdemail = value;
    }

    /**
     * Gets the value of the prdrcntaind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRCNTAIND() {
        return prdrcntaind;
    }

    /**
     * Sets the value of the prdrcntaind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRCNTAIND(String value) {
        this.prdrcntaind = value;
    }

    /**
     * Gets the value of the insdcntaind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDCNTAIND() {
        return insdcntaind;
    }

    /**
     * Sets the value of the insdcntaind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDCNTAIND(String value) {
        this.insdcntaind = value;
    }

    /**
     * Gets the value of the projnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROJNM() {
        return projnm;
    }

    /**
     * Sets the value of the projnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROJNM(String value) {
        this.projnm = value;
    }

    /**
     * Gets the value of the iccnbr property.
     * 
     */
    public int getICCNBR() {
        return iccnbr;
    }

    /**
     * Sets the value of the iccnbr property.
     * 
     */
    public void setICCNBR(int value) {
        this.iccnbr = value;
    }

    /**
     * Gets the value of the usdotnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSDOTNBR() {
        return usdotnbr;
    }

    /**
     * Sets the value of the usdotnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSDOTNBR(String value) {
        this.usdotnbr = value;
    }

    /**
     * Gets the value of the cndabrktyptxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNDABRKTYPTXT() {
        return cndabrktyptxt;
    }

    /**
     * Sets the value of the cndabrktyptxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNDABRKTYPTXT(String value) {
        this.cndabrktyptxt = value;
    }

    /**
     * Gets the value of the paceelgbind property.
     * 
     */
    public int getPACEELGBIND() {
        return paceelgbind;
    }

    /**
     * Sets the value of the paceelgbind property.
     * 
     */
    public void setPACEELGBIND(int value) {
        this.paceelgbind = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the submissionProductStatusV2 property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionProductStatusV2 }
     *     
     */
    public ArrayOfSubmissionProductStatusV2 getSubmissionProductStatusV2() {
        return submissionProductStatusV2;
    }

    /**
     * Sets the value of the submissionProductStatusV2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionProductStatusV2 }
     *     
     */
    public void setSubmissionProductStatusV2(ArrayOfSubmissionProductStatusV2 value) {
        this.submissionProductStatusV2 = value;
    }

}
