
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetSMSNDetailsForGivenProducerResult" type="{http://workstation.znawebservices.zurichna.com}SMSNDetailsForGivenProducerResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSMSNDetailsForGivenProducerResult"
})
@XmlRootElement(name = "GetSMSNDetailsForGivenProducerResponse")
public class GetSMSNDetailsForGivenProducerResponse {

    @XmlElement(name = "GetSMSNDetailsForGivenProducerResult")
    protected SMSNDetailsForGivenProducerResponse getSMSNDetailsForGivenProducerResult;

    /**
     * Gets the value of the getSMSNDetailsForGivenProducerResult property.
     * 
     * @return
     *     possible object is
     *     {@link SMSNDetailsForGivenProducerResponse }
     *     
     */
    public SMSNDetailsForGivenProducerResponse getGetSMSNDetailsForGivenProducerResult() {
        return getSMSNDetailsForGivenProducerResult;
    }

    /**
     * Sets the value of the getSMSNDetailsForGivenProducerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SMSNDetailsForGivenProducerResponse }
     *     
     */
    public void setGetSMSNDetailsForGivenProducerResult(SMSNDetailsForGivenProducerResponse value) {
        this.getSMSNDetailsForGivenProducerResult = value;
    }

}
