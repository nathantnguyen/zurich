
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PremiumAmount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PremiumAmount"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="theAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="theUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PremiumAmount", propOrder = {
    "theAmount",
    "theUnit"
})
public class PremiumAmount {

    @XmlElement(required = true)
    protected BigDecimal theAmount;
    @XmlElement(defaultValue = "USD")
    protected String theUnit;

    /**
     * Gets the value of the theAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTheAmount() {
        return theAmount;
    }

    /**
     * Sets the value of the theAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTheAmount(BigDecimal value) {
        this.theAmount = value;
    }

    /**
     * Gets the value of the theUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheUnit() {
        return theUnit;
    }

    /**
     * Sets the value of the theUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheUnit(String value) {
        this.theUnit = value;
    }

}
