
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RecordSubmissionResult" type="{http://workstation.znawebservices.zurichna.com}SubmissionV2Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recordSubmissionResult"
})
@XmlRootElement(name = "RecordSubmissionResponse")
public class RecordSubmissionResponse {

    @XmlElement(name = "RecordSubmissionResult")
    protected SubmissionV2Response recordSubmissionResult;

    /**
     * Gets the value of the recordSubmissionResult property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionV2Response }
     *     
     */
    public SubmissionV2Response getRecordSubmissionResult() {
        return recordSubmissionResult;
    }

    /**
     * Sets the value of the recordSubmissionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionV2Response }
     *     
     */
    public void setRecordSubmissionResult(SubmissionV2Response value) {
        this.recordSubmissionResult = value;
    }

}
