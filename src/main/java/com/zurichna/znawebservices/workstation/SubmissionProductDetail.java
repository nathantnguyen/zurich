
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmissionProductDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionProductDetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PRDT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_BU_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_BU_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STS_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDERWRITER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RQST_COVG_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="QTE_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="BOUND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionProductDetail", propOrder = {
    "prdtnm",
    "orgbunm",
    "orgbuabbr",
    "newrenlcd",
    "stsnm",
    "underwriter",
    "undrfstnm",
    "undrlstnm",
    "rqstcovgeffdt",
    "qtepremamt",
    "boundpremamt"
})
public class SubmissionProductDetail {

    @XmlElement(name = "PRDT_NM")
    protected String prdtnm;
    @XmlElement(name = "ORG_BU_NM")
    protected String orgbunm;
    @XmlElement(name = "ORG_BU_ABBR")
    protected String orgbuabbr;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "STS_NM")
    protected String stsnm;
    @XmlElement(name = "UNDERWRITER")
    protected String underwriter;
    @XmlElement(name = "UNDR_FST_NM")
    protected String undrfstnm;
    @XmlElement(name = "UNDR_LST_NM")
    protected String undrlstnm;
    @XmlElement(name = "RQST_COVG_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgeffdt;
    @XmlElement(name = "QTE_PREM_AMT", required = true)
    protected BigDecimal qtepremamt;
    @XmlElement(name = "BOUND_PREM_AMT", required = true)
    protected BigDecimal boundpremamt;

    /**
     * Gets the value of the prdtnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTNM() {
        return prdtnm;
    }

    /**
     * Sets the value of the prdtnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTNM(String value) {
        this.prdtnm = value;
    }

    /**
     * Gets the value of the orgbunm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGBUNM() {
        return orgbunm;
    }

    /**
     * Sets the value of the orgbunm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGBUNM(String value) {
        this.orgbunm = value;
    }

    /**
     * Gets the value of the orgbuabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGBUABBR() {
        return orgbuabbr;
    }

    /**
     * Sets the value of the orgbuabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGBUABBR(String value) {
        this.orgbuabbr = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the stsnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTSNM() {
        return stsnm;
    }

    /**
     * Sets the value of the stsnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTSNM(String value) {
        this.stsnm = value;
    }

    /**
     * Gets the value of the underwriter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDERWRITER() {
        return underwriter;
    }

    /**
     * Sets the value of the underwriter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDERWRITER(String value) {
        this.underwriter = value;
    }

    /**
     * Gets the value of the undrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRFSTNM() {
        return undrfstnm;
    }

    /**
     * Sets the value of the undrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRFSTNM(String value) {
        this.undrfstnm = value;
    }

    /**
     * Gets the value of the undrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRLSTNM() {
        return undrlstnm;
    }

    /**
     * Sets the value of the undrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRLSTNM(String value) {
        this.undrlstnm = value;
    }

    /**
     * Gets the value of the rqstcovgeffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEFFDT() {
        return rqstcovgeffdt;
    }

    /**
     * Sets the value of the rqstcovgeffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEFFDT(XMLGregorianCalendar value) {
        this.rqstcovgeffdt = value;
    }

    /**
     * Gets the value of the qtepremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTEPREMAMT() {
        return qtepremamt;
    }

    /**
     * Sets the value of the qtepremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTEPREMAMT(BigDecimal value) {
        this.qtepremamt = value;
    }

    /**
     * Gets the value of the boundpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBOUNDPREMAMT() {
        return boundpremamt;
    }

    /**
     * Sets the value of the boundpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBOUNDPREMAMT(BigDecimal value) {
        this.boundpremamt = value;
    }

}
