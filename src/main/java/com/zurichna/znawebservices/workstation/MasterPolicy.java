
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MasterPolicy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MasterPolicy"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="INSRD_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="INSRD_ROL_TYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MSTR_POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MSTR_POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MSTR_MODU_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MSTR_AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MasterPolicy", propOrder = {
    "insrdptyid",
    "insrdroltyp",
    "mstrpolnbr",
    "mstrpolsym",
    "mstrmodunbr",
    "mstragmtid",
    "changeState"
})
public class MasterPolicy {

    @XmlElement(name = "INSRD_PTY_ID")
    protected int insrdptyid;
    @XmlElement(name = "INSRD_ROL_TYP")
    protected String insrdroltyp;
    @XmlElement(name = "MSTR_POL_NBR")
    protected String mstrpolnbr;
    @XmlElement(name = "MSTR_POL_SYM")
    protected String mstrpolsym;
    @XmlElement(name = "MSTR_MODU_NBR")
    protected String mstrmodunbr;
    @XmlElement(name = "MSTR_AGMT_ID")
    protected int mstragmtid;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the insrdptyid property.
     * 
     */
    public int getINSRDPTYID() {
        return insrdptyid;
    }

    /**
     * Sets the value of the insrdptyid property.
     * 
     */
    public void setINSRDPTYID(int value) {
        this.insrdptyid = value;
    }

    /**
     * Gets the value of the insrdroltyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSRDROLTYP() {
        return insrdroltyp;
    }

    /**
     * Sets the value of the insrdroltyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSRDROLTYP(String value) {
        this.insrdroltyp = value;
    }

    /**
     * Gets the value of the mstrpolnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSTRPOLNBR() {
        return mstrpolnbr;
    }

    /**
     * Sets the value of the mstrpolnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSTRPOLNBR(String value) {
        this.mstrpolnbr = value;
    }

    /**
     * Gets the value of the mstrpolsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSTRPOLSYM() {
        return mstrpolsym;
    }

    /**
     * Sets the value of the mstrpolsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSTRPOLSYM(String value) {
        this.mstrpolsym = value;
    }

    /**
     * Gets the value of the mstrmodunbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSTRMODUNBR() {
        return mstrmodunbr;
    }

    /**
     * Sets the value of the mstrmodunbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSTRMODUNBR(String value) {
        this.mstrmodunbr = value;
    }

    /**
     * Gets the value of the mstragmtid property.
     * 
     */
    public int getMSTRAGMTID() {
        return mstragmtid;
    }

    /**
     * Sets the value of the mstragmtid property.
     * 
     */
    public void setMSTRAGMTID(int value) {
        this.mstragmtid = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
