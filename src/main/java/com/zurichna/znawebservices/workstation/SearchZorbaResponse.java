
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SearchZorbaResult" type="{http://workstation.znawebservices.zurichna.com}SearchResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchZorbaResult"
})
@XmlRootElement(name = "SearchZorbaResponse")
public class SearchZorbaResponse {

    @XmlElement(name = "SearchZorbaResult")
    protected SearchResponse searchZorbaResult;

    /**
     * Gets the value of the searchZorbaResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchResponse }
     *     
     */
    public SearchResponse getSearchZorbaResult() {
        return searchZorbaResult;
    }

    /**
     * Sets the value of the searchZorbaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchResponse }
     *     
     */
    public void setSearchZorbaResult(SearchResponse value) {
        this.searchZorbaResult = value;
    }

}
