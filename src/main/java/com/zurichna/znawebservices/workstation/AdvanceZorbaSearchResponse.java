
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdvanceZorbaSearchResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdvanceZorbaSearchResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="advancezorba" type="{http://workstation.znawebservices.zurichna.com}ArrayOfAdvanceZorbaSearch" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdvanceZorbaSearchResponse", propOrder = {
    "advancezorba"
})
public class AdvanceZorbaSearchResponse
    extends ResponseBase
{

    protected ArrayOfAdvanceZorbaSearch advancezorba;

    /**
     * Gets the value of the advancezorba property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAdvanceZorbaSearch }
     *     
     */
    public ArrayOfAdvanceZorbaSearch getAdvancezorba() {
        return advancezorba;
    }

    /**
     * Sets the value of the advancezorba property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAdvanceZorbaSearch }
     *     
     */
    public void setAdvancezorba(ArrayOfAdvanceZorbaSearch value) {
        this.advancezorba = value;
    }

}
