
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmissionBind complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionBind"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ANNL_RPT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BKD_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="BOUND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="BRCHR_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CAPTV_RQST_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CMNT_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LOS_HIST_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OT_AGMT_RQST_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRIO_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QTE_DUE_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="QTE_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="RECD_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SPL_AGMT_RQST_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SYS_SRC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUST_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PERSONNEL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="submissionProduct" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionProducts" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionBind", propOrder = {
    "smsnid",
    "effdt",
    "expidt",
    "enttistmp",
    "nm",
    "annlrptind",
    "bkdpremamt",
    "boundpremamt",
    "brchrind",
    "captvrqstind",
    "cmnttxt",
    "loshistind",
    "newrenlcd",
    "otagmtrqstind",
    "priocd",
    "qteduedt",
    "qtepremamt",
    "recddt",
    "splagmtrqstind",
    "syssrccd",
    "orgptyid",
    "orgid",
    "custid",
    "personnelid",
    "changeState",
    "submissionProduct"
})
public class SubmissionBind {

    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "ANNL_RPT_IND")
    protected String annlrptind;
    @XmlElement(name = "BKD_PREM_AMT", required = true)
    protected BigDecimal bkdpremamt;
    @XmlElement(name = "BOUND_PREM_AMT", required = true)
    protected BigDecimal boundpremamt;
    @XmlElement(name = "BRCHR_IND")
    protected String brchrind;
    @XmlElement(name = "CAPTV_RQST_IND")
    protected String captvrqstind;
    @XmlElement(name = "CMNT_TXT")
    protected String cmnttxt;
    @XmlElement(name = "LOS_HIST_IND")
    protected String loshistind;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "OT_AGMT_RQST_IND")
    protected String otagmtrqstind;
    @XmlElement(name = "PRIO_CD")
    protected String priocd;
    @XmlElement(name = "QTE_DUE_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar qteduedt;
    @XmlElement(name = "QTE_PREM_AMT", required = true)
    protected BigDecimal qtepremamt;
    @XmlElement(name = "RECD_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recddt;
    @XmlElement(name = "SPL_AGMT_RQST_IND")
    protected String splagmtrqstind;
    @XmlElement(name = "SYS_SRC_CD")
    protected String syssrccd;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_ID")
    protected int orgid;
    @XmlElement(name = "CUST_ID")
    protected int custid;
    @XmlElement(name = "PERSONNEL_ID")
    protected int personnelid;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfSubmissionProducts submissionProduct;

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the annlrptind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getANNLRPTIND() {
        return annlrptind;
    }

    /**
     * Sets the value of the annlrptind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setANNLRPTIND(String value) {
        this.annlrptind = value;
    }

    /**
     * Gets the value of the bkdpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBKDPREMAMT() {
        return bkdpremamt;
    }

    /**
     * Sets the value of the bkdpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBKDPREMAMT(BigDecimal value) {
        this.bkdpremamt = value;
    }

    /**
     * Gets the value of the boundpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBOUNDPREMAMT() {
        return boundpremamt;
    }

    /**
     * Sets the value of the boundpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBOUNDPREMAMT(BigDecimal value) {
        this.boundpremamt = value;
    }

    /**
     * Gets the value of the brchrind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRCHRIND() {
        return brchrind;
    }

    /**
     * Sets the value of the brchrind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRCHRIND(String value) {
        this.brchrind = value;
    }

    /**
     * Gets the value of the captvrqstind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPTVRQSTIND() {
        return captvrqstind;
    }

    /**
     * Sets the value of the captvrqstind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPTVRQSTIND(String value) {
        this.captvrqstind = value;
    }

    /**
     * Gets the value of the cmnttxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMNTTXT() {
        return cmnttxt;
    }

    /**
     * Sets the value of the cmnttxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMNTTXT(String value) {
        this.cmnttxt = value;
    }

    /**
     * Gets the value of the loshistind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOSHISTIND() {
        return loshistind;
    }

    /**
     * Sets the value of the loshistind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOSHISTIND(String value) {
        this.loshistind = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the otagmtrqstind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOTAGMTRQSTIND() {
        return otagmtrqstind;
    }

    /**
     * Sets the value of the otagmtrqstind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOTAGMTRQSTIND(String value) {
        this.otagmtrqstind = value;
    }

    /**
     * Gets the value of the priocd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRIOCD() {
        return priocd;
    }

    /**
     * Sets the value of the priocd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRIOCD(String value) {
        this.priocd = value;
    }

    /**
     * Gets the value of the qteduedt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQTEDUEDT() {
        return qteduedt;
    }

    /**
     * Sets the value of the qteduedt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQTEDUEDT(XMLGregorianCalendar value) {
        this.qteduedt = value;
    }

    /**
     * Gets the value of the qtepremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTEPREMAMT() {
        return qtepremamt;
    }

    /**
     * Sets the value of the qtepremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTEPREMAMT(BigDecimal value) {
        this.qtepremamt = value;
    }

    /**
     * Gets the value of the recddt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRECDDT() {
        return recddt;
    }

    /**
     * Sets the value of the recddt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRECDDT(XMLGregorianCalendar value) {
        this.recddt = value;
    }

    /**
     * Gets the value of the splagmtrqstind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPLAGMTRQSTIND() {
        return splagmtrqstind;
    }

    /**
     * Sets the value of the splagmtrqstind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPLAGMTRQSTIND(String value) {
        this.splagmtrqstind = value;
    }

    /**
     * Gets the value of the syssrccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSYSSRCCD() {
        return syssrccd;
    }

    /**
     * Sets the value of the syssrccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSYSSRCCD(String value) {
        this.syssrccd = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgid property.
     * 
     */
    public int getORGID() {
        return orgid;
    }

    /**
     * Sets the value of the orgid property.
     * 
     */
    public void setORGID(int value) {
        this.orgid = value;
    }

    /**
     * Gets the value of the custid property.
     * 
     */
    public int getCUSTID() {
        return custid;
    }

    /**
     * Sets the value of the custid property.
     * 
     */
    public void setCUSTID(int value) {
        this.custid = value;
    }

    /**
     * Gets the value of the personnelid property.
     * 
     */
    public int getPERSONNELID() {
        return personnelid;
    }

    /**
     * Sets the value of the personnelid property.
     * 
     */
    public void setPERSONNELID(int value) {
        this.personnelid = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the submissionProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionProducts }
     *     
     */
    public ArrayOfSubmissionProducts getSubmissionProduct() {
        return submissionProduct;
    }

    /**
     * Sets the value of the submissionProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionProducts }
     *     
     */
    public void setSubmissionProduct(ArrayOfSubmissionProducts value) {
        this.submissionProduct = value;
    }

}
