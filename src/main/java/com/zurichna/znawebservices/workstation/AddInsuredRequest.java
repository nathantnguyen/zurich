
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddInsuredRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddInsuredRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AddInsured" type="{http://workstation.znawebservices.zurichna.com}InsuredDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddInsuredRequest", propOrder = {
    "addInsured"
})
public class AddInsuredRequest
    extends RequestBase
{

    @XmlElement(name = "AddInsured")
    protected InsuredDetails addInsured;

    /**
     * Gets the value of the addInsured property.
     * 
     * @return
     *     possible object is
     *     {@link InsuredDetails }
     *     
     */
    public InsuredDetails getAddInsured() {
        return addInsured;
    }

    /**
     * Sets the value of the addInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuredDetails }
     *     
     */
    public void setAddInsured(InsuredDetails value) {
        this.addInsured = value;
    }

}
