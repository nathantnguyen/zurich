
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetContactRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetContactRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestType" type="{http://workstation.znawebservices.zurichna.com}ContactRequestType"/&gt;
 *         &lt;element name="customerPartyId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="contactPartyId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="contactFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contactLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetContactRequest", propOrder = {
    "requestType",
    "customerPartyId",
    "contactPartyId",
    "contactFirstName",
    "contactLastName"
})
public class GetContactRequest
    extends RequestBase
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ContactRequestType requestType;
    protected int customerPartyId;
    protected int contactPartyId;
    protected String contactFirstName;
    protected String contactLastName;

    /**
     * Gets the value of the requestType property.
     * 
     * @return
     *     possible object is
     *     {@link ContactRequestType }
     *     
     */
    public ContactRequestType getRequestType() {
        return requestType;
    }

    /**
     * Sets the value of the requestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactRequestType }
     *     
     */
    public void setRequestType(ContactRequestType value) {
        this.requestType = value;
    }

    /**
     * Gets the value of the customerPartyId property.
     * 
     */
    public int getCustomerPartyId() {
        return customerPartyId;
    }

    /**
     * Sets the value of the customerPartyId property.
     * 
     */
    public void setCustomerPartyId(int value) {
        this.customerPartyId = value;
    }

    /**
     * Gets the value of the contactPartyId property.
     * 
     */
    public int getContactPartyId() {
        return contactPartyId;
    }

    /**
     * Sets the value of the contactPartyId property.
     * 
     */
    public void setContactPartyId(int value) {
        this.contactPartyId = value;
    }

    /**
     * Gets the value of the contactFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactFirstName() {
        return contactFirstName;
    }

    /**
     * Sets the value of the contactFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactFirstName(String value) {
        this.contactFirstName = value;
    }

    /**
     * Gets the value of the contactLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactLastName() {
        return contactLastName;
    }

    /**
     * Sets the value of the contactLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactLastName(String value) {
        this.contactLastName = value;
    }

}
