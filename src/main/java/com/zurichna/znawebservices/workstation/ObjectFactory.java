
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.zurichna.znawebservices.workstation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.zurichna.znawebservices.workstation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetVersionNumber }
     * 
     */
    public GetVersionNumber createGetVersionNumber() {
        return new GetVersionNumber();
    }

    /**
     * Create an instance of {@link GetVersionNumberResponse }
     * 
     */
    public GetVersionNumberResponse createGetVersionNumberResponse() {
        return new GetVersionNumberResponse();
    }

    /**
     * Create an instance of {@link TestEDBConnection }
     * 
     */
    public TestEDBConnection createTestEDBConnection() {
        return new TestEDBConnection();
    }

    /**
     * Create an instance of {@link TestEDBConnectionResponse }
     * 
     */
    public TestEDBConnectionResponse createTestEDBConnectionResponse() {
        return new TestEDBConnectionResponse();
    }

    /**
     * Create an instance of {@link QuickSearch }
     * 
     */
    public QuickSearch createQuickSearch() {
        return new QuickSearch();
    }

    /**
     * Create an instance of {@link QuickSearchrequest }
     * 
     */
    public QuickSearchrequest createQuickSearchrequest() {
        return new QuickSearchrequest();
    }

    /**
     * Create an instance of {@link QuickSearchResponse }
     * 
     */
    public QuickSearchResponse createQuickSearchResponse() {
        return new QuickSearchResponse();
    }

    /**
     * Create an instance of {@link QuickSearchResponse2 }
     * 
     */
    public QuickSearchResponse2 createQuickSearchResponse2() {
        return new QuickSearchResponse2();
    }

    /**
     * Create an instance of {@link SearchZorba }
     * 
     */
    public SearchZorba createSearchZorba() {
        return new SearchZorba();
    }

    /**
     * Create an instance of {@link SearchRequest }
     * 
     */
    public SearchRequest createSearchRequest() {
        return new SearchRequest();
    }

    /**
     * Create an instance of {@link SearchZorbaResponse }
     * 
     */
    public SearchZorbaResponse createSearchZorbaResponse() {
        return new SearchZorbaResponse();
    }

    /**
     * Create an instance of {@link SearchResponse }
     * 
     */
    public SearchResponse createSearchResponse() {
        return new SearchResponse();
    }

    /**
     * Create an instance of {@link Search }
     * 
     */
    public Search createSearch() {
        return new Search();
    }

    /**
     * Create an instance of {@link SearchResponse2 }
     * 
     */
    public SearchResponse2 createSearchResponse2() {
        return new SearchResponse2();
    }

    /**
     * Create an instance of {@link ZorbaAdvanceSearch }
     * 
     */
    public ZorbaAdvanceSearch createZorbaAdvanceSearch() {
        return new ZorbaAdvanceSearch();
    }

    /**
     * Create an instance of {@link ZorbaAdvanceSearchResponse }
     * 
     */
    public ZorbaAdvanceSearchResponse createZorbaAdvanceSearchResponse() {
        return new ZorbaAdvanceSearchResponse();
    }

    /**
     * Create an instance of {@link AdvanceZorbaSearchResponse }
     * 
     */
    public AdvanceZorbaSearchResponse createAdvanceZorbaSearchResponse() {
        return new AdvanceZorbaSearchResponse();
    }

    /**
     * Create an instance of {@link GetCustomer }
     * 
     */
    public GetCustomer createGetCustomer() {
        return new GetCustomer();
    }

    /**
     * Create an instance of {@link GetCustomerRequest }
     * 
     */
    public GetCustomerRequest createGetCustomerRequest() {
        return new GetCustomerRequest();
    }

    /**
     * Create an instance of {@link GetCustomerResponse }
     * 
     */
    public GetCustomerResponse createGetCustomerResponse() {
        return new GetCustomerResponse();
    }

    /**
     * Create an instance of {@link CustomerResponse }
     * 
     */
    public CustomerResponse createCustomerResponse() {
        return new CustomerResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomer }
     * 
     */
    public UpdateCustomer createUpdateCustomer() {
        return new UpdateCustomer();
    }

    /**
     * Create an instance of {@link UpdateCustomerRequest }
     * 
     */
    public UpdateCustomerRequest createUpdateCustomerRequest() {
        return new UpdateCustomerRequest();
    }

    /**
     * Create an instance of {@link UpdateCustomerResponse }
     * 
     */
    public UpdateCustomerResponse createUpdateCustomerResponse() {
        return new UpdateCustomerResponse();
    }

    /**
     * Create an instance of {@link GetSubmission }
     * 
     */
    public GetSubmission createGetSubmission() {
        return new GetSubmission();
    }

    /**
     * Create an instance of {@link GetSubmissionRequest }
     * 
     */
    public GetSubmissionRequest createGetSubmissionRequest() {
        return new GetSubmissionRequest();
    }

    /**
     * Create an instance of {@link GetSubmissionResponse }
     * 
     */
    public GetSubmissionResponse createGetSubmissionResponse() {
        return new GetSubmissionResponse();
    }

    /**
     * Create an instance of {@link SubmissionResponse }
     * 
     */
    public SubmissionResponse createSubmissionResponse() {
        return new SubmissionResponse();
    }

    /**
     * Create an instance of {@link GetMVRData }
     * 
     */
    public GetMVRData createGetMVRData() {
        return new GetMVRData();
    }

    /**
     * Create an instance of {@link GetMVRDataRequest }
     * 
     */
    public GetMVRDataRequest createGetMVRDataRequest() {
        return new GetMVRDataRequest();
    }

    /**
     * Create an instance of {@link GetMVRDataResponse }
     * 
     */
    public GetMVRDataResponse createGetMVRDataResponse() {
        return new GetMVRDataResponse();
    }

    /**
     * Create an instance of {@link GetMVRDataResponse2 }
     * 
     */
    public GetMVRDataResponse2 createGetMVRDataResponse2() {
        return new GetMVRDataResponse2();
    }

    /**
     * Create an instance of {@link UpdateSubmission }
     * 
     */
    public UpdateSubmission createUpdateSubmission() {
        return new UpdateSubmission();
    }

    /**
     * Create an instance of {@link UpdateSubmissionRequest }
     * 
     */
    public UpdateSubmissionRequest createUpdateSubmissionRequest() {
        return new UpdateSubmissionRequest();
    }

    /**
     * Create an instance of {@link UpdateSubmissionResponse }
     * 
     */
    public UpdateSubmissionResponse createUpdateSubmissionResponse() {
        return new UpdateSubmissionResponse();
    }

    /**
     * Create an instance of {@link GetReferenceData }
     * 
     */
    public GetReferenceData createGetReferenceData() {
        return new GetReferenceData();
    }

    /**
     * Create an instance of {@link ReferenceRequest }
     * 
     */
    public ReferenceRequest createReferenceRequest() {
        return new ReferenceRequest();
    }

    /**
     * Create an instance of {@link GetReferenceDataResponse }
     * 
     */
    public GetReferenceDataResponse createGetReferenceDataResponse() {
        return new GetReferenceDataResponse();
    }

    /**
     * Create an instance of {@link ReferenceResponse }
     * 
     */
    public ReferenceResponse createReferenceResponse() {
        return new ReferenceResponse();
    }

    /**
     * Create an instance of {@link GetContact }
     * 
     */
    public GetContact createGetContact() {
        return new GetContact();
    }

    /**
     * Create an instance of {@link GetContactRequest }
     * 
     */
    public GetContactRequest createGetContactRequest() {
        return new GetContactRequest();
    }

    /**
     * Create an instance of {@link GetContactResponse }
     * 
     */
    public GetContactResponse createGetContactResponse() {
        return new GetContactResponse();
    }

    /**
     * Create an instance of {@link ContactResponse }
     * 
     */
    public ContactResponse createContactResponse() {
        return new ContactResponse();
    }

    /**
     * Create an instance of {@link UpdateContact }
     * 
     */
    public UpdateContact createUpdateContact() {
        return new UpdateContact();
    }

    /**
     * Create an instance of {@link UpdateContactRequest }
     * 
     */
    public UpdateContactRequest createUpdateContactRequest() {
        return new UpdateContactRequest();
    }

    /**
     * Create an instance of {@link UpdateContactResponse }
     * 
     */
    public UpdateContactResponse createUpdateContactResponse() {
        return new UpdateContactResponse();
    }

    /**
     * Create an instance of {@link GetPolicyGvnPolAndNoOfTerms }
     * 
     */
    public GetPolicyGvnPolAndNoOfTerms createGetPolicyGvnPolAndNoOfTerms() {
        return new GetPolicyGvnPolAndNoOfTerms();
    }

    /**
     * Create an instance of {@link PolicyRequest }
     * 
     */
    public PolicyRequest createPolicyRequest() {
        return new PolicyRequest();
    }

    /**
     * Create an instance of {@link GetPolicyGvnPolAndNoOfTermsResponse }
     * 
     */
    public GetPolicyGvnPolAndNoOfTermsResponse createGetPolicyGvnPolAndNoOfTermsResponse() {
        return new GetPolicyGvnPolAndNoOfTermsResponse();
    }

    /**
     * Create an instance of {@link PolicyResponse }
     * 
     */
    public PolicyResponse createPolicyResponse() {
        return new PolicyResponse();
    }

    /**
     * Create an instance of {@link GetPolicy }
     * 
     */
    public GetPolicy createGetPolicy() {
        return new GetPolicy();
    }

    /**
     * Create an instance of {@link GetPolicyRequest }
     * 
     */
    public GetPolicyRequest createGetPolicyRequest() {
        return new GetPolicyRequest();
    }

    /**
     * Create an instance of {@link GetPolicyResponse }
     * 
     */
    public GetPolicyResponse createGetPolicyResponse() {
        return new GetPolicyResponse();
    }

    /**
     * Create an instance of {@link GetPty }
     * 
     */
    public GetPty createGetPty() {
        return new GetPty();
    }

    /**
     * Create an instance of {@link GetPtyRequest }
     * 
     */
    public GetPtyRequest createGetPtyRequest() {
        return new GetPtyRequest();
    }

    /**
     * Create an instance of {@link GetPtyResponse }
     * 
     */
    public GetPtyResponse createGetPtyResponse() {
        return new GetPtyResponse();
    }

    /**
     * Create an instance of {@link PtyResponse }
     * 
     */
    public PtyResponse createPtyResponse() {
        return new PtyResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicy }
     * 
     */
    public UpdatePolicy createUpdatePolicy() {
        return new UpdatePolicy();
    }

    /**
     * Create an instance of {@link UpdatePolicyRequest }
     * 
     */
    public UpdatePolicyRequest createUpdatePolicyRequest() {
        return new UpdatePolicyRequest();
    }

    /**
     * Create an instance of {@link UpdatePolicyResponse }
     * 
     */
    public UpdatePolicyResponse createUpdatePolicyResponse() {
        return new UpdatePolicyResponse();
    }

    /**
     * Create an instance of {@link GetAlert }
     * 
     */
    public GetAlert createGetAlert() {
        return new GetAlert();
    }

    /**
     * Create an instance of {@link GetAlertRequest }
     * 
     */
    public GetAlertRequest createGetAlertRequest() {
        return new GetAlertRequest();
    }

    /**
     * Create an instance of {@link GetAlertResponse }
     * 
     */
    public GetAlertResponse createGetAlertResponse() {
        return new GetAlertResponse();
    }

    /**
     * Create an instance of {@link AlertResponse }
     * 
     */
    public AlertResponse createAlertResponse() {
        return new AlertResponse();
    }

    /**
     * Create an instance of {@link UpdateAlert }
     * 
     */
    public UpdateAlert createUpdateAlert() {
        return new UpdateAlert();
    }

    /**
     * Create an instance of {@link UpdateAlertRequest }
     * 
     */
    public UpdateAlertRequest createUpdateAlertRequest() {
        return new UpdateAlertRequest();
    }

    /**
     * Create an instance of {@link UpdateAlertResponse }
     * 
     */
    public UpdateAlertResponse createUpdateAlertResponse() {
        return new UpdateAlertResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyStatusNREN }
     * 
     */
    public UpdatePolicyStatusNREN createUpdatePolicyStatusNREN() {
        return new UpdatePolicyStatusNREN();
    }

    /**
     * Create an instance of {@link UpdatePolicyStatusRequest }
     * 
     */
    public UpdatePolicyStatusRequest createUpdatePolicyStatusRequest() {
        return new UpdatePolicyStatusRequest();
    }

    /**
     * Create an instance of {@link UpdatePolicyStatusNRENResponse }
     * 
     */
    public UpdatePolicyStatusNRENResponse createUpdatePolicyStatusNRENResponse() {
        return new UpdatePolicyStatusNRENResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyStatusResponse }
     * 
     */
    public UpdatePolicyStatusResponse createUpdatePolicyStatusResponse() {
        return new UpdatePolicyStatusResponse();
    }

    /**
     * Create an instance of {@link BindSubmission }
     * 
     */
    public BindSubmission createBindSubmission() {
        return new BindSubmission();
    }

    /**
     * Create an instance of {@link BindSubmissionRequest }
     * 
     */
    public BindSubmissionRequest createBindSubmissionRequest() {
        return new BindSubmissionRequest();
    }

    /**
     * Create an instance of {@link BindSubmissionResponse }
     * 
     */
    public BindSubmissionResponse createBindSubmissionResponse() {
        return new BindSubmissionResponse();
    }

    /**
     * Create an instance of {@link SubmissionBindResponse }
     * 
     */
    public SubmissionBindResponse createSubmissionBindResponse() {
        return new SubmissionBindResponse();
    }

    /**
     * Create an instance of {@link RenewPolicy }
     * 
     */
    public RenewPolicy createRenewPolicy() {
        return new RenewPolicy();
    }

    /**
     * Create an instance of {@link RenewPolicyRequest }
     * 
     */
    public RenewPolicyRequest createRenewPolicyRequest() {
        return new RenewPolicyRequest();
    }

    /**
     * Create an instance of {@link RenewPolicyResponse }
     * 
     */
    public RenewPolicyResponse createRenewPolicyResponse() {
        return new RenewPolicyResponse();
    }

    /**
     * Create an instance of {@link RenewPolicyInfoResponse }
     * 
     */
    public RenewPolicyInfoResponse createRenewPolicyInfoResponse() {
        return new RenewPolicyInfoResponse();
    }

    /**
     * Create an instance of {@link RenewMultiplePolicys }
     * 
     */
    public RenewMultiplePolicys createRenewMultiplePolicys() {
        return new RenewMultiplePolicys();
    }

    /**
     * Create an instance of {@link RenewMultiplePolicyRequest }
     * 
     */
    public RenewMultiplePolicyRequest createRenewMultiplePolicyRequest() {
        return new RenewMultiplePolicyRequest();
    }

    /**
     * Create an instance of {@link RenewMultiplePolicysResponse }
     * 
     */
    public RenewMultiplePolicysResponse createRenewMultiplePolicysResponse() {
        return new RenewMultiplePolicysResponse();
    }

    /**
     * Create an instance of {@link AddInsured }
     * 
     */
    public AddInsured createAddInsured() {
        return new AddInsured();
    }

    /**
     * Create an instance of {@link AddInsuredRequest }
     * 
     */
    public AddInsuredRequest createAddInsuredRequest() {
        return new AddInsuredRequest();
    }

    /**
     * Create an instance of {@link AddInsuredResponse }
     * 
     */
    public AddInsuredResponse createAddInsuredResponse() {
        return new AddInsuredResponse();
    }

    /**
     * Create an instance of {@link AddInsuredResponse2 }
     * 
     */
    public AddInsuredResponse2 createAddInsuredResponse2() {
        return new AddInsuredResponse2();
    }

    /**
     * Create an instance of {@link ModifyInsured }
     * 
     */
    public ModifyInsured createModifyInsured() {
        return new ModifyInsured();
    }

    /**
     * Create an instance of {@link ModifyInsuredRequest }
     * 
     */
    public ModifyInsuredRequest createModifyInsuredRequest() {
        return new ModifyInsuredRequest();
    }

    /**
     * Create an instance of {@link ModifyInsuredResponse }
     * 
     */
    public ModifyInsuredResponse createModifyInsuredResponse() {
        return new ModifyInsuredResponse();
    }

    /**
     * Create an instance of {@link ModifyInsuredResponse2 }
     * 
     */
    public ModifyInsuredResponse2 createModifyInsuredResponse2() {
        return new ModifyInsuredResponse2();
    }

    /**
     * Create an instance of {@link AddSubmission }
     * 
     */
    public AddSubmission createAddSubmission() {
        return new AddSubmission();
    }

    /**
     * Create an instance of {@link AddSubmissionRequest }
     * 
     */
    public AddSubmissionRequest createAddSubmissionRequest() {
        return new AddSubmissionRequest();
    }

    /**
     * Create an instance of {@link AddSubmissionResponse }
     * 
     */
    public AddSubmissionResponse createAddSubmissionResponse() {
        return new AddSubmissionResponse();
    }

    /**
     * Create an instance of {@link CreateAccount }
     * 
     */
    public CreateAccount createCreateAccount() {
        return new CreateAccount();
    }

    /**
     * Create an instance of {@link CreateAccountRequest }
     * 
     */
    public CreateAccountRequest createCreateAccountRequest() {
        return new CreateAccountRequest();
    }

    /**
     * Create an instance of {@link CreateAccountResponse }
     * 
     */
    public CreateAccountResponse createCreateAccountResponse() {
        return new CreateAccountResponse();
    }

    /**
     * Create an instance of {@link CreateAccountResponse2 }
     * 
     */
    public CreateAccountResponse2 createCreateAccountResponse2() {
        return new CreateAccountResponse2();
    }

    /**
     * Create an instance of {@link CreateAccountForSurety }
     * 
     */
    public CreateAccountForSurety createCreateAccountForSurety() {
        return new CreateAccountForSurety();
    }

    /**
     * Create an instance of {@link CreateAccountForSuretyResponse }
     * 
     */
    public CreateAccountForSuretyResponse createCreateAccountForSuretyResponse() {
        return new CreateAccountForSuretyResponse();
    }

    /**
     * Create an instance of {@link DelSMSNProductStatus }
     * 
     */
    public DelSMSNProductStatus createDelSMSNProductStatus() {
        return new DelSMSNProductStatus();
    }

    /**
     * Create an instance of {@link DelSMSNProductRequest }
     * 
     */
    public DelSMSNProductRequest createDelSMSNProductRequest() {
        return new DelSMSNProductRequest();
    }

    /**
     * Create an instance of {@link DelSMSNProductStatusResponse }
     * 
     */
    public DelSMSNProductStatusResponse createDelSMSNProductStatusResponse() {
        return new DelSMSNProductStatusResponse();
    }

    /**
     * Create an instance of {@link DelSMSNProductResponse }
     * 
     */
    public DelSMSNProductResponse createDelSMSNProductResponse() {
        return new DelSMSNProductResponse();
    }

    /**
     * Create an instance of {@link DeletePolicyTerm }
     * 
     */
    public DeletePolicyTerm createDeletePolicyTerm() {
        return new DeletePolicyTerm();
    }

    /**
     * Create an instance of {@link DelPolTermRequest }
     * 
     */
    public DelPolTermRequest createDelPolTermRequest() {
        return new DelPolTermRequest();
    }

    /**
     * Create an instance of {@link DeletePolicyTermResponse }
     * 
     */
    public DeletePolicyTermResponse createDeletePolicyTermResponse() {
        return new DeletePolicyTermResponse();
    }

    /**
     * Create an instance of {@link DelPolTermResponse }
     * 
     */
    public DelPolTermResponse createDelPolTermResponse() {
        return new DelPolTermResponse();
    }

    /**
     * Create an instance of {@link GetWCTechPrice }
     * 
     */
    public GetWCTechPrice createGetWCTechPrice() {
        return new GetWCTechPrice();
    }

    /**
     * Create an instance of {@link GetWCTechPriceDtls }
     * 
     */
    public GetWCTechPriceDtls createGetWCTechPriceDtls() {
        return new GetWCTechPriceDtls();
    }

    /**
     * Create an instance of {@link GetWCTechPriceResponse }
     * 
     */
    public GetWCTechPriceResponse createGetWCTechPriceResponse() {
        return new GetWCTechPriceResponse();
    }

    /**
     * Create an instance of {@link WCTechPriceResponse }
     * 
     */
    public WCTechPriceResponse createWCTechPriceResponse() {
        return new WCTechPriceResponse();
    }

    /**
     * Create an instance of {@link GetAutoIndicationPric }
     * 
     */
    public GetAutoIndicationPric createGetAutoIndicationPric() {
        return new GetAutoIndicationPric();
    }

    /**
     * Create an instance of {@link GetAutoIndicationPrcRequest }
     * 
     */
    public GetAutoIndicationPrcRequest createGetAutoIndicationPrcRequest() {
        return new GetAutoIndicationPrcRequest();
    }

    /**
     * Create an instance of {@link GetAutoIndicationPricResponse }
     * 
     */
    public GetAutoIndicationPricResponse createGetAutoIndicationPricResponse() {
        return new GetAutoIndicationPricResponse();
    }

    /**
     * Create an instance of {@link AutoIndicationPrcResponse }
     * 
     */
    public AutoIndicationPrcResponse createAutoIndicationPrcResponse() {
        return new AutoIndicationPrcResponse();
    }

    /**
     * Create an instance of {@link GetPolicySymbol }
     * 
     */
    public GetPolicySymbol createGetPolicySymbol() {
        return new GetPolicySymbol();
    }

    /**
     * Create an instance of {@link GetPolicySymbolRequest }
     * 
     */
    public GetPolicySymbolRequest createGetPolicySymbolRequest() {
        return new GetPolicySymbolRequest();
    }

    /**
     * Create an instance of {@link GetPolicySymbolResponse }
     * 
     */
    public GetPolicySymbolResponse createGetPolicySymbolResponse() {
        return new GetPolicySymbolResponse();
    }

    /**
     * Create an instance of {@link GetPolicySymbolsResponse }
     * 
     */
    public GetPolicySymbolsResponse createGetPolicySymbolsResponse() {
        return new GetPolicySymbolsResponse();
    }

    /**
     * Create an instance of {@link WCTargetPrice }
     * 
     */
    public WCTargetPrice createWCTargetPrice() {
        return new WCTargetPrice();
    }

    /**
     * Create an instance of {@link GetWCAHTargetPrcDtls }
     * 
     */
    public GetWCAHTargetPrcDtls createGetWCAHTargetPrcDtls() {
        return new GetWCAHTargetPrcDtls();
    }

    /**
     * Create an instance of {@link WCTargetPriceResponse }
     * 
     */
    public WCTargetPriceResponse createWCTargetPriceResponse() {
        return new WCTargetPriceResponse();
    }

    /**
     * Create an instance of {@link WCTargetPriceResponse2 }
     * 
     */
    public WCTargetPriceResponse2 createWCTargetPriceResponse2() {
        return new WCTargetPriceResponse2();
    }

    /**
     * Create an instance of {@link GetAgreementsGivenPolicyNumber }
     * 
     */
    public GetAgreementsGivenPolicyNumber createGetAgreementsGivenPolicyNumber() {
        return new GetAgreementsGivenPolicyNumber();
    }

    /**
     * Create an instance of {@link GetAgreementsGivenPolicyNumberRequest }
     * 
     */
    public GetAgreementsGivenPolicyNumberRequest createGetAgreementsGivenPolicyNumberRequest() {
        return new GetAgreementsGivenPolicyNumberRequest();
    }

    /**
     * Create an instance of {@link GetAgreementsGivenPolicyNumberResponse }
     * 
     */
    public GetAgreementsGivenPolicyNumberResponse createGetAgreementsGivenPolicyNumberResponse() {
        return new GetAgreementsGivenPolicyNumberResponse();
    }

    /**
     * Create an instance of {@link GetAgreementsGivenPolicyNumberResponse2 }
     * 
     */
    public GetAgreementsGivenPolicyNumberResponse2 createGetAgreementsGivenPolicyNumberResponse2() {
        return new GetAgreementsGivenPolicyNumberResponse2();
    }

    /**
     * Create an instance of {@link GetRMSLoginGivenSubmissionProductId }
     * 
     */
    public GetRMSLoginGivenSubmissionProductId createGetRMSLoginGivenSubmissionProductId() {
        return new GetRMSLoginGivenSubmissionProductId();
    }

    /**
     * Create an instance of {@link GetRMSLoginGivenSubmissionProductIdRequest }
     * 
     */
    public GetRMSLoginGivenSubmissionProductIdRequest createGetRMSLoginGivenSubmissionProductIdRequest() {
        return new GetRMSLoginGivenSubmissionProductIdRequest();
    }

    /**
     * Create an instance of {@link GetRMSLoginGivenSubmissionProductIdResponse }
     * 
     */
    public GetRMSLoginGivenSubmissionProductIdResponse createGetRMSLoginGivenSubmissionProductIdResponse() {
        return new GetRMSLoginGivenSubmissionProductIdResponse();
    }

    /**
     * Create an instance of {@link GetRMSLoginGivenSubmissionProductIdResponse2 }
     * 
     */
    public GetRMSLoginGivenSubmissionProductIdResponse2 createGetRMSLoginGivenSubmissionProductIdResponse2() {
        return new GetRMSLoginGivenSubmissionProductIdResponse2();
    }

    /**
     * Create an instance of {@link ZSpireSubmissionIdForRenewalQuote }
     * 
     */
    public ZSpireSubmissionIdForRenewalQuote createZSpireSubmissionIdForRenewalQuote() {
        return new ZSpireSubmissionIdForRenewalQuote();
    }

    /**
     * Create an instance of {@link ZSpireSMSNIdForRenewalQuoteRequest }
     * 
     */
    public ZSpireSMSNIdForRenewalQuoteRequest createZSpireSMSNIdForRenewalQuoteRequest() {
        return new ZSpireSMSNIdForRenewalQuoteRequest();
    }

    /**
     * Create an instance of {@link ZSpireSubmissionIdForRenewalQuoteResponse }
     * 
     */
    public ZSpireSubmissionIdForRenewalQuoteResponse createZSpireSubmissionIdForRenewalQuoteResponse() {
        return new ZSpireSubmissionIdForRenewalQuoteResponse();
    }

    /**
     * Create an instance of {@link ZSpireSMSNIdForRenewalQuoteResponse }
     * 
     */
    public ZSpireSMSNIdForRenewalQuoteResponse createZSpireSMSNIdForRenewalQuoteResponse() {
        return new ZSpireSMSNIdForRenewalQuoteResponse();
    }

    /**
     * Create an instance of {@link GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDt }
     * 
     */
    public GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDt createGetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDt() {
        return new GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDt();
    }

    /**
     * Create an instance of {@link GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest }
     * 
     */
    public GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest createGetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest() {
        return new GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtRequest();
    }

    /**
     * Create an instance of {@link GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse }
     * 
     */
    public GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse createGetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse() {
        return new GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse();
    }

    /**
     * Create an instance of {@link GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2 }
     * 
     */
    public GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2 createGetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2() {
        return new GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2();
    }

    /**
     * Create an instance of {@link GetSmsnAndPolInfoForWFM }
     * 
     */
    public GetSmsnAndPolInfoForWFM createGetSmsnAndPolInfoForWFM() {
        return new GetSmsnAndPolInfoForWFM();
    }

    /**
     * Create an instance of {@link GetWFMSubmissionRequest }
     * 
     */
    public GetWFMSubmissionRequest createGetWFMSubmissionRequest() {
        return new GetWFMSubmissionRequest();
    }

    /**
     * Create an instance of {@link GetSmsnAndPolInfoForWFMResponse }
     * 
     */
    public GetSmsnAndPolInfoForWFMResponse createGetSmsnAndPolInfoForWFMResponse() {
        return new GetSmsnAndPolInfoForWFMResponse();
    }

    /**
     * Create an instance of {@link WFMSubmissionResponse }
     * 
     */
    public WFMSubmissionResponse createWFMSubmissionResponse() {
        return new WFMSubmissionResponse();
    }

    /**
     * Create an instance of {@link GetSMSNDetailsForGivenProducer }
     * 
     */
    public GetSMSNDetailsForGivenProducer createGetSMSNDetailsForGivenProducer() {
        return new GetSMSNDetailsForGivenProducer();
    }

    /**
     * Create an instance of {@link GetSMSNDetailsForGivenProducerRequest }
     * 
     */
    public GetSMSNDetailsForGivenProducerRequest createGetSMSNDetailsForGivenProducerRequest() {
        return new GetSMSNDetailsForGivenProducerRequest();
    }

    /**
     * Create an instance of {@link GetSMSNDetailsForGivenProducerResponse }
     * 
     */
    public GetSMSNDetailsForGivenProducerResponse createGetSMSNDetailsForGivenProducerResponse() {
        return new GetSMSNDetailsForGivenProducerResponse();
    }

    /**
     * Create an instance of {@link SMSNDetailsForGivenProducerResponse }
     * 
     */
    public SMSNDetailsForGivenProducerResponse createSMSNDetailsForGivenProducerResponse() {
        return new SMSNDetailsForGivenProducerResponse();
    }

    /**
     * Create an instance of {@link AddModifySuretyAccount }
     * 
     */
    public AddModifySuretyAccount createAddModifySuretyAccount() {
        return new AddModifySuretyAccount();
    }

    /**
     * Create an instance of {@link AddModifyAccountRequest }
     * 
     */
    public AddModifyAccountRequest createAddModifyAccountRequest() {
        return new AddModifyAccountRequest();
    }

    /**
     * Create an instance of {@link AddModifySuretyAccountResponse }
     * 
     */
    public AddModifySuretyAccountResponse createAddModifySuretyAccountResponse() {
        return new AddModifySuretyAccountResponse();
    }

    /**
     * Create an instance of {@link GetWCCATModellingResults }
     * 
     */
    public GetWCCATModellingResults createGetWCCATModellingResults() {
        return new GetWCCATModellingResults();
    }

    /**
     * Create an instance of {@link GetWCCATModellingResultsRequest }
     * 
     */
    public GetWCCATModellingResultsRequest createGetWCCATModellingResultsRequest() {
        return new GetWCCATModellingResultsRequest();
    }

    /**
     * Create an instance of {@link GetWCCATModellingResultsResponse }
     * 
     */
    public GetWCCATModellingResultsResponse createGetWCCATModellingResultsResponse() {
        return new GetWCCATModellingResultsResponse();
    }

    /**
     * Create an instance of {@link GetWCCATModellingResultsResponse2 }
     * 
     */
    public GetWCCATModellingResultsResponse2 createGetWCCATModellingResultsResponse2() {
        return new GetWCCATModellingResultsResponse2();
    }

    /**
     * Create an instance of {@link GetTargetPriceRateTrackDtls }
     * 
     */
    public GetTargetPriceRateTrackDtls createGetTargetPriceRateTrackDtls() {
        return new GetTargetPriceRateTrackDtls();
    }

    /**
     * Create an instance of {@link GetTargetPriceRateTrackDtlsRequest }
     * 
     */
    public GetTargetPriceRateTrackDtlsRequest createGetTargetPriceRateTrackDtlsRequest() {
        return new GetTargetPriceRateTrackDtlsRequest();
    }

    /**
     * Create an instance of {@link GetTargetPriceRateTrackDtlsResponse }
     * 
     */
    public GetTargetPriceRateTrackDtlsResponse createGetTargetPriceRateTrackDtlsResponse() {
        return new GetTargetPriceRateTrackDtlsResponse();
    }

    /**
     * Create an instance of {@link GetTargetPriceRateTrackDtlsResponse2 }
     * 
     */
    public GetTargetPriceRateTrackDtlsResponse2 createGetTargetPriceRateTrackDtlsResponse2() {
        return new GetTargetPriceRateTrackDtlsResponse2();
    }

    /**
     * Create an instance of {@link PerformAutomatedClearance }
     * 
     */
    public PerformAutomatedClearance createPerformAutomatedClearance() {
        return new PerformAutomatedClearance();
    }

    /**
     * Create an instance of {@link PerformAutomatedClearanceRequest }
     * 
     */
    public PerformAutomatedClearanceRequest createPerformAutomatedClearanceRequest() {
        return new PerformAutomatedClearanceRequest();
    }

    /**
     * Create an instance of {@link PerformAutomatedClearanceResponse }
     * 
     */
    public PerformAutomatedClearanceResponse createPerformAutomatedClearanceResponse() {
        return new PerformAutomatedClearanceResponse();
    }

    /**
     * Create an instance of {@link PerformAutomatedClearanceResponse2 }
     * 
     */
    public PerformAutomatedClearanceResponse2 createPerformAutomatedClearanceResponse2() {
        return new PerformAutomatedClearanceResponse2();
    }

    /**
     * Create an instance of {@link RetrieveUserHistory }
     * 
     */
    public RetrieveUserHistory createRetrieveUserHistory() {
        return new RetrieveUserHistory();
    }

    /**
     * Create an instance of {@link RetrieveUserHistoryRequest }
     * 
     */
    public RetrieveUserHistoryRequest createRetrieveUserHistoryRequest() {
        return new RetrieveUserHistoryRequest();
    }

    /**
     * Create an instance of {@link RetrieveUserHistoryResponse }
     * 
     */
    public RetrieveUserHistoryResponse createRetrieveUserHistoryResponse() {
        return new RetrieveUserHistoryResponse();
    }

    /**
     * Create an instance of {@link RetrieveUserHistoryResponse2 }
     * 
     */
    public RetrieveUserHistoryResponse2 createRetrieveUserHistoryResponse2() {
        return new RetrieveUserHistoryResponse2();
    }

    /**
     * Create an instance of {@link RecordSubmission }
     * 
     */
    public RecordSubmission createRecordSubmission() {
        return new RecordSubmission();
    }

    /**
     * Create an instance of {@link RecordSubmissionRequest }
     * 
     */
    public RecordSubmissionRequest createRecordSubmissionRequest() {
        return new RecordSubmissionRequest();
    }

    /**
     * Create an instance of {@link RecordSubmissionResponse }
     * 
     */
    public RecordSubmissionResponse createRecordSubmissionResponse() {
        return new RecordSubmissionResponse();
    }

    /**
     * Create an instance of {@link SubmissionV2Response }
     * 
     */
    public SubmissionV2Response createSubmissionV2Response() {
        return new SubmissionV2Response();
    }

    /**
     * Create an instance of {@link UpdateSubmissionV2 }
     * 
     */
    public UpdateSubmissionV2 createUpdateSubmissionV2() {
        return new UpdateSubmissionV2();
    }

    /**
     * Create an instance of {@link UpdateSubmissionV2Request }
     * 
     */
    public UpdateSubmissionV2Request createUpdateSubmissionV2Request() {
        return new UpdateSubmissionV2Request();
    }

    /**
     * Create an instance of {@link UpdateSubmissionV2Response }
     * 
     */
    public UpdateSubmissionV2Response createUpdateSubmissionV2Response() {
        return new UpdateSubmissionV2Response();
    }

    /**
     * Create an instance of {@link UpdateSubmissionV2Response2 }
     * 
     */
    public UpdateSubmissionV2Response2 createUpdateSubmissionV2Response2() {
        return new UpdateSubmissionV2Response2();
    }

    /**
     * Create an instance of {@link BindSubmissionV2 }
     * 
     */
    public BindSubmissionV2 createBindSubmissionV2() {
        return new BindSubmissionV2();
    }

    /**
     * Create an instance of {@link BindSubmissionV2Response }
     * 
     */
    public BindSubmissionV2Response createBindSubmissionV2Response() {
        return new BindSubmissionV2Response();
    }

    /**
     * Create an instance of {@link SubmissionBindV2Response }
     * 
     */
    public SubmissionBindV2Response createSubmissionBindV2Response() {
        return new SubmissionBindV2Response();
    }

    /**
     * Create an instance of {@link GetSubmissionV2 }
     * 
     */
    public GetSubmissionV2 createGetSubmissionV2() {
        return new GetSubmissionV2();
    }

    /**
     * Create an instance of {@link GetSubmissionV2Response }
     * 
     */
    public GetSubmissionV2Response createGetSubmissionV2Response() {
        return new GetSubmissionV2Response();
    }

    /**
     * Create an instance of {@link UpdateSubmissionProductStatus }
     * 
     */
    public UpdateSubmissionProductStatus createUpdateSubmissionProductStatus() {
        return new UpdateSubmissionProductStatus();
    }

    /**
     * Create an instance of {@link SubmissionProductStatusRequest }
     * 
     */
    public SubmissionProductStatusRequest createSubmissionProductStatusRequest() {
        return new SubmissionProductStatusRequest();
    }

    /**
     * Create an instance of {@link UpdateSubmissionProductStatusResponse }
     * 
     */
    public UpdateSubmissionProductStatusResponse createUpdateSubmissionProductStatusResponse() {
        return new UpdateSubmissionProductStatusResponse();
    }

    /**
     * Create an instance of {@link SubmissionProductStatusResponse }
     * 
     */
    public SubmissionProductStatusResponse createSubmissionProductStatusResponse() {
        return new SubmissionProductStatusResponse();
    }

    /**
     * Create an instance of {@link UpdateProduct }
     * 
     */
    public UpdateProduct createUpdateProduct() {
        return new UpdateProduct();
    }

    /**
     * Create an instance of {@link UpdateProductRequest }
     * 
     */
    public UpdateProductRequest createUpdateProductRequest() {
        return new UpdateProductRequest();
    }

    /**
     * Create an instance of {@link UpdateProductResponse }
     * 
     */
    public UpdateProductResponse createUpdateProductResponse() {
        return new UpdateProductResponse();
    }

    /**
     * Create an instance of {@link UpdateProductResponse2 }
     * 
     */
    public UpdateProductResponse2 createUpdateProductResponse2() {
        return new UpdateProductResponse2();
    }

    /**
     * Create an instance of {@link Requester }
     * 
     */
    public Requester createRequester() {
        return new Requester();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link ArrayOfResponseInfo }
     * 
     */
    public ArrayOfResponseInfo createArrayOfResponseInfo() {
        return new ArrayOfResponseInfo();
    }

    /**
     * Create an instance of {@link ResponseInfo }
     * 
     */
    public ResponseInfo createResponseInfo() {
        return new ResponseInfo();
    }

    /**
     * Create an instance of {@link ArrayOfQuickSerachCustomer }
     * 
     */
    public ArrayOfQuickSerachCustomer createArrayOfQuickSerachCustomer() {
        return new ArrayOfQuickSerachCustomer();
    }

    /**
     * Create an instance of {@link QuickSerachCustomer }
     * 
     */
    public QuickSerachCustomer createQuickSerachCustomer() {
        return new QuickSerachCustomer();
    }

    /**
     * Create an instance of {@link CustomerSearchCriteria }
     * 
     */
    public CustomerSearchCriteria createCustomerSearchCriteria() {
        return new CustomerSearchCriteria();
    }

    /**
     * Create an instance of {@link SubmissionSearchCriteria }
     * 
     */
    public SubmissionSearchCriteria createSubmissionSearchCriteria() {
        return new SubmissionSearchCriteria();
    }

    /**
     * Create an instance of {@link PolicySearchCriteria }
     * 
     */
    public PolicySearchCriteria createPolicySearchCriteria() {
        return new PolicySearchCriteria();
    }

    /**
     * Create an instance of {@link ArrayOfCustomerSearchResults }
     * 
     */
    public ArrayOfCustomerSearchResults createArrayOfCustomerSearchResults() {
        return new ArrayOfCustomerSearchResults();
    }

    /**
     * Create an instance of {@link CustomerSearchResults }
     * 
     */
    public CustomerSearchResults createCustomerSearchResults() {
        return new CustomerSearchResults();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionSearchResults }
     * 
     */
    public ArrayOfSubmissionSearchResults createArrayOfSubmissionSearchResults() {
        return new ArrayOfSubmissionSearchResults();
    }

    /**
     * Create an instance of {@link SubmissionSearchResults }
     * 
     */
    public SubmissionSearchResults createSubmissionSearchResults() {
        return new SubmissionSearchResults();
    }

    /**
     * Create an instance of {@link ArrayOfPolicySearchResults }
     * 
     */
    public ArrayOfPolicySearchResults createArrayOfPolicySearchResults() {
        return new ArrayOfPolicySearchResults();
    }

    /**
     * Create an instance of {@link PolicySearchResults }
     * 
     */
    public PolicySearchResults createPolicySearchResults() {
        return new PolicySearchResults();
    }

    /**
     * Create an instance of {@link ArrayOfInsuredSearchResults }
     * 
     */
    public ArrayOfInsuredSearchResults createArrayOfInsuredSearchResults() {
        return new ArrayOfInsuredSearchResults();
    }

    /**
     * Create an instance of {@link InsuredSearchResults }
     * 
     */
    public InsuredSearchResults createInsuredSearchResults() {
        return new InsuredSearchResults();
    }

    /**
     * Create an instance of {@link ArrayOfAdvanceZorbaSearch }
     * 
     */
    public ArrayOfAdvanceZorbaSearch createArrayOfAdvanceZorbaSearch() {
        return new ArrayOfAdvanceZorbaSearch();
    }

    /**
     * Create an instance of {@link AdvanceZorbaSearch }
     * 
     */
    public AdvanceZorbaSearch createAdvanceZorbaSearch() {
        return new AdvanceZorbaSearch();
    }

    /**
     * Create an instance of {@link ZorbaCustomer }
     * 
     */
    public ZorbaCustomer createZorbaCustomer() {
        return new ZorbaCustomer();
    }

    /**
     * Create an instance of {@link ArrayOfZorbaAddress }
     * 
     */
    public ArrayOfZorbaAddress createArrayOfZorbaAddress() {
        return new ArrayOfZorbaAddress();
    }

    /**
     * Create an instance of {@link ZorbaAddress }
     * 
     */
    public ZorbaAddress createZorbaAddress() {
        return new ZorbaAddress();
    }

    /**
     * Create an instance of {@link ArrayOfZorbaTelephone }
     * 
     */
    public ArrayOfZorbaTelephone createArrayOfZorbaTelephone() {
        return new ArrayOfZorbaTelephone();
    }

    /**
     * Create an instance of {@link ZorbaTelephone }
     * 
     */
    public ZorbaTelephone createZorbaTelephone() {
        return new ZorbaTelephone();
    }

    /**
     * Create an instance of {@link ArrayOfZorbaSubmision }
     * 
     */
    public ArrayOfZorbaSubmision createArrayOfZorbaSubmision() {
        return new ArrayOfZorbaSubmision();
    }

    /**
     * Create an instance of {@link ZorbaSubmision }
     * 
     */
    public ZorbaSubmision createZorbaSubmision() {
        return new ZorbaSubmision();
    }

    /**
     * Create an instance of {@link ArrayOfZorbaProducts }
     * 
     */
    public ArrayOfZorbaProducts createArrayOfZorbaProducts() {
        return new ArrayOfZorbaProducts();
    }

    /**
     * Create an instance of {@link ZorbaProducts }
     * 
     */
    public ZorbaProducts createZorbaProducts() {
        return new ZorbaProducts();
    }

    /**
     * Create an instance of {@link ArrayOfZorbaProductStatus }
     * 
     */
    public ArrayOfZorbaProductStatus createArrayOfZorbaProductStatus() {
        return new ArrayOfZorbaProductStatus();
    }

    /**
     * Create an instance of {@link ZorbaProductStatus }
     * 
     */
    public ZorbaProductStatus createZorbaProductStatus() {
        return new ZorbaProductStatus();
    }

    /**
     * Create an instance of {@link ArrayOfProductPolicy }
     * 
     */
    public ArrayOfProductPolicy createArrayOfProductPolicy() {
        return new ArrayOfProductPolicy();
    }

    /**
     * Create an instance of {@link ProductPolicy }
     * 
     */
    public ProductPolicy createProductPolicy() {
        return new ProductPolicy();
    }

    /**
     * Create an instance of {@link ArrayOfCustomer }
     * 
     */
    public ArrayOfCustomer createArrayOfCustomer() {
        return new ArrayOfCustomer();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link ArrayOfAddress }
     * 
     */
    public ArrayOfAddress createArrayOfAddress() {
        return new ArrayOfAddress();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link ArrayOfTelephone }
     * 
     */
    public ArrayOfTelephone createArrayOfTelephone() {
        return new ArrayOfTelephone();
    }

    /**
     * Create an instance of {@link Telephone }
     * 
     */
    public Telephone createTelephone() {
        return new Telephone();
    }

    /**
     * Create an instance of {@link ArrayOfCustomerAdditionalInformation }
     * 
     */
    public ArrayOfCustomerAdditionalInformation createArrayOfCustomerAdditionalInformation() {
        return new ArrayOfCustomerAdditionalInformation();
    }

    /**
     * Create an instance of {@link CustomerAdditionalInformation }
     * 
     */
    public CustomerAdditionalInformation createCustomerAdditionalInformation() {
        return new CustomerAdditionalInformation();
    }

    /**
     * Create an instance of {@link ArrayOfSubmission }
     * 
     */
    public ArrayOfSubmission createArrayOfSubmission() {
        return new ArrayOfSubmission();
    }

    /**
     * Create an instance of {@link Submission }
     * 
     */
    public Submission createSubmission() {
        return new Submission();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProduct }
     * 
     */
    public ArrayOfSubmissionProduct createArrayOfSubmissionProduct() {
        return new ArrayOfSubmissionProduct();
    }

    /**
     * Create an instance of {@link SubmissionProduct }
     * 
     */
    public SubmissionProduct createSubmissionProduct() {
        return new SubmissionProduct();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProductStatus }
     * 
     */
    public ArrayOfSubmissionProductStatus createArrayOfSubmissionProductStatus() {
        return new ArrayOfSubmissionProductStatus();
    }

    /**
     * Create an instance of {@link SubmissionProductStatus }
     * 
     */
    public SubmissionProductStatus createSubmissionProductStatus() {
        return new SubmissionProductStatus();
    }

    /**
     * Create an instance of {@link ArrayOfMVRDataSubmissionProduct }
     * 
     */
    public ArrayOfMVRDataSubmissionProduct createArrayOfMVRDataSubmissionProduct() {
        return new ArrayOfMVRDataSubmissionProduct();
    }

    /**
     * Create an instance of {@link MVRDataSubmissionProduct }
     * 
     */
    public MVRDataSubmissionProduct createMVRDataSubmissionProduct() {
        return new MVRDataSubmissionProduct();
    }

    /**
     * Create an instance of {@link ArrayOfDriver }
     * 
     */
    public ArrayOfDriver createArrayOfDriver() {
        return new ArrayOfDriver();
    }

    /**
     * Create an instance of {@link Driver }
     * 
     */
    public Driver createDriver() {
        return new Driver();
    }

    /**
     * Create an instance of {@link ArrayOfReferenceRequestCriteria }
     * 
     */
    public ArrayOfReferenceRequestCriteria createArrayOfReferenceRequestCriteria() {
        return new ArrayOfReferenceRequestCriteria();
    }

    /**
     * Create an instance of {@link ReferenceRequestCriteria }
     * 
     */
    public ReferenceRequestCriteria createReferenceRequestCriteria() {
        return new ReferenceRequestCriteria();
    }

    /**
     * Create an instance of {@link ArrayOfCountry }
     * 
     */
    public ArrayOfCountry createArrayOfCountry() {
        return new ArrayOfCountry();
    }

    /**
     * Create an instance of {@link Country }
     * 
     */
    public Country createCountry() {
        return new Country();
    }

    /**
     * Create an instance of {@link Organization }
     * 
     */
    public Organization createOrganization() {
        return new Organization();
    }

    /**
     * Create an instance of {@link ArrayOfOrganization }
     * 
     */
    public ArrayOfOrganization createArrayOfOrganization() {
        return new ArrayOfOrganization();
    }

    /**
     * Create an instance of {@link ArrayOfProducer }
     * 
     */
    public ArrayOfProducer createArrayOfProducer() {
        return new ArrayOfProducer();
    }

    /**
     * Create an instance of {@link Producer }
     * 
     */
    public Producer createProducer() {
        return new Producer();
    }

    /**
     * Create an instance of {@link ArrayOfProduct }
     * 
     */
    public ArrayOfProduct createArrayOfProduct() {
        return new ArrayOfProduct();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link ArrayOfSIC }
     * 
     */
    public ArrayOfSIC createArrayOfSIC() {
        return new ArrayOfSIC();
    }

    /**
     * Create an instance of {@link SIC }
     * 
     */
    public SIC createSIC() {
        return new SIC();
    }

    /**
     * Create an instance of {@link ArrayOfState }
     * 
     */
    public ArrayOfState createArrayOfState() {
        return new ArrayOfState();
    }

    /**
     * Create an instance of {@link State }
     * 
     */
    public State createState() {
        return new State();
    }

    /**
     * Create an instance of {@link ArrayOfEmployee }
     * 
     */
    public ArrayOfEmployee createArrayOfEmployee() {
        return new ArrayOfEmployee();
    }

    /**
     * Create an instance of {@link Employee }
     * 
     */
    public Employee createEmployee() {
        return new Employee();
    }

    /**
     * Create an instance of {@link ArrayOfUnderwritingProgram }
     * 
     */
    public ArrayOfUnderwritingProgram createArrayOfUnderwritingProgram() {
        return new ArrayOfUnderwritingProgram();
    }

    /**
     * Create an instance of {@link UnderwritingProgram }
     * 
     */
    public UnderwritingProgram createUnderwritingProgram() {
        return new UnderwritingProgram();
    }

    /**
     * Create an instance of {@link ArrayOfGeoLocation }
     * 
     */
    public ArrayOfGeoLocation createArrayOfGeoLocation() {
        return new ArrayOfGeoLocation();
    }

    /**
     * Create an instance of {@link GeoLocation }
     * 
     */
    public GeoLocation createGeoLocation() {
        return new GeoLocation();
    }

    /**
     * Create an instance of {@link ArrayOfCarrier }
     * 
     */
    public ArrayOfCarrier createArrayOfCarrier() {
        return new ArrayOfCarrier();
    }

    /**
     * Create an instance of {@link Carrier }
     * 
     */
    public Carrier createCarrier() {
        return new Carrier();
    }

    /**
     * Create an instance of {@link ArrayOfReasonCode }
     * 
     */
    public ArrayOfReasonCode createArrayOfReasonCode() {
        return new ArrayOfReasonCode();
    }

    /**
     * Create an instance of {@link ReasonCode }
     * 
     */
    public ReasonCode createReasonCode() {
        return new ReasonCode();
    }

    /**
     * Create an instance of {@link ArrayOfReInsuranceProgramType }
     * 
     */
    public ArrayOfReInsuranceProgramType createArrayOfReInsuranceProgramType() {
        return new ArrayOfReInsuranceProgramType();
    }

    /**
     * Create an instance of {@link ReInsuranceProgramType }
     * 
     */
    public ReInsuranceProgramType createReInsuranceProgramType() {
        return new ReInsuranceProgramType();
    }

    /**
     * Create an instance of {@link ArrayOfCompetitorPrice }
     * 
     */
    public ArrayOfCompetitorPrice createArrayOfCompetitorPrice() {
        return new ArrayOfCompetitorPrice();
    }

    /**
     * Create an instance of {@link CompetitorPrice }
     * 
     */
    public CompetitorPrice createCompetitorPrice() {
        return new CompetitorPrice();
    }

    /**
     * Create an instance of {@link ArrayOfBillType }
     * 
     */
    public ArrayOfBillType createArrayOfBillType() {
        return new ArrayOfBillType();
    }

    /**
     * Create an instance of {@link BillType }
     * 
     */
    public BillType createBillType() {
        return new BillType();
    }

    /**
     * Create an instance of {@link ArrayOfContact }
     * 
     */
    public ArrayOfContact createArrayOfContact() {
        return new ArrayOfContact();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link ArrayOfContactAddress }
     * 
     */
    public ArrayOfContactAddress createArrayOfContactAddress() {
        return new ArrayOfContactAddress();
    }

    /**
     * Create an instance of {@link ContactAddress }
     * 
     */
    public ContactAddress createContactAddress() {
        return new ContactAddress();
    }

    /**
     * Create an instance of {@link ArrayOfContactTelephone }
     * 
     */
    public ArrayOfContactTelephone createArrayOfContactTelephone() {
        return new ArrayOfContactTelephone();
    }

    /**
     * Create an instance of {@link ContactTelephone }
     * 
     */
    public ContactTelephone createContactTelephone() {
        return new ContactTelephone();
    }

    /**
     * Create an instance of {@link ArrayOfContactRole }
     * 
     */
    public ArrayOfContactRole createArrayOfContactRole() {
        return new ArrayOfContactRole();
    }

    /**
     * Create an instance of {@link ContactRole }
     * 
     */
    public ContactRole createContactRole() {
        return new ContactRole();
    }

    /**
     * Create an instance of {@link ArrayOfContactComment }
     * 
     */
    public ArrayOfContactComment createArrayOfContactComment() {
        return new ArrayOfContactComment();
    }

    /**
     * Create an instance of {@link ContactComment }
     * 
     */
    public ContactComment createContactComment() {
        return new ContactComment();
    }

    /**
     * Create an instance of {@link ArrayOfPolicy }
     * 
     */
    public ArrayOfPolicy createArrayOfPolicy() {
        return new ArrayOfPolicy();
    }

    /**
     * Create an instance of {@link Policy }
     * 
     */
    public Policy createPolicy() {
        return new Policy();
    }

    /**
     * Create an instance of {@link ArrayOfPolicyProduct }
     * 
     */
    public ArrayOfPolicyProduct createArrayOfPolicyProduct() {
        return new ArrayOfPolicyProduct();
    }

    /**
     * Create an instance of {@link PolicyProduct }
     * 
     */
    public PolicyProduct createPolicyProduct() {
        return new PolicyProduct();
    }

    /**
     * Create an instance of {@link ArrayOfAlert }
     * 
     */
    public ArrayOfAlert createArrayOfAlert() {
        return new ArrayOfAlert();
    }

    /**
     * Create an instance of {@link Alert }
     * 
     */
    public Alert createAlert() {
        return new Alert();
    }

    /**
     * Create an instance of {@link MasterPolicy }
     * 
     */
    public MasterPolicy createMasterPolicy() {
        return new MasterPolicy();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionBindProduct }
     * 
     */
    public ArrayOfSubmissionBindProduct createArrayOfSubmissionBindProduct() {
        return new ArrayOfSubmissionBindProduct();
    }

    /**
     * Create an instance of {@link SubmissionBindProduct }
     * 
     */
    public SubmissionBindProduct createSubmissionBindProduct() {
        return new SubmissionBindProduct();
    }

    /**
     * Create an instance of {@link PolicyBind }
     * 
     */
    public PolicyBind createPolicyBind() {
        return new PolicyBind();
    }

    /**
     * Create an instance of {@link SubmissionBind }
     * 
     */
    public SubmissionBind createSubmissionBind() {
        return new SubmissionBind();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProducts }
     * 
     */
    public ArrayOfSubmissionProducts createArrayOfSubmissionProducts() {
        return new ArrayOfSubmissionProducts();
    }

    /**
     * Create an instance of {@link SubmissionProducts }
     * 
     */
    public SubmissionProducts createSubmissionProducts() {
        return new SubmissionProducts();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProductStatuses }
     * 
     */
    public ArrayOfSubmissionProductStatuses createArrayOfSubmissionProductStatuses() {
        return new ArrayOfSubmissionProductStatuses();
    }

    /**
     * Create an instance of {@link SubmissionProductStatuses }
     * 
     */
    public SubmissionProductStatuses createSubmissionProductStatuses() {
        return new SubmissionProductStatuses();
    }

    /**
     * Create an instance of {@link ArrayOfPolicys }
     * 
     */
    public ArrayOfPolicys createArrayOfPolicys() {
        return new ArrayOfPolicys();
    }

    /**
     * Create an instance of {@link Policys }
     * 
     */
    public Policys createPolicys() {
        return new Policys();
    }

    /**
     * Create an instance of {@link ArrayOfProducts }
     * 
     */
    public ArrayOfProducts createArrayOfProducts() {
        return new ArrayOfProducts();
    }

    /**
     * Create an instance of {@link Products }
     * 
     */
    public Products createProducts() {
        return new Products();
    }

    /**
     * Create an instance of {@link ArrayOfAgreements }
     * 
     */
    public ArrayOfAgreements createArrayOfAgreements() {
        return new ArrayOfAgreements();
    }

    /**
     * Create an instance of {@link Agreements }
     * 
     */
    public Agreements createAgreements() {
        return new Agreements();
    }

    /**
     * Create an instance of {@link InsuredDetails }
     * 
     */
    public InsuredDetails createInsuredDetails() {
        return new InsuredDetails();
    }

    /**
     * Create an instance of {@link ArrayOfInsuredDetails }
     * 
     */
    public ArrayOfInsuredDetails createArrayOfInsuredDetails() {
        return new ArrayOfInsuredDetails();
    }

    /**
     * Create an instance of {@link CreateAccount2 }
     * 
     */
    public CreateAccount2 createCreateAccount2() {
        return new CreateAccount2();
    }

    /**
     * Create an instance of {@link ArrayOfCreateAccount }
     * 
     */
    public ArrayOfCreateAccount createArrayOfCreateAccount() {
        return new ArrayOfCreateAccount();
    }

    /**
     * Create an instance of {@link AutoTechPricRequest }
     * 
     */
    public AutoTechPricRequest createAutoTechPricRequest() {
        return new AutoTechPricRequest();
    }

    /**
     * Create an instance of {@link ArrayOfAutoLoss }
     * 
     */
    public ArrayOfAutoLoss createArrayOfAutoLoss() {
        return new ArrayOfAutoLoss();
    }

    /**
     * Create an instance of {@link AutoLoss }
     * 
     */
    public AutoLoss createAutoLoss() {
        return new AutoLoss();
    }

    /**
     * Create an instance of {@link AutoTechPricResponse }
     * 
     */
    public AutoTechPricResponse createAutoTechPricResponse() {
        return new AutoTechPricResponse();
    }

    /**
     * Create an instance of {@link ArrayOfInt }
     * 
     */
    public ArrayOfInt createArrayOfInt() {
        return new ArrayOfInt();
    }

    /**
     * Create an instance of {@link ArrayOfPolicySymbol }
     * 
     */
    public ArrayOfPolicySymbol createArrayOfPolicySymbol() {
        return new ArrayOfPolicySymbol();
    }

    /**
     * Create an instance of {@link PolicySymbol }
     * 
     */
    public PolicySymbol createPolicySymbol() {
        return new PolicySymbol();
    }

    /**
     * Create an instance of {@link WCTargetPriceRequest }
     * 
     */
    public WCTargetPriceRequest createWCTargetPriceRequest() {
        return new WCTargetPriceRequest();
    }

    /**
     * Create an instance of {@link InsurancePolicy }
     * 
     */
    public InsurancePolicy createInsurancePolicy() {
        return new InsurancePolicy();
    }

    /**
     * Create an instance of {@link PremiumSummary }
     * 
     */
    public PremiumSummary createPremiumSummary() {
        return new PremiumSummary();
    }

    /**
     * Create an instance of {@link ArrayOfPremiumSummaryStates }
     * 
     */
    public ArrayOfPremiumSummaryStates createArrayOfPremiumSummaryStates() {
        return new ArrayOfPremiumSummaryStates();
    }

    /**
     * Create an instance of {@link PremiumSummaryStates }
     * 
     */
    public PremiumSummaryStates createPremiumSummaryStates() {
        return new PremiumSummaryStates();
    }

    /**
     * Create an instance of {@link ArrayOfPremiumSummaryClassCode }
     * 
     */
    public ArrayOfPremiumSummaryClassCode createArrayOfPremiumSummaryClassCode() {
        return new ArrayOfPremiumSummaryClassCode();
    }

    /**
     * Create an instance of {@link PremiumSummaryClassCode }
     * 
     */
    public PremiumSummaryClassCode createPremiumSummaryClassCode() {
        return new PremiumSummaryClassCode();
    }

    /**
     * Create an instance of {@link ExposureAmount }
     * 
     */
    public ExposureAmount createExposureAmount() {
        return new ExposureAmount();
    }

    /**
     * Create an instance of {@link PremiumAmount }
     * 
     */
    public PremiumAmount createPremiumAmount() {
        return new PremiumAmount();
    }

    /**
     * Create an instance of {@link WCTargetPriceResponseData }
     * 
     */
    public WCTargetPriceResponseData createWCTargetPriceResponseData() {
        return new WCTargetPriceResponseData();
    }

    /**
     * Create an instance of {@link WCTargetPrice2 }
     * 
     */
    public WCTargetPrice2 createWCTargetPrice2() {
        return new WCTargetPrice2();
    }

    /**
     * Create an instance of {@link ArrayOfGetAgreementsGivenPolicyNumber }
     * 
     */
    public ArrayOfGetAgreementsGivenPolicyNumber createArrayOfGetAgreementsGivenPolicyNumber() {
        return new ArrayOfGetAgreementsGivenPolicyNumber();
    }

    /**
     * Create an instance of {@link GetAgreementsGivenPolicyNumber2 }
     * 
     */
    public GetAgreementsGivenPolicyNumber2 createGetAgreementsGivenPolicyNumber2() {
        return new GetAgreementsGivenPolicyNumber2();
    }

    /**
     * Create an instance of {@link WFMCustomer }
     * 
     */
    public WFMCustomer createWFMCustomer() {
        return new WFMCustomer();
    }

    /**
     * Create an instance of {@link ArrayOfWFMPolicy }
     * 
     */
    public ArrayOfWFMPolicy createArrayOfWFMPolicy() {
        return new ArrayOfWFMPolicy();
    }

    /**
     * Create an instance of {@link WFMPolicy }
     * 
     */
    public WFMPolicy createWFMPolicy() {
        return new WFMPolicy();
    }

    /**
     * Create an instance of {@link RenewedPolicy }
     * 
     */
    public RenewedPolicy createRenewedPolicy() {
        return new RenewedPolicy();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionDetails }
     * 
     */
    public ArrayOfSubmissionDetails createArrayOfSubmissionDetails() {
        return new ArrayOfSubmissionDetails();
    }

    /**
     * Create an instance of {@link SubmissionDetails }
     * 
     */
    public SubmissionDetails createSubmissionDetails() {
        return new SubmissionDetails();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProductDetail }
     * 
     */
    public ArrayOfSubmissionProductDetail createArrayOfSubmissionProductDetail() {
        return new ArrayOfSubmissionProductDetail();
    }

    /**
     * Create an instance of {@link SubmissionProductDetail }
     * 
     */
    public SubmissionProductDetail createSubmissionProductDetail() {
        return new SubmissionProductDetail();
    }

    /**
     * Create an instance of {@link AddModifyAccount }
     * 
     */
    public AddModifyAccount createAddModifyAccount() {
        return new AddModifyAccount();
    }

    /**
     * Create an instance of {@link WCCATModellingResponseData }
     * 
     */
    public WCCATModellingResponseData createWCCATModellingResponseData() {
        return new WCCATModellingResponseData();
    }

    /**
     * Create an instance of {@link ArrayOfWCCATModellingResult }
     * 
     */
    public ArrayOfWCCATModellingResult createArrayOfWCCATModellingResult() {
        return new ArrayOfWCCATModellingResult();
    }

    /**
     * Create an instance of {@link WCCATModellingResult }
     * 
     */
    public WCCATModellingResult createWCCATModellingResult() {
        return new WCCATModellingResult();
    }

    /**
     * Create an instance of {@link ArrayOfTargetPriceRateTrackDtls }
     * 
     */
    public ArrayOfTargetPriceRateTrackDtls createArrayOfTargetPriceRateTrackDtls() {
        return new ArrayOfTargetPriceRateTrackDtls();
    }

    /**
     * Create an instance of {@link TargetPriceRateTrackDtls }
     * 
     */
    public TargetPriceRateTrackDtls createTargetPriceRateTrackDtls() {
        return new TargetPriceRateTrackDtls();
    }

    /**
     * Create an instance of {@link ArrayOfProductDetails }
     * 
     */
    public ArrayOfProductDetails createArrayOfProductDetails() {
        return new ArrayOfProductDetails();
    }

    /**
     * Create an instance of {@link ProductDetails }
     * 
     */
    public ProductDetails createProductDetails() {
        return new ProductDetails();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProductResult }
     * 
     */
    public ArrayOfSubmissionProductResult createArrayOfSubmissionProductResult() {
        return new ArrayOfSubmissionProductResult();
    }

    /**
     * Create an instance of {@link SubmissionProductResult }
     * 
     */
    public SubmissionProductResult createSubmissionProductResult() {
        return new SubmissionProductResult();
    }

    /**
     * Create an instance of {@link ArrayOfClearanceResult }
     * 
     */
    public ArrayOfClearanceResult createArrayOfClearanceResult() {
        return new ArrayOfClearanceResult();
    }

    /**
     * Create an instance of {@link ClearanceResult }
     * 
     */
    public ClearanceResult createClearanceResult() {
        return new ClearanceResult();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProductList }
     * 
     */
    public ArrayOfSubmissionProductList createArrayOfSubmissionProductList() {
        return new ArrayOfSubmissionProductList();
    }

    /**
     * Create an instance of {@link SubmissionProductList }
     * 
     */
    public SubmissionProductList createSubmissionProductList() {
        return new SubmissionProductList();
    }

    /**
     * Create an instance of {@link ArrayOfProductResult }
     * 
     */
    public ArrayOfProductResult createArrayOfProductResult() {
        return new ArrayOfProductResult();
    }

    /**
     * Create an instance of {@link ProductResult }
     * 
     */
    public ProductResult createProductResult() {
        return new ProductResult();
    }

    /**
     * Create an instance of {@link ArrayOfUnderwritingAssts }
     * 
     */
    public ArrayOfUnderwritingAssts createArrayOfUnderwritingAssts() {
        return new ArrayOfUnderwritingAssts();
    }

    /**
     * Create an instance of {@link UnderwritingAssts }
     * 
     */
    public UnderwritingAssts createUnderwritingAssts() {
        return new UnderwritingAssts();
    }

    /**
     * Create an instance of {@link ArrayOfUnderwriters }
     * 
     */
    public ArrayOfUnderwriters createArrayOfUnderwriters() {
        return new ArrayOfUnderwriters();
    }

    /**
     * Create an instance of {@link Underwriters }
     * 
     */
    public Underwriters createUnderwriters() {
        return new Underwriters();
    }

    /**
     * Create an instance of {@link ArrayOfProdDetails }
     * 
     */
    public ArrayOfProdDetails createArrayOfProdDetails() {
        return new ArrayOfProdDetails();
    }

    /**
     * Create an instance of {@link ProdDetails }
     * 
     */
    public ProdDetails createProdDetails() {
        return new ProdDetails();
    }

    /**
     * Create an instance of {@link ArrayOfOrgs }
     * 
     */
    public ArrayOfOrgs createArrayOfOrgs() {
        return new ArrayOfOrgs();
    }

    /**
     * Create an instance of {@link Orgs }
     * 
     */
    public Orgs createOrgs() {
        return new Orgs();
    }

    /**
     * Create an instance of {@link ArrayOfUnderwritingPrograms }
     * 
     */
    public ArrayOfUnderwritingPrograms createArrayOfUnderwritingPrograms() {
        return new ArrayOfUnderwritingPrograms();
    }

    /**
     * Create an instance of {@link UnderwritingPrograms }
     * 
     */
    public UnderwritingPrograms createUnderwritingPrograms() {
        return new UnderwritingPrograms();
    }

    /**
     * Create an instance of {@link SubmissionName }
     * 
     */
    public SubmissionName createSubmissionName() {
        return new SubmissionName();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionNameAdditionalInformation }
     * 
     */
    public ArrayOfSubmissionNameAdditionalInformation createArrayOfSubmissionNameAdditionalInformation() {
        return new ArrayOfSubmissionNameAdditionalInformation();
    }

    /**
     * Create an instance of {@link SubmissionNameAdditionalInformation }
     * 
     */
    public SubmissionNameAdditionalInformation createSubmissionNameAdditionalInformation() {
        return new SubmissionNameAdditionalInformation();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionV2 }
     * 
     */
    public ArrayOfSubmissionV2 createArrayOfSubmissionV2() {
        return new ArrayOfSubmissionV2();
    }

    /**
     * Create an instance of {@link SubmissionV2 }
     * 
     */
    public SubmissionV2 createSubmissionV2() {
        return new SubmissionV2();
    }

    /**
     * Create an instance of {@link ArrayOfProjectName }
     * 
     */
    public ArrayOfProjectName createArrayOfProjectName() {
        return new ArrayOfProjectName();
    }

    /**
     * Create an instance of {@link ProjectName }
     * 
     */
    public ProjectName createProjectName() {
        return new ProjectName();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProductV2 }
     * 
     */
    public ArrayOfSubmissionProductV2 createArrayOfSubmissionProductV2() {
        return new ArrayOfSubmissionProductV2();
    }

    /**
     * Create an instance of {@link SubmissionProductV2 }
     * 
     */
    public SubmissionProductV2 createSubmissionProductV2() {
        return new SubmissionProductV2();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProductStatusV2 }
     * 
     */
    public ArrayOfSubmissionProductStatusV2 createArrayOfSubmissionProductStatusV2() {
        return new ArrayOfSubmissionProductStatusV2();
    }

    /**
     * Create an instance of {@link SubmissionProductStatusV2 }
     * 
     */
    public SubmissionProductStatusV2 createSubmissionProductStatusV2() {
        return new SubmissionProductStatusV2();
    }

    /**
     * Create an instance of {@link SubmissionBindV2 }
     * 
     */
    public SubmissionBindV2 createSubmissionBindV2() {
        return new SubmissionBindV2();
    }

    /**
     * Create an instance of {@link ArrayOfSubmissionProductsV2 }
     * 
     */
    public ArrayOfSubmissionProductsV2 createArrayOfSubmissionProductsV2() {
        return new ArrayOfSubmissionProductsV2();
    }

    /**
     * Create an instance of {@link SubmissionProductsV2 }
     * 
     */
    public SubmissionProductsV2 createSubmissionProductsV2() {
        return new SubmissionProductsV2();
    }

}
