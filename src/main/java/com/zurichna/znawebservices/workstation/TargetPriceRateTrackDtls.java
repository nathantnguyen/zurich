
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TargetPriceRateTrackDtls complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TargetPriceRateTrackDtls"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InsuredName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LOBEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="LOBID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LineOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ManualPremium" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="TargetPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="FinalTargetPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CurrentTermWrittenPremium" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ExpiringTermWrittenPremium" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CurrentTermManualPremium" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ExpiringTermManualPremium" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CurrentTermAnnualExposures" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="ExpiringTermAnnualExposures" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="CurrentTermLimit" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ExpiringTermLimit" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CurrentTermDeductible" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ExpiringTermDeductible" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CurrentTermCommission" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ExpiringTermCommission" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CurrentTermExperienceModFctr" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ExpiringTermExperienceModFctr" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="RateChange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PolicySymbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PolicyMod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetPriceRateTrackDtls", propOrder = {
    "insuredName",
    "lobEffectiveDate",
    "lobid",
    "lineOfBusiness",
    "manualPremium",
    "targetPrice",
    "finalTargetPrice",
    "currentTermWrittenPremium",
    "expiringTermWrittenPremium",
    "currentTermManualPremium",
    "expiringTermManualPremium",
    "currentTermAnnualExposures",
    "expiringTermAnnualExposures",
    "currentTermLimit",
    "expiringTermLimit",
    "currentTermDeductible",
    "expiringTermDeductible",
    "currentTermCommission",
    "expiringTermCommission",
    "currentTermExperienceModFctr",
    "expiringTermExperienceModFctr",
    "rateChange",
    "policyNumber",
    "policySymbol",
    "policyMod"
})
public class TargetPriceRateTrackDtls {

    @XmlElement(name = "InsuredName")
    protected String insuredName;
    @XmlElement(name = "LOBEffectiveDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lobEffectiveDate;
    @XmlElement(name = "LOBID")
    protected int lobid;
    @XmlElement(name = "LineOfBusiness")
    protected String lineOfBusiness;
    @XmlElement(name = "ManualPremium", required = true)
    protected BigDecimal manualPremium;
    @XmlElement(name = "TargetPrice", required = true)
    protected BigDecimal targetPrice;
    @XmlElement(name = "FinalTargetPrice", required = true)
    protected BigDecimal finalTargetPrice;
    @XmlElement(name = "CurrentTermWrittenPremium")
    protected int currentTermWrittenPremium;
    @XmlElement(name = "ExpiringTermWrittenPremium")
    protected int expiringTermWrittenPremium;
    @XmlElement(name = "CurrentTermManualPremium")
    protected int currentTermManualPremium;
    @XmlElement(name = "ExpiringTermManualPremium")
    protected int expiringTermManualPremium;
    @XmlElement(name = "CurrentTermAnnualExposures")
    protected long currentTermAnnualExposures;
    @XmlElement(name = "ExpiringTermAnnualExposures")
    protected long expiringTermAnnualExposures;
    @XmlElement(name = "CurrentTermLimit")
    protected int currentTermLimit;
    @XmlElement(name = "ExpiringTermLimit")
    protected int expiringTermLimit;
    @XmlElement(name = "CurrentTermDeductible")
    protected int currentTermDeductible;
    @XmlElement(name = "ExpiringTermDeductible")
    protected int expiringTermDeductible;
    @XmlElement(name = "CurrentTermCommission", required = true)
    protected BigDecimal currentTermCommission;
    @XmlElement(name = "ExpiringTermCommission", required = true)
    protected BigDecimal expiringTermCommission;
    @XmlElement(name = "CurrentTermExperienceModFctr", required = true)
    protected BigDecimal currentTermExperienceModFctr;
    @XmlElement(name = "ExpiringTermExperienceModFctr", required = true)
    protected BigDecimal expiringTermExperienceModFctr;
    @XmlElement(name = "RateChange")
    protected String rateChange;
    @XmlElement(name = "PolicyNumber")
    protected String policyNumber;
    @XmlElement(name = "PolicySymbol")
    protected String policySymbol;
    @XmlElement(name = "PolicyMod")
    protected String policyMod;

    /**
     * Gets the value of the insuredName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredName() {
        return insuredName;
    }

    /**
     * Sets the value of the insuredName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredName(String value) {
        this.insuredName = value;
    }

    /**
     * Gets the value of the lobEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLOBEffectiveDate() {
        return lobEffectiveDate;
    }

    /**
     * Sets the value of the lobEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLOBEffectiveDate(XMLGregorianCalendar value) {
        this.lobEffectiveDate = value;
    }

    /**
     * Gets the value of the lobid property.
     * 
     */
    public int getLOBID() {
        return lobid;
    }

    /**
     * Sets the value of the lobid property.
     * 
     */
    public void setLOBID(int value) {
        this.lobid = value;
    }

    /**
     * Gets the value of the lineOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    /**
     * Sets the value of the lineOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineOfBusiness(String value) {
        this.lineOfBusiness = value;
    }

    /**
     * Gets the value of the manualPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getManualPremium() {
        return manualPremium;
    }

    /**
     * Sets the value of the manualPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setManualPremium(BigDecimal value) {
        this.manualPremium = value;
    }

    /**
     * Gets the value of the targetPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTargetPrice() {
        return targetPrice;
    }

    /**
     * Sets the value of the targetPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTargetPrice(BigDecimal value) {
        this.targetPrice = value;
    }

    /**
     * Gets the value of the finalTargetPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFinalTargetPrice() {
        return finalTargetPrice;
    }

    /**
     * Sets the value of the finalTargetPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFinalTargetPrice(BigDecimal value) {
        this.finalTargetPrice = value;
    }

    /**
     * Gets the value of the currentTermWrittenPremium property.
     * 
     */
    public int getCurrentTermWrittenPremium() {
        return currentTermWrittenPremium;
    }

    /**
     * Sets the value of the currentTermWrittenPremium property.
     * 
     */
    public void setCurrentTermWrittenPremium(int value) {
        this.currentTermWrittenPremium = value;
    }

    /**
     * Gets the value of the expiringTermWrittenPremium property.
     * 
     */
    public int getExpiringTermWrittenPremium() {
        return expiringTermWrittenPremium;
    }

    /**
     * Sets the value of the expiringTermWrittenPremium property.
     * 
     */
    public void setExpiringTermWrittenPremium(int value) {
        this.expiringTermWrittenPremium = value;
    }

    /**
     * Gets the value of the currentTermManualPremium property.
     * 
     */
    public int getCurrentTermManualPremium() {
        return currentTermManualPremium;
    }

    /**
     * Sets the value of the currentTermManualPremium property.
     * 
     */
    public void setCurrentTermManualPremium(int value) {
        this.currentTermManualPremium = value;
    }

    /**
     * Gets the value of the expiringTermManualPremium property.
     * 
     */
    public int getExpiringTermManualPremium() {
        return expiringTermManualPremium;
    }

    /**
     * Sets the value of the expiringTermManualPremium property.
     * 
     */
    public void setExpiringTermManualPremium(int value) {
        this.expiringTermManualPremium = value;
    }

    /**
     * Gets the value of the currentTermAnnualExposures property.
     * 
     */
    public long getCurrentTermAnnualExposures() {
        return currentTermAnnualExposures;
    }

    /**
     * Sets the value of the currentTermAnnualExposures property.
     * 
     */
    public void setCurrentTermAnnualExposures(long value) {
        this.currentTermAnnualExposures = value;
    }

    /**
     * Gets the value of the expiringTermAnnualExposures property.
     * 
     */
    public long getExpiringTermAnnualExposures() {
        return expiringTermAnnualExposures;
    }

    /**
     * Sets the value of the expiringTermAnnualExposures property.
     * 
     */
    public void setExpiringTermAnnualExposures(long value) {
        this.expiringTermAnnualExposures = value;
    }

    /**
     * Gets the value of the currentTermLimit property.
     * 
     */
    public int getCurrentTermLimit() {
        return currentTermLimit;
    }

    /**
     * Sets the value of the currentTermLimit property.
     * 
     */
    public void setCurrentTermLimit(int value) {
        this.currentTermLimit = value;
    }

    /**
     * Gets the value of the expiringTermLimit property.
     * 
     */
    public int getExpiringTermLimit() {
        return expiringTermLimit;
    }

    /**
     * Sets the value of the expiringTermLimit property.
     * 
     */
    public void setExpiringTermLimit(int value) {
        this.expiringTermLimit = value;
    }

    /**
     * Gets the value of the currentTermDeductible property.
     * 
     */
    public int getCurrentTermDeductible() {
        return currentTermDeductible;
    }

    /**
     * Sets the value of the currentTermDeductible property.
     * 
     */
    public void setCurrentTermDeductible(int value) {
        this.currentTermDeductible = value;
    }

    /**
     * Gets the value of the expiringTermDeductible property.
     * 
     */
    public int getExpiringTermDeductible() {
        return expiringTermDeductible;
    }

    /**
     * Sets the value of the expiringTermDeductible property.
     * 
     */
    public void setExpiringTermDeductible(int value) {
        this.expiringTermDeductible = value;
    }

    /**
     * Gets the value of the currentTermCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrentTermCommission() {
        return currentTermCommission;
    }

    /**
     * Sets the value of the currentTermCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrentTermCommission(BigDecimal value) {
        this.currentTermCommission = value;
    }

    /**
     * Gets the value of the expiringTermCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExpiringTermCommission() {
        return expiringTermCommission;
    }

    /**
     * Sets the value of the expiringTermCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExpiringTermCommission(BigDecimal value) {
        this.expiringTermCommission = value;
    }

    /**
     * Gets the value of the currentTermExperienceModFctr property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrentTermExperienceModFctr() {
        return currentTermExperienceModFctr;
    }

    /**
     * Sets the value of the currentTermExperienceModFctr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrentTermExperienceModFctr(BigDecimal value) {
        this.currentTermExperienceModFctr = value;
    }

    /**
     * Gets the value of the expiringTermExperienceModFctr property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExpiringTermExperienceModFctr() {
        return expiringTermExperienceModFctr;
    }

    /**
     * Sets the value of the expiringTermExperienceModFctr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExpiringTermExperienceModFctr(BigDecimal value) {
        this.expiringTermExperienceModFctr = value;
    }

    /**
     * Gets the value of the rateChange property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateChange() {
        return rateChange;
    }

    /**
     * Sets the value of the rateChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateChange(String value) {
        this.rateChange = value;
    }

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the policySymbol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicySymbol() {
        return policySymbol;
    }

    /**
     * Sets the value of the policySymbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicySymbol(String value) {
        this.policySymbol = value;
    }

    /**
     * Gets the value of the policyMod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyMod() {
        return policyMod;
    }

    /**
     * Sets the value of the policyMod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyMod(String value) {
        this.policyMod = value;
    }

}
