
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PremiumSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PremiumSummary"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="premSummStates" type="{http://workstation.znawebservices.zurichna.com}ArrayOfPremiumSummaryStates" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PremiumSummary", propOrder = {
    "premSummStates"
})
public class PremiumSummary {

    protected ArrayOfPremiumSummaryStates premSummStates;

    /**
     * Gets the value of the premSummStates property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPremiumSummaryStates }
     *     
     */
    public ArrayOfPremiumSummaryStates getPremSummStates() {
        return premSummStates;
    }

    /**
     * Sets the value of the premSummStates property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPremiumSummaryStates }
     *     
     */
    public void setPremSummStates(ArrayOfPremiumSummaryStates value) {
        this.premSummStates = value;
    }

}
