
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetAgreementsGivenPolicyNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetAgreementsGivenPolicyNumber"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="MODU_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SELCT_RENL_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SELCT_NRENL_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetAgreementsGivenPolicyNumber", propOrder = {
    "agmtid",
    "modunbr",
    "newrenlcd",
    "polnbr",
    "polsym",
    "smsnid",
    "selctrenlind",
    "selctnrenlind"
})
public class GetAgreementsGivenPolicyNumber2 {

    @XmlElement(name = "AGMT_ID")
    protected int agmtid;
    @XmlElement(name = "MODU_NBR")
    protected String modunbr;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "POL_NBR")
    protected String polnbr;
    @XmlElement(name = "POL_SYM")
    protected String polsym;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "SELCT_RENL_IND")
    protected String selctrenlind;
    @XmlElement(name = "SELCT_NRENL_IND")
    protected String selctnrenlind;

    /**
     * Gets the value of the agmtid property.
     * 
     */
    public int getAGMTID() {
        return agmtid;
    }

    /**
     * Sets the value of the agmtid property.
     * 
     */
    public void setAGMTID(int value) {
        this.agmtid = value;
    }

    /**
     * Gets the value of the modunbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODUNBR() {
        return modunbr;
    }

    /**
     * Sets the value of the modunbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODUNBR(String value) {
        this.modunbr = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the polnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLNBR() {
        return polnbr;
    }

    /**
     * Sets the value of the polnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLNBR(String value) {
        this.polnbr = value;
    }

    /**
     * Gets the value of the polsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLSYM() {
        return polsym;
    }

    /**
     * Sets the value of the polsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLSYM(String value) {
        this.polsym = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the selctrenlind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSELCTRENLIND() {
        return selctrenlind;
    }

    /**
     * Sets the value of the selctrenlind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSELCTRENLIND(String value) {
        this.selctrenlind = value;
    }

    /**
     * Gets the value of the selctnrenlind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSELCTNRENLIND() {
        return selctnrenlind;
    }

    /**
     * Sets the value of the selctnrenlind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSELCTNRENLIND(String value) {
        this.selctnrenlind = value;
    }

}
