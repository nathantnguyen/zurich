
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateSubmissionV2Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateSubmissionV2Request"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="submissionName" type="{http://workstation.znawebservices.zurichna.com}SubmissionName" minOccurs="0"/&gt;
 *         &lt;element name="submissionV2" type="{http://workstation.znawebservices.zurichna.com}SubmissionV2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateSubmissionV2Request", propOrder = {
    "submissionName",
    "submissionV2"
})
public class UpdateSubmissionV2Request
    extends RequestBase
{

    protected SubmissionName submissionName;
    protected SubmissionV2 submissionV2;

    /**
     * Gets the value of the submissionName property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionName }
     *     
     */
    public SubmissionName getSubmissionName() {
        return submissionName;
    }

    /**
     * Sets the value of the submissionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionName }
     *     
     */
    public void setSubmissionName(SubmissionName value) {
        this.submissionName = value;
    }

    /**
     * Gets the value of the submissionV2 property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionV2 }
     *     
     */
    public SubmissionV2 getSubmissionV2() {
        return submissionV2;
    }

    /**
     * Sets the value of the submissionV2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionV2 }
     *     
     */
    public void setSubmissionV2(SubmissionV2 value) {
        this.submissionV2 = value;
    }

}
