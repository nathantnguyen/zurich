
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetAgreementsGivenPolicyNumberResult" type="{http://workstation.znawebservices.zurichna.com}GetAgreementsGivenPolicyNumberResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAgreementsGivenPolicyNumberResult"
})
@XmlRootElement(name = "GetAgreementsGivenPolicyNumberResponse")
public class GetAgreementsGivenPolicyNumberResponse {

    @XmlElement(name = "GetAgreementsGivenPolicyNumberResult")
    protected GetAgreementsGivenPolicyNumberResponse2 getAgreementsGivenPolicyNumberResult;

    /**
     * Gets the value of the getAgreementsGivenPolicyNumberResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetAgreementsGivenPolicyNumberResponse2 }
     *     
     */
    public GetAgreementsGivenPolicyNumberResponse2 getGetAgreementsGivenPolicyNumberResult() {
        return getAgreementsGivenPolicyNumberResult;
    }

    /**
     * Sets the value of the getAgreementsGivenPolicyNumberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAgreementsGivenPolicyNumberResponse2 }
     *     
     */
    public void setGetAgreementsGivenPolicyNumberResult(GetAgreementsGivenPolicyNumberResponse2 value) {
        this.getAgreementsGivenPolicyNumberResult = value;
    }

}
