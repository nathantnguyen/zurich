
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerSearchResults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerSearchResults"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUST_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUST_CRMS_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LST_ALRT_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="LST_ALRT_TTL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TRD_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SITE_DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ENPRS_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FULL_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEP_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AREA_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="XCH_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TE4_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TEL_EXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FULL_TELC_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FRGN_TELC_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRI_SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEC_SIC_CDS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUSTOMER_SOURCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerSearchResults", propOrder = {
    "custptyid",
    "custcrmsptyid",
    "lstalrtind",
    "lstalrtttl",
    "nm",
    "trdnm",
    "dunsnbr",
    "sitedunsnbr",
    "enprsid",
    "fulladdr",
    "addr",
    "depaddr",
    "ctynm",
    "stabbr",
    "zipcd",
    "ctrycd",
    "ctrynm",
    "areacd",
    "xchnbr",
    "te4NBR",
    "telext",
    "fulltelcnbr",
    "frgntelcnbr",
    "prisiccd",
    "secsiccds",
    "customersource"
})
public class CustomerSearchResults {

    @XmlElement(name = "CUST_PTY_ID")
    protected int custptyid;
    @XmlElement(name = "CUST_CRMS_PTY_ID")
    protected int custcrmsptyid;
    @XmlElement(name = "LST_ALRT_IND")
    protected boolean lstalrtind;
    @XmlElement(name = "LST_ALRT_TTL")
    protected String lstalrtttl;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "TRD_NM")
    protected String trdnm;
    @XmlElement(name = "DUNS_NBR")
    protected String dunsnbr;
    @XmlElement(name = "SITE_DUNS_NBR")
    protected String sitedunsnbr;
    @XmlElement(name = "ENPRS_ID")
    protected String enprsid;
    @XmlElement(name = "FULL_ADDR")
    protected String fulladdr;
    @XmlElement(name = "ADDR")
    protected String addr;
    @XmlElement(name = "DEP_ADDR")
    protected String depaddr;
    @XmlElement(name = "CTY_NM")
    protected String ctynm;
    @XmlElement(name = "ST_ABBR")
    protected String stabbr;
    @XmlElement(name = "ZIP_CD")
    protected String zipcd;
    @XmlElement(name = "CTRY_CD")
    protected String ctrycd;
    @XmlElement(name = "CTRY_NM")
    protected String ctrynm;
    @XmlElement(name = "AREA_CD")
    protected String areacd;
    @XmlElement(name = "XCH_NBR")
    protected String xchnbr;
    @XmlElement(name = "TE4_NBR")
    protected String te4NBR;
    @XmlElement(name = "TEL_EXT")
    protected String telext;
    @XmlElement(name = "FULL_TELC_NBR")
    protected String fulltelcnbr;
    @XmlElement(name = "FRGN_TELC_NBR")
    protected String frgntelcnbr;
    @XmlElement(name = "PRI_SIC_CD")
    protected String prisiccd;
    @XmlElement(name = "SEC_SIC_CDS")
    protected String secsiccds;
    @XmlElement(name = "CUSTOMER_SOURCE")
    protected String customersource;

    /**
     * Gets the value of the custptyid property.
     * 
     */
    public int getCUSTPTYID() {
        return custptyid;
    }

    /**
     * Sets the value of the custptyid property.
     * 
     */
    public void setCUSTPTYID(int value) {
        this.custptyid = value;
    }

    /**
     * Gets the value of the custcrmsptyid property.
     * 
     */
    public int getCUSTCRMSPTYID() {
        return custcrmsptyid;
    }

    /**
     * Sets the value of the custcrmsptyid property.
     * 
     */
    public void setCUSTCRMSPTYID(int value) {
        this.custcrmsptyid = value;
    }

    /**
     * Gets the value of the lstalrtind property.
     * 
     */
    public boolean isLSTALRTIND() {
        return lstalrtind;
    }

    /**
     * Sets the value of the lstalrtind property.
     * 
     */
    public void setLSTALRTIND(boolean value) {
        this.lstalrtind = value;
    }

    /**
     * Gets the value of the lstalrtttl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLSTALRTTTL() {
        return lstalrtttl;
    }

    /**
     * Sets the value of the lstalrtttl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLSTALRTTTL(String value) {
        this.lstalrtttl = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the trdnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRDNM() {
        return trdnm;
    }

    /**
     * Sets the value of the trdnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRDNM(String value) {
        this.trdnm = value;
    }

    /**
     * Gets the value of the dunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNSNBR() {
        return dunsnbr;
    }

    /**
     * Sets the value of the dunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNSNBR(String value) {
        this.dunsnbr = value;
    }

    /**
     * Gets the value of the sitedunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSITEDUNSNBR() {
        return sitedunsnbr;
    }

    /**
     * Sets the value of the sitedunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSITEDUNSNBR(String value) {
        this.sitedunsnbr = value;
    }

    /**
     * Gets the value of the enprsid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENPRSID() {
        return enprsid;
    }

    /**
     * Sets the value of the enprsid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENPRSID(String value) {
        this.enprsid = value;
    }

    /**
     * Gets the value of the fulladdr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFULLADDR() {
        return fulladdr;
    }

    /**
     * Sets the value of the fulladdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFULLADDR(String value) {
        this.fulladdr = value;
    }

    /**
     * Gets the value of the addr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDR() {
        return addr;
    }

    /**
     * Sets the value of the addr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDR(String value) {
        this.addr = value;
    }

    /**
     * Gets the value of the depaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEPADDR() {
        return depaddr;
    }

    /**
     * Sets the value of the depaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEPADDR(String value) {
        this.depaddr = value;
    }

    /**
     * Gets the value of the ctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTYNM() {
        return ctynm;
    }

    /**
     * Sets the value of the ctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTYNM(String value) {
        this.ctynm = value;
    }

    /**
     * Gets the value of the stabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTABBR() {
        return stabbr;
    }

    /**
     * Sets the value of the stabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTABBR(String value) {
        this.stabbr = value;
    }

    /**
     * Gets the value of the zipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIPCD() {
        return zipcd;
    }

    /**
     * Sets the value of the zipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIPCD(String value) {
        this.zipcd = value;
    }

    /**
     * Gets the value of the ctrycd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYCD() {
        return ctrycd;
    }

    /**
     * Sets the value of the ctrycd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYCD(String value) {
        this.ctrycd = value;
    }

    /**
     * Gets the value of the ctrynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYNM() {
        return ctrynm;
    }

    /**
     * Sets the value of the ctrynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYNM(String value) {
        this.ctrynm = value;
    }

    /**
     * Gets the value of the areacd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAREACD() {
        return areacd;
    }

    /**
     * Sets the value of the areacd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAREACD(String value) {
        this.areacd = value;
    }

    /**
     * Gets the value of the xchnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXCHNBR() {
        return xchnbr;
    }

    /**
     * Sets the value of the xchnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXCHNBR(String value) {
        this.xchnbr = value;
    }

    /**
     * Gets the value of the te4NBR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTE4NBR() {
        return te4NBR;
    }

    /**
     * Sets the value of the te4NBR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTE4NBR(String value) {
        this.te4NBR = value;
    }

    /**
     * Gets the value of the telext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTELEXT() {
        return telext;
    }

    /**
     * Sets the value of the telext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTELEXT(String value) {
        this.telext = value;
    }

    /**
     * Gets the value of the fulltelcnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFULLTELCNBR() {
        return fulltelcnbr;
    }

    /**
     * Sets the value of the fulltelcnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFULLTELCNBR(String value) {
        this.fulltelcnbr = value;
    }

    /**
     * Gets the value of the frgntelcnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRGNTELCNBR() {
        return frgntelcnbr;
    }

    /**
     * Sets the value of the frgntelcnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRGNTELCNBR(String value) {
        this.frgntelcnbr = value;
    }

    /**
     * Gets the value of the prisiccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRISICCD() {
        return prisiccd;
    }

    /**
     * Sets the value of the prisiccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRISICCD(String value) {
        this.prisiccd = value;
    }

    /**
     * Gets the value of the secsiccds property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSECSICCDS() {
        return secsiccds;
    }

    /**
     * Sets the value of the secsiccds property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSECSICCDS(String value) {
        this.secsiccds = value;
    }

    /**
     * Gets the value of the customersource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMERSOURCE() {
        return customersource;
    }

    /**
     * Sets the value of the customersource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMERSOURCE(String value) {
        this.customersource = value;
    }

}
