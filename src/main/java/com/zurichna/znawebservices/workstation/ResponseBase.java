
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="status" type="{http://workstation.znawebservices.zurichna.com}Status" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseBase", propOrder = {
    "status"
})
@XmlSeeAlso({
    QuickSearchResponse2 .class,
    SearchResponse.class,
    AdvanceZorbaSearchResponse.class,
    CustomerResponse.class,
    SubmissionResponse.class,
    GetMVRDataResponse2 .class,
    ReferenceResponse.class,
    ContactResponse.class,
    PolicyResponse.class,
    PtyResponse.class,
    AlertResponse.class,
    UpdatePolicyStatusResponse.class,
    SubmissionBindResponse.class,
    RenewPolicyInfoResponse.class,
    AddInsuredResponse2 .class,
    ModifyInsuredResponse2 .class,
    CreateAccountResponse2 .class,
    DelSMSNProductResponse.class,
    DelPolTermResponse.class,
    WCTechPriceResponse.class,
    AutoIndicationPrcResponse.class,
    GetPolicySymbolsResponse.class,
    GetAgreementsGivenPolicyNumberResponse2 .class,
    GetRMSLoginGivenSubmissionProductIdResponse2 .class,
    ZSpireSMSNIdForRenewalQuoteResponse.class,
    GetPISNCRQSTIDGvnAgmtIdBusnTranTypCdEffDtResponse2 .class,
    WFMSubmissionResponse.class,
    SMSNDetailsForGivenProducerResponse.class,
    GetTargetPriceRateTrackDtlsResponse2 .class,
    PerformAutomatedClearanceResponse2 .class,
    RetrieveUserHistoryResponse2 .class,
    SubmissionV2Response.class,
    UpdateSubmissionV2Response2 .class,
    SubmissionBindV2Response.class,
    SubmissionProductStatusResponse.class,
    UpdateProductResponse2 .class,
    WCTargetPriceResponseData.class,
    WCCATModellingResponseData.class
})
public abstract class ResponseBase {

    protected Status status;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

}
