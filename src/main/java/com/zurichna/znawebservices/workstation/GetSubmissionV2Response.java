
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetSubmissionV2Result" type="{http://workstation.znawebservices.zurichna.com}SubmissionV2Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSubmissionV2Result"
})
@XmlRootElement(name = "GetSubmissionV2Response")
public class GetSubmissionV2Response {

    @XmlElement(name = "GetSubmissionV2Result")
    protected SubmissionV2Response getSubmissionV2Result;

    /**
     * Gets the value of the getSubmissionV2Result property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionV2Response }
     *     
     */
    public SubmissionV2Response getGetSubmissionV2Result() {
        return getSubmissionV2Result;
    }

    /**
     * Sets the value of the getSubmissionV2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionV2Response }
     *     
     */
    public void setGetSubmissionV2Result(SubmissionV2Response value) {
        this.getSubmissionV2Result = value;
    }

}
