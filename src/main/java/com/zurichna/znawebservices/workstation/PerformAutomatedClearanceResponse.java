
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PerformAutomatedClearanceResult" type="{http://workstation.znawebservices.zurichna.com}PerformAutomatedClearanceResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "performAutomatedClearanceResult"
})
@XmlRootElement(name = "PerformAutomatedClearanceResponse")
public class PerformAutomatedClearanceResponse {

    @XmlElement(name = "PerformAutomatedClearanceResult")
    protected PerformAutomatedClearanceResponse2 performAutomatedClearanceResult;

    /**
     * Gets the value of the performAutomatedClearanceResult property.
     * 
     * @return
     *     possible object is
     *     {@link PerformAutomatedClearanceResponse2 }
     *     
     */
    public PerformAutomatedClearanceResponse2 getPerformAutomatedClearanceResult() {
        return performAutomatedClearanceResult;
    }

    /**
     * Sets the value of the performAutomatedClearanceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PerformAutomatedClearanceResponse2 }
     *     
     */
    public void setPerformAutomatedClearanceResult(PerformAutomatedClearanceResponse2 value) {
        this.performAutomatedClearanceResult = value;
    }

}
