
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModifyInsuredResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModifyInsuredResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Insured" type="{http://workstation.znawebservices.zurichna.com}ArrayOfInsuredDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModifyInsuredResponse", propOrder = {
    "insured"
})
public class ModifyInsuredResponse2
    extends ResponseBase
{

    @XmlElement(name = "Insured")
    protected ArrayOfInsuredDetails insured;

    /**
     * Gets the value of the insured property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInsuredDetails }
     *     
     */
    public ArrayOfInsuredDetails getInsured() {
        return insured;
    }

    /**
     * Sets the value of the insured property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInsuredDetails }
     *     
     */
    public void setInsured(ArrayOfInsuredDetails value) {
        this.insured = value;
    }

}
