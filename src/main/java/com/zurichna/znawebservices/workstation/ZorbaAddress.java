
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ZorbaAddress complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ZorbaAddress"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEP_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNTY_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_NM_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FRGN_POSTAL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEP_ADDR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTRY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZorbaAddress", propOrder = {
    "addr",
    "depaddr",
    "ctynm",
    "stabbr",
    "zipcd",
    "cntycd",
    "ctrycd",
    "ctrynmabbr",
    "frgnpostalcd",
    "depaddr2",
    "ctrynm",
    "changeState"
})
public class ZorbaAddress {

    @XmlElement(name = "ADDR")
    protected String addr;
    @XmlElement(name = "DEP_ADDR")
    protected String depaddr;
    @XmlElement(name = "CTY_NM")
    protected String ctynm;
    @XmlElement(name = "ST_ABBR")
    protected String stabbr;
    @XmlElement(name = "ZIP_CD")
    protected String zipcd;
    @XmlElement(name = "CNTY_CD")
    protected String cntycd;
    @XmlElement(name = "CTRY_CD")
    protected String ctrycd;
    @XmlElement(name = "CTRY_NM_ABBR")
    protected String ctrynmabbr;
    @XmlElement(name = "FRGN_POSTAL_CD")
    protected String frgnpostalcd;
    @XmlElement(name = "DEP_ADDR2")
    protected String depaddr2;
    @XmlElement(name = "CTRY_NM")
    protected String ctrynm;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the addr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDR() {
        return addr;
    }

    /**
     * Sets the value of the addr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDR(String value) {
        this.addr = value;
    }

    /**
     * Gets the value of the depaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEPADDR() {
        return depaddr;
    }

    /**
     * Sets the value of the depaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEPADDR(String value) {
        this.depaddr = value;
    }

    /**
     * Gets the value of the ctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTYNM() {
        return ctynm;
    }

    /**
     * Sets the value of the ctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTYNM(String value) {
        this.ctynm = value;
    }

    /**
     * Gets the value of the stabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTABBR() {
        return stabbr;
    }

    /**
     * Sets the value of the stabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTABBR(String value) {
        this.stabbr = value;
    }

    /**
     * Gets the value of the zipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIPCD() {
        return zipcd;
    }

    /**
     * Sets the value of the zipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIPCD(String value) {
        this.zipcd = value;
    }

    /**
     * Gets the value of the cntycd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNTYCD() {
        return cntycd;
    }

    /**
     * Sets the value of the cntycd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNTYCD(String value) {
        this.cntycd = value;
    }

    /**
     * Gets the value of the ctrycd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYCD() {
        return ctrycd;
    }

    /**
     * Sets the value of the ctrycd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYCD(String value) {
        this.ctrycd = value;
    }

    /**
     * Gets the value of the ctrynmabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYNMABBR() {
        return ctrynmabbr;
    }

    /**
     * Sets the value of the ctrynmabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYNMABBR(String value) {
        this.ctrynmabbr = value;
    }

    /**
     * Gets the value of the frgnpostalcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRGNPOSTALCD() {
        return frgnpostalcd;
    }

    /**
     * Sets the value of the frgnpostalcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRGNPOSTALCD(String value) {
        this.frgnpostalcd = value;
    }

    /**
     * Gets the value of the depaddr2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDEPADDR2() {
        return depaddr2;
    }

    /**
     * Sets the value of the depaddr2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEPADDR2(String value) {
        this.depaddr2 = value;
    }

    /**
     * Gets the value of the ctrynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRYNM() {
        return ctrynm;
    }

    /**
     * Sets the value of the ctrynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRYNM(String value) {
        this.ctrynm = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
