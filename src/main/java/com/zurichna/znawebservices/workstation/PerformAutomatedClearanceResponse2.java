
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PerformAutomatedClearanceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PerformAutomatedClearanceResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="submissionProductResult" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionProductResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PerformAutomatedClearanceResponse", propOrder = {
    "submissionProductResult"
})
public class PerformAutomatedClearanceResponse2
    extends ResponseBase
{

    protected ArrayOfSubmissionProductResult submissionProductResult;

    /**
     * Gets the value of the submissionProductResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionProductResult }
     *     
     */
    public ArrayOfSubmissionProductResult getSubmissionProductResult() {
        return submissionProductResult;
    }

    /**
     * Sets the value of the submissionProductResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionProductResult }
     *     
     */
    public void setSubmissionProductResult(ArrayOfSubmissionProductResult value) {
        this.submissionProductResult = value;
    }

}
