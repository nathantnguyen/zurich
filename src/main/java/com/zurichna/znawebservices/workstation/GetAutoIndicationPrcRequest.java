
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetAutoIndicationPrcRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetAutoIndicationPrcRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="techPric" type="{http://workstation.znawebservices.zurichna.com}AutoTechPricRequest" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetAutoIndicationPrcRequest", propOrder = {
    "techPric"
})
public class GetAutoIndicationPrcRequest
    extends RequestBase
{

    protected AutoTechPricRequest techPric;

    /**
     * Gets the value of the techPric property.
     * 
     * @return
     *     possible object is
     *     {@link AutoTechPricRequest }
     *     
     */
    public AutoTechPricRequest getTechPric() {
        return techPric;
    }

    /**
     * Sets the value of the techPric property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoTechPricRequest }
     *     
     */
    public void setTechPric(AutoTechPricRequest value) {
        this.techPric = value;
    }

}
