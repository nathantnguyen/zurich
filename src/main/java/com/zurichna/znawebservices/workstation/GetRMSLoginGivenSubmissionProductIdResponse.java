
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetRMSLoginGivenSubmissionProductIdResult" type="{http://workstation.znawebservices.zurichna.com}GetRMSLoginGivenSubmissionProductIdResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRMSLoginGivenSubmissionProductIdResult"
})
@XmlRootElement(name = "GetRMSLoginGivenSubmissionProductIdResponse")
public class GetRMSLoginGivenSubmissionProductIdResponse {

    @XmlElement(name = "GetRMSLoginGivenSubmissionProductIdResult")
    protected GetRMSLoginGivenSubmissionProductIdResponse2 getRMSLoginGivenSubmissionProductIdResult;

    /**
     * Gets the value of the getRMSLoginGivenSubmissionProductIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetRMSLoginGivenSubmissionProductIdResponse2 }
     *     
     */
    public GetRMSLoginGivenSubmissionProductIdResponse2 getGetRMSLoginGivenSubmissionProductIdResult() {
        return getRMSLoginGivenSubmissionProductIdResult;
    }

    /**
     * Sets the value of the getRMSLoginGivenSubmissionProductIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetRMSLoginGivenSubmissionProductIdResponse2 }
     *     
     */
    public void setGetRMSLoginGivenSubmissionProductIdResult(GetRMSLoginGivenSubmissionProductIdResponse2 value) {
        this.getRMSLoginGivenSubmissionProductIdResult = value;
    }

}
