
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BindSubmissionResult" type="{http://workstation.znawebservices.zurichna.com}SubmissionBindResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bindSubmissionResult"
})
@XmlRootElement(name = "BindSubmissionResponse")
public class BindSubmissionResponse {

    @XmlElement(name = "BindSubmissionResult")
    protected SubmissionBindResponse bindSubmissionResult;

    /**
     * Gets the value of the bindSubmissionResult property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionBindResponse }
     *     
     */
    public SubmissionBindResponse getBindSubmissionResult() {
        return bindSubmissionResult;
    }

    /**
     * Sets the value of the bindSubmissionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionBindResponse }
     *     
     */
    public void setBindSubmissionResult(SubmissionBindResponse value) {
        this.bindSubmissionResult = value;
    }

}
