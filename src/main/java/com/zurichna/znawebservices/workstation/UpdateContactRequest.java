
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateContactRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateContactRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deleteAtTopLevel" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="customerPartyId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="groupingId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="contact" type="{http://workstation.znawebservices.zurichna.com}ArrayOfContact" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateContactRequest", propOrder = {
    "deleteAtTopLevel",
    "customerPartyId",
    "groupingId",
    "contact"
})
public class UpdateContactRequest
    extends RequestBase
{

    protected boolean deleteAtTopLevel;
    protected int customerPartyId;
    protected int groupingId;
    protected ArrayOfContact contact;

    /**
     * Gets the value of the deleteAtTopLevel property.
     * 
     */
    public boolean isDeleteAtTopLevel() {
        return deleteAtTopLevel;
    }

    /**
     * Sets the value of the deleteAtTopLevel property.
     * 
     */
    public void setDeleteAtTopLevel(boolean value) {
        this.deleteAtTopLevel = value;
    }

    /**
     * Gets the value of the customerPartyId property.
     * 
     */
    public int getCustomerPartyId() {
        return customerPartyId;
    }

    /**
     * Sets the value of the customerPartyId property.
     * 
     */
    public void setCustomerPartyId(int value) {
        this.customerPartyId = value;
    }

    /**
     * Gets the value of the groupingId property.
     * 
     */
    public int getGroupingId() {
        return groupingId;
    }

    /**
     * Sets the value of the groupingId property.
     * 
     */
    public void setGroupingId(int value) {
        this.groupingId = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContact }
     *     
     */
    public ArrayOfContact getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContact }
     *     
     */
    public void setContact(ArrayOfContact value) {
        this.contact = value;
    }

}
