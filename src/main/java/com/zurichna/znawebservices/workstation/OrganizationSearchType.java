
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrganizationSearchType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganizationSearchType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="IMMEDIATE"/&gt;
 *     &lt;enumeration value="SYSTEM"/&gt;
 *     &lt;enumeration value="PARNTSCHLD"/&gt;
 *     &lt;enumeration value="PARNTSCHL2"/&gt;
 *     &lt;enumeration value="CHILDREN"/&gt;
 *     &lt;enumeration value="PARENTS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OrganizationSearchType")
@XmlEnum
public enum OrganizationSearchType {

    IMMEDIATE("IMMEDIATE"),
    SYSTEM("SYSTEM"),
    PARNTSCHLD("PARNTSCHLD"),
    @XmlEnumValue("PARNTSCHL2")
    PARNTSCHL_2("PARNTSCHL2"),
    CHILDREN("CHILDREN"),
    PARENTS("PARENTS");
    private final String value;

    OrganizationSearchType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrganizationSearchType fromValue(String v) {
        for (OrganizationSearchType c: OrganizationSearchType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
