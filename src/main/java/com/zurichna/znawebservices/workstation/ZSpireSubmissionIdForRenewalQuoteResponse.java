
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZSpireSubmissionIdForRenewalQuoteResult" type="{http://workstation.znawebservices.zurichna.com}ZSpireSMSNIdForRenewalQuoteResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "zSpireSubmissionIdForRenewalQuoteResult"
})
@XmlRootElement(name = "ZSpireSubmissionIdForRenewalQuoteResponse")
public class ZSpireSubmissionIdForRenewalQuoteResponse {

    @XmlElement(name = "ZSpireSubmissionIdForRenewalQuoteResult")
    protected ZSpireSMSNIdForRenewalQuoteResponse zSpireSubmissionIdForRenewalQuoteResult;

    /**
     * Gets the value of the zSpireSubmissionIdForRenewalQuoteResult property.
     * 
     * @return
     *     possible object is
     *     {@link ZSpireSMSNIdForRenewalQuoteResponse }
     *     
     */
    public ZSpireSMSNIdForRenewalQuoteResponse getZSpireSubmissionIdForRenewalQuoteResult() {
        return zSpireSubmissionIdForRenewalQuoteResult;
    }

    /**
     * Sets the value of the zSpireSubmissionIdForRenewalQuoteResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZSpireSMSNIdForRenewalQuoteResponse }
     *     
     */
    public void setZSpireSubmissionIdForRenewalQuoteResult(ZSpireSMSNIdForRenewalQuoteResponse value) {
        this.zSpireSubmissionIdForRenewalQuoteResult = value;
    }

}
