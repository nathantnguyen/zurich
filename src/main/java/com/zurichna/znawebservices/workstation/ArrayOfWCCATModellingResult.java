
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfWCCATModellingResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfWCCATModellingResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WCCATModellingResult" type="{http://workstation.znawebservices.zurichna.com}WCCATModellingResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfWCCATModellingResult", propOrder = {
    "wccatModellingResult"
})
public class ArrayOfWCCATModellingResult {

    @XmlElement(name = "WCCATModellingResult", nillable = true)
    protected List<WCCATModellingResult> wccatModellingResult;

    /**
     * Gets the value of the wccatModellingResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wccatModellingResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWCCATModellingResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WCCATModellingResult }
     * 
     * 
     */
    public List<WCCATModellingResult> getWCCATModellingResult() {
        if (wccatModellingResult == null) {
            wccatModellingResult = new ArrayList<WCCATModellingResult>();
        }
        return this.wccatModellingResult;
    }

}
