
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetMVRDataResult" type="{http://workstation.znawebservices.zurichna.com}GetMVRDataResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getMVRDataResult"
})
@XmlRootElement(name = "GetMVRDataResponse")
public class GetMVRDataResponse {

    @XmlElement(name = "GetMVRDataResult")
    protected GetMVRDataResponse2 getMVRDataResult;

    /**
     * Gets the value of the getMVRDataResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetMVRDataResponse2 }
     *     
     */
    public GetMVRDataResponse2 getGetMVRDataResult() {
        return getMVRDataResult;
    }

    /**
     * Sets the value of the getMVRDataResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetMVRDataResponse2 }
     *     
     */
    public void setGetMVRDataResult(GetMVRDataResponse2 value) {
        this.getMVRDataResult = value;
    }

}
