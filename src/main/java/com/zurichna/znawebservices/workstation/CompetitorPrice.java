
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompetitorPrice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompetitorPrice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CMPT_PRC_RNG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompetitorPrice", propOrder = {
    "cmptprcrng"
})
public class CompetitorPrice {

    @XmlElement(name = "CMPT_PRC_RNG")
    protected String cmptprcrng;

    /**
     * Gets the value of the cmptprcrng property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMPTPRCRNG() {
        return cmptprcrng;
    }

    /**
     * Sets the value of the cmptprcrng property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMPTPRCRNG(String value) {
        this.cmptprcrng = value;
    }

}
