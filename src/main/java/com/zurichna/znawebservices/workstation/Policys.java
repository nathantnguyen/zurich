
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Policys complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Policys"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MODU_NBR" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ENT_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SELCT_RENL_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZODIAC_POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ANNV_RATG_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FNL_ADT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FLWP_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="POL_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PREV_POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PREV_MODU_NBR" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PREV_POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORIG_MMCCYY" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="UNDR_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RETRO_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CM_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RETRO_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SELCT_NRENL_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CONV_POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CONV_POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INS_GRP_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SYS_SRC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RSN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_CONTR_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUST_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BIL_TYP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AGMT_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="AGMT_LIM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="AGMT_DED_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="MSTR_AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="AGMT_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDG_PGM_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ACQN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ADT_FREQ_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BOUND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CAN_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RSK_EXPR_CLM_CNT" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RSKEXPR_RX_ILOSAMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ZIPS_ANNL_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ZIPS_BKD_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="RPT_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INTL_DIR_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AGMT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="POL_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="POL_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PRDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_STRT_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_DEP_STRT_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_CTRY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_AREA_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_XCH_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_TE4_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_TEL_EXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORGL_PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_INS_GRP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="INT_ORG_UNT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_TYP_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_TYP_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LVL_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OPRT_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SIC_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CUST_GP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="IND_LICENSE_PRDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSD_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNTA_PRDR_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CNTA_INSD_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MANG_CARE_BIL_REVW_FEE_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="REINS_PGM_TYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CAPTV_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DOMCL_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CAPTV_MGR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LIM_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="policyProduct" type="{http://workstation.znawebservices.zurichna.com}ArrayOfProducts" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Policys", propOrder = {
    "polnbr",
    "polsym",
    "modunbr",
    "entdt",
    "selctrenlind",
    "zodiacpolnbr",
    "annvratgind",
    "fnladtind",
    "flwpdt",
    "polenttistmp",
    "prevpolnbr",
    "prevmodunbr",
    "prevpolsym",
    "origmmccyy",
    "undrcd",
    "retrodt",
    "cmdt",
    "retrocd",
    "selctnrenlind",
    "convpolnbr",
    "convpolsym",
    "insgrpptyid",
    "syssrccd",
    "rsncd",
    "undgpgmcd",
    "undgpgmnm",
    "polcontrtypid",
    "custptyid",
    "nm",
    "biltypcd",
    "agmtenttistmp",
    "agmtlimamt",
    "agmtdedamt",
    "mstragmtid",
    "agmttypid",
    "undgpgmid",
    "smsnid",
    "acqncd",
    "adtfreqcd",
    "boundpremamt",
    "candt",
    "newrenlcd",
    "rskexprclmcnt",
    "rskexprrxilosamt",
    "zipsannlpremamt",
    "zipsbkdpremamt",
    "rptind",
    "intldirind",
    "agmtid",
    "poleffdt",
    "polexpidt",
    "prdrptyid",
    "prdrenttistmp",
    "prdrnbr",
    "prdrnm",
    "prdrstrtaddr",
    "prdrdepstrtaddr",
    "prdrctynm",
    "prdrstabbr",
    "prdrzipcd",
    "prdrctrynm",
    "prdrstscd",
    "prdrareacd",
    "prdrxchnbr",
    "prdrte4NBR",
    "prdrtelext",
    "orglprdrnbr",
    "prdrinsgrpid",
    "intorguntid",
    "orgtypnm",
    "orgtypabbr",
    "orgptyid",
    "orgnm",
    "orgabbr",
    "ptyid",
    "siccd",
    "sicnm",
    "lvlind",
    "oprtdesc",
    "siceffdt",
    "sicexpidt",
    "custgpid",
    "indlicenseprdr",
    "prdrfstnm",
    "prdrlstnm",
    "prdremail",
    "insdfstnm",
    "insdlstnm",
    "insdemail",
    "cntaprdrind",
    "cntainsdind",
    "mangcarebilrevwfeecd",
    "reinspgmtyp",
    "captvnm",
    "domclnm",
    "captvmgrnm",
    "limtxt",
    "policyProduct"
})
public class Policys {

    @XmlElement(name = "POL_NBR")
    protected String polnbr;
    @XmlElement(name = "POL_SYM")
    protected String polsym;
    @XmlElement(name = "MODU_NBR")
    protected int modunbr;
    @XmlElement(name = "ENT_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar entdt;
    @XmlElement(name = "SELCT_RENL_IND")
    protected String selctrenlind;
    @XmlElement(name = "ZODIAC_POL_NBR")
    protected String zodiacpolnbr;
    @XmlElement(name = "ANNV_RATG_IND")
    protected String annvratgind;
    @XmlElement(name = "FNL_ADT_IND")
    protected String fnladtind;
    @XmlElement(name = "FLWP_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar flwpdt;
    @XmlElement(name = "POL_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar polenttistmp;
    @XmlElement(name = "PREV_POL_NBR")
    protected String prevpolnbr;
    @XmlElement(name = "PREV_MODU_NBR")
    protected int prevmodunbr;
    @XmlElement(name = "PREV_POL_SYM")
    protected String prevpolsym;
    @XmlElement(name = "ORIG_MMCCYY", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar origmmccyy;
    @XmlElement(name = "UNDR_CD")
    protected String undrcd;
    @XmlElement(name = "RETRO_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar retrodt;
    @XmlElement(name = "CM_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar cmdt;
    @XmlElement(name = "RETRO_CD")
    protected String retrocd;
    @XmlElement(name = "SELCT_NRENL_IND")
    protected String selctnrenlind;
    @XmlElement(name = "CONV_POL_NBR")
    protected String convpolnbr;
    @XmlElement(name = "CONV_POL_SYM")
    protected String convpolsym;
    @XmlElement(name = "INS_GRP_PTY_ID")
    protected int insgrpptyid;
    @XmlElement(name = "SYS_SRC_CD")
    protected String syssrccd;
    @XmlElement(name = "RSN_CD")
    protected String rsncd;
    @XmlElement(name = "UNDG_PGM_CD")
    protected String undgpgmcd;
    @XmlElement(name = "UNDG_PGM_NM")
    protected String undgpgmnm;
    @XmlElement(name = "POL_CONTR_TYP_ID")
    protected int polcontrtypid;
    @XmlElement(name = "CUST_PTY_ID")
    protected int custptyid;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "BIL_TYP_CD")
    protected String biltypcd;
    @XmlElement(name = "AGMT_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar agmtenttistmp;
    @XmlElement(name = "AGMT_LIM_AMT", required = true)
    protected BigDecimal agmtlimamt;
    @XmlElement(name = "AGMT_DED_AMT", required = true)
    protected BigDecimal agmtdedamt;
    @XmlElement(name = "MSTR_AGMT_ID")
    protected int mstragmtid;
    @XmlElement(name = "AGMT_TYP_ID")
    protected int agmttypid;
    @XmlElement(name = "UNDG_PGM_ID")
    protected int undgpgmid;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "ACQN_CD")
    protected String acqncd;
    @XmlElement(name = "ADT_FREQ_CD")
    protected String adtfreqcd;
    @XmlElement(name = "BOUND_PREM_AMT", required = true)
    protected BigDecimal boundpremamt;
    @XmlElement(name = "CAN_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar candt;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "RSK_EXPR_CLM_CNT")
    protected int rskexprclmcnt;
    @XmlElement(name = "RSKEXPR_RX_ILOSAMT", required = true)
    protected BigDecimal rskexprrxilosamt;
    @XmlElement(name = "ZIPS_ANNL_PREM_AMT", required = true)
    protected BigDecimal zipsannlpremamt;
    @XmlElement(name = "ZIPS_BKD_PREM_AMT", required = true)
    protected BigDecimal zipsbkdpremamt;
    @XmlElement(name = "RPT_IND")
    protected String rptind;
    @XmlElement(name = "INTL_DIR_IND")
    protected String intldirind;
    @XmlElement(name = "AGMT_ID")
    protected int agmtid;
    @XmlElement(name = "POL_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar poleffdt;
    @XmlElement(name = "POL_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar polexpidt;
    @XmlElement(name = "PRDR_PTY_ID")
    protected int prdrptyid;
    @XmlElement(name = "PRDR_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar prdrenttistmp;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "PRDR_NM")
    protected String prdrnm;
    @XmlElement(name = "PRDR_STRT_ADDR")
    protected String prdrstrtaddr;
    @XmlElement(name = "PRDR_DEP_STRT_ADDR")
    protected String prdrdepstrtaddr;
    @XmlElement(name = "PRDR_CTY_NM")
    protected String prdrctynm;
    @XmlElement(name = "PRDR_ST_ABBR")
    protected String prdrstabbr;
    @XmlElement(name = "PRDR_ZIP_CD")
    protected String prdrzipcd;
    @XmlElement(name = "PRDR_CTRY_NM")
    protected String prdrctrynm;
    @XmlElement(name = "PRDR_STS_CD")
    protected String prdrstscd;
    @XmlElement(name = "PRDR_AREA_CD")
    protected String prdrareacd;
    @XmlElement(name = "PRDR_XCH_NBR")
    protected String prdrxchnbr;
    @XmlElement(name = "PRDR_TE4_NBR")
    protected String prdrte4NBR;
    @XmlElement(name = "PRDR_TEL_EXT")
    protected String prdrtelext;
    @XmlElement(name = "ORGL_PRDR_NBR")
    protected String orglprdrnbr;
    @XmlElement(name = "PRDR_INS_GRP_ID")
    protected int prdrinsgrpid;
    @XmlElement(name = "INT_ORG_UNT_ID")
    protected int intorguntid;
    @XmlElement(name = "ORG_TYP_NM")
    protected String orgtypnm;
    @XmlElement(name = "ORG_TYP_ABBR")
    protected String orgtypabbr;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_NM")
    protected String orgnm;
    @XmlElement(name = "ORG_ABBR")
    protected String orgabbr;
    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "SIC_NM")
    protected String sicnm;
    @XmlElement(name = "LVL_IND")
    protected String lvlind;
    @XmlElement(name = "OPRT_DESC")
    protected String oprtdesc;
    @XmlElement(name = "SIC_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar siceffdt;
    @XmlElement(name = "SIC_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sicexpidt;
    @XmlElement(name = "CUST_GP_ID")
    protected int custgpid;
    @XmlElement(name = "IND_LICENSE_PRDR")
    protected String indlicenseprdr;
    @XmlElement(name = "PRDR_FST_NM")
    protected String prdrfstnm;
    @XmlElement(name = "PRDR_LST_NM")
    protected String prdrlstnm;
    @XmlElement(name = "PRDR_EMAIL")
    protected String prdremail;
    @XmlElement(name = "INSD_FST_NM")
    protected String insdfstnm;
    @XmlElement(name = "INSD_LST_NM")
    protected String insdlstnm;
    @XmlElement(name = "INSD_EMAIL")
    protected String insdemail;
    @XmlElement(name = "CNTA_PRDR_IND")
    protected String cntaprdrind;
    @XmlElement(name = "CNTA_INSD_IND")
    protected String cntainsdind;
    @XmlElement(name = "MANG_CARE_BIL_REVW_FEE_CD")
    protected String mangcarebilrevwfeecd;
    @XmlElement(name = "REINS_PGM_TYP")
    protected String reinspgmtyp;
    @XmlElement(name = "CAPTV_NM")
    protected String captvnm;
    @XmlElement(name = "DOMCL_NM")
    protected String domclnm;
    @XmlElement(name = "CAPTV_MGR_NM")
    protected String captvmgrnm;
    @XmlElement(name = "LIM_TXT")
    protected String limtxt;
    protected ArrayOfProducts policyProduct;

    /**
     * Gets the value of the polnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLNBR() {
        return polnbr;
    }

    /**
     * Sets the value of the polnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLNBR(String value) {
        this.polnbr = value;
    }

    /**
     * Gets the value of the polsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLSYM() {
        return polsym;
    }

    /**
     * Sets the value of the polsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLSYM(String value) {
        this.polsym = value;
    }

    /**
     * Gets the value of the modunbr property.
     * 
     */
    public int getMODUNBR() {
        return modunbr;
    }

    /**
     * Sets the value of the modunbr property.
     * 
     */
    public void setMODUNBR(int value) {
        this.modunbr = value;
    }

    /**
     * Gets the value of the entdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTDT() {
        return entdt;
    }

    /**
     * Sets the value of the entdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTDT(XMLGregorianCalendar value) {
        this.entdt = value;
    }

    /**
     * Gets the value of the selctrenlind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSELCTRENLIND() {
        return selctrenlind;
    }

    /**
     * Sets the value of the selctrenlind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSELCTRENLIND(String value) {
        this.selctrenlind = value;
    }

    /**
     * Gets the value of the zodiacpolnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZODIACPOLNBR() {
        return zodiacpolnbr;
    }

    /**
     * Sets the value of the zodiacpolnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZODIACPOLNBR(String value) {
        this.zodiacpolnbr = value;
    }

    /**
     * Gets the value of the annvratgind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getANNVRATGIND() {
        return annvratgind;
    }

    /**
     * Sets the value of the annvratgind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setANNVRATGIND(String value) {
        this.annvratgind = value;
    }

    /**
     * Gets the value of the fnladtind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFNLADTIND() {
        return fnladtind;
    }

    /**
     * Sets the value of the fnladtind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFNLADTIND(String value) {
        this.fnladtind = value;
    }

    /**
     * Gets the value of the flwpdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFLWPDT() {
        return flwpdt;
    }

    /**
     * Sets the value of the flwpdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFLWPDT(XMLGregorianCalendar value) {
        this.flwpdt = value;
    }

    /**
     * Gets the value of the polenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLENTTISTMP() {
        return polenttistmp;
    }

    /**
     * Sets the value of the polenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLENTTISTMP(XMLGregorianCalendar value) {
        this.polenttistmp = value;
    }

    /**
     * Gets the value of the prevpolnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPREVPOLNBR() {
        return prevpolnbr;
    }

    /**
     * Sets the value of the prevpolnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPREVPOLNBR(String value) {
        this.prevpolnbr = value;
    }

    /**
     * Gets the value of the prevmodunbr property.
     * 
     */
    public int getPREVMODUNBR() {
        return prevmodunbr;
    }

    /**
     * Sets the value of the prevmodunbr property.
     * 
     */
    public void setPREVMODUNBR(int value) {
        this.prevmodunbr = value;
    }

    /**
     * Gets the value of the prevpolsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPREVPOLSYM() {
        return prevpolsym;
    }

    /**
     * Sets the value of the prevpolsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPREVPOLSYM(String value) {
        this.prevpolsym = value;
    }

    /**
     * Gets the value of the origmmccyy property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getORIGMMCCYY() {
        return origmmccyy;
    }

    /**
     * Sets the value of the origmmccyy property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setORIGMMCCYY(XMLGregorianCalendar value) {
        this.origmmccyy = value;
    }

    /**
     * Gets the value of the undrcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRCD() {
        return undrcd;
    }

    /**
     * Sets the value of the undrcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRCD(String value) {
        this.undrcd = value;
    }

    /**
     * Gets the value of the retrodt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRETRODT() {
        return retrodt;
    }

    /**
     * Sets the value of the retrodt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRETRODT(XMLGregorianCalendar value) {
        this.retrodt = value;
    }

    /**
     * Gets the value of the cmdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCMDT() {
        return cmdt;
    }

    /**
     * Sets the value of the cmdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCMDT(XMLGregorianCalendar value) {
        this.cmdt = value;
    }

    /**
     * Gets the value of the retrocd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRETROCD() {
        return retrocd;
    }

    /**
     * Sets the value of the retrocd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRETROCD(String value) {
        this.retrocd = value;
    }

    /**
     * Gets the value of the selctnrenlind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSELCTNRENLIND() {
        return selctnrenlind;
    }

    /**
     * Sets the value of the selctnrenlind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSELCTNRENLIND(String value) {
        this.selctnrenlind = value;
    }

    /**
     * Gets the value of the convpolnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONVPOLNBR() {
        return convpolnbr;
    }

    /**
     * Sets the value of the convpolnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONVPOLNBR(String value) {
        this.convpolnbr = value;
    }

    /**
     * Gets the value of the convpolsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONVPOLSYM() {
        return convpolsym;
    }

    /**
     * Sets the value of the convpolsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONVPOLSYM(String value) {
        this.convpolsym = value;
    }

    /**
     * Gets the value of the insgrpptyid property.
     * 
     */
    public int getINSGRPPTYID() {
        return insgrpptyid;
    }

    /**
     * Sets the value of the insgrpptyid property.
     * 
     */
    public void setINSGRPPTYID(int value) {
        this.insgrpptyid = value;
    }

    /**
     * Gets the value of the syssrccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSYSSRCCD() {
        return syssrccd;
    }

    /**
     * Sets the value of the syssrccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSYSSRCCD(String value) {
        this.syssrccd = value;
    }

    /**
     * Gets the value of the rsncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRSNCD() {
        return rsncd;
    }

    /**
     * Sets the value of the rsncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRSNCD(String value) {
        this.rsncd = value;
    }

    /**
     * Gets the value of the undgpgmcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMCD() {
        return undgpgmcd;
    }

    /**
     * Sets the value of the undgpgmcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMCD(String value) {
        this.undgpgmcd = value;
    }

    /**
     * Gets the value of the undgpgmnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMNM() {
        return undgpgmnm;
    }

    /**
     * Sets the value of the undgpgmnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMNM(String value) {
        this.undgpgmnm = value;
    }

    /**
     * Gets the value of the polcontrtypid property.
     * 
     */
    public int getPOLCONTRTYPID() {
        return polcontrtypid;
    }

    /**
     * Sets the value of the polcontrtypid property.
     * 
     */
    public void setPOLCONTRTYPID(int value) {
        this.polcontrtypid = value;
    }

    /**
     * Gets the value of the custptyid property.
     * 
     */
    public int getCUSTPTYID() {
        return custptyid;
    }

    /**
     * Sets the value of the custptyid property.
     * 
     */
    public void setCUSTPTYID(int value) {
        this.custptyid = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the biltypcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBILTYPCD() {
        return biltypcd;
    }

    /**
     * Sets the value of the biltypcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBILTYPCD(String value) {
        this.biltypcd = value;
    }

    /**
     * Gets the value of the agmtenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAGMTENTTISTMP() {
        return agmtenttistmp;
    }

    /**
     * Sets the value of the agmtenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAGMTENTTISTMP(XMLGregorianCalendar value) {
        this.agmtenttistmp = value;
    }

    /**
     * Gets the value of the agmtlimamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAGMTLIMAMT() {
        return agmtlimamt;
    }

    /**
     * Sets the value of the agmtlimamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAGMTLIMAMT(BigDecimal value) {
        this.agmtlimamt = value;
    }

    /**
     * Gets the value of the agmtdedamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAGMTDEDAMT() {
        return agmtdedamt;
    }

    /**
     * Sets the value of the agmtdedamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAGMTDEDAMT(BigDecimal value) {
        this.agmtdedamt = value;
    }

    /**
     * Gets the value of the mstragmtid property.
     * 
     */
    public int getMSTRAGMTID() {
        return mstragmtid;
    }

    /**
     * Sets the value of the mstragmtid property.
     * 
     */
    public void setMSTRAGMTID(int value) {
        this.mstragmtid = value;
    }

    /**
     * Gets the value of the agmttypid property.
     * 
     */
    public int getAGMTTYPID() {
        return agmttypid;
    }

    /**
     * Sets the value of the agmttypid property.
     * 
     */
    public void setAGMTTYPID(int value) {
        this.agmttypid = value;
    }

    /**
     * Gets the value of the undgpgmid property.
     * 
     */
    public int getUNDGPGMID() {
        return undgpgmid;
    }

    /**
     * Sets the value of the undgpgmid property.
     * 
     */
    public void setUNDGPGMID(int value) {
        this.undgpgmid = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the acqncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACQNCD() {
        return acqncd;
    }

    /**
     * Sets the value of the acqncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACQNCD(String value) {
        this.acqncd = value;
    }

    /**
     * Gets the value of the adtfreqcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADTFREQCD() {
        return adtfreqcd;
    }

    /**
     * Sets the value of the adtfreqcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADTFREQCD(String value) {
        this.adtfreqcd = value;
    }

    /**
     * Gets the value of the boundpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBOUNDPREMAMT() {
        return boundpremamt;
    }

    /**
     * Sets the value of the boundpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBOUNDPREMAMT(BigDecimal value) {
        this.boundpremamt = value;
    }

    /**
     * Gets the value of the candt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCANDT() {
        return candt;
    }

    /**
     * Sets the value of the candt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCANDT(XMLGregorianCalendar value) {
        this.candt = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the rskexprclmcnt property.
     * 
     */
    public int getRSKEXPRCLMCNT() {
        return rskexprclmcnt;
    }

    /**
     * Sets the value of the rskexprclmcnt property.
     * 
     */
    public void setRSKEXPRCLMCNT(int value) {
        this.rskexprclmcnt = value;
    }

    /**
     * Gets the value of the rskexprrxilosamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRSKEXPRRXILOSAMT() {
        return rskexprrxilosamt;
    }

    /**
     * Sets the value of the rskexprrxilosamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRSKEXPRRXILOSAMT(BigDecimal value) {
        this.rskexprrxilosamt = value;
    }

    /**
     * Gets the value of the zipsannlpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getZIPSANNLPREMAMT() {
        return zipsannlpremamt;
    }

    /**
     * Sets the value of the zipsannlpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setZIPSANNLPREMAMT(BigDecimal value) {
        this.zipsannlpremamt = value;
    }

    /**
     * Gets the value of the zipsbkdpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getZIPSBKDPREMAMT() {
        return zipsbkdpremamt;
    }

    /**
     * Sets the value of the zipsbkdpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setZIPSBKDPREMAMT(BigDecimal value) {
        this.zipsbkdpremamt = value;
    }

    /**
     * Gets the value of the rptind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPTIND() {
        return rptind;
    }

    /**
     * Sets the value of the rptind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPTIND(String value) {
        this.rptind = value;
    }

    /**
     * Gets the value of the intldirind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTLDIRIND() {
        return intldirind;
    }

    /**
     * Sets the value of the intldirind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTLDIRIND(String value) {
        this.intldirind = value;
    }

    /**
     * Gets the value of the agmtid property.
     * 
     */
    public int getAGMTID() {
        return agmtid;
    }

    /**
     * Sets the value of the agmtid property.
     * 
     */
    public void setAGMTID(int value) {
        this.agmtid = value;
    }

    /**
     * Gets the value of the poleffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLEFFDT() {
        return poleffdt;
    }

    /**
     * Sets the value of the poleffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLEFFDT(XMLGregorianCalendar value) {
        this.poleffdt = value;
    }

    /**
     * Gets the value of the polexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPOLEXPIDT() {
        return polexpidt;
    }

    /**
     * Sets the value of the polexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPOLEXPIDT(XMLGregorianCalendar value) {
        this.polexpidt = value;
    }

    /**
     * Gets the value of the prdrptyid property.
     * 
     */
    public int getPRDRPTYID() {
        return prdrptyid;
    }

    /**
     * Sets the value of the prdrptyid property.
     * 
     */
    public void setPRDRPTYID(int value) {
        this.prdrptyid = value;
    }

    /**
     * Gets the value of the prdrenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPRDRENTTISTMP() {
        return prdrenttistmp;
    }

    /**
     * Sets the value of the prdrenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPRDRENTTISTMP(XMLGregorianCalendar value) {
        this.prdrenttistmp = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the prdrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNM() {
        return prdrnm;
    }

    /**
     * Sets the value of the prdrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNM(String value) {
        this.prdrnm = value;
    }

    /**
     * Gets the value of the prdrstrtaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTRTADDR() {
        return prdrstrtaddr;
    }

    /**
     * Sets the value of the prdrstrtaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTRTADDR(String value) {
        this.prdrstrtaddr = value;
    }

    /**
     * Gets the value of the prdrdepstrtaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRDEPSTRTADDR() {
        return prdrdepstrtaddr;
    }

    /**
     * Sets the value of the prdrdepstrtaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRDEPSTRTADDR(String value) {
        this.prdrdepstrtaddr = value;
    }

    /**
     * Gets the value of the prdrctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRCTYNM() {
        return prdrctynm;
    }

    /**
     * Sets the value of the prdrctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRCTYNM(String value) {
        this.prdrctynm = value;
    }

    /**
     * Gets the value of the prdrstabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTABBR() {
        return prdrstabbr;
    }

    /**
     * Sets the value of the prdrstabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTABBR(String value) {
        this.prdrstabbr = value;
    }

    /**
     * Gets the value of the prdrzipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRZIPCD() {
        return prdrzipcd;
    }

    /**
     * Sets the value of the prdrzipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRZIPCD(String value) {
        this.prdrzipcd = value;
    }

    /**
     * Gets the value of the prdrctrynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRCTRYNM() {
        return prdrctrynm;
    }

    /**
     * Sets the value of the prdrctrynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRCTRYNM(String value) {
        this.prdrctrynm = value;
    }

    /**
     * Gets the value of the prdrstscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTSCD() {
        return prdrstscd;
    }

    /**
     * Sets the value of the prdrstscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTSCD(String value) {
        this.prdrstscd = value;
    }

    /**
     * Gets the value of the prdrareacd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRAREACD() {
        return prdrareacd;
    }

    /**
     * Sets the value of the prdrareacd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRAREACD(String value) {
        this.prdrareacd = value;
    }

    /**
     * Gets the value of the prdrxchnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRXCHNBR() {
        return prdrxchnbr;
    }

    /**
     * Sets the value of the prdrxchnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRXCHNBR(String value) {
        this.prdrxchnbr = value;
    }

    /**
     * Gets the value of the prdrte4NBR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRTE4NBR() {
        return prdrte4NBR;
    }

    /**
     * Sets the value of the prdrte4NBR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRTE4NBR(String value) {
        this.prdrte4NBR = value;
    }

    /**
     * Gets the value of the prdrtelext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRTELEXT() {
        return prdrtelext;
    }

    /**
     * Sets the value of the prdrtelext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRTELEXT(String value) {
        this.prdrtelext = value;
    }

    /**
     * Gets the value of the orglprdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGLPRDRNBR() {
        return orglprdrnbr;
    }

    /**
     * Sets the value of the orglprdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGLPRDRNBR(String value) {
        this.orglprdrnbr = value;
    }

    /**
     * Gets the value of the prdrinsgrpid property.
     * 
     */
    public int getPRDRINSGRPID() {
        return prdrinsgrpid;
    }

    /**
     * Sets the value of the prdrinsgrpid property.
     * 
     */
    public void setPRDRINSGRPID(int value) {
        this.prdrinsgrpid = value;
    }

    /**
     * Gets the value of the intorguntid property.
     * 
     */
    public int getINTORGUNTID() {
        return intorguntid;
    }

    /**
     * Sets the value of the intorguntid property.
     * 
     */
    public void setINTORGUNTID(int value) {
        this.intorguntid = value;
    }

    /**
     * Gets the value of the orgtypnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGTYPNM() {
        return orgtypnm;
    }

    /**
     * Sets the value of the orgtypnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGTYPNM(String value) {
        this.orgtypnm = value;
    }

    /**
     * Gets the value of the orgtypabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGTYPABBR() {
        return orgtypabbr;
    }

    /**
     * Sets the value of the orgtypabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGTYPABBR(String value) {
        this.orgtypabbr = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGNM() {
        return orgnm;
    }

    /**
     * Sets the value of the orgnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGNM(String value) {
        this.orgnm = value;
    }

    /**
     * Gets the value of the orgabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGABBR() {
        return orgabbr;
    }

    /**
     * Sets the value of the orgabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGABBR(String value) {
        this.orgabbr = value;
    }

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the sicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICNM() {
        return sicnm;
    }

    /**
     * Sets the value of the sicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICNM(String value) {
        this.sicnm = value;
    }

    /**
     * Gets the value of the lvlind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLVLIND() {
        return lvlind;
    }

    /**
     * Sets the value of the lvlind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLVLIND(String value) {
        this.lvlind = value;
    }

    /**
     * Gets the value of the oprtdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOPRTDESC() {
        return oprtdesc;
    }

    /**
     * Sets the value of the oprtdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOPRTDESC(String value) {
        this.oprtdesc = value;
    }

    /**
     * Gets the value of the siceffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSICEFFDT() {
        return siceffdt;
    }

    /**
     * Sets the value of the siceffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSICEFFDT(XMLGregorianCalendar value) {
        this.siceffdt = value;
    }

    /**
     * Gets the value of the sicexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSICEXPIDT() {
        return sicexpidt;
    }

    /**
     * Sets the value of the sicexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSICEXPIDT(XMLGregorianCalendar value) {
        this.sicexpidt = value;
    }

    /**
     * Gets the value of the custgpid property.
     * 
     */
    public int getCUSTGPID() {
        return custgpid;
    }

    /**
     * Sets the value of the custgpid property.
     * 
     */
    public void setCUSTGPID(int value) {
        this.custgpid = value;
    }

    /**
     * Gets the value of the indlicenseprdr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINDLICENSEPRDR() {
        return indlicenseprdr;
    }

    /**
     * Sets the value of the indlicenseprdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINDLICENSEPRDR(String value) {
        this.indlicenseprdr = value;
    }

    /**
     * Gets the value of the prdrfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRFSTNM() {
        return prdrfstnm;
    }

    /**
     * Sets the value of the prdrfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRFSTNM(String value) {
        this.prdrfstnm = value;
    }

    /**
     * Gets the value of the prdrlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRLSTNM() {
        return prdrlstnm;
    }

    /**
     * Sets the value of the prdrlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRLSTNM(String value) {
        this.prdrlstnm = value;
    }

    /**
     * Gets the value of the prdremail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDREMAIL() {
        return prdremail;
    }

    /**
     * Sets the value of the prdremail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDREMAIL(String value) {
        this.prdremail = value;
    }

    /**
     * Gets the value of the insdfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDFSTNM() {
        return insdfstnm;
    }

    /**
     * Sets the value of the insdfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDFSTNM(String value) {
        this.insdfstnm = value;
    }

    /**
     * Gets the value of the insdlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDLSTNM() {
        return insdlstnm;
    }

    /**
     * Sets the value of the insdlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDLSTNM(String value) {
        this.insdlstnm = value;
    }

    /**
     * Gets the value of the insdemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSDEMAIL() {
        return insdemail;
    }

    /**
     * Sets the value of the insdemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSDEMAIL(String value) {
        this.insdemail = value;
    }

    /**
     * Gets the value of the cntaprdrind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNTAPRDRIND() {
        return cntaprdrind;
    }

    /**
     * Sets the value of the cntaprdrind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNTAPRDRIND(String value) {
        this.cntaprdrind = value;
    }

    /**
     * Gets the value of the cntainsdind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCNTAINSDIND() {
        return cntainsdind;
    }

    /**
     * Sets the value of the cntainsdind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCNTAINSDIND(String value) {
        this.cntainsdind = value;
    }

    /**
     * Gets the value of the mangcarebilrevwfeecd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMANGCAREBILREVWFEECD() {
        return mangcarebilrevwfeecd;
    }

    /**
     * Sets the value of the mangcarebilrevwfeecd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMANGCAREBILREVWFEECD(String value) {
        this.mangcarebilrevwfeecd = value;
    }

    /**
     * Gets the value of the reinspgmtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREINSPGMTYP() {
        return reinspgmtyp;
    }

    /**
     * Sets the value of the reinspgmtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREINSPGMTYP(String value) {
        this.reinspgmtyp = value;
    }

    /**
     * Gets the value of the captvnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPTVNM() {
        return captvnm;
    }

    /**
     * Sets the value of the captvnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPTVNM(String value) {
        this.captvnm = value;
    }

    /**
     * Gets the value of the domclnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOMCLNM() {
        return domclnm;
    }

    /**
     * Sets the value of the domclnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOMCLNM(String value) {
        this.domclnm = value;
    }

    /**
     * Gets the value of the captvmgrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPTVMGRNM() {
        return captvmgrnm;
    }

    /**
     * Sets the value of the captvmgrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPTVMGRNM(String value) {
        this.captvmgrnm = value;
    }

    /**
     * Gets the value of the limtxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLIMTXT() {
        return limtxt;
    }

    /**
     * Sets the value of the limtxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLIMTXT(String value) {
        this.limtxt = value;
    }

    /**
     * Gets the value of the policyProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProducts }
     *     
     */
    public ArrayOfProducts getPolicyProduct() {
        return policyProduct;
    }

    /**
     * Sets the value of the policyProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProducts }
     *     
     */
    public void setPolicyProduct(ArrayOfProducts value) {
        this.policyProduct = value;
    }

}
