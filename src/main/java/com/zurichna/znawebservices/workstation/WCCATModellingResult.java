
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WCCATModellingResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WCCATModellingResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UNT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ZIP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GEO_MLVL_DESC_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EMPE_CNT_NBR" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="MAX_EMPE_CNT_NBR" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TR_ZN_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LOC_CNTR" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WCCATModellingResult", propOrder = {
    "untid",
    "addr",
    "ctynm",
    "stabbr",
    "zipcd",
    "geomlvldesctxt",
    "empecntnbr",
    "maxempecntnbr",
    "trznnm",
    "loccntr"
})
public class WCCATModellingResult {

    @XmlElement(name = "UNT_ID")
    protected int untid;
    @XmlElement(name = "ADDR")
    protected String addr;
    @XmlElement(name = "CTY_NM")
    protected String ctynm;
    @XmlElement(name = "ST_ABBR")
    protected String stabbr;
    @XmlElement(name = "ZIP_CD")
    protected String zipcd;
    @XmlElement(name = "GEO_MLVL_DESC_TXT")
    protected String geomlvldesctxt;
    @XmlElement(name = "EMPE_CNT_NBR")
    protected int empecntnbr;
    @XmlElement(name = "MAX_EMPE_CNT_NBR")
    protected int maxempecntnbr;
    @XmlElement(name = "TR_ZN_NM")
    protected String trznnm;
    @XmlElement(name = "LOC_CNTR")
    protected double loccntr;

    /**
     * Gets the value of the untid property.
     * 
     */
    public int getUNTID() {
        return untid;
    }

    /**
     * Sets the value of the untid property.
     * 
     */
    public void setUNTID(int value) {
        this.untid = value;
    }

    /**
     * Gets the value of the addr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDR() {
        return addr;
    }

    /**
     * Sets the value of the addr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDR(String value) {
        this.addr = value;
    }

    /**
     * Gets the value of the ctynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTYNM() {
        return ctynm;
    }

    /**
     * Sets the value of the ctynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTYNM(String value) {
        this.ctynm = value;
    }

    /**
     * Gets the value of the stabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTABBR() {
        return stabbr;
    }

    /**
     * Sets the value of the stabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTABBR(String value) {
        this.stabbr = value;
    }

    /**
     * Gets the value of the zipcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIPCD() {
        return zipcd;
    }

    /**
     * Sets the value of the zipcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIPCD(String value) {
        this.zipcd = value;
    }

    /**
     * Gets the value of the geomlvldesctxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGEOMLVLDESCTXT() {
        return geomlvldesctxt;
    }

    /**
     * Sets the value of the geomlvldesctxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGEOMLVLDESCTXT(String value) {
        this.geomlvldesctxt = value;
    }

    /**
     * Gets the value of the empecntnbr property.
     * 
     */
    public int getEMPECNTNBR() {
        return empecntnbr;
    }

    /**
     * Sets the value of the empecntnbr property.
     * 
     */
    public void setEMPECNTNBR(int value) {
        this.empecntnbr = value;
    }

    /**
     * Gets the value of the maxempecntnbr property.
     * 
     */
    public int getMAXEMPECNTNBR() {
        return maxempecntnbr;
    }

    /**
     * Sets the value of the maxempecntnbr property.
     * 
     */
    public void setMAXEMPECNTNBR(int value) {
        this.maxempecntnbr = value;
    }

    /**
     * Gets the value of the trznnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRZNNM() {
        return trznnm;
    }

    /**
     * Sets the value of the trznnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRZNNM(String value) {
        this.trznnm = value;
    }

    /**
     * Gets the value of the loccntr property.
     * 
     */
    public double getLOCCNTR() {
        return loccntr;
    }

    /**
     * Sets the value of the loccntr property.
     * 
     */
    public void setLOCCNTR(double value) {
        this.loccntr = value;
    }

}
