
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateAccountRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateAccountRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AddCustomer" type="{http://workstation.znawebservices.zurichna.com}CreateAccount" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateAccountRequest", propOrder = {
    "addCustomer"
})
public class CreateAccountRequest
    extends RequestBase
{

    @XmlElement(name = "AddCustomer")
    protected CreateAccount2 addCustomer;

    /**
     * Gets the value of the addCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link CreateAccount2 }
     *     
     */
    public CreateAccount2 getAddCustomer() {
        return addCustomer;
    }

    /**
     * Sets the value of the addCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateAccount2 }
     *     
     */
    public void setAddCustomer(CreateAccount2 value) {
        this.addCustomer = value;
    }

}
