
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Carrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Carrier"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CARR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CARR_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CARR_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CARR_LONGDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Carrier", propOrder = {
    "carrid",
    "carrcd",
    "carrdesc",
    "carrlongdesc"
})
public class Carrier {

    @XmlElement(name = "CARR_ID")
    protected int carrid;
    @XmlElement(name = "CARR_CD")
    protected String carrcd;
    @XmlElement(name = "CARR_DESC")
    protected String carrdesc;
    @XmlElement(name = "CARR_LONGDESC")
    protected String carrlongdesc;

    /**
     * Gets the value of the carrid property.
     * 
     */
    public int getCARRID() {
        return carrid;
    }

    /**
     * Sets the value of the carrid property.
     * 
     */
    public void setCARRID(int value) {
        this.carrid = value;
    }

    /**
     * Gets the value of the carrcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCARRCD() {
        return carrcd;
    }

    /**
     * Sets the value of the carrcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCARRCD(String value) {
        this.carrcd = value;
    }

    /**
     * Gets the value of the carrdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCARRDESC() {
        return carrdesc;
    }

    /**
     * Sets the value of the carrdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCARRDESC(String value) {
        this.carrdesc = value;
    }

    /**
     * Gets the value of the carrlongdesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCARRLONGDESC() {
        return carrlongdesc;
    }

    /**
     * Sets the value of the carrlongdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCARRLONGDESC(String value) {
        this.carrlongdesc = value;
    }

}
