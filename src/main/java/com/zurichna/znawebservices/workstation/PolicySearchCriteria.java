
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PolicySearchCriteria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicySearchCriteria"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="POL_SYM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MODU_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TMPL_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EFF_DT_BEGIN" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EFF_DT_END" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT_BEGIN" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT_END" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicySearchCriteria", propOrder = {
    "polsym",
    "polnbr",
    "modunbr",
    "tmplinsspecid",
    "effdtbegin",
    "effdtend",
    "expidtbegin",
    "expidtend",
    "siccd",
    "undgpgmid",
    "prdrptyid",
    "undrptyid",
    "orgptyid",
    "newrenlcd",
    "stscd"
})
public class PolicySearchCriteria {

    @XmlElement(name = "POL_SYM")
    protected String polsym;
    @XmlElement(name = "POL_NBR")
    protected String polnbr;
    @XmlElement(name = "MODU_NBR")
    protected String modunbr;
    @XmlElement(name = "TMPL_INS_SPEC_ID")
    protected int tmplinsspecid;
    @XmlElement(name = "EFF_DT_BEGIN", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdtbegin;
    @XmlElement(name = "EFF_DT_END", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdtend;
    @XmlElement(name = "EXPI_DT_BEGIN", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidtbegin;
    @XmlElement(name = "EXPI_DT_END", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidtend;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "UNDG_PGM_ID")
    protected int undgpgmid;
    @XmlElement(name = "PRDR_PTY_ID")
    protected int prdrptyid;
    @XmlElement(name = "UNDR_PTY_ID")
    protected int undrptyid;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "STS_CD")
    protected String stscd;

    /**
     * Gets the value of the polsym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLSYM() {
        return polsym;
    }

    /**
     * Sets the value of the polsym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLSYM(String value) {
        this.polsym = value;
    }

    /**
     * Gets the value of the polnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLNBR() {
        return polnbr;
    }

    /**
     * Sets the value of the polnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLNBR(String value) {
        this.polnbr = value;
    }

    /**
     * Gets the value of the modunbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMODUNBR() {
        return modunbr;
    }

    /**
     * Sets the value of the modunbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMODUNBR(String value) {
        this.modunbr = value;
    }

    /**
     * Gets the value of the tmplinsspecid property.
     * 
     */
    public int getTMPLINSSPECID() {
        return tmplinsspecid;
    }

    /**
     * Sets the value of the tmplinsspecid property.
     * 
     */
    public void setTMPLINSSPECID(int value) {
        this.tmplinsspecid = value;
    }

    /**
     * Gets the value of the effdtbegin property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDTBEGIN() {
        return effdtbegin;
    }

    /**
     * Sets the value of the effdtbegin property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDTBEGIN(XMLGregorianCalendar value) {
        this.effdtbegin = value;
    }

    /**
     * Gets the value of the effdtend property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDTEND() {
        return effdtend;
    }

    /**
     * Sets the value of the effdtend property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDTEND(XMLGregorianCalendar value) {
        this.effdtend = value;
    }

    /**
     * Gets the value of the expidtbegin property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDTBEGIN() {
        return expidtbegin;
    }

    /**
     * Sets the value of the expidtbegin property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDTBEGIN(XMLGregorianCalendar value) {
        this.expidtbegin = value;
    }

    /**
     * Gets the value of the expidtend property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDTEND() {
        return expidtend;
    }

    /**
     * Sets the value of the expidtend property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDTEND(XMLGregorianCalendar value) {
        this.expidtend = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the undgpgmid property.
     * 
     */
    public int getUNDGPGMID() {
        return undgpgmid;
    }

    /**
     * Sets the value of the undgpgmid property.
     * 
     */
    public void setUNDGPGMID(int value) {
        this.undgpgmid = value;
    }

    /**
     * Gets the value of the prdrptyid property.
     * 
     */
    public int getPRDRPTYID() {
        return prdrptyid;
    }

    /**
     * Sets the value of the prdrptyid property.
     * 
     */
    public void setPRDRPTYID(int value) {
        this.prdrptyid = value;
    }

    /**
     * Gets the value of the undrptyid property.
     * 
     */
    public int getUNDRPTYID() {
        return undrptyid;
    }

    /**
     * Sets the value of the undrptyid property.
     * 
     */
    public void setUNDRPTYID(int value) {
        this.undrptyid = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the stscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTSCD() {
        return stscd;
    }

    /**
     * Sets the value of the stscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTSCD(String value) {
        this.stscd = value;
    }

}
