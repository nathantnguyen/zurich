
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseInfoType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ResponseInfoType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Success"/&gt;
 *     &lt;enumeration value="Exception"/&gt;
 *     &lt;enumeration value="Validation"/&gt;
 *     &lt;enumeration value="Warning"/&gt;
 *     &lt;enumeration value="QueueToken"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ResponseInfoType")
@XmlEnum
public enum ResponseInfoType {

    @XmlEnumValue("Success")
    SUCCESS("Success"),
    @XmlEnumValue("Exception")
    EXCEPTION("Exception"),
    @XmlEnumValue("Validation")
    VALIDATION("Validation"),
    @XmlEnumValue("Warning")
    WARNING("Warning"),
    @XmlEnumValue("QueueToken")
    QUEUE_TOKEN("QueueToken");
    private final String value;

    ResponseInfoType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ResponseInfoType fromValue(String v) {
        for (ResponseInfoType c: ResponseInfoType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
