
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReferenceRequestCriteria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReferenceRequestCriteria"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestCriteriaType" type="{http://workstation.znawebservices.zurichna.com}ReferenceRequestCriteriaType"/&gt;
 *         &lt;element name="requestCriteriaValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceRequestCriteria", propOrder = {
    "requestCriteriaType",
    "requestCriteriaValue"
})
public class ReferenceRequestCriteria {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ReferenceRequestCriteriaType requestCriteriaType;
    protected String requestCriteriaValue;

    /**
     * Gets the value of the requestCriteriaType property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceRequestCriteriaType }
     *     
     */
    public ReferenceRequestCriteriaType getRequestCriteriaType() {
        return requestCriteriaType;
    }

    /**
     * Sets the value of the requestCriteriaType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceRequestCriteriaType }
     *     
     */
    public void setRequestCriteriaType(ReferenceRequestCriteriaType value) {
        this.requestCriteriaType = value;
    }

    /**
     * Gets the value of the requestCriteriaValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestCriteriaValue() {
        return requestCriteriaValue;
    }

    /**
     * Sets the value of the requestCriteriaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestCriteriaValue(String value) {
        this.requestCriteriaValue = value;
    }

}
