
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdvanceZorbaSearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdvanceZorbaSearch"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customer" type="{http://workstation.znawebservices.zurichna.com}ZorbaCustomer" minOccurs="0"/&gt;
 *         &lt;element name="zorbaSubmission" type="{http://workstation.znawebservices.zurichna.com}ArrayOfZorbaSubmision" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdvanceZorbaSearch", propOrder = {
    "customer",
    "zorbaSubmission"
})
public class AdvanceZorbaSearch {

    protected ZorbaCustomer customer;
    protected ArrayOfZorbaSubmision zorbaSubmission;

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link ZorbaCustomer }
     *     
     */
    public ZorbaCustomer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZorbaCustomer }
     *     
     */
    public void setCustomer(ZorbaCustomer value) {
        this.customer = value;
    }

    /**
     * Gets the value of the zorbaSubmission property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfZorbaSubmision }
     *     
     */
    public ArrayOfZorbaSubmision getZorbaSubmission() {
        return zorbaSubmission;
    }

    /**
     * Sets the value of the zorbaSubmission property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfZorbaSubmision }
     *     
     */
    public void setZorbaSubmission(ArrayOfZorbaSubmision value) {
        this.zorbaSubmission = value;
    }

}
