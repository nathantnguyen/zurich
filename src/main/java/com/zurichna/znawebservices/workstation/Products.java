
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Products complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Products"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ACQN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BKD_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="CMPT_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="EXPO_LOC_CLS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INCL_EXCL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INS_SPEC_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ND_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QTE_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OPTN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUSTM_INS_SPEC_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CUSTM_INS_SPEC_EXP_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="TMPL_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDT_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PERSONNEL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_TYP_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_TYP_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Products", propOrder = {
    "acqncd",
    "bkdpremamt",
    "cmptpremamt",
    "expolocclscd",
    "inclexclcd",
    "insspectypid",
    "ndpremamt",
    "newrenlcd",
    "qtepremamt",
    "smsnid",
    "optnid",
    "custminsspecid",
    "custminsspeceffdt",
    "custminsspecexpdt",
    "tmplinsspecid",
    "prdtnm",
    "prdtabbr",
    "personnelid",
    "orgid",
    "orgtypnm",
    "orgtypabbr",
    "orgptyid",
    "orgnm",
    "orgabbr"
})
public class Products {

    @XmlElement(name = "ACQN_CD")
    protected String acqncd;
    @XmlElement(name = "BKD_PREM_AMT", required = true)
    protected BigDecimal bkdpremamt;
    @XmlElement(name = "CMPT_PREM_AMT", required = true)
    protected BigDecimal cmptpremamt;
    @XmlElement(name = "EXPO_LOC_CLS_CD")
    protected String expolocclscd;
    @XmlElement(name = "INCL_EXCL_CD")
    protected String inclexclcd;
    @XmlElement(name = "INS_SPEC_TYP_ID")
    protected int insspectypid;
    @XmlElement(name = "ND_PREM_AMT", required = true)
    protected BigDecimal ndpremamt;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "QTE_PREM_AMT", required = true)
    protected BigDecimal qtepremamt;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "OPTN_ID")
    protected int optnid;
    @XmlElement(name = "CUSTM_INS_SPEC_ID")
    protected int custminsspecid;
    @XmlElement(name = "CUSTM_INS_SPEC_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar custminsspeceffdt;
    @XmlElement(name = "CUSTM_INS_SPEC_EXP_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar custminsspecexpdt;
    @XmlElement(name = "TMPL_INS_SPEC_ID")
    protected int tmplinsspecid;
    @XmlElement(name = "PRDT_NM")
    protected String prdtnm;
    @XmlElement(name = "PRDT_ABBR")
    protected String prdtabbr;
    @XmlElement(name = "PERSONNEL_ID")
    protected int personnelid;
    @XmlElement(name = "ORG_ID")
    protected int orgid;
    @XmlElement(name = "ORG_TYP_NM")
    protected String orgtypnm;
    @XmlElement(name = "ORG_TYP_ABBR")
    protected String orgtypabbr;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_NM")
    protected String orgnm;
    @XmlElement(name = "ORG_ABBR")
    protected String orgabbr;

    /**
     * Gets the value of the acqncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACQNCD() {
        return acqncd;
    }

    /**
     * Sets the value of the acqncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACQNCD(String value) {
        this.acqncd = value;
    }

    /**
     * Gets the value of the bkdpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBKDPREMAMT() {
        return bkdpremamt;
    }

    /**
     * Sets the value of the bkdpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBKDPREMAMT(BigDecimal value) {
        this.bkdpremamt = value;
    }

    /**
     * Gets the value of the cmptpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCMPTPREMAMT() {
        return cmptpremamt;
    }

    /**
     * Sets the value of the cmptpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCMPTPREMAMT(BigDecimal value) {
        this.cmptpremamt = value;
    }

    /**
     * Gets the value of the expolocclscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPOLOCCLSCD() {
        return expolocclscd;
    }

    /**
     * Sets the value of the expolocclscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPOLOCCLSCD(String value) {
        this.expolocclscd = value;
    }

    /**
     * Gets the value of the inclexclcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINCLEXCLCD() {
        return inclexclcd;
    }

    /**
     * Sets the value of the inclexclcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINCLEXCLCD(String value) {
        this.inclexclcd = value;
    }

    /**
     * Gets the value of the insspectypid property.
     * 
     */
    public int getINSSPECTYPID() {
        return insspectypid;
    }

    /**
     * Sets the value of the insspectypid property.
     * 
     */
    public void setINSSPECTYPID(int value) {
        this.insspectypid = value;
    }

    /**
     * Gets the value of the ndpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNDPREMAMT() {
        return ndpremamt;
    }

    /**
     * Sets the value of the ndpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNDPREMAMT(BigDecimal value) {
        this.ndpremamt = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the qtepremamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTEPREMAMT() {
        return qtepremamt;
    }

    /**
     * Sets the value of the qtepremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTEPREMAMT(BigDecimal value) {
        this.qtepremamt = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the optnid property.
     * 
     */
    public int getOPTNID() {
        return optnid;
    }

    /**
     * Sets the value of the optnid property.
     * 
     */
    public void setOPTNID(int value) {
        this.optnid = value;
    }

    /**
     * Gets the value of the custminsspecid property.
     * 
     */
    public int getCUSTMINSSPECID() {
        return custminsspecid;
    }

    /**
     * Sets the value of the custminsspecid property.
     * 
     */
    public void setCUSTMINSSPECID(int value) {
        this.custminsspecid = value;
    }

    /**
     * Gets the value of the custminsspeceffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCUSTMINSSPECEFFDT() {
        return custminsspeceffdt;
    }

    /**
     * Sets the value of the custminsspeceffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCUSTMINSSPECEFFDT(XMLGregorianCalendar value) {
        this.custminsspeceffdt = value;
    }

    /**
     * Gets the value of the custminsspecexpdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCUSTMINSSPECEXPDT() {
        return custminsspecexpdt;
    }

    /**
     * Sets the value of the custminsspecexpdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCUSTMINSSPECEXPDT(XMLGregorianCalendar value) {
        this.custminsspecexpdt = value;
    }

    /**
     * Gets the value of the tmplinsspecid property.
     * 
     */
    public int getTMPLINSSPECID() {
        return tmplinsspecid;
    }

    /**
     * Sets the value of the tmplinsspecid property.
     * 
     */
    public void setTMPLINSSPECID(int value) {
        this.tmplinsspecid = value;
    }

    /**
     * Gets the value of the prdtnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTNM() {
        return prdtnm;
    }

    /**
     * Sets the value of the prdtnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTNM(String value) {
        this.prdtnm = value;
    }

    /**
     * Gets the value of the prdtabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTABBR() {
        return prdtabbr;
    }

    /**
     * Sets the value of the prdtabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTABBR(String value) {
        this.prdtabbr = value;
    }

    /**
     * Gets the value of the personnelid property.
     * 
     */
    public int getPERSONNELID() {
        return personnelid;
    }

    /**
     * Sets the value of the personnelid property.
     * 
     */
    public void setPERSONNELID(int value) {
        this.personnelid = value;
    }

    /**
     * Gets the value of the orgid property.
     * 
     */
    public int getORGID() {
        return orgid;
    }

    /**
     * Sets the value of the orgid property.
     * 
     */
    public void setORGID(int value) {
        this.orgid = value;
    }

    /**
     * Gets the value of the orgtypnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGTYPNM() {
        return orgtypnm;
    }

    /**
     * Sets the value of the orgtypnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGTYPNM(String value) {
        this.orgtypnm = value;
    }

    /**
     * Gets the value of the orgtypabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGTYPABBR() {
        return orgtypabbr;
    }

    /**
     * Sets the value of the orgtypabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGTYPABBR(String value) {
        this.orgtypabbr = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGNM() {
        return orgnm;
    }

    /**
     * Sets the value of the orgnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGNM(String value) {
        this.orgnm = value;
    }

    /**
     * Gets the value of the orgabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGABBR() {
        return orgabbr;
    }

    /**
     * Sets the value of the orgabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGABBR(String value) {
        this.orgabbr = value;
    }

}
