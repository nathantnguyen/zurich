
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetAgreementsGivenPolicyNumberResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetAgreementsGivenPolicyNumberResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="policyNumbers" type="{http://workstation.znawebservices.zurichna.com}ArrayOfGetAgreementsGivenPolicyNumber" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetAgreementsGivenPolicyNumberResponse", propOrder = {
    "policyNumbers"
})
public class GetAgreementsGivenPolicyNumberResponse2
    extends ResponseBase
{

    protected ArrayOfGetAgreementsGivenPolicyNumber policyNumbers;

    /**
     * Gets the value of the policyNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetAgreementsGivenPolicyNumber }
     *     
     */
    public ArrayOfGetAgreementsGivenPolicyNumber getPolicyNumbers() {
        return policyNumbers;
    }

    /**
     * Sets the value of the policyNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetAgreementsGivenPolicyNumber }
     *     
     */
    public void setPolicyNumbers(ArrayOfGetAgreementsGivenPolicyNumber value) {
        this.policyNumbers = value;
    }

}
