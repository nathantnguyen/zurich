
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetAlertResult" type="{http://workstation.znawebservices.zurichna.com}AlertResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAlertResult"
})
@XmlRootElement(name = "GetAlertResponse")
public class GetAlertResponse {

    @XmlElement(name = "GetAlertResult")
    protected AlertResponse getAlertResult;

    /**
     * Gets the value of the getAlertResult property.
     * 
     * @return
     *     possible object is
     *     {@link AlertResponse }
     *     
     */
    public AlertResponse getGetAlertResult() {
        return getAlertResult;
    }

    /**
     * Sets the value of the getAlertResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertResponse }
     *     
     */
    public void setGetAlertResult(AlertResponse value) {
        this.getAlertResult = value;
    }

}
