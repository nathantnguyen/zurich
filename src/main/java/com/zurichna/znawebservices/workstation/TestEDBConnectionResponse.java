
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TestEDBConnectionResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "testEDBConnectionResult"
})
@XmlRootElement(name = "TestEDBConnectionResponse")
public class TestEDBConnectionResponse {

    @XmlElement(name = "TestEDBConnectionResult")
    protected String testEDBConnectionResult;

    /**
     * Gets the value of the testEDBConnectionResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestEDBConnectionResult() {
        return testEDBConnectionResult;
    }

    /**
     * Sets the value of the testEDBConnectionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestEDBConnectionResult(String value) {
        this.testEDBConnectionResult = value;
    }

}
