
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WCTargetPriceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WCTargetPriceRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="insurancePolicy" type="{http://workstation.znawebservices.zurichna.com}InsurancePolicy" minOccurs="0"/&gt;
 *         &lt;element name="premiumSummary" type="{http://workstation.znawebservices.zurichna.com}PremiumSummary" minOccurs="0"/&gt;
 *         &lt;element name="SubmissionID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TotOthCasuality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dunsNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WCTargetPriceRequest", propOrder = {
    "insurancePolicy",
    "premiumSummary",
    "submissionID",
    "totOthCasuality",
    "dunsNumber"
})
public class WCTargetPriceRequest {

    protected InsurancePolicy insurancePolicy;
    protected PremiumSummary premiumSummary;
    @XmlElement(name = "SubmissionID")
    protected int submissionID;
    @XmlElement(name = "TotOthCasuality", defaultValue = "0")
    protected String totOthCasuality;
    protected String dunsNumber;

    /**
     * Gets the value of the insurancePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link InsurancePolicy }
     *     
     */
    public InsurancePolicy getInsurancePolicy() {
        return insurancePolicy;
    }

    /**
     * Sets the value of the insurancePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsurancePolicy }
     *     
     */
    public void setInsurancePolicy(InsurancePolicy value) {
        this.insurancePolicy = value;
    }

    /**
     * Gets the value of the premiumSummary property.
     * 
     * @return
     *     possible object is
     *     {@link PremiumSummary }
     *     
     */
    public PremiumSummary getPremiumSummary() {
        return premiumSummary;
    }

    /**
     * Sets the value of the premiumSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link PremiumSummary }
     *     
     */
    public void setPremiumSummary(PremiumSummary value) {
        this.premiumSummary = value;
    }

    /**
     * Gets the value of the submissionID property.
     * 
     */
    public int getSubmissionID() {
        return submissionID;
    }

    /**
     * Sets the value of the submissionID property.
     * 
     */
    public void setSubmissionID(int value) {
        this.submissionID = value;
    }

    /**
     * Gets the value of the totOthCasuality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotOthCasuality() {
        return totOthCasuality;
    }

    /**
     * Sets the value of the totOthCasuality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotOthCasuality(String value) {
        this.totOthCasuality = value;
    }

    /**
     * Gets the value of the dunsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDunsNumber() {
        return dunsNumber;
    }

    /**
     * Sets the value of the dunsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDunsNumber(String value) {
        this.dunsNumber = value;
    }

}
