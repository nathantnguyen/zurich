
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfUnderwritingAssts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfUnderwritingAssts"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnderwritingAssts" type="{http://workstation.znawebservices.zurichna.com}UnderwritingAssts" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfUnderwritingAssts", propOrder = {
    "underwritingAssts"
})
public class ArrayOfUnderwritingAssts {

    @XmlElement(name = "UnderwritingAssts", nillable = true)
    protected List<UnderwritingAssts> underwritingAssts;

    /**
     * Gets the value of the underwritingAssts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the underwritingAssts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnderwritingAssts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UnderwritingAssts }
     * 
     * 
     */
    public List<UnderwritingAssts> getUnderwritingAssts() {
        if (underwritingAssts == null) {
            underwritingAssts = new ArrayList<UnderwritingAssts>();
        }
        return this.underwritingAssts;
    }

}
