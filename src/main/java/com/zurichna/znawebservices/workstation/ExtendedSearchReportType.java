
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExtendedSearchReportType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ExtendedSearchReportType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Customer"/&gt;
 *     &lt;enumeration value="CustomerSmall"/&gt;
 *     &lt;enumeration value="Submission"/&gt;
 *     &lt;enumeration value="SubmissionSmall"/&gt;
 *     &lt;enumeration value="Policy"/&gt;
 *     &lt;enumeration value="PolicySmall"/&gt;
 *     &lt;enumeration value="Insured"/&gt;
 *     &lt;enumeration value="InsuredPolicy"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ExtendedSearchReportType")
@XmlEnum
public enum ExtendedSearchReportType {

    @XmlEnumValue("Customer")
    CUSTOMER("Customer"),
    @XmlEnumValue("CustomerSmall")
    CUSTOMER_SMALL("CustomerSmall"),
    @XmlEnumValue("Submission")
    SUBMISSION("Submission"),
    @XmlEnumValue("SubmissionSmall")
    SUBMISSION_SMALL("SubmissionSmall"),
    @XmlEnumValue("Policy")
    POLICY("Policy"),
    @XmlEnumValue("PolicySmall")
    POLICY_SMALL("PolicySmall"),
    @XmlEnumValue("Insured")
    INSURED("Insured"),
    @XmlEnumValue("InsuredPolicy")
    INSURED_POLICY("InsuredPolicy");
    private final String value;

    ExtendedSearchReportType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExtendedSearchReportType fromValue(String v) {
        for (ExtendedSearchReportType c: ExtendedSearchReportType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
