
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContactTelephone complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactTelephone"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TELC_NBR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="GLOC_RNM_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ROL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TELC_NBR_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TELC_NBR_TYP_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AREA_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="XCH_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TE4_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TEL_EXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PROL_TELC_NBR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTN_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactTelephone", propOrder = {
    "ptyid",
    "telcnbrid",
    "glocrnmid",
    "rolid",
    "telcnbrtypid",
    "telcnbrtypnm",
    "areacd",
    "xchnbr",
    "te4NBR",
    "telext",
    "enttistmp",
    "proltelcnbrid",
    "ptnenttistmp",
    "changeState"
})
public class ContactTelephone {

    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "TELC_NBR_ID")
    protected int telcnbrid;
    @XmlElement(name = "GLOC_RNM_ID")
    protected int glocrnmid;
    @XmlElement(name = "ROL_ID")
    protected int rolid;
    @XmlElement(name = "TELC_NBR_TYP_ID")
    protected int telcnbrtypid;
    @XmlElement(name = "TELC_NBR_TYP_NM")
    protected String telcnbrtypnm;
    @XmlElement(name = "AREA_CD")
    protected String areacd;
    @XmlElement(name = "XCH_NBR")
    protected String xchnbr;
    @XmlElement(name = "TE4_NBR")
    protected String te4NBR;
    @XmlElement(name = "TEL_EXT")
    protected String telext;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "PROL_TELC_NBR_ID")
    protected int proltelcnbrid;
    @XmlElement(name = "PTN_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ptnenttistmp;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the telcnbrid property.
     * 
     */
    public int getTELCNBRID() {
        return telcnbrid;
    }

    /**
     * Sets the value of the telcnbrid property.
     * 
     */
    public void setTELCNBRID(int value) {
        this.telcnbrid = value;
    }

    /**
     * Gets the value of the glocrnmid property.
     * 
     */
    public int getGLOCRNMID() {
        return glocrnmid;
    }

    /**
     * Sets the value of the glocrnmid property.
     * 
     */
    public void setGLOCRNMID(int value) {
        this.glocrnmid = value;
    }

    /**
     * Gets the value of the rolid property.
     * 
     */
    public int getROLID() {
        return rolid;
    }

    /**
     * Sets the value of the rolid property.
     * 
     */
    public void setROLID(int value) {
        this.rolid = value;
    }

    /**
     * Gets the value of the telcnbrtypid property.
     * 
     */
    public int getTELCNBRTYPID() {
        return telcnbrtypid;
    }

    /**
     * Sets the value of the telcnbrtypid property.
     * 
     */
    public void setTELCNBRTYPID(int value) {
        this.telcnbrtypid = value;
    }

    /**
     * Gets the value of the telcnbrtypnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTELCNBRTYPNM() {
        return telcnbrtypnm;
    }

    /**
     * Sets the value of the telcnbrtypnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTELCNBRTYPNM(String value) {
        this.telcnbrtypnm = value;
    }

    /**
     * Gets the value of the areacd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAREACD() {
        return areacd;
    }

    /**
     * Sets the value of the areacd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAREACD(String value) {
        this.areacd = value;
    }

    /**
     * Gets the value of the xchnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXCHNBR() {
        return xchnbr;
    }

    /**
     * Sets the value of the xchnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXCHNBR(String value) {
        this.xchnbr = value;
    }

    /**
     * Gets the value of the te4NBR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTE4NBR() {
        return te4NBR;
    }

    /**
     * Sets the value of the te4NBR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTE4NBR(String value) {
        this.te4NBR = value;
    }

    /**
     * Gets the value of the telext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTELEXT() {
        return telext;
    }

    /**
     * Sets the value of the telext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTELEXT(String value) {
        this.telext = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the proltelcnbrid property.
     * 
     */
    public int getPROLTELCNBRID() {
        return proltelcnbrid;
    }

    /**
     * Sets the value of the proltelcnbrid property.
     * 
     */
    public void setPROLTELCNBRID(int value) {
        this.proltelcnbrid = value;
    }

    /**
     * Gets the value of the ptnenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPTNENTTISTMP() {
        return ptnenttistmp;
    }

    /**
     * Sets the value of the ptnenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPTNENTTISTMP(XMLGregorianCalendar value) {
        this.ptnenttistmp = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
