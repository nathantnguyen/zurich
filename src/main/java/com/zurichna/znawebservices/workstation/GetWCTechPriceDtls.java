
package com.zurichna.znawebservices.workstation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetWCTechPriceDtls complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetWCTechPriceDtls"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="iSAN_NUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="decTot_MNL_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetWCTechPriceDtls", propOrder = {
    "isannum",
    "decTotMNLPREMAMT"
})
public class GetWCTechPriceDtls
    extends RequestBase
{

    @XmlElement(name = "iSAN_NUM")
    protected String isannum;
    @XmlElement(name = "decTot_MNL_PREM_AMT", required = true)
    protected BigDecimal decTotMNLPREMAMT;

    /**
     * Gets the value of the isannum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISANNUM() {
        return isannum;
    }

    /**
     * Sets the value of the isannum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISANNUM(String value) {
        this.isannum = value;
    }

    /**
     * Gets the value of the decTotMNLPREMAMT property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDecTotMNLPREMAMT() {
        return decTotMNLPREMAMT;
    }

    /**
     * Sets the value of the decTotMNLPREMAMT property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDecTotMNLPREMAMT(BigDecimal value) {
        this.decTotMNLPREMAMT = value;
    }

}
