
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetContactResult" type="{http://workstation.znawebservices.zurichna.com}ContactResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getContactResult"
})
@XmlRootElement(name = "GetContactResponse")
public class GetContactResponse {

    @XmlElement(name = "GetContactResult")
    protected ContactResponse getContactResult;

    /**
     * Gets the value of the getContactResult property.
     * 
     * @return
     *     possible object is
     *     {@link ContactResponse }
     *     
     */
    public ContactResponse getGetContactResult() {
        return getContactResult;
    }

    /**
     * Sets the value of the getContactResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactResponse }
     *     
     */
    public void setGetContactResult(ContactResponse value) {
        this.getContactResult = value;
    }

}
