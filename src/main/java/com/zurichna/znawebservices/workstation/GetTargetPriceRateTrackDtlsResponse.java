
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetTargetPriceRateTrackDtlsResult" type="{http://workstation.znawebservices.zurichna.com}GetTargetPriceRateTrackDtlsResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTargetPriceRateTrackDtlsResult"
})
@XmlRootElement(name = "GetTargetPriceRateTrackDtlsResponse")
public class GetTargetPriceRateTrackDtlsResponse {

    @XmlElement(name = "GetTargetPriceRateTrackDtlsResult")
    protected GetTargetPriceRateTrackDtlsResponse2 getTargetPriceRateTrackDtlsResult;

    /**
     * Gets the value of the getTargetPriceRateTrackDtlsResult property.
     * 
     * @return
     *     possible object is
     *     {@link GetTargetPriceRateTrackDtlsResponse2 }
     *     
     */
    public GetTargetPriceRateTrackDtlsResponse2 getGetTargetPriceRateTrackDtlsResult() {
        return getTargetPriceRateTrackDtlsResult;
    }

    /**
     * Sets the value of the getTargetPriceRateTrackDtlsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTargetPriceRateTrackDtlsResponse2 }
     *     
     */
    public void setGetTargetPriceRateTrackDtlsResult(GetTargetPriceRateTrackDtlsResponse2 value) {
        this.getTargetPriceRateTrackDtlsResult = value;
    }

}
