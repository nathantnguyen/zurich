
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetWCCATModellingResultsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetWCCATModellingResultsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="wcCATModellingResponseData" type="{http://workstation.znawebservices.zurichna.com}WCCATModellingResponseData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetWCCATModellingResultsResponse", propOrder = {
    "wcCATModellingResponseData"
})
public class GetWCCATModellingResultsResponse2 {

    protected WCCATModellingResponseData wcCATModellingResponseData;

    /**
     * Gets the value of the wcCATModellingResponseData property.
     * 
     * @return
     *     possible object is
     *     {@link WCCATModellingResponseData }
     *     
     */
    public WCCATModellingResponseData getWcCATModellingResponseData() {
        return wcCATModellingResponseData;
    }

    /**
     * Sets the value of the wcCATModellingResponseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link WCCATModellingResponseData }
     *     
     */
    public void setWcCATModellingResponseData(WCCATModellingResponseData value) {
        this.wcCATModellingResponseData = value;
    }

}
