
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SIC complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SIC"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CUST_GP_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SIC", propOrder = {
    "siccd",
    "sicnm",
    "effdt",
    "expidt",
    "enttistmp",
    "custgpid"
})
public class SIC {

    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "SIC_NM")
    protected String sicnm;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "CUST_GP_ID")
    protected String custgpid;

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the sicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICNM() {
        return sicnm;
    }

    /**
     * Sets the value of the sicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICNM(String value) {
        this.sicnm = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the custgpid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTGPID() {
        return custgpid;
    }

    /**
     * Sets the value of the custgpid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTGPID(String value) {
        this.custgpid = value;
    }

}
