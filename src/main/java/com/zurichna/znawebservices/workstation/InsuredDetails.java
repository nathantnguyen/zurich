
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InsuredDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuredDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PHTC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PTY_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_ROL_TYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ROL_KND_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="DUNS_PND_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SITE_DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ENPRS_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DUNS_FAM_IND" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ULT_PARNT_DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HDQT_DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NAIC_CO_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TRD_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FED_TAX_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IMDTPARNT_DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HONR_TTL_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEX_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TTL_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SSEC_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NCK_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BUSN_TYP_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BUSN_TTL_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PTY_ROL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_ROL_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CRMS_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PTY_CRMS_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_CRMS_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PTY_ROL_SIC_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PTY_ROL_SIC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="INTERNET_ADDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEC_SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEC_SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ADDR_CHG_FLG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NAME_CHG_FLG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="address" type="{http://workstation.znawebservices.zurichna.com}ArrayOfAddress" minOccurs="0"/&gt;
 *         &lt;element name="telephone" type="{http://workstation.znawebservices.zurichna.com}ArrayOfTelephone" minOccurs="0"/&gt;
 *         &lt;element name="customerAdditionalInformation" type="{http://workstation.znawebservices.zurichna.com}ArrayOfCustomerAdditionalInformation" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuredDetails", propOrder = {
    "ptyid",
    "effdt",
    "expidt",
    "enttistmp",
    "phtcnm",
    "ptytypid",
    "ptyroltyp",
    "rolkndid",
    "dunspndind",
    "nm",
    "sitedunsnbr",
    "enprsid",
    "dunsnbr",
    "dunsfamind",
    "ultparntdunsnbr",
    "hdqtdunsnbr",
    "naiccocd",
    "trdnm",
    "fedtaxid",
    "abbr",
    "imdtparntdunsnbr",
    "lstnm",
    "fstnm",
    "honrttldesc",
    "sexcd",
    "ttldesc",
    "ssecnbr",
    "ncknm",
    "busntypcd",
    "busnttldesc",
    "ptyrolid",
    "ptyrolenttistmp",
    "crmsptyid",
    "ptycrmsid",
    "ptycrmsenttistmp",
    "siccd",
    "ptyrolsicenttistmp",
    "ptyrolsicid",
    "internetaddr",
    "secsiccd",
    "secsicnm",
    "addrchgflg",
    "namechgflg",
    "changeState",
    "address",
    "telephone",
    "customerAdditionalInformation"
})
public class InsuredDetails {

    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "PHTC_NM")
    protected String phtcnm;
    @XmlElement(name = "PTY_TYP_ID")
    protected int ptytypid;
    @XmlElement(name = "PTY_ROL_TYP")
    protected String ptyroltyp;
    @XmlElement(name = "ROL_KND_ID")
    protected int rolkndid;
    @XmlElement(name = "DUNS_PND_IND")
    protected String dunspndind;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "SITE_DUNS_NBR")
    protected String sitedunsnbr;
    @XmlElement(name = "ENPRS_ID")
    protected String enprsid;
    @XmlElement(name = "DUNS_NBR")
    protected String dunsnbr;
    @XmlElement(name = "DUNS_FAM_IND")
    protected String dunsfamind;
    @XmlElement(name = "ULT_PARNT_DUNS_NBR")
    protected String ultparntdunsnbr;
    @XmlElement(name = "HDQT_DUNS_NBR")
    protected String hdqtdunsnbr;
    @XmlElement(name = "NAIC_CO_CD")
    protected String naiccocd;
    @XmlElement(name = "TRD_NM")
    protected String trdnm;
    @XmlElement(name = "FED_TAX_ID")
    protected String fedtaxid;
    @XmlElement(name = "ABBR")
    protected String abbr;
    @XmlElement(name = "IMDTPARNT_DUNS_NBR")
    protected String imdtparntdunsnbr;
    @XmlElement(name = "LST_NM")
    protected String lstnm;
    @XmlElement(name = "FST_NM")
    protected String fstnm;
    @XmlElement(name = "HONR_TTL_DESC")
    protected String honrttldesc;
    @XmlElement(name = "SEX_CD")
    protected String sexcd;
    @XmlElement(name = "TTL_DESC")
    protected String ttldesc;
    @XmlElement(name = "SSEC_NBR")
    protected String ssecnbr;
    @XmlElement(name = "NCK_NM")
    protected String ncknm;
    @XmlElement(name = "BUSN_TYP_CD")
    protected String busntypcd;
    @XmlElement(name = "BUSN_TTL_DESC")
    protected String busnttldesc;
    @XmlElement(name = "PTY_ROL_ID")
    protected int ptyrolid;
    @XmlElement(name = "PTY_ROL_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ptyrolenttistmp;
    @XmlElement(name = "CRMS_PTY_ID")
    protected String crmsptyid;
    @XmlElement(name = "PTY_CRMS_ID")
    protected int ptycrmsid;
    @XmlElement(name = "PTY_CRMS_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ptycrmsenttistmp;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "PTY_ROL_SIC_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ptyrolsicenttistmp;
    @XmlElement(name = "PTY_ROL_SIC_ID")
    protected int ptyrolsicid;
    @XmlElement(name = "INTERNET_ADDR")
    protected String internetaddr;
    @XmlElement(name = "SEC_SIC_CD")
    protected String secsiccd;
    @XmlElement(name = "SEC_SIC_NM")
    protected String secsicnm;
    @XmlElement(name = "ADDR_CHG_FLG")
    protected String addrchgflg;
    @XmlElement(name = "NAME_CHG_FLG")
    protected String namechgflg;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfAddress address;
    protected ArrayOfTelephone telephone;
    protected ArrayOfCustomerAdditionalInformation customerAdditionalInformation;

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the phtcnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPHTCNM() {
        return phtcnm;
    }

    /**
     * Sets the value of the phtcnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPHTCNM(String value) {
        this.phtcnm = value;
    }

    /**
     * Gets the value of the ptytypid property.
     * 
     */
    public int getPTYTYPID() {
        return ptytypid;
    }

    /**
     * Sets the value of the ptytypid property.
     * 
     */
    public void setPTYTYPID(int value) {
        this.ptytypid = value;
    }

    /**
     * Gets the value of the ptyroltyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTYROLTYP() {
        return ptyroltyp;
    }

    /**
     * Sets the value of the ptyroltyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTYROLTYP(String value) {
        this.ptyroltyp = value;
    }

    /**
     * Gets the value of the rolkndid property.
     * 
     */
    public int getROLKNDID() {
        return rolkndid;
    }

    /**
     * Sets the value of the rolkndid property.
     * 
     */
    public void setROLKNDID(int value) {
        this.rolkndid = value;
    }

    /**
     * Gets the value of the dunspndind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNSPNDIND() {
        return dunspndind;
    }

    /**
     * Sets the value of the dunspndind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNSPNDIND(String value) {
        this.dunspndind = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the sitedunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSITEDUNSNBR() {
        return sitedunsnbr;
    }

    /**
     * Sets the value of the sitedunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSITEDUNSNBR(String value) {
        this.sitedunsnbr = value;
    }

    /**
     * Gets the value of the enprsid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENPRSID() {
        return enprsid;
    }

    /**
     * Sets the value of the enprsid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENPRSID(String value) {
        this.enprsid = value;
    }

    /**
     * Gets the value of the dunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNSNBR() {
        return dunsnbr;
    }

    /**
     * Sets the value of the dunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNSNBR(String value) {
        this.dunsnbr = value;
    }

    /**
     * Gets the value of the dunsfamind property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNSFAMIND() {
        return dunsfamind;
    }

    /**
     * Sets the value of the dunsfamind property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNSFAMIND(String value) {
        this.dunsfamind = value;
    }

    /**
     * Gets the value of the ultparntdunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTPARNTDUNSNBR() {
        return ultparntdunsnbr;
    }

    /**
     * Sets the value of the ultparntdunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTPARNTDUNSNBR(String value) {
        this.ultparntdunsnbr = value;
    }

    /**
     * Gets the value of the hdqtdunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHDQTDUNSNBR() {
        return hdqtdunsnbr;
    }

    /**
     * Sets the value of the hdqtdunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHDQTDUNSNBR(String value) {
        this.hdqtdunsnbr = value;
    }

    /**
     * Gets the value of the naiccocd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAICCOCD() {
        return naiccocd;
    }

    /**
     * Sets the value of the naiccocd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAICCOCD(String value) {
        this.naiccocd = value;
    }

    /**
     * Gets the value of the trdnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRDNM() {
        return trdnm;
    }

    /**
     * Sets the value of the trdnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRDNM(String value) {
        this.trdnm = value;
    }

    /**
     * Gets the value of the fedtaxid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFEDTAXID() {
        return fedtaxid;
    }

    /**
     * Sets the value of the fedtaxid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFEDTAXID(String value) {
        this.fedtaxid = value;
    }

    /**
     * Gets the value of the abbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getABBR() {
        return abbr;
    }

    /**
     * Sets the value of the abbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setABBR(String value) {
        this.abbr = value;
    }

    /**
     * Gets the value of the imdtparntdunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMDTPARNTDUNSNBR() {
        return imdtparntdunsnbr;
    }

    /**
     * Sets the value of the imdtparntdunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMDTPARNTDUNSNBR(String value) {
        this.imdtparntdunsnbr = value;
    }

    /**
     * Gets the value of the lstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLSTNM() {
        return lstnm;
    }

    /**
     * Sets the value of the lstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLSTNM(String value) {
        this.lstnm = value;
    }

    /**
     * Gets the value of the fstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFSTNM() {
        return fstnm;
    }

    /**
     * Sets the value of the fstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFSTNM(String value) {
        this.fstnm = value;
    }

    /**
     * Gets the value of the honrttldesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHONRTTLDESC() {
        return honrttldesc;
    }

    /**
     * Sets the value of the honrttldesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHONRTTLDESC(String value) {
        this.honrttldesc = value;
    }

    /**
     * Gets the value of the sexcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEXCD() {
        return sexcd;
    }

    /**
     * Sets the value of the sexcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEXCD(String value) {
        this.sexcd = value;
    }

    /**
     * Gets the value of the ttldesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTTLDESC() {
        return ttldesc;
    }

    /**
     * Sets the value of the ttldesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTTLDESC(String value) {
        this.ttldesc = value;
    }

    /**
     * Gets the value of the ssecnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSECNBR() {
        return ssecnbr;
    }

    /**
     * Sets the value of the ssecnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSECNBR(String value) {
        this.ssecnbr = value;
    }

    /**
     * Gets the value of the ncknm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNCKNM() {
        return ncknm;
    }

    /**
     * Sets the value of the ncknm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNCKNM(String value) {
        this.ncknm = value;
    }

    /**
     * Gets the value of the busntypcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNTYPCD() {
        return busntypcd;
    }

    /**
     * Sets the value of the busntypcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNTYPCD(String value) {
        this.busntypcd = value;
    }

    /**
     * Gets the value of the busnttldesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSNTTLDESC() {
        return busnttldesc;
    }

    /**
     * Sets the value of the busnttldesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSNTTLDESC(String value) {
        this.busnttldesc = value;
    }

    /**
     * Gets the value of the ptyrolid property.
     * 
     */
    public int getPTYROLID() {
        return ptyrolid;
    }

    /**
     * Sets the value of the ptyrolid property.
     * 
     */
    public void setPTYROLID(int value) {
        this.ptyrolid = value;
    }

    /**
     * Gets the value of the ptyrolenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPTYROLENTTISTMP() {
        return ptyrolenttistmp;
    }

    /**
     * Sets the value of the ptyrolenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPTYROLENTTISTMP(XMLGregorianCalendar value) {
        this.ptyrolenttistmp = value;
    }

    /**
     * Gets the value of the crmsptyid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRMSPTYID() {
        return crmsptyid;
    }

    /**
     * Sets the value of the crmsptyid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRMSPTYID(String value) {
        this.crmsptyid = value;
    }

    /**
     * Gets the value of the ptycrmsid property.
     * 
     */
    public int getPTYCRMSID() {
        return ptycrmsid;
    }

    /**
     * Sets the value of the ptycrmsid property.
     * 
     */
    public void setPTYCRMSID(int value) {
        this.ptycrmsid = value;
    }

    /**
     * Gets the value of the ptycrmsenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPTYCRMSENTTISTMP() {
        return ptycrmsenttistmp;
    }

    /**
     * Sets the value of the ptycrmsenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPTYCRMSENTTISTMP(XMLGregorianCalendar value) {
        this.ptycrmsenttistmp = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the ptyrolsicenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPTYROLSICENTTISTMP() {
        return ptyrolsicenttistmp;
    }

    /**
     * Sets the value of the ptyrolsicenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPTYROLSICENTTISTMP(XMLGregorianCalendar value) {
        this.ptyrolsicenttistmp = value;
    }

    /**
     * Gets the value of the ptyrolsicid property.
     * 
     */
    public int getPTYROLSICID() {
        return ptyrolsicid;
    }

    /**
     * Sets the value of the ptyrolsicid property.
     * 
     */
    public void setPTYROLSICID(int value) {
        this.ptyrolsicid = value;
    }

    /**
     * Gets the value of the internetaddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTERNETADDR() {
        return internetaddr;
    }

    /**
     * Sets the value of the internetaddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTERNETADDR(String value) {
        this.internetaddr = value;
    }

    /**
     * Gets the value of the secsiccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSECSICCD() {
        return secsiccd;
    }

    /**
     * Sets the value of the secsiccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSECSICCD(String value) {
        this.secsiccd = value;
    }

    /**
     * Gets the value of the secsicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSECSICNM() {
        return secsicnm;
    }

    /**
     * Sets the value of the secsicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSECSICNM(String value) {
        this.secsicnm = value;
    }

    /**
     * Gets the value of the addrchgflg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDRCHGFLG() {
        return addrchgflg;
    }

    /**
     * Sets the value of the addrchgflg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDRCHGFLG(String value) {
        this.addrchgflg = value;
    }

    /**
     * Gets the value of the namechgflg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAMECHGFLG() {
        return namechgflg;
    }

    /**
     * Sets the value of the namechgflg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAMECHGFLG(String value) {
        this.namechgflg = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAddress }
     *     
     */
    public ArrayOfAddress getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAddress }
     *     
     */
    public void setAddress(ArrayOfAddress value) {
        this.address = value;
    }

    /**
     * Gets the value of the telephone property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTelephone }
     *     
     */
    public ArrayOfTelephone getTelephone() {
        return telephone;
    }

    /**
     * Sets the value of the telephone property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTelephone }
     *     
     */
    public void setTelephone(ArrayOfTelephone value) {
        this.telephone = value;
    }

    /**
     * Gets the value of the customerAdditionalInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomerAdditionalInformation }
     *     
     */
    public ArrayOfCustomerAdditionalInformation getCustomerAdditionalInformation() {
        return customerAdditionalInformation;
    }

    /**
     * Sets the value of the customerAdditionalInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomerAdditionalInformation }
     *     
     */
    public void setCustomerAdditionalInformation(ArrayOfCustomerAdditionalInformation value) {
        this.customerAdditionalInformation = value;
    }

}
