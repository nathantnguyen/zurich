
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuickSearchResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuickSearchResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QuickSrchCustomer" type="{http://workstation.znawebservices.zurichna.com}ArrayOfQuickSerachCustomer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuickSearchResponse", propOrder = {
    "quickSrchCustomer"
})
public class QuickSearchResponse2
    extends ResponseBase
{

    @XmlElement(name = "QuickSrchCustomer")
    protected ArrayOfQuickSerachCustomer quickSrchCustomer;

    /**
     * Gets the value of the quickSrchCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfQuickSerachCustomer }
     *     
     */
    public ArrayOfQuickSerachCustomer getQuickSrchCustomer() {
        return quickSrchCustomer;
    }

    /**
     * Sets the value of the quickSrchCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfQuickSerachCustomer }
     *     
     */
    public void setQuickSrchCustomer(ArrayOfQuickSerachCustomer value) {
        this.quickSrchCustomer = value;
    }

}
