
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSubmissionProductsV2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSubmissionProductsV2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SubmissionProductsV2" type="{http://workstation.znawebservices.zurichna.com}SubmissionProductsV2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSubmissionProductsV2", propOrder = {
    "submissionProductsV2"
})
public class ArrayOfSubmissionProductsV2 {

    @XmlElement(name = "SubmissionProductsV2", nillable = true)
    protected List<SubmissionProductsV2> submissionProductsV2;

    /**
     * Gets the value of the submissionProductsV2 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the submissionProductsV2 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubmissionProductsV2().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubmissionProductsV2 }
     * 
     * 
     */
    public List<SubmissionProductsV2> getSubmissionProductsV2() {
        if (submissionProductsV2 == null) {
            submissionProductsV2 = new ArrayList<SubmissionProductsV2>();
        }
        return this.submissionProductsV2;
    }

}
