
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WCCATModellingResponseData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WCCATModellingResponseData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="wcCATModellingResults" type="{http://workstation.znawebservices.zurichna.com}ArrayOfWCCATModellingResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WCCATModellingResponseData", propOrder = {
    "wcCATModellingResults"
})
public class WCCATModellingResponseData
    extends ResponseBase
{

    protected ArrayOfWCCATModellingResult wcCATModellingResults;

    /**
     * Gets the value of the wcCATModellingResults property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWCCATModellingResult }
     *     
     */
    public ArrayOfWCCATModellingResult getWcCATModellingResults() {
        return wcCATModellingResults;
    }

    /**
     * Sets the value of the wcCATModellingResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWCCATModellingResult }
     *     
     */
    public void setWcCATModellingResults(ArrayOfWCCATModellingResult value) {
        this.wcCATModellingResults = value;
    }

}
