
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Alert complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Alert"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EVNT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EVNT_IX_NBR" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EVNT_ROOT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EVNT_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SUB_EVNT_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ACCT_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CRTE_BY_USR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CRTE_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ASND_TO_USR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ASND_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CMPL_BY_USR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CMPL_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="DUE_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EVNT_TTL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EVNT_DTL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DYS_UNTL_NOTF_NBR" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RECR_NOTF_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="EVNT_LFTM_DYS" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PST_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="EVNT_TYP_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CRTE_BY_USR_ID_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CRTE_BY_USR_ID_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CRTE_BY_USR_ID_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASND_TO_USR_ID_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASND_TO_USR_ID_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ASND_TO_USR_ID_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CMPL_BY_USR_ID_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CMPL_BY_USR_ID_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CMPL_BY_USR_ID_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ACCT_ID_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ARC_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Alert", propOrder = {
    "evntid",
    "evntixnbr",
    "evntrootid",
    "evnttypid",
    "subevnttypid",
    "acctid",
    "crtebyusrid",
    "crtedt",
    "asndtousrid",
    "asnddt",
    "cmplbyusrid",
    "cmpldt",
    "duedt",
    "evntttl",
    "evntdtl",
    "dysuntlnotfnbr",
    "recrnotfind",
    "evntlftmdys",
    "pstid",
    "enttistmp",
    "evnttypnm",
    "crtebyusridnm",
    "crtebyusridfstnm",
    "crtebyusridlstnm",
    "asndtousridnm",
    "asndtousridfstnm",
    "asndtousridlstnm",
    "cmplbyusridnm",
    "cmplbyusridfstnm",
    "cmplbyusridlstnm",
    "acctidnm",
    "arcdt",
    "changeState"
})
public class Alert {

    @XmlElement(name = "EVNT_ID")
    protected int evntid;
    @XmlElement(name = "EVNT_IX_NBR")
    protected int evntixnbr;
    @XmlElement(name = "EVNT_ROOT_ID")
    protected String evntrootid;
    @XmlElement(name = "EVNT_TYP_ID")
    protected int evnttypid;
    @XmlElement(name = "SUB_EVNT_TYP_ID")
    protected int subevnttypid;
    @XmlElement(name = "ACCT_ID")
    protected int acctid;
    @XmlElement(name = "CRTE_BY_USR_ID")
    protected int crtebyusrid;
    @XmlElement(name = "CRTE_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar crtedt;
    @XmlElement(name = "ASND_TO_USR_ID")
    protected int asndtousrid;
    @XmlElement(name = "ASND_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar asnddt;
    @XmlElement(name = "CMPL_BY_USR_ID")
    protected int cmplbyusrid;
    @XmlElement(name = "CMPL_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar cmpldt;
    @XmlElement(name = "DUE_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar duedt;
    @XmlElement(name = "EVNT_TTL")
    protected String evntttl;
    @XmlElement(name = "EVNT_DTL")
    protected String evntdtl;
    @XmlElement(name = "DYS_UNTL_NOTF_NBR")
    protected int dysuntlnotfnbr;
    @XmlElement(name = "RECR_NOTF_IND")
    protected boolean recrnotfind;
    @XmlElement(name = "EVNT_LFTM_DYS")
    protected int evntlftmdys;
    @XmlElement(name = "PST_ID")
    protected int pstid;
    @XmlElement(name = "ENT_TISTMP")
    protected byte[] enttistmp;
    @XmlElement(name = "EVNT_TYP_NM")
    protected String evnttypnm;
    @XmlElement(name = "CRTE_BY_USR_ID_NM")
    protected String crtebyusridnm;
    @XmlElement(name = "CRTE_BY_USR_ID_FST_NM")
    protected String crtebyusridfstnm;
    @XmlElement(name = "CRTE_BY_USR_ID_LST_NM")
    protected String crtebyusridlstnm;
    @XmlElement(name = "ASND_TO_USR_ID_NM")
    protected String asndtousridnm;
    @XmlElement(name = "ASND_TO_USR_ID_FST_NM")
    protected String asndtousridfstnm;
    @XmlElement(name = "ASND_TO_USR_ID_LST_NM")
    protected String asndtousridlstnm;
    @XmlElement(name = "CMPL_BY_USR_ID_NM")
    protected String cmplbyusridnm;
    @XmlElement(name = "CMPL_BY_USR_ID_FST_NM")
    protected String cmplbyusridfstnm;
    @XmlElement(name = "CMPL_BY_USR_ID_LST_NM")
    protected String cmplbyusridlstnm;
    @XmlElement(name = "ACCT_ID_NM")
    protected String acctidnm;
    @XmlElement(name = "ARC_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arcdt;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;

    /**
     * Gets the value of the evntid property.
     * 
     */
    public int getEVNTID() {
        return evntid;
    }

    /**
     * Sets the value of the evntid property.
     * 
     */
    public void setEVNTID(int value) {
        this.evntid = value;
    }

    /**
     * Gets the value of the evntixnbr property.
     * 
     */
    public int getEVNTIXNBR() {
        return evntixnbr;
    }

    /**
     * Sets the value of the evntixnbr property.
     * 
     */
    public void setEVNTIXNBR(int value) {
        this.evntixnbr = value;
    }

    /**
     * Gets the value of the evntrootid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEVNTROOTID() {
        return evntrootid;
    }

    /**
     * Sets the value of the evntrootid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEVNTROOTID(String value) {
        this.evntrootid = value;
    }

    /**
     * Gets the value of the evnttypid property.
     * 
     */
    public int getEVNTTYPID() {
        return evnttypid;
    }

    /**
     * Sets the value of the evnttypid property.
     * 
     */
    public void setEVNTTYPID(int value) {
        this.evnttypid = value;
    }

    /**
     * Gets the value of the subevnttypid property.
     * 
     */
    public int getSUBEVNTTYPID() {
        return subevnttypid;
    }

    /**
     * Sets the value of the subevnttypid property.
     * 
     */
    public void setSUBEVNTTYPID(int value) {
        this.subevnttypid = value;
    }

    /**
     * Gets the value of the acctid property.
     * 
     */
    public int getACCTID() {
        return acctid;
    }

    /**
     * Sets the value of the acctid property.
     * 
     */
    public void setACCTID(int value) {
        this.acctid = value;
    }

    /**
     * Gets the value of the crtebyusrid property.
     * 
     */
    public int getCRTEBYUSRID() {
        return crtebyusrid;
    }

    /**
     * Sets the value of the crtebyusrid property.
     * 
     */
    public void setCRTEBYUSRID(int value) {
        this.crtebyusrid = value;
    }

    /**
     * Gets the value of the crtedt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCRTEDT() {
        return crtedt;
    }

    /**
     * Sets the value of the crtedt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCRTEDT(XMLGregorianCalendar value) {
        this.crtedt = value;
    }

    /**
     * Gets the value of the asndtousrid property.
     * 
     */
    public int getASNDTOUSRID() {
        return asndtousrid;
    }

    /**
     * Sets the value of the asndtousrid property.
     * 
     */
    public void setASNDTOUSRID(int value) {
        this.asndtousrid = value;
    }

    /**
     * Gets the value of the asnddt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getASNDDT() {
        return asnddt;
    }

    /**
     * Sets the value of the asnddt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setASNDDT(XMLGregorianCalendar value) {
        this.asnddt = value;
    }

    /**
     * Gets the value of the cmplbyusrid property.
     * 
     */
    public int getCMPLBYUSRID() {
        return cmplbyusrid;
    }

    /**
     * Sets the value of the cmplbyusrid property.
     * 
     */
    public void setCMPLBYUSRID(int value) {
        this.cmplbyusrid = value;
    }

    /**
     * Gets the value of the cmpldt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCMPLDT() {
        return cmpldt;
    }

    /**
     * Sets the value of the cmpldt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCMPLDT(XMLGregorianCalendar value) {
        this.cmpldt = value;
    }

    /**
     * Gets the value of the duedt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDUEDT() {
        return duedt;
    }

    /**
     * Sets the value of the duedt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDUEDT(XMLGregorianCalendar value) {
        this.duedt = value;
    }

    /**
     * Gets the value of the evntttl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEVNTTTL() {
        return evntttl;
    }

    /**
     * Sets the value of the evntttl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEVNTTTL(String value) {
        this.evntttl = value;
    }

    /**
     * Gets the value of the evntdtl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEVNTDTL() {
        return evntdtl;
    }

    /**
     * Sets the value of the evntdtl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEVNTDTL(String value) {
        this.evntdtl = value;
    }

    /**
     * Gets the value of the dysuntlnotfnbr property.
     * 
     */
    public int getDYSUNTLNOTFNBR() {
        return dysuntlnotfnbr;
    }

    /**
     * Sets the value of the dysuntlnotfnbr property.
     * 
     */
    public void setDYSUNTLNOTFNBR(int value) {
        this.dysuntlnotfnbr = value;
    }

    /**
     * Gets the value of the recrnotfind property.
     * 
     */
    public boolean isRECRNOTFIND() {
        return recrnotfind;
    }

    /**
     * Sets the value of the recrnotfind property.
     * 
     */
    public void setRECRNOTFIND(boolean value) {
        this.recrnotfind = value;
    }

    /**
     * Gets the value of the evntlftmdys property.
     * 
     */
    public int getEVNTLFTMDYS() {
        return evntlftmdys;
    }

    /**
     * Sets the value of the evntlftmdys property.
     * 
     */
    public void setEVNTLFTMDYS(int value) {
        this.evntlftmdys = value;
    }

    /**
     * Gets the value of the pstid property.
     * 
     */
    public int getPSTID() {
        return pstid;
    }

    /**
     * Sets the value of the pstid property.
     * 
     */
    public void setPSTID(int value) {
        this.pstid = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setENTTISTMP(byte[] value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the evnttypnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEVNTTYPNM() {
        return evnttypnm;
    }

    /**
     * Sets the value of the evnttypnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEVNTTYPNM(String value) {
        this.evnttypnm = value;
    }

    /**
     * Gets the value of the crtebyusridnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRTEBYUSRIDNM() {
        return crtebyusridnm;
    }

    /**
     * Sets the value of the crtebyusridnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRTEBYUSRIDNM(String value) {
        this.crtebyusridnm = value;
    }

    /**
     * Gets the value of the crtebyusridfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRTEBYUSRIDFSTNM() {
        return crtebyusridfstnm;
    }

    /**
     * Sets the value of the crtebyusridfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRTEBYUSRIDFSTNM(String value) {
        this.crtebyusridfstnm = value;
    }

    /**
     * Gets the value of the crtebyusridlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRTEBYUSRIDLSTNM() {
        return crtebyusridlstnm;
    }

    /**
     * Sets the value of the crtebyusridlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRTEBYUSRIDLSTNM(String value) {
        this.crtebyusridlstnm = value;
    }

    /**
     * Gets the value of the asndtousridnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASNDTOUSRIDNM() {
        return asndtousridnm;
    }

    /**
     * Sets the value of the asndtousridnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASNDTOUSRIDNM(String value) {
        this.asndtousridnm = value;
    }

    /**
     * Gets the value of the asndtousridfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASNDTOUSRIDFSTNM() {
        return asndtousridfstnm;
    }

    /**
     * Sets the value of the asndtousridfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASNDTOUSRIDFSTNM(String value) {
        this.asndtousridfstnm = value;
    }

    /**
     * Gets the value of the asndtousridlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASNDTOUSRIDLSTNM() {
        return asndtousridlstnm;
    }

    /**
     * Sets the value of the asndtousridlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASNDTOUSRIDLSTNM(String value) {
        this.asndtousridlstnm = value;
    }

    /**
     * Gets the value of the cmplbyusridnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMPLBYUSRIDNM() {
        return cmplbyusridnm;
    }

    /**
     * Sets the value of the cmplbyusridnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMPLBYUSRIDNM(String value) {
        this.cmplbyusridnm = value;
    }

    /**
     * Gets the value of the cmplbyusridfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMPLBYUSRIDFSTNM() {
        return cmplbyusridfstnm;
    }

    /**
     * Sets the value of the cmplbyusridfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMPLBYUSRIDFSTNM(String value) {
        this.cmplbyusridfstnm = value;
    }

    /**
     * Gets the value of the cmplbyusridlstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMPLBYUSRIDLSTNM() {
        return cmplbyusridlstnm;
    }

    /**
     * Sets the value of the cmplbyusridlstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMPLBYUSRIDLSTNM(String value) {
        this.cmplbyusridlstnm = value;
    }

    /**
     * Gets the value of the acctidnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCTIDNM() {
        return acctidnm;
    }

    /**
     * Sets the value of the acctidnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCTIDNM(String value) {
        this.acctidnm = value;
    }

    /**
     * Gets the value of the arcdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getARCDT() {
        return arcdt;
    }

    /**
     * Sets the value of the arcdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setARCDT(XMLGregorianCalendar value) {
        this.arcdt = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

}
